#include <zCore/zCore.hpp>
#include <zCore/zCoreAutolink.hpp>
#include <zImageAutolink.hpp>
using namespace zzz;

ZFLAGS_SWITCH2(print_mode, "Output all 256 ascii letters", 'p');
ZFLAGS_STRING2(input, "", "Input font map image file name", 'i');
ZFLAGS_STRING2(output, "BMPFontData.hpp", "Output c++ code file name", 'o');
ZFLAGS_INT2(height, 12, "font height in pixels", 'h');
ZFLAGS_INT2(width, 8, "font width in pixels", 'w');
ZFLAGS_INT2(row, 16, "font map row number", 'r');
ZFLAGS_INT2(col, 16, "font map column number", 'c');

int main(int argc, char *argv[])
{
  ZCMDPARSE(argv[0]);
  if (FLAGS_print_mode) {
    for (int i=0; i<16; i++)
    {
      for (int j=0; j<16; j++)
      {
        char x(i*16+j);
        if (x=='\n') x=' ';
        zout<<x;
      }
      zout<<endl;
    }
    return 0;
 }

  ZCHECK_FALSE(FLAGS_input.empty())
    <<"Need input file name!";
  Imageuc img(FLAGS_input.c_str());
  const int height=FLAGS_height;
  const int width=FLAGS_width;
  const int row=FLAGS_row;
  const int col=FLAGS_col;
  ZCHECK(img.Cols()==width*col && img.Rows()==height*row)
    <<"font size or font arrangement is not correct:"<<ZVAR(height)<<ZVAR(width)<<ZVAR(row)<<ZVAR(col);

  ofstream fo(FLAGS_output.c_str());
  fo<<"#pragma warning(disable:4309)\n";
  fo<<"const int fontwidth="<<width<<",fontheight="<<height<<"; \n";
  fo<<"const char fontmap[]={";
  for (int fr=0;fr<height;fr++) for (int r=row-1; r>=0; r--)
  {
    for (int fc=0;fc<width*col;fc++)
    {
      fo<<int(img.At(r*height+fr,fc));
      if (fr!=height-1 || r!=row-1 || fc!=width*col) //last one
        fo<<',';
    }
    fo<<"\\\n";
  }
  fo<<"}; \n";
  fo.close();
}