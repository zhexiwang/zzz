#include <zNet/Serverz/Serverz.hpp>
#include <zNet/zNetAutoLink.hpp>

#include <zCore/zCoreAutoLink.hpp>
#include <z3rd/z3rdAutoLink.hpp>

#include <zCore/Utility/FileTools.hpp>
#include <zCore/Utility/CmdParser.hpp>

using namespace zzz;
ZFLAGS_INT(port, 80, "Server port");
ZFLAGS_STRING(root, CurrentPath(), "Server root");
ZFLAGS_INT(worker, 2, "Server works");

// int main(int argc, char* argv[]) {
//   ZSIMPLEINIT();
//   HttpFileServer server(ZFLAG_port, &ZFLAG_root);
//   server.Run(ZFLAG_worker);
//   server.Wait();
//   return 0;
// }

using namespace std;

int main(int argc, char** argv) {
  ZSIMPLEINIT();
  SERVERZ.Start();
  PressAnyKeyToContinue(); 
  return 0;
}