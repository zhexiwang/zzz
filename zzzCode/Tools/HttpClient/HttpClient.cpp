#include <zCore/zCoreAutoLink.hpp>
#include <z3rd/z3rdAutoLink.hpp>
#include <zNet/zNetAutoLink.hpp>
#include <zCore/Utility/FileTools.hpp>
#include <zCore/Utility/CmdParser.hpp>
#include <zNet/Http/HttpTools.hpp>

using namespace zzz;
int main(int argc, char* argv[]) {
  ZSIMPLEINIT();
  if (ZCMDSEQ.empty()) {
    cout << argv[0] << " request_URL\n";
    return -1;
  }
  string str;
  cout << HttpGet(ZCMDSEQ[0], str) << endl;
  cout << str << endl;
  return 0;
}