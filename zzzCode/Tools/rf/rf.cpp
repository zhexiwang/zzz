#include <zCore/zCore.hpp>
#include <zCore/zCoreAutoLink.hpp>
#include <z3rd/z3rdAutoLink.hpp>
using namespace zzz;

ZFLAGS_STRING(rf, "", "RecordFile");

void PrintInfo(RecordFile *rf, int indent) {
  string ori_heading;
  for (int i = 0; i < indent; ++i) ori_heading += ' ';
  string heading;
  indent += 2;
  for (int i = 0; i < indent; ++i) heading += ' ';
  zout << " <" << endl;
  vector<pair<zint32, RecordItem::RFNodeType> > labels = rf->GetItemLabels();
  for (zuint i = 0; i < labels.size(); ++i) {
    zint32 label = labels[i].first;
    zout << heading << label << " : " << rf->GetChildItemSize(label) << " Bytes";
    if (labels[i].second == RecordItem::NON_NODE) {
      zout << endl;
    } else {
      rf->ReadChildBegin(label);
      PrintInfo(rf, indent);
      rf->ReadChildEnd();
    }
  }
  zout << ori_heading << ">" << endl;
}

int main(int argc, char* argv[]) {
  ZINIT(argv[0]);
  ZHELP("Read RecordFile.");
  ZCHECK_FALSE(ZFLAG_rf.empty());

  RecordFile rf;
  rf.LoadFileBegin(ZFLAG_rf);
  zout << ZFLAG_rf;
  PrintInfo(&rf, 0);
  rf.LoadFileEnd();
  return 0;
}