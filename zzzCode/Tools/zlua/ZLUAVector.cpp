#include "ZLUAWrapper.hpp"
#include <zCore/Math/Vector3.hpp>
using namespace zzz;

typedef zzz::VectorBase<3, double> VectorBase3d;
OOLUA_PROXY(VectorBase3d)
  OOLUA_TAGS(
  )
  OOLUA_CTORS(
    OOLUA_CTOR(const VectorBase3d &)
    OOLUA_CTOR(const double&)
  )
  OOLUA_MEM_FUNC_CONST(VectorBase3d::const_reference, at, VectorBase3d::size_type)
  OOLUA_MEM_FUNC(VectorBase3d::reference, at, VectorBase3d::size_type)
OOLUA_PROXY_END
OOLUA_EXPORT_FUNCTIONS(VectorBase3d, at)
OOLUA_EXPORT_FUNCTIONS_CONST(VectorBase3d, at)
ZLUA_REG_CLASS(VectorBase3d);

/////////////////////////////////////////////
OOLUA_PROXY(Vector3d, VectorBase3d)
  OOLUA_TAGS(
    OOLUA::Equal_op,
    OOLUA::Less_op,
    OOLUA::Less_equal_op,
    OOLUA::Add_op,
    OOLUA::Sub_op,
    OOLUA::Mul_op,
    OOLUA::Div_op
  )
  OOLUA_CTORS(
    OOLUA_CTOR(const VectorBase3d &)
    OOLUA_CTOR(const Vector3d &)
    OOLUA_CTOR(const double&)
    OOLUA_CTOR(const double&, const double&, const double&)
  )
  OOLUA_MEM_FUNC_CONST(Vector3d::const_reference, at, Vector3d::size_type)
  OOLUA_MEM_FUNC(Vector3d::reference, at, Vector3d::size_type)
OOLUA_PROXY_END
OOLUA_EXPORT_FUNCTIONS(Vector3d, at)
OOLUA_EXPORT_FUNCTIONS_CONST(Vector3d, at)
ZLUA_REG_CLASS(Vector3d);