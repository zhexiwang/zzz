#include "LuaResourceManager.hpp"
#include "oolua.h"

namespace zzz {
bool LuaResourceManager::Add(lua_CFunction func, const string &name, bool overwrite/*=true*/) {
  return AnyHolder::Add(func, name, overwrite);
}

bool LuaResourceManager::Add(RegisterClassFunction reg_class_func) {
  reg_class_funcs_.insert(reg_class_func);
  return true;
}

void LuaResourceManager::SetCFuncGlobal(lua_State *l, const string &name) {
  OOLUA::set_global(l, name.c_str(), Get<lua_CFunction>(name));
}

void LuaResourceManager::SetAllGlobal(lua_State *l) {
  for (map<string, Any>::iterator mi = holder_.begin(); mi != holder_.end(); ++mi) {
    if (mi->second.IsType<lua_CFunction>()) SetCFuncGlobal(l, mi->first);
  }
}

void LuaResourceManager::RegAllClass(lua_State *l) {
  for (set<RegisterClassFunction>::iterator si = reg_class_funcs_.begin(); si != reg_class_funcs_.end(); ++si)
    (*si)(l);
}


void LuaResourceManager::Destroy(Any &v) {

}

}   // namespace zzz