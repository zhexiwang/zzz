#pragma once
#include <zCore/Utility/AnyHolder.hpp>

extern "C" {
#include <lua/lua.h>
};

#define ZLRM zzz::LuaResourceManager::Instance()

namespace zzz {
class LuaResourceManager : public AnyHolder, public Singleton<LuaResourceManager> {
public:
  bool Add(lua_CFunction func, const string &name, bool overwrite=true);
  typedef void (*RegisterClassFunction)(lua_State *);
  bool Add(RegisterClassFunction reg_class_func);

  void SetCFuncGlobal(lua_State *l, const string &name);
  void SetAllGlobal(lua_State *l);
  void RegAllClass(lua_State *l);
private:
  void Destroy(Any &v);
  set<RegisterClassFunction> reg_class_funcs_;
public:
  struct AddCFuncHelper {
    AddCFuncHelper(lua_CFunction func, const string &name, bool overwrite=true) {
      ZLRM.Add(func, name, overwrite);
    }
  };
  struct AddRegClassHelper {
    AddRegClassHelper(RegisterClassFunction func) {
      ZLRM.Add(func);
    }
  };
};


}