#pragma once
#include <zCore/common.hpp>

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
};
#include <oolua.h>

namespace zzz {
class LuaWrapper {
public:
  LuaWrapper(void);
  ~LuaWrapper(void);

  bool RunFile(const string& filename);
  bool LoadFile(const string& filename);
  void Call(const string &func, const char *sig, ...);

  void RegisterFunction(lua_CFunction func, const string &funcname);
  OOLUA::Script L_;
};

}; // namespace zzz