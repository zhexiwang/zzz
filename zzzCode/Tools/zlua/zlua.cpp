#include <zCore/zCoreAutoLink.hpp>
#include <zCore/Math/Vector3.hpp>
#pragma comment(lib, "libluaD.lib")
#pragma comment(lib, "ooluaD.lib")
using namespace zzz;

#include "ZLUAWrapper.hpp"

void RegVectorBase(lua_State *l);
///////////////////////////////////////////////////
void HelloWorld(const char *input) {
  cout << "zLua is saying " << input << endl;
}
ZLUA_SIMPLE_CFUNC(HelloWorld);
///////////////////////////////////////////////////
//////////////////////////////////////////////////
int main (int argc, char **argv) {
  cout << OOLUA::version_str << endl;
  OOLUA::Script s;
  ZLRM.SetAllGlobal(s);
  ZLRM.RegAllClass(s);
  if (!OOLUA::run_file(s, "test.lua"))
    cout << "Error.\n";
  return 0;
}

