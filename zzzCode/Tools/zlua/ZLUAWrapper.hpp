#pragma once
#include "oolua.h"
#include "LuaResourceManager.hpp"
using namespace OOLUA;

///////// For normal c-function registration
#define ZLUA_CFUNC(name, func) \
namespace {\
  OOLUA_CFUNC(func, __ZLUA_##name);\
  LuaResourceManager::AddCFuncHelper __lrm_helper_##name(__ZLUA_##name, #name);\
}

#define ZLUA_SIMPLE_CFUNC(func) ZLUA_CFUNC(func, func)

///////// For template normal function registration
#define ZLUA_CFUNC_T(name, func, ...) \
  auto __ZLUA_##T_##name = func<__VA_ARGS__>;\
  ZLUA_CFUNC(name, __ZLUA_##T_##name);
#define ZLUA_SIMPLE_CFUNC_T(func, ...) ZLUA_CFUNC_T(func, func, __VA_ARGS__);

///////// For overloaded c-function registration
#define ZLUA_CFUNCTION(name, ret, func, ...) \
namespace {\
  int __ZLUA_##name(lua_State* l) {\
    OOLUA_C_FUNCTION(ret, func, __VA_ARGS__)\
  }\
  LuaResourceManager::AddCFuncHelper __lrm_helper_##name(__ZLUA_##name, #name);\
}
#define ZLUA_SIMPLE_CFUNCTION(ret, func, ...) ZLUA_CFUNCTION(func, ret, func, __VA_ARGS__)

#define ZLUA_REG_CLASS(T) \
  LuaResourceManager::AddRegClassHelper __reg__##T(OOLUA::register_class<T>);