HelloWorld('Hello Lua');
print(Abs(1));
print(Min(1,2));
print(Max(1,2));
print(Sqrt(2));
print(Sqr(4));
print(Cube(4));
print(Quart(4));
print(Quint(4));
print(Clamp(1,1.4,2));
print(Clamp(1,0.4,2));
print(Within(1,2,3));
print(Within(2,1,3));
print(Sign(12));
print(Sign(-134));
print(Lerp(1,5,0.23));
print(Round(1.2,1));
print(FlowToPower2(34));
print(Log2(12));
print(Log2Int(12));
print(Pow(2,4));
print(SafeACos(2.3));
print(SafeACos(-0.1));
print(WrapPI(2.3));
print(FromHex('123a'));


a = VectorBase3d.new(1)
print("Done\n")
print(a:at(0))
print(a:at(1))
print(a:at(2))

b = Vector3d.new(a)
print(b:at(0))
print(b:at(1))
print(b:at(2))

c = Vector3d.new(1,2,3)
print(c:at(0))
print(c:at(1))
print(c:at(2))
b = c
print(b:at(0))
print(b:at(1))
print(b:at(2))

d = b + c
print(d:at(0))
print(d:at(1))
print(d:at(2))
print(type(d))
