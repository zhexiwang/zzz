#include "ZLUAWrapper.hpp"
#include <zCore/Math/Math.hpp>
using namespace zzz;

// Math
ZLUA_SIMPLE_CFUNC_T(Min, double);
ZLUA_SIMPLE_CFUNC_T(Max, double);
ZLUA_SIMPLE_CFUNC_T(Abs, double);
ZLUA_SIMPLE_CFUNC_T(Sqrt, double);
ZLUA_SIMPLE_CFUNC_T(Sqr, double);
ZLUA_SIMPLE_CFUNC_T(Cube, double);
ZLUA_SIMPLE_CFUNC_T(Quart, double);
ZLUA_SIMPLE_CFUNC_T(Quint, double);
ZLUA_SIMPLE_CFUNC_T(Clamp, double);
ZLUA_SIMPLE_CFUNC_T(Within, double);
ZLUA_SIMPLE_CFUNC_T(Sign, double);
ZLUA_SIMPLE_CFUNC_T(Lerp, double);
ZLUA_SIMPLE_CFUNC(Round);
ZLUA_SIMPLE_CFUNC(FlowToPower2);
ZLUA_SIMPLE_CFUNC_T(Log2, double);
ZLUA_CFUNCTION(Log2Int, int, Log2, int);
ZLUA_SIMPLE_CFUNC_T(Pow, double, double);
ZLUA_SIMPLE_CFUNC(SafeACos);
ZLUA_SIMPLE_CFUNC(WrapPI);
ZLUA_SIMPLE_CFUNC(FromHex);

