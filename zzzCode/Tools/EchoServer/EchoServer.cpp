#include <zCore/zCoreAutoLink.hpp>
#include <z3rd/z3rdAutoLink.hpp>
#include <zNet/zNetAutoLink.hpp>
#include <zNet/Http/HttpFileServer.hpp>
#include <zCore/Utility/FileTools.hpp>
#include <zCore/Utility/CmdParser.hpp>
#include <zNet/Serverz/Serverz.hpp>
#include <zCore/Utility/IAmTheOnlyOne.hpp>

using namespace zzz;
ZFLAGS_INT(port, 7496, "Server port");

class TCPEchoSession : public TCPSession {
public:
  TCPEchoSession(boost::asio::io_service& io_service, void *userdata)
    : TCPSession(io_service, 1024 * 10, userdata) {}
  void Start() { AsyncReceive(); }
private:
  void AfterAsyncReceive(const boost::system::error_code& error, size_t bytes_transferred) {
    if (!error) {
      cout << "Received " << string(buffer_.begin(), buffer_.begin() + bytes_transferred) << endl;
      AsyncSend(&(buffer_[0]), bytes_transferred);
    }
    AsyncReceive();
  }
  void AfterAsyncSend(const boost::system::error_code& error, size_t bytes_transferred) {
    cout << "Sent " << bytes_transferred << " bytes" << endl;
  }
};

int main(int argc, char* argv[]) {
  I_AM_THE_ONLY_ONE("EchoServer");
  ZSIMPLEINIT();
  SERVERZ.Start();
  TCPServer<TCPEchoSession> server(ZFLAG_port);
  server.Run(1);
  server.Wait();
  return 0;
}
