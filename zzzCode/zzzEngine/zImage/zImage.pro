include($$PWD/../../zzzlib.pri)

QT       -= core gui

TARGET = zImage$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    $$files($$PWD/zImage/Image/*.cpp) \
    $$files($$PWD/zImage/ImageAlgo/*.cpp) \
    $$files($$PWD/zImage/ImageIO/*.cpp) \
    $$files($$PWD/zImage/*.cpp)

HEADERS += \
    $$files($$PWD/zImage/Image/*.hpp) \
    $$files($$PWD/zImage/ImageAlgo/*.hpp) \
    $$files($$PWD/zImage/ImageIO/*.hpp) \
    $$files($$PWD/zImage/*.hpp)

OTHER_FILES += \

Z3RDENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZ3rd.pri) {
    error("Cannot include QTFlagsZ3rd.pri")
}

ZCOREENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZCore.pri) {
    error("Cannot include QTFlagsZCore.pri")
}

ZMATRIXENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZMatrix.pri) {
    error("Cannot include QTFlagsZMatrix.pri")
}

OPENCVENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsOpenCV.pri) {
    error("Cannot include QTFlagsOpenCV.pri")
}

DEFINES = $$replace(DEFINES, ZETA, ZZZ_LIB)
DEFINES = $$replace(DEFINES, _ENABLE, )
message($$DEFINES)
