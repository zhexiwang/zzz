#pragma once

// Separate link so when change a lib, only link needs redo.
#include "zImageConfig.hpp"

#ifndef ZZZ_NO_PRAGMA_LIB

#ifdef ZZZ_COMPILER_MSVC

//#ifdef ZZZ_LIB_LIBJPEG
//#ifdef ZZZ_OS_WIN64
//#pragma comment(lib,"jpeg_x64.lib")
//#else
//#pragma comment(lib,"jpeg.lib")
//#endif
//#endif
//
//#ifdef ZZZ_LIB_LIBPNG
//#ifdef ZZZ_OS_WIN64
//#pragma comment(lib,"libpng15_x64.lib")
//#else
//#pragma comment(lib,"libpng15.lib")
//#endif
//#endif
//
//#ifdef ZZZ_LIB_LIBTIFF
//#ifdef ZZZ_OS_WIN64
//#pragma comment(lib,"libtiff_x64.lib")
//#else
//#pragma comment(lib,"libtiff.lib")
//#endif
//#endif

#ifdef ZZZ_LIB_DEVIL
#ifndef ZZZ_OS_WIN64
#pragma comment(lib,"devil.lib")
#pragma comment(lib,"ilu.lib")
#pragma comment(lib,"ilut.lib")
#else
#pragma comment(lib,"devil_x64.lib")
#pragma comment(lib,"ilu_x64.lib")
#pragma comment(lib,"ilut_x64.lib")
#endif
#endif // ZZZ_LIB_DEVIL

#ifdef ZZZ_LIB_FREEIMAGE
#pragma comment(lib,"FreeImage.lib")
#endif // ZZZ_LIB_FREEIMAGE


#endif // ZZZ_COMPILER_MSVC

#endif // ZZZ_NO_PRAGMA_LIB