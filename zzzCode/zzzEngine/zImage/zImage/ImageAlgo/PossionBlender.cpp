#include "PossionBlender.hpp"
#include <deque>
#include <algorithm>
#include <zCore/Math/Random.hpp>
#ifdef ZZZ_LIB_SUPERLU
namespace zzz{
bool noboundary = true;
void PossionBlender::BlendIterative(Image3uc *const src, int srcindex, Image3uc *dst, int dstindex, Array2i *mask) {
  m_src=src;
  m_mask=mask;
  m_dst=dst;
  m_srcindex=srcindex;
  m_dstindex=dstindex;
  m_nrow=mask->Size(0);
  m_ncol=mask->Size(1);
  m_last.SetSize(m_nrow,m_ncol);
  for (int i = 0; i < m_nrow; ++i) for (int j = 0; j < m_ncol; ++j) {
    if (m_mask->At(i,j)==m_srcindex) {
      m_last.At(i,j)[0]=m_src->At(i,j)[0];
      m_last.At(i,j)[1]=m_src->At(i,j)[1];
      m_last.At(i,j)[2]=m_src->At(i,j)[2];
    }
  }
  m_this=m_last;
  CreateGuideVector();
  Iterate();
  //  int count=0;
  while (Iterate() > 0.2) ;
  //write mask & result
  for (int i = 0; i < m_nrow; ++i) for (int j = 0; j < m_ncol; ++j) {
    if (m_mask->At(i,j)==srcindex) {
      m_mask->At(i,j)=dstindex;
      m_dst->At(i,j)[0]=(unsigned char)Clamp<double>(0, m_last.At(i,j)[0], 254.9);
      m_dst->At(i,j)[1]=(unsigned char)Clamp<double>(0, m_last.At(i,j)[1], 254.9);
      m_dst->At(i,j)[2]=(unsigned char)Clamp<double>(0, m_last.At(i,j)[2], 254.9);
    }
  }
}

double PossionBlender::Iterate() {
  double change = 0;
  for (int i = 0; i < m_nrow; ++i) for (int j = 0; j < m_ncol; ++j) {
    if (m_mask->At(i, j) != m_srcindex) continue;
    Vector3d sum(0.0);
    int np = 0;
    if (i != 0 && GetSum(i, j, i - 1, j, sum)) ++np;
    if (i != m_nrow - 1 && GetSum(i, j, i + 1, j, sum)) ++np;
    if (j != 0 && GetSum(i, j, i, j - 1, sum)) ++np;
    if (j != m_ncol - 1 && GetSum(i, j, i, j + 1, sum)) ++np;
    if (sum[0] < 0) sum[0] = 0;
    if (sum[1] < 0) sum[1] = 0;
    if (sum[2] < 0) sum[2] = 0;
    m_this.At(i, j) = sum / (float)np;
    change += Abs(m_this.At(i, j)[0] - m_last.At(i,j)[0]) +
              Abs(m_this.At(i,j)[1] - m_last.At(i,j)[1]) +
              Abs(m_this.At(i,j)[2] - m_last.At(i,j)[2]);
  }
  int n_src = 0;
  for (int i = 0; i < m_nrow; ++i) for (int j = 0; j < m_ncol; ++j) {
    if (m_mask->At(i, j) == m_srcindex) {
      m_last.At(i, j) = m_this.At(i, j);
      ++n_src;
    }
  }
  return change / n_src;
}

bool PossionBlender::GetSum(int i, int j, int i1, int j1, Vector3d &sum) {
  int dir = (i == i1 ? 1 : 0);
  double sign = (double)(dir == 0 ? i - i1 : j - j1);
  if (m_mask->At(i1, j1) == m_srcindex) {
    sum += m_last(i1, j1) + (m_v(i, j)[dir] + m_v(i1, j1)[dir]) / 2.0 * sign;
    return true;
  } else if (m_mask->At(i1, j1) == m_dstindex) {
    sum += Vector3d(m_dst->At(i1, j1)[0], m_dst->At(i1, j1)[1], m_dst->At(i1, j1)[2]) + m_v(i, j)[dir] * sign;
    return true;
  }
  return false;
}

void PossionBlender::CreateGuideVector() {
  m_v.SetSize(m_nrow,m_ncol);

  for (int i = 0; i < m_nrow; ++i) for (int j = 0; j < m_ncol; ++j) {
    if (m_mask->At(i, j) == m_srcindex) {
      if (i != 0 && m_mask->At(i - 1, j) == m_srcindex && i != m_nrow - 1 && m_mask->At(i + 1, j) == m_srcindex)
        m_v(i, j)[0] = (m_last.At(i + 1, j) - m_last.At(i - 1, j)) / 2.0;
      else if (i != 0 && m_mask->At(i - 1, j) == m_srcindex)
        m_v(i, j)[0] = m_last.At(i, j) - m_last.At(i - 1, j);
      else if (i != m_nrow - 1 && m_mask->At(i + 1, j) == m_srcindex)
        m_v(i, j)[0] = m_last.At(i + 1, j) - m_last.At(i, j);
      else
        m_v(i, j)[0] = 0.0;

      if (j != 0 && m_mask->At(i, j-1) == m_srcindex && j != m_ncol - 1 && m_mask->At(i, j + 1) == m_srcindex)
        m_v(i, j)[1] = (m_last.At(i, j + 1) - m_last.At(i, j - 1))/2.0;
      else if (j !=0 && m_mask->At(i, j - 1) == m_srcindex)
        m_v(i, j)[1] = m_last.At(i, j) - m_last.At(i, j - 1);
      else if (j !=m_ncol - 1 && m_mask->At(i, j + 1) == m_srcindex)
        m_v(i, j)[1] = m_last.At(i, j + 1) - m_last.At(i, j);
      else
        m_v(i, j)[1] = 0.0;
    } else if (m_mask->At(i, j) == m_dstindex) {
      bool besidesrc = false;
      if (i != 0 && m_mask->At(i - 1, j) == m_srcindex) besidesrc = true;
      else if (i != m_nrow - 1 && m_mask->At(i + 1, j) == m_srcindex) besidesrc = true;
      else if (j != 0 && m_mask->At(i, j - 1) == m_srcindex) besidesrc = true;
      else if (j != m_ncol - 1 && m_mask->At(i, j + 1) == m_srcindex) besidesrc = true;
      if (!besidesrc) continue;

      if (i != 0 && m_mask->At(i - 1, j) == m_dstindex && i != m_nrow - 1 && m_mask->At(i+1, j) == m_dstindex)
        m_v(i, j)[0] = (m_last.At(i+1, j) - m_last.At(i - 1, j))/2.0;
      else if (i != 0 && m_mask->At(i - 1, j) == m_dstindex)
        m_v(i, j)[0] = m_last.At(i, j) - m_last.At(i - 1, j);
      else if (i != m_nrow - 1 && m_mask->At(i+1, j) == m_dstindex)
        m_v(i, j)[0] = m_last.At(i+1, j) - m_last.At(i, j);
      else
        m_v(i, j)[0]=0.0;

      if (j != 0 && m_mask->At(i, j - 1) == m_dstindex && j != m_ncol - 1 && m_mask->At(i, j+1) == m_dstindex)
        m_v(i, j)[1] = (m_last.At(i, j+1) - m_last.At(i, j - 1))/2.0;
      else if (j != 0 && m_mask->At(i, j - 1) == m_dstindex)
        m_v(i, j)[1] = m_last.At(i, j) - m_last.At(i, j - 1);
      else if (j != m_ncol - 1 && m_mask->At(i, j+1) == m_dstindex)
        m_v(i, j)[1] = m_last.At(i, j+1) - m_last.At(i, j);
      else
        m_v(i, j)[1] = 0.0;
    }
  }
}

void PossionBlender::BlendDirect(Image3uc *const src, int srcindex, Image3uc *dst, int dstindex, Array2i *mask) {
  m_src = src;
  m_mask = mask;
  m_dst = dst;
  m_srcindex = srcindex;
  m_dstindex = dstindex;
  m_nrow = mask->Size(0);
  m_ncol = mask->Size(1);
  m_last.SetSize(m_nrow, m_ncol);
  m_idxmap.clear();
  int n = 0;
  for (int i = 0; i < m_nrow; ++i) for (int j = 0;j < m_ncol; ++j) {
    if (m_mask->At(i,j) == m_srcindex) {
      m_last.At(i, j)[0] = m_src->At(i, j)[0];
      m_last.At(i, j)[1] = m_src->At(i, j)[1];
      m_last.At(i, j)[2] = m_src->At(i, j)[2];
      m_idxmap[Vector2i(i, j)] = n++;
    } else if (m_mask->At(i, j) == m_dstindex) {
      m_last.At(i, j)[0] = m_dst->At(i, j)[0];
      m_last.At(i, j)[1] = m_dst->At(i, j)[1];
      m_last.At(i, j)[2] = m_dst->At(i, j)[2];
    }
  }
  ZLOGV << "number of pixel to be processed: " << n << endl;
  CreateGuideVector();

  noboundary = true;
  //fill A,B
  ccs_data A(n);
  vector<double> B[3];
  B[0].resize(n, 0);
  B[1].resize(n, 0);
  B[2].resize(n, 0);
  vector<double> start[3];
  start[0].resize(n, 0);
  start[1].resize(n, 0);
  start[2].resize(n, 0);
  for (map<Vector2i,int>::iterator mi = m_idxmap.begin(); mi != m_idxmap.end(); ++mi) {
    int idx = mi->second;
    int r = mi->first[0], c = mi->first[1];
    start[0][idx] = m_src->At(r, c)[0];
    start[1][idx] = m_src->At(r, c)[1];
    start[2][idx] = m_src->At(r, c)[2];
    int np = 0;
    if (r > 0 && AddToMatrixDirect(r, c, r - 1, c, A, B)) ++np;
    if (r < m_nrow - 1 && AddToMatrixDirect(r, c, r + 1, c, A, B)) ++np;
    if (c > 0 && AddToMatrixDirect(r, c, r, c - 1, A, B)) ++np;
    if (c < m_ncol - 1 && AddToMatrixDirect(r, c, r, c + 1, A, B)) ++np;
    AddValue(A, idx, idx, np);
    if (np == 0)
      ZLOGE << "isolated point!" << endl;
  }
  for (size_t i = 0; i < A.size(); ++i)
    sort(A[i].begin(), A[i].end());
  if (noboundary) {
    ZLOGE << "NO BOUNDARY CONSTRAINTS, this should not happen!\n";
    for (map<Vector2i, int>::iterator mi = m_idxmap.begin(); mi != m_idxmap.end(); ++mi) {
      int i = mi->first[0], j = mi->first[1];
      m_mask->At(i, j) = dstindex;
    }
    return;
  }

  MustSolve(A, n, n, B, start, 3);
  //write mask & result
  for (map<Vector2i,int>::iterator mi = m_idxmap.begin(); mi != m_idxmap.end(); ++mi) {
    int i = mi->first[0], j = mi->first[1];
    int idx = mi->second;
    m_dst->At(i, j)[0] = (unsigned char)Clamp<double>(0, B[0][idx], 255);
    m_dst->At(i, j)[1] = (unsigned char)Clamp<double>(0, B[1][idx], 255);
    m_dst->At(i, j)[2] = (unsigned char)Clamp<double>(0, B[2][idx], 255);
    m_mask->At(i, j) = dstindex;
  }
}

bool PossionBlender::AddToMatrixDirect(int i, int j, int i1, int j1,ccs_data &A, vector<double> B[3]) {
  int dir = (i == i1 ? 1 : 0);
  double sign = (dir == 0 ? i - i1 : j - j1);
  int idx1 = m_idxmap[Vector2i(i, j)];

  if (m_mask->At(i1, j1) == m_srcindex) {
    int idx2 = m_idxmap[Vector2i(i1, j1)];
    AddValue(A, idx1, idx2, -1);
    B[0][idx1] += (m_v(i, j)[dir][0] + m_v(i1, j1)[dir][0]) / 2.0 * sign;
    B[1][idx1] += (m_v(i, j)[dir][1] + m_v(i1, j1)[dir][1]) / 2.0 * sign;
    B[2][idx1] += (m_v(i, j)[dir][2] + m_v(i1, j1)[dir][2]) / 2.0 * sign;
    return true;
  } else if (m_mask->At(i1, j1) == m_dstindex) {
    noboundary = false;
    B[0][idx1] += double(m_dst->At(i1, j1)[0]) + (m_v(i, j)[dir][0] + m_v(i1, j1)[dir][0]) / 2.0 * sign;
    B[1][idx1] += double(m_dst->At(i1, j1)[1]) + (m_v(i, j)[dir][1] + m_v(i1, j1)[dir][1]) / 2.0 * sign;
    B[2][idx1] += double(m_dst->At(i1, j1)[2]) + (m_v(i, j)[dir][2] + m_v(i1, j1)[dir][2]) / 2.0 * sign;
    return true;
  }
  return false;
}

// void PossionBlender::blendDirect( Image3uc *const src, int srcindex, Image3uc *dst, int dstindex, Array2i *mask )
// {
//   m_src=src;
//   m_mask=mask;
//   m_dst=dst;
//   m_srcindex=srcindex;
//   m_dstindex=dstindex;
//   m_nrow=mask->size(0);
//   m_ncol=mask->size(1);
//   m_last.setSize(m_nrow,m_ncol);
//   m_idxmap.clear();
//   int n=0;
//   for (int i=0;i<m_nrow;i++) for (int j=0;j<m_ncol;j++)
//     if (m_mask->At(i,j)==m_srcindex)
//     {
//       m_last.At(i,j)[0]=m_src->At(i,j)[0];
//       m_last.At(i,j)[1]=m_src->At(i,j)[1];
//       m_last.At(i,j)[2]=m_src->At(i,j)[2];
//       m_idxmap[make_pair(i,j)]=n++;
//     }
//     else if (m_mask->At(i,j)==m_dstindex)
//     {
//       m_last.At(i,j)[0]=m_dst->At(i,j)[0];
//       m_last.At(i,j)[1]=m_dst->At(i,j)[1];
//       m_last.At(i,j)[2]=m_dst->At(i,j)[2];
//     }
//   cout<<"number of pixel to be processed: "<<n<<endl;
//   createGuideVector();
//
//   noboundary=true;
//   //fill A,B
//   SparseSMatd A(n);
//   vector<VarVecd> B(3);
//   B[0].setSize(n);
//   B[1].setSize(n);
//   B[2].setSize(n);
//   B[0].set(0.0);
//   B[1].set(0.0);
//   B[2].set(0.0);
//
//   for (map<pair<int,int>,int>::iterator mi=m_idxmap.begin();mi!=m_idxmap.end();mi++)
//   {
//     int idx=mi->second;
//     int r=mi->first.first,c=mi->first.second;
//     int np=0;
//     if (r>0 && addToMatrixDirect(r,c,r-1,c,A,B)) np++;
//     if (r<m_nrow-1 && addToMatrixDirect(r,c,r+1,c,A,B)) np++;
//     if (c>0 && addToMatrixDirect(r,c,r,c-1,A,B)) np++;
//     if (c<m_ncol-1 && addToMatrixDirect(r,c,r,c+1,A,B)) np++;
//     A.At(idx,idx)=np;
//     if (np==0)
//       cout<<"isolated point!"<<endl;
//   }
//   if (noboundary)
//   {
//     cout<<"NO BOUNDARY CONSTRAINTS, this should not happen!\n";
//     for (map<pair<int,int>,int>::iterator mi=m_idxmap.begin();mi!=m_idxmap.end();mi++)
//     {
//       int i=mi->first.first,j=mi->first.second;
//       m_mask->At(i,j)=dstindex;
//     }
//     return;
//   }
//
//   SparseLinearSystem<double> solver(A);
//   vector<VarVecd> solution;
//   solver.lu(B,solution);
//   //write mask & result
//   for (map<pair<int,int>,int>::iterator mi=m_idxmap.begin();mi!=m_idxmap.end();mi++)
//   {
//     int i=mi->first.first,j=mi->first.second;
//     int idx=mi->second;
//     m_dst->At(i,j)[0]=(unsigned char)puma_clamp<double>(solution[0][idx],0,255);
//     m_dst->At(i,j)[1]=(unsigned char)puma_clamp<double>(solution[1][idx],0,255);
//     m_dst->At(i,j)[2]=(unsigned char)puma_clamp<double>(solution[2][idx],0,255);
//     m_mask->At(i,j)=dstindex;
//   }
// }
//
// bool PossionBlender::addToMatrixDirect( int i, int j, int i1, int j1,SparseSMatd &A, vector<VarVecd> B )
// {
//   int dir=(i==i1?1:0);
//   double sign=(dir==0?i-i1:j-j1);
//   int idx1=m_idxmap[pair<int,int>(i,j)];
//
//   if (m_mask->At(i1,j1)==m_srcindex)
//   {
//     int idx2=m_idxmap[pair<int,int>(i1,j1)];
//     A.At(idx1,idx2)=-1;
//     B[0][idx1]+=(m_v(i,j)[dir][0]+m_v(i1,j1)[dir][0])/2.0*sign;
//     B[1][idx1]+=(m_v(i,j)[dir][1]+m_v(i1,j1)[dir][1])/2.0*sign;
//     B[2][idx1]+=(m_v(i,j)[dir][2]+m_v(i1,j1)[dir][2])/2.0*sign;
//     return true;
//   }
//   else if (m_mask->At(i1,j1)==m_dstindex)
//   {
//     noboundary=false;
//     B[0][idx1]+= double(m_dst->At(i1,j1)[0]) + (m_v(i,j)[dir][0]+m_v(i1,j1)[dir][0])/2.0*sign;
//     B[1][idx1]+= double(m_dst->At(i1,j1)[1]) + (m_v(i,j)[dir][1]+m_v(i1,j1)[dir][1])/2.0*sign;
//     B[2][idx1]+= double(m_dst->At(i1,j1)[2]) + (m_v(i,j)[dir][2]+m_v(i1,j1)[dir][2])/2.0*sign;
//     return true;
//   }
//   return false;
// }
//////////////////////////////////////////////////////////////////////////
void PossionBlender::BlendDirectKeepBoundary(Image3uc *const src, int srcindex, Image3uc *dst, int dstindex, Array2i *mask) {
  m_src = src;
  m_mask = mask;
  m_dst = dst;
  m_srcindex = srcindex;
  m_dstindex = dstindex;
  m_nrow = mask->Size(0);
  m_ncol = mask->Size(1);
  m_last.SetSize(m_nrow, m_ncol);
  m_idxmap.clear();
  int n=0;
  for (int i = 0; i < m_nrow; ++i) for (int j = 0; j < m_ncol; ++j) {
    if (m_mask->At(i, j) == m_srcindex) {
      m_last.At(i, j)[0] = m_src->At(i, j)[0];
      m_last.At(i, j)[1] = m_src->At(i, j)[1];
      m_last.At(i, j)[2] = m_src->At(i, j)[2];
      m_idxmap[Vector2i(i,j)] = n++;
    } else if (m_mask->At(i, j)==m_dstindex) {
      m_last.At(i, j)[0] = m_dst->At(i, j)[0];
      m_last.At(i, j)[1] = m_dst->At(i, j)[1];
      m_last.At(i, j)[2] = m_dst->At(i, j)[2];
    }
  }
  ZLOGV << "number of pixel to be processed: " << n << endl;
  CreateGuideVector();

  noboundary=true;
  //fill A,B
  ccs_data A(n);
  vector<double> B[3];
  B[0].resize(n, 0);
  B[1].resize(n, 0);
  B[2].resize(n, 0);
  vector<double> start[3];
  start[0].resize(n, 0);
  start[1].resize(n, 0);
  start[2].resize(n, 0);
  for (map<Vector2i,int>::iterator mi=m_idxmap.begin();mi!=m_idxmap.end();mi++) {
    int idx = mi->second;
    int r = mi->first[0], c = mi->first[1];
    start[0][idx] = m_src->At(r, c)[0];
    start[1][idx] = m_src->At(r, c)[1];
    start[2][idx] = m_src->At(r, c)[2];
    //fix boundary
    if (r == 0 || c == 0 || r == m_nrow - 1 || c == m_ncol - 1 ||
        m_mask->At(r - 1, c) == -1 || m_mask->At(r + 1, c) == -1 ||
        m_mask->At(r, c - 1) == -1 || m_mask->At(r, c + 1) == -1) {
      AddValue(A, idx, idx, 1);
      B[0][idx] = m_src->At(r, c)[0];
      B[1][idx] = m_src->At(r, c)[1];
      B[2][idx] = m_src->At(r, c)[2];
      continue;
    }
    int np = 0;
    if (r > 0 && AddToMatrixKeepBoundary(r, c, r - 1, c, A, B)) ++np;
    if (r < m_nrow - 1 && AddToMatrixKeepBoundary(r, c, r + 1, c, A, B)) ++np;
    if (c > 0 && AddToMatrixKeepBoundary(r, c, r, c - 1, A, B)) ++np;
    if (c < m_ncol - 1 && AddToMatrixKeepBoundary(r, c, r, c + 1, A, B)) ++np;
    AddValue(A, idx, idx, np);
    if (np==0)
      ZLOGV << "isolated point!" << endl;
  }
  for (size_t i = 0; i < A.size(); ++i)
    sort(A[i].begin(), A[i].end());
  if (noboundary) {
    for (map<Vector2i, int>::iterator mi = m_idxmap.begin(); mi != m_idxmap.end(); ++mi) {
      int i = mi->first[0], j = mi->first[1];
      m_mask->At(i, j) = dstindex;
    }
    return;
  }

  MustSolve(A, n, n, B, start, 3);
  //write mask & result
  for (map<Vector2i, int>::iterator mi = m_idxmap.begin(); mi != m_idxmap.end(); ++mi) {
    int i = mi->first[0], j = mi->first[1];
    int idx = mi->second;
    m_dst->At(i, j)[0] = (unsigned char)Clamp<double>(0, B[0][idx], 255);
    m_dst->At(i, j)[1] = (unsigned char)Clamp<double>(0, B[1][idx], 255);
    m_dst->At(i, j)[2] = (unsigned char)Clamp<double>(0, B[2][idx], 255);
    m_mask->At(i, j) = dstindex;
  }
}

bool PossionBlender::AddToMatrixKeepBoundary(int i, int j, int i1, int j1,ccs_data &A, vector<double> B[3]) {
  int dir = (i == i1 ? 1 : 0);
  double sign = (dir == 0 ? i - i1 : j - j1);
  int idx1 = m_idxmap[Vector2i(i, j)];

  if (m_mask->At(i1, j1) == m_srcindex) {
    int idx2 = m_idxmap[Vector2i(i1, j1)];
    AddValue(A, idx1, idx2, -1);
    B[0][idx1] += (m_v(i, j)[dir][0] + m_v(i1, j1)[dir][0]) / 2.0 * sign;
    B[1][idx1] += (m_v(i, j)[dir][1] + m_v(i1, j1)[dir][1]) / 2.0 * sign;
    B[2][idx1] += (m_v(i, j)[dir][2] + m_v(i1, j1)[dir][2]) / 2.0 * sign;
    return true;
  } else if (m_mask->At(i1, j1) == m_dstindex) {
    //too large difference
    double c1 = double(m_src->At(i, j)[0]) + double(m_src->At(i, j)[1]) + double(m_src->At(i, j)[2]);
    double c2 = double(m_dst->At(i1, j1)[0]) + double(m_dst->At(i1, j1)[1]) + double(m_dst->At(i1, j1)[2]);
    if (Abs(c1 - c2) > 70 * 3) return false;
    noboundary = false;
    B[0][idx1] += double(m_dst->At(i1,j1)[0]) + (m_v(i,j)[dir][0] + m_v(i1, j1)[dir][0]) / 2.0 * sign;
    B[1][idx1] += double(m_dst->At(i1,j1)[1]) + (m_v(i,j)[dir][1] + m_v(i1, j1)[dir][1]) / 2.0 * sign;
    B[2][idx1] += double(m_dst->At(i1,j1)[2]) + (m_v(i,j)[dir][2] + m_v(i1, j1)[dir][2]) / 2.0 * sign;
    return true;
  }
  return false;
}
//////////////////////////////////////////////////////////////////////////
void PossionBlender::BlendAllDirect(Image3uc *img, Array2i *mask) {
  m_src = img;
  m_mask = mask;
  m_nrow = mask->Size(0);
  m_ncol = mask->Size(1);
  m_last.SetSize(m_nrow, m_ncol);
  m_idxmap.clear();
  int n=0;
  for (int i = 0; i < m_nrow; ++i) for (int j = 0; j < m_ncol; ++j) {
    if (m_mask->At(i, j)<0) continue;
    m_last.At(i, j)[0] = img->At(i, j)[0];
    m_last.At(i, j)[1] = img->At(i, j)[1];
    m_last.At(i, j)[2] = img->At(i, j)[2];
    m_idxmap[Vector2i(i,j)] = n++;
  }
  ZLOGV << "number of pixel to be processed: " << n << endl;
  //////////////////////////////////////////////////////////////////////////
  //guide vector
  m_v.SetSize(m_nrow, m_ncol);
  for (int i = 0; i < m_nrow; ++i) for (int j = 0; j < m_ncol; ++j) {
    int imask = m_mask->At(i, j);
    if (imask < 0) continue;

    if (i!=0 && m_mask->At(i-1,j)==imask && i!=m_nrow-1 && m_mask->At(i+1,j)==imask)
      m_v(i,j)[0]=(m_last.At(i+1,j)-m_last.At(i-1,j))/2.0;
    else if (i!=0 && m_mask->At(i-1,j)==imask)
      m_v(i,j)[0]=m_last.At(i,j)-m_last.At(i-1,j);
    else if (i!=m_nrow-1 && m_mask->At(i+1,j)==imask)
      m_v(i,j)[0]=m_last.At(i+1,j)-m_last.At(i,j);
    else
      m_v(i,j)[0]=0.0;

    if (j!=0 && m_mask->At(i,j-1)==imask && j!=m_ncol-1 && m_mask->At(i,j+1)==imask)
      m_v(i,j)[1]=(m_last.At(i,j+1)-m_last.At(i,j-1))/2.0;
    else if (j!=0 && m_mask->At(i,j-1)==imask)
      m_v(i,j)[1]=m_last.At(i,j)-m_last.At(i,j-1);
    else if (j!=m_ncol-1 && m_mask->At(i,j+1)==imask)
      m_v(i,j)[1]=m_last.At(i,j+1)-m_last.At(i,j);
    else
      m_v(i,j)[1]=0.0;
  }

  //////////////////////////////////////////////////////////////////////////
  //fill A,B
  ccs_data A(n);
  vector<double> B[3];
  B[0].resize(n,0);
  B[1].resize(n,0);
  B[2].resize(n,0);
  vector<double> start[3];
  start[0].resize(n,0);
  start[1].resize(n,0);
  start[2].resize(n,0);
  for (int r=0;r<m_nrow;r++) for (int c=0;c<m_ncol;c++) {
    if (m_mask->At(r,c)<0) continue;
    int idx=m_idxmap[Vector2i(r,c)];
    start[0][idx]=m_src->At(r,c)[0];
    start[1][idx]=m_src->At(r,c)[1];
    start[2][idx]=m_src->At(r,c)[2];

    //fix boundary
    if (r==0 || r==m_nrow-1 || c==0 || c==m_ncol-1 \
      || mask->At(r-1,c)==-1 || mask->At(r+1,c)==-1 || mask->At(r,c)==-1 || mask->At(r,c+1)==-1)
    {
      AddValue(A,idx,idx,1);
      B[0][idx]=img->At(r,c)[0];
      B[1][idx]=img->At(r,c)[1];
      B[2][idx]=img->At(r,c)[2];
      continue;
    }
    int np=0;
    if (r>0 && AddToMatrixAllDirect(r,c,r-1,c,A,B)) np++;
    if (r<m_nrow-1 && AddToMatrixAllDirect(r,c,r+1,c,A,B)) np++;
    if (c>0 && AddToMatrixAllDirect(r,c,r,c-1,A,B)) np++;
    if (c<m_ncol-1 && AddToMatrixAllDirect(r,c,r,c+1,A,B)) np++;
    if (np==0) {
      AddValue(A,idx,idx,1);
      B[0][idx]=img->At(r,c)[0];
      B[1][idx]=img->At(r,c)[1];
      B[2][idx]=img->At(r,c)[2];
    }
    else
      AddValue(A,idx,idx,np);
  }

  cout<<"solving...";
  MustSolve(A,n,n,B,start,3);
  cout<<"done!\n";
  //write mask & result
  for (map<Vector2i,int>::iterator mi=m_idxmap.begin();mi!=m_idxmap.end();mi++) {
    int i = mi->first[0], j = mi->first[1];
    int idx = mi->second;
    img->At(i,j)[0]=(unsigned char)Clamp<double>(0, B[0][idx], 255);
    img->At(i,j)[1]=(unsigned char)Clamp<double>(0, B[1][idx], 255);
    img->At(i,j)[2]=(unsigned char)Clamp<double>(0, B[2][idx], 255);
    mask->At(i,j)=1;
  }
}
bool PossionBlender::AddToMatrixAllDirect( int i, int j, int i1, int j1,ccs_data &A, vector<double> B[3] ) {
  //   double c1=double(m_src->At(i,j)[0])+double(m_src->At(i,j)[1])+double(m_src->At(i,j)[2]);
  //   double c2=double(m_src->At(i1,j1)[0])+double(m_src->At(i1,j1)[1])+double(m_src->At(i1,j1)[2]);
  //   if (abs(c1-c2)>70*3) return false;
  int dir=(i==i1?1:0);
  double sign=(dir==0?i-i1:j-j1);
  int idx1=m_idxmap[Vector2i(i,j)];
  int idx2=m_idxmap[Vector2i(i1,j1)];
  AddValue(A,idx1,idx2,-1);
  B[0][idx1]+=(m_v(i,j)[dir][0]+m_v(i1,j1)[dir][0])/2.0*sign;
  B[1][idx1]+=(m_v(i,j)[dir][1]+m_v(i1,j1)[dir][1])/2.0*sign;
  B[2][idx1]+=(m_v(i,j)[dir][2]+m_v(i1,j1)[dir][2])/2.0*sign;
  return true;
}

//////////////////////////////////////////////////////////////////////////
void PossionBlender::BlendImageSequential(Image3uc *img, Array2i *mask) {
  Image3uc &image=*img;
  cout<<"texture size: "<<image.Size(1)<<'x'<<image.Size(0)<<endl;
  Array2i cluster=*mask;
  //make cluster
  for (int r=0;r<cluster.Size(0);r++) for (int c=0;c<cluster.Size(1);c++)
    if (cluster.At(r,c)!=-1) cluster.At(r,c)=-2;  //mark the non-empty region
  int cluster_idx=0;
  for (int r=0;r<cluster.Size(0);r++) for (int c=0;c<cluster.Size(1);c++)
    if (cluster.At(r,c)==-2) ClusterHelper(cluster,Vector2i(r,c),cluster_idx++);
  cout<<"subregion number:"<<cluster_idx<<endl;
  //for each cluster, blend them
  Array2i thismask(cluster.Size(0),cluster.Size(1));
  for (int i=0;i<cluster_idx;i++) {
    //prepare mask for this cluster
    for (int r=0;r<cluster.Size(0);r++) for (int c=0;c<cluster.Size(1);c++) {
      if (cluster.At(r,c)==i)  thismask.At(r,c)=mask->At(r,c);
      else thismask.At(r,c)=-1;
    }
    //cluster again, prevent color apart, (0,1,2,,,)->(-2,-3,-4,,,)
    int cluster_idx2=-2;
    for (int r=0;r<thismask.Size(0);r++) for (int c=0;c<thismask.Size(1);c++)
      if (thismask.At(r,c)>-1) ClusterHelper2(thismask,Vector2i(r,c),cluster_idx2--);
    cluster_idx2=-cluster_idx2-2;
    cout<<"color block number:"<<cluster_idx2<<endl;
    //find start point
    Array2uc checked(image.Size(0),image.Size(1));
    checked.Fill((unsigned char)0);
    int center_index=-1;
    std::deque<Vector2i> checking;
    for (int r=0;r<cluster.Size(0);r++) for (int c=0;c<cluster.Size(1);c++) {
      if (thismask.At(r,c)!=-1) {
        thismask.At(r,c)=-thismask.At(r,c)-2; //from negative back to normal mask
        if (center_index==-1) {
          center_index=thismask.At(r,c);
          checking.push_back(Vector2i(r,c));
          checked.At(r,c)=1;
        }
      }
    }
    while(!checking.empty()) {
      Vector2i now=checking.front();
      if (thismask(now[0] ,now[1])!=center_index) {
        cout<<"Blending: "<<thismask(now[0],now[1])<<"->"<<center_index<<endl;
        BlendDirectKeepBoundary(&image,thismask(now[0],now[1]),&image,center_index,&thismask);
      }
      if (now[0]>0 && checked(now[0]-1,now[1])==0 && thismask(now[0]-1,now[1])!=-1) {
        checked(now[0]-1,now[1])=1;
        checking.push_back(Vector2i(now[0]-1,now[1]));
      }
      if (now[0]<image.Size(0)-1 && checked(now[0]+1,now[1])==0 && thismask(now[0]+1,now[1])!=-1) {
        checked(now[0]+1,now[1])=1;
        checking.push_back(Vector2i(now[0]+1,now[1]));
      }
      if (now[1]>0 && checked(now[0],now[1]-1)==0 && thismask(now[0],now[1]-1)!=-1) {
        checked(now[0],now[1]-1)=1;
        checking.push_back(Vector2i(now[0],now[1]-1));
      }
      if (now[1]<image.Size(1)-1 && checked(now[0],now[1]+1)==0 && thismask(now[0],now[1]+1)!=-1) {
        checked(now[0],now[1]+1)=1;
        checking.push_back(Vector2i(now[0],now[1]+1));
      }
      checking.pop_front();
    }
  }
}

void PossionBlender::ClusterHelper(Array2i &cluster, const Vector2i &seed, int value) {
  Array2uc checked(cluster.Size());
  checked.Fill((unsigned char)0);
  deque<Vector2i> checking;
  checking.push_back(seed);
  checked.At(seed[0], seed[1]) = 1;
  while(!checking.empty()) {
    Vector2i cur=checking.front();
    cluster.At(cur[0], cur[1])=value;
    if (cur[0] > 0 && checked.At(cur[0] - 1,cur[1]) == 0 && cluster.At(cur[0] - 1, cur[1]) != -1) {
      checking.push_back(Vector2i(cur[0] - 1, cur[1]));
      checked.At(cur[0] - 1, cur[1]) = 1;
    }
    if (cur[0] < cluster.Size(0)-1 && checked.At(cur[0] + 1,cur[1])==0 && cluster.At(cur[0]+1,cur[1])!=-1) {
      checking.push_back(Vector2i(cur[0]+1,cur[1]));
      checked.At(cur[0]+1,cur[1])=1;
    }
    if (cur[1]>0 && checked.At(cur[0],cur[1]-1)==0 && cluster.At(cur[0],cur[1]-1)!=-1) {
      checking.push_back(Vector2i(cur[0],cur[1]-1));
      checked.At(cur[0],cur[1]-1)=1;
    }
    if (cur[1]<cluster.Size(1)-1 && checked.At(cur[0],cur[1]+1)==0 && cluster.At(cur[0],cur[1]+1)!=-1) {
      checking.push_back(Vector2i(cur[0],cur[1]+1));
      checked.At(cur[0],cur[1]+1)=1;
    }
    checking.pop_front();
  }
}

void PossionBlender::ClusterHelper2(Array2i &cluster, const Vector2i &seed, int value) {
  int orivalue=cluster.At(seed[0],seed[1]);
  Array2uc checked(cluster.Size());
  checked.Fill((unsigned char)0);
  deque<Vector2i> checking;
  checking.push_back(seed);
  checked.At(seed[0],seed[1])=1;
  while(!checking.empty()) {
    Vector2i cur=checking.front();
    cluster.At(cur[0],cur[1])=value;
    if (cur[0]>0 && checked.At(cur[0]-1,cur[1])==0 && cluster.At(cur[0]-1,cur[1])==orivalue) {
      checking.push_back(Vector2i(cur[0]-1,cur[1]));
      checked.At(cur[0]-1,cur[1])=1;
    }
    if (cur[0]<cluster.Size(0)-1 && checked.At(cur[0]+1,cur[1])==0 && cluster.At(cur[0]+1,cur[1])==orivalue) {
      checking.push_back(Vector2i(cur[0]+1,cur[1]));
      checked.At(cur[0]+1,cur[1])=1;
    }
    if (cur[1]>0 && checked.At(cur[0],cur[1]-1)==0 && cluster.At(cur[0],cur[1]-1)==orivalue) {
      checking.push_back(Vector2i(cur[0],cur[1]-1));
      checked.At(cur[0],cur[1]-1)=1;
    }
    if (cur[1]<cluster.Size(1)-1 && checked.At(cur[0],cur[1]+1)==0 && cluster.At(cur[0],cur[1]+1)==orivalue) {
      checking.push_back(Vector2i(cur[0],cur[1]+1));
      checked.At(cur[0],cur[1]+1)=1;
    }
    checking.pop_front();
  }
}

//////////////////////////////////////////////////////////////////////////
void PossionBlender::PrepareGuidedVector(Image3uc *img, Array2i *mask) {
  m_nrow=img->Size(0);
  m_ncol=img->Size(1);
  m_mask=mask;
  m_last.SetSize(m_nrow,m_ncol);
  for (int r=0;r<m_nrow;r++) for (int c=0;c<m_ncol;c++)
    m_last.At(r,c)=img->At(r,c);
  m_v.SetSize(m_nrow,m_ncol);
  for (int r=0;r<m_nrow;r++) for (int c=0;c<m_ncol;c++) {
    int idx=mask->At(r,c);
    if (idx==-1) continue;

    if (r!=0 && m_mask->At(r-1,c)==idx && r!=m_nrow-1 && m_mask->At(r+1,c)==idx)
      m_v(r,c)[0]=(m_last.At(r+1,c)-m_last.At(r-1,c))/2.0;
    else if (r!=0 && m_mask->At(r-1,c)==idx)
      m_v(r,c)[0]=m_last.At(r,c)-m_last.At(r-1,c);
    else if (r!=m_nrow-1 && m_mask->At(r+1,c)==idx)
      m_v(r,c)[0]=m_last.At(r+1,c)-m_last.At(r,c);
    else
      m_v(r,c)[0]=0.0;

    if (c!=0 && m_mask->At(r,c-1)==idx && c!=m_ncol-1 && m_mask->At(r,c+1)==idx)
      m_v(r,c)[1]=(m_last.At(r,c+1)-m_last.At(r,c-1))/2.0;
    else if (c!=0 && m_mask->At(r,c-1)==idx)
      m_v(r,c)[1]=m_last.At(r,c)-m_last.At(r,c-1);
    else if (c!=m_ncol-1 && m_mask->At(r,c+1)==idx)
      m_v(r,c)[1]=m_last.At(r,c+1)-m_last.At(r,c);
    else
      m_v(r,c)[1]=0.0;
  }
}

double PossionBlender::BlendSinglePatch(Image3uc *img, Array2i *mask, int srcindex) {
  double change = 0;
  m_srcindex = srcindex;
  m_mask = mask;
  m_src = img;
  m_idxmap.clear();
  int n = 0;
  for (int i = 0; i < m_nrow; ++i) for (int j = 0; j < m_ncol; ++j) {
    if (m_mask->At(i,j)!=m_srcindex) continue;
    m_idxmap[Vector2i(i,j)] = n++;
  }
  ZLOGV << "number of pixel to be processed: "<< n << endl;

  noboundary=true;
  //fill A,B
  ccs_data A(n);
  vector<double> B[3];
  B[0].resize(n,0);
  B[1].resize(n,0);
  B[2].resize(n,0);
  vector<double> start[3];
  start[0].resize(n,0);
  start[1].resize(n,0);
  start[2].resize(n,0);
  for (map<Vector2i,int>::iterator mi=m_idxmap.begin();mi!=m_idxmap.end();mi++) {
    int idx=mi->second;
    int r=mi->first[0],c=mi->first[1];
    start[0][idx]=m_src->At(r,c)[0];
    start[1][idx]=m_src->At(r,c)[1];
    start[2][idx]=m_src->At(r,c)[2];

    //fix boundary
    if (r==0 || c==0 || r==m_nrow-1 || c==m_ncol-1 ||
        m_mask->At(r-1,c)==-1 || m_mask->At(r+1,c)==-1 ||
        m_mask->At(r,c-1)==-1 || m_mask->At(r,c+1)==-1) {
      AddValue(A,idx,idx,1);
      B[0][idx]=img->At(r,c)[0];
      B[1][idx]=img->At(r,c)[1];
      B[2][idx]=img->At(r,c)[2];
      continue;
    }
    int np=0;
    if (r>0 && AddToMatrixSinglePatch(r,c,r-1,c,A,B)) np++;
    if (r<m_nrow-1 && AddToMatrixSinglePatch(r,c,r+1,c,A,B)) np++;
    if (c>0 && AddToMatrixSinglePatch(r,c,r,c-1,A,B)) np++;
    if (c<m_ncol-1 && AddToMatrixSinglePatch(r,c,r,c+1,A,B)) np++;
    AddValue(A,idx,idx,np);
    if (np==0)
      ZLOGE << "isolated point!"<<endl;
  }
  if (noboundary)
    return -1;
  for (size_t i=0;i<A.size();i++)
    sort(A[i].begin(),A[i].end());

  MustSolve(A,n,n,B,start,3);
  //write mask & result
  for (map<Vector2i,int>::iterator mi=m_idxmap.begin();mi!=m_idxmap.end();mi++) {
    int r=mi->first[0],c=mi->first[1];
    int idx=mi->second;
    Vector3d newpixel=Vector3d(Clamp<double>(0, B[0][idx], 255),\
                               Clamp<double>(0, B[1][idx], 255),\
                               Clamp<double>(0, B[2][idx], 255));
    change+=abs(newpixel[0]-img->At(r,c)[0]);
    change+=abs(newpixel[1]-img->At(r,c)[1]);
    change+=abs(newpixel[2]-img->At(r,c)[2]);
    img->At(r,c)=newpixel;
  }
  return change/n/3.0;
}

bool PossionBlender::AddToMatrixSinglePatch(int i, int j, int i1, int j1, ccs_data &A, vector<double> B[3]) {
  int dir=(i==i1?1:0);
  double sign=(dir==0?i-i1:j-j1);
  int idx1=m_idxmap[Vector2i(i,j)];

  if (m_mask->At(i1,j1)==m_srcindex) {
    int idx2=m_idxmap[Vector2i(i1,j1)];
    AddValue(A,idx1,idx2,-1);
    B[0][idx1]+=(m_v(i,j)[dir][0]+m_v(i1,j1)[dir][0])/2.0*sign;
    B[1][idx1]+=(m_v(i,j)[dir][1]+m_v(i1,j1)[dir][1])/2.0*sign;
    B[2][idx1]+=(m_v(i,j)[dir][2]+m_v(i1,j1)[dir][2])/2.0*sign;
    return true;
  } else if (m_mask->At(i1,j1)!=-1) {
    noboundary=false;
    B[0][idx1]+= double(m_src->At(i1,j1)[0]) + (m_v(i,j)[dir][0]+m_v(i1,j1)[dir][0])/2.0*sign;
    B[1][idx1]+= double(m_src->At(i1,j1)[1]) + (m_v(i,j)[dir][1]+m_v(i1,j1)[dir][1])/2.0*sign;
    B[2][idx1]+= double(m_src->At(i1,j1)[2]) + (m_v(i,j)[dir][2]+m_v(i1,j1)[dir][2])/2.0*sign;
    return true;
  }
  return false;
}

void PossionBlender::BlendItrativeGlobal(Image3uc *img, Array2i *mask) {
  Image3uc &image=*img;
  cout<<"texture size: "<<image.Size(1)<<'x'<<image.Size(0)<<endl;
  Array2i cluster=*mask;
  //make cluster
  for (int r=0;r<cluster.Size(0);r++) for (int c=0;c<cluster.Size(1);c++)
    if (cluster.At(r,c)!=-1) cluster.At(r,c)=-2;  //mark the non-empty region
  int cluster_idx=0;
  for (int r=0;r<cluster.Size(0);r++) for (int c=0;c<cluster.Size(1);c++)
    if (cluster.At(r,c)==-2) ClusterHelper(cluster,Vector2i(r,c),cluster_idx++);
  cout<<"subregion number:"<<cluster_idx<<endl;
  //for each cluster, blend them
  Array2i thismask(cluster.Size(0),cluster.Size(1));
  for (int i=0;i<cluster_idx;i++) {
    //prepare mask for this cluster
    for (int r=0;r<cluster.Size(0);r++) for (int c=0;c<cluster.Size(1);c++) {
      if (cluster.At(r,c)==i)  thismask.At(r,c)=mask->At(r,c);
      else thismask.At(r,c)=-1;
    }
    //cluster again, prevent color apart, (0,1,2,,,)->(-2,-3,-4,,,)
    int cluster_idx2=-2;
    for (int r=0;r<thismask.Size(0);r++) for (int c=0;c<thismask.Size(1);c++)
      if (thismask.At(r,c)>-1) ClusterHelper2(thismask,Vector2i(r,c),cluster_idx2--);
    cluster_idx2=-cluster_idx2-2;
    cout<<"color block number:"<<cluster_idx2<<endl;
    for (int r=0;r<cluster.Size(0);r++) for (int c=0;c<cluster.Size(1);c++)
      if (thismask.At(r,c)!=-1) thismask.At(r,c)=-thismask.At(r,c)-2; //from negative back to normal mask
    PrepareGuidedVector(img,&thismask);
    int count=0;
    deque<double> changes(Max(cluster_idx2/10,1),0);
    double allchange=0;
    RandomInteger<int> rand(0, cluster_idx2 - 1);
    while(true) {
      int x = rand.Rand();
      double change = BlendSinglePatch(img, &thismask, x);
      if (change!=-1) {
        allchange+=change;
        allchange-=changes.front();
        changes.pop_front();
        changes.push_back(change);
        cout<<"allchanges:"<<allchange<<"=>"<<changes.size()/2.0<<endl;
      }
      count++;
      if (count>cluster_idx2*2 && allchange*2<changes.size()) break;
    }
  }
}
}
#endif // SUPERLU
