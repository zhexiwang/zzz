#pragma once
#include <zCore/common.hpp>
#include <z3rd/Wrapper/SuperLUWrapper.hpp>
#include <zCore/Math/Array2.hpp>
#include "../Image/Image.hpp"

namespace zzz{
class PossionBlender {
public:
  //clustering, find sequence to blend
  void BlendImageSequential(Image3uc *img, Array2i *mask);
  void ClusterHelper(Array2i &cluster, const Vector2i &seed, int value);
  void ClusterHelper2(Array2i &cluster, const Vector2i &seed, int value);

  //create guide vector from m_last, fill m_v, only where m_mask==m_src or m_mask==m_dst beside m_src
  void CreateGuideVector();
  //blendIterative src to dst, write the new color into dst
  //src and dst must be at the same size
  //it will also merge the srcmask to dstmask
  void BlendIterative(Image3uc *const src, int srcindex, Image3uc *dst, int dstindex, Array2i *mask); //iterative solve
  double Iterate();
  bool GetSum(int i, int j, int i1, int j1, Vector3d &sum);

#ifdef ZZZ_LIB_SUPERLU
  //blend direct by using superLU
  void BlendDirect(Image3uc *const src, int srcindex, Image3uc *dst, int dstindex, Array2i *mask); //by superLU direct solve
  bool AddToMatrixDirect(int i, int j, int i1, int j1,ccs_data &A, vector<double> B[3]);

  //////////////////////////////////////////////////////////////////////////
  // solving using SparseSMat
  //   void blendDirect(Image3uc *const src, int srcindex, Image3uc *dst, int dstindex, Array2i *mask);
  //   bool addToMatrixDirect( int i, int j, int i1, int j1,SparseSMatd &A, vector<VarVecd> B );

  //blend direct by using SuperLU while keeping boundary of the image and keep too big difference on boundary
  void BlendDirectKeepBoundary(Image3uc *const src, int srcindex, Image3uc *dst, int dstindex, Array2i *mask);
  bool AddToMatrixKeepBoundary(int i, int j, int i1, int j1,ccs_data &A, vector<double> B[3]);

  //blend the whole image, all pixels are considered
  void BlendAllDirect(Image3uc *img, Array2i *mask);
  bool AddToMatrixAllDirect(int i, int j, int i1, int j1,ccs_data &A, vector<double> B[3]);

  //blend this patch with all others as boundary, do not change the mask value
  //will return the amount of average changing
  void PrepareGuidedVector(Image3uc *img, Array2i *mask);
  double BlendSinglePatch(Image3uc *img, Array2i *mask, int srcindex);
  bool AddToMatrixSinglePatch(int i, int j, int i1, int j1,ccs_data &A, vector<double> B[3]);
#endif

  //iteratively blend each patch with all others, to get a global optimization
  void BlendItrativeGlobal(Image3uc *img, Array2i *mask);

  //variables that used to communicate between functions
  const Image3uc *m_src;
  Image3uc *m_dst;
  Array2i *m_mask;
  int m_srcindex,m_dstindex;
  Array<2, Vector3d> m_last;
  Array<2, Vector3d> m_this;
  Array<2, Vector<2, Vector3d> > m_v;
  int m_nrow,m_ncol;
  //for direct solver
  map<Vector2i, int> m_idxmap;
};
}
