#pragma once

// Separate link so when change a lib, only link needs redo.
#include "zImageConfig.hpp"
#include "zImageAutoLink3rdParty.hpp"

#ifndef ZZZ_NO_PRAGMA_LIB

#ifdef ZZZ_COMPILER_MSVC

#ifdef ZZZ_DYNAMIC
  #ifdef ZZZ_DEBUG
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zImageD.lib")
    #else
      #pragma comment(lib,"zImageD_x64.lib")
    #endif // ZZZ_OS_WIN64
  #else
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zImage.lib")
    #else
      #pragma comment(lib,"zImage_x64.lib")
    #endif // ZZZ_OS_WIN64
  #endif
#else
  #ifdef ZZZ_DEBUG
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zImageD.lib")
    #else
      #pragma comment(lib,"zImageD_x64.lib")
    #endif // ZZZ_OS_WIN64
  #else
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zImage.lib")
    #else
      #pragma comment(lib,"zImage_x64.lib")
    #endif // ZZZ_OS_WIN64
  #endif
#endif

#endif // ZZZ_COMPILER_MSVC

#endif // ZZZ_NO_PRAGMA_LIB
