#pragma once

namespace zzz{
//copy from gl.h
#define ZZZ_BYTE                           0x1400
#define ZZZ_UNSIGNED_BYTE                  0x1401
#define ZZZ_SHORT                          0x1402
#define ZZZ_UNSIGNED_SHORT                 0x1403
#define ZZZ_INT                            0x1404
#define ZZZ_UNSIGNED_INT                   0x1405
#define ZZZ_FLOAT                          0x1406
#define ZZZ_DOUBLE                         0x140A

#define ZZZ_RGB                            0x1907
#define ZZZ_RGBA                           0x1908
#define ZZZ_LUMINANCE                      0x1909
}
