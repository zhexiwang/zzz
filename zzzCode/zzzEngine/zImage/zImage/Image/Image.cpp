#include "Image.hpp"
namespace zzz{
#define IMAGE_SPECIALIZE(T,format,type,channels) \
template<>\
const int Image<T>::Channels_=channels; \
template<>\
const int Image<T>::Format_=format; \
template<>\
const int Image<T>::Type_=type;
IMAGE_SPECIALIZE(Vector4uc,ZZZ_RGBA, ZZZ_UNSIGNED_BYTE, 4);
IMAGE_SPECIALIZE(Vector4f,ZZZ_RGBA, ZZZ_FLOAT, 4);
IMAGE_SPECIALIZE(Vector3uc,ZZZ_RGB, ZZZ_UNSIGNED_BYTE, 3);
IMAGE_SPECIALIZE(Vector3f,ZZZ_RGB, ZZZ_FLOAT, 3);
IMAGE_SPECIALIZE(zuchar,ZZZ_LUMINANCE, ZZZ_UNSIGNED_BYTE, 1);
IMAGE_SPECIALIZE(float,ZZZ_LUMINANCE, ZZZ_FLOAT, 1);

}
