#pragma once
#include <zCore/Math/Vector2.hpp>

namespace zzz{
//R=row, C=column
//R from bottom to top, C from left to right
//_R from top to bottom, _C from right ot left
//X from left to right, Y from bottom to top
//_X from right to left, _Y from top to bottom
//X=C, Y=R
//image coordinate used in Image is R,C
  typedef Vector2f CoordRCf;  //original
  typedef Vector2f CoordXYf;  //xy
  typedef Vector2f CoordX_Yf;  //x,-y
  typedef Vector2f Coord_XYf;  //-x,y

  typedef Vector2f32 CoordRCf32;  //original
  typedef Vector2f32 CoordXYf32;  //xy
  typedef Vector2f32 CoordX_Yf32;  //x,-y
  typedef Vector2f32 Coord_XYf32;  //-x,y
}