#pragma once
#include "Image.hpp"
#include <zMatrix/zMat.hpp>

//For simple image process
//scale, flip, offset, etc
//TODO: flip...
namespace zzz{
template<typename T>
class ImageProcessor {
public:
  static void ScaleToShow(Image<T> &image);
  static void FlipH(Image<T> &image) { //flip horizontally
    Array<2, T> ori(image);
    Dress(image)=Dress(ori)(Colon(), Colon(image.Cols() - 1, 0, -1));
  }
  static void FlipV(Image<T> &image) { //flip vertically
    Array<2,T> ori(image);
    Dress(image)=Dress(ori)(Colon(image.Rows() - 1, 0, -1),Colon());
  }
};

}
