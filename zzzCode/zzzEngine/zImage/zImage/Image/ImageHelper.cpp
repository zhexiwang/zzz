#include "ImageHelper.hpp"

namespace zzz{
//Min and Max
template<>
const float PixelHelper<float>::Min=0;
template<>
const float PixelHelper<float>::Max=1;
template<>
const Vector3f PixelHelper<Vector3f>::Min=Vector3f(0,0,0);
template<>
const Vector3f PixelHelper<Vector3f>::Max=Vector3f(1,1,1);
template<>
const Vector4f PixelHelper<Vector4f>::Min=Vector4f(0,0,0,0);
template<>
const Vector4f PixelHelper<Vector4f>::Max=Vector4f(1,1,1,1);
template<>
const zuchar PixelHelper<zuchar>::Min=0;
template<>
const zuchar PixelHelper<zuchar>::Max=255;
template<>
const Vector3uc PixelHelper<Vector3uc>::Min=Vector3uc(0,0,0);
template<>
const Vector3uc PixelHelper<Vector3uc>::Max=Vector3uc(255,255,255);
template<>
const Vector4uc PixelHelper<Vector4uc>::Min=Vector4uc(0,0,0,0);
template<>
const Vector4uc PixelHelper<Vector4uc>::Max=Vector4uc(255,255,255,255);


}
