#pragma once
#include "../Utility/CacheSet.hpp"
#include "Image.hpp"

namespace zzz{
class ImageDirCacheSet : public CacheSet<Image3uc>
{
public:
  void SetFiles(const vector<string> &files, int cache_size)
  {
    files_=files;
    SetSize(files_.size(),cache_size);
  }
private:
  Image3uc* Load(zuint i)
  {
    Image3uc *img=new Image3uc(files_[i].c_str());
    return img;
  }
  vector<string> files_;
};
}