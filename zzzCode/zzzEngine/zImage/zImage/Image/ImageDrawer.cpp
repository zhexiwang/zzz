#include "ImageDrawer.hpp"

namespace zzz{
void RasterizeLine(int r0, int c0, int r1, int c1, vector<Vector2i> &res)
{
  res.clear();
  double dr=r1-r0,dc=c1-c0;
  if (dr==0 && dc==0)
  {
    res.push_back(Vector2i(r0,c0));
    return;
  }
  if (abs(dr)>abs(dc))
  {
    double slope=dc/dr;
    if (r1>r0)
      for (int r=r0; r<=r1; r++)
      {
        int c=Round((r-r0)*slope+c0);
        res.push_back(Vector2i(r,c));
      }
    else
      for (int r=r1; r<=r0; r++)
      {
        int c=Round((r-r0)*slope+c0);
        res.push_back(Vector2i(r,c));
      }
  }
  else
  {
    double slope=dr/dc;
    if (c1>c0)
      for (int c=c0; c<=c1; c++)
      {
        int r=Round((c-c0)*slope+r0);
        res.push_back(Vector2i(r,c));
      }
    else
      for (int c=c1; c<=c0; c++)
      {
        int r=Round((c-c0)*slope+r0);
        res.push_back(Vector2i(r,c));
      }
  }
  return;
}
}
