#include "ImageProcessor.hpp"
#include <zCore/Math/Range.hpp>

namespace zzz {
template<>
void ImageProcessor<float>::ScaleToShow(Image<float> &image) {
  float minx=image.Min(),maxx=image.Max();
  float diff = maxx-minx;
  float scale = 1.0f / diff;
  for (zuint i=0; i<image.size(); i++) {
    image[i]-=minx;
    image[i]*=scale;
  }
}

template<>
void ImageProcessor<Vector3f>::ScaleToShow(Image<Vector3f> &image) {
  Range<3,float> aabb;
  aabb.AddData(image.begin(), image.end());
  Vector3f diff=aabb.Diff();
  Vector3f scale=Vector3f(1.0f)/diff;
  for (zuint i=0; i<image.size(); i++) {
    image[i]-=aabb.Min();
    image[i]*=scale;
  }
}

};  // namespace zzz
