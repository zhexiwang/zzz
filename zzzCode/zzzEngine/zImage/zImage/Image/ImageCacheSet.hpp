#pragma once
#include "../Utility/CacheSet.hpp"
#include "Image.hpp"

namespace zzz{
template<typename T>
class ImageCacheSet : public CacheSet<Image<T> >
{
public:
  void SetData(const string &pattern, int start, int end, int cache_size)
  {
    pattern_=pattern;
    start_=start;
    end_=end;
    SetSize(end-start+1,cache_size);
  }
private:
  Image<T>* Load(zuint i)
  {
    char filename[1024];
    sprintf(filename,pattern_.c_str(),start_+i);
    Image<T> *img=new Image<T>(filename);
    return img;
  }
  string pattern_;
  int start_,end_;
};
}