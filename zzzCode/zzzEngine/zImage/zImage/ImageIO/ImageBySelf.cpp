#include <zImage/zImageConfig.hpp>

#ifdef ZZZ_IMAGE_BY_SELF
#include <zCore/Utility/StringTools.hpp>
#include <zCore/Utility/Log.hpp>
#include "../Image/Image.hpp"

namespace zzz{
#define UNIMPLEMENT(T) \
template<>\
bool Image<T>::LoadFile(const string &filename) {\
  ZLOGF<<"Load image from "<<filename<<" is not implemented!\n"; \
  return false; \
}\
template<>\
bool Image<T>::SaveFile(const string &filename) const {\
  ZLOGF<<"Save image to "<<filename<<" is not implemented!\n"; \
  return false; \
}
  
UNIMPLEMENT(Vector4uc);
UNIMPLEMENT(Vector4f);
UNIMPLEMENT(Vector3uc);
UNIMPLEMENT(Vector3f);
UNIMPLEMENT(zuchar);
UNIMPLEMENT(float);

#undef UNIMPLEMENT
}
#endif // ZZZ_IMAGE_BY_SELF
