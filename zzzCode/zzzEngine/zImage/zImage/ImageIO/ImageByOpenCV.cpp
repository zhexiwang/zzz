#pragma once
#include <zImage/zImageConfig.hpp>
#if defined(ZZZ_IMAGE_IO_OPENCV) && defined(ZZZ_LIB_OPENCV)
#include <zImage/Image/Image.hpp>
#include "CVMatConverter.hpp"

namespace zzz {
#define OPENCV_LOAD(T) \
template<> \
bool Image<T>::LoadFile(const string &filename) { \
  ZLOGV << "Loading image: " << filename << endl; \
  cv::Mat mat = cv::imread(filename, CV_LOAD_IMAGE_COLOR); \
  if (!mat.data) {ZLOGE << "Load image failed: " << filename << endl; return false;} \
  cv::Mat mat2(1, 1, CVType<T>::TYPE); \
  CVMatToImage(mat, *this); \
  return true; \
}

OPENCV_LOAD(Vector3f);
OPENCV_LOAD(Vector3uc);
OPENCV_LOAD(Vector4f);
OPENCV_LOAD(Vector4uc);

#undef OPENCV_LOAD

#define OPENCV_SAVE(T) \
template<> \
bool Image<T>::SaveFile(const string &filename) const { \
  ZLOGV << "Saving image: " << filename << endl; \
  cv::Mat mat, mat2; \
  ImageToCVMat(*this, mat); \
  SmartConvertToBGR(mat, mat2); \
  bool res = cv::imwrite(filename, mat2); \
  if (!res) {ZLOGE << "Save image failed: " << filename << endl; return false;} \
  return true; \
}
OPENCV_SAVE(Vector3f);
OPENCV_SAVE(Vector3uc);
OPENCV_SAVE(Vector4f);
OPENCV_SAVE(Vector4uc);

#undef OPENCV_SAVE
} // namespace zzz
#endif // ZZZ_LIB_OPENCV