#pragma once
#include <z3rd/Wrapper/OpenCVTools.hpp>

#ifdef ZZZ_LIB_OPENCV
#include <opencv2/opencv.hpp>
#include <zImage/Image/Image.hpp>

namespace zzz {
void SmartConvertToRGB(const cv::Mat &src, cv::Mat &dst);
void SmartConvertToBGR(const cv::Mat &src, cv::Mat &dst);

template<typename T>
void CVMatToImage(const cv::Mat &src, Array<2, T> &dst) {
  if (CVType<T>::TYPE != src.type()) {
    cv::Mat tmp(1, 1, CVType<T>::TYPE);
    SmartConvertToRGB(src, tmp);
    CVMatToArray(tmp, dst);
    return;
  } else {
    dst.SetSize(src.rows, src.cols);
    dst.SetData((T*)src.ptr());
    // flip image, since in OpenCV row index from top to bottom
    dst.Flip();
  }
}

template<typename T>
void ImageToCVMat(const Array<2, T> &src, cv::Mat &dst) {
  dst.create(src.Size(0), src.Size(1), CVType<T>::TYPE);
  // flip image, since in OpenCV row index from top to bottom
  Image<T> tmp = src;
  tmp.Flip();
  memcpy(dst.ptr(), tmp.Data(), tmp.size() * sizeof(T));
}
} // namespace zzz
#endif // ZZZ_LIB_OPENCV
