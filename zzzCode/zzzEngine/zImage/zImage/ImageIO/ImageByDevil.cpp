#include "../zImageConfig.hpp"

#if defined(ZZZ_IMAGE_IO_DEVIL) && defined(ZZZ_LIB_DEVIL)
#include <zCore/Utility/StringTools.hpp>
#include <zCore/Utility/Log.hpp>
#include "../Image/Image.hpp"
#include "ILImage.hpp"

namespace zzz{
#define IMAGE_SPECIALIZE(T,format,type,channels) \
template<>\
bool Image<T>::LoadFile(const string &filename) {\
  ILImage image; \
  ZLOG(ZVERBOSE)<<"Loading image: "<<filename<<"\n"; \
  if (image.Load(filename.c_str())==IL_FALSE) {ZLOG(ZERROR)<<"Load image failed: "<<filename<<endl; return false;}\
  image.Convert(format,type); \
  SetSize(image.Height(),image.Width()); \
  memcpy(Data(),image.GetData(),sizeof(T)*size_all); \
  return true; \
}\
template<>\
bool Image<T>::SaveFile(const string &filename) const {\
  ILImage image; \
  image.Convert(format,type); \
  image.Resize(Cols(),Rows(),1); \
  memcpy(image.GetData(),Data(),sizeof(T)*size_all); \
  if (channels==4) image.Convert(IL_RGBA,IL_UNSIGNED_BYTE); \
  ZLOG(ZVERBOSE)<<"Saving image: "<<filename<<"\n"; \
  if (image.Save(filename.c_str())==IL_FALSE) {ZLOG(ZERROR)<<"Save image failed: "<<filename<<endl; return false;}\
  return true; \
}\
;
IMAGE_SPECIALIZE(Vector4uc,ZZZ_RGBA,ZZZ_UNSIGNED_BYTE,4);
IMAGE_SPECIALIZE(Vector4f,ZZZ_RGBA,ZZZ_FLOAT,4);
IMAGE_SPECIALIZE(Vector3uc,ZZZ_RGB,ZZZ_UNSIGNED_BYTE,3);
IMAGE_SPECIALIZE(Vector3f,ZZZ_RGB,ZZZ_FLOAT,3);
IMAGE_SPECIALIZE(zuchar,ZZZ_LUMINANCE,ZZZ_UNSIGNED_BYTE,1);
IMAGE_SPECIALIZE(float,ZZZ_LUMINANCE,ZZZ_FLOAT,1);
}
#endif // ZZZ_LIB_DEVIL
