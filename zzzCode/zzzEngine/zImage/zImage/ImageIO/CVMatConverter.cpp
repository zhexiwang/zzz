#include "CVMatConverter.hpp"

#ifdef ZZZ_LIB_OPENCV
namespace zzz {
void SmartConvertToRGB(const cv::Mat &src, cv::Mat &dst) {
  cv::Mat tmp;
  switch(src.channels()) {
  case 1:
    if (dst.channels() == 3) {
      tmp.create(1, 1, CV_32FC3);
      cv::cvtColor(src, tmp, CV_GRAY2RGB);
      if (dst.type() == CV_8UC3) tmp.convertTo(dst, dst.type());
      else if (dst.type() == CV_32FC3) tmp.convertTo(dst, dst.type(), 1.0/256.0);
    } else if (dst.channels() == 4) {
      tmp.create(1, 1, CV_32FC4);
      cv::cvtColor(src, tmp, CV_GRAY2RGBA);
      if (dst.type() == CV_8UC4) tmp.convertTo(dst, dst.type());
      else if (dst.type() == CV_32FC4) tmp.convertTo(dst, dst.type(), 1.0/256.0);
    } else if (dst.channels() == 1) {
      if (dst.type() == CV_8UC1) src.convertTo(dst, dst.type());
      else if (dst.type() == CV_32FC1) src.convertTo(dst, dst.type(), 1.0/256.0);
    }
    break;
  case 3:
    if (dst.channels() == 1) {
      tmp.create(1, 1, CV_32FC1);
      cv::cvtColor(src, tmp, CV_BGR2GRAY);
      if (dst.type() == CV_8UC1) tmp.convertTo(dst, dst.type());
      else if (dst.type() == CV_32FC1) tmp.convertTo(dst, dst.type(), 1.0/256.0);
    } else if (dst.channels() == 4) {
      tmp.create(1, 1, CV_32FC4);
      cv::cvtColor(src, tmp, CV_BGR2RGBA);
      if (dst.type() == CV_8UC4) tmp.convertTo(dst, dst.type());
      else if (dst.type() == CV_32FC4) tmp.convertTo(dst, dst.type(), 1.0/256.0);
    } else if (dst.channels() == 3) {
      tmp.create(1, 1, CV_32FC3);
      cv::cvtColor(src, tmp, CV_BGR2RGB);
      if (dst.type() == CV_8UC3) tmp.convertTo(dst, dst.type());
      else if (dst.type() == CV_32FC3) tmp.convertTo(dst, dst.type(), 1.0/256.0);
    }
    break;
  case 4:
    if (dst.channels() == 1) {
      tmp.create(1, 1, CV_32FC1);
      cv::cvtColor(src, tmp, CV_BGRA2GRAY);
      if (dst.type() == CV_8UC1) tmp.convertTo(dst, dst.type());
      else if (dst.type() == CV_32FC1) tmp.convertTo(dst, dst.type(), 1.0/256.0);
    } else if (dst.channels() == 3) {
      tmp.create(1, 1, CV_32FC3);
      cv::cvtColor(src, tmp, CV_BGRA2RGB);
      if (dst.type() == CV_8UC3) tmp.convertTo(dst, dst.type());
      else if (dst.type() == CV_32FC3) tmp.convertTo(dst, dst.type(), 1.0/256.0);
    } else if (dst.channels() == 4) {
      tmp.create(1, 1, CV_32FC4);
      cv::cvtColor(src, tmp, CV_BGRA2RGBA);
      if (dst.type() == CV_8UC4) tmp.convertTo(dst, dst.type());
      else if (dst.type() == CV_32FC4) tmp.convertTo(dst, dst.type(), 1.0/256.0);
    }
    break;
  }
}

void SmartConvertToBGR(const cv::Mat &src, cv::Mat &dst) {
  cv::Mat tmp;
  switch(src.channels()) {
  case 1:
    dst = src;
    break;
  case 3:
    dst.create(1, 1, src.type());
    cv::cvtColor(src, dst, CV_RGB2BGR);
    break;
  case 4:
    dst.create(1, 1, src.type());
    cv::cvtColor(src, dst, CV_RGBA2BGRA);
    break;
  }
}

};  // namespace zzz
#endif