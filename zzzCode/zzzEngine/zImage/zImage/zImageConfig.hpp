#pragma once

#include <zCore/EnvDetect.hpp>
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_COMPILER_MSVC
  #ifdef ZZZ_OS_WIN32
  #include "zImageConfig.win32.hpp"
  #endif

  #ifdef ZZZ_OS_WIN64
  #include "zImageConfig.win64.hpp"
  #endif
#endif

#ifdef ZZZ_OS_MACOS
#include "zImageConfig.macos.hpp"
#endif

#ifdef ZZZ_COMPILER_MINGW
#include "zImageConfig.mingw.hpp"
#endif

#if !defined(ZZZ_LIB_DEVIL) && !defined(ZZZ_LIB_FREEIMAGE) && !defined(ZZZ_LIB_OPENCV)
#define ZZZ_IMAGE_BY_SELF
#endif

#ifdef ZZZ_DYNAMIC
  #ifdef ZGRAPHICS_SOURCE
    #define ZIMAGE_FUNC __declspec(dllexport)
    #define ZIMAGE_CLASS __declspec(dllexport)
  #else
    #define ZIMAGE_FUNC __declspec(dllimport)
    #define ZIMAGE_CLASS __declspec(dllimport)
  #endif
#else
  #define ZIMAGE_FUNC
  #define ZIMAGE_CLASS
#endif
