#pragma once

#ifndef ZZZ_NO_PRAGMA_LIB

#ifdef _DEBUG
#ifndef ZZZ_OS_WIN64
#pragma comment(lib,"zVisualizationD.lib")
#else
#pragma comment(lib,"zVisualizationD_x64.lib")
#endif // ZZZ_OS_WIN64
#else
#ifndef ZZZ_OS_WIN64
#pragma comment(lib,"zVisualization.lib")
#else
#pragma comment(lib,"zVisualization_x64.lib")
#endif // ZZZ_OS_WIN64
#endif

#endif  // ZZZ_NO_PRAGMA_LIB


#include "Visualizer/TreeVisualizer2D.hpp"
#include "Visualizer/SequenceVisualizer2D.hpp"
#include "Visualizer/VisualizeNode2D.hpp"
#include "Visualizer/VImage2D.hpp"
#include "Visualizer/VText2D.hpp"
