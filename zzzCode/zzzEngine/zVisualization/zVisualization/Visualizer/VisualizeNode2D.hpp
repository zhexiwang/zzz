#pragma once
#include <zCore/common.hpp>
#include "VisualizeObj2D.hpp"
#include <zCore/Math/Vector2.hpp>

namespace zzz{
class Vis2DRenderer;
class VisualizeNode2D
{
public:
  VisualizeNode2D():drawedge_(true), bbox_(Vector2i(0)){}
  virtual ~VisualizeNode2D();
  void AddObject(VisualizeObj2D* obj);
  void DrawPixels(Vis2DRenderer *renderer);
  void Clear();
  vector<VisualizeObj2D*> objs_;
  double posx_,posy_;
  AABB<2,int> bbox_;
  bool drawedge_;
};
}