#pragma once
#include "VisualizeObj2D.hpp"
#include <zGraphics/Graphics/BMPFont.hpp>

namespace zzz{
class VText2D : public VisualizeObj2D
{
public:
  VText2D(string &str,int x, int y):str_(str)
  {
    Vector2i size=BMPFont::Instance().Size(str.c_str());
    bbox_.Min()=Vector2i(x, y);
    bbox_.Max()=Vector2i(x+size[0], y+size[1]);
  }

  virtual void DrawPixels(Vis2DRenderer *renderer, double basex, double basey)
  {
    int x = basex + bbox_.Min(0);
    int y = basey + bbox_.Min(1);
    renderer->SetRasterPos(x, y);
    BMPFont::Instance().Draw(str_.c_str());
  }
  string str_;
  int posx_,posy_;
};
}