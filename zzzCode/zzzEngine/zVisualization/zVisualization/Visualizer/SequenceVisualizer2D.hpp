#pragma once
#include "Visualizer2D.hpp"
#include "VisualizeNode2D.hpp"

namespace zzz{
class SequenceVisualizer2D:public Visualizer2D
{
public:
  SequenceVisualizer2D(Vis2DRenderer *renderer,int num):Visualizer2D(renderer),maxnum_(num),autofocus_(true){}
  ~SequenceVisualizer2D(void)
  {
    for (list<VisualizeNode2D*>::iterator lvi=nodes_.begin();lvi!=nodes_.end();lvi++)
      delete *lvi;
    nodes_.clear();
  }
  void AddNode(VisualizeNode2D *node);
  void Draw();
  list<VisualizeNode2D*> nodes_;
  int maxnum_;

  bool autofocus_;
};
}