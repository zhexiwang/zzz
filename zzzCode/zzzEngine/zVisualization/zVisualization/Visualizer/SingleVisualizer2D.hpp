#pragma once
#include "Visualizer2D.hpp"
#include "VisualizeNode2D.hpp"

//has only one node to show one image

namespace zzz{
class SingleVisualizer2D:public Visualizer2D
{
public:
  SingleVisualizer2D(Vis2DRenderer *renderer):Visualizer2D(renderer),autocenter_(true),node_(NULL){}
  ~SingleVisualizer2D(void)
  {
    if (node_) delete node_;
    node_=NULL;
  }
  void SetNode(VisualizeNode2D *node)
  {
    if (node_) delete node_;
    node_=node;
  }
  void Draw()
  {
    if (!node_) return;
    if (autocenter_)
    {
      Vector3d pos=renderer_->UnProject(renderer_->width_,renderer_->height_);
      node_->posx_=pos[0]-node_->bbox_.topleft[0];
      node_->posy_=pos[1]-node_->bbox_.topleft[1];
    }
    node_->DrawPixels(renderer_);
  }
  VisualizeNode2D* node_;
  bool autocenter_;
};
}