#pragma once
#include "VisualizeNode2D.hpp"
#include "Visualizer2D.hpp"
#include <zCore/Utility/Tree.hpp>
namespace zzz{
class TreeVisualizer2D:public Visualizer2D
{
public:
  TreeVisualizer2D(Vis2DRenderer *renderer):Visualizer2D(renderer),direction(1)
  {}
  void Draw();
  void Rerange(int gap=10);
  Tree<VisualizeNode2D*> tree_;
  int direction;
};
}