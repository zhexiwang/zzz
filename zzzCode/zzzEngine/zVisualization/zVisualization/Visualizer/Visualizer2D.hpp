#pragma once
#include <zGraphics/Renderer/Vis2DRenderer.hpp>

//use Vis2DRenderer to visualize 2D information

namespace zzz{
class Visualizer2D
{
public:
  Visualizer2D(Vis2DRenderer *renderer):renderer_(renderer){}
  virtual void Draw()=0;
  Vis2DRenderer *renderer_;
};
}