#pragma once
#include "Visualizer2D.hpp"
#include "VisualizeNode2D.hpp"

namespace zzz{
class CombineVisualizer2D:public Visualizer2D
{
public:
  CombineVisualizer2D(Vis2DRenderer *renderer):Visualizer2D(renderer),autocenter_(true){}
  ~CombineVisualizer2D(void)
  {
    for (size_t i=0; i<visualizers_.size(); i++)
      delete visualizers_[i];
    visualizers_.clear();
  }
  void AddVisualizer(Visualizer2D *node)
  {
    node->SetRenderer(renderer_);
    visualizers_.push_back(node);
  }
  void Draw()
  {
    for (size_t i=0; i<visualizers_.size(); i++)
      visualizers_[i]->Draw();
  }
  vector<Visualizer2D*> visualizers_;
};
}