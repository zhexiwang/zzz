#include "SequenceVisualizer2D.hpp"

namespace zzz{
void SequenceVisualizer2D::AddNode(VisualizeNode2D *node)
{
  if ((int)nodes_.size()>=maxnum_) nodes_.pop_front();
  double width=node->bbox_.Diff(0);
  for (list<VisualizeNode2D*>::iterator lvi=nodes_.begin();lvi!=nodes_.end();lvi++)
  {
    VisualizeNode2D *thisnode=*lvi;
    thisnode->posx_+=width;
  }
  node->posx_=0;
  node->posy_=0;
  nodes_.push_back(node);
}

void SequenceVisualizer2D::Draw()
{
  if (autofocus_)
  {
    Vector2i size=nodes_.back()->bbox_.Diff();
    int x=nodes_.back()->posx_+size[0]/2,y=nodes_.back()->posy_+size[1]/2;
    renderer_->SetCenter(x,y);
    CHECK_GL_ERROR();
  }
  for (list<VisualizeNode2D*>::iterator lvi=nodes_.begin();lvi!=nodes_.end();lvi++)
  {
    VisualizeNode2D *node=*lvi;
    node->DrawPixels(renderer_);
    CHECK_GL_ERROR();
  }
}
}