#pragma once
#include <zGraphics/Graphics/AABB.hpp>
namespace zzz{
class Vis2DRenderer;
class VisualizeObj2D
{
public:
  AABB<2,int> bbox_;
  virtual void DrawPixels(Vis2DRenderer *renderer,double basex, double basey)=0;
};
}