#pragma once
#include <zImage/Image/Image.hpp>
#include "VisualizeObj2D.hpp"
namespace zzz{
template<typename T>
class VImage2D : public VisualizeObj2D, public GraphicsHelper
{
public:
  VImage2D(const Image<T> &img, int x=0, int y=0):image(img)
  {
    bbox_.Min()=Vector2i(x,y);
    bbox_.Max()=Vector2i(x+image.Cols(), y+image.Rows());
  }
  virtual void DrawPixels(Vis2DRenderer *renderer, double basex, double basey)
  {
    int x = basex + bbox_.Min(0);
    int y = basey + bbox_.Min(1);
    renderer->SetRasterPosRelative(x, y);
    DrawImage(image);
  }
  Image<T> image;
};
}