#pragma once
#include <z3rd/LibraryConfig.hpp>
#include "NetworkSession.hpp"
#include <zCore/Utility/Thread.hpp>

namespace zzz {
class UDPSession;
template<typename S = UDPSession>
class UDPClient {
public:
  UDPClient(size_t buffer_size = 1024 * 10, void *userdata = NULL)
    : buffer_size_(buffer_size),
      userdata_(userdata) {
  }
  bool Init(std::string const & address, const int port) {
    address_ = address;
    port_ = port;
    connected_ = false;
    if (address.empty()) return false;
    boost::asio::ip::udp::resolver resolver(io_service_);
    boost::asio::ip::udp::resolver::query query(boost::asio::ip::udp::v4(), address_, "daytime");
    boost::asio::ip::udp::resolver::iterator query_iter = resolver.resolve(query);
    boost::asio::ip::udp::resolver::iterator end;
    if (query_iter != end) {
      session_.reset(new S(io_service_, 0, buffer_size_, userdata_));
      session_->GetEndPoint() = *query_iter;
      session_->GetEndPoint().port(port_);
      connected_ = true;
    } else {
      ZLOGE << "Cannot resolve " << address_ << "\n";
      connected_ = false;
    }
    return connected_;
  }
  bool Close() {
    if (session_) {
      return session_->Close();
    }
    return false;
  }
  inline bool Ready() const {return connected_;}
  size_t SyncSend(const char* data, size_t length) {
    if (!Ready()) return 0;
    return session_->SyncSend(data, length);
  }
  size_t SyncReceive(const char* data, size_t length) {
    if (!Ready()) return 0;
    return session_->SyncReceive();
  }
  void AsyncSend(const char *data, long long data_length) {
    if (!Ready()) return;
    return session_->AsyncSend(data, data_length);
  }
  void AsyncReceive() {
    if (!Ready()) return;
    return session_->AsyncReceive();
  }
protected:
  boost::asio::io_service io_service_;
  boost::shared_ptr<S> session_;
  std::string address_;
  int port_;
  size_t buffer_size_;
  void* userdata_;
  bool connected_;
};

class TCPSession;
template<typename S = TCPSession>
class TCPClient {
  class ServiceIOThread : public Thread {
  public:
    ServiceIOThread(boost::asio::io_service &io_service) : io_service_(io_service) {}
    void Main() {
      boost::system::error_code ec;
      if (!io_service_.run(ec)) {
        cout << ec.message() << endl;
      }
    }
  private:
    boost::asio::io_service &io_service_;
  };
public:
  typedef boost::asio::ip::tcp::socket Socket;
  TCPClient(size_t buffer_size = 1024 * 10, void *userdata = NULL)
    : port_(0),
      session_(new S(io_service_, buffer_size, userdata)){}
  bool Init(std::string const & address, const int port) {
    address_ = address;
    port_ = port;
    connected_ = false;
    if (address.empty()) return false;
    boost::asio::ip::tcp::resolver resolver(io_service_);
    boost::asio::ip::tcp::resolver::query query(boost::asio::ip::tcp::v4(), address_, "daytime");
    boost::asio::ip::tcp::resolver::iterator queryIter = resolver.resolve(query);
    boost::asio::ip::tcp::resolver::iterator end;

    if (queryIter != end) {
      server_ = *queryIter;
      server_.port(port_);
      boost::system::error_code error;
      session_->GetSocket().connect(server_, error);
      if (!error) {
        connected_ = true;
      } else {
        ZLOGE << "Cannot connect to " << address_ << ":" << port_ << "\n";
      }
    } else {
      ZLOGE << "Cannot resolve " << address_ << "\n";
    }
    session_->Start();
    return connected_;
  }
  bool Close() {
    return session_->Close();
  }
  inline bool Ready() const {return connected_;}

  // ATTENTION: This is for async_operations.
  // Always need to issue a pending async_read or async_write or io_service loop will exit;
  // Remember to call async_op before this and keep at least on async_op runs.
  void Run(int thread_number) {
    if (thread_number == 0) {
      io_service_.run();
    } else {
      for (int i = 0; i < thread_number; ++i) {
        ServiceIOThread *t = new ServiceIOThread(io_service_);
        threads_.push_back(t);
        t->Start();
      }
    }
  }
  void Wait() {
    if (threads_.size() > 0) {
      for (size_t i = 0; i < threads_.size(); ++i)
        threads_[i]->Wait();
    }
  }
  void Stop() {
    io_service_.stop();
    if (threads_.size() > 0) {
      for (size_t i = 0; i < threads_.size(); ++i)
        threads_[i]->Wait();
      for (size_t i = 0; i < threads_.size(); ++i)
        delete threads_[i];
      threads_.clear();
    }
  }

  size_t SyncSend(const char* data, size_t length) {
    if (!Ready()) return 0;
    return session_->SyncSend(data, length);
  }
  size_t SyncReceive() {
    if (!Ready()) return 0;
    return session_->SyncReceive();
  }
  size_t SyncReceive(char* data, size_t length) {
    if (!Ready()) return 0;
    return session_->SyncReceive(data, length);
  }
  void AsyncSend(const char *data, long long data_length) {
    if (!Ready()) return;
    return session_->AsyncSend(data, data_length);
  }
  void AsyncReceive() {
    if (!Ready()) return;
    return session_->AsyncReceive();
  }
  void AsyncReceive(char* data, size_t length) {
    if (!Ready()) return;
    return session_->AsyncReceive(data, length);
  }

protected:
  boost::asio::io_service io_service_;
  boost::shared_ptr<S> session_;
  boost::asio::ip::tcp::endpoint server_;
  std::string address_;
  int port_;
  bool connected_;
  vector<ServiceIOThread*> threads_;
};

} //namespace zzz
