#pragma once
#include <z3rd/LibraryConfig.hpp>
#include "NetworkSession.hpp"
#include <zCore/Utility/Thread.hpp>
#include <boost/shared_ptr.hpp>

namespace zzz {
class ServerThread : public Thread {
public:
  ServerThread(boost::asio::io_service &io_service) : io_service_(io_service) {}
  void Main() {io_service_.run();}
private:
  boost::asio::io_service &io_service_;
};

template<typename S>
class TCPServer {
public:
  TCPServer()
    : port_(-1) {};
  TCPServer(short port, void* userdata = NULL)
    : port_(port),
      acceptor_(io_service_, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)),
      userdata_(userdata),
      thread_number_(0) {
    StartAccept();
  }
  ~TCPServer() {
    Stop();
  }
  virtual void Prepare(){}
  virtual void CleanUp(){}
  void Run(int thread_number) {
    Prepare();
    thread_number_ = thread_number;
    if (thread_number == 0) {
      io_service_.run();
    } else {
      for (int i = 0; i < thread_number; ++i) {
        ServerThread *t = new ServerThread(io_service_);
        threads_.push_back(t);
        t->Start();
      }
    }
  }
  void Wait() {
    if (threads_.size() > 0) {
      for (size_t i = 0; i < threads_.size(); ++i)
        threads_[i]->Wait();
    }
  }
  void Stop() {
    io_service_.stop();
    if (threads_.size() > 0) {
      for (size_t i = 0; i < threads_.size(); ++i)
        threads_[i]->Wait();
      for (size_t i = 0; i < threads_.size(); ++i)
        delete threads_[i];
      threads_.clear();
    }
    thread_number_ = 0;
    CleanUp();
  }
protected:
  void StartAccept() {
    boost::shared_ptr<S> new_session;
    new_session.reset(new S(io_service_, userdata_));
    acceptor_.async_accept(new_session->GetSocket(),
      boost::bind(&TCPServer::AfterAccept, this, new_session, boost::asio::placeholders::error));
  }

  void AfterAccept(boost::shared_ptr<S> new_session, const boost::system::error_code& error) {
    if (!error) {
      new_session->Start();
    } 
    StartAccept();
  }
  boost::asio::io_service io_service_;
  boost::asio::ip::tcp::acceptor acceptor_;
  int port_;
  int thread_number_;
  vector<ServerThread*> threads_;
  void *userdata_;
};

template<typename S>
class UDPServer {
public:
  UDPServer(short port, void *userdata = NULL)
    : port_(port),
      session_(new S(io_service_, port, userdata)),
      userdata_(userdata),
      thread_number_(0) {
    session_->Start();
  }
  ~UDPServer() {
    Stop();
  }
  virtual void Prepare(){}
  virtual void CleanUp(){}
  void Run(int thread_number) {
    Prepare();
    thread_number_ = thread_number;
    if (thread_number == 0) {
      io_service_.run();
    } else {
      for (int i = 0; i < thread_number; ++i) {
        ServerThread *t = new ServerThread(io_service_);
        threads_.push_back(t);
        t->Start();
      }
    }
  }
  void Wait() {
    if (threads_.size() > 0) {
      for (size_t i = 0; i < threads_.size(); ++i)
        threads_[i]->Wait();
    }
  }
  void Stop() {
    io_service_.stop();
    if (threads_.size() > 0) {
      for (size_t i = 0; i < threads_.size(); ++i)
        threads_[i]->Wait();
      for (size_t i = 0; i < threads_.size(); ++i)
        delete threads_[i];
      threads_.clear();
    }
    thread_number_ = 0;
    CleanUp();
  }
private:
  int port_;
  boost::asio::io_service io_service_;
  boost::shared_ptr<S> session_;
  int thread_number_;
  std::vector<ServerThread*> threads_;
  void *userdata_;
};
} //namespace zzz
