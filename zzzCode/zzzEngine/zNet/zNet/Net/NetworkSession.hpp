#pragma once
#include <cstdlib>
#include <iostream>
#include <z3rd/LibraryConfig.hpp>
#include <boost/bind.hpp>
#ifdef ZZZ_OS_WIN
  #ifndef _WIN32_WINNT
    #define _WIN32_WINNT 0x0501
  #endif
#endif
#include <boost/asio.hpp>
#include <zCore/Utility/Log.hpp>
#include <zCore/Utility/Uncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace zzz {
class TCPSession : public boost::enable_shared_from_this<TCPSession>, zzz::Uncopyable {
public:
  typedef boost::asio::ip::tcp::socket Socket;
  TCPSession(boost::asio::io_service& io_service, std::string::size_type buffer_size, void *userdata)
    : socket_(io_service),
      userdata_(userdata),
      strand_(io_service) {
    buffer_.resize(buffer_size);
  }
  Socket& GetSocket() { return socket_; }
  virtual void Start() {AsyncReceive();}
  virtual void Close() {
    boost::system::error_code error;
    socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_send, error);
    if (error) {
      ZLOGE << "Failed to shut down the connection.";
    }
    socket_.close();
  }
  size_t JustSyncSend(const char* data, size_t length, boost::system::error_code &error) {
    size_t send_bytes = socket_.send(boost::asio::buffer(data, length), 0, error);
    return send_bytes;
  }
  size_t SyncSend(const char* data, size_t length) {
    boost::system::error_code error;
    size_t send_bytes = JustSyncSend(data, length, error);
    HandleSyncSend(error, send_bytes);
    return send_bytes;
  }
  size_t JustSyncReceive(boost::system::error_code &error, char *buf, size_t size) {
    size_t receive_bytes = socket_.receive(boost::asio::buffer(buf, size), 0, error);
    return receive_bytes;
  }
  size_t JustSyncReceive(boost::system::error_code &error) {
    return JustSyncReceive(error, &(buffer_[0]), buffer_.size());
  }
  size_t SyncReceive(char *buf, size_t size) {
    boost::system::error_code error;
    size_t receive_bytes = JustSyncReceive(error, buf, size);
    HandleSyncReceive(error, receive_bytes);
    return receive_bytes;
  }
  size_t SyncReceive() {
    return SyncReceive(&(buffer_[0]), buffer_.size());
  }
  template<typename ConstBufferSequence>
  void AsyncSend(const ConstBufferSequence &buffers) {
    boost::asio::async_write(socket_, buffers,
        strand_.wrap(boost::bind(&TCPSession::HandleAsyncSend, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)));
  }
  void AsyncSend(const char *data, size_t data_length) {
    boost::asio::async_write(socket_, boost::asio::buffer(data, data_length),
        strand_.wrap(boost::bind(&TCPSession::HandleAsyncSend, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)));
  }
  void AsyncReceive() {
    AsyncReceive(&(buffer_[0]), buffer_.size());
  }
  void AsyncReceive(char *buf, size_t size) {
    socket_.async_read_some(boost::asio::buffer(buf, size),
        strand_.wrap(boost::bind(&TCPSession::HandleAsyncReceive, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)));
  }
protected:
  void HandleSyncReceive(const boost::system::error_code& error, size_t bytes_transferred){
    AfterSyncReceive(error, bytes_transferred);
  }
  void HandleAsyncReceive(const boost::system::error_code& error, size_t bytes_transferred){
    AfterAsyncReceive(error, bytes_transferred);
  }
  void HandleSyncSend(const boost::system::error_code& error, size_t bytes_transferred){
    AfterSyncSend(error, bytes_transferred);
  }
  void HandleAsyncSend(const boost::system::error_code& error, size_t bytes_transferred){
    AfterAsyncSend(error, bytes_transferred);
  }
  // To be overloaded
  virtual void AfterSyncReceive(const boost::system::error_code& error, size_t bytes_transferred){}
  virtual void AfterAsyncReceive(const boost::system::error_code& error, size_t bytes_transferred){}
  virtual void AfterSyncSend(const boost::system::error_code& error, size_t bytes_transferred){}
  virtual void AfterAsyncSend(const boost::system::error_code& error, size_t bytes_transferred){}
  /// Strand to ensure the connection's handlers are not called concurrently.
  boost::asio::io_service::strand strand_;
  Socket socket_;
  std::string buffer_;
  void *userdata_;
};
class UDPSession : public boost::enable_shared_from_this<UDPSession>, zzz::Uncopyable {
public:
  typedef boost::asio::ip::udp::socket Socket;
  UDPSession(boost::asio::io_service& io_service, short port, std::string::size_type buffer_size, void *userdata)
    : socket_(io_service, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), port)),
      userdata_(userdata),
      strand_(io_service) {
    buffer_.resize(buffer_size);
  }
  ~UDPSession() {Close();}
  boost::asio::ip::udp::endpoint &GetEndPoint() {return endpoint_;}
  bool Close() {
    socket_.close();
    return true;
  }
  void Start() {AsyncReceive();}
  size_t SyncSend(const char* data, size_t length) {
    boost::system::error_code error;
    size_t send_bytes = socket_.send_to(boost::asio::buffer(data, length), endpoint_, 0, error);
    HandleSyncSend(error, send_bytes);
    return send_bytes;
  }
  size_t SyncReceive(const char* data, size_t length) {
    boost::system::error_code error;
    size_t receive_bytes = socket_.receive_from(boost::asio::buffer(&(buffer_[0]), buffer_.size()), endpoint_, 0, error);
    HandleSyncReceive(error, receive_bytes);
    return receive_bytes;
  }
  void AsyncSend(const char *data, long long data_length) {
    socket_.async_send_to(
        boost::asio::buffer(data, data_length), endpoint_,
        strand_.wrap(boost::bind(&UDPSession::HandleAsyncSend, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)));
  }
  void AsyncReceive() {
    socket_.async_receive_from(
        boost::asio::buffer(&(buffer_[0]), buffer_.size()), endpoint_,
        strand_.wrap(boost::bind(&UDPSession::HandleAsyncReceive, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)));
  }

protected:
  void HandleSyncReceive(const boost::system::error_code &error, size_t bytes_received) {
    AfterSyncReceive(error, bytes_received);
  }
  void HandleAsyncReceive(const boost::system::error_code &error, size_t bytes_received) {
    AfterAsyncReceive(error, bytes_received);
  }
  void HandleSyncSend(const boost::system::error_code& error, size_t bytes_sent) {
    AfterSyncSend(error);
  }
  void HandleAsyncSend(const boost::system::error_code& error, size_t bytes_sent) {
    AfterAsyncSend(error);
  }
  virtual void AfterSyncReceive(const boost::system::error_code& error, size_t bytes_transferred) {}
  virtual void AfterAsyncReceive(const boost::system::error_code& error, size_t bytes_transferred) {}
  virtual void AfterSyncSend(const boost::system::error_code& error) {}
  virtual void AfterAsyncSend(const boost::system::error_code& error) {}
  std::string buffer_;
  Socket socket_;
  boost::asio::ip::udp::endpoint endpoint_;
  /// Strand to ensure the connection's handlers are not called concurrently.
  boost::asio::io_service::strand strand_;
  void *userdata_;
};


} //namespace zzz
