#pragma once

#ifndef ZZZ_NO_PRAGMA_LIB

#ifdef ZZZ_COMPILER_MSVC

#ifdef ZZZ_DEBUG
#ifndef ZZZ_OS_WIN64
#pragma comment(lib,"zNetD.lib")
#else
#pragma comment(lib,"zNetD_x64.lib")
#endif // ZZZ_OS_WIN64
#else
#ifndef ZZZ_OS_WIN64
#pragma comment(lib,"zNet.lib")
#else
#pragma comment(lib,"zNet_x64.lib")
#endif // ZZZ_OS_WIN64
#endif

#endif

#endif