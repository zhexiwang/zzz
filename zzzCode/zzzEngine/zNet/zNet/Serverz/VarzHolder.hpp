#pragma once
#include <zCore/Utility/AnyHolder.hpp>

namespace zzz {

class VarzHolder : public AnyHolder {
public:
  template<typename T>
  T& AddVarz(const string& name, const T& init) {
    T* v = new T(init);
    this->Add(v, name, false);
    return *v;
  }
  template<typename T>
  T& GetVarz(const string& name) {
    return *(this->Get<T*>(name));
  }
};
} // namespace zzz

#define VARZHOLDER zzz::Singleton<zzz::VarzHolder>::Instance

#define ADDVARZ(type, name, init) VARZHOLDER().AddVarz<type>(name, init)
#define ADDVARZ_INT(name, init) VARZHOLDER().AddVarz<int>(name, init)
#define ADDVARZ_STRING(name, init) VARZHOLDER().AddVarz<string>(name, init)
#define ADDVARZ_DOUBLE(name, init) VARZHOLDER().AddVarz<double>(name, init)
#define ADDVARZ_FLOAT(name, init) VARZHOLDER().AddVarz<float>(name, init)

#define VARZ(type, name) VARZHOLDER().GetVarz<type>(name)
#define VARZ_INT(name) VARZHOLDER().GetVarz<int>(name)
#define VARZ_STRING(name) VARZHOLDER().GetVarz<string>(name)
#define VARZ_DOUBLE(name) VARZHOLDER().GetVarz<double>(name)
#define VARZ_FLOAT(name) VARZHOLDER().GetVarz<float>(name)
