#include "Serverz.hpp"
#include <Poco/Net/DNS.h>
#include <zCore/Utility/FileTools.hpp>
#include <zCore/Utility/CmdParser.hpp>
#include "VarzHolder.hpp"

ZFLAGS_INT(serverz_port, -1, "Serverz server port. -1 means random number from 10000 to 20000.");

namespace zzz {

const char INDEX_HTML[] = 
"<html><body>"
"<h1>Registered Handles</h1>"
"<p><a href=\"/\">public get handles</a> <a href=\"/?show_hidden=1\">all get handles</a> <a href=\"/?show_post=1\">all public handles</a> <a href=\"/?show_hidden=1&show_post=1\">all handles</a></p>"
"{links}"
"</body></html>";

const char VARZ_HTML[] = 
"\
<html><head>\
 <title>{program} Varz</title>\
 <style>\
 table, td, th {\
  border:1px solid green;\
 }\
 th {\
  background-color:green;\
  color:white;\
 }\
 </style>\
 <script>\
  function output(data) {\
   var str = '<table><tr><th>Name</th><th>Value</th>';\
   for (var k in data)\
    str += '<tr><td>' + k + '</td><td>' + data[k] + '</td></tr>';\
   document.getElementById('content').innerHTML = str;\
  }\
  function main() {\
   var data = {varz};\
   output(data);\
   document.getElementById(\"auto_reloader\").selectedIndex = parseInt(window.location.hash.substr(1));\
   SetAutoReload();\
  }\
  window.onload = main;\
  auto_refresh_interval = 0;\
  function ReloadPage() {location.reload();}\
  function SetAutoReload() {var e = document.getElementById(\"auto_reloader\");\
  window.location.hash = \"#\" + e.selectedIndex; auto_refresh_interval = parseInt(e.options[e.selectedIndex].value); if (auto_refresh_interval != 0) window.setInterval(ReloadPage, auto_refresh_interval);}\
 </script>\
</head>\
<body>\
<h1 style=\"color: blue;\">Varz for {program}</h1>\
<button onclick=\"ReloadPage()\">Refresh</button>\
Auto refresh every <select id=\"auto_reloader\" onchange=\"SetAutoReload()\">\
<option value=\"0\">Disabled</option>\
<option value=\"500\">0.5 second</option>\
<option value=\"1000\">1 second</option>\
<option value=\"5000\">5 seconds</option>\
<option value=\"10000\">10 seconds</option>\
<option value=\"60000\">1 minute</option>\
</select>\
<div id=\"content\"></div>\
</body>\
</html>\
";

void ServerzRequestHandler::handleRequest(HTTPServerRequest& request, HTTPServerResponse& response) {
  HTMLForm form(request, request.stream());
  string request_path(request.getURI().begin(),
    find(request.getURI().begin(), request.getURI().end(), '?'));
  response.setStatus(HTTPResponse::HTTP_OK);
  response.setContentType("text/html");

  if (request.getMethod() == "GET") {
    if (serverz_->HandleGET(request_path, form, request, response)) return;
  } else if (request.getMethod() == "POST") {
    if (serverz_->HandlePOST(request_path, form, request, response)) return;
  }
  ReplySpecial(response, HTTPResponse::HTTP_INTERNAL_SERVER_ERROR);
}


bool Serverz::DefaultHandler(const std::string& request_path, const HTMLForm &params, const HTTPServerRequest& req, HTTPServerResponse& rep) {
  if (request_path == "/") {
    bool show_hidden = false;
    bool show_post = false;
    HTMLForm::ConstIterator show_hidden_it = params.find("show_hidden");
    HTMLForm::ConstIterator show_post_it = params.find("show_post");
    if (show_hidden_it != params.end() && show_hidden_it->second == "1") show_hidden = true;
    if (show_post_it != params.end() && show_post_it->second == "1") show_post = true;
    string links;
    links += "<h2>Any handler</h2>";
    for (map<string, HandleFunction>::const_iterator mi = any_handlers_.begin(); mi != any_handlers_.end(); ++mi) {
      if (mi->first == "/") continue;
      if (!show_hidden && mi->first.find("/_") != string::npos) continue;
      links += "<a href=\"" + mi->first + "\">" + mi->first + "</a><br>";
    }
    links += "<h2>Get handler</h2>";
    for (map<string, HandleFunction>::const_iterator mi = get_handlers_.begin(); mi != get_handlers_.end(); ++mi) {
      if (mi->first == "/") continue;
      if (!show_hidden && mi->first.find("/_") != string::npos) continue;
      links += "<a href=\"" + mi->first + "\">" + mi->first + "</a><br>";
    }
    if (show_post) {
      links += "<h2>Post handler</h2>";
      for (map<string, HandleFunction>::const_iterator mi = post_handlers_.begin(); mi != post_handlers_.end(); ++mi) {
        if (mi->first == "/") continue;
        if (!show_hidden && mi->first.find("/_") != string::npos) continue;
        links += "<a href=\"" + mi->first + "\">" + mi->first + "</a><br>";
      }
    }
    string ret = Replace(INDEX_HTML, "{links}", links);
    ReplyHTML(rep, ret);
    return true;
  }
  if (request_path == "/globalz") {
    string res = Replace(VARZ_HTML, "{program}", GetFilename(ZGLOBAL_GET(string, __CURRENT_PROGRAM_NAME__)));
    res = Replace(res, "Varz", "Globalz");
    string jsonstr;
    zzz::Global::Instance().GetJsonString(jsonstr);
    res = Replace(res, "{varz}", jsonstr);
    ReplyHTML(rep, res);
    return true;
  } else if (request_path == "/varz") {
    string res = Replace(VARZ_HTML, "{program}", GetFilename(ZGLOBAL_GET(string, __CURRENT_PROGRAM_NAME__)));
    string jsonstr;
    VARZHOLDER().GetJsonString(jsonstr);
    res = Replace(res, "{varz}", jsonstr);
    ReplyHTML(rep, res);
    return true;
  } else if (request_path == "/helloz") {
    ReplyHTML(rep, "Hello World!");
    return true;
  } else if (request_path == "/healthz") {
    ReplyHTML(rep, "OK!");
    return true;
  } else if (request_path == "/statz") {
    ostringstream oss;
    oss << "Get request number: " << get_request_counter_ << "<br>"
        << "Post request number: " << post_request_counter_ << "<br>"
        << "Maximum thread number: " << server_->params().getMaxThreads() << "<br>"
        << "Started: " << start_timer_.Elapsed() << "seconds<br>";
    ReplyHTML(rep, oss.str());
    return true;
  }
  return false;
}

bool Serverz::HandleGET(const std::string& request_path, const HTMLForm &params, const HTTPServerRequest& req, HTTPServerResponse& rep) {
  AtomicInc(get_request_counter_);

  if (get_handlers_.find(request_path) != get_handlers_.end()) {
    if (get_handlers_[request_path](request_path, params, req, rep)) return true;
  } else if (any_handlers_.find(request_path) != any_handlers_.end()) {
    if (any_handlers_[request_path](request_path, params, req, rep)) return true;
  }
  ReplySpecial(rep, HTTPServerResponse::HTTP_NOT_FOUND);
  return true;
}

bool Serverz::HandlePOST(const std::string& request_path, const HTMLForm &params, const HTTPServerRequest& req, HTTPServerResponse& rep) {
  AtomicInc(post_request_counter_);

  if (post_handlers_.find(request_path) != post_handlers_.end()) {
    if (post_handlers_[request_path](request_path, params, req, rep)) return true;
  } else if (any_handlers_.find(request_path) != any_handlers_.end()) {
    if (any_handlers_[request_path](request_path, params, req, rep)) return true;
  }
  ReplySpecial(rep, HTTPServerResponse::HTTP_NOT_FOUND);
  return true;
}

void Serverz::Start(short port) {
  port_ = (port != -1) ? port : ZFLAG_serverz_port;
  if (port_ == -1) {
    zzz::RandomInteger<int> random(10000, 20000);
    random.SeedFromTime();
    port_ = random.Rand();
  }
  HTTPServerParams::Ptr params(new HTTPServerParams);
  params->setTimeout(Poco::Timespan::SECONDS * 5);
  params->setMaxThreads(1);
  server_ = new HTTPServer(new ServerzRequestHandlerFactory(this), ServerSocket(port_), params);
  cout << "Serverz starts at http://" << Poco::Net::DNS::hostName() << ":" << port_ << endl;
  server_->start();
}

} // namespace zzz