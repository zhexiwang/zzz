#pragma once
#pragma warning(disable: 4355)
#include <z3rd/Wrapper/PocoNetWrapper.hpp>
#include <zCore/Utility/AnyHolder.hpp>
#include <zCore/Utility/CmdParser.hpp>
#include <zCore/Math/Random.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>

namespace zzz {

class Serverz;

/// The common handler for all incoming requests.
class ServerzRequestHandler: public Poco::Net::HTTPRequestHandler {
public:
  ServerzRequestHandler(Serverz* serverz)
    : serverz_(serverz) {
  }
  virtual void handleRequest(HTTPServerRequest& request, HTTPServerResponse& response);

  Serverz *serverz_;
};

class ServerzRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {
public:
  ServerzRequestHandlerFactory(Serverz *serverz)
    : serverz_(serverz) {
  }

  virtual HTTPRequestHandler* createRequestHandler(const HTTPServerRequest &) {
    return new ServerzRequestHandler(serverz_);
  }

  Serverz *serverz_;
};

class Serverz {
public:
  Serverz()
    : port_(-1),
      server_(NULL),
      get_request_counter_(0),
      post_request_counter_(0) {
    Prepare();
  }
  ~Serverz(){}

  void Start(short port = -1);

  void Prepare() {
    RegisterGetHandler("/", boost::bind(&Serverz::DefaultHandler, this, _1, _2, _3, _4));
    RegisterGetHandler("/globalz", boost::bind(&Serverz::DefaultHandler, this, _1, _2, _3, _4));
    RegisterGetHandler("/varz", boost::bind(&Serverz::DefaultHandler, this, _1, _2, _3, _4));
    RegisterGetHandler("/healthz", boost::bind(&Serverz::DefaultHandler, this, _1, _2, _3, _4));
    RegisterGetHandler("/helloz", boost::bind(&Serverz::DefaultHandler, this, _1, _2, _3, _4));
    RegisterGetHandler("/statz", boost::bind(&Serverz::DefaultHandler, this, _1, _2, _3, _4));
    start_timer_.Restart();
  }

  typedef boost::function<bool (const std::string& request_path,
                                const Poco::Net::HTMLForm &form,
                                const Poco::Net::HTTPServerRequest &req,
                                Poco::Net::HTTPServerResponse &rsp)> HandleFunction;
  void RegisterAnyHandler(const string &path, HandleFunction func) {
    ZCHECK_FALSE(path.empty());
    ZCHECK_EQ(path[0], '/') << "path should starts with /";

    if (any_handlers_.find(path) != any_handlers_.end()) {
      ZLOGS << "path overwritten" << path;
    }
    any_handlers_[path] = func;
  }

  void UnregisterAnyHandler(const string &path) {
    any_handlers_.erase(path);
  }

  void RegisterGetHandler(const string &path, HandleFunction func) {
    ZCHECK_FALSE(path.empty());
    ZCHECK_EQ(path[0], '/') << "path should starts with /";

    if (get_handlers_.find(path) != get_handlers_.end()) {
      ZLOGS << "path overwritten" << path;
    }
    get_handlers_[path] = func;
  }

  void UnregisterGetHandler(const string &path) {
    get_handlers_.erase(path);
  }

  void RegisterPostHandler(const string &path, HandleFunction func) {
    ZCHECK_FALSE(path.empty());
    ZCHECK_EQ(path[0], '/') << "path should starts with /";

    if (post_handlers_.find(path) != post_handlers_.end()) {
      ZLOGS << "path overwritten" << path;
    }
    post_handlers_[path] = func;
  }

  void UnregisterPostHandler(const string &path) {
    post_handlers_.erase(path);
  }

  bool HandleGET(const std::string& request_path, const HTMLForm &params, const HTTPServerRequest& req, HTTPServerResponse& rep);
  bool HandlePOST(const std::string& request_path, const HTMLForm &params, const HTTPServerRequest& req, HTTPServerResponse& rep);
  bool DefaultHandler(const std::string& request_path, const HTMLForm &params, const HTTPServerRequest& req, HTTPServerResponse& rep);

private:
  Poco::Net::HTTPServer *server_;

  map<string, HandleFunction> get_handlers_, post_handlers_, any_handlers_;
  long get_request_counter_, post_request_counter_;
  Timer start_timer_;
  short port_;
};

typedef Singleton<Serverz> TheServerz;
#define SERVERZ TheServerz::Instance()
} // namespace zzz
