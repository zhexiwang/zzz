#include "HttpFileServer.hpp"
#include "HttpRequest.hpp"
#include "HttpReply.hpp"
#include "MIMETypes.hpp"
#include <vector>
#include <zNet/Net/NetworkSession.hpp>
#include <zCore/Utility/FileTools.hpp>
#include <zCore/Utility/CmdParser.hpp>
using namespace std;

namespace zzz {
void HttpFileRequestHandler::HandleGET(const std::string& request_path, const std::map<std::string, std::string>& params, const HttpRequest& req, HttpReply& rep) {
  std::string path = request_path;

  // Request path must be absolute and not contain "..".
  if (path.empty() || path[0] != '/' || path.find("..") != std::string::npos) {
    rep = HttpReply::StockReply(HttpReply::BAD_REQUEST);
    return;
  }

  path.assign(path.begin() + 1, path.end());
  path = PathFile(doc_root_, path);
  if (DirExists(path)) {
    string ret;
    ret += "<html><head><title>zzzEngine HTTP File Server</title></head><body>";
    ret += "<h2>Directory listing for ";
    ret += request_path;
    ret += "</h2><hr><ul>\n";
    vector<string> dirlist;
    ListFileAndDir(path, false, dirlist);
    for (zuint i = 0; i < dirlist.size(); ++i) {
      string rel = RelativeTo(dirlist[i], doc_root_);
      ret += "<li><a href=\"/";
      ret += rel;
      ret += "\">";
      ret += rel;
      ret += "</a>\n";
    }
    ret += "</ul><hr></body></html>";
    rep.ReplyHTML(ret);
    return;
  } else {
    // Open the file to send back.
    rep.ReplyFile(path);
  }
}

HttpFileRequestHandler::HttpFileRequestHandler(void *userdata)
  : HttpRequestHandler(userdata) {
  doc_root_ = *(reinterpret_cast<const string *>(userdata));
}


};  // namespace zzz