#pragma once
#include <string>

namespace zzz {
/// Convert a file extension into a MIME type.
std::string ExtensionToMIMEType(const std::string& extension);

};  // namespace zzz