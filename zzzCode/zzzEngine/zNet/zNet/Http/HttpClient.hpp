#pragma once
#include <zNet/Http/HttpRequest.hpp>
#include <zNet/Http/HttpReply.hpp>
#include <zNet/Net/NetworkSession.hpp>
#include <zNet/Net/NetworkClient.hpp>

namespace zzz {
class HttpClientSession : public TCPSession {
public:
  HttpClientSession(boost::asio::io_service &io, int buffer_size, void *userdata) 
    : TCPSession(io, buffer_size, userdata) {
  }
  void Start(){}
};

class HttpClient : public TCPClient<HttpClientSession> {
public:
  bool GET(const string &url, HttpReply &rep);
public:
  static void ToString(const HttpRequest &req, string &str);
};

} // namespace zzz
