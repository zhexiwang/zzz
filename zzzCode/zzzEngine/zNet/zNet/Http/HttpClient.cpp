#include "HttpClient.hpp"
#include <Poco/URI.h>
#include <zCore/Utility/Format.hpp>
#include <zCore/Utility/ZCheck.hpp>
#include "HttpReplyParser.hpp"
#include "HttpReply.hpp"

namespace zzz {
bool HttpClient::GET(const string &url, HttpReply &rep) {
  Poco::URI uri(url);

  string scheme = uri.getScheme();
  int port = uri.getPort();
  if (scheme.empty() && port == 0) port = 80; // default is HTTP
  ZCHECK_NOT_ZERO(port) << "Cannot get port";
  string host = uri.getHost();
  string path = uri.getPathEtc();
  if (host.empty()) host = path;

  HttpRequest req;
  req.method = "GET";
  req.uri = path;
  req.http_version_major = 1;
  req.http_version_minor = 1;
  req.headers.push_back(HttpHeader("Host", host));

  string str;
  ToString(req, str);

  Init(host, port);
  SyncSend(str.data(), str.size());

  string res;
  while(true) {
    const int BUFFER_SIZE = 1024 * 10;
    char buffer[BUFFER_SIZE];
    int bytes = SyncReceive(buffer, BUFFER_SIZE);
    res.insert(res.end(), buffer, buffer + bytes);
    HttpReplyParser parser;
    if (!parser.Parse(rep, res.begin(), res.end())) break;
    bool goon = false;
    for (zuint i = 0; i < rep.headers.size(); ++i) {
      if (rep.headers[i].name == "Content-length") {
        int len;
        FromString(rep.headers[i].value, len);
        if (rep.content.size() < len) {
          goon = true;
          break;
        }
      }
    }
    if (goon) continue;
    break;
  }
  return true;
}

void HttpClient::ToString(const HttpRequest &req, string &str) {
  str.clear();
  str += Format("%s %s HTTP/%d.%d\n") % req.method % req.uri % req.http_version_major % req.http_version_minor;
  for (zuint i = 0; i < req.headers.size(); ++i) {
    str += Format("%s: %s\n") % req.headers[i].name % req.headers[i].value;
  }
  str += "\n";
  str += req.content;
}

} // namespace zzz
