#include "MIMETypes.hpp"

namespace zzz {
struct mapping {
  const char* extension;
  const char* mime_type;
} mappings[] = {
  { ".pdf", "application/pdf" },
  { ".zip", "application/zip" },
  { ".gzip", "application/gzip" },

  { ".mp4", "audio/mp4" },
  { ".mp3", "audio/mpeg" },
  { ".mpeg", "audio/mpeg" },
  { ".ogg", "audio/ogg" },

  { ".gif", "image/gif" },
  { ".jpg", "image/jpeg" },
  { ".png", "image/png" },
  { ".tiff", "image/tiff" },

  { ".css", "text/css" },
  { ".csv", "text/csv" },
  { ".htm", "text/html" },
  { ".html", "text/html" },

  { ".flv", "video/x-flv" },
  { ".mov", "video/quicktime" },
  { 0, 0 } // Marks end of list.
};

std::string ExtensionToMIMEType(const std::string& extension) {
  for (mapping* m = mappings; m->extension; ++m) {
    if (m->extension == extension) {
      return m->mime_type;
    }
  }
  return "text/plain";
}

};  // namespace zzz