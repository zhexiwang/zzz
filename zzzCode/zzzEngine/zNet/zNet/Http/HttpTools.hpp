#pragma once
#include <string>

namespace zzz {
int HttpGet(const std::string &url, std::string &str);
};  // namespace zzz