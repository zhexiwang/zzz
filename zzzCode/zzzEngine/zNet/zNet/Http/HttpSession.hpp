#pragma once

#include <zNet/Net/NetworkSession.hpp>
#include <zNet/Net/NetworkServer.hpp>
#include <zCore/Utility/Log.hpp>
#include <zCore/Utility/CmdParser.hpp>
#include "HttpRequestParser.hpp"
#include "HttpRequest.hpp"
#include "HttpRequestHandler.hpp"
#include "HttpReply.hpp"

ZDECLARE_BOOL(http_session_debug);

namespace zzz {
template<typename R>
class HttpSession : public zzz::TCPSession {
public:
  HttpSession(boost::asio::io_service& io_service, void *userdata)
    : TCPSession(io_service, 1024 * 1024, userdata),
      userdata_(userdata) {
  }
  void AfterAsyncReceive(const boost::system::error_code& e, std::size_t bytes_transferred) {
    if (!e) {
      HttpRequest request;
      boost::tribool result;
      content_.append(&(buffer_[0]), &(buffer_[0]) + bytes_transferred);
      HttpRequestParser parser;
      result = parser.Parse(request, content_.begin(), content_.end());
      for (zuint i = 0; i < request.headers.size(); ++i) {
        if (request.headers[i].name == "Content-Length") {
          int length = FromString<int>(request.headers[i].value);
          if (int(request.content.size()) < length - 10) {
            AsyncReceive();
            return;
          } else {
            break;
          }
        }
      }
      content_.clear();
      ostringstream debug_oss;
      if (ZFLAG_http_session_debug) {
        debug_oss << socket_.remote_endpoint().address().to_string() << " "
                  << request.method << " [" << request.uri << "] "
                  << ZVAR(request.content)
                  << " -> ";
      }
      if (result) {
        R request_handler(userdata_);
        request_handler.HandleRequest(request, reply_);
        if (ZFLAG_http_session_debug) {
          debug_oss << reply_.status << endl;
          ZLOGD << debug_oss.str();
        }
        AsyncSend(reply_.ToBuffers());
      } else if (!result) {
        reply_ = HttpReply::StockReply(HttpReply::BAD_REQUEST);
        if (ZFLAG_http_session_debug) {
          debug_oss << reply_.status << endl;
          ZLOGD << debug_oss.str();
        }
        AsyncSend(reply_.ToBuffers());
      } else {
        AsyncReceive();
      }
    }
  }
  void AfterAsyncSend(const boost::system::error_code& e, size_t bytes_transferred) {
    if (!e) {
      // Initiate graceful connection closure.
      boost::system::error_code ignored_ec;
      socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ignored_ec);
    }
  }
protected:
  string content_;
  void *userdata_;
  // The reply will be async send out, so it cannot be destroyed immediately
  HttpReply reply_;
};

typedef HttpSession<HttpRequestHandler> TrivialHttpSession;
typedef TCPServer<TrivialHttpSession> TrivialHttpServer;

}  // namespace zzz