#pragma once

#include <string>
#include <vector>
#include "HttpHeader.hpp"

namespace boost {
  namespace asio {
    class const_buffer;
  }
}
namespace zzz {
/// A reply to be sent to a client.
struct HttpReply {
  HttpReply()
    : http_version_minor(0),
      http_version_major(0),
      status_code(0)  {}
  int http_version_minor, http_version_major;
  int status_code;
  std::string status_msg;

  void ReplyJSON(const std::string &str);
  void ReplyText(const std::string &str);
  void ReplyHTML(const std::string &str);
  void ReplyFile(const std::string &str);

  /// The status of the reply.
  enum StatusType {
    OK = 200,
    CREATED = 201,
    ACCEPTED = 202,
    NO_CONTENT = 204,
    MULTIPLE_CHOICES = 300,
    MOVED_PERMANENTLY = 301,
    MOVED_TEMPORARILY = 302,
    NOT_MODIFIED = 304,
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500,
    NOT_IMPLEMENTED = 501,
    BAD_GATEWAY = 502,
    SERVICE_UNAVAILABLE = 503
  } status;

  /// The headers to be included in the reply.
  std::vector<HttpHeader> headers;

  /// The content to be sent in the reply.
  std::string content;

  /// Convert the reply into a vector of buffers. The buffers do not own the
  /// underlying memory blocks, therefore the reply object must remain valid and
  /// not be changed until the write operation has completed.
  std::vector<boost::asio::const_buffer> ToBuffers();

  /// Get a stock reply.
  static HttpReply StockReply(StatusType status);
};

};  // namespace zzz