#pragma once
#include "HttpRequestHandler.hpp"
#include "MIMETypes.hpp"
#include "HttpReply.hpp"
#include "HttpRequest.hpp"
#include <zCore/Utility/StringTools.hpp>
#include <zCore/Utility/FileTools.hpp>
#include <zCore/Utility/zVar.hpp>
using namespace std;
namespace zzz {
void HttpRequestHandler::HandleRequest(const HttpRequest& req, HttpReply& rep) {
  // Decode url to path.
  std::string request_path;
  map<string, string> params;
  if (req.method == "GET") {
    vector<string> s;
    SplitString(req.uri, s, "?");
    switch(s.size()) {
    case 1:
      if (!URLDecode(s[0], request_path)) {
        rep = HttpReply::StockReply(HttpReply::BAD_REQUEST);
        return;
      }
      break;
    case 2:
      if (!URLDecode(s[0], request_path)) {
        rep = HttpReply::StockReply(HttpReply::BAD_REQUEST);
        return;
      }
      if (!ParseParams(s[1], params)) {
        rep = HttpReply::StockReply(HttpReply::BAD_REQUEST);
        return;
      }
      break;
    default:
      rep = HttpReply::StockReply(HttpReply::BAD_REQUEST);
      return;
    }
    return HandleGET(request_path, params, req, rep);
  } else if (req.method == "POST") {
    if (!URLDecode(req.uri, request_path)) {
      rep = HttpReply::StockReply(HttpReply::BAD_REQUEST);
      return;
    }
    if (!ParseParams(req.content, params)) {
      rep = HttpReply::StockReply(HttpReply::BAD_REQUEST);
      return;
    }
    return HandlePOST(request_path, params, req, rep);
  } else {
    rep = HttpReply::StockReply(HttpReply::NOT_IMPLEMENTED);
    return;
  }
}

void HttpRequestHandler::HandlePOST(const std::string& request_path, const std::map<std::string, std::string>& params, const HttpRequest& req, HttpReply& rep) {
  string ret = "<html><head><title>zzzEngine</title></head><body><h1>HTTP Server powered by zzzEngine</h1></body></html>"; 
  rep.ReplyHTML(ret);
}

void HttpRequestHandler::HandleGET(const std::string& request_path, const std::map<std::string, std::string>& params, const HttpRequest& req, HttpReply& rep) {
  string ret = "<html><head><title>zzzEngine</title></head><body><h1>HTTP Server powered by zzzEngine</h1></body></html>"; 
  rep.ReplyHTML(ret);
}


bool HttpRequestHandler::URLDecode(const std::string& in, std::string& out) {
  out.clear();
  out.reserve(in.size());
  for (std::size_t i = 0; i < in.size(); ++i) {
    if (in[i] == '%') {
      if (i + 3 <= in.size()) {
        int value = 0;
        std::istringstream is(in.substr(i + 1, 2));
        if (is >> std::hex >> value) {
          out += static_cast<char>(value);
          i += 2;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else if (in[i] == '+') {
      out += ' ';
    } else {
      out += in[i];
    }
  }
  return true;
}

bool HttpRequestHandler::URLEncode(const std::string& in, std::string& out) {
  out.clear();
  out.reserve(in.size() * 3);
  for (std::size_t i = 0; i < in.size(); ++i) {
    char x = in[i];
    if ((x >= '0' && x <= '9') ||
        (x >= 'a' && x <= 'z') ||
        (x >= 'A' && x <= 'Z')) {
      out.push_back(x);
    } else {
      out.push_back('%');
      ostringstream oss;
      oss << std::hex << int(x);
      out.append(oss.str());
    }
  }
  return true;
}

bool HttpRequestHandler::ParseParams(const std::string& str, std::map<std::string, std::string> &params) {
  vector<string> s;
  SplitString(str, s, "&");
  for (zuint i = 0; i < s.size(); ++i) {
    vector<string> s2;
    SplitString(s[i], s2, "=");
    if (s2.size() != 2) return false;
    string key, value;
    if (!URLDecode(s2[0], key)) return false;
    if (!URLDecode(s2[1], value)) return false;
    params[key] = value;
  }
  return true;
}

bool HttpRequestHandler::HTMLEncode(const std::string& in, std::string& out) {
  out.clear();
  out.reserve(in.size() * 3);
  for (std::size_t i = 0; i < in.size(); ++i) {
    char x = in[i];
    switch(x) {
    case ' ': out.append("&nbsp;"); break;
    case '\"': out.append("&quot;"); break;
    case '&': out.append("&amp;"); break;
    case '<': out.append("&lt;"); break;
    case '>': out.append("&gt;"); break;
    default: out.push_back(x); break;
    }
  }
  return true;
}

HttpRequestHandler::HttpRequestHandler(void *userdata)
  : userdata_(userdata) {
}

};  // namespace zzz