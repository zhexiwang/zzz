#include <zNet/Net/NetworkSession.hpp>
#include "HttpReply.hpp"
#include <string>
#include <boost/lexical_cast.hpp>
#include <zCore/Utility/FileTools.hpp>
#include "MIMETypes.hpp"

namespace zzz {
namespace status_strings {
const std::string OK =
  "HTTP/1.0 200 OK\r\n";
const std::string CREATED =
  "HTTP/1.0 201 Created\r\n";
const std::string ACCEPTED =
  "HTTP/1.0 202 Accepted\r\n";
const std::string NO_CONTENT =
  "HTTP/1.0 204 No Content\r\n";
const std::string MULTIPLE_CHOICES =
  "HTTP/1.0 300 Multiple Choices\r\n";
const std::string MOVED_PERMANENTLY =
  "HTTP/1.0 301 Moved Permanently\r\n";
const std::string MOVED_TEMPORARILY =
  "HTTP/1.0 302 Moved Temporarily\r\n";
const std::string NOT_MODIFIED =
  "HTTP/1.0 304 Not Modified\r\n";
const std::string BAD_REQUEST =
  "HTTP/1.0 400 Bad Request\r\n";
const std::string UNAUTHORIZED =
  "HTTP/1.0 401 Unauthorized\r\n";
const std::string FORBIDDEN =
  "HTTP/1.0 403 Forbidden\r\n";
const std::string NOT_FOUND =
  "HTTP/1.0 404 Not Found\r\n";
const std::string INTERNAL_SERVER_ERROR =
  "HTTP/1.0 500 Internal Server Error\r\n";
const std::string NOT_IMPLEMENTED =
  "HTTP/1.0 501 Not Implemented\r\n";
const std::string BAD_GATEWAY =
  "HTTP/1.0 502 Bad Gateway\r\n";
const std::string SERVICE_UNAVAILABLE =
  "HTTP/1.0 503 Service Unavailable\r\n";

boost::asio::const_buffer to_buffer(HttpReply::StatusType status) {
  switch (status) {
  case HttpReply::OK:
    return boost::asio::buffer(OK);
  case HttpReply::CREATED:
    return boost::asio::buffer(CREATED);
  case HttpReply::ACCEPTED:
    return boost::asio::buffer(ACCEPTED);
  case HttpReply::NO_CONTENT:
    return boost::asio::buffer(NO_CONTENT);
  case HttpReply::MULTIPLE_CHOICES:
    return boost::asio::buffer(MULTIPLE_CHOICES);
  case HttpReply::MOVED_PERMANENTLY:
    return boost::asio::buffer(MOVED_PERMANENTLY);
  case HttpReply::MOVED_TEMPORARILY:
    return boost::asio::buffer(MOVED_TEMPORARILY);
  case HttpReply::NOT_MODIFIED:
    return boost::asio::buffer(NOT_MODIFIED);
  case HttpReply::BAD_REQUEST:
    return boost::asio::buffer(BAD_REQUEST);
  case HttpReply::UNAUTHORIZED:
    return boost::asio::buffer(UNAUTHORIZED);
  case HttpReply::FORBIDDEN:
    return boost::asio::buffer(FORBIDDEN);
  case HttpReply::NOT_FOUND:
    return boost::asio::buffer(NOT_FOUND);
  case HttpReply::INTERNAL_SERVER_ERROR:
    return boost::asio::buffer(INTERNAL_SERVER_ERROR);
  case HttpReply::NOT_IMPLEMENTED:
    return boost::asio::buffer(NOT_IMPLEMENTED);
  case HttpReply::BAD_GATEWAY:
    return boost::asio::buffer(BAD_GATEWAY);
  case HttpReply::SERVICE_UNAVAILABLE:
    return boost::asio::buffer(SERVICE_UNAVAILABLE);
  default:
    return boost::asio::buffer(INTERNAL_SERVER_ERROR);
  }
}

} // namespace status_strings

namespace misc_strings {
const char name_value_separator[] = { ':', ' ' };
const char crlf[] = { '\r', '\n' };
} // namespace misc_strings

std::vector<boost::asio::const_buffer> HttpReply::ToBuffers() {
  std::vector<boost::asio::const_buffer> buffers;
  buffers.push_back(status_strings::to_buffer(status));
  for (std::size_t i = 0; i < headers.size(); ++i) {
    HttpHeader& h = headers[i];
    buffers.push_back(boost::asio::buffer(h.name));
    buffers.push_back(boost::asio::buffer(misc_strings::name_value_separator));
    buffers.push_back(boost::asio::buffer(h.value));
    buffers.push_back(boost::asio::buffer(misc_strings::crlf));
  }
  buffers.push_back(boost::asio::buffer(misc_strings::crlf));
  buffers.push_back(boost::asio::buffer(content));
  return buffers;
}

namespace stock_replies { 
const char ok[] = "";
const char created[] =
  "<html>"
  "<head><title>Created</title></head>"
  "<body><h1>201 Created</h1></body>"
  "</html>";
const char accepted[] =
  "<html>"
  "<head><title>Accepted</title></head>"
  "<body><h1>202 Accepted</h1></body>"
  "</html>";
const char no_content[] =
  "<html>"
  "<head><title>No Content</title></head>"
  "<body><h1>204 Content</h1></body>"
  "</html>";
const char multiple_choices[] =
  "<html>"
  "<head><title>Multiple Choices</title></head>"
  "<body><h1>300 Multiple Choices</h1></body>"
  "</html>";
const char moved_permanently[] =
  "<html>"
  "<head><title>Moved Permanently</title></head>"
  "<body><h1>301 Moved Permanently</h1></body>"
  "</html>";
const char moved_temporarily[] =
  "<html>"
  "<head><title>Moved Temporarily</title></head>"
  "<body><h1>302 Moved Temporarily</h1></body>"
  "</html>";
const char not_modified[] =
  "<html>"
  "<head><title>Not Modified</title></head>"
  "<body><h1>304 Not Modified</h1></body>"
  "</html>";
const char bad_request[] =
  "<html>"
  "<head><title>Bad Request</title></head>"
  "<body><h1>400 Bad Request</h1></body>"
  "</html>";
const char unauthorized[] =
  "<html>"
  "<head><title>Unauthorized</title></head>"
  "<body><h1>401 Unauthorized</h1></body>"
  "</html>";
const char forbidden[] =
  "<html>"
  "<head><title>Forbidden</title></head>"
  "<body><h1>403 Forbidden</h1></body>"
  "</html>";
const char not_found[] =
  "<html>"
  "<head><title>Not Found</title></head>"
  "<body><h1>404 Not Found</h1></body>"
  "</html>";
const char internal_server_error[] =
  "<html>"
  "<head><title>Internal Server Error</title></head>"
  "<body><h1>500 Internal Server Error</h1></body>"
  "</html>";
const char not_implemented[] =
  "<html>"
  "<head><title>Not Implemented</title></head>"
  "<body><h1>501 Not Implemented</h1></body>"
  "</html>";
const char bad_gateway[] =
  "<html>"
  "<head><title>Bad Gateway</title></head>"
  "<body><h1>502 Bad Gateway</h1></body>"
  "</html>";
const char service_unavailable[] =
  "<html>"
  "<head><title>Service Unavailable</title></head>"
  "<body><h1>503 Service Unavailable</h1></body>"
  "</html>";

std::string ToString(HttpReply::StatusType status) {
  switch (status) {
  case HttpReply::OK:
    return ok;
  case HttpReply::CREATED:
    return created;
  case HttpReply::ACCEPTED:
    return accepted;
  case HttpReply::NO_CONTENT:
    return no_content;
  case HttpReply::MULTIPLE_CHOICES:
    return multiple_choices;
  case HttpReply::MOVED_PERMANENTLY:
    return moved_permanently;
  case HttpReply::MOVED_TEMPORARILY:
    return moved_temporarily;
  case HttpReply::NOT_MODIFIED:
    return not_modified;
  case HttpReply::BAD_REQUEST:
    return bad_request;
  case HttpReply::UNAUTHORIZED:
    return unauthorized;
  case HttpReply::FORBIDDEN:
    return forbidden;
  case HttpReply::NOT_FOUND:
    return not_found;
  case HttpReply::INTERNAL_SERVER_ERROR:
    return internal_server_error;
  case HttpReply::NOT_IMPLEMENTED:
    return not_implemented;
  case HttpReply::BAD_GATEWAY:
    return bad_gateway;
  case HttpReply::SERVICE_UNAVAILABLE:
    return service_unavailable;
  default:
    return internal_server_error;
  }
}

} // namespace stock_replies

HttpReply HttpReply::StockReply(HttpReply::StatusType status) {
  HttpReply rep;
  rep.status = status;
  rep.content = stock_replies::ToString(status);
  rep.headers.resize(2);
  rep.headers[0].name = "Content-Length";
  rep.headers[0].value = boost::lexical_cast<std::string>(rep.content.size());
  rep.headers[1].name = "Content-Type";
  rep.headers[1].value = "text/html";
  return rep;
}

void HttpReply::ReplyJSON(const std::string &str) {
  status = HttpReply::OK;
  content = str;
  headers.push_back(HttpHeader("Content-Length", ToString(str.length())));
  headers.push_back(HttpHeader("Content-Type", "application/json;"));
}

void HttpReply::ReplyText(const std::string &str) {
  status = HttpReply::OK;
  content = str;
  headers.push_back(HttpHeader("Content-Length", ToString(str.length())));
  headers.push_back(HttpHeader("Content-Type", "text/plain;"));
}

void HttpReply::ReplyHTML(const std::string &str) {
  status = HttpReply::OK;
  content = str;
  headers.push_back(HttpHeader("Content-Length", ToString(str.length())));
  headers.push_back(HttpHeader("Content-Type", "text/html"));
}

void HttpReply::ReplyFile(const std::string &filename) {
  std::ifstream is(filename.c_str(), std::ios::in | std::ios::binary);
  if (!is) {
    ZLOGS << "Cannot open " << filename << endl;
    (*this) = HttpReply::StockReply(HttpReply::NOT_FOUND);
    return;
  }

  // Fill out the reply to be sent to the client.
  status = HttpReply::OK;
  char buf[10240];
  while (is.read(buf, sizeof(buf)).gcount() > 0)
    content.append(buf, is.gcount());
  headers.push_back(HttpHeader("Content-Length", ToString(content.size())));
  headers.push_back(HttpHeader("Content-Type", ExtensionToMIMEType(GetExt(filename))));
}

};  // namespace zzz