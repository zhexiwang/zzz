#include "HttpReplyParser.hpp"
#include "HttpReply.hpp"
namespace zzz {
HttpReplyParser::HttpReplyParser()
  : state_(HTTP_VERSION_H) {
}

void HttpReplyParser::Reset() {
  state_ = HTTP_VERSION_H;
}

boost::tribool HttpReplyParser::Consume(HttpReply& rep, char input) {
  switch (state_) {
  case METHOD_START:
    if (!IsChar(input) || IsCtl(input) || IsTspecial(input)) {
      return false;
    } else {
      state_ = HTTP_VERSION_H;
      return boost::indeterminate;
    }
  case HTTP_VERSION_H:
    if (input == 'H') {
      state_ = HTTP_VERSION_T_1;
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_VERSION_T_1:
    if (input == 'T') {
      state_ = HTTP_VERSION_T_2;
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_VERSION_T_2:
    if (input == 'T') {
      state_ = HTTP_VERSION_P;
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_VERSION_P:
    if (input == 'P') {
      state_ = HTTP_VERSION_SLASH;
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_VERSION_SLASH:
    if (input == '/') {
      rep.http_version_major = 0;
      rep.http_version_minor = 0;
      state_ = HTTP_VERSION_MAJOR_START;
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_VERSION_MAJOR_START:
    if (IsDigit(input)) {
      rep.http_version_major = input - '0';
      state_ = HTTP_VERSION_MAJOR;
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_VERSION_MAJOR:
    if (input == '.') {
      state_ = HTTP_VERSION_MINOR_START;
      return boost::indeterminate;
    } else if (IsDigit(input)) {
      rep.http_version_major = rep.http_version_major * 10 + input - '0';
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_VERSION_MINOR_START:
    if (IsDigit(input)) {
      rep.http_version_minor = input - '0';
      state_ = HTTP_VERSION_MINOR;
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_VERSION_MINOR:
    if (input == ' ') {
      state_ = HTTP_STATUS_CODE_START;
      return boost::indeterminate;
    } else if (IsDigit(input)) {
      rep.http_version_minor = rep.http_version_minor * 10 + input - '0';
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_STATUS_CODE_START:
    if (IsDigit(input)) {
      rep.status_code = input - '0';
      state_ = HTTP_STATUS_CODE;
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_STATUS_CODE:
    if (input == ' ') {
      state_ = HTTP_STATUS_MSG;
      return boost::indeterminate;
    } else if (IsDigit(input)) {
      rep.status_code = rep.status_code * 10 + input - '0';
      return boost::indeterminate;
    } else {
      return false;
    }
  case HTTP_STATUS_MSG:
    if (input == '\r') {
      state_ = EXPECTION_NEWLINE_1;
      return boost::indeterminate;
    } else if (!IsChar(input) || IsCtl(input) || IsTspecial(input)) {
      return false;
    } else {
      rep.status_msg.push_back(input);
      return boost::indeterminate;
    }
  case EXPECTION_NEWLINE_1:
    if (input == '\n') {
      state_ = HEADER_LINE_START;
      return boost::indeterminate;
    } else {
      return false;
    }
  case HEADER_LINE_START:
    if (input == '\r') {
      state_ = EXPECTING_NEWLINE_3;
      return boost::indeterminate;
    } else if (!rep.headers.empty() && (input == ' ' || input == '\t')) {
      state_ = HEADER_LWS;
      return boost::indeterminate;
    } else if (!IsChar(input) || IsCtl(input) || IsTspecial(input)) {
      return false;
    } else {
      rep.headers.push_back(HttpHeader());
      rep.headers.back().name.push_back(input);
      state_ = HEADER_NAME;
      return boost::indeterminate;
    }
  case HEADER_LWS:
    if (input == '\r') {
      state_ = EXPECTING_NEWLINE_2;
      return boost::indeterminate;
    } else if (input == ' ' || input == '\t') {
      return boost::indeterminate;
    } else if (IsCtl(input)) {
      return false;
    } else {
      state_ = HEADER_VALUE;
      rep.headers.back().value.push_back(input);
      return boost::indeterminate;
    }
  case HEADER_NAME:
    if (input == ':') {
      state_ = SPACE_BEFORE_HEADER_VALUE;
      return boost::indeterminate;
    } else if (!IsChar(input) || IsCtl(input) || IsTspecial(input)) {
      return false;
    } else {
      rep.headers.back().name.push_back(input);
      return boost::indeterminate;
    }
  case SPACE_BEFORE_HEADER_VALUE:
    if (input == ' ') { state_ = HEADER_VALUE;
      return boost::indeterminate;
    } else {
      return false;
    }
  case HEADER_VALUE:
    if (input == '\r') {
      state_ = EXPECTING_NEWLINE_2;
      return boost::indeterminate;
    } else if (IsCtl(input)) {
      return false;
    } else {
      rep.headers.back().value.push_back(input);
      return boost::indeterminate;
    }
  case EXPECTING_NEWLINE_2:
    if (input == '\n') {
      state_ = HEADER_LINE_START;
      return boost::indeterminate;
    } else {
      return false;
    }
  case EXPECTING_NEWLINE_3:
    return (input == '\n');
  default:
    return false;
  }
}

bool HttpReplyParser::IsChar(int c) {
  return c >= 0 && c <= 127;
}

bool HttpReplyParser::IsCtl(int c) {
  return (c >= 0 && c <= 31) || (c == 127);
}

bool HttpReplyParser::IsTspecial(int c) {
  switch (c) {
  case '(': case ')': case '<': case '>': case '@':
  case ',': case ';': case ':': case '\\': case '"':
  case '/': case '[': case ']': case '?': case '=':
  case '{': case '}': case ' ': case '\t':
    return true;
  default:
    return false;
  }
}

bool HttpReplyParser::IsDigit(int c) {
  return c >= '0' && c <= '9';
}

};  // namespace zzz