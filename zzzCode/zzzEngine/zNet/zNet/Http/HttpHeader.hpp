#pragma once

#include <string>

namespace zzz {
struct HttpHeader {
  HttpHeader() {}
  HttpHeader(const std::string &_name, const std::string &_value)
    : name(_name),
      value(_value) {}
  std::string name;
  std::string value;
};

};  // namespace zzz