#include "HttpTools.hpp"
#define POCO_NO_AUTOMATIC_LIBS
#define POCO_STATIC
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/URI.h>
#include <Poco/StreamCopier.h>
#include <Poco/Exception.h>
#include <zCore/Utility/ZCheck.hpp>

using namespace std;

namespace zzz {
int HttpGet(const string &url, string &str) {
  // prepare session
  Poco::URI uri(url);
  ZCHECK_NOT_ZERO(uri.getPort()) << "Cannot get port.";
  ZCHECK_FALSE(uri.getHost().empty()) << "Cannot get host.";
  Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());

  // prepare path
  string path(uri.getPathAndQuery());
  if (path.empty()) path = "/";

  // send request
  Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_GET, path, Poco::Net::HTTPMessage::HTTP_1_1);
  session.sendRequest(req);

  // get response
  Poco::Net::HTTPResponse res;
  istream &is = session.receiveResponse(res);

  // print response
  ostringstream oss;
  Poco::StreamCopier::copyStream(is, oss);
  str = oss.str();
  return res.getStatus();
}

};  // namespace zzz
