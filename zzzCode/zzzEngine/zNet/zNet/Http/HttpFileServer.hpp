#pragma once
#include "HttpRequestHandler.hpp"
#include "HttpSession.hpp"
#include "../Net/NetworkServer.hpp"

namespace zzz {
struct HttpReply;
struct HttpRequest;

/// The common handler for all incoming requests.
class HttpFileRequestHandler: public HttpRequestHandler {
public:
  HttpFileRequestHandler(void *userdata);
  void HandleGET(const std::string& request_path, const std::map<std::string, std::string>& params, const HttpRequest& req, HttpReply& rep);
private:
  std::string doc_root_;
};

typedef HttpSession<HttpFileRequestHandler> HttpFileSession;
typedef TCPServer<HttpFileSession> HttpFileServer;
};  // namespace zzz