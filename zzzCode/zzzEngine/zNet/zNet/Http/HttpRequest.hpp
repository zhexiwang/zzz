#pragma once
#include <string>
#include <vector>
#include "HttpHeader.hpp"

namespace zzz {
/// A request received from a client.
struct HttpRequest {
  std::string method;
  std::string uri;
  int http_version_major;
  int http_version_minor;
  std::vector<HttpHeader> headers;
  std::string content;
};

};  // namespace zzz