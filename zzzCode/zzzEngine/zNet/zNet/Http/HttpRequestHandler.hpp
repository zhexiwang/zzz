#pragma once
#include <map>
#include <string>
#include <zCore/Utility/Uncopyable.hpp>

namespace zzz {
struct HttpReply;
struct HttpRequest;

/// The common handler for all incoming requests.
class HttpRequestHandler : zzz::Uncopyable {
public:
  HttpRequestHandler(void *userdata);
  /// Handle a request and produce a reply.
  virtual void HandleRequest(const HttpRequest& req, HttpReply& rep);
  virtual void HandleGET(const std::string& request_path, const std::map<std::string, std::string>& params, const HttpRequest& req, HttpReply& rep);
  virtual void HandlePOST(const std::string& request_path, const std::map<std::string, std::string>& params, const HttpRequest& req, HttpReply& rep);

protected:
  static bool ParseParams(const std::string& str, std::map<std::string, std::string> &params);
  static bool URLDecode(const std::string& in, std::string& out);
  static bool URLEncode(const std::string& in, std::string& out);
  static bool HTMLEncode(const std::string& in, std::string& out);
  
  void *userdata_;
};

};  // namespace zzz