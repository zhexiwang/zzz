#pragma once
#include <boost/logic/tribool.hpp>
#include <boost/tuple/tuple.hpp>
namespace zzz {
struct HttpRequest;

/// Parser for incoming requests.
class HttpRequestParser {
public:
  /// Construct ready to parse the request method.
  HttpRequestParser();

  /// Reset to initial parser state.
  void Reset();

  /// Parse some data. The tribool return value is true when a complete request
  /// has been parsed, false if the data is invalid, indeterminate when more
  /// data is required. The InputIterator return value indicates how much of the
  /// input has been consumed.
  template <typename InputIterator>
  boost::tribool Parse(HttpRequest& req, InputIterator begin, InputIterator end) {
    boost::tribool result;
    while (begin != end) { 
      result = Consume(req, *begin++);
      if (result || !result)
        break;
    }
    if (result == boost::indeterminate) return boost::indeterminate;
    req.content.assign(begin, end);
    return true;
  }

private:
  /// Handle the next character of input.
  boost::tribool Consume(HttpRequest& req, char input);

  /// Check if a byte is an HTTP character.
  static bool IsChar(int c);

  /// Check if a byte is an HTTP control character.
  static bool IsCtl(int c);

  /// Check if a byte is defined as an HTTP tspecial character.
  static bool IsTspecial(int c);

  /// Check if a byte is a digit.
  static bool IsDigit(int c);

  /// The current state of the parser.
  enum State {
    METHOD_START,
    METHOD,
    URI,
    HTTP_VERSION_H,
    HTTP_VERSION_T_1,
    HTTP_VERSION_T_2,
    HTTP_VERSION_P,
    HTTP_VERSION_SLASH,
    HTTP_VERSION_MAJOR_START,
    HTTP_VERSION_MAJOR,
    HTTP_VERSION_MINOR_START,
    HTTP_VERSION_MINOR,
    EXPECTION_NEWLINE_1,
    HEADER_LINE_START,
    HEADER_LWS,
    HEADER_NAME,
    SPACE_BEFORE_HEADER_VALUE,
    HEADER_VALUE,
    EXPECTING_NEWLINE_2,
    EXPECTING_NEWLINE_3
  } state_;
};

};  // namespace zzz