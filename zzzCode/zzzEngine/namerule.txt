class ClassName
{
public:
  void PublicFunctionName(int local_variable);
private:
  void privateFunctionName();
  
public:
  int public_variable_name_;
private:
  int private_variable_name_;
}

//for variable * and & should attach variable name
int *good;
int &good;
int* bad;
int& bad;

//for function return value * and & should attach variable type
int& good();
int* good();
int &bad();
int *bad();

//for parameter
if it is & or *, const should be specified
otherwise, make it const does not make many sence, although it can prevent some error, 
it will bring some troublesome when optimize