#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "CameraGUI.hpp"
#include <zCore/Utility/Timer.hpp>

namespace zzz {
class ZGRAPHICS_CLASS MovingCameraGUI : public CameraGUI
{
public:
  MovingCameraGUI();
  bool OnLButtonDown(unsigned int nFlags,int x,int y);
  bool OnMouseMove(unsigned int nFlags,int x,int y);
  bool OnMouseWheel(unsigned int nFlags, int zDelta, int x,int y);
  bool OnMButtonDown(unsigned int nFlags,int x,int y);
  bool OnMButtonUp(unsigned int nFlags,int x,int y);
  bool OnChar(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);

  double moving_speed_;
private:
  void UpdateSpeed();
  int last_x_, last_y_;
  double current_speed_;
  Timer timer_;
  const double accelerate_time_;
};
}  // namespace zzz
