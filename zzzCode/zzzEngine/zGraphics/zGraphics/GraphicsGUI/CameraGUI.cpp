#include "CameraGUI.hpp"
#include "../Renderer/Renderer.hpp"

namespace zzz {
CameraGUI::CameraGUI() {
  SetPosition(0, 0, 2);
}

bool CameraGUI::OnMouseWheel(unsigned int nFlags, int zDelta, int x,int y) {
  int nstep=zDelta;
  if (nstep<0) {
    nstep=-nstep;
    if (ortho_mode_) {
      for (int i=0; i<nstep; i++) ortho_scale_*=1.03f;
      SetPerspective(width_, height_);
    } else {
      for (int i=0; i<nstep; i++) Position_[2]*=1.03f;
    }
  } else {
    if (ortho_mode_) {
      for (int i=0; i<nstep; i++) ortho_scale_/=1.03f;
      SetPerspective(width_, height_);
    } else {
      for (int i=0; i<nstep; i++) Position_[2]/=1.03f;
    }
  }
  return true;
}

bool CameraGUI::OnMButtonDown(unsigned int nFlags,int x,int y) {
  renderer_->GetContext()->OwnMouse(true);
  lastX_ = x;
  lastY_ = y;
  return true;
}

bool CameraGUI::OnRButtonDown(unsigned int nFlags,int x,int y)
{
  renderer_->GetContext()->OwnMouse(true);
  lastX_ = x;
  lastY_ = y;
  return true;
}

bool CameraGUI::OnMouseMove(unsigned int nFlags,int x,int y)
{
  if (CheckBit(nFlags, ZZZFLAG_MMOUSE)) {
    if (!ortho_mode_) {
      MoveUpwards(0.003*Position_[2]*(y-lastY_));
      MoveLeftwards(0.003*Position_[2]*(x-lastX_));
    } else {
      MoveUpwards(0.003*ortho_scale_*(y-lastY_));
      MoveLeftwards(0.003*ortho_scale_*(x-lastX_));
    }
    lastX_=x;
    lastY_=y;
    return true;
  } else if (CheckBit(nFlags, ZZZFLAG_RMOUSE)) {
    int step = y - lastY_;
    if (step > 0)
      for (int i=0; i<step; i++) Position_[2]*=1.003f;
    else {
      step = -step;
      for (int i=0; i<step; i++) Position_[2]/=1.003f;
    }
    lastX_=x;
    lastY_=y;
    return true;
  } else
    return false;
}

void CameraGUI::OnSize(unsigned int nType, int cx, int cy)
{
  if (cy <= 0) return;
  SetPerspective(cx,cy);
}

}  // namespace zzz