#include "ArcBallGUI.hpp"
#include "../Renderer/Renderer.hpp"

namespace zzz {
bool ArcBallGUI::OnMouseMove(unsigned int nFlags,int x,int y) {
  if (CheckBit(nFlags, ZZZFLAG_LMOUSE)) {
    Rotate(x,y);
    return true;
  }
  return false;
}

bool ArcBallGUI::OnLButtonDown(unsigned int nFlags,int x,int y) {
  renderer_->GetContext()->OwnMouse(true);
  SetOldPos(x, y);
  return true;
}

void ArcBallGUI::OnSize(unsigned int nType, int cx, int cy) {
  if (cy > 0) {
    SetWindowSize(cx,cy);
  }
}

}  // namespace zzz