#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "../Graphics/ArcBall.hpp"
#include "GraphicsGUI.hpp"
#include "../Renderer/Renderer.hpp"

namespace zzz {
class ZGRAPHICS_CLASS ArcBallGUI : public ArcBall, public GraphicsGUI<Renderer> {
public:
  bool OnMouseMove(unsigned int nFlags,int x,int y);
  bool OnLButtonDown(unsigned int nFlags,int x,int y);
  bool OnLButtonUp(unsigned int nFlags,int x,int y) {return true;}
  void OnSize(unsigned int nType, int cx, int cy);
};
}  // namespace zzz