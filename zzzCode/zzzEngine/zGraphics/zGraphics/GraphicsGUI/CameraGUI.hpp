#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "../Graphics/Camera.hpp"
#include "GraphicsGUI.hpp"

namespace zzz {
class Renderer;
class ZGRAPHICS_CLASS CameraGUI : public Camera, public GraphicsGUI<Renderer> {
public:
  CameraGUI();
  bool OnMButtonDown(unsigned int nFlags,int x,int y);
  bool OnRButtonDown(unsigned int nFlags,int x,int y);
  bool OnMouseMove(unsigned int nFlags,int x,int y);
  bool OnMouseWheel(unsigned int nFlags, int zDelta, int x,int y);
  void OnSize(unsigned int nType, int cx, int cy);

private:
  bool middleButton_;
  bool rightButton_;
  int lastX_, lastY_;
};
}  // namespace zzz