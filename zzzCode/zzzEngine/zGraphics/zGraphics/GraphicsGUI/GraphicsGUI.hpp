#pragma once

namespace zzz{
template<typename T>
class GraphicsGUI {
public:
  //true: succeed, false: failed
  bool Init(T *renderer) {
    renderer_=renderer;
    return InitData();
  }
  virtual bool Terminate(){return true;}

  virtual bool InitData(){return true;}

  //true: handled by GUI, false: not handled by GUI
  virtual bool OnMouseMove(unsigned int /*nFlags*/,int /*x*/,int/*y*/){return false;}
  virtual bool OnLButtonDown(unsigned int/*nFlags*/,int/*x*/,int/*y*/){return false;}
  virtual bool OnLButtonUp(unsigned int/*nFlags*/,int/*x*/,int/*y*/){return false;}
  virtual bool OnRButtonDown(unsigned int/*nFlags*/,int/*x*/,int/*y*/){return false;}
  virtual bool OnRButtonUp(unsigned int/*nFlags*/,int/*x*/,int/*y*/){return false;}
  virtual bool OnMButtonDown(unsigned int/*nFlags*/,int/*x*/,int/*y*/){return false;}
  virtual bool OnMButtonUp(unsigned int/*nFlags*/,int/*x*/,int/*y*/){return false;}
  virtual bool OnMouseWheel(unsigned int/*nFlags*/, int /*zDelta*/, int/*x*/,int/*y*/){return false;}
  virtual bool OnChar(unsigned int /*nChar*/, unsigned int /*nRepCnt*/, unsigned int/*nFlags*/){return false;}
  virtual bool OnKeyDown(unsigned int /*nChar*/, unsigned int /*nRepCnt*/, unsigned int/*nFlags*/){return false;}
  virtual bool OnKeyUp(unsigned int /*nChar*/, unsigned int /*nRepCnt*/, unsigned int/*nFlags*/){return false;}

  virtual void Draw(){};
  virtual void OnSize(unsigned int /*nType*/, int /*cx*/, int /*cy*/){}

protected:
  T *renderer_;
};

}
