#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zGraphics/zGraphicsConfig.hpp>

#include "GraphicsGUI.hpp"
#ifdef ZZZ_LIB_ANTTWEAKBAR
#include <AntTweakBar.h>

namespace zzz{
class Renderer;
class ZGRAPHICS_CLASS AntTweakBarGUI : public GraphicsGUI<Renderer>
{
public:
  AntTweakBarGUI();
  virtual bool Terminate();

  //true: handled by GUI, false: not handled by GUI
  virtual bool OnMouseMove(unsigned int nFlags,int x,int y);
  virtual bool OnLButtonDown(unsigned int nFlags,int x,int y);
  virtual bool OnLButtonUp(unsigned int nFlags,int x,int y);
  virtual bool OnRButtonDown(unsigned int nFlags,int x,int y);
  virtual bool OnRButtonUp(unsigned int nFlags,int x,int y);
  virtual bool OnMButtonDown(unsigned int nFlags,int x,int y);
  virtual bool OnMButtonUp(unsigned int nFlags,int x,int y);
  virtual bool OnMouseWheel(unsigned int nFlags, int zDelta, int x,int y);
  virtual bool OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
  
  virtual void Draw();
  virtual void OnSize(unsigned int nType, int cx, int cy);

private:
  int ZZZKeyToTwKey(unsigned int nChar);
  int ZZZFlagToTwModifier(unsigned int nFlags);
};
}

#define MAKE_TW_CALL(c,f) void TW_CALL Call##f(void *clientData) {reinterpret_cast<c*>(clientData)->f();}

#define MAKE_TW_SET(c,f,t) void TW_CALL Call##f(const void *x, void *clientData) {reinterpret_cast<c*>(clientData)->f(reinterpret_cast<const t*>(x));}

#define MAKE_TW_GET(c,f,t) void TW_CALL Call##f(void *x, void *clientData) {reinterpret_cast<c*>(clientData)->f(reinterpret_cast<t*>(x));}

#endif