#include "AntTweakBarGUI.hpp"
#include "../Renderer/Renderer.hpp"
#ifdef ZZZ_LIB_ANTTWEAKBAR
namespace zzz{
bool AntTweakBarGUI::Terminate() {
  return TwTerminate()!=0;
}

bool AntTweakBarGUI::OnMouseMove(unsigned int nFlags,int x,int y) {
  bool ret=(TwMouseMotion(x,y)!=0);
  if (ret) renderer_->Redraw();
  return ret;
}

bool AntTweakBarGUI::OnLButtonDown(unsigned int nFlags,int x,int y) {
  bool ret=(TwMouseButton(TW_MOUSE_PRESSED,TW_MOUSE_LEFT)!=0);
  if (ret) renderer_->Redraw();
  return ret;
}

bool AntTweakBarGUI::OnLButtonUp(unsigned int nFlags,int x,int y) {
  bool ret=(TwMouseButton(TW_MOUSE_RELEASED,TW_MOUSE_LEFT)!=0);
  if (ret) renderer_->Redraw();
  return ret;
}

bool AntTweakBarGUI::OnRButtonDown(unsigned int nFlags,int x,int y) {
  bool ret=(TwMouseButton(TW_MOUSE_PRESSED,TW_MOUSE_RIGHT)!=0);
  if (ret) renderer_->Redraw();
  return ret;
}

bool AntTweakBarGUI::OnRButtonUp(unsigned int nFlags,int x,int y) {
  bool ret=(TwMouseButton(TW_MOUSE_RELEASED,TW_MOUSE_RIGHT)!=0);
  if (ret) renderer_->Redraw();
  return ret;
}

bool AntTweakBarGUI::OnMButtonDown(unsigned int nFlags,int x,int y) {
  bool ret=(TwMouseButton(TW_MOUSE_PRESSED,TW_MOUSE_MIDDLE)!=0);
  if (ret) renderer_->Redraw();
  return ret;
}

bool AntTweakBarGUI::OnMButtonUp(unsigned int nFlags,int x,int y) {
  bool ret=(TwMouseButton(TW_MOUSE_RELEASED,TW_MOUSE_MIDDLE)!=0);
  if (ret) renderer_->Redraw();
  return ret;
}

bool AntTweakBarGUI::OnMouseWheel(unsigned int nFlags, int zDelta, int x,int y) {
  static int wheel_pos = 0;
  if (zDelta > 0) ++wheel_pos;
  else --wheel_pos;
  bool ret=(TwMouseWheel(wheel_pos)!=0);
  if (ret) renderer_->Redraw();
  return ret;
}

bool AntTweakBarGUI::OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags) {
  bool ret=(TwKeyPressed(ZZZKeyToTwKey(nChar), ZZZFlagToTwModifier(nFlags))!=0);
  if (ret) renderer_->Redraw();
  return ret;
}

void AntTweakBarGUI::OnSize(unsigned int nType, int cx, int cy) {
  TwWindowSize(cx,cy);
}

void AntTweakBarGUI::Draw() {
  TwDraw();
}

AntTweakBarGUI::AntTweakBarGUI() {
  if (!TwInit(TW_OPENGL, NULL)) {
    ZLOGE << "AntTweakBar initialization failed: " << TwGetLastError() << endl;
  }
}

int AntTweakBarGUI::ZZZKeyToTwKey(unsigned int nChar) {
  switch(nChar) {
  case ZZZKEY_BACK: return TW_KEY_BACKSPACE;
  case ZZZKEY_TAB: return TW_KEY_TAB;
  case ZZZKEY_RETURN: return TW_KEY_RETURN;
  case ZZZKEY_PAUSE: return TW_KEY_PAUSE;
  case ZZZKEY_ESC: return TW_KEY_ESCAPE;
  case ZZZKEY_SPACE: return TW_KEY_SPACE;
  case ZZZKEY_DELETE: return TW_KEY_DELETE;
  case ZZZKEY_UP: return TW_KEY_UP;
  case ZZZKEY_DOWN: return TW_KEY_DOWN;
  case ZZZKEY_RIGHT: return TW_KEY_RIGHT;
  case ZZZKEY_LEFT: return TW_KEY_LEFT;
  case ZZZKEY_INSERT: return TW_KEY_INSERT;
  case ZZZKEY_HOME: return TW_KEY_HOME;
  case ZZZKEY_END: return TW_KEY_END;
  case ZZZKEY_PAGEUP: return TW_KEY_PAGE_UP;
  case ZZZKEY_PAGEDOWN: return TW_KEY_PAGE_DOWN;
  case ZZZKEY_F1: return TW_KEY_F1;
  case ZZZKEY_F2: return TW_KEY_F2;
  case ZZZKEY_F3: return TW_KEY_F3;
  case ZZZKEY_F4: return TW_KEY_F4;
  case ZZZKEY_F5: return TW_KEY_F5;
  case ZZZKEY_F6: return TW_KEY_F6;
  case ZZZKEY_F7: return TW_KEY_F7;
  case ZZZKEY_F8: return TW_KEY_F8;
  case ZZZKEY_F9: return TW_KEY_F9;
  case ZZZKEY_F10: return TW_KEY_F10;
  case ZZZKEY_F11: return TW_KEY_F11;
  case ZZZKEY_F12: return TW_KEY_F12;
  case ZZZKEY_F13: return TW_KEY_F13;
  case ZZZKEY_F14: return TW_KEY_F14;
  case ZZZKEY_F15: return TW_KEY_F15;
  default: return nChar;
  }
}

int AntTweakBarGUI::ZZZFlagToTwModifier(unsigned int nFlags) {
  int x = TW_KMOD_NONE;
  if (CheckBit(nFlags, ZZZFLAG_SHIFT))
    SetBit(x, TW_KMOD_SHIFT);
  if (CheckBit(nFlags, ZZZFLAG_CTRL))
    SetBit(x, TW_KMOD_CTRL);
  if (CheckBit(nFlags, ZZZFLAG_ALT))
    SetBit(x, TW_KMOD_ALT);
  return x;
}

}
#endif
