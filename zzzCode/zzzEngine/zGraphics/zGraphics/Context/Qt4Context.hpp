#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "../zGraphicsConfig.hpp"
#include "Context.hpp"
#ifdef ZZZ_CONTEXT_QT4
#include "../Renderer/Renderer.hpp"

#pragma warning(disable:4311) //reinterpret_cast
#pragma warning(disable:4312) //reinterpret_cast

#include <QtOpenGL/QGLWidget>
#include <QtGui/QMouseEvent>
#include <QtGui/QWheelEvent>
#include <QtGui/QKeyEvent>

namespace zzz{
class ZGRAPHICS_CLASS zQt4GLWidget : public QGLWidget, public Context
{
public:
  zQt4GLWidget(Renderer *renderer);
protected:
  void initializeGL();
  void resizeGL(int width, int height);
  void paintGL();
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
#ifndef QT_NO_WHEELEVENT
  void wheelEvent(QWheelEvent *);
#endif
  void keyPressEvent(QKeyEvent *);
  void keyReleaseEvent(QKeyEvent *);

//Context interface
public:
  void Redraw() {
    this->update();
  }
  void SwapBuffer() {
    this->swapBuffers();
  }
  void HideCursor() {
    this->setCursor(QCursor(Qt::BlankCursor));
  }
  void ShowCursor() {
    this->setCursor(QCursor(Qt::ArrowCursor));
  }
  void MakeCurrent() {
    this->makeCurrent();
    Context::MakeCurrent();
  }

  static unsigned int QT4Key2ZZZKey(int key);
  unsigned int GetInputFlag();
};  

}

#endif
