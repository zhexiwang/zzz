#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "Context.hpp"
#ifdef ZZZ_CONTEXT_SFML
#include <SFML/Graphics.hpp>

#ifdef ZZZ_DEBUG
#pragma comment(lib,"sfml-window-s-d.lib")
#pragma comment(lib,"sfml-graphics-s-d.lib")
#pragma comment(lib,"sfml-system-s-d.lib")
#else
#pragma comment(lib,"sfml-window-s.lib")
#pragma comment(lib,"sfml-graphics-s.lib")
#pragma comment(lib,"sfml-system-s.lib")
#endif

#pragma comment(lib,"libpng.lib")
#pragma comment(lib,"libjpeg.lib")

namespace zzz{

class ZGRAPHICS_CLASS SFMLContext : public Context {
public:
  SFMLContext(void);
  ~SFMLContext(void);

  void Initialize(Renderer* renderer, const string &title=0, int sizex=600, int sizey=600);
  void Redraw();
  void SwapBuffer();
  void MakeCurrent();

private:
  sf::RenderWindow *App_;
  bool needRedraw_;
  static unsigned int SFMLKey2ZZZKey(sf::Key::Code&);
  unsigned int GetInputFlag();
};

}
#endif