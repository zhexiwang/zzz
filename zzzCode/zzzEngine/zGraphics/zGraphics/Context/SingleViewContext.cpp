#define ZGRAPHICS_SOURCE
#ifdef ZZZ_CONTEXT_MFC

#ifdef _LIB
#define _AFXDLL
#endif
#ifndef WINVER
//#define WINVER 0x0600//VISTA
//#define WINVER 0x0502//2003
#define WINVER 0x0501//XP
#endif
#include <afxwin.h>

#include "SingleViewContext.hpp"
#include "../Resource/Shader/ShaderSpecify.hpp"
#include "../Graphics/OpenGLTools.hpp"

namespace zzz{


bool SingleViewContext::Initialize(HWND hWnd)
{
  hWnd_=hWnd;
  renderer_->SetContext(this);
#ifdef ZZZ_OPENGL_MX
  OpenGLMutex.Lock();
#endif // ZZZ_OPENGL_MX
  GLinited_=InitGL();
  InitGLEW();
  renderer_->InitState();
  renderer_->InitData();
#ifdef ZZZ_OPENGL_MX
  OpenGLMutex.Unlock();
#endif // ZZZ_OPENGL_MX

  //Init GraphicsGUI
  if (gui_) gui_->Init(renderer_);
  return TRUE;
}

bool SingleViewContext::InitGL()
{
  hDC_=::GetDC(hWnd_);
  if (hDC_==NULL)
  {
    printf("[CGLContext::Init] error getting hDC from hWnd.\n");
    return false;
  }
  SetupPixelFormat();
  hRC_=::wglCreateContext(hDC_);
  wglMakeCurrent(hDC_,hRC_);

  //TODO: how to turn on AA
//   int pf=InitMultisample();
//   if (pf!=0)
//   {
//     BOOL b1=::wglMakeCurrent(lastDC,lastRC);
//     BOOL b2=wglDeleteContext (hRC_);
//     BOOL b3=ReleaseDC(v->GetSafeHwnd(),pDC_->GetSafeHdc());
//     delete pDC_;
//     pDC_=0;hRC_=0;
//     pDC_=new CClientDC(v);   //获取客户区的设备描述表
//     SetupPixelFormat(pf);   //首先把DC的象素格式调整为指定的格式，以便后面对DC的使用
//     hRC_=::wglCreateContext(pDC_->GetSafeHdc());   //根据DC来创建RC
//     ::wglMakeCurrent(pDC_->GetSafeHdc(),hRC_);   //设置当前的RC，以后的画图操作都画在pDC_指向的DC上
//   }

  renderer_->InitState();

  glGetIntegerv(GL_DRAW_BUFFER, &DefaultDrawBuffer_); //save default drawbuffer
  int viewports[4]; //save window size
  glGetIntegerv(GL_VIEWPORT,viewports);
  renderer_->width_=viewports[2];
  renderer_->height_=viewports[3];

  return true;
}

int SingleViewContext::InitMultisample()
{
  int iAttributes[] = { WGL_DRAW_TO_WINDOW_ARB,GL_TRUE,
    WGL_SUPPORT_OPENGL_ARB,GL_TRUE,
    WGL_ACCELERATION_ARB,WGL_FULL_ACCELERATION_ARB,
    WGL_COLOR_BITS_ARB,24,
    WGL_ALPHA_BITS_ARB,8,
    WGL_DEPTH_BITS_ARB,16,
    WGL_STENCIL_BITS_ARB,0,
    WGL_DOUBLE_BUFFER_ARB,GL_TRUE,
    WGL_SAMPLE_BUFFERS_ARB,GL_TRUE,
    WGL_SAMPLES_ARB, 4 ,            // Check For 4x Multisampling
    0,0};
  float fAttributes[] = {0,0};
  int pixelFormat;
  zuint numFormats;
  BOOL valid = wglChoosePixelFormatARB(hDC_,iAttributes,fAttributes,1,&pixelFormat,&numFormats);

  // If We Returned True, And Our Format Count Is Greater Than 1
  if (valid && numFormats >= 1)
  {
    printf("Support 4X Multisample!\n");
    return pixelFormat;
  }

  // Our Pixel Format With 4 Samples Failed, Test For 2 Samples
  iAttributes[19] = 2;
  valid = wglChoosePixelFormatARB(hDC_,iAttributes,fAttributes,1,&pixelFormat,&numFormats);
  if (valid && numFormats >= 1)
  {
    printf("Support 2X Multisample!\n");
    return pixelFormat;
  }
  printf("Don't support Multisample!\n");

  return 0;

}

bool SingleViewContext::SetupPixelFormat(int pf/*=0*/)
{
  PIXELFORMATDESCRIPTOR pfd = {
    sizeof(PIXELFORMATDESCRIPTOR),
    1,
    PFD_DRAW_TO_WINDOW |
    PFD_SUPPORT_OPENGL |
    PFD_DOUBLEBUFFER,
    PFD_TYPE_RGBA,
    24,
    0, 0, 0, 0, 0, 0,
    8,
    0,
    0,
    0, 0, 0, 0,
    32,
    8,
    0,
    PFD_MAIN_PLANE,
    0,
    0, 0, 0
  };
  int pixelformat=(pf==0)?ChoosePixelFormat(hDC_,&pfd):pf;
  if (pixelformat==0)
  {
    printf("no matched pixelformat!\n");
    return FALSE;
  }
  if (SetPixelFormat(hDC_,pixelformat,&pfd)==FALSE)
  {
    printf("can't set specified pixelformat!\n");
    return FALSE;
  }

  return TRUE;

}

SingleViewContext::~SingleViewContext()
{
  if (hRC_)
  {
    ::wglDeleteContext(hRC_);
    ::ReleaseDC(hWnd_,hDC_);
  }
}

void MFC2ZZZ(unsigned int &nChar, unsigned int &nFlags)
{
  unsigned int oldFlags=nFlags;
  nFlags=0;
  if (CheckBit(oldFlags,(zuint)1<<29)) SetBit(nFlags,ZZZKEY_ALT);
  if (CheckBit(oldFlags,(zuint)1<<30)) SetBit(nFlags,ZZZKEY_CTRL);
  switch (oldFlags)
  {
  case 284: nChar=ZZZKEY_NUM_ENTER; break;
  case 82: nChar=ZZZKEY_NUM_0; break;
  case 79: nChar=ZZZKEY_NUM_1; break;
  case 80: nChar=ZZZKEY_NUM_2; break;
  case 81: nChar=ZZZKEY_NUM_3; break;
  case 75: nChar=ZZZKEY_NUM_4; break;
  case 76: nChar=ZZZKEY_NUM_5; break;
  case 77: nChar=ZZZKEY_NUM_6; break;
  case 71: nChar=ZZZKEY_NUM_7; break;
  case 72: nChar=ZZZKEY_NUM_8; break;
  case 73: nChar=ZZZKEY_NUM_9; break;
  case 309: nChar=ZZZKEY_NUM_DIVIDE; break;
  case 55: nChar=ZZZKEY_NUM_TIMES; break;
  case 74: nChar=ZZZKEY_NUM_MINUS; break;
  case 78: nChar=ZZZKEY_NUM_PLUS; break;
  case 83: nChar=ZZZKEY_NUM_DOT; break;
  }
  
}
}

#endif