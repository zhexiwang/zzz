#pragma once
#include <zCore/Utility/Tools.hpp>

namespace zzz {
//for OnMouse...
#define ZZZFLAG_LMOUSE  (0x00000001)
#define ZZZFLAG_RMOUSE  (0x00000002)
#define ZZZFLAG_MMOUSE  (0x00000004)
#define ZZZFLAG_X1MOUSE (0x00000008)
#define ZZZFLAG_X2MOUSE (0x00000010)

#define ZZZFLAG_SHIFT  (0x00010000)
#define ZZZFLAG_CTRL  (0x00020000)
#define ZZZFLAG_ALT    (0x00040000)
#define ZZZFLAG_META  (0x00080000)

#define ZZZFLAG_LSHIFT  (0x00100000)
#define ZZZFLAG_LCTRL  (0x00200000)
#define ZZZFLAG_LALT  (0x00400000)
#define ZZZFLAG_LMETA  (0x00800000)
#define ZZZFLAG_RSHIFT  (0x01000000)
#define ZZZFLAG_RCTRL  (0x02000000)
#define ZZZFLAG_RALT  (0x04000000)
#define ZZZFLAG_RMETA  (0x08000000)

inline void CompleteZZZFlags(unsigned int &flag)
{
  if (CheckBit(flag, ZZZFLAG_LSHIFT) | CheckBit(flag, ZZZFLAG_RSHIFT)) SetBit(flag,ZZZFLAG_SHIFT);
  if (CheckBit(flag, ZZZFLAG_LCTRL) | CheckBit(flag, ZZZFLAG_RCTRL)) SetBit(flag,ZZZFLAG_CTRL);
  if (CheckBit(flag, ZZZFLAG_LALT) | CheckBit(flag, ZZZFLAG_RALT)) SetBit(flag,ZZZFLAG_ALT);
  if (CheckBit(flag, ZZZFLAG_LMETA) | CheckBit(flag, ZZZFLAG_RMETA)) SetBit(flag,ZZZFLAG_META);
}

//OnKeyDown,OnChar
#define ZZZKEY_NOKEY (0)

#define ZZZKEY_1 ('1')
#define ZZZKEY_2 ('2')
#define ZZZKEY_3 ('3')
#define ZZZKEY_4 ('4')
#define ZZZKEY_5 ('5')
#define ZZZKEY_6 ('6')
#define ZZZKEY_7 ('7')
#define ZZZKEY_8 ('8')
#define ZZZKEY_9 ('9')
#define ZZZKEY_0 ('0')

#define ZZZKEY_A ('A')
#define ZZZKEY_B ('B')
#define ZZZKEY_C ('C')
#define ZZZKEY_D ('D')
#define ZZZKEY_E ('E')
#define ZZZKEY_F ('F')
#define ZZZKEY_G ('G')
#define ZZZKEY_H ('H')
#define ZZZKEY_I ('I')
#define ZZZKEY_J ('J')
#define ZZZKEY_K ('K')
#define ZZZKEY_L ('L')
#define ZZZKEY_M ('M')
#define ZZZKEY_N ('N')
#define ZZZKEY_O ('O')
#define ZZZKEY_P ('P')
#define ZZZKEY_Q ('Q')
#define ZZZKEY_R ('R')
#define ZZZKEY_S ('S')
#define ZZZKEY_T ('T')
#define ZZZKEY_U ('U')
#define ZZZKEY_V ('V')
#define ZZZKEY_W ('W')
#define ZZZKEY_X ('X')
#define ZZZKEY_Y ('Y')
#define ZZZKEY_Z ('Z')

#define ZZZKEY_a ('a')
#define ZZZKEY_b ('b')
#define ZZZKEY_c ('c')
#define ZZZKEY_d ('d')
#define ZZZKEY_e ('e')
#define ZZZKEY_f ('f')
#define ZZZKEY_g ('g')
#define ZZZKEY_h ('h')
#define ZZZKEY_i ('i')
#define ZZZKEY_j ('j')
#define ZZZKEY_k ('k')
#define ZZZKEY_l ('l')
#define ZZZKEY_m ('m')
#define ZZZKEY_n ('n')
#define ZZZKEY_o ('o')
#define ZZZKEY_p ('p')
#define ZZZKEY_q ('q')
#define ZZZKEY_r ('r')
#define ZZZKEY_s ('s')
#define ZZZKEY_t ('t')
#define ZZZKEY_u ('u')
#define ZZZKEY_v ('v')
#define ZZZKEY_w ('w')
#define ZZZKEY_x ('x')
#define ZZZKEY_y ('y')
#define ZZZKEY_z ('z')

#define ZZZKEY_MINUS ('-')
#define ZZZKEY_UL ('_')  //underline
#define ZZZKEY_EQUAL ('=')
#define ZZZKEY_PLUS ('+')
#define ZZZKEY_LSB ('[')  //left square bracket
#define ZZZKEY_RSB (']')  //right square bracket
#define ZZZKEY_LB ('{')    //left brace
#define ZZZKEY_RB ('}')    //right brace
#define ZZZKEY_SCOLON ('; ')    //Semi Colon
#define ZZZKEY_COLON (':')    //Colon
#define ZZZKEY_SQM ('\'')  //Single Quotation mark
#define ZZZKEY_DQM ('\"')  //Double Quotation mark
#define ZZZKEY_COMMA (',')
#define ZZZKEY_LT ('<')  //Less Than
#define ZZZKEY_DOT ('.')
#define ZZZKEY_GT ('>')  //Grater Than
#define ZZZKEY_SLASH ('/')
#define ZZZKEY_QM ('?')  //Question Mark
#define ZZZKEY_BSLASH ('\\')
#define ZZZKEY_VB ('|')  //Vertical Bar
#define ZZZKEY_SPACE (' ')
#define ZZZKEY_BQ ('`')  //Back Quotation
#define ZZZKEY_TILDE ('~')
#define ZZZKEY_EM ('!')  //Exclamation Mark
#define ZZZKEY_AT ('@')
#define ZZZKEY_NS ('#') //Number Sign
#define ZZZKEY_DOLLAR ('$')
#define ZZZKEY_PERCENT ('%')
#define ZZZKEY_CARET ('^')
#define ZZZKEY_AMPERSAND ('&')
#define ZZZKEY_ASTERISK ('*')
#define ZZZKEY_LP ('(') //Left Parenthesis
#define ZZZKEY_RP (')') //Right Parenthesis

#define ZZZKEY_ESC (27)
#define ZZZKEY_ESCAPE (27)
#define ZZZKEY_TAB (9)
#define ZZZKEY_BACK (8)
#define ZZZKEY_ENTER (13)
#define ZZZKEY_RETURN (13)

#define ZZZKEY_BASE_FUNC (256)
#define ZZZKEY_CAPSLOCK (259)
#define ZZZKEY_SCROLLLOCK (260)
#define ZZZKEY_PAUSE (261)
#define ZZZKEY_PRINT (262)

#define ZZZKEY_F_KEY (290)
#define ZZZKEY_F1 (291)
#define ZZZKEY_F2 (292)
#define ZZZKEY_F3 (293)
#define ZZZKEY_F4 (294)
#define ZZZKEY_F5 (295)
#define ZZZKEY_F6 (296)
#define ZZZKEY_F7 (297)
#define ZZZKEY_F8 (298)
#define ZZZKEY_F9 (299)
#define ZZZKEY_F10 (300)
#define ZZZKEY_F11 (301)
#define ZZZKEY_F12 (302)
#define ZZZKEY_F13 (303)
#define ZZZKEY_F14 (304)
#define ZZZKEY_F15 (305)

#define ZZZKEY_FUNC (310)
#define ZZZKEY_INSERT (311)
#define ZZZKEY_DELETE (312)
#define ZZZKEY_HOME (313)
#define ZZZKEY_END (314)
#define ZZZKEY_PAGEUP (315)
#define ZZZKEY_PAGEDOWN (316)

#define ZZZKEY_DIR (320)
#define ZZZKEY_UP (321)
#define ZZZKEY_DOWN (322)
#define ZZZKEY_LEFT (323)
#define ZZZKEY_RIGHT (324)

#define ZZZKEY_NUM_KEY (330)
#define ZZZKEY_NUM_LOCK (331)
#define ZZZKEY_NUM_PLUS (332)
#define ZZZKEY_NUM_MINUS (333)
#define ZZZKEY_NUM_TIMES (334)
#define ZZZKEY_NUM_DIVIDE (335)
#define ZZZKEY_NUM_1 (336)
#define ZZZKEY_NUM_2 (337)
#define ZZZKEY_NUM_3 (338)
#define ZZZKEY_NUM_4 (339)
#define ZZZKEY_NUM_5 (340)
#define ZZZKEY_NUM_6 (341)
#define ZZZKEY_NUM_7 (342)
#define ZZZKEY_NUM_8 (343)
#define ZZZKEY_NUM_9 (344)
#define ZZZKEY_NUM_0 (345)
#define ZZZKEY_NUM_DOT (346)
#define ZZZKEY_NUM_ENTER (347)


#define ZZZKEY_CTRL (400)
#define ZZZKEY_ALT (410)
#define ZZZKEY_SHIFT (420)

#define ZZZKEY_META (500)  //WIN for Windows and CMD for Mac
#define ZZZKEY_MENU (510)

#define ZZZKEY_LCTRL (401)
#define ZZZKEY_RCTRL (402)
#define ZZZKEY_LALT (411)
#define ZZZKEY_RALT (412)
#define ZZZKEY_LSHIFT (421)
#define ZZZKEY_RSHIFT (422)

#define ZZZKEY_LMETA (501)
#define ZZZKEY_RMETA (502)

}
