#pragma once
#include "../../common.hpp"
#include "../../Graphics/Box2.hpp"
#include "../../Renderer/Renderer.hpp"

namespace zzz{
struct MultiContextDescriptorBase
{
	float m_ratio; //width / height
	Box2<int> m_box;
};

struct MultiContextDescriptor : public MultiContextDescriptorBase
{
	Renderer *m_renderer;
};

struct MultiContextHSplitor : public MultiContextDescriptor
{
public:
	vector<MultiContextDescriptor*> m_desc;
	void AddDescriptor(MultiContextDescriptor *desc)
	{
		m_desc.push_back(desc);
	}
	void Clear()
	{
		m_desc.clear();
	}
};

struct MultiContextVSplitor : public MultiContextDescriptor
{
	vector<MultiContextDescriptor*> m_desc;
	void AddDescriptor(MultiContextDescriptor *desc)
	{
		m_desc.push_back(desc);
	}
	void Clear()
	{
		m_desc.clear();
	}
};

}