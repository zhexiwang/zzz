#pragma once

// Separate link so when change a lib, only link needs redo.
#include "zGraphicsConfig.hpp"
#include "zGraphicsAutoLink3rdParty.hpp"

#ifndef ZZZ_NO_PRAGMA_LIB

#ifdef ZZZ_COMPILER_MSVC

#ifdef ZZZ_DYNAMIC
  #ifdef ZZZ_DEBUG
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zGraphicsdllD.lib")
    #else
      #pragma comment(lib,"zGraphicsdllD_x64.lib")
    #endif // ZZZ_OS_WIN64
  #else
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zGraphicsdll.lib")
    #else
      #pragma comment(lib,"zGraphicsdll_x64.lib")
    #endif // ZZZ_OS_WIN64
  #endif
#else
  #ifdef ZZZ_DEBUG
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zGraphicsD.lib")
    #else
      #pragma comment(lib,"zGraphicsD_x64.lib")
    #endif // ZZZ_OS_WIN64
  #else
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zGraphics.lib")
    #else
      #pragma comment(lib,"zGraphics_x64.lib")
    #endif // ZZZ_OS_WIN64
  #endif
#endif

#endif // ZZZ_COMPILER_MSVC

#endif  // ZZZ_NO_PRAGMA_LIB
