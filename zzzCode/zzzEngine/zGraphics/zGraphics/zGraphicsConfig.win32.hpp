#pragma once

//#define ZZZ_CONTEXT_MFC

//#define ZZZ_CONTEXT_GLUI

#ifdef ZZZ_LIB_QT4
#define ZZZ_CONTEXT_QT4
#endif

//#define ZZZ_OPENMESH

#ifdef ZZZ_DEBUG
#define ENABLE_CHECKGL
#endif

#define GLEW_STATIC

// Multi-Context support, always lock RenderScene, so only one threading is rendering at one time.
// In case context is switched while rendering.
// Not 100% safe, since one can call MakeCurrent() any time.
// Just avoid to call any fundanmental function, let Context class handle them.
#define ZZZ_OPENGL_MX

#define FREEGLUT_STATIC