#include "RT_KdTree.hpp"
#include "RT_Ray.hpp"
#include "../Graphics/AABB.hpp"
#include <zCore/Utility/Timer.hpp>

namespace zzz {
template <typename T>
inline int Log2i(const T& x)
{
  int k =	static_cast<int>(x);
  int i =	0;
  while (k>1) {
    k >>= 1;
    i++;
  }
  return i;
}

struct BoundEdge {
public:
  BoundEdge(float tt, int pn, bool starting) 
    :t_(tt), triIndex_(pn), type_(starting?START:END)
  {
  }

  bool operator < (const BoundEdge& e) const
  {
    if (t_ == e.t_)
      return (int)type_ < (int)e.type_;
    else
      return t_ < e.t_;
  }

public:
  float t_;
  int triIndex_;
  enum {START, END} type_;
};

RT_KdTree::RT_KdTree()
{
  Clear();
}

void RT_KdTree::Clear()
{
  mbe = 0.5f ; // bonus param of be ,in the range of [0, 1]
  mCostTt = 1.f;  // cost on traverse the interior node and decide ray partition method, i set it as 1.0f
  mCostTi = 80.f;  // cost on intersect with a elem ,say a triangle, i set is a 80.0f; 

  //when the interior node's depth exceed the max depth, or it contains no more than minElems elems, it should become a RT_KdLeaf
  //maxDepth = 100; // generally a kdtree should no more than 100-200 in depth
  maxDepth = -1;
  minElems = 1;  //这个参数的设置值得再商榷

  mNodes.clear();
  mLeafs.clear();
  faces_.clear();
  points_.clear();
  mPrimList.clear();  //for each leaf, corresponding a prim bounding box 

  mLeafs.push_back(RT_KdLeaf()); //skip one leaf
  mPrimList.push_back(AABB3f()); //skip one primitive

  mCurNodeIndex = 0; 	//start from 0
  mCurLeafIndex = 1;  //start from 1: because i use negative integer to express the leaf index

  root = 0;
}

int RT_KdTree::CreateLeaf(const AABB3f &leafBounds, const vector<int> &mTris, int numTris)
{
  RT_KdLeaf leaf;
  leaf.elems.assign(mTris.begin(), mTris.end());
  leaf.nElems = numTris;

  AABB3f leafPrimBox;
  for (int i = 0; i<numTris; ++i) {
    leafPrimBox += GetTriangleAABB(mTris[i]);
  }

  mPrimList.push_back(leafPrimBox);
  leaf.prim = mPrimList.size() - 1;
  mLeafs.push_back(leaf);
  mCurLeafIndex = mPrimList.size() -1;

  return -mCurLeafIndex;
}

// BuildTree: return current node's index,or current leaf's negative index
int RT_KdTree::BuildTree(const AABB3f& nodeBounds, vector<int> &mTris, int numTris ,int depth)
{
  // initialize leaf nodes if termination criteria met
  if (numTris <= minElems || depth >= maxDepth) {
    int leafIndex = this->CreateLeaf(nodeBounds, mTris, numTris); //return created leaf's index
    return leafIndex;
  }

  //or else initialize interior nodes and continue recursively
  RT_KdNode newNode ;
  newNode.nElems = numTris ; //contains numTris triangles

  //decide the split axis
  Vector3f range = nodeBounds.Diff();
  switch(range.MaxPos()) {
  case 0: newNode.splitFlag = RT_KdNode::AXIS_X; break;
  case 1: newNode.splitFlag = RT_KdNode::AXIS_Y; break;
  case 2: newNode.splitFlag = RT_KdNode::AXIS_Z; break;
  default: ZLOGF<<"This should not happen!";
  }


  Vector3f d = nodeBounds.Diff();
  float SA = 2.f * (d[0] * d[1] + d[1] * d[2] + d[2] * d[0]);
  float invTotalSA = 1.f / SA;

  std::vector<BoundEdge> candidateEdges;
  float commonArea, sideArea;  
  float start, end;  //start and end pos along the split axis
  AABB3f primBox;
  switch (newNode.splitFlag)
  {
  case RT_KdNode::AXIS_X:
    for (int i =0 ; i < numTris; ++i) {
      primBox = GetTriangleAABB(mTris[i]);
      candidateEdges.push_back(BoundEdge(primBox.Min(0), mTris[i], true));
      candidateEdges.push_back(BoundEdge(primBox.Max(0), mTris[i], false));
    }
    start = nodeBounds.Min(0);
    end = nodeBounds.Max(0);
    commonArea = 2.f * (d[0] * d[1] + d[0] * d[2]);
    sideArea = d[1] * d[2];
    break;
  case RT_KdNode::AXIS_Y:
    for (int i =0 ; i < numTris; ++i) {
      primBox = GetTriangleAABB(mTris[i]);
      candidateEdges.push_back(BoundEdge(primBox.Min(1), mTris[i], true));
      candidateEdges.push_back(BoundEdge(primBox.Max(1), mTris[i], false));
    }
    start = nodeBounds.Min(1);
    end = nodeBounds.Max(1);
    commonArea = 2.f * (d[1] * d[0] + d[1] * d[2]);
    sideArea = d[0] * d[2];
    break;
  case RT_KdNode::AXIS_Z:
    for (int i =0 ; i < numTris; ++i) {
      primBox = GetTriangleAABB(mTris[i]);
      candidateEdges.push_back(BoundEdge(primBox.Min(2), mTris[i], true));
      candidateEdges.push_back(BoundEdge(primBox.Max(2), mTris[i], false));
    }
    start = nodeBounds.Min(2);
    end = nodeBounds.Max(2);
    commonArea = 2.f * (d[2] * d[0] + d[2] * d[1]);
    sideArea = d[0] * d[1];
    break;
  }
  sort(candidateEdges.begin(), candidateEdges.end());

  float minCost = 2 * (numTris) * mCostTi;  // initialize min to the state of no division plane cost
  bool findSplit = false;
  float candiCost = 0.0f;
  float axisStride = end - start; //nodeBounds.Max()[newNode.splitFlag] - nodeBounds.Min()[newNode.splitFlag];
  int balanceLeft = 0, balanceRight = 0;

  int leftCount = 0, rightCount = numTris;
  int bestOffset = 0;
  for (int i = 0; i < 2 * numTris; ++i) {
    if (candidateEdges[i].type_ == BoundEdge::END) 
      rightCount--;
    float candiSplit = candidateEdges[i].t_;
    if (candiSplit > nodeBounds.Min(newNode.splitFlag) && candiSplit < nodeBounds.Max(newNode.splitFlag)) {
      //compute cost for split at ith edge
      float splitPencent = (candiSplit - start) / axisStride;
      float SB =  commonArea * splitPencent + 2.f * sideArea;
      float SC =  commonArea * (1.f - splitPencent) + 2.f * sideArea;

      if (rightCount == 0)
        candiCost = mCostTt + mCostTi * (SB * leftCount + SC * rightCount) * (1 - mbe) * invTotalSA; //bonus for one empty side
      else  
        candiCost = mCostTt + mCostTi * (SB * leftCount + SC * rightCount) * invTotalSA;
      if (candiCost < minCost) {
        minCost = candiCost;
        newNode.split = candiSplit;
        findSplit = true;
        bestOffset = i; 
        balanceLeft = leftCount; 
        balanceRight = rightCount;
      }
    }
    if (candidateEdges[i].type_ == BoundEdge::START) 
      leftCount++;
  }

  //if no propieate split plane found, make a leaf node
  if (!findSplit) {
    return this->CreateLeaf(nodeBounds, mTris, numTris);
  }

  //push newNode to mNodes
  mNodes.push_back(newNode); 
  mCurNodeIndex = mNodes.size() - 1;
  int nodeIndex = mCurNodeIndex;

  //partition the triangle index vector
  vector<int> leftList, rightList;
  for (int i = 0; i < numTris; ++i) {
    int numCount = mTris[i];
    int posFlag = TriangleIntersectPlaneAxis(numCount, newNode.split, newNode.splitFlag);
    if (posFlag == 0) {  //below the split plane
      leftList.push_back(numCount); 
    } else if (posFlag == 1) {  //above the split plane
      rightList.push_back(numCount); 
    } else {
      leftList.push_back(numCount);
      rightList.push_back(numCount);
    }
  } 

  candidateEdges.clear();

  AABB3f leftBounds, rightBounds;
  leftBounds.Min() = nodeBounds.Min();
  rightBounds.Max() = nodeBounds.Max();
  switch (newNode.splitFlag) {
  case RT_KdNode::AXIS_X:
    leftBounds.Max() = Vector3f(newNode.split, nodeBounds.Max(1), nodeBounds.Max(2));
    rightBounds.Min() = Vector3f(newNode.split, nodeBounds.Min(1), nodeBounds.Min(2));
    break;
  case RT_KdNode::AXIS_Y:
    leftBounds.Max() = Vector3f(nodeBounds.Max(0), newNode.split, nodeBounds.Max(2));
    rightBounds.Min() = Vector3f(nodeBounds.Min(0), newNode.split, nodeBounds.Min(2));
    break;
  case RT_KdNode::AXIS_Z:
    leftBounds.Max() = Vector3f(nodeBounds.Max(0), nodeBounds.Max(1), newNode.split);
    rightBounds.Min() = Vector3f(nodeBounds.Min(0), nodeBounds.Min(1), newNode.split);
    break;
  }

  mTris.clear();
  //recursively build left tree
  int leftNodeIndex = this->BuildTree(leftBounds, leftList, leftList.size(), depth+1);
  int rightNodeIndex = this->BuildTree(rightBounds, rightList, rightList.size(), depth+1);

  //set parent and left/right child index ,note: the leaf node have a negative index
  //if left child is a interior node
  if (leftNodeIndex > 0) {
    mNodes[leftNodeIndex].parent = nodeIndex; 	
  }	else {
    //if left child is a leaf
    mLeafs[-leftNodeIndex].parent = nodeIndex; 	
  }
  //if right child is a interior node
  if (rightNodeIndex > 0) {
    mNodes[rightNodeIndex].parent = nodeIndex; 	
  } else {
    //if right child is leaf
    mLeafs[-rightNodeIndex].parent = nodeIndex; 	
  }
  mNodes[nodeIndex].left = leftNodeIndex;
  mNodes[nodeIndex].right = rightNodeIndex;

  //return current node index
  return nodeIndex;
}

bool RT_KdTree::Build(const vector<Vector3f> &points, const vector<Vector3i> &faces)
{
  Timer timer;
  ZLOGV<<"Start building RayTracing KDTree with "<<faces.size()<<" triangles...\n";
  points_=points;
  faces_=faces;
  //allocate memory 
  mNodes.reserve(faces_.size() * 2);
  mLeafs.reserve(faces_.size() * 2);
  mPrimList.reserve(mLeafs.size() * 2);

  //set maxDepth adaptively according to the scene size
  if (maxDepth <= 0)
    maxDepth = 8 + 1.3f * Log2i<int>(faces_.size());

  //assume there are at least one triangle in the scene
  mBounds = GetTriangleAABB(0);
  vector<int> tris(faces_.size());
  tris[0] = 0;
  for (zuint i = 1; i < faces_.size(); ++i) {
    mBounds += GetTriangleAABB(i);
    tris[i] = i;
  }

  //build kd tree
  root = 0;
  this->BuildTree(mBounds, tris, faces_.size(), 0);
  ZLOGV<<"Done in "<<timer.Elapsed()<<" seconds.\n";
  return true;
}

bool RT_KdTree::Intersect(RT_Ray& ray, int &f, Vector3f &bary) 
{
  float tmin, tmax;
  if (!ray.IntersectP(mBounds, tmin, tmax)) 
    return false ;

  ray.mint_ = tmin;
  ray.maxt_ = tmax;
  //if no interior node in kd tree,usually this should not happen
  if (mNodes.empty()) {
    if (mLeafs.size() <= 1) 
      return false;
    return LeafIntersect(1, ray, f, bary);
  }
  return NodeIntersect(root, mBounds, ray, f, bary);
}

bool RT_KdTree::DoesIntersect(RT_Ray& ray) 
{
  float tmin, tmax;
  // if not intersected with the whole bounds , then no further test needed
  if (!ray.IntersectP(mBounds, tmin, tmax)) 
    return false ;
  ray.mint_ = tmin;
  ray.maxt_ = tmax;
  //if no interior node in kd tree,usually this should not happen
  if (mNodes.empty()) {
    if (mLeafs.size() <= 1) return false;
    return LeafIntersectP(1, ray);
  }
  return NodeIntersectP(root, mBounds, ray);

}

bool RT_KdTree::NodeIntersect(int index, const AABB3f& nodeBounds, RT_Ray& ray, int &f, Vector3f &bary)
{
  //if leaf node met
  if (index < 0) {
    return LeafIntersect(-index, ray, f, bary);
  }
  float tmin = ray.mint_, tmax = ray.maxt_;
  if (tmin > tmax) 
    return false;

  //prepare to traverse kd-tree for ray
  Vector3f invDir (1.f / ray.dir_[0], 1.f / ray.dir_[1], 1.f / ray.dir_[2]);

  //compute parametric distance along ray to split plane
  int axis = mNodes[index].splitFlag;
  float splitPos = mNodes[index].split;
  float tplane = (splitPos - ray.ori_[axis]) * invDir[axis];

  AABB3f leftBounds, rightBounds;
  leftBounds.Min() = nodeBounds.Min();
  rightBounds.Max() = nodeBounds.Max();
  switch (axis) {
  case RT_KdNode::AXIS_X:
    leftBounds.Max() = Vector3f(splitPos, nodeBounds.Max(1),  nodeBounds.Max(2));
    rightBounds.Min() = Vector3f(splitPos, nodeBounds.Min(1),  nodeBounds.Min(2));
    break;
  case RT_KdNode::AXIS_Y:
    leftBounds.Max() = Vector3f(nodeBounds.Max(0), splitPos,  nodeBounds.Max(2));
    rightBounds.Min() = Vector3f(nodeBounds.Min(0), splitPos, nodeBounds.Min(2));
    break;
  case RT_KdNode::AXIS_Z:
    leftBounds.Max() = Vector3f(nodeBounds.Max(0),  nodeBounds.Max(1), splitPos);
    rightBounds.Min() = Vector3f(nodeBounds.Min(0), nodeBounds.Min(1),  splitPos);
    break;
  }

  bool is_min_below;
  if ((ray.ori_[axis] + tmin * ray.dir_[axis]) <= splitPos) 
    is_min_below = true;
  else 
    is_min_below = false;
  //3 conditions according to spane's position with [tmin, tmax]
  if (tplane< tmin) {
    //below side traversal
    if (is_min_below) {
      RT_Ray leftRay(ray);
      return this->NodeIntersect(mNodes[index].left, leftBounds, leftRay, f, bary);
    } else {
      //above side traversal
      RT_Ray rightRay(ray);
      return this->NodeIntersect(mNodes[index].right, rightBounds, rightRay, f, bary);
    }
  } else if (tplane > tmax) {
    //below side traversal
    if (is_min_below){
      RT_Ray leftRay(ray);
      return this->NodeIntersect(mNodes[index].left, leftBounds, leftRay, f, bary);
    } else {
    //above side traversal
      RT_Ray rightRay(ray);
      return this->NodeIntersect(mNodes[index].right, rightBounds, rightRay, f, bary);
    }
  } else {
  //if in tplane in the rang of [tmin, tmax]
    //test front section
    if (is_min_below)
    {	
    // ray: from left to right
      RT_Ray leftRay(ray);
      leftRay.maxt_ = tplane;
      bool testFlag = this->NodeIntersect(mNodes[index].left, leftBounds, leftRay, f, bary);
      if (testFlag) return true;
      //test back section 
      RT_Ray rightRay(ray);
      rightRay.mint_ = tplane;
      return this->NodeIntersect(mNodes[index].right, rightBounds, rightRay, f, bary);
    } else {
    //ray : from right to left
      RT_Ray rightRay(ray);
      rightRay.maxt_ = tplane;
      bool testFlag = this->NodeIntersect(mNodes[index].right, rightBounds, rightRay, f, bary);
      if (testFlag) return true;
      //test back section 
      RT_Ray leftRay(ray);
      leftRay.mint_ = tplane;
      return this->NodeIntersect(mNodes[index].left, leftBounds, leftRay, f, bary);
    }
  }
}


bool RT_KdTree::NodeIntersectP(int index, const AABB3f& nodeBounds, RT_Ray& ray)
{		
  //if leaf node met
  if (index < 0) {
    return LeafIntersectP(-index, ray);
  }
  float tmin = ray.mint_, tmax = ray.maxt_;
  if (tmin > tmax) return false;
  //prepare to traverse kd-tree for ray
  Vector3f invDir (1.f / ray.dir_[0], 1.f / ray.dir_[1], 1.f / ray.dir_[2]);

  //compute parametric distance along ray to split plane
  int axis = mNodes[index].splitFlag;
  float splitPos = mNodes[index].split;
  float tplane = (splitPos - ray.ori_[axis]) * invDir[axis];

  AABB3f leftBounds, rightBounds;
  leftBounds.Min() = nodeBounds.Min();
  rightBounds.Max() = nodeBounds.Max();
  switch (axis)
  {
  case RT_KdNode::AXIS_X:
    leftBounds.Max() = Vector3f(splitPos, nodeBounds.Max(1),  nodeBounds.Max(2));
    rightBounds.Min() = Vector3f(splitPos, nodeBounds.Min(1),  nodeBounds.Min(2));
    break;
  case RT_KdNode::AXIS_Y:
    leftBounds.Max() = Vector3f(nodeBounds.Max(0), splitPos,  nodeBounds.Max(2));
    rightBounds.Min() = Vector3f(nodeBounds.Min(0), splitPos, nodeBounds.Min(2));
    break;
  case RT_KdNode::AXIS_Z:
    leftBounds.Max() = Vector3f(nodeBounds.Max(0),  nodeBounds.Max(1), splitPos);
    rightBounds.Min() = Vector3f(nodeBounds.Min(0), nodeBounds.Min(1),  splitPos);
    break;
  }

  bool is_min_below;
  if ((ray.ori_[axis] + tmin * ray.dir_[axis]) <= splitPos) 
    is_min_below = true;
  else 
    is_min_below = false;
  //3 conditions according to spane's position with [tmin, tmax]
  if (tplane< tmin) {
    //below side traversal
    if (is_min_below) {
      RT_Ray leftRay(ray);
      //leftBounds.IntersectP(leftRay, leftRay.mint_, leftRay.maxt_);
      return NodeIntersectP(mNodes[index].left, leftBounds, leftRay);
    } else {
    //above side traversal
      RT_Ray rightRay(ray);
      //leftBounds.IntersectP(rightRay, rightRay.mint_, rightRay.maxt_);
      return NodeIntersectP(mNodes[index].right, rightBounds, rightRay);
    }
  } else if (tplane > tmax) {
    //below side traversal
    if (is_min_below){
      RT_Ray leftRay(ray);
      //leftBounds.IntersectP(leftRay, leftRay.mint_, leftRay.maxt_);
      return NodeIntersectP(mNodes[index].left, leftBounds, leftRay);
    } else {
    //above side traversal
      RT_Ray rightRay(ray);
      //leftBounds.IntersectP(rightRay, rightRay.mint_, rightRay.maxt_);
      return NodeIntersectP(mNodes[index].right, rightBounds, rightRay);
    }
  } else { 
  //if in tplane in the rang of [tmin, tmax]
    //test front section
    if (is_min_below) {  // ray: from left to right
      RT_Ray leftRay(ray);
      leftRay.maxt_ = tplane;
      bool testFlag = NodeIntersectP(mNodes[index].left, leftBounds, leftRay);
      if (testFlag) 
        return true;
      //test back section 
      RT_Ray rightRay(ray);
      rightRay.mint_ = tplane;
      return this->NodeIntersectP(mNodes[index].right, rightBounds, rightRay);
    } else {  //ray : from right to left
      RT_Ray rightRay(ray);
      rightRay.maxt_ = tplane;
      bool testFlag = NodeIntersectP(mNodes[index].right, rightBounds, rightRay);
      if (testFlag) 
        return true;
      //test back section 
      RT_Ray leftRay(ray);
      leftRay.mint_ = tplane;
      return NodeIntersectP(mNodes[index].left, leftBounds, leftRay);
    }	
  }
}

bool RT_KdTree::LeafIntersect(int index, RT_Ray& ray, int &f, Vector3f &bary)
{
  float tmp0, tmp1;
  if (!ray.IntersectP(mPrimList[mLeafs[index].prim], tmp0, tmp1))
    return false;

  int numTris = mLeafs[index].nElems;
  float minDist = RT_Ray::MAX_RAY_RANGE, hitDist = RT_Ray::MAX_RAY_RANGE;
  bool flag = false;
  vector<int> &curTris = mLeafs[index].elems;
  for (int i = 0; i < numTris; ++i) {
    if (TriangleIntersect(curTris[i], ray, bary, hitDist)) {
      //do intersect
      flag = true;
      if (hitDist < minDist) {
        minDist = hitDist;
        f=curTris[i];
      }
    }	
  }
  return flag;
}


bool RT_KdTree::LeafIntersectP(int index, RT_Ray& ray)
{
  float tmp0, tmp1;
  if (!ray.IntersectP(mPrimList[mLeafs[index].prim], tmp0, tmp1))
    return false;

  int numTris = mLeafs[index].nElems;
  vector<int>& curTris = mLeafs[index].elems;
  for (int i = 0; i < numTris; ++i) {
    if (TriangleDoesIntersect(curTris[i],ray))
      return true; 	
  }
  return false;
}

AABB3f RT_KdTree::GetTriangleAABB(int t) const
{
  AABB3f aabb(points_[faces_[t][0]]);
  aabb += points_[faces_[t][1]];
  aabb += points_[faces_[t][2]];
  return aabb;
}

int RT_KdTree::TriangleIntersectPlaneAxis(int t, float pos, int axis) const
{
  const Vector3f p0 = points_[faces_[t][0]];
  const Vector3f p1 = points_[faces_[t][1]];
  const Vector3f p2 = points_[faces_[t][2]];
  if ((p0[axis] <= pos) &&  (p1[axis] <= pos) && (p2[axis] <= pos)) return 0; //below the plane
  if ((p0[axis] >= pos) &&  (p1[axis] >= pos) && (p2[axis] >= pos)) return 1; //above the plane
  return 2; //intersect the plane
}

bool RT_KdTree::TriangleIntersect(int tri, const RT_Ray &ray, Vector3f &bary, float &hitDist) const
{
  const float eps = 0.000f;
  const Vector3f &p1 = points_[faces_[tri][0]];
  const Vector3f &p2 = points_[faces_[tri][1]];
  const Vector3f &p3 = points_[faces_[tri][2]];
  //get e1, e2
  Vector3f e1 = p2 - p1;
  Vector3f e2 = p3 - p1;
  //get s1
  Vector3f s1 = Cross(ray.dir_, e2);

  float divisor = FastDot(s1, e1);

  //if degenerated triangle then no intersection
  if (abs(divisor) < eps) return false; 
  float invDivisor = 1.f / divisor;

  // Compute first barycentric coordinate b1
  Vector3f d = ray.ori_ - p1;
  float b1 = FastDot(d, s1) * invDivisor;

  // is b1 in range[0,1], if not,no intersection
  if (b1 < -eps || b1 > 1 + eps)
	  return false;

  // Compute second barycentric coordinate b2
  Vector3f s2 = Cross(d, e1);
  float b2 = FastDot(ray.dir_, s2) * invDivisor;
  //is b1+b2  in  range[0,1], if not ,no intersection
  if (b2 < - eps || b1 + b2 > 1.f + eps)
	  return false;
  // Compute _t_ to intersection point
  float t = FastDot(e2, s2) * invDivisor;
  //if intersect para t is out of ray 's range [mint, maxt], no intersection
  if (t < ray.mint_ || t > ray.maxt_)
	  return false;

  bary[0] = 1.0f-b1-b2;
  bary[1] = b1;
  bary[2] = b2;
  hitDist = t;
  return true;
}

bool RT_KdTree::TriangleDoesIntersect(int t, const RT_Ray &ray) const
{
  Vector3f bary;
  float dist;
  return TriangleIntersect(t, ray, bary, dist);
}

}  // namespace zzz