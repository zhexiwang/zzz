#define ZGRAPHICS_SOURCE
#include "RT_Ray.hpp"

namespace zzz {
const float RT_Ray::MAX_RAY_RANGE=4096.0f;
const float RT_Ray::RAY_EPSILON=0.00003f;
}