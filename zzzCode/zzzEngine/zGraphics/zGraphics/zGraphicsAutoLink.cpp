#include <zGraphics/zGraphicsConfig.hpp>

#ifdef ZZZ_DYNAMIC
#include <zCore/zCoreAutoLink.hpp>
#include <zImageAutoLink.hpp>
#include <zGraphicsAutoLink3rdParty.hpp>
#endif