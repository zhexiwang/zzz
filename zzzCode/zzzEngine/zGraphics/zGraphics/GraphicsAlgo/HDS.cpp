#include "HDS.hpp"
//#include "matrix.h"
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <numeric>
#include <float.h>

#include <Base/glHelper/ObjMesh.h>
#include <Base/Common/notify.h>
#include <Base/Common/serializer.h>

///////////////////////////////////////
// struct MatrixElement
// elements of Matrix class, represent non-zero entry in a matrix
struct MatrixElement
{
  int row, col;	// row and column of the element
  double value;	// value of the element

  // constructor
  MatrixElement(int r, int c, double v)
    : row(r), col(c), value(v) {}

  // compare function for Matrix::SortMatrix() function
  static bool order (MatrixElement e1, MatrixElement e2)
  {
    if (e1.row < e2.row) return true;
    if (e1.row == e2.row) return (e1.col < e2.col);
    return false;
  }
};

// class declaration
typedef std::vector<MatrixElement> MatrixElementList;

// class Matrix definition
class Matrix
{
private:
  int m, n;	// size of matrix (m = # of rows, n # of columns)
  MatrixElementList elements;	// list of non-zero entries
  int * rowIndex;				// row indice of non-zero entries

  // fields for CG method
  double* diagInv;
  double* r;
  double* r2;
  double* d;
  double* d2;
  double* q;
  double* s;
  double* s2;

public:
  // constructor & destructor
  Matrix(int m, int n) : m(m), n(n)
  {
    rowIndex = new int[m+1];
    diagInv = new double[m];
    r = new double[m];
    r2 = new double[m];
    d = new double[m];
    d2 = new double[m];
    q = new double[m];
    s = new double[m];
    s2 = new double[m];
  }
  ~Matrix()
  {
    delete[] rowIndex;
    delete[] r;
    delete[] r2;
    delete[] d;
    delete[] d2;
    delete[] q;
    delete[] s;
    delete[] s2;
  }

  /////////////////////////////////////
  // function AddElement
  // add a new entry into the matrix
  void AddElement(int row, int col, double value)
  {
    elements.push_back(MatrixElement(row, col, value));
  }

  /////////////////////////////////////
  // function SortMatrix
  // sort the matrix elements after you add ALL elements into the matrix
  void SortMatrix()
  {
    sort(elements.begin( ), elements.end( ), MatrixElement::order);

    for (int i=0; i<m+1; i++)
      rowIndex[i] = 0;
    for (int i=0; i<(int)elements.size(); i++)
      rowIndex[elements[i].row + 1] = i + 1;

    for (int i=0; i<m; i++)
      diagInv[i] = 0;
    for (int i=0; i<(int)elements.size(); i++)
      if (elements[i].row == elements[i].col)
        diagInv[elements[i].row] = 1.0 / elements[i].value;
  }


  /////////////////////////////////////
  // function Multiply
  // compute A * xIn = xOut
  // the arrays pointed by xIn and xOut have to be pre-allocated
  // and have enough space
  void Multiply(double* xIn, double* xOut)
  {
    for (int i=0; i<m; i++)
    {
      double sum = 0;
      for (int j=rowIndex[i]; j<rowIndex[i+1]; j++)
        sum += elements[j].value * xIn[elements[j].col];
      xOut[i] = sum;
    }
  }
  /////////////////////////////////////
  // Multiply PreMultiply
  // compute xIn * A = xOut
  // the arrays pointed by xIn and xOut have to be pre-allocated
  // and have enough space
  void PreMultiply(double* xIn, double* xOut)
  {
    for (int i=0; i<n; i++) xOut[i] = 0;

    for (int i=0; i<m; i++)
    {
      for (int j=rowIndex[i]; j<rowIndex[i+1]; j++)
        xOut[elements[j].col] += elements[j].value * xIn[i];
    }
  }

  /**********************************************/
  /* function: BCG                              */
  /* description: solve Ax = b for unknowns x   */
  /**********************************************/
  void BCG(double* b, double* x, int N)
  {
    double eps = 0.05;
    int imax = 100;
    double* d = new double[N];
    double* r = new double[N];
    double alpha = 0;
    double beta = 0;

    double* temp = new double[N];
    Multiply(x, temp);

    //d(0) = r(0) = b - A*x(0)
    for (int i=0; i<N; i++){
      r[i] = b[i] - temp[i];
      d[i] = r[i];
    }
    delete []temp;

    int i = 1;

    double en = MultipleSingle(r, r, N); //e_new = r'*r;
    double e0 = en;

    while(i < imax & en > eps*eps*e0) {
      double *q = new double[N];  // q = Ad
      Multiply(d, q);
      alpha = en/MultipleSingle(d, q, N);
      for (int j=0; j<N; j++) {
        x[j] = x[j] + alpha*d[j];
      }
      if (i%50 == 0) {
        double *temp2 = new double[N];
        Multiply(x, temp2);
        for (int j=0; j<N; j++){
          r[j] = b[j] - temp2[j]; //r = b - Ax
        }
        delete []temp2;
      } else {
        for (int j=0; j<N; j++){
          r[j] = r[j] - alpha*q[j];  //r = r - alpha*q
        }
      }

      double eo = en;  // e_old = e_new
      en = MultipleSingle(r, r, N);  //e_new = r'*r;
      beta = en/eo;
      for (int j=0; j<N; j++){
        d[j] = r[j] + beta*d[j];  // d = r + beta*d
      }
      delete []q;
      i++;
    }

    delete []d;
    delete []r;
  }

  // b = left'*right; [1*N]*[N*1]
  double MultipleSingle(double* left, double* right, int N)
  {
    double sum = 0;
    for (int i=0; i<N; i++) {
      sum += left[i]*right[i];
    }
    return sum;
  }

  // friend operators
  friend std::ostream & operator<< (std::ostream & out, const Matrix & r)
  {
    for (int i=0; i<r.m; i++)
    {
      for(int j=r.rowIndex[i]; j<r.rowIndex[i+1]; j++)
        out << r.elements[j].value << " ";
      out << std::endl;
    }
    return out;
  }
};


using namespace std;

namespace zeta
{

  /////////////////////////////////////////
  // helping inline functions
  inline static double Cot(const V3d & p1, const V3d & p2, const V3d & p3) {
    V3d v1 = p1 - p2;
    V3d v2 = p3 - p2;

    v1 /= v1.norml2();
    v2 /= v2.norml2();
    double tmp = v1.dot(v2);
    if (std::fabs(tmp) < std::numeric_limits<double>::epsilon())
      return 0;
    else if (std::fabs(1-std::fabs(tmp)) < std::numeric_limits<double>::epsilon())
    {
      return 1e5;
    }
    else
      return 1.0 / tan(acos(tmp));
  }

  inline static double Area(const V3d & p1, const V3d & p2, const V3d & p3) {
    V3d v1 = p2 - p1;
    V3d v2 = p3 - p1;
    return v1.cross(v2).norml2() / 2.0;
  }


  /////////////////////////////////////////
  // implementation of OneRingHEdge class
  OneRingHEdge::OneRingHEdge(const Vertex * v) {
    if (v == NULL) start = next = NULL;
    else start = next = v->HalfEdge();
  }

  HEdge * OneRingHEdge::NextHEdge() {
    HEdge *ret = next;
    if (next && next->Prev()->Twin() != start)
      next = next->Prev()->Twin();
    else
      next = NULL;
    return ret;
  }

  /////////////////////////////////////////
  // implementation of Mesh class
  //
  // function AddFace
  // it's only for loading obj model, you do not need to understand it
  Face* Mesh::AddFace(int v1, int v2, int v3) {
    int i;
    HEdge *he[3], *bhe[3];
    Vertex *v[3];
    Face *f;

    // obtain objects
    for (i=0; i<3; i++) he[i] = new HEdge();
    for (i=0; i<3; i++) bhe[i] = new HEdge(true);
    v[0] = vList[v1];
    v[1] = vList[v2];
    v[2] = vList[v3];
    f = new Face();

    // connect start pointers for bhe
    bhe[0]->SetStart(v[1]);
    bhe[1]->SetStart(v[0]);
    bhe[2]->SetStart(v[2]);
    for (i=0; i<3; i++) he[i]->SetStart(v[i]);

    // connect prev-next pointers
    SetPrevNext(he[0], he[1]);
    SetPrevNext(he[1], he[2]);
    SetPrevNext(he[2], he[0]);
    SetPrevNext(bhe[0], bhe[1]);
    SetPrevNext(bhe[1], bhe[2]);
    SetPrevNext(bhe[2], bhe[0]);

    // connect twin pointers
    SetTwin(he[0], bhe[0]);
    SetTwin(he[1], bhe[2]);
    SetTwin(he[2], bhe[1]);

    // connect start pointers
    // connect face-hedge pointers
    for (i=0; i<3; i++) {
      v[i]->SetHalfEdge(he[i]);
      //    v[i]->adjHEdges.push_back(he[i]);
      SetFace(f, he[i]);
    }
    v[0]->adjHEdges.push_back(bhe[1]);
    v[1]->adjHEdges.push_back(bhe[0]);
    v[2]->adjHEdges.push_back(bhe[2]);

    // merge boundary if in need
    for (i=0; i<3; i++) {
      Vertex *start = bhe[i]->Start();
      Vertex *end   = bhe[i]->End();
      for (size_t j=0; j<end->adjHEdges.size(); j++) {
        HEdge *curr = end->adjHEdges[j];
        if (curr->IsBoundary() && curr->End()==start && curr->Start()==end) {
          SetPrevNext(bhe[i]->Prev(), curr->Next());
          SetPrevNext(curr->Prev(), bhe[i]->Next());
          SetTwin(bhe[i]->Twin(), curr->Twin());
          bhe[i]->SetStart(NULL);	// mark as unused
          bhe[i]->SetBoundary(false);
          curr->SetStart(NULL);	// mark as unused
          curr->SetBoundary(false);
          start->adjHEdges.erase(find(start->adjHEdges.begin(), start->adjHEdges.end(), bhe[i]));  // remove from v2e
          end->adjHEdges.erase(find(end->adjHEdges.begin(), end->adjHEdges.end(), curr));  // remove from v2e
          break;
        }
      }
    }

    // finally add hedges and faces to list
    for (i=0; i<3; i++) heList.push_back(he[i]);
    for (i=0; i<3; i++) heList.push_back(bhe[i]);
    fList.push_back(f);
    face_id_map_[f] = static_cast<int>(fList.size())-1;
    return f;
  }

  bool Mesh::RemoveLastFace() {
    Face* f = fList.back();
    face_id_map_.erase(f);
    fList.pop_back();
    HEdge *he[3], *bhe[3];
    he[0] = f->HalfEdge();
    he[1] = he[0]->Next();
    he[2] = he[1]->Next();
    bhe[0] = he[0]->Twin();
    bhe[1] = he[2]->Twin();
    bhe[2] = he[1]->Twin();
    he[0]->SetFace(NULL);
    he[1]->SetFace(NULL);
    he[2]->SetFace(NULL);
    for (int i = 0; i < 3; ++i) {
      if (bhe[i]->IsBoundary()) {
        Vertex *v = bhe[i]->Start();
        v->adjHEdges.erase(find(v->adjHEdges.begin(), v->adjHEdges.end(), bhe[i]));
        bhe[i]->SetStart(NULL);
        bhe[i]->Twin()->SetStart(NULL);
        bhe[i]->Twin()->SetBoundary(false);
        SetPrevNext(bhe[i]->Prev(), bhe[i]->Twin()->Next());
        SetPrevNext(bhe[i]->Twin()->Prev(), bhe[i]->Next());
      } else {
        Vertex *v = bhe[i]->Twin()->Start();
        bhe[i]->Twin()->SetBoundary(true);
        v->adjHEdges.push_back(bhe[i]->Twin());
      }
    }
    //  cout << endl;
    delete f;
  }

  // function LoadObjFile
  // it's only for loading obj model, you do not need to understand it
  bool Mesh::LoadObjFile(const char *filename) {
    if (filename==NULL || strlen(filename)==0) return false;
    ifstream ifs(filename);
    if (ifs.fail()) return false;

    Clear();

    char buf[1024], type[1024];
    do {
      ifs.getline(buf, 1024);
      stringstream iss(buf);
      iss >> type;

      // vertex
      if (strcmp(type, "v") == 0) {
        double x, y, z;
        iss >> x >> y >> z;
        AddVertex(new Vertex(x,y,z));
      }
      // face
      else if (strcmp(type, "f") == 0) {
        int index[3];
        iss >> index[0] >> index[1] >> index[2];
        AddFace(index[0]-1, index[1]-1, index[2]-1);
      }
    } while (!ifs.eof());
    ifs.close();

    size_t i;
    V3d box = this->MaxCoord() - this->MinCoord();
    // Make the x-length of the mesh to a unit?
    for (i=0; i<vList.size(); i++) vList[i]->SetPosition(vList[i]->Position() / box.x());

    V3d tot;
    for (i=0; i<vList.size(); i++) tot += vList[i]->Position();
    V3d avg = tot / vList.size();
    for (i=0; i<vList.size(); i++) vList[i]->SetPosition(vList[i]->Position() - avg);

    HEdgeList list;
    for (i=0; i<heList.size(); i++)
      if (heList[i]->Start()) list.push_back(heList[i]);
    heList = list;

    for (i=0; i<vList.size(); i++)
    {
      vList[i]->adjHEdges.clear();
      vList[i]->SetIndex((int)i);
      vList[i]->SetFlag(0);
    }

    return true;
  }

  void Mesh::HSVtoRGB( double *r, double *g, double *b, double h, double s , double v )
  {
    int i;
    double f, p, q, t;
    if( s == 0 ) {
      // achromatic (grey)
      *r = *g = *b = v;
      return;
    }
    h /= 60;			// sector 0 to 5
    i = floor( h );
    f = h - i;			// factorial part of h
    p = v * ( 1 - s );
    q = v * ( 1 - s * f );
    t = v * ( 1 - s * ( 1 - f ) );
    switch( i ) {
    case 0:
      *r = v;
      *g = t;
      *b = p;
      break;
    case 1:
      *r = q;
      *g = v;
      *b = p;
      break;
    case 2:
      *r = p;
      *g = v;
      *b = t;
      break;
    case 3:
      *r = p;
      *g = q;
      *b = v;
      break;
    case 4:
      *r = t;
      *g = p;
      *b = v;
      break;
    default:		// case 5:
      *r = v;
      *g = p;
      *b = q;
      break;
    }
  }
  // -------------------------------------------------------
  // Implement the following functions
  // -------------------------------------------------------
  void Mesh::ComputeVertexNormals() 
  {

    /*************************/
    /* insert your code here */
    /*************************/

    double PI = 3.1415926535897932;
    std::vector<Vertex*> oneRingV;
    //std::vector<double> weight;
    V3d t1, t2;
    for (size_t i = 0; i < vList.size(); i++)
    {
      OneRingVertex ring(vList[i]);
      oneRingV.clear();
      t1[0] = t1[1] = t1[2] = 0;
      t2[0] = t2[1] = t2[2] = 0;
      int numNeighbor = vList[i]->Valence();
      for (int j = 0; j < numNeighbor; j++)
      {
        oneRingV.push_back(ring.NextVertex());
      }
      if (!vList[i]->IsBoundary())
      {
        for (size_t j = 0; j < oneRingV.size(); j++)
        {
          double coef1, coef2;
          coef1 = cos(2 * PI * j / numNeighbor);
          coef2 = sin(2 * PI * j / numNeighbor);
          t1 += coef1 * oneRingV[j]->Position();
          t2 += coef2 * oneRingV[j]->Position();
        }
        V3d n = t1.cross(t2);
        n /= n.norml2();
        vList[i]->SetNormal(n);
      }
      else
      {
        t1 = oneRingV[0]->Position() - oneRingV[numNeighbor - 1]->Position();
        if (numNeighbor == 2)
        {
          t2 = oneRingV[0]->Position() - oneRingV[numNeighbor - 1]->Position() - (vList[i]->Position() * 2.0);
        }
        else if (numNeighbor == 3)
        {
          t2 = oneRingV[1]->Position() - vList[i]->Position();
        }
        else if (numNeighbor >= 4)
        {
          double theta = PI / (numNeighbor - 1);
          t2 = sin(theta) * (oneRingV[0]->Position() + oneRingV[numNeighbor - 1]->Position());
          //                double coef = 2 * cos(theta) - 2;
          for (int j = 1; j < numNeighbor - 1; j++)
          {
            t2 += sin(j * theta) * oneRingV[j]->Position();
          }
        }
        V3d n = t1.cross(t2);
        n /= n.norml2();
        vList[i]->SetNormal(n);
      }
    }
  }

  void Mesh::UmbrellaSmooth() 
  {
    /*************************/
    /* insert your code here */
    /*************************/
    Matrix L(vList.size(), vList.size());
    std::vector<Vertex*> oneRingV;
    std::vector<double> weight;
    //    zeta::notify(zeta::Notify::Gossip) << "Setting the laplacian matrix ...\n";
    for (size_t i = 0; i < vList.size(); i++)
    {
      OneRingVertex ring(vList[i]);
      oneRingV.clear();
      weight.clear();
      int numNeighbor = vList[i]->Valence();
      for (int j = 0; j < numNeighbor; j++)
      {
        oneRingV.push_back(ring.NextVertex());
      }
      weight.resize(numNeighbor, 0);
      for (size_t j = 0; j < oneRingV.size(); j++)
      {
        int prevIdx, nextIdx;
        prevIdx = (j + oneRingV.size() - 1) % oneRingV.size();
        nextIdx = (j + 1) % oneRingV.size();
        double cot1, cot2;
        cot1 = Cot(vList[i]->Position(), oneRingV[prevIdx]->Position(), oneRingV[j]->Position());
        cot2 = Cot(vList[i]->Position(), oneRingV[nextIdx]->Position(), oneRingV[j]->Position());
        weight[j] = cot1 + cot2;
      }
      double totWeight = std::accumulate(weight.begin(), weight.end(), 0.0);

      for (size_t j = 0; j < oneRingV.size(); j++)
      {
        if (std::fabs(totWeight) < std::numeric_limits<double>::epsilon())
          weight[j] = 0;
        else
          weight[j] /= totWeight;
      }
      assert(!isnan(totWeight));
      double lambda = 1;
      for (size_t j = 0; j < oneRingV.size(); j++)
      {
        L.AddElement(i, oneRingV[j]->Index(), lambda * weight[j]);
      }
      if (std::fabs(totWeight) < std::numeric_limits<double>::epsilon())
        L.AddElement(i, i, 1);
      else
        L.AddElement(i, i, -1 * lambda + 1);
    }
    //    zeta::notify(zeta::Notify::Gossip) << "Done.\n";
    //    zeta::notify(zeta::Notify::Gossip) << "Computing new positions...\n";
    L.SortMatrix();
    double* x = new double[vList.size()];
    double* nx = new double[vList.size()];
    double* y = new double[vList.size()];
    double* ny = new double[vList.size()];
    double* z = new double[vList.size()];
    double* nz = new double[vList.size()];
    for (size_t i = 0; i < vList.size(); i++)
    {
      x[i] = vList[i]->Position()[0];
      y[i] = vList[i]->Position()[1];
      z[i] = vList[i]->Position()[2];
    }
    L.Multiply(x, nx);
    L.Multiply(y, ny);
    L.Multiply(z, nz);
    for (size_t i = 0; i < vList.size(); i++)
    {
      V3d p;

      p[0] = nx[i];
      p[1] = ny[i];
      p[2] = nz[i];
      vList[i]->SetPosition(p);
    }

    delete []x;
    delete []y;
    delete []z;
    delete []nx;
    delete []ny;
    delete []nz;
  }


  //typedef Eigen::Matrix<int, Eigen::Dynamic, 1> VectorXi;
  void Mesh::ImplicitUmbrellaSmooth()
  {
    /*************************/
    /* insert your code here */
    /*************************/
    Matrix L(vList.size(), vList.size());
    std::vector<Vertex*> oneRingV;
    std::vector<double> weight;
    for (size_t i = 0; i < vList.size(); i++)
    {
      OneRingVertex ring(vList[i]);
      oneRingV.clear();
      weight.clear();
      int numNeighbor = vList[i]->Valence();
      for (int j = 0; j < numNeighbor; j++)
      {
        oneRingV.push_back(ring.NextVertex());
      }
      weight.resize(numNeighbor, 0);
      for (size_t j = 0; j < oneRingV.size(); j++)
      {
        int prevIdx, nextIdx;
        prevIdx = (j + oneRingV.size() - 1) % oneRingV.size();
        nextIdx = (j + 1) % oneRingV.size();
        double cot1, cot2;
        cot1 = Cot(vList[i]->Position(), oneRingV[prevIdx]->Position(), oneRingV[j]->Position());
        cot2 = Cot(vList[i]->Position(), oneRingV[nextIdx]->Position(), oneRingV[j]->Position());
        weight[j] = cot1 + cot2;
      }
      double totWeight = 0;
      for (size_t j = 0; j < oneRingV.size(); j++)
      {
        totWeight += weight[j];
      }
      for (size_t j = 0; j < oneRingV.size(); j++)
      {
        weight[j] /= totWeight;
      }
      double lambda = 1;
      for (size_t j = 0; j < oneRingV.size(); j++)
      {
        L.AddElement(i, oneRingV[j]->Index(), -lambda * weight[j]);
      }
      L.AddElement(i, i, 1 - lambda);
    }
    L.SortMatrix();
    double* x = new double[vList.size()];
    double* nx = new double[vList.size()];
    double* y = new double[vList.size()];
    double* ny = new double[vList.size()];
    double* z = new double[vList.size()];
    double* nz = new double[vList.size()];
    for (size_t i = 0; i < vList.size(); i++)
    {
      x[i] = vList[i]->Position()[0];
      y[i] = vList[i]->Position()[1];
      z[i] = vList[i]->Position()[2];

      nx[i] = vList[i]->Position()[0];
      ny[i] = vList[i]->Position()[1];
      nz[i] = vList[i]->Position()[2];
    }
    L.BCG(x, nx, vList.size());
    L.BCG(y, ny, vList.size());
    L.BCG(z, nz, vList.size());
    for (size_t i = 0; i < vList.size(); i++)
    {
      V3d p;

      p[0] = -nx[i];
      p[1] = -ny[i];
      p[2] = -nz[i];
      vList[i]->SetPosition(p);
    }

    delete []x;
    delete []y;
    delete []z;
    delete []nx;
    delete []ny;
    delete []nz;
  }
  void Mesh::ComputeVertexCurvatures()
  {
    /*************************/
    /* insert your code here */
    /*************************/
    std::vector<Vertex*> oneRingV;
    std::vector<double> curvature;
    curvature.resize(vList.size(), 0);
    double maxC = 0;
    for (size_t i = 0; i < vList.size(); i++)
    {
      OneRingVertex ring(vList[i]);
      oneRingV.clear();
      int numNeighbor = vList[i]->Valence();
      for (int j = 0; j < numNeighbor; j++)
      {
        oneRingV.push_back(ring.NextVertex());
      }
      double totArea;
      totArea = 0;
      V3d vec;
      vec[0] = 0;
      vec[1] = 0;
      vec[2] = 0;
      for (size_t j = 0; j < oneRingV.size(); j++)
      {
        int prevIdx, nextIdx;
        prevIdx = (j + oneRingV.size() - 1) % oneRingV.size();
        nextIdx = (j + 1) % oneRingV.size();
        double cot1, cot2;
        cot1 = Cot(vList[i]->Position(), oneRingV[prevIdx]->Position(), oneRingV[j]->Position());
        cot2 = Cot(vList[i]->Position(), oneRingV[nextIdx]->Position(), oneRingV[j]->Position());
        vec += (cot1 + cot2) * (oneRingV[j]->Position() - vList[i]->Position());
        totArea += Area(vList[i]->Position(), oneRingV[prevIdx]->Position(), oneRingV[j]->Position());
      }
      vec /= 4 * totArea;
      curvature[i] = vec.norml2();
      if (maxC < curvature[i])
      {
        maxC = curvature[i];
      }
    }
    for (size_t i = 0; i < curvature.size(); i++)
    {
      double h;
      V3d c;
      h = curvature[i] / maxC * 360;
      HSVtoRGB(&c[0], &c[1], &c[2], h);
      vList[i]->SetColor(c);
    }

  }

  bool Mesh::FromOBJMesh(OBJMesh const & mesh)
  {
    Clear();

    // Add vertices
    for (size_t i = 1; i < mesh.GetVertexNum(); ++i)
    {
      AddVertex(new Vertex(mesh.GetVertex(i)));
    }

    // Add all faces.
    for (size_t i = 0; i < mesh.GetTriangleNum(); ++i)
    {
      OBJTriangle const & tri = mesh.GetTriangle(i);
      AddFace(tri.vindices[0]-1, tri.vindices[1]-1, tri.vindices[2]-1);
    }

    SetupAuxHalfEdgeStructure();

    return true;
  }
  bool Mesh::FromPLYMesh(zeta::PLYMesh const & /*mesh*/)
  {
    return true;
  }
  bool Mesh::FromRawTriangles(std::vector<zeta::V3d> const & vertices, std::vector<int> const & indices)
  {
    assert(indices.size() % 3 == 0);

    Clear();

    // Add vertices
    for (size_t i = 0; i < vertices.size(); ++i)
    {
      AddVertex(new Vertex(vertices[i]));
    }

    // Add all faces.
    for (size_t i = 0; i < indices.size(); i += 3)
    {
      AddFace(indices[i], indices[i+1], indices[i+2]);
    }

    SetupAuxHalfEdgeStructure();

    return true;
  }

  void Mesh::SetupAuxHalfEdgeStructure()
  {
    // Setup the half edge structure
    HEdgeList list;
    for (size_t i=0; i<heList.size(); i++)
      if (heList[i]->Start()) list.push_back(heList[i]);
    heList = list;

    for (size_t i=0; i<vList.size(); i++)
    {
      vList[i]->adjHEdges.clear();
      vList[i]->SetIndex((int)i);
      vList[i]->SetFlag(0);
    }
  }

  bool Mesh::ToOBJMesh(zeta::OBJMesh & mesh) const
  {
    if (mesh.GetVertexNum() != vList.size()+1)
    {
      mesh.SetVertexVectorLength(vList.size()+1);
    }
    typedef std::tr1::unordered_map<zeta::V3d, int> VertexIDMap;
    VertexIDMap vertex_id_map;
    for (size_t i = 0; i < vList.size(); ++i)
    {
      mesh.SetVertex(i+1, vList[i]->Position());
      vertex_id_map[vList[i]->Position()] = i+1;
    }

    if (mesh.GetTriangleNum() != fList.size())
    {
      mesh.SetTrianglesVectorLength(fList.size());
    }
    for (size_t f = 0; f < fList.size(); ++f)
    {
      OBJTriangle tri;
      Face * face = fList[f];
      HEdge * edge = face->HalfEdge();
      tri.vindices[0] = vertex_id_map.find(edge->Start()->Position())->second;
      //        assert(vertex_id_map_.find(edge->Start()) != vertex_id_map_.end());
      //        tri.vindices[0] = vertex_id_map_.find(edge->Start())->second + 1;
      edge = edge->Next();
      tri.vindices[1] = vertex_id_map.find(edge->Start()->Position())->second;
      //        assert(vertex_id_map_.find(edge->Start()) != vertex_id_map_.end());
      //        tri.vindices[1] = vertex_id_map_.find(edge->Start())->second + 1;
      edge = edge->Next();
      tri.vindices[2] = vertex_id_map.find(edge->Start()->Position())->second;
      //        assert(vertex_id_map_.find(edge->Start()) != vertex_id_map_.end());
      //        tri.vindices[2] = vertex_id_map_.find(edge->Start())->second + 1;
      mesh.SetTriangle(f, tri);
    }

    return false;
  }
  bool Mesh::ToPLYMesh(zeta::PLYMesh & mesh) const
  {
    return false;
  }
  bool Mesh::ToRawTriangles(std::vector<zeta::V3d> & vertices, std::vector<int> & indices) const
  {
    if (vertices.size() != vList.size())
    {
      vertices.resize(vList.size());
    }
    typedef std::tr1::unordered_map<zeta::V3d, int> VertexIDMap;
    VertexIDMap vertex_id_map;
    for (size_t i = 0; i < vList.size(); ++i)
    {
      vertices[i] = vList[i]->Position();
      vertex_id_map[vList[i]->Position()] = i;
    }

    if (indices.size() != fList.size()*3)
    {
      indices.resize(fList.size()*3);
    }
    for (size_t f = 0; f < fList.size(); ++f)
    {
      OBJTriangle tri;
      Face * face = fList[f];
      HEdge * edge = face->HalfEdge();
      tri.vindices[0] = vertex_id_map.find(edge->Start()->Position())->second;
      edge = edge->Next();
      tri.vindices[1] = vertex_id_map.find(edge->Start()->Position())->second;
      edge = edge->Next();
      tri.vindices[2] = vertex_id_map.find(edge->Start()->Position())->second;
      indices[f*3] = (tri.vindices[0]);
      indices[f*3+1] = (tri.vindices[1]);
      indices[f*3+2] = (tri.vindices[2]);
    }
    return false;
  }

  bool Mesh::ExtractFaces(std::vector<int> const & vertex_ids
    , std::vector<zeta::V3d> & vertices
    , std::vector<int> & indices
    , std::vector<int> & map_to_global_indices)
  {
    typedef std::tr1::unordered_set<int> IDSet;
    IDSet face_set;
    // Find all faces
    for (size_t i = 0; i < vertex_ids.size(); ++i)
    {
      int vertex_id = vertex_ids[i];
      Vertex * vertex = vList[vertex_id];
      OneRingHEdge ring(vertex);
      HEdge *curr = NULL;
      while ((curr=ring.NextHEdge()))
      {
        if (curr->LeftFace() != NULL)
        {
          int face_id = faceID(curr->LeftFace());
          //                if (face_set.find(face_id) == face_set.end())
          {
            face_set.insert(face_id);
          }
        }
      }

      //        for (size_t e = 0; e < vertex->adjHEdges.size(); ++e)
      //        {
      //            HEdge * edge = vertex->adjHEdges[e];

      //        }
    }
    std::cerr << "Face # " << face_set.size() << "\n";

    // Find all vertex
    IDSet vertex_set;
    for (IDSet::iterator iter = face_set.begin()
      ; iter != face_set.end(); ++iter)
    {
      int face_id = *iter;
      Face * face = fList[face_id];
      HEdge * edge = face->HalfEdge();
      vertex_set.insert(vertexID(edge->Start()));
      edge = edge->Next();
      vertex_set.insert(vertexID(edge->Start()));
      edge = edge->Next();
      vertex_set.insert(vertexID(edge->Start()));
    }

    vertices.reserve(vertex_set.size());
    map_to_global_indices.reserve(vertex_set.size());
    std::tr1::unordered_map<int, int> vertex_local_indices_map;
    for (IDSet::iterator iter = vertex_set.begin(); iter != vertex_set.end(); ++iter)
    {
      int global_vertex_id = *iter;
      map_to_global_indices.push_back(global_vertex_id);
      vertex_local_indices_map[global_vertex_id] = vertices.size();
      vertices.push_back(vList[global_vertex_id]->Position());
    }

    indices.reserve(face_set.size()*3);
    for (IDSet::iterator iter = face_set.begin()
      ; iter != face_set.end(); ++iter)
    {
      int face_id = *iter;
      Face * face = fList[face_id];
      HEdge * edge = face->HalfEdge();
      indices.push_back(vertex_local_indices_map.find(vertexID(edge->Start()))->second);
      edge = edge->Next();
      indices.push_back(vertex_local_indices_map.find(vertexID(edge->Start()))->second);
      edge = edge->Next();
      indices.push_back(vertex_local_indices_map.find(vertexID(edge->Start()))->second);
    }

    return true;
  }
}

