#pragma once
#include <zCore/common.hpp>

namespace zzz {
class HEdge;
class Vertex;
class Face;
class Mesh;
class OneRingHEdge;
class OneRingVertex;
class FaceEnumeration;

class OBJMesh;
class PLYMesh;

// types
typedef std::vector<HEdge*> HEdgeList;
typedef std::vector<Vertex*> VertexList;
typedef std::vector<Face*> FaceList;

// other helper functions
inline void SetPrevNext(HEdge *e1, HEdge *e2);
inline void SetTwin(HEdge *e1, HEdge *e2);
inline void SetFace(Face *f, HEdge *e);

////////// class HEdge //////////
class HEdge {
private:
  HEdge *twin, *prev, *next;	// twin/previous/next half edges
  Vertex *start;				// start vertex
  Face *face;					// left face
  bool boundary;				// flag for boundary edge

  bool valid;

  // it's for count boundary loop use,
  // you can use it freely for marking in boundary loop counting
  // and connected component counting
  bool flag;

public:
  /////////////////////////////////////
  // constructor
  HEdge(bool b=false) {
    boundary = b;
    twin = prev = next = NULL;
    start = NULL;
    face = NULL;
    flag = false;
    valid = true;
  }
  /////////////////////////////////////
  // access functions
  HEdge*  Twin() const { return twin; }
  HEdge*  Prev() const { return prev; }
  HEdge*  Next() const { return next; }
  Vertex* Start() const { return start; }
  Vertex* End() const { return next->start; } // for convenience
  Face*   LeftFace() const { return face; }
  bool    Flag() const { return flag; }

  HEdge*  SetTwin(HEdge* e) { return twin = e; }
  HEdge*  SetPrev(HEdge* e) { return prev = e; }
  HEdge*  SetNext(HEdge* e) { return next = e; }
  Vertex* SetStart(Vertex* v) { return start = v; }
  Face*   SetFace(Face* f) { return face = f; }
  void    SetBoundary(bool b) { boundary = b; }
  bool    SetFlag(bool b) { return flag = b; }
  bool    SetValid(bool b) { return valid = b; }
  bool    IsBoundary() const { return boundary; }
  bool    IsValid() const { return valid; }
};

////////// class OneRingHEdge //////////
// this class is use for access the neighbor HALF EDGES
// of a given vertex, please see Vertex::IsBoundary() for its usage
class OneRingHEdge {
private:
  HEdge *start, *next;
public:
  OneRingHEdge(const Vertex * v);	// constructor
  HEdge * NextHEdge();			// iterator
};

////////// class OneRingVertex //////////
// this class is use for access the neighbor VERTICES 
// of a given vertex, please see Vertex::Valence() for its usage
class OneRingVertex {
private:
  OneRingHEdge ring;
public:
  OneRingVertex(const Vertex * v) : ring(v) { }	// constructor
  Vertex * NextVertex() { HEdge *he=ring.NextHEdge(); return (he)?he->End():NULL; } // iterator
};

////////// class Vertex //////////
class Vertex {
private:
  V3d position;	// position (x,y,z) in space
  V3d normal;	// normal vector for smooth shading rendering
  V3d color;		// color value for curvature displaying
  HEdge *he;			// one of half edge starts with this vertex
  int index;			// index in the Mesh::vList, DO NOT UPDATE IT
  int flag;           // 0 for unselected, 1 for selected
  bool valid;
public:
  std::vector<HEdge*> adjHEdges; // for reading object only, do not use it in other place

  // constructors
  Vertex() : he(NULL), flag(0), valid(true) { }
  Vertex(const V3d & v) : position(v), he(NULL), flag(0), valid(true) { }
  Vertex(double x, double y, double z) : position(x,y,z), he(NULL), flag(0), valid(true) { }

  // access functions
  const V3d & Position() const { return position; }
  const V3d & Normal() const { return normal; }
  const V3d & Color() const { return color; }
  HEdge * HalfEdge() const { return he; }
  int Index() const { return index; }
  int Flag() const { return flag; }
  const V3d & SetPosition(const V3d & p) { return position = p; }
  const V3d & SetNormal(const V3d & n) { return normal = n; }
  const V3d & SetColor(const V3d & c) { return color = c; }
  HEdge * SetHalfEdge(HEdge * he) { return Vertex::he = he; }
  int SetIndex(int index) { return Vertex::index = index; }

  int SetFlag(int value) { return Vertex::flag = value; }

  bool IsValid() const { return valid; }
  bool SetValid(bool b) { return valid = b; }

  // check for boundary vertex
  bool IsBoundary() const {
    OneRingHEdge ring(this);
    HEdge *curr = NULL;
    while ((curr=ring.NextHEdge())) if (curr->IsBoundary()) return true;
    return false;
  }

  // compute the valence (# of neighbor vertices)
  int Valence() const {
    int count = 0;
    OneRingVertex ring(this);
    Vertex *curr = NULL;
    while ((curr=ring.NextVertex())) count++;
    return count;
  }
};

////////// class Face //////////
class Face {
private:
  HEdge * he;
  bool valid;
public:
  // constructor
  Face() : he(NULL), valid(true) { }

  // access function
  HEdge * HalfEdge() const { return he; }
  HEdge * SetHalfEdge(HEdge * he) { return Face::he = he; }

  // check for boundary face
  bool IsBoundary() {
    HEdge *curr = he;
    do {
      if (curr->Twin()->IsBoundary()) return true;
      curr = curr->Next();
    } while (curr != he);
    return false;
  }
  bool SetValid(bool b) { return valid = b; }
  bool IsValid() const { return valid;}
};

////////// class Mesh //////////
class Mesh {
public:
  typedef std::tr1::unordered_map<Vertex*, int> VertexIDMap;
  typedef std::tr1::unordered_map<Face*, int> FaceIDMap;

  // constructor & destructors
  Mesh() { }
  ~Mesh() { Clear(); }

  // access functions
  const HEdgeList & Edges() const { return heList; }
  const VertexList & Vertices() const { return vList; }
  const FaceList & Faces() const { return fList; }

  HEdgeList & Edges() { return heList; }
  VertexList & Vertices() { return vList; }
  FaceList & Faces() { return fList; }

  inline int faceID(Face * const face) const {
    return face_id_map_.find(face)->second;
  }
  inline int vertexID(Vertex * const vertex) const {
    return vertex_id_map_.find(vertex)->second;
  }
  inline zeta::V3i triangleVertexID(Face * const face) const {
    HEdge * edge = face->HalfEdge();
    zeta::V3i tri;
    tri[0] = vertexID(edge->Start());
    edge = edge->Next(); tri[1] = vertexID(edge->Start());
    edge = edge->Next(); tri[2] = vertexID(edge->Start());
    return tri;
  }

  V3d MinCoord() const {
    V3d minCoord(std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
    for (size_t i=0; i<vList.size(); i++) {
      V3d const & pos = (vList[i])->Position();
      if (pos.x() < minCoord.x()) minCoord.x() = pos.x();
      if (pos.y() < minCoord.y()) minCoord.y() = pos.y();
      if (pos.z() < minCoord.z()) minCoord.z() = pos.z();
    }
    return minCoord;
  }

  V3d MaxCoord() const {
    V3d maxCoord(-std::numeric_limits<double>::max(), -std::numeric_limits<double>::max(), -std::numeric_limits<double>::max());
    for (size_t i=0; i<vList.size(); i++) {
      V3d const & pos = (vList[i])->Position();
      if (pos.x() > maxCoord.x()) maxCoord.x() = pos.x();
      if (pos.y() > maxCoord.y()) maxCoord.y() = pos.y();
      if (pos.z() > maxCoord.z()) maxCoord.z() = pos.z();
    }
    return maxCoord;
  }

  bool ExtractFaces(std::vector<int> const & vertex_ids
    , std::vector<zeta::V3d> & vertex
    , std::vector<int> & indices
    , std::vector<int> & map_to_global_indices);

  bool LoadObjFile(const char * filename);
  bool FromOBJMesh(zeta::OBJMesh const & mesh);
  bool FromPLYMesh(zeta::PLYMesh const & mesh);
  bool FromRawTriangles(std::vector<zeta::V3d> const & vertices, std::vector<int> const & indices);

  bool ToOBJMesh(zeta::OBJMesh & mesh) const;
  bool ToPLYMesh(zeta::PLYMesh & mesh) const;
  bool ToRawTriangles(std::vector<zeta::V3d> & vertices, std::vector<int> & indices) const;

  void DisplayMeshInfo();

  // you may use the following fuction for smooth color coding
  void HSVtoRGB( double *r, double *g, double *b, double h, double s=1 , double v=1 ); // r/g/b/s/v~[0,1], h~[0,360]

  /************************************************************************/
  /* please implement the following functions */
  void ComputeVertexNormals();
  void ComputeVertexCurvatures();
  void UmbrellaSmooth();
  void ImplicitUmbrellaSmooth();
  /************************************************************************/

  // additional helper functions
  // implement them if in needed
  int CountBoundaryLoops();
  int CountConnectedComponents();


  size_t VertexNumber() {return this->vList.size();}
  size_t FaceNumber() {return this->fList.size();}
  size_t BoundaryLoopNumber() {
    // clear heList flag
    for(size_t i=0; i<this->heList.size(); i++){this->heList[i]->SetFlag(false);}

    // boundary loops
    int bloop_count = 0;
    for(size_t i=0; i<this->heList.size(); i++){
      HEdge* curE = this->heList[i];
      if (curE->Start() == NULL) continue;
      if (!curE->IsBoundary()) continue;
      if (curE->Flag()) continue;
      bloop_count++;
      while(!curE->Flag()){ // not visited edge
        //std::cout << "(" << curE->Start()->Index() << curE->End()->Index() << curE << ")";
        if (!curE->IsBoundary())
          std::cout << "Boundary connected to non-boundary.\n";
        curE->SetFlag(true);
        curE = curE->Next();
      }
      //std::cout << std::endl;
    }
    return bloop_count;
  }
  bool HasSingularVertex() {
    for (size_t i = 0; i < vList.size(); ++i) {
      Vertex *v = vList[i];
      int b = 0;
      for (size_t j = 0; j < v->adjHEdges.size(); ++j) {
        HEdge *e = v->adjHEdges[j];
        if (e->Start() == NULL) continue;
        if (!e->IsBoundary()) continue;
        ++b;
      }
      if (b > 1) return true;
    }
    return false;
  }

  // functions for loading obj files,
  // you DO NOT need to understand and use them
  inline void AddVertex(Vertex *v) { vList.push_back(v); vertex_id_map_[v] = static_cast<int>(vList.size())-1; v->SetIndex(vList.size() - 1);}
  Face* AddFace(int v1, int v2, int v3);
  bool RemoveLastFace();

  void Clear() {
    size_t i;
    for (i=0; i<heList.size(); i++) delete heList[i];
    for (i=0; i<vList.size(); i++) delete vList[i];
    for (i=0; i<fList.size(); i++) delete fList[i];
    heList.clear();
    vList.clear();
    fList.clear();
    face_id_map_.clear();
    vertex_id_map_.clear();
  }
  void ClearFaces() {
    size_t i;
    for (i=0; i<heList.size(); i++) delete heList[i];
    for (i=0; i<vList.size(); i++) {vList[i]->SetHalfEdge(NULL); vList[i]->adjHEdges.clear();}
    for (i=0; i<fList.size(); i++) delete fList[i];
    heList.clear();
    fList.clear();
    face_id_map_.clear();
  }

  void SetupAuxHalfEdgeStructure();

  HEdgeList heList;		// list of half edges
  VertexList vList;		// list of vertices
  FaceList fList;			// list of faces

  VertexIDMap vertex_id_map_;
  FaceIDMap face_id_map_;
};

// other helper functions
inline void SetPrevNext(HEdge *e1, HEdge *e2) {
  e1->SetNext(e2); e2->SetPrev(e1);
  //  std::cout << "PN(" << (e1->Start()?e1->Start()->Index():-1) << ","
  //            << (e2->Start()?e2->Start()->Index():-1) << ")\n";
}
inline void SetTwin(HEdge *e1, HEdge *e2) {e1->SetTwin(e2); e2->SetTwin(e1);}
inline void SetFace(Face *f, HEdge *e) {f->SetHalfEdge(e); e->SetFace(f);}
}
