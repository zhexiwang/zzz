#pragma once

// Separate link so when change a lib, only link needs redo.
#include "zGraphicsConfig.hpp"

#ifndef ZZZ_NO_PRAGMA_LIB

#ifdef ZZZ_COMPILER_MSVC

// GLEW will be built along with the engine
#ifdef ZZZ_DEBUG
  #ifndef ZZZ_OS_WIN64
    #pragma comment(lib,"GlewD.lib")
  #else
    #pragma comment(lib,"GlewD_X64.lib")
  #endif // ZZZ_OS_WIN64
#else
  #ifndef ZZZ_OS_WIN64
    #pragma comment(lib,"Glew.lib")
  #else
    #pragma comment(lib,"Glew_X64.lib")
  #endif // ZZZ_OS_WIN64
#endif  // ZZZ_DEBUG

#ifdef ZZZ_LIB_OPENGL
  #pragma comment(lib,"opengl32.lib")
  #pragma comment(lib,"glu32.lib")
#endif // ZZZ_LIB_OPENGL

#ifdef ZZZ_LIB_ANTTWEAKBAR
  #ifndef ZZZ_OS_WIN64
    #pragma comment(lib, "AntTweakBar.lib")
  #else
    #pragma comment(lib, "AntTweakBar64.lib")
  #endif // ZZZ_OS_WIN64
#endif // ZZZ_LIB_ANTTWEAKBAR

#ifdef ZZZ_LIB_GLFW
#ifdef ZZZ_DEBUG
#pragma comment(lib, "GLFWD.lib")
#else
#pragma comment(lib, "GLFW.lib")
#endif /// ZZZ_DEBUG
#endif // ZZZ_LIB_GLFW

// Freeglut is included in zzzEngine
#ifndef ZZZ_OS_WIN64
#ifdef ZZZ_DEBUG
  #pragma comment(lib,"freeglutD.lib")
#else
  #pragma comment(lib,"freeglut.lib")
#endif
#else
#ifdef ZZZ_DEBUG
  #pragma comment(lib,"freeglutD_X64.lib")
#else
  #pragma comment(lib,"freeglut_X64.lib")
#endif
#endif // ZZZ_OS_WIN64

#ifdef ZZZ_LIB_GLUI
  #pragma comment(lib,"glui32.lib")
#endif  // ZZZ_LIB_GLUI

#ifdef ZZZ_LIB_QT4
#if !defined(ZZZ_OS_WIN64)
  #ifdef ZZZ_DEBUG
    #pragma comment(lib,"QtGuid4.lib")
    #pragma comment(lib,"QtCored4.lib")
    #pragma comment(lib,"QtOpenGLd4.lib")
  #else
    #pragma comment(lib,"QtGui4.lib")
    #pragma comment(lib,"QtCore4.lib")
    #pragma comment(lib,"QtOpenGL4.lib")
  #endif
#else
  #ifdef ZZZ_DEBUG
    #pragma comment(lib,"QtGui_X64d4.lib")
    #pragma comment(lib,"QtCore_X64d4.lib")
    #pragma comment(lib,"QtOpenGL_X64d4.lib")
  #else
    #pragma comment(lib,"QtGui_X644.lib")
    #pragma comment(lib,"QtCore_X644.lib")
    #pragma comment(lib,"QtOpenGL_X644.lib")
  #endif
#endif  // ZZZ_OS_WIN64
#endif  // ZZZ_LIB_QT4

#ifdef ZZZ_OS_WIN
#pragma comment(lib, "WINMM.LIB")
#endif
#endif // ZZZ_COMPILER_MSVC

#endif  // ZZZ_NO_PRAGMA_LIB
