#include "OneObjEnvRenderer.hpp"

namespace zzz {
void OneObjEnvRenderer::AfterOnSize(unsigned int nType, int cx, int cy) {
  camera_.OnSize(nType, cx, cy);
  obj_arcball_.OnSize(nType, cx, cy);
  env_arcball_.OnSize(nType, cx, cy);
}

void OneObjEnvRenderer::DrawObj() {
  glColor3f(1.0,1.0,1.0);
  glBegin(GL_QUADS);
  glVertex3d(-1,-1,0);
  glVertex3d(1,-1,0);
  glVertex3d(1,1,0);
  glVertex3d(-1,1,0);
  glEnd();
}

void OneObjEnvRenderer::SetupCamera() {
  camera_.ApplyGL();
  obj_arcball_.ApplyGL();
}

OneObjEnvRenderer::OneObjEnvRenderer() {
  current_obj_arcball_=&obj_arcball_;
  current_env_arcball_=&env_arcball_;
}

void OneObjEnvRenderer::OnLButtonDown(unsigned int nFlags,int x,int y) {
  if (!lock_arcball_) {
    obj_arcball_.OnLButtonDown(nFlags, x, y);
    env_arcball_.OnLButtonDown(nFlags, x, y);
  }
}

void OneObjEnvRenderer::OnLButtonUp(unsigned int nFlags,int x,int y) {
  obj_arcball_.OnLButtonUp(nFlags, x, y);
  env_arcball_.OnLButtonUp(nFlags, x, y);
}

void OneObjEnvRenderer::OnRButtonDown(unsigned int nFlags,int x,int y) {
  if (!lock_arcball_) {
    env_arcball_.OnLButtonDown(nFlags, x, y);
  }
}

void OneObjEnvRenderer::OnRButtonUp(unsigned int nFlags,int x,int y) {
  env_arcball_.OnLButtonUp(nFlags, x, y);
}

void OneObjEnvRenderer::OnMButtonDown(unsigned int nFlags,int x,int y) {
  if (!lock_arcball_) {
    obj_arcball_.OnLButtonDown(nFlags, x, y);
  }
}

void OneObjEnvRenderer::OnMButtonUp(unsigned int nFlags,int x,int y) {
  obj_arcball_.OnLButtonUp(nFlags, x, y);
}

void OneObjEnvRenderer::OnMouseMove(unsigned int nFlags,int x,int y) {
  if (!lock_arcball_) {
    obj_arcball_.OnMouseMove(nFlags, x, y);
    env_arcball_.OnMouseMove(nFlags, x, y);
    Redraw();
  }
}

void OneObjEnvRenderer::ResetArcball() {
  obj_arcball_.Reset();
  env_arcball_.Reset();
}

bool OneObjEnvRenderer::InitData() {
  env_arcball_.Init(this);
  obj_arcball_.Init(this);
  return ArcBallRenderer::InitData();
}

}