#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "Renderer.hpp"
namespace zzz{

class ZGRAPHICS_CLASS SwitchRenderer : public Renderer
{
public:
  SwitchRenderer(void);
  ~SwitchRenderer(void);

  bool AddRenderer(Renderer *, const int idx);
  bool SwitchTo(const int idx);

  virtual void SetContext(Context *context);
  virtual bool RenderScene();
  virtual bool InitData();
  virtual bool InitState();
  virtual void OnMouseMove(unsigned int nFlags,int x,int y);
  virtual void OnLButtonDown(unsigned int nFlags,int x,int y);
  virtual void OnLButtonUp(unsigned int nFlags,int x,int y);
  virtual void OnRButtonDown(unsigned int nFlags,int x,int y);
  virtual void OnRButtonUp(unsigned int nFlags,int x,int y);
  virtual void OnMButtonDown(unsigned int nFlags,int x,int y);
  virtual void OnMButtonUp(unsigned int nFlags,int x,int y);
  virtual void OnMouseWheel(unsigned int nFlags, int zDelta, int x,int y);
  virtual void OnSize(unsigned int nType, int cx, int cy);
  virtual void OnChar(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
  virtual void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
  virtual void OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
  virtual void OnIdle();

protected:
  vector<pair<int, Renderer*> > renderers_;
  vector<int> statuses_;
  Renderer *currenderer_;

};
}
