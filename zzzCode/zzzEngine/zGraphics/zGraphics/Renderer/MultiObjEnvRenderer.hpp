#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "ArcBallRenderer.hpp"

namespace zzz {
class ZGRAPHICS_CLASS MultiObjEnvRenderer : public ArcBallRenderer
{
public:
  MultiObjEnvRenderer();
  void ResetArcball();

  int current_obj_arcball_idx_;
  bool allArcBall_;
  ArcBallGUI obj_arcball_[10];        //arcball for object rotation
  ArcBallGUI env_arcball_;          //arcball for environment rotation

protected:
  void CreateMsg();
  void DrawObj();
  void SetupCamera();
  void AfterOnSize(unsigned int nType, int cx, int cy);
  bool InitData();

public:
  void OnMouseMove(unsigned int nFlags,int x,int y);
  void OnLButtonDown(unsigned int nFlags,int x,int y);
  void OnLButtonUp(unsigned int nFlags,int x,int y);
  void OnRButtonDown(unsigned int nFlags,int x,int y);
  void OnRButtonUp(unsigned int nFlags,int x,int y);
  void OnMButtonDown(unsigned int nFlags,int x,int y);
  void OnMButtonUp(unsigned int nFlags,int x,int y);
  void OnChar(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
};


}