#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "Renderer.hpp"
#include "../Graphics/Camera.hpp"
#include <zCore/Math/Vector2.hpp>
#include "../GraphicsGUI/MovingCameraGUI.hpp"

namespace zzz{
class ZGRAPHICS_CLASS CameraRenderer : public Renderer
{
public:
  CameraRenderer();

  void DrawObj();
  void SetupCamera();
  void AfterOnSize(unsigned int nType, int cx, int cy);
  bool InitData();

  void OnMouseMove(unsigned int nFlags,int x,int y);
  void OnLButtonDown(unsigned int nFlags,int x,int y);
  void OnMouseWheel(unsigned int nFlags, int zDelta, int x,int y);
  void OnChar(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);

  MovingCameraGUI camera_;
};
}