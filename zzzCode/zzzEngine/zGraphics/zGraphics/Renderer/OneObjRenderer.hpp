#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "ArcBallRenderer.hpp"
#include "../Graphics/ArcBall.hpp"
#include "../Graphics/Transformation.hpp"

namespace zzz {
class ZGRAPHICS_CLASS OneObjRenderer : public ArcBallRenderer {
public:
  OneObjRenderer();
  void ResetArcball();
  ArcBallGUI obj_arcball_;

protected:
  void CreateMsg();
  void AfterOnSize(unsigned int nType, int cx, int cy);
  void DrawObj();
  void SetupCamera();
  bool InitData();
  void UpdateTransformation();
  Transformationd trans_;

public:
  void OnMouseMove(unsigned int nFlags,int x,int y);
  void OnLButtonDown(unsigned int nFlags,int x,int y);
  void OnLButtonUp(unsigned int nFlags,int x,int y);
  void OnRButtonUp(unsigned int nFlags,int x,int y);
  void OnMButtonUp(unsigned int nFlags,int x,int y);

  void OnSize(unsigned int nType, int cx, int cy);
};


}