#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zCore/Math/Vector2.hpp>
#include "../Graphics/Coordinate.hpp"
#include "../Resource/Mesh/Mesh.hpp"
#include "../Resource/Texture/TextureSpecify.hpp"
#include "../Resource/Shader/ShaderSpecify.hpp"
#include "../FBO/FBO.hpp"
#include "../FBO/RenderBuffer.hpp"
#include "OneObjRenderer.hpp"

namespace zzz
{
class ZGRAPHICS_CLASS ControlRenderer : public OneObjRenderer
{
public:
  ControlRenderer();

  virtual bool Draw();
  virtual bool InitData();

  virtual void OnMouseMove(unsigned int nFlags,int x,int y);
  virtual void OnLButtonDown(unsigned int nFlags,int x,int y);
  virtual void OnLButtonUp(unsigned int nFlags,int x,int y);
  virtual void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
  virtual void OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
  virtual void OnSize(unsigned int nType, int cx, int cy);

  bool LoadObj(const string & filename);

protected:
  vector<Cartesian3DCoordf> ControlPoints_;

  vector<Cartesian3DCoordf> selected_;
  int FindNearest(int x,int y);
  int FindNearestSelected(int x,int y);
  void RenderIdMap();
  vector<zuint> mesh_group_end_;
  TriMesh *mesh_;
  FBO *fbo_;
  Texture2D3ub *target_;
  Renderbuffer *depth_;
  GLdouble modelMatrix_[16], projMatrix_[16];
  GLint viewport_[4];

  zuint keyDown_;
  int overpoint_,selectedpoint_;
  double selectedx_,selectedy_,selectedz_;
  double selectedwx_,selectedwy_;
  GLUquadricObj* quadric;

  bool showObj_,showCpt_;

  bool leftButton_, middleButton_;

};
}
