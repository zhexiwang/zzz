#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zCore/common.hpp>
#include "../GraphicsGUI/ArcBallGUI.hpp"
#include "Renderer.hpp"

namespace zzz {
class ZGRAPHICS_CLASS ArcBallRenderer : public Renderer {
public:
  ArcBallRenderer();
  virtual ~ArcBallRenderer();

  ArcBallGUI *current_obj_arcball_,*current_env_arcball_;
  bool lock_arcball_;

};


}