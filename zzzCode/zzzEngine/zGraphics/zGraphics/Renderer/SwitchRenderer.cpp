#include "SwitchRenderer.hpp"
#include <zCore/Utility/Tools.hpp>

namespace zzz{
#define STATUS_NEED_RESIZE (1)

SwitchRenderer::SwitchRenderer(void)
:currenderer_(NULL)
{
}

SwitchRenderer::~SwitchRenderer(void)
{
}

bool SwitchRenderer::AddRenderer(Renderer *renderer, const int idx)
{
  for (zuint i=0; i<renderers_.size(); i++)
    if (renderers_[i].first==idx) 
      return false;
  renderers_.push_back(make_pair(idx,renderer));
  statuses_.push_back(0);
  return true;
}

bool SwitchRenderer::SwitchTo(const int idx)
{
  for (zuint i=0; i<renderers_.size(); i++)
    if (renderers_[i].first==idx) 
    {
      currenderer_=renderers_[i].second;
      if (CheckBit(statuses_[i],STATUS_NEED_RESIZE))
      {
        currenderer_->OnSize(0,width_,height_);
        ClearBit(statuses_[i],STATUS_NEED_RESIZE);
      }
      return true;
    }
  return false;
}

void SwitchRenderer::SetContext(Context *context)
{
  for (zuint i=0; i<renderers_.size(); i++)
    renderers_[i].second->SetContext(context);
  context_=context;
}

bool SwitchRenderer::RenderScene()
{
  if (currenderer_==NULL) return true;
  return currenderer_->RenderScene();
}

bool SwitchRenderer::InitData()
{
  for (zuint i=0; i<renderers_.size(); i++)
    renderers_[i].second->InitData();
  return true;
}

bool SwitchRenderer::InitState()
{
  for (zuint i=0; i<renderers_.size(); i++)
    renderers_[i].second->InitState();
  return true;
}

void SwitchRenderer::OnMouseMove(unsigned int nFlags,int x,int y)
{
  if (currenderer_==NULL) return;
  currenderer_->OnMouseMove( nFlags, x, y);
}

void SwitchRenderer::OnLButtonDown(unsigned int nFlags,int x,int y)
{
  if (currenderer_==NULL) return;
  currenderer_->OnLButtonDown( nFlags, x, y);
}

void SwitchRenderer::OnLButtonUp(unsigned int nFlags,int x,int y)
{
  if (currenderer_==NULL) return;
  currenderer_->OnLButtonUp( nFlags, x, y);
}

void SwitchRenderer::OnRButtonDown(unsigned int nFlags,int x,int y)
{
  if (currenderer_==NULL) return;
  currenderer_->OnRButtonDown( nFlags, x, y);
}

void SwitchRenderer::OnRButtonUp(unsigned int nFlags,int x,int y)
{
  if (currenderer_==NULL) return;
  currenderer_->OnRButtonUp( nFlags, x, y);
}

void SwitchRenderer::OnMButtonDown(unsigned int nFlags,int x,int y)
{
  if (currenderer_==NULL) return;
  currenderer_->OnMButtonDown( nFlags, x, y);
}

void SwitchRenderer::OnMButtonUp(unsigned int nFlags,int x,int y)
{
  if (currenderer_==NULL) return;
  currenderer_->OnMButtonUp( nFlags, x, y);
}

void SwitchRenderer::OnMouseWheel(unsigned int nFlags, int zDelta, int x,int y)
{
  if (currenderer_==NULL) return;
  currenderer_->OnMouseWheel( nFlags, zDelta, x, y);
}

void SwitchRenderer::OnSize(unsigned int nType, int cx, int cy)
{
  Renderer::OnSize(nType,cx,cy);
  for (zuint i=0; i<renderers_.size(); i++)
    if (renderers_[i].second==currenderer_)
      currenderer_->OnSize(nType,cx,cy);
    else
      SetBit(statuses_[i],STATUS_NEED_RESIZE);
}

void SwitchRenderer::OnChar(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags)
{
  if (currenderer_==NULL) return;
  currenderer_->OnChar(nChar, nRepCnt, nFlags);
}

void SwitchRenderer::OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags)
{
  if (currenderer_==NULL) return;
  currenderer_->OnKeyDown(nChar, nRepCnt, nFlags);
}

void SwitchRenderer::OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags)
{
  if (currenderer_==NULL) return;
  currenderer_->OnKeyUp(nChar, nRepCnt, nFlags);
}

void SwitchRenderer::OnIdle()
{
  if (currenderer_==NULL) return;
  currenderer_->OnIdle();
}
}

