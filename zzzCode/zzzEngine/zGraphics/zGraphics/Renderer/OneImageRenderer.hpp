#pragma once
#include "Vis2DRenderer.hpp"
#include <zImage/Image/Image.hpp>
#include <zCore/Math/Vector2.hpp>

namespace zzz{
/// To manipulate A SINGLE image
/// some functions are overriden to satisfy the relative to image originate
template<typename T>
class OneImageRenderer : public Vis2DRenderer, public GraphicsHelper {
public:
  OneImageRenderer():image_(NULL){}

  Vector2d GetHoverPixel(int mouse_x, int mouse_y) const {
    if (image_==NULL) return Vector2d(-1,-1);
    Vector2d p=Vis2DRenderer::GetHoverPixel(mouse_x, mouse_y);
    p[0]-=image_->orir_;
    p[1]-=image_->oric_;
    return p;
  }

  /// ATTENTION this cannot be inside glBegin~glEnd
  Vector3d UnProjectRelative(double winx,double winy) const {
    if (image_==NULL) return Vector3d(0);
    return Vis2DRenderer::UnProjectRelative(winx+image_->oric_, winy+image_->orir_);
  }

  void DrawObj() {
    if (image_!=NULL) DrawImage(*image_);
  }
  Image<T> *image_;
};
}