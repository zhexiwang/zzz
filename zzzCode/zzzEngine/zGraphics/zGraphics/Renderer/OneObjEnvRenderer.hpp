#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "ArcBallRenderer.hpp"

namespace zzz {
class ZGRAPHICS_CLASS OneObjEnvRenderer : public ArcBallRenderer {
public:
  OneObjEnvRenderer();
  void ResetArcball();

  int current_obj_arcball_idx_;
  bool allArcBall_;
  ArcBallGUI obj_arcball_;          //arcball for object rotation
  ArcBallGUI env_arcball_;          //arcball for environment rotation

protected:
  void CreateMsg();
  void AfterOnSize(unsigned int nType, int cx, int cy);
  void DrawObj();
  void SetupCamera();
  bool InitData();

public:
  void OnMouseMove(unsigned int nFlags,int x,int y);
  void OnLButtonDown(unsigned int nFlags,int x,int y);
  void OnLButtonUp(unsigned int nFlags,int x,int y);
  void OnRButtonDown(unsigned int nFlags,int x,int y);
  void OnRButtonUp(unsigned int nFlags,int x,int y);
  void OnMButtonDown(unsigned int nFlags,int x,int y);
  void OnMButtonUp(unsigned int nFlags,int x,int y);
  void OnSize(unsigned int nType, int cx, int cy);
};


}