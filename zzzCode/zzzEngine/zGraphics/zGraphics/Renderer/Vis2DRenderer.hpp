#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "Renderer.hpp"
#include "../Graphics/GraphicsHelper.hpp"
#include <zCore/Utility/Timer.hpp>

//For showing images
//no projective matrix
//no zooming
//middle mouse button to move around

namespace zzz {
class ZGRAPHICS_CLASS Vis2DRenderer : public Renderer {
public:
  Vis2DRenderer();

  virtual bool InitState();

  virtual void OnMouseMove(unsigned int nFlags,int x,int y);
  virtual void OnMButtonDown(unsigned int nFlags,int x,int y);
  virtual void OnMButtonUp(unsigned int nFlags,int x,int y);
  virtual void OnMouseWheel(unsigned int nFlags, int zDelta, int x,int y);
  virtual void SetupCamera();
  virtual void DrawObj();
  void SetCenter(int width,int height);

  //set raster position relative to original screen coordinate
  //zoom will be considered
  //left bottom corner of original screen is zero, right x, up y
  //it will move as origin moves
  void SetRasterPosRelative(double x, double y) const;

  //ATTENTION this cannot be inside glBegin~glEnd
  Vector3d UnProjectRelative(double winx,double winy) const;

  //return row and column of the pixel which mouse is hovering on
  //the image should drawpixels at (x,y)
  Vector2d GetHoverPixel(int mouse_x, int mouse_y, int img_x=0, int img_y=0) const;

  //to draw 3d stuff according to 2d position
  void Draw2DPoint(double x, double y, const Colorf& color, float size);
  void Draw2DLine(double x0, double y0, double x1, double y1, const zzz::Colorf& color, float size);

protected:
  bool allowmove_,allowzoom_;
  double zoomratio_;
  int posx_,posy_;

private:
  Timer middletimer_;
  int lastx_,lasty_;
  bool middleButton_;
};
}