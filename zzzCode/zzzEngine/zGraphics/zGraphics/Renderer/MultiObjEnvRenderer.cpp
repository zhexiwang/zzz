#include "MultiObjEnvRenderer.hpp"
#include "../Graphics/ColorDefine.hpp"
#include <zCore/Utility/StringPrintf.hpp>

namespace zzz {
void MultiObjEnvRenderer::DrawObj()
{
  ColorDefine::white.ApplyGL();
  glBegin(GL_QUADS);
  glVertex3d(-1,-1,0);
  glVertex3d(1,-1,0);
  glVertex3d(1,1,0);
  glVertex3d(-1,1,0);
  glEnd();
}

void MultiObjEnvRenderer::SetupCamera()
{
  camera_.ApplyGL();
  obj_arcball_[current_obj_arcball_idx_].ApplyGL();
}


MultiObjEnvRenderer::MultiObjEnvRenderer()
{
  allArcBall_=false;
  lock_arcball_=false;
  current_obj_arcball_idx_=0;
  current_obj_arcball_=&obj_arcball_[0];
  current_env_arcball_=&env_arcball_;
}

void MultiObjEnvRenderer::OnLButtonDown(unsigned int nFlags,int x,int y)
{
  if (!lock_arcball_) {
    if (allArcBall_) {
      for (int i=0; i<10; i++) {
        obj_arcball_[i].OnLButtonDown(nFlags, x, y);
      }
    } else {
      obj_arcball_[current_obj_arcball_idx_].OnLButtonDown(nFlags, x, y);
    }
    env_arcball_.OnLButtonDown(nFlags, x, y);
  }
}

void MultiObjEnvRenderer::OnLButtonUp(unsigned int nFlags,int x,int y)
{
  for (int i=0; i<10; i++)
    obj_arcball_[i].OnLButtonUp(nFlags, x, y);
  env_arcball_.OnLButtonUp(nFlags, x, y);
}

void MultiObjEnvRenderer::OnRButtonDown(unsigned int nFlags,int x,int y)
{
  if (!lock_arcball_) {
    env_arcball_.OnLButtonDown(nFlags, x, y);
  }
}

void MultiObjEnvRenderer::OnRButtonUp(unsigned int nFlags,int x,int y)
{
  env_arcball_.OnLButtonUp(nFlags, x, y);
}

void MultiObjEnvRenderer::OnMButtonDown(unsigned int nFlags,int x,int y)
{
  if (!lock_arcball_) {
    if (allArcBall_) {
      for (int i=0; i<10; i++) {
        obj_arcball_[i].OnLButtonDown(nFlags, x, y);
      }
    } else {
      obj_arcball_[current_obj_arcball_idx_].OnLButtonDown(nFlags, x, y);
    }
  }
}

void MultiObjEnvRenderer::OnMButtonUp(unsigned int nFlags,int x,int y)
{
  for (int i=0; i<10; i++)
    obj_arcball_[i].OnLButtonUp(nFlags, x, y);
}

void MultiObjEnvRenderer::OnMouseMove(unsigned int nFlags,int x,int y)
{
  if (!lock_arcball_) {
    if (allArcBall_) {
      for (int i=0; i<10; i++)
        obj_arcball_[i].OnMouseMove(nFlags, x, y);
    } else
      obj_arcball_[current_obj_arcball_idx_].OnMouseMove(nFlags, x, y);
    env_arcball_.OnMouseMove(nFlags, x, y);
  }
  Redraw();
}

void MultiObjEnvRenderer::OnChar(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags)
{
  switch(nChar) {
  case ZZZKEY_NUM_DOT:  //'numpad .'
    allArcBall_=true;
    printf("All the ArcBalls are controled\n");
    Redraw();
    break;
  case ZZZKEY_NUM_0:
    current_obj_arcball_idx_=0;allArcBall_=false;current_obj_arcball_=&(obj_arcball_[current_obj_arcball_idx_]);
    printf("Current ArcBall: %d\n",current_obj_arcball_idx_);
    Redraw();
    break;
  case ZZZKEY_NUM_1:
    current_obj_arcball_idx_=1;allArcBall_=false;current_obj_arcball_=&(obj_arcball_[current_obj_arcball_idx_]);
    printf("Current ArcBall: %d\n",current_obj_arcball_idx_);
    Redraw();
    break;
  case ZZZKEY_NUM_2:
    current_obj_arcball_idx_=2;allArcBall_=false;current_obj_arcball_=&(obj_arcball_[current_obj_arcball_idx_]);
    printf("Current ArcBall: %d\n",current_obj_arcball_idx_);
    Redraw();
    break;
  case ZZZKEY_NUM_3:
    current_obj_arcball_idx_=3;allArcBall_=false;current_obj_arcball_=&(obj_arcball_[current_obj_arcball_idx_]);
    printf("Current ArcBall: %d\n",current_obj_arcball_idx_);
    Redraw();
    break;
  case ZZZKEY_NUM_4:
    current_obj_arcball_idx_=4;allArcBall_=false;current_obj_arcball_=&(obj_arcball_[current_obj_arcball_idx_]);
    printf("Current ArcBall: %d\n",current_obj_arcball_idx_);
    Redraw();
    break;
  case ZZZKEY_NUM_5:
    current_obj_arcball_idx_=5;allArcBall_=false;current_obj_arcball_=&(obj_arcball_[current_obj_arcball_idx_]);
    printf("Current ArcBall: %d\n",current_obj_arcball_idx_);
    Redraw();
    break;
  case ZZZKEY_NUM_6:
    current_obj_arcball_idx_=6;allArcBall_=false;current_obj_arcball_=&(obj_arcball_[current_obj_arcball_idx_]);
    printf("Current ArcBall: %d\n",current_obj_arcball_idx_);
    Redraw();
    break;
  case ZZZKEY_NUM_7:
    current_obj_arcball_idx_=7;allArcBall_=false;current_obj_arcball_=&(obj_arcball_[current_obj_arcball_idx_]);
    printf("Current ArcBall: %d\n",current_obj_arcball_idx_);
    Redraw();
    break;
  case ZZZKEY_NUM_8:
    current_obj_arcball_idx_=8;allArcBall_=false;current_obj_arcball_=&(obj_arcball_[current_obj_arcball_idx_]);
    printf("Current ArcBall: %d\n",current_obj_arcball_idx_);
    Redraw();
    break;
  case ZZZKEY_NUM_9:
    current_obj_arcball_idx_=9;allArcBall_=false;current_obj_arcball_=&(obj_arcball_[current_obj_arcball_idx_]);
    printf("Current ArcBall: %d\n",current_obj_arcball_idx_);
    Redraw();
  }
}

void MultiObjEnvRenderer::AfterOnSize(unsigned int nType, int cx, int cy)
{
  for (int i=0; i<10; i++)
    obj_arcball_[i].OnSize(nType, cx, cy);
  env_arcball_.OnSize(nType, cx, cy);
}

void MultiObjEnvRenderer::ResetArcball()
{
  env_arcball_.Reset();
  for (zuint i=0; i<10; i++)
    obj_arcball_[i].Reset();
}

void MultiObjEnvRenderer::CreateMsg()
{
  SStringPrintf(msg_,"Current ArcBall: %d, Res: %d x %d, SPF: %lf = FPS: %lf",allArcBall_?123:current_obj_arcball_idx_,width_,height_,SPF_,FPS_);
}

bool MultiObjEnvRenderer::InitData()
{
  env_arcball_.Init(this);
  for (zuint i=0; i<10; i++)
    obj_arcball_[i].Init(this);
  return ArcBallRenderer::InitData();
}


}
