#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "../FBO/FBO.hpp"
#include "../FBO/RenderBuffer.hpp"
#include <zCore/common.hpp>
#include "../Resource/Texture/TextureSpecify.hpp"
#include "../Resource/Mesh/Mesh.hpp"
#include "../Resource/Shader/Shader.hpp"
#include <zCore/Utility/Singleton.hpp>
#ifdef ZZZ_LIB_BOOST
#include <boost/function.hpp>

namespace zzz{
class ZGRAPHICS_CLASS RendererHelper : public Singleton<RendererHelper> {
public:
  RendererHelper();
  ~RendererHelper();
  void RenderCubemap(TextureCube &t, TriMesh &o,Cartesian3DCoordf &c,Shader &s);
  void RenderCubemap(TextureCube &t, Cartesian3DCoordf &c, boost::function<void (void)> drawfunc);
  void RenderCubemap(Texture2D *t, Cartesian3DCoordf &c, boost::function<void (void)> drawfunc);
  void RenderCubemap(Texture2D *t, Texture2D *depth, Cartesian3DCoordf &c, boost::function<void (void)> drawfunc);
  
  void RenderToTexture(Texture2D &tex, boost::function<void (void)> setupcamera, boost::function<void (void)> drawfunc);
  FBO fbo;
  Texture2D depthbuffer;
  double zNear, zFar;

  static Colorui8 ZuintToColor(zuint32 x);
  static zuint32 ColorToZuint(const Vector3ui8 &c);
  static zuint32 ColorToZuint(const Vector4ui8 &c);
};
}
#endif // ZZZ_LIB_BOOST