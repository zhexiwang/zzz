#include "OneObjRenderer.hpp"
#include <zCore/Utility/StringPrintf.hpp>

namespace zzz {
OneObjRenderer::OneObjRenderer() {
  current_obj_arcball_=&obj_arcball_;
  current_env_arcball_=NULL;
}

void OneObjRenderer::AfterOnSize(unsigned int nType, int cx, int cy) {
  camera_.OnSize(nType, cx, cy);
  obj_arcball_.OnSize(nType, cx, cy);
}

void OneObjRenderer::SetupCamera() {
  camera_.ApplyGL();
  obj_arcball_.ApplyGL();
  trans_.ApplyGL();
}

void OneObjRenderer::OnLButtonDown(unsigned int nFlags,int x,int y) { 
  if (!lock_arcball_) {
    obj_arcball_.OnLButtonDown(nFlags, x, y);
  }
}

void OneObjRenderer::OnLButtonUp( unsigned int nFlags,int x,int y ) {
  ArcBallRenderer::OnLButtonUp(nFlags, x, y);
  UpdateTransformation();
}

void OneObjRenderer::OnRButtonUp( unsigned int nFlags,int x,int y ) {
  ArcBallRenderer::OnRButtonUp(nFlags, x, y);
  UpdateTransformation();
}

void OneObjRenderer::OnMButtonUp( unsigned int nFlags,int x,int y ) {
  ArcBallRenderer::OnLButtonUp(nFlags, x, y);
  obj_arcball_.OnLButtonUp(nFlags, x, y);
  UpdateTransformation();
}

void OneObjRenderer::OnMouseMove(unsigned int nFlags,int x,int y) {
  if (!lock_arcball_) {
    if (obj_arcball_.OnMouseMove(nFlags, x, y)) Redraw();
  }
  ArcBallRenderer::OnMouseMove(nFlags, x, y);
}

void OneObjRenderer::OnSize(unsigned int nType, int cx, int cy) {
  if (cy > 0) {
    obj_arcball_.OnSize(nType, cx,cy);
  }
  ArcBallRenderer::OnSize(nType,cx,cy);
}

void OneObjRenderer::ResetArcball() {
  obj_arcball_.Reset();
}

void OneObjRenderer::CreateMsg() {
  SStringPrintf(msg_,"Res: %d x %d, SPF: %lf = FPS: %lf",width_,height_,SPF_,FPS_);
}

void OneObjRenderer::DrawObj() {
  glColor3f(1.0,1.0,1.0);
  glBegin(GL_QUADS);
  glVertex3d(-1,-1,0);
  glVertex3d(1,-1,0);
  glVertex3d(1,1,0);
  glVertex3d(-1,1,0);
  glEnd();
}

bool OneObjRenderer::InitData() {
  obj_arcball_.Init(this);
  return ArcBallRenderer::InitData();
}

void OneObjRenderer::UpdateTransformation() {
  float zoffset = camera_.Position_[2];
  camera_.SetPosition(camera_.Position_[0], camera_.Position_[1], 0);
  trans_ = camera_.GetGLTransformation().Transposed() * obj_arcball_.GetGLTransform().Transposed() * trans_;
  obj_arcball_.Reset();
  camera_.SetPosition(0, 0, zoffset);
}


}

