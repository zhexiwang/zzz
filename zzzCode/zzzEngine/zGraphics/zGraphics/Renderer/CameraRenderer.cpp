#include "CameraRenderer.hpp"
#include "../Context/Context.hpp"
#include "../Graphics/ColorDefine.hpp"

namespace zzz{
CameraRenderer::CameraRenderer()
{
}

void CameraRenderer::AfterOnSize(unsigned int nType, int cx, int cy)
{
  camera_.OnSize(nType, cx, cy);
}

void CameraRenderer::SetupCamera()
{
  camera_.ApplyGL();
}

void CameraRenderer::OnMouseMove(unsigned int nFlags,int x,int y)
{
  if (camera_.OnMouseMove(nFlags, x, y))
    Redraw();
}

void CameraRenderer::OnLButtonDown(unsigned int nFlags,int x,int y)
{
  if (camera_.OnLButtonDown(nFlags, x, y))
    Redraw();
}

void CameraRenderer::OnMouseWheel(unsigned int nFlags, int zDelta, int x,int y)
{
  if (camera_.OnMouseWheel(nFlags, zDelta, x, y))
    Redraw();
}

void CameraRenderer::OnChar(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags)
{
  if (camera_.OnChar(nChar, nRepCnt, nFlags))
    Redraw();
}

void CameraRenderer::DrawObj()
{
  glBegin(GL_QUADS);
  //wall
  ColorDefine::cyan.ApplyGL();
  glVertex3d(-1,-1,1);
  glVertex3d(1,-1,1);
  glVertex3d(1,1,1);
  glVertex3d(-1,1,1);

  ColorDefine::magenta.ApplyGL();
  glColor3f(1.0,0.0,1.0);
  glVertex3d(-1,-1,-1);
  glVertex3d(1,-1,-1);
  glVertex3d(1,1,-1);
  glVertex3d(-1,1,-1);

  ColorDefine::yellow.ApplyGL();
  glVertex3d(-1,-1,-1);
  glVertex3d(-1,1,-1);
  glVertex3d(-1,1,1);
  glVertex3d(-1,-1,1);

  ColorDefine::blue.ApplyGL();
  glVertex3d(1,-1,-1);
  glVertex3d(1,1,-1);
  glVertex3d(1,1,1);
  glVertex3d(1,-1,1);

  //ceiling
  ColorDefine::black.ApplyGL();
  glVertex3d(1,1,-1);
  glVertex3d(1,1,1);
  glVertex3d(-1,1,1);
  glVertex3d(-1,1,-1);

  //floor
  ColorDefine::white.ApplyGL();
  glVertex3d(1,-1,-1);
  glVertex3d(1,-1,1);
  glVertex3d(-1,-1,1);
  glVertex3d(-1,-1,-1);
  glEnd();

}

bool CameraRenderer::InitData()
{
  return camera_.Init(this) && Renderer::InitData();
}

}