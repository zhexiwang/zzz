#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zCore/common.hpp>
#include "VBODescript.hpp"
#include <zCore/Utility/Uncopyable.hpp>

namespace zzz{

class ZGRAPHICS_CLASS ElementVBO : public Uncopyable {
public:
  ElementVBO();
  ~ElementVBO();
  bool Create(void *data,int numOfElement,VBODescript &vbodesc,GLenum usage=GL_STATIC_DRAW);
  bool Bind();
  bool Unbind();
private:
  bool binded;
  GLuint VBO_;
};

}