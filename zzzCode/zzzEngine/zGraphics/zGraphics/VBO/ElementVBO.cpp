#include "ElementVBO.hpp"

namespace zzz{
ElementVBO::ElementVBO()
  : VBO_(0),
    binded(false) {
}

ElementVBO::~ElementVBO() {
  if (binded)
    Unbind();
  if (VBO_!=0)
    glDeleteBuffers(1,&VBO_);
}

bool ElementVBO::Create(void *data,int numOfElement,VBODescript &vbodesc,GLenum usage/*=GL_STATIC_DRAW*/) {
  if (VBO_==0)
    glGenBuffers(1,&VBO_);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,vbodesc.GetElementSize()*numOfElement,data,usage);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
  return true;
}

bool ElementVBO::Bind() {
  if (VBO_==0)
    return false;
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,VBO_);
  binded=true;
  return true;
}

bool ElementVBO::Unbind() {
  if (VBO_==0)
    return false;
  if (binded==false)
    return false;
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
  binded=false;
  return true;
}
}