#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "../Graphics/Graphics.hpp"

namespace zzz{

//////////////////////////////////////////////////////////////////////////
//There may be something wrong when using unsigned byte type or something like that
//since because of the memory align, sizeof(struct) may not be equal to sum(sizeof(element))
struct ZGRAPHICS_CLASS VBODescript {
  VBODescript(GLenum arraytype,GLint size,GLenum type)
    :ArrayType(arraytype),Size(size),Type(type),Loc(0){}

  GLenum ArrayType; //GL_VERTEX_ARRAY or so on
  GLint Size; //number of component per vertex
  GLenum Type; //GL_FLOAT or so on

  GLint Loc; //the location of attribute

  int GetElementSize();

  static VBODescript Vertex4f;
  static VBODescript Vertex3f;
  static VBODescript Vertex2f;
  static VBODescript Vertex4d;
  static VBODescript Vertex3d;
  static VBODescript Vertex2d;
  static VBODescript Color3f;
  static VBODescript Color3ub;
  static VBODescript Normalf;
  static VBODescript Indexui;
  static VBODescript EdgeFlag;
  static VBODescript TexCoord3f;
  static VBODescript TexCoord2f;

  static VBODescript Attribute1ub;
  static VBODescript Attribute2ub;
  static VBODescript Attribute3ub;
  static VBODescript Attribute4ub;

  static VBODescript Attribute1i;
  static VBODescript Attribute2i;
  static VBODescript Attribute3i;
  static VBODescript Attribute4i;

  static VBODescript Attribute1f;
  static VBODescript Attribute2f;
  static VBODescript Attribute3f;
  static VBODescript Attribute4f;

  //ONLY the following three is available for GL_ELEMENT_ARRAY_BUFFER
  static VBODescript Element3ub;
  static VBODescript Element3us;
  static VBODescript Element3ui;
  static VBODescript Element4ui;
};

}