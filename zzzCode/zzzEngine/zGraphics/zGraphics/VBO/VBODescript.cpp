#include <zCore/common.hpp>
#include "VBODescript.hpp"

namespace zzz
{
VBODescript VBODescript::Vertex4f(GL_VERTEX_ARRAY,4,GL_FLOAT);
VBODescript VBODescript::Vertex3f(GL_VERTEX_ARRAY,3,GL_FLOAT);
VBODescript VBODescript::Vertex2f(GL_VERTEX_ARRAY,2,GL_FLOAT);
VBODescript VBODescript::Vertex4d(GL_VERTEX_ARRAY,4,GL_DOUBLE);
VBODescript VBODescript::Vertex3d(GL_VERTEX_ARRAY,3,GL_DOUBLE);
VBODescript VBODescript::Vertex2d(GL_VERTEX_ARRAY,2,GL_DOUBLE);
VBODescript VBODescript::Color3f(GL_COLOR_ARRAY,3,GL_FLOAT);
VBODescript VBODescript::Color3ub(GL_COLOR_ARRAY,3,GL_UNSIGNED_BYTE);
VBODescript VBODescript::Normalf(GL_NORMAL_ARRAY,3,GL_FLOAT);
VBODescript VBODescript::Indexui(GL_NORMAL_ARRAY,3,GL_UNSIGNED_INT);
VBODescript VBODescript::EdgeFlag(GL_EDGE_FLAG_ARRAY,1,GL_BOOL);
VBODescript VBODescript::TexCoord3f(GL_TEXTURE_COORD_ARRAY,3,GL_FLOAT);
VBODescript VBODescript::TexCoord2f(GL_TEXTURE_COORD_ARRAY,2,GL_FLOAT);
VBODescript VBODescript::Attribute1ub(0,1,GL_UNSIGNED_BYTE);
VBODescript VBODescript::Attribute2ub(0,2,GL_UNSIGNED_BYTE);
VBODescript VBODescript::Attribute3ub(0,3,GL_UNSIGNED_BYTE);
VBODescript VBODescript::Attribute4ub(0,4,GL_UNSIGNED_BYTE);
VBODescript VBODescript::Attribute1i(0,1,GL_INT);
VBODescript VBODescript::Attribute2i(0,2,GL_INT);
VBODescript VBODescript::Attribute3i(0,3,GL_INT);
VBODescript VBODescript::Attribute4i(0,4,GL_INT);
VBODescript VBODescript::Attribute1f(0,1,GL_FLOAT);
VBODescript VBODescript::Attribute2f(0,2,GL_FLOAT);
VBODescript VBODescript::Attribute3f(0,3,GL_FLOAT);
VBODescript VBODescript::Attribute4f(0,4,GL_FLOAT);

VBODescript VBODescript::Element3ub(0,3,GL_UNSIGNED_BYTE);
VBODescript VBODescript::Element3us(0,3,GL_UNSIGNED_SHORT);
VBODescript VBODescript::Element3ui(0,3,GL_UNSIGNED_INT);
VBODescript VBODescript::Element4ui(0,4,GL_UNSIGNED_INT);

int VBODescript::GetElementSize() {
  int typesize;
  switch(Type) {
  case GL_FLOAT:
    typesize=sizeof(GLfloat);
    break;
  case GL_UNSIGNED_BYTE:
    typesize=sizeof(GLubyte);
    break;
  case GL_BYTE:
    typesize=sizeof(GLbyte);
    break;
  case GL_UNSIGNED_INT:
    typesize=sizeof(GLuint);
    break;
  case GL_INT:
    typesize=sizeof(GLint);
    break;
  case GL_DOUBLE:
    typesize=sizeof(GLdouble);
    break;
  case GL_SHORT:
    typesize=sizeof(GLshort);
    break;
  case GL_UNSIGNED_SHORT:
    typesize=sizeof(GLushort);
    break;
  case GL_BOOL:
    typesize=sizeof(GLboolean);
    break;
  default:
    printf("UNKNOWN type: %d\n",Type);
    return 0;
  }
  return typesize*Size;
}

}