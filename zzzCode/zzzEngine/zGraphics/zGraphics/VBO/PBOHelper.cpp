#include "PBOHelper.hpp"

namespace zzz{
bool zzz::PBOHelper::FBOToVBO(int x,int y,int width, int height,GLenum from, VBO &to)
{
  glReadBuffer(from);
  glBindBuffer(GL_PIXEL_PACK_BUFFER_EXT, to.VBO_);
  GLenum format=0,type;
  if (to.vbodesc_[0].Size==1) format=GL_RED;
  else if (to.vbodesc_[0].Size==2) return false;
  else if (to.vbodesc_[0].Size==3) format=GL_RGB;
  else if (to.vbodesc_[0].Size==4) format=GL_RGBA;
  type=to.vbodesc_[0].Type;
  glReadPixels(x,y,width,height,format,type,0);
  glReadBuffer(GL_NONE);
  glBindBuffer(GL_PIXEL_PACK_BUFFER_EXT,0);
  return true;
}

bool PBOHelper::FBOToVBO(int x,int y,int width,int height,GLenum format,GLenum from, VBO &to)
{
  glReadBuffer(from);
  glBindBuffer(GL_PIXEL_PACK_BUFFER_EXT, to.VBO_);
  GLenum type;
  type=to.vbodesc_[0].Type;
  glReadPixels(x,y,width,height,format,type,0);
  glReadBuffer(GL_NONE);
  glBindBuffer(GL_PIXEL_PACK_BUFFER_EXT,0);
  return true;
}

bool PBOHelper::FBOToVBO(int x,int y,int width,int height,GLenum format,GLenum type, GLenum from, VBO &to)
{
  glReadBuffer(from);
  glBindBuffer(GL_PIXEL_PACK_BUFFER_EXT, to.VBO_);
  glReadPixels(x,y,width,height,format,type,0);
  glReadBuffer(GL_NONE);
  glBindBuffer(GL_PIXEL_PACK_BUFFER_EXT,0);
  return true;
}
}