#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zCore/common.hpp>
#include "VBODescript.hpp"
#include <zCore/Utility/Uncopyable.hpp>

namespace zzz {

class ZGRAPHICS_CLASS VBO : public Uncopyable {
public:
  VBO();
  ~VBO();
  bool CreateEmpty(int numOfElement,VBODescript &vbodesc,GLenum usage);
  bool CreateEmpty(int numOfElement,vector<VBODescript> &vbodesc,GLenum usage);
  bool Create(const void *data,int numOfElement,VBODescript &vbodesc,GLenum usage=GL_STATIC_DRAW); //single data buffer
  bool Create(const void *data,int numOfElement,vector<VBODescript> &vbodesc,GLenum usage=GL_STATIC_DRAW); //multiple data buffer
  bool SetAttrbuiteLoc(zuint n,GLint loc);
  bool Bind();
  bool Unbind();
  int GetElementNumber();
  GLuint VBO_;
private:
  bool binded;
  bool ProcessVBODescript(vector<VBODescript> &vbodesc);
  int nelements_;
  GLsizeiptr elementsize_;
  vector<VBODescript> vbodesc_;
  vector<GLvoid *> offset_;

  friend class PBOHelper;
};

} // namespace zzz
