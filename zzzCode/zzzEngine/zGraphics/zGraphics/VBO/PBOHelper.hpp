#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "VBO.hpp"

namespace zzz{
class ZGRAPHICS_CLASS PBOHelper {
public:
  static bool FBOToVBO(int x,int y,int width,int height,GLenum from, VBO &to);
  static bool FBOToVBO(int x,int y,int width,int height,GLenum format,GLenum from, VBO &to);
  static bool FBOToVBO(int x,int y,int width,int height,GLenum format,GLenum type, GLenum from, VBO &to);
};
}