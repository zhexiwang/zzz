#pragma once


#include <zCore/EnvDetect.hpp>
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_MINIMAL
#include "zGraphicsConfig.minimal.hpp"
#else

#ifdef ZZZ_COMPILER_MSVC
#ifdef ZZZ_OS_WIN32
#include "zGraphicsConfig.win32.hpp"
#endif

#ifdef ZZZ_OS_WIN64
#include "zGraphicsConfig.win64.hpp"
#endif
#endif

#ifdef ZZZ_COMPILER_MINGW
#include "zGraphicsConfig.MinGW.hpp"
#endif

#ifdef ZZZ_OS_LINUX
#include "zGraphicsConfig.linux.hpp"
#endif

#ifdef ZZZ_OS_MACOS
#include "zGraphicsConfig.macos.hpp"
#endif

#endif // ZZZ_LIB_MINIMAL

#ifdef ZZZ_DYNAMIC
  #ifdef ZGRAPHICS_SOURCE
    #define ZGRAPHICS_FUNC __declspec(dllexport)
    #define ZGRAPHICS_CLASS __declspec(dllexport)
  #else
    #define ZGRAPHICS_FUNC __declspec(dllimport)
    #define ZGRAPHICS_CLASS __declspec(dllimport)
  #endif
#else
  #define ZGRAPHICS_FUNC
  #define ZGRAPHICS_CLASS
#endif

#ifdef ZZZ_COMPILER_MSVC
//WARNINGs
#pragma warning(disable:4100)//unreferenced formal paramter
#pragma warning(disable:4244)//double to float
#pragma warning(disable:4305)//double to float
#pragma warning(disable:4267)//: 'initializing' : conversion from 'size_t' to 'uint', possible loss of data
#pragma warning(disable:4522)//multiple assignment operators specified
#pragma warning(disable:4503)//decorated name length exceeded, name was truncated
#pragma warning(disable:4996)//'std::_Equal1': Function call with parameters that may be unsafe
#endif
