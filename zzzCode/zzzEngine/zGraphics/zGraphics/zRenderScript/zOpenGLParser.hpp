#pragma once
#include <zs.hpp>
#include "../Renderer/ArcBallRenderer.hpp"

namespace zzz{
class zOpenGLParser : public zExtParser
{
public:
  enum {GLENABLE,GLDISABLE,GLCLEAR,GLMATRIXMODE,GLPUSHMATRIX,GLPOPMATRIX,GLLOADIDENTITY,GLVIEWPORT,
    GLTRANSLATE,GLROTATE,GLPOLYGONMODE,GLDEPTHFUNC,GLCHECKERROR,GLCLEARDEPTH,GLCLEARCOLOR,//OPENGLBASE
    GLBEGIN,GLEND,GLVERTEX,GLCOLOR,GLNORMAL,GLTEXCOORD,//OPENGL Draw
    PLACEOBJ,PLACEENV,GLDRAWBUFFER,
    DRAWCOORDINATE,DRAWTRIANGLE,DRAWTEXTURE
  };
  bool Parse(const string &cmd, int &id);
  bool Do(int id, const vector<zParam*> &params, zParam *ret);
  void Init(zScript *zs);
  void SetRenderer(ArcBallRenderer *renderer);
private:
  ArcBallRenderer *m_renderer;
};
}