#pragma once
#include <zs.hpp>
#include "../Resource/Texture/TextureCube.hpp"

namespace zzz{
typedef zObject<TextureCube> zObjectTextureCube;
}