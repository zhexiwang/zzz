#pragma once
#include <zs.hpp>
#include "../Resource/Shader/Shader.hpp"

namespace zzz{

class ShaderLoader
{
public:
  static bool LoadShaderFile(const char* vertexFile, const char* fragmentFile,Shader *shader);
  static bool LoadShaderMemory(const char* vertexMem,const char* fragmentMem,Shader *shader);
};

typedef zObject<Shader> zObjectShader;
}