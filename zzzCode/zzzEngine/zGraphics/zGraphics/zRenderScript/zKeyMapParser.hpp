#pragma once
//#include <zs.hpp>

namespace zzz{
class zRenderScript;
class zKeyMapParser : public zExtParser
{
public:
  bool Parse(const string &cmd, int &id);
  bool Do(int id, const vector<zParam*> &params, zParam *ret);
  void SetZRS(zRenderScript *zrs);
private:
  zRenderScript *m_zrs;
};
}