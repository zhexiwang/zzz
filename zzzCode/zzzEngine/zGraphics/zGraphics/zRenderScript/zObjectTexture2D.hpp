#pragma once
#include <zs.hpp>
#include "../Resource/Texture/Texture2D.hpp"

namespace zzz{
typedef  zObject<Texture2D> zObjectTexture2D;
}