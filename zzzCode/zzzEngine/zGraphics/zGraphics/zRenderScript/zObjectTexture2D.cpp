#include "zObjectTexture2D.hpp"
#include "zObjectTextureHelper.hpp"

namespace zzz{
bool zObjectTexture2D::LoadFromMemory(istringstream &iss, const string &path)
{
  Texture2D *t=(Texture2D*)m_obj;
  string tmp;
  int tmpi;
  while(true)
  {
    iss>>tmp;
    if (iss.fail()) break;
    ToLow(tmp);
    if (tmp=="f" || tmp=="file")
    {
      iss>>tmp;
      tmp=PathFile(path,tmp);
      t->Create(tmp.c_str());
    }
    else if (tmp=="s" || tmp=="size")
    {
      float width,height;
      iss>>width>>height;
      t->ChangeSize(width,height);
    }
    else if (tmp=="i" || tmp=="iformat" || tmp=="internalformat")
    {
      iss>>tmp;
      if (zObjectTextureHelper::TextureConst(tmp,tmpi)) t->ChangeInternalFormat(tmpi);
      else printf("UNKNOWN CONSTANT: %s\n",tmp.c_str());
    }
    else
    {
      printf("UNKNOWN PARAMETER: %s\n",tmp.c_str());
      return false;
    }
  }
  return true;
}
bool zObjectTexture2D::Do(const string &cmd1, const vector<zParam*> &param, zParam *ret)
{
  Texture2D *obj=(Texture2D*)m_obj;
  string cmd=ToLow_copy(cmd1);
  if (cmd=="bind")
  {
    double *d0=zparam_cast<double*>(param[0]);
    int i0=(int)(*d0);
    obj->Bind(GL_TEXTURE0+i0);
  }
  else if (cmd=="disable")
  {
    double *d0=zparam_cast<double*>(param[0]);
    int i0=(int)(*d0);
    obj->Disable(GL_TEXTURE0+i0);
  }
  else if (cmd=="disableall")
    obj->DisableAll();
  else if (cmd=="changesize")
  {
    double *d0=zparam_cast<double*>(param[0]);
    double *d1=zparam_cast<double*>(param[1]);
    obj->ChangeSize(*d0,*d1);
  }
  else return false;
  return true;
}

bool zObjectTexture2D::IsTrue()
{
  return true;
}

string zObjectTexture2D::ToString()
{
  return string("Texture2D");
}


}