#pragma once

#include <zs.hpp>
#include "zOpenGLParser.hpp"
#include "zKeyMapParser.hpp"
#include "../Renderer/ArcBallRenderer.hpp"

namespace zzz{
class zRenderScript : public zScript
{
public:
  zRenderScript();
  void Init(ArcBallRenderer *renderer);
  void OnChar( unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags );
  ArcBallRenderer *m_renderer;
private:
  void setConst();
  void setParser();
  void setObject();
  zOpenGLParser *m_glParser;
  zKeyMapParser *m_kmParser;
  map<char,string> m_keymap;
  friend zKeyMapParser;
};
}