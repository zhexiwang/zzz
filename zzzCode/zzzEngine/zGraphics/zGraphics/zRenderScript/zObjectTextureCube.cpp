#include "zObjectTextureCube.hpp"
#include "zObjectTextureHelper.hpp"
#include "../Resource/Shader/ShaderSpecify.hpp"

namespace zzz{
bool zObjectTextureCube::LoadFromMemory(istringstream &iss, const string &path)
{
  TextureCube *t=(TextureCube*)m_obj;
  string tmp;
  int tmpi;
  while(true)
  {
    iss>>tmp;
    if (iss.fail()) break;
    ToLow(tmp);
    if (tmp=="f" || tmp=="file")
    {
      iss>>tmp;
      tmp=PathFile(path,tmp);
      t->Create(tmp.c_str());
    }
    else if (tmp=="files")
    {
      string posx,posy,posz,negx,negy,negz;
      iss>>posx;
      posx=PathFile(path,posx);
      iss>>negx;
      negx=PathFile(path,negx);
      iss>>posy;
      posy=PathFile(path,posy);
      iss>>negy;
      negy=PathFile(path,negy);
      iss>>posz;
      posz=PathFile(path,posz);
      iss>>negz;
      negz=PathFile(path,negz);
      t->Create(posx.c_str(),negx.c_str(),posy.c_str(),negy.c_str(),posz.c_str(),negz.c_str());
    }
    else if (tmp=="s" || tmp=="size")
    {
      float width,height;
      iss>>width>>height;
      t->ChangeSize(width,height);
    }
    else if (tmp=="c" || tmp=="cubesize")
    {
      float size;
      iss>>size;
      t->ChangeSize(size);
    }
    else if (tmp=="i" || tmp=="internalformat")
    {
      iss>>tmp;
      if (zObjectTextureHelper::TextureConst(tmp,tmpi)) t->ChangeInternalFormat(tmpi);
      else printf("UNKNOWN CONSTANT: %s\n",tmp.c_str());
    }
    else
    {
      printf("UNKNOWN PARAMETER: %s\n",tmp.c_str());
      return false;
    }
  }
  return true;
}
bool zObjectTextureCube::Do(const string &cmd1, const vector<zParam*> &param, zParam *ret)
{
  TextureCube *obj=(TextureCube*)m_obj;
  string cmd=ToLow_copy(cmd1);
  if (cmd=="bind")
  {
    if (param.size()!=1) return false;
    double *d0=zparam_cast<double*>(param[0]);
    int i0=(int)(*d0);
    obj->Bind(GL_TEXTURE0+i0);
  }
  else if (cmd=="disable")
  {
    if (param.size()!=1) return false;
    double *d0=zparam_cast<double*>(param[0]);
    int i0=(int)(*d0);
    obj->Disable(GL_TEXTURE0+i0);
  }
  else if (cmd=="disableall")
    obj->DisableAll();
  else if (cmd=="changesize")
  {
    if (param.size()!=1 && param.size()!=2) return false;
    double *d0=zparam_cast<double*>(param[0]);
    if (param.size()==2)
    {
      double *d1=zparam_cast<double*>(param[1]);
      obj->ChangeSize(*d0,*d1);
    }
    else
      obj->ChangeSize(*d0,*d0);
  }
  else if (cmd=="drawcubemap")
  {
    if (param.size()!=1) return false;
    double *d0=zparam_cast<double*>(param[0]);
    CubemapShader.Begin();
    obj->DrawCubemap(*d0);
    CubemapShader.End();
  }
  else return false;
  return true;
}

bool zObjectTextureCube::IsTrue()
{
  return true;
}

string zObjectTextureCube::ToString()
{
  return string("Texture2D");
}


}