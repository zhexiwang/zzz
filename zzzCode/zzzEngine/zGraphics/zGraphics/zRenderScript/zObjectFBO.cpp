#include "zObjectFBO.hpp"
#include "zObjectTexture2D.hpp"


namespace zzz{
bool zObjectFBO::LoadFromMemory(istringstream &iss, const string &path)
{
  return true;
}
bool zObjectFBO::Do(const string &cmd1, const vector<zParam*> &param, zParam *ret)
{
  FBO *obj=(FBO*)m_obj;
  string cmd=ToLow_copy(cmd1);
  if (cmd=="bind")
  {
    obj->Bind();
  }
  else if (cmd=="attach")
  {
    if (param.size()!=2) return false;
    else
    {
      Texture2D *tex=zparam_obj_cast<Texture2D*>(param[0]);
      double *d0=zparam_cast<double*>(param[1]);
      int i0=(int)(*d0);
      obj->AttachTexture(GL_TEXTURE_2D,tex->GetID(),i0);
    }
  }
  else if (cmd=="disable")
  {
    obj->Disable();
  }
  else if (cmd=="isvalid")
  {
    obj->IsValid();
  }
  else return false;
  return true;
}

bool zObjectFBO::IsTrue()
{
  return true;
}

string zObjectFBO::ToString()
{
  return string("FBO");
}
}