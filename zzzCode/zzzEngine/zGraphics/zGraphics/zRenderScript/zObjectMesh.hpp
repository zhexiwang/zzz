#pragma once
#include <zs.hpp>
#include "../Resource/Mesh/ObjMesh.hpp"

namespace zzz{
typedef zObject<ObjMesh> zObjectMesh;
}