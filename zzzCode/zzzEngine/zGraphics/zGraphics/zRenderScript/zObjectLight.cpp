#include "zObjectLight.hpp"
#include "../Resource/Shader/ShaderSpecify.hpp"

namespace zzz{
bool zObjectLight::LoadFromMemory(istringstream &iss, const string &path)
{
  Light *l=(Light*)m_obj;
  string tmp;
  while(true)
  {
    iss>>tmp;
    if (iss.fail()) break;
    ToLow(tmp);
    if (tmp=="p" || tmp=="pos" || tmp=="position" || tmp=="pst")
    {
      iss>>l->m_pos[0]>>l->m_pos[1]>>l->m_pos[2]>>l->m_pos[3];
      memcpy(l->m_oripos,l->m_pos,sizeof(float)*4);
    }
    else if (tmp=="a" || tmp=="amb" || tmp=="ambient" || tmp=="abt")
    {
      iss>>l->m_amb[0]>>l->m_amb[1]>>l->m_amb[2]>>l->m_amb[3];
    }
    else if (tmp=="d" || tmp=="dif" || tmp=="diffuse" || tmp=="dfs")
    {
      iss>>l->m_dif[0]>>l->m_dif[1]>>l->m_dif[2]>>l->m_dif[3];
    }
    else if (tmp=="s" || tmp=="spe" || tmp=="specular" || tmp=="spc")
    {
      iss>>l->m_spe[0]>>l->m_spe[1]>>l->m_spe[2]>>l->m_spe[3];
    }
    else if (tmp=="l" || tmp=="lig" || tmp=="light" || tmp=="lit")
    {
      int x;
      iss>>x;
      if (x==0) l->m_mylight=GL_LIGHT0;
      else if (x==1) l->m_mylight=GL_LIGHT1;
      else if (x==2) l->m_mylight=GL_LIGHT2;
      else if (x==3) l->m_mylight=GL_LIGHT3;
      else if (x==4) l->m_mylight=GL_LIGHT4;
      else if (x==5) l->m_mylight=GL_LIGHT5;
      else if (x==6) l->m_mylight=GL_LIGHT6;
      else if (x==7) l->m_mylight=GL_LIGHT7;
    }
    else
    {
      printf("UNKNOWN PARAMETER: %s\n",tmp.c_str());
      return false;
    }
  }
  l->m_inited=true;
  return true;
}
bool zObjectLight::Do(const string &cmd1, const vector<zParam*> &param, zParam *ret)
{
  Light *obj=(Light*)m_obj;
  string cmd=ToLow_copy(cmd1);
  if (cmd=="enable")
  {
    double *d0=zparam_cast<double*>(param[0]);
    int i0=(int)(*d0);
    obj->Enable(GL_LIGHT0+i0);
  }
  else if (cmd=="disable")
  {
    if (param.empty())
      obj->Disable();
    else
    {
      double *d0=zparam_cast<double*>(param[0]);
      int i0=(int)(*d0);
      obj->Disable(GL_LIGHT0+i0);
    }
  }
  else if (cmd=="disableall")
  {
    obj->DisableAll();
  }
  else if (cmd=="drawline")
  {
    ColorShader.Begin();
    obj->DrawLine();
    ColorShader.End();
  }
  else return false;
  return true;
}

bool zObjectLight::IsTrue()
{
  return true;
}

string zObjectLight::ToString()
{
  return string("Texture2D");
}


}