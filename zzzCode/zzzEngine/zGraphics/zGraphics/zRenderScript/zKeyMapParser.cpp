#include "zKeyMapParser.hpp"
#include "zRenderScript.hpp"

namespace zzz{

bool zKeyMapParser::Parse( const string &cmd, int &id )
{
  string cmd1=ToLow_copy(cmd);
  if (cmd1=="keymap")
  {
    id=0;
    return true;
  }
  return false;
}

bool zKeyMapParser::Do( int id, const vector<zParam*> &params, zParam *ret )
{
  if (id==0)
  {
    if (params.size()!=2) return false;
    string *str=zparam_cast<string*>(params[0]);
    if (str==0) return false;
    if (params[1]->m_func.empty()) return false;
    m_zrs->m_keymap[str->at(0)]=params[1]->m_func;
    return true;    
  }
  return false;
}

void zKeyMapParser::SetZRS( zRenderScript *zrs )
{
  m_zrs=zrs;
}

}