#include "zRenderScript.hpp"
#include "zObjectTexture2D.hpp"
#include "zObjectTextureCube.hpp"
#include "zObjectShader.hpp"
#include "zObjectLight.hpp"
//#include "zObjectMesh.hpp"
#include "zObjectFBO.hpp"

namespace zzz{
zRenderScript::zRenderScript()
{
}

void zRenderScript::setConst()
{

}

void zRenderScript::setParser()
{
  m_glParser=new zOpenGLParser;
  m_glParser->SetRenderer(m_renderer);
  AddParser(m_glParser);
  m_kmParser=new zKeyMapParser;
  m_kmParser->SetZRS(this);
  AddParser(m_kmParser);
}

void zRenderScript::setObject()
{
  AddObjectTemplate("Texture2D",new zObjectTexture2D);
  AddObjectTemplate("TextureCube",new zObjectTextureCube);
  AddObjectTemplate("Shader",new zObjectShader);
  AddObjectTemplate("Light",new zObjectLight);
//  AddObjectTemplate("Mesh",new zObjectMesh);
  AddObjectTemplate("FBO",new zObjectFBO);
}

void zRenderScript::Init( ArcBallRenderer *renderer )
{
  m_renderer=renderer;
  setConst();
  setParser();
  setObject();
}

void zRenderScript::OnChar( unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags )
{
  map<char,string>::iterator mcsi=m_keymap.find(nChar);
  if (mcsi!=m_keymap.end())
  {
    zParam ret;
    DoFunction(mcsi->second,&ret);
  }
}
}