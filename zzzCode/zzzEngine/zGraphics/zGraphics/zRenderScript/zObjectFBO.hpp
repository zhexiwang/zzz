#pragma once

#include <zs.hpp>
#include "../FBO/FBO.hpp"

namespace zzz{
typedef zObject<FBO> zObjectFBO;
}