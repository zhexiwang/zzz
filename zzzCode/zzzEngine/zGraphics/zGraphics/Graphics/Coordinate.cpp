#include "Coordinate.hpp"

namespace zzz{


CubeCoord::CubeCoord()
{
  cubeface=POSX;
  x=0; y=0;
  size=0;
}

CubeCoord::CubeCoord(const CUBEFACE _face,const zuint _x,const zuint _y, const zuint _size)
{
  cubeface=_face;
  x=_x; y=_y;
  size=_size;
}

CubeCoord::CubeCoord(const CubeCoord& c)
{
  *this=c;
}

const CubeCoord& CubeCoord::operator=(const CubeCoord& c)
{
  x=c.x; y=c.y;size=c.size;cubeface=c.cubeface;
  return *this;
}

void CubeCoord::Standardize()
{
  int ox=x,oy=y;
  int osize=size;
  if (ox>=0 && oy>=0 && ox<osize && oy<osize)
    return ;
  switch(cubeface)
  {
  case POSX:
    if (ox<0)
    {cubeface=POSZ; x+=size;Standardize();return;}
    if (ox>=osize)
    {cubeface=NEGZ; x-=size;Standardize();return;}
    if (oy<0)
    {cubeface=POSY; x=oy+osize; y=osize-1-ox;Standardize();return;}
    if (oy>=osize)
    {cubeface=NEGY; x=osize-1-(oy-osize); y=ox;Standardize();return;}
    break;
  case POSY:
    if (ox<0)
    {cubeface=NEGX; x=oy; y=-1-ox;Standardize();return;}
    if (ox>=osize)
    {cubeface=POSX; x=osize-1-oy; y=ox-osize;Standardize();return;}
    if (oy<0)
    {cubeface=NEGZ; x=osize-1-ox; y=-1-oy;Standardize();return;}
    if (oy>=osize)
    {cubeface=POSZ; x=ox; y=oy-osize;Standardize();return;}
    break;
  case POSZ:
    if (ox<0)
    {cubeface=NEGX; x+=osize;Standardize();return;}
    if (ox>=osize)
    {cubeface=POSX; x-=osize;Standardize();return;}
    if (oy<0)
    {cubeface=POSY; x=ox; y=oy+osize;Standardize();return;}
    if (oy>=osize)
    {cubeface=NEGY; x=ox; y=oy-osize;Standardize();return;}
    break;
  case NEGX:
    if (ox<0)
    {cubeface=NEGZ; x+=osize;Standardize();return;}
    if (ox>=osize)
    {cubeface=POSZ; x-=osize;Standardize();return;}
    if (oy<0)
    {cubeface=POSY; x=-1-oy; y=ox;Standardize();return;}
    if (oy>=osize)
    {cubeface=NEGY; x=oy-osize; y=osize-1-ox;Standardize();return;}
    break;
  case NEGY:
    if (ox<0)
    {cubeface=NEGX; x=osize-1-oy; y=ox+osize;Standardize();return;}
    if (ox>=osize)
    {cubeface=POSX; x=oy; y=osize-1-(ox-osize);Standardize();return;}
    if (oy<0)
    {cubeface=POSZ; x=ox; y=oy+osize;Standardize();return;}
    if (oy>=osize)
    {cubeface=NEGZ; x=osize-1-ox; y=osize-1-(oy-osize);Standardize();return;}
    break;
  case NEGZ:
    if (ox<0)
    {cubeface=POSX; x+=osize;Standardize();return;}
    if (ox>=osize)
    {cubeface=NEGX; x-=osize;Standardize();return;}
    if (oy<0)
    {cubeface=POSY; x=osize-1-ox; y=-1-oy;Standardize();return;}
    if (oy>=osize)
    {cubeface=NEGY; x=osize-1-ox; y=osize-1-(oy-osize);Standardize();return;}
    break;
  default:
    break;
  }
  return;
}

}
