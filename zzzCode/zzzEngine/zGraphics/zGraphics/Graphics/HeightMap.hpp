#pragma once
#include <zGraphics/zGraphicsConfig.hpp>

namespace zzz{
#define MAP_SIZE 128

class ZGRAPHICS_CLASS HeightMap {
public:
  int ToIndex(int x, int y) const;
  void Generate();
  void GetVertexColor(int x, int y, float *col);
  bool SetVertexColor(int x, int y);
  float GetHeight(int x, int y);
  void SetHeight(int x, int y, float h);
  void DrawHeightMap(float drawsize);
  HeightMap();
  virtual ~HeightMap();
  float ScaleValue_;

private:
  float HeightMap_[(MAP_SIZE+1)*(MAP_SIZE+1)];
  float Left_, Right_, Front_, Back_;
};
}