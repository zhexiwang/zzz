#pragma  once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zCore/Math/Vector3.hpp>

namespace zzz{
ZGRAPHICS_FUNC float Ft_in(float costheta1,float eta);
ZGRAPHICS_FUNC float Ft_out(float costheta2,float eta);
ZGRAPHICS_FUNC float Ft_out2(float costheta1,float eta);
ZGRAPHICS_FUNC bool RefractTo(Vector3f inray,Vector3f &outray,float eta,const Vector3f &normal=Vector3f(0,0,1));
ZGRAPHICS_FUNC bool RefractFrom(Vector3f outray,Vector3f &inray,float eta,const Vector3f &normal=Vector3f(0,0,1));
}