#pragma once

#include <zCore/Math/Matrix2x2.hpp>

namespace zzz{
template <typename T>
class Rotation2 : public Matrix<2, 2, T> {
public:
  using Matrix<2, 2, T>::At;
  Rotation2():Matrix<2, 2, T>(1, 0, 0, 1){}
  Rotation2(const Rotation2<T> &other):Matrix<2, 2, T>(other){}
  explicit Rotation2(const MatrixBase<2, 2, T> &other):Matrix<2, 2, T>(other){}
  Rotation2(const T angle) {
    At(0, 0) = At(1, 1) = cos(angle);
    At(0, 1) = -sin(angle);
    At(1, 0) = -At(0, 1);
  }

  T ToAngle() {
    return atan2(At(1, 0), At(0, 0));
  }

  using Matrix<2, 2, T>::operator=;
  using Matrix<2, 2, T>::Data;
  using Matrix<2, 2, T>::Diagonal;
  
  VectorBase<2, T> Apply(const VectorBase<2, T> &a) const {
    return (*this)*a;
  }
};

typedef Rotation2<zfloat32> Rotation2f32;
typedef Rotation2<zfloat64> Rotation2f64;

typedef Rotation2<float> Rotation2f;
typedef Rotation2<double> Rotation2d;

}
