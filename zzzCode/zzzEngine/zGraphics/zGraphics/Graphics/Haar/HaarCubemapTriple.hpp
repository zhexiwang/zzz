// ***************************************************************
//  HaarCubemapTriple   version:  1.0   ��  date: 07/31/2007
//  -------------------------------------------------------------
//  static class for HaarCoeffCubemap4Triple[Full|Sparse] triple product
//  -------------------------------------------------------------
//  Copyright (C) 2007 - All Rights Reserved
// ***************************************************************
// 
// ***************************************************************
#pragma once

#include "HaarCoeffCubemap4Triple.hpp"
namespace zzz{
template <int CUBEFACEN,typename T1=float,typename T2=unsigned short >
class HaarCubemapTriple
{
public:
  static void InitIndexToS()
  {
    static const T2 span[9]={0,1,5,21,85,341,1365,5461,21845}; //up to 256x256x6 cubemap
    for (T2 index=0;index<((CUBEFACEN*CUBEFACEN-1)/3);index++)
    {
      int x=0;
      while(index>=span[x]) x++;
      x--;
      IndexTol[index]=x;
      IndexToj[index]=(index-span[x])%(1<<x);
      IndexToi[index]=(index-span[x]-IndexToj[index])/(1<<x);
      int curlevel=(log2(index * 3 + 1) / 2);
      Cuvw[index] = (float)(1 << curlevel) / CUBEFACEN; 
    }
    memcpy(IndexTol+((CUBEFACEN*CUBEFACEN-1)/3),IndexTol,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexTol+((CUBEFACEN*CUBEFACEN-1)/3)*2,IndexTol,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexTol+((CUBEFACEN*CUBEFACEN-1)/3)*3,IndexTol,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexTol+((CUBEFACEN*CUBEFACEN-1)/3)*4,IndexTol,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexTol+((CUBEFACEN*CUBEFACEN-1)/3)*5,IndexTol,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexToi+((CUBEFACEN*CUBEFACEN-1)/3),IndexToi,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexToi+((CUBEFACEN*CUBEFACEN-1)/3)*2,IndexToi,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexToi+((CUBEFACEN*CUBEFACEN-1)/3)*3,IndexToi,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexToi+((CUBEFACEN*CUBEFACEN-1)/3)*4,IndexToi,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexToi+((CUBEFACEN*CUBEFACEN-1)/3)*5,IndexToi,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexToj+((CUBEFACEN*CUBEFACEN-1)/3),IndexToj,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexToj+((CUBEFACEN*CUBEFACEN-1)/3)*2,IndexToj,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexToj+((CUBEFACEN*CUBEFACEN-1)/3)*3,IndexToj,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexToj+((CUBEFACEN*CUBEFACEN-1)/3)*4,IndexToj,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(IndexToj+((CUBEFACEN*CUBEFACEN-1)/3)*5,IndexToj,sizeof(T2)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(Cuvw+((CUBEFACEN*CUBEFACEN-1)/3),Cuvw,sizeof(float)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(Cuvw+((CUBEFACEN*CUBEFACEN-1)/3)*2,Cuvw,sizeof(float)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(Cuvw+((CUBEFACEN*CUBEFACEN-1)/3)*3,Cuvw,sizeof(float)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(Cuvw+((CUBEFACEN*CUBEFACEN-1)/3)*4,Cuvw,sizeof(float)*((CUBEFACEN*CUBEFACEN-1)/3));
    memcpy(Cuvw+((CUBEFACEN*CUBEFACEN-1)/3)*5,Cuvw,sizeof(float)*((CUBEFACEN*CUBEFACEN-1)/3));
    const T2 squaren=(CUBEFACEN*CUBEFACEN-1)/3;
    for (T2 index=0;index<squaren*6;index++)
    {
      _s s;
      IndexToS(index,s);
      int face=index/squaren;
      IndexToSide[index]=face;
      IndexToFaceIndex[index]=index%squaren;
      if (s.l==0 && s.i==0 && s.j==0)
        continue;
      _s os;
      os.l=s.l-1;
      os.i=s.i/2;
      os.j=s.j/2;
      NextIndex[index]=SToIndex(os,face);
    }
  }
  static void InitCijk()
  {
    for (int index=0;index<(CUBEFACEN*CUBEFACEN-1)/3;index++)
    {
      InitCijkHelper(index,index,index);
      InitCijkHelper2(index,index,index);
      InitCijkHelper3(index,index,index);
    }
  }



private:
  struct jkvalue
  {
    Vector<3,T1> value;
    T2 j,k;
  };
  static vector<jkvalue> Cijk[(CUBEFACEN*CUBEFACEN-1)/3]; //K.Cijk[i].k      += I.i * Cijk[i].value * J.Cijk[i].j;
  static void InitCijkHelper(int index,int i,int j)
  {
    _s s;
    IndexToS(index,s);
    if (s.l==0 && s.i==0 && s.j==0)
      return;
    _s os;
    os.l=s.l-1;
    os.i=s.i/2;
    os.j=s.j/2;
    int qx=s.i-2*os.i,qy=s.j-2*os.j;
    T2 pindex=NextIndex[index];
    T1 data1=(1<<os.l);
    jkvalue newjkvalue;
    newjkvalue.k=pindex;newjkvalue.j=j;
    newjkvalue.value=Vector<3,T1>(SignOfQuadrant(1,qx,qy) * data1,SignOfQuadrant(2,qx,qy) * data1,SignOfQuadrant(3,qx,qy) * data1);
    Cijk[i].push_back(newjkvalue);
    InitCijkHelper(pindex,i,j);
  }
  static void InitCijkHelper2(int index,int i,int k)
  {
    _s s;
    IndexToS(index,s);
    if (s.l==0 && s.i==0 && s.j==0)
      return;
    _s os;
    os.l=s.l-1;
    os.i=s.i/2;
    os.j=s.j/2;
    int qx=s.i-2*os.i,qy=s.j-2*os.j;
    T2 pindex=NextIndex[index];
    T1 data1=(1<<os.l);
    jkvalue newjkvalue;
    newjkvalue.k=k;newjkvalue.j=pindex;
    newjkvalue.value=Vector<3,T1>(data1*SignOfQuadrant(1,qx,qy),data1*SignOfQuadrant(2,qx,qy),data1*SignOfQuadrant(3,qx,qy));
    Cijk[i].push_back(newjkvalue);
    InitCijkHelper2(pindex,i,k);
  }
  static void InitCijkHelper3(int index,int j,int k)
  {
    _s s;
    IndexToS(index,s);
    if (s.l==0 && s.i==0 && s.j==0)
      return;
    _s os;
    os.l=s.l-1;
    os.i=s.i/2;
    os.j=s.j/2;
    int qx=s.i-2*os.i,qy=s.j-2*os.j;
    T2 pindex=NextIndex[index];
    T1 data1=(1<<os.l);
    jkvalue newjkvalue;
    newjkvalue.k=k;newjkvalue.j=j;
    newjkvalue.value=Vector<3,T1>(data1*SignOfQuadrant(1,qx,qy),data1*SignOfQuadrant(2,qx,qy),data1*SignOfQuadrant(3,qx,qy));
    Cijk[pindex].push_back(newjkvalue);
    InitCijkHelper3(pindex,j,k);
  }
  static T2 IndexTol[((CUBEFACEN*CUBEFACEN-1)/3)*6];
  static T2 IndexToi[((CUBEFACEN*CUBEFACEN-1)/3)*6];
  static T2 IndexToj[((CUBEFACEN*CUBEFACEN-1)/3)*6];
  static float Cuvw[((CUBEFACEN*CUBEFACEN-1)/3)*6];
  static T2 NextIndex[((CUBEFACEN*CUBEFACEN-1)/3)*6];
  static T2 IndexToSide[((CUBEFACEN*CUBEFACEN-1)/3)*6];
  static T2 IndexToFaceIndex[((CUBEFACEN*CUBEFACEN-1)/3)*6];
  static int SignOfQuadrant(int MIdx, int qx, int qy)
  {
    switch(MIdx)
    {
    case 1:
      if(qx == 0)  return 1;
      else return -1;
    case 2:
      if(qy == 0) return 1; //might wrong
      else return -1;
    case 3:
      if(qx == 0)
      {
        if(qy == 0) return 1;
        else return -1;
      }
      else
      {
        if(qy == 0) return -1;
        else return 1;
      }
    }
    return 0;
  }


  static inline _s IndexToS(T2 index)//the index in HaarCoeffCubemap4Triple, not in HaarCoeffCubemap. Index in face
  {
    index=index % ((CUBEFACEN*CUBEFACEN-1)/3);
    static const T2 span[9]={0,1,5,21,85,341,1365,5461,21845}; //up to 256x256x6 cubemap
    int x=0;
    while(index>=span[x]) x++;
    x--;
    _s ret;
    ret.l=x;
    ret.j=(index-span[x])%(1<<x);
    ret.i=(index-span[x]-ret.j)/(1<<x);
    return ret;
  }

  static inline void IndexToS(T2 index,_s &ret)//Fast version, need to call InitIndexToS() first!
  {
    ret.l=IndexTol[index];
    ret.i=IndexToi[index];
    ret.j=IndexToj[index];
  }

  static inline T2 SToIndex(_s s, int face)
  {
    static const T2 span[9]={0,1,5,21,85,341,1365,5461,21845}; //up to 256x256x6 cubemap
    T2 ret=span[s.l]+s.i*(1<<s.l)+s.j;
    ret+=face*(CUBEFACEN*CUBEFACEN-1)/3;
    return ret;
  }

  static T1 psum(HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &Coeff, T2 index)
  {
    if (Coeff.psum[index]!=Coeff.EMPTYPSUM)
      return Coeff.psum[index];
    _s s;
     IndexToS(index,s);
     int face=IndexToSide[index];
    if (s.l==0 && s.i==0 && s.j==0)
      return Coeff.Scale[face];
    _s os;
    os.l=s.l-1;
    os.i=s.i/2;
    os.j=s.j/2;
    int qx=s.i-2*os.i,qy=s.j-2*os.j;
    T2 pindex=NextIndex[index];
    T1 ret=psum(Coeff,pindex) + (1<<os.l) *
      (Coeff[pindex].v[0] * SignOfQuadrant(1,qx,qy) 
      + Coeff[pindex].v[1] * SignOfQuadrant(2,qx,qy) 
      + Coeff[pindex].v[2] * SignOfQuadrant(3,qx,qy));
    Coeff.psum[index]=ret;
    return ret;
  }

  static T1 psum(HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &Coeff, T2 index)
  {
    if (Coeff.psum[index]!=Coeff.EMPTYPSUM)
      return Coeff.psum[index];
    _s s;
    IndexToS(index,s);
    int face=IndexToSide[index];
    if (s.l==0 && s.i==0 && s.j==0)
      return Coeff.Scale[face];
    _s os;
    os.l=s.l-1;
    os.i=s.i/2;
    os.j=s.j/2;
    int qx=s.i-2*os.i,qy=s.j-2*os.j;
    T2 pindex=NextIndex[index];
    const Vector<3,T1> &c=Coeff[pindex];
    T1 ret=psum(Coeff,pindex) + (1<<os.l) *
      (c.v[0] * SignOfQuadrant(1,qx,qy) 
      + c.v[1] * SignOfQuadrant(2,qx,qy) 
      + c.v[2] * SignOfQuadrant(3,qx,qy));
    Coeff.psum[index]=ret;
    return ret;
  }

  template <int KEEPNUM>
  static T1 psum(HaarCoeffCubemap4TripleSparseStatic<CUBEFACEN,KEEPNUM,T1,T2> &Coeff, T2 index)
  {
    if (Coeff.psum[index]!=Coeff.EMPTYPSUM)
      return Coeff.psum[index];
    _s s;
    IndexToS(index,s);
    int face=index/Coeff.FACEDATASIZE;
    if (s.l==0 && s.i==0 && s.j==0)
      return Coeff.Scale[face];
    _s os;
    os.l=s.l-1;
    os.i=s.i/2;
    os.j=s.j/2;
    int qx=s.i-2*os.i,qy=s.j-2*os.j;
    T2 pindex=SToIndex(os,face);
    T1 ret=psum(Coeff,pindex) + (1<<os.l) *
      (Coeff[pindex].v[0] * SignOfQuadrant(1,qx,qy) 
      + Coeff[pindex].v[1] * SignOfQuadrant(2,qx,qy) 
      + Coeff[pindex].v[2] * SignOfQuadrant(3,qx,qy));
    Coeff.psum[index]=ret;
    return ret;
  }

  static void psum2(HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &Coeff, T2 index,T1 data)
  {
    _s s;
    IndexToS(index,s);
    int face=IndexToSide[index];
    if (s.l==0 && s.i==0 && s.j==0)
    {
      Coeff.Scale[face]+=data;
      return;
    }
    _s os;
    os.l=s.l-1;
    os.i=s.i/2;
    os.j=s.j/2;
    int qx=s.i-2*os.i,qy=s.j-2*os.j;
     T2 pindex=NextIndex[index];
     Vector<3,T1> &c=Coeff.Square[pindex];
    T1 data1=(1<<os.l)*data;
    c.v[0] += SignOfQuadrant(1,qx,qy) * data1;
    c.v[1] += SignOfQuadrant(2,qx,qy) * data1;
    c.v[2] += SignOfQuadrant(3,qx,qy) * data1;
    psum2(Coeff,pindex,data);
    return;
  }

public:
  
  static T1 TripleProduct(HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &p,
              HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &l,
              HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &v)
  {
    const int leveln=(log2(CUBEFACEN));
    const int squaren=(CUBEFACEN*CUBEFACEN-1)/3;

    T1 integral=0;
    for(int sideIdx = 0; sideIdx < 6; ++sideIdx) //scale
    {
      integral += p.Scale[sideIdx] * l.Scale[sideIdx] * v.Scale[sideIdx];
    }
    for(T2 i = 0; i < squaren*6; ++i)
    {
      const Vector<3,T1> &Pi = p[i];
      const Vector<3,T1> &Li = l[i];
      const Vector<3,T1> &Vi = v[i];
      int curlevel=(log2((i % squaren) * 3 + 1) / 2);
      int C_uvw = (1 << curlevel); //2^lev, lev=log4(i1%t1k+1) = log(i1%t1k+1)/2
      //case2
      integral += C_uvw * 
        ( Vi.v[0] * (Pi.v[1] * Li.v[2] + Pi.v[2] * Li.v[1]) +
          Vi.v[1] * (Pi.v[0] * Li.v[2] + Pi.v[2] * Li.v[0]) +
          Vi.v[2] * (Pi.v[0] * Li.v[1] + Pi.v[1] * Li.v[0]) );
      //case3
      T1 v_psum=psum(v,i),l_psum=psum(l,i),p_psum=psum(p,i);
      integral += (Pi.Dot(Li)*v_psum + Vi.Dot(Pi)*l_psum + Vi.Dot(Li)*p_psum);
    }
    return integral/CUBEFACEN;
  }
  static T1 TripleProduct(HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &p,
              HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &l,
              HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &v)
  {
    bool palloc=false,lalloc=false,valloc=false;
    if (p.psum==NULL)
    {
      p.psum=new T1[p.DATASIZE];
      for (int i=0; i<p.DATASIZE; i++)
        p.psum[i]=p.EMPTYPSUM;
      palloc=true;
    }
    if (l.psum==NULL)
    {
      l.psum=new T1[l.DATASIZE];
      for (int i=0; i<l.DATASIZE; i++)
        l.psum[i]=l.EMPTYPSUM;
      lalloc=true;
    }
    if (v.psum==NULL)
    {
      v.psum=new T1[v.DATASIZE];
      for (int i=0; i<v.DATASIZE; i++)
        v.psum[i]=v.EMPTYPSUM;
      valloc=true;
    }

    const int leveln=(log2(CUBEFACEN));
    const int squaren=(CUBEFACEN*CUBEFACEN-1)/3;

    T1 integral=0;
    for(int sideIdx = 0; sideIdx < 6; ++sideIdx) //scale
    {
      integral += p.Scale[sideIdx] * l.Scale[sideIdx] * v.Scale[sideIdx];
    }
    for(T2 i = 0; i < p.Size; ++i)
    {
      T2 index=p.Coeff[i].index;
      const Vector<3,T1> &Pi = p.Coeff[i].value;
      const Vector<3,T1> &Li = l[index];
      const Vector<3,T1> &Vi = v[index];
      int curlevel=(log2((i % squaren) * 3 + 1) / 2);
      int C_uvw = (1 << curlevel); //2^lev, lev=log4(i1%t1k+1) = log(i1%t1k+1)/2
      //case2
      integral += C_uvw * 
        ( Vi.v[0] * (Pi.v[1] * Li.v[2] + Pi.v[2] * Li.v[1]) +
          Vi.v[1] * (Pi.v[0] * Li.v[2] + Pi.v[2] * Li.v[0]) +
          Vi.v[2] * (Pi.v[0] * Li.v[1] + Pi.v[1] * Li.v[0]) );
      //case3
      T1 v_psum=psum(v,index),l_psum=psum(l,index),p_psum=psum(p,index);
      integral += (Pi.Dot(Li)*v_psum + Vi.Dot(Pi)*l_psum);
    }
    for(T2 i = 0; i < l.Size; ++i)
    {
      T2 index=l.Coeff[i].index;
      const Vector<3,T1> &Li = l.Coeff[i].value;
      const Vector<3,T1> &Pi = p[index];
      const Vector<3,T1> &Vi = v[index];
      //case3
      T1 p_psum=psum(p,index);
      integral += Vi.Dot(Li)*p_psum;
    }
    if (palloc)
    {
      delete[] p.psum;
      p.psum=NULL;
    }
    if (lalloc)
    {
      delete[] l.psum;
      l.psum=NULL;
    }
    if (valloc)
    {
      delete[] v.psum;
      v.psum=NULL;
    }
    return integral/CUBEFACEN;
  }

  //Triple product for 2, result in res
  static void TripleProduct2( HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &p,
                HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &l,
                HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &res)
  {
    const int leveln=(log2(CUBEFACEN));
    const int squaren=(CUBEFACEN*CUBEFACEN-1)/3;
    
    res.Zero();
    Vector<3,T1> zerovalue(0,0,0);
    for(int sideIdx = 0; sideIdx < 6; ++sideIdx) //scale
    {
      res.Scale[sideIdx]+=p.Scale[sideIdx]*l.Scale[sideIdx];
    }
    for(T2 i = 0; i < squaren*6; ++i)
    {
      const Vector<3,T1> &Pi = p[i];
      const Vector<3,T1> &Li = l[i];
      Vector<3,T1> &Vi = res[i];
      int curlevel=(log2((i % squaren) * 3 + 1) / 2);
      int C_uvw = (1 << curlevel); 
      //case2
      Vi.v[0]+=C_uvw*(Pi.v[1]*Li.v[2]+Pi.v[2]*Li.v[1]);
      Vi.v[1]+=C_uvw*(Pi.v[0]*Li.v[2]+Pi.v[2]*Li.v[0]);
      Vi.v[2]+=C_uvw*(Pi.v[0]*Li.v[1]+Pi.v[1]*Li.v[0]);
      //case3
      if (Pi!=zerovalue)
        Vi+=Pi*psum(l,i);
      if (Li!=zerovalue)
        Vi+=Li*psum(p,i);
      T1 dotvalue=Pi.Dot(Li);
      if (dotvalue!=0)
        psum2(res,i,dotvalue);
    }
    for(int i = 0; i < 6; ++i)
    {
      res.Scale[i]/=CUBEFACEN;
    }
    for(T2 i = 0; i < squaren*6; ++i)
    {
      res[i]/=CUBEFACEN;
    }
  }

  //Triple product for 2, result in res
  //ATTENTION: p and res CANNOT be same
  static void TripleProduct2( HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &p,
                HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &l,
                HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &res)
  {
    static T1 *lpsum=new T1[l.DATASIZE];
    bool lalloc=false;
    if (l.psum==NULL)
    {
      l.psum=lpsum;
      l.ResetPsum();
      lalloc=true;
    }
    const int leveln=(log2(CUBEFACEN));
    const int squaren=(CUBEFACEN*CUBEFACEN-1)/3;

    for(int sideIdx = 0; sideIdx < 6; ++sideIdx) //scale
    {
      res.Scale[sideIdx]=p.Scale[sideIdx]*l.Scale[sideIdx]/CUBEFACEN;
    }
    for(int i = 0; i < l.Size; ++i)
    {
      T2 index=l.Coeff[i].index;
      const Vector<3,T1> &Li = l.Coeff[i].value;
      const Vector<3,T1> &Pi = p.Square[index];
      Vector<3,T1> &Vi = res.Square[index];
      Vi+=Li*(psum(p,index)/CUBEFACEN);
      if (Pi==0) continue;
      int C_uvw = Cuvw[index]; 
      //case2
      Vi.v[0]+=C_uvw*(Pi.v[1]*Li.v[2]+Pi.v[2]*Li.v[1]);
      Vi.v[1]+=C_uvw*(Pi.v[0]*Li.v[2]+Pi.v[2]*Li.v[0]);
      Vi.v[2]+=C_uvw*(Pi.v[0]*Li.v[1]+Pi.v[1]*Li.v[0]);
      //case3
      T1 dotvalue=Pi.Dot(Li)/CUBEFACEN;
      psum2(res,index,dotvalue);
    }
    for (T2 i=0; i<squaren*6; i++)
    {
      const Vector<3,T1> &Pi = p.Square[i];
      if (Pi==0) continue;
      res.Square[i]+=Pi*psum(l,i)/CUBEFACEN;
    }
    if (lalloc)
    {
      l.psum=NULL;
    }
  }

  //Triple product for 2, result in res
  static void TripleProduct2( HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &p,
                HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &l,
                HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &res1,
                int BigElementNumber)
  {
    static T1 *lpsum=new T1[l.DATASIZE];
    bool lalloc=false;
    if (l.psum==NULL)
    {
      l.psum=lpsum;
      l.ResetPsum();
      lalloc=true;
    }
    const int leveln=(log2(CUBEFACEN));
    const int squaren=(CUBEFACEN*CUBEFACEN-1)/3;

    HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> res;
    for(int sideIdx = 0; sideIdx < 6; ++sideIdx) //scale
    {
      res.Scale[sideIdx]=p.Scale[sideIdx]*l.Scale[sideIdx]/CUBEFACEN;
    }
    for(int i = 0; i < l.Size; ++i)
    {
      T2 index=l.Coeff[i].index;
      const Vector<3,T1> &Li = l.Coeff[i].value;
      const Vector<3,T1> &Pi = p.Square[index];
      Vector<3,T1> &Vi = res.Square[index];
      Vi+=Li*(psum(p,index)/CUBEFACEN);
      if (Pi.IsZero()) continue;
      int C_uvw = Cuvw[index]; 
      //case2
      Vi.v[0]+=C_uvw*(Pi.v[1]*Li.v[2]+Pi.v[2]*Li.v[1]);
      Vi.v[1]+=C_uvw*(Pi.v[0]*Li.v[2]+Pi.v[2]*Li.v[0]);
      Vi.v[2]+=C_uvw*(Pi.v[0]*Li.v[1]+Pi.v[1]*Li.v[0]);
      //case3
      T1 dotvalue=Pi.Dot(Li)/CUBEFACEN;
      psum2(res,index,dotvalue);
    }
    for (T2 i=0; i<squaren*6; i++)
    {
      const Vector<3,T1> &Pi = p.Square[i];
      if (Pi.IsZero()) continue;
      res.Square[i]+=Pi*psum(l,i)/CUBEFACEN;
    }
    if (lalloc)
    {
      l.psum=NULL;
    }
    res.KeepBigElement(BigElementNumber,res1,false);
  }

  //Triple product for 2, result in res
  static void TripleProduct2( HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &p,
                HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &l,
                HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &res1,
                int BigElementNumber)
  {
    static T1 *ppsum=new T1[p.DATASIZE],*lpsum=new T1[l.DATASIZE];
    bool palloc=false,lalloc=false;
    if (p.psum==NULL)
    {
      p.psum=ppsum;
      p.ResetPsum();
      palloc=true;
    }
    if (l.psum==NULL)
    {
      l.psum=lpsum;
      l.ResetPsum();
      lalloc=true;
    }
    static const int leveln=(log2(CUBEFACEN));
    static const int squaren=(CUBEFACEN*CUBEFACEN-1)/3;

    HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> res;
    for(int sideIdx = 0; sideIdx < 6; ++sideIdx) //scale
    {
      res.Scale[sideIdx]=p.Scale[sideIdx]*l.Scale[sideIdx]/CUBEFACEN;
    }
    for(T2 i = 0; i < p.Size; ++i)
    {
      T2 index=p.Coeff[i].index;
      const Vector<3,T1> &Pi = p.Coeff[i].value;
      const Vector<3,T1> *Li = l.Find(index);
      Vector<3,T1> &Vi = res.Square[index];
      if (Li!=NULL)
      {
        int C_uvw=Cuvw[index];
        //case2
        Vi.v[0]+=C_uvw*(Pi.v[1]*Li->v[2]+Pi.v[2]*Li->v[1]);
        Vi.v[1]+=C_uvw*(Pi.v[0]*Li->v[2]+Pi.v[2]*Li->v[0]);
        Vi.v[2]+=C_uvw*(Pi.v[0]*Li->v[1]+Pi.v[1]*Li->v[0]);
      }
      //case3
      Vi+=Pi*(psum(l,index)/CUBEFACEN);
      if (Li!=NULL)
      {
        T1 dotvalue=Pi.Dot(*Li)/CUBEFACEN;
        psum2(res,index,dotvalue);
      }
    }
    for (T2 i = 0; i < l.Size; ++i)
    {
      T2 index=l.Coeff[i].index;
      const Vector<3,T1> &Li = l.Coeff[i].value;
      Vector<3,T1> &Vi = res.Square[index];
      Vi+=Li*psum(p,index)/CUBEFACEN;
    }
    if (palloc)
    {
      p.psum=NULL;
    }
    if (lalloc)
    {
      l.psum=NULL;
    }
    res.KeepBigElement(BigElementNumber,res1,false);
  }


  //Triple product for 2, result in res
  static void TripleProduct2( HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &p,
                HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &l,
                HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> &res)
  {
    static T1 *ppsum=new T1[p.DATASIZE],*lpsum=new T1[l.DATASIZE];
    bool palloc=false,lalloc=false;
    if (p.psum==NULL)
    {
      p.psum=ppsum;
      p.ResetPsum();
      palloc=true;
    }
    if (l.psum==NULL)
    {
      l.psum=lpsum;
      l.ResetPsum();
      lalloc=true;
    }
    static const int leveln=(log2(CUBEFACEN));
    static const int squaren=(CUBEFACEN*CUBEFACEN-1)/3;

    res.Zero();
    for(int sideIdx = 0; sideIdx < 6; ++sideIdx) //scale
    {
      res.Scale[sideIdx]=p.Scale[sideIdx]*l.Scale[sideIdx]/CUBEFACEN;
    }
    for(T2 i = 0; i < p.Size; ++i)
    {
      T2 index=p.Coeff[i].index;
      const Vector<3,T1> &Pi = p.Coeff[i].value;
      const Vector<3,T1> *Li = l.Find(index);
      Vector<3,T1> &Vi = res.Square[index];
      if (Li!=NULL)
      {
        int C_uvw=Cuvw[index];
        //case2
        Vi.v[0]+=C_uvw*(Pi.v[1]*Li->v[2]+Pi.v[2]*Li->v[1]);
        Vi.v[1]+=C_uvw*(Pi.v[0]*Li->v[2]+Pi.v[2]*Li->v[0]);
        Vi.v[2]+=C_uvw*(Pi.v[0]*Li->v[1]+Pi.v[1]*Li->v[0]);
      }
      //case3
      Vi+=Pi*(psum(l,index)/CUBEFACEN);
      if (Li!=NULL)
      {
        T1 dotvalue=Pi.Dot(*Li)/CUBEFACEN;
        psum2(res,index,dotvalue);
      }
    }
    for (T2 i = 0; i < l.Size; ++i)
    {
      T2 index=l.Coeff[i].index;
      const Vector<3,T1> &Li = l.Coeff[i].value;
      Vector<3,T1> &Vi = res.Square[index];
      Vi+=Li*(psum(p,index)/CUBEFACEN);
    }
    if (palloc)
    {
      p.psum=NULL;
    }
    if (lalloc)
    {
      l.psum=NULL;
    }
  }


  //Triple product for 2, result in res
  static void TripleProduct2Fast( HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &p,
                  HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &l,
                  HaarCoeffCubemap4TripleSparse<CUBEFACEN,T1,T2> &res1,
                  int BigElementNumber)
  {
    static const int leveln=(log2(CUBEFACEN));
    static const int squaren=(CUBEFACEN*CUBEFACEN-1)/3;

    HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> res;
    for(int sideIdx = 0; sideIdx < 6; ++sideIdx) //scale
    {
      res.Scale[sideIdx]=p.Scale[sideIdx]*l.Scale[sideIdx];
    }
    for(T2 i = 0; i < p.Size; ++i)
    {
      T2 index=p.Coeff[i].index;
      const Vector<3,T1> &Pi = p.Coeff[i].value;
      const Vector<3,T1> *Li = l.Find(index);
      Vector<3,T1> &Vi = res.Square[index];
      if (Li!=NULL)
      {
        int &C_uvw=Cuvw[index];
        //case2
        Vi.v[0]+=C_uvw*(Pi.v[1]*Li->z+Pi.v[2]*Li->y);
        Vi.v[1]+=C_uvw*(Pi.v[0]*Li->z+Pi.v[2]*Li->x);
        Vi.v[2]+=C_uvw*(Pi.v[0]*Li->y+Pi.v[1]*Li->x);
      }

      int sideIdx=IndexToSide[index];
      res.Square[index]+=p.Coeff[i].value*l.Scale[sideIdx];

      if (Li!=NULL) 
        res.Scale[sideIdx]+=p.Coeff[i].value.Dot(*Li);

      int faceindex=IndexToFaceIndex[index],indexbegin=sideIdx*squaren,Cijklen=Cijk[faceindex].size();
      for (int j=0; j<Cijklen; j++)
      {
        int indexj=Cijk[faceindex][j].j+indexbegin;
        const Vector<3,T1> *Li=l.Find(indexj);
        if (Li!=NULL)
          res.Square[Cijk[faceindex][j].k+indexbegin] += p.Coeff[i].value*Cijk[faceindex][j].value*(*Li);
      }
    }
    //case3
    for (int j=0; j<l.Size; j++)
    {
      int index=l.Coeff[j].index,sideIdx=IndexToSide[index];
      res.Square[index]+=l.Coeff[j].value*p.Scale[sideIdx];
    }
    res.KeepBigElement(BigElementNumber,res1,false);

    for(int i = 0; i < 6; ++i)
    {
      res1.Scale[i]/=CUBEFACEN;
    }
    for(T2 i = 0; i < res1.Size; ++i)
    {
      res1.Coeff[i].value/=CUBEFACEN;
    }

  }

  template <int KEEPNUM1, int KEEPNUM2, int KEEPNUM3>
  static void TripleProduct2( HaarCoeffCubemap4TripleSparseStatic<CUBEFACEN,KEEPNUM1,T1,T2> &p,
                HaarCoeffCubemap4TripleSparseStatic<CUBEFACEN,KEEPNUM2,T1,T2> &l,
                HaarCoeffCubemap4TripleSparseStatic<CUBEFACEN,KEEPNUM3,T1,T2> &res1)
  {
    static T1 *ppsum=new T1[p.DATASIZE],*lpsum=new T1[l.DATASIZE];
    bool palloc=false,lalloc=false;
    if (p.psum==NULL)
    {
      p.psum=ppsum;
      p.ResetPsum();
      palloc=true;
    }
    if (l.psum==NULL)
    {
      l.psum=lpsum;
      l.ResetPsum();
      lalloc=true;
    }
    const int leveln=(log2(CUBEFACEN));
    const int squaren=(CUBEFACEN*CUBEFACEN-1)/3;

    HaarCoeffCubemap4TripleFull<CUBEFACEN,T1,T2> res;
    res.Zero();
    for(int sideIdx = 0; sideIdx < 6; ++sideIdx) //scale
    {
      res.Scale[sideIdx]=p.Scale[sideIdx]*l.Scale[sideIdx];
    }
    for(T2 i = 0; i < KEEPNUM1; ++i)
    {
      const Vector<3,T1> &Pi = p.Coeff[i].value;
      if (Pi==p.zerovalue) continue;
      T2 index=p.Coeff[i].index;
      const Vector<3,T1> &Li = l[index];
      Vector<3,T1> &Vi = res.Square[index];
      int curlevel=(log2((index % squaren) * 3 + 1) / 2);
      int C_uvw = (1 << curlevel); 
      //case2
      Vi.v[0]+=C_uvw*(Pi.v[1]*Li.v[2]+Pi.v[2]*Li.v[1]);
      Vi.v[1]+=C_uvw*(Pi.v[0]*Li.v[2]+Pi.v[2]*Li.v[0]);
      Vi.v[2]+=C_uvw*(Pi.v[0]*Li.v[1]+Pi.v[1]*Li.v[0]);
      //case3
      Vi+=Pi*psum(l,index);
      T1 dotvalue=Pi.Dot(Li);
      if (dotvalue!=0)
        psum2(res,index,dotvalue);
    }
    for (T2 i = 0; i < KEEPNUM2; ++i)
    {
      const Vector<3,T1> &Li = l.Coeff[i].value;
      if (Li==l.zerovalue) continue;
      T2 index=l.Coeff[i].index;
      Vector<3,T1> &Vi = res.Square[index];
      Vi+=Li*psum(p,index);
    }
    for(int i = 0; i < 6; ++i)
    {
      res.Scale[i]/=CUBEFACEN;
    }
    for(T2 i = 0; i < squaren*6; ++i)
    {
      res[i]/=CUBEFACEN;
    }
    if (palloc)
    {
      p.psum=NULL;
    }
    if (lalloc)
    {
      l.psum=NULL;
    }
    res.KeepBigElement(res1,true);
  }



};
}