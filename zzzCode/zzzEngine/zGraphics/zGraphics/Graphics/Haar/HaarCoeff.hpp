#pragma once

namespace zzz{
template <typename T1, typename T2, size_t N>
struct HaarCoeff
{
  typedef T1 Index_Type;
  typedef T2 Value_Type;
  struct _coeff{
    Index_Type index;
    Value_Type value;
  }Coeff[N];
};
}