#pragma once

//#include "../../common.hpp"
#include "../CubeMap.hpp"
namespace zzz{
//no scale, no index compression
template <class T1, typename T2, int CUBEFACEN>
struct HaarCoeffCubemap
{
  typedef T1 VALUE_TYPE;
  typedef T2 INDEX_TYPE;
  struct _coeff
  {
    T2 index;
    T1 value;
  }*Coeff;
public:
  HaarCoeffCubemap()
  {
    Coeff=NULL;
  }
  ~HaarCoeffCubemap()
  {
    Clear();
  }
  inline void Clear()
  {
    delete[] Coeff;
    Coeff=NULL;
  }
  void LoadFromCubemap(const Cubemap<VALUE_TYPE> &c, int KeepNumber, bool AreaWeighted=true, bool bSortByCoef=false, bool bSortByIdx=false)
  {
    Clear();
    Coeff=new _coeff[KeepNumber];
    _coeff *oricoeff=new _coeff[c.datasize];
    VALUE_TYPE *v=c.v;
    if (AreaWeighted)
    {
      for (int i=0; i<c.datasize; i++)
      {
        oricoeff[i].index=i;
        oricoeff[i].value=v[i];
      }
    }

  }
};
}