#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zCore/Utility/Singleton.hpp>
#include <zCore/Utility/STLString.hpp>
#include <zCore/Utility/STLVector.hpp>
#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/Array2.hpp>
#include "Color.hpp"

namespace zzz {
class ZGRAPHICS_CLASS BMPFont : public Singleton<BMPFont> {
public:
  BMPFont();
  Vector2i Size(const string &msg);
  void Draw(const string &msg);
  void DrawFont();
  zuint FontHeight() const;
  zuint FontWidth() const;
  typedef enum {TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT} AnchorType;
	void DrawAt(const string &msg, const Vector3d &pos, AnchorType anchor = BOTTOM_LEFT);
	void DrawAtScreen(const string &msg, const Vector2d &win, AnchorType anchor = BOTTOM_LEFT);
	Vector2i GetMsgSize(const string &msg);
private:
	STLVector<STLString> msglines_;
	void MassageMessage(const string &msg);

  Array<2,zuchar> font_;
  Array<2,Vector4uc> color_font_;
	void MakeColorFont(const Colorf &foreground, const Colorf &background, float alpha = 1.0f);

  Array<2,zuchar> msgimg_;
  Array<2,Vector4uc> color_msgimg_;
	void MakeMsgImg(const string &msg);
	void MakeColorMsgImg(const string &msg);
	
	

};
}