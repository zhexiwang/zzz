#pragma once
#include <zCore/Math/Vector3.hpp>
#include <zCore/Utility/IOObject.hpp>

namespace zzz{
template <typename T>
class Translation : public Vector<3,T> {
  using Vector<3,T>::v;
public:
  Translation():Vector<3,T>(0){}
  Translation(const Translation<T> &other):Vector<3,T>(other.v){}
  Translation(const T x, const T y, const T z):Vector<3,T>(x,y,z){}
  explicit Translation(const VectorBase<3,T> &other):Vector<3,T>(other){}

  using Vector<3,T>::operator=;

  VectorBase<3,T> Apply(const VectorBase<3,T>& other) const {
    return other+(*this);
  }

// Make it unable to compile
  void ApplyGL() const;
};

typedef Translation<zfloat32> Translationf32;
typedef Translation<zfloat64> Translationf64;


typedef Translation<float> Translationf;
typedef Translation<double> Translationd;

SIMPLE_IOOBJECT(Translationf);
SIMPLE_IOOBJECT(Translationd);
}
