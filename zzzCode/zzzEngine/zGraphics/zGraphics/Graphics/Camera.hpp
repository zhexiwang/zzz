#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "Graphics.hpp"
#include "Quaternion.hpp"
#include "Rotation.hpp"
#include "Transformation.hpp"
#include "Translation.hpp"
#include "Coordinate.hpp"
#include <zCore/Utility/Uncopyable.hpp>

//stupid MFC define...
#undef near
#undef far

namespace zzz {
class ZGRAPHICS_CLASS Camera {
public:
  Vector3d Position_;

  void SetPerspective(const int cw, const int ch, const double near=-1, const double far=-1, const double angle=-1);
  void SetOrthoMode(bool mode);
  void SetPosition(const Vector<3,double> &pos);
  void SetPosition(const double &x, const double &y, const double &z);
  void OffsetPosition(const Vector<3,double> &offset);
  void OffsetPosition(const double &x, const double &y, const double &z);
  void MoveForwards(double dist);
  void MoveLeftwards(double dist);
  void MoveUpwards(double dist);

  void ChangeYaw(double degrees);
  void ChangePitch(double degrees);

  void LookAt(const Vector3d &from, const Vector3d &to, const Vector3d &up);

  void ApplyGL(void) const;
  GLTransformationd GetGLTransformation();
  void Reset();
  Camera();
//private:
  void Update();

  double fovy_;
  double aspect_;
  double z_near_;
  double z_far_;

  double max_pitch_rate_;
  double max_yaw_rate_;
  double yaw_degrees_;
  double pitch_degrees_;

  Quaterniond heading_;
  Quaterniond pitch_;
  GLTransformationd transform_;
  CartesianDirCoord<double> direction_vector_;

  bool ortho_mode_;
  double ortho_scale_;
  int width_, height_;
};
}
