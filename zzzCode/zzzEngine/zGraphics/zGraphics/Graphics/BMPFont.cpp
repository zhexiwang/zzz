#include "BMPFont.hpp"
#include <zCore/common.hpp>
#include <zCore/Math/Vector4.hpp>
#include <zCore/Math/Array.hpp>
#include "Graphics.hpp"
#include "BMPFontData.hpp"
#include "OpenGLTools.hpp"
#include <zGraphics/Resource/Texture/Texture.hpp>

namespace zzz{
void BMPFont::Draw(const string &msg) {
  MakeMsgImg(msg);
  CHECK_GL_ERROR()
  Texture::DisableAll();
  CHECK_GL_ERROR()
  glDrawPixels(msgimg_.Size(1), msgimg_.Size(0), GL_LUMINANCE, GL_UNSIGNED_BYTE, msgimg_.Data());
  CHECK_GL_ERROR()
}

void BMPFont::DrawFont() {
  glDrawPixels(fontwidth*256,fontheight,GL_LUMINANCE,GL_UNSIGNED_BYTE,font_.Data());
}

BMPFont::BMPFont() {
  font_.SetSize(Vector2i(fontheight, 256 * fontwidth));
  memcpy(font_.Data(),fontmap,fontwidth*fontheight*256);
}

Vector2i BMPFont::Size(const string &msg) {
  return Vector2i(fontwidth*msg.size(),fontheight);
}

zuint BMPFont::FontHeight() const {
  return fontheight;
}

zuint BMPFont::FontWidth() const {
  return fontwidth;
}

void BMPFont::MakeColorFont(const Colorf &foreground, const Colorf &background, float alpha /*= 1.0f*/) {
  Vector4uc fore(foreground.ToRGB<zuchar>(), Clamp(0.0f, alpha, 1.0f) / 1.0f * 255);
  Vector4uc back(background.ToRGB<zuchar>(), Clamp(0.0f, alpha, 1.0f) / 1.0f * 255);
  color_font_.SetSize(font_.Size());
  for (zuint i = 0; i < font_.size(); ++i)
    color_font_[i] = font_[i] ? fore : back;
}

void BMPFont::MakeMsgImg(const string &msg) {
  Vector2i drawsize = GetMsgSize(msg);
  msgimg_.SetSize(drawsize);
  msgimg_.Zero();
  for (zuint n = 0; n < msglines_.size(); ++n) {
    int msglen = msglines_[n].size();
    // Make it unsigned, so multiply make sense.
    STLVector<zuchar> charidx(msglen, 0);
    memcpy(charidx.Data(), msglines_[n].c_str(), msglen);
    for (int i = 0; i < fontheight; ++i) {
      zuchar *cur = &(msgimg_(n * fontheight + i, 0));
      for (int j = 0; j < msglen; ++j) {
        memcpy(cur, font_.Data() + font_.ToIndex(Vector2i(i, charidx[j] * fontwidth)), fontwidth);
        cur+=fontwidth;
      }
    }
  }
}

void BMPFont::DrawAt(const string &msg, const Vector3d &pos, AnchorType anchor /*= BOTTOM_LEFT*/) {
  OpenGLProjector proj;
  Vector3d win = proj.Project(pos[0], pos[1], pos[2]);
  DrawAtScreen(msg, Vector2d(win), anchor);
}

void BMPFont::DrawAtScreen(const string &msg, const Vector2d &win, AnchorType anchor /*= BOTTOM_LEFT*/) {
  Vector2i drawsize = GetMsgSize(msg);
  glWindowPos2d(0, 0);
  switch(anchor) {
  case TOP_LEFT:
    glBitmap(0, 0, 0, 0, win[0], win[1] - drawsize[0], NULL);
    break;
  case TOP_RIGHT:
    glBitmap(0, 0, 0, 0, win[0] - drawsize[1], win[1] - drawsize[0], NULL);
    break;
  case BOTTOM_LEFT:
    glBitmap(0, 0, 0, 0, win[0], win[1], NULL);
    break;
  case BOTTOM_RIGHT:
    glBitmap(0, 0, 0, 0, win[0] - drawsize[1], win[1], NULL);
    break;
  }
  Draw(msg);
}

zzz::Vector2i BMPFont::GetMsgSize(const string &msg) {
  MassageMessage(msg);
  int maxlen = 0;
  for (zuint i = 0; i < msglines_.size(); ++i)
    maxlen = Max<int>(maxlen, msglines_[i].size());
  return Vector2i(fontheight * msglines_.size(), fontwidth * maxlen);
}

void BMPFont::MassageMessage(const string &msg) {
  msglines_.clear();
  if (msg.empty()) return;
  msglines_.push_back(STLString());
  for (zuint i = 0; i < msg.size(); ++i) {
    if (msg[i] == '\n')
      msglines_.push_back(STLString());
    else if (msg[i] == '\t')
      msglines_.back() += "  ";
    else
      msglines_.back() += msg[i];
  }
  if (msglines_.back().empty())
    msglines_.pop_back();
  reverse(msglines_.begin(), msglines_.end());
}

}