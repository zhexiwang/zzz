#pragma once
#include "Translation.hpp"
#include <zGraphics/Graphics/OpenGLTools.hpp>

namespace zzz{
template<>
inline void Translation<float>::ApplyGL() const {
  glTranslatef(v[0],v[1],v[2]);
}
template<>
inline void Translation<double>::ApplyGL() const {
  glTranslated(v[0],v[1],v[2]);
}
}
