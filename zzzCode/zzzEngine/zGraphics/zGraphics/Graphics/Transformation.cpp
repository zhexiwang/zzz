#pragma once
#include "Transformation.hpp"
#include <zGraphics/Graphics/OpenGLTools.hpp>

namespace zzz{
template<>
void Transformation<float>::ApplyGL() const {
  glMultMatrixf(Transposed().Data());
}
template<>
void Transformation<double>::ApplyGL() const {
  glMultMatrixd(Transposed().Data());
}
template<>
void GLTransformation<float>::ApplyGL() const {
  glMultMatrixf(Data());
}
template<>
void GLTransformation<double>::ApplyGL() const {
  glMultMatrixd(Data());
}
}
