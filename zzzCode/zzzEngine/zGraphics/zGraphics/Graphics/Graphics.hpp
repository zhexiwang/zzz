#pragma once
#include "../zGraphicsConfig.hpp"

#ifdef ZZZ_LIB_OPENGL

// Glew need to be included before gl.
#define GLEW_STATIC
#ifdef ZZZ_COMPILER_MSVC
#include <glew.h>
#else
#include <GL/glew.h>
#endif

// OpenGL
#ifdef ZZZ_OS_WIN
#include <Windows.h>
#endif 

#ifdef ZZZ_OS_MACOS
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
#endif

#endif // ZZZ_LIB_OPENGL

