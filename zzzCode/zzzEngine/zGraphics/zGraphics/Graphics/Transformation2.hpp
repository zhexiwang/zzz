#pragma once

#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/Matrix3x3.hpp>
#include <zCore/Math/Matrix2x2.hpp>
#include <zGraphics/Graphics/Translation2.hpp>
#include <zGraphics/Graphics/Rotation2.hpp>

namespace zzz{
template <typename T>
class Transformation2 : public Matrix<3,3,T> {
public:
  Transformation2():Matrix<3,3,T>(1, 0, 0, 0, 1, 0, 0, 0, 1) {}
  Transformation2(const Transformation2<T> &other):Matrix<3,3,T>(other) {}
  explicit Transformation2(const MatrixBase<3,3,T> &other):Matrix<3,3,T>(other){}
  explicit Transformation2(const Translation2<T> &t) {
    Matrix<3,3,T>::Identical();
    T* v=Data();
    v[2]=t[0];
    v[5]=t[1];
  }
  explicit Transformation2(const Rotation<T> &r) {
    Matrix<3,3,T>::Identical();
    T* v=Data();
    v[0]=r[0];  v[1]=r[1];
    v[3]=r[2];  v[4]=r[3];
  }
  Transformation2(const MatrixBase<2,2,T> &r, const VectorBase<2,T> &t) {
    Matrix<3,3,T>::Identical();
    Set(r,t);
  }

  using Matrix<3,3,T>::operator =;
  using Matrix<3,3,T>::operator *;
  using Matrix<3,3,T>::Data;

  void Set(const MatrixBase<2,2,T> &r, const VectorBase<2,T> &t) {
    T* v=Data();
    v[0]=r[0];  v[1]=r[1];  v[2]=t[0];
    v[3]=r[2];  v[4]=r[3];  v[5]=t[1];
    v[6]=0;     v[7]=0;     v[8]=1;
  }

  VectorBase<2,T> Apply(const VectorBase<2,T> &other) const {
    VectorBase<3,T> v(other,(T)1);
    VectorBase<3,T> res=(*this)*v;
    res /= res[2];
    return VectorBase<2,T>(res);
  }

  Transformation2<T> RelativeTo(const Transformation2<T> &other) {
    return (*this)*other.Inverted();
  }

  //Fast inverse of a rigid transform matrix
  //M=[r t]
  //  [0 1]
  //inv(M)=[r' -r'*t]
  //       [0      1]
  void Invert() {
    *this=Inverted();
  }

  Transformation2<T> Inverted() const {
    const T* v=Data();
    Matrix<2,2,T> r(v[0],v[3],v[1],v[4]);
    Vector<2,T> t(-v[2],-v[5]);
    return Transformation2<T>(Rotation2<T>(r),Translation2<T>(r*t));
  }

  static Transformation2<T> GetIdentical() {
    static Transformation2<T> mat;
    return mat;
  }
};

typedef Transformation2<zfloat32> Transformation2f32;
typedef Transformation2<zfloat64> Transformation2f64;

typedef Transformation2<float> Transformation2f;
typedef Transformation2<double> Transformation2d;

}
