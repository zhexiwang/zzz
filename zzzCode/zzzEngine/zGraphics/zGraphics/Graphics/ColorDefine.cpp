#include "ColorDefine.hpp"

namespace zzz{
const Colorf ColorDefine::aliceBlue           (0.94118,0.97255,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#F0F8FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::antiqueWhite        (0.98039,0.92157,0.84314);  /// \htmlonly <table><tr><td width="300" bgcolor="#FAEBD7">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::antiqueWhite1       (1.00000,0.93725,0.85882);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFEFDB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::antiqueWhite2       (0.93333,0.87451,0.80000);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEDFCC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::antiqueWhite3       (0.80392,0.75294,0.69020);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDC0B0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::antiqueWhite4       (0.54510,0.51373,0.47059);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B8378">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::aquamarine          (0.49804,1.00000,0.83137);  /// \htmlonly <table><tr><td width="300" bgcolor="#7FFFD4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::aquamarine1         (0.49804,1.00000,0.83137);  /// \htmlonly <table><tr><td width="300" bgcolor="#7FFFD4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::aquamarine2         (0.46275,0.93333,0.77647);  /// \htmlonly <table><tr><td width="300" bgcolor="#76EEC6">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::aquamarine3         (0.40000,0.80392,0.66667);  /// \htmlonly <table><tr><td width="300" bgcolor="#66CDAA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::aquamarine4         (0.27059,0.54510,0.45490);  /// \htmlonly <table><tr><td width="300" bgcolor="#458B74">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::azure               (0.94118,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#F0FFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::azure1              (0.94118,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#F0FFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::azure2              (0.87843,0.93333,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#E0EEEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::azure3              (0.75686,0.80392,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#C1CDCD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::azure4              (0.51373,0.54510,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#838B8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::beige               (0.96078,0.96078,0.86275);  /// \htmlonly <table><tr><td width="300" bgcolor="#F5F5DC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::bisque              (1.00000,0.89412,0.76863);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFE4C4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::bisque1             (1.00000,0.89412,0.76863);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFE4C4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::bisque2             (0.93333,0.83529,0.71765);  /// \htmlonly <table><tr><td width="300" bgcolor="#EED5B7">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::bisque3             (0.80392,0.71765,0.61961);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDB79E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::bisque4             (0.54510,0.49020,0.41961);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B7D6B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::black               (0.00000,0.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#000000">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::blanchedAlmond      (1.00000,0.92157,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFEBCD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::blue                (0.00000,0.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#0000FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::blue1               (0.00000,0.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#0000FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::blue2               (0.00000,0.00000,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#0000EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::blue3               (0.00000,0.00000,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#0000CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::blue4               (0.00000,0.00000,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#00008B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::blueViolet          (0.54118,0.16863,0.88627);  /// \htmlonly <table><tr><td width="300" bgcolor="#8A2BE2">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::brown               (0.64706,0.16471,0.16471);  /// \htmlonly <table><tr><td width="300" bgcolor="#A52A2A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::brown1              (1.00000,0.25098,0.25098);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF4040">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::brown2              (0.93333,0.23137,0.23137);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE3B3B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::brown3              (0.80392,0.20000,0.20000);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD3333">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::brown4              (0.54510,0.13725,0.13725);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B2323">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::burlywood           (0.87059,0.72157,0.52941);  /// \htmlonly <table><tr><td width="300" bgcolor="#DEB887">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::burlywood1          (1.00000,0.82745,0.60784);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFD39B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::burlywood2          (0.93333,0.77255,0.56863);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEC591">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::burlywood3          (0.80392,0.66667,0.49020);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDAA7D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::burlywood4          (0.54510,0.45098,0.33333);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B7355">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cadetBlue           (0.37255,0.61961,0.62745);  /// \htmlonly <table><tr><td width="300" bgcolor="#5F9EA0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cadetBlue1          (0.59608,0.96078,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#98F5FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cadetBlue2          (0.55686,0.89804,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#8EE5EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cadetBlue3          (0.47843,0.77255,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#7AC5CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cadetBlue4          (0.32549,0.52549,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#53868B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::chartreuse          (0.49804,1.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#7FFF00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::chartreuse1         (0.49804,1.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#7FFF00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::chartreuse2         (0.46275,0.93333,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#76EE00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::chartreuse3         (0.40000,0.80392,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#66CD00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::chartreuse4         (0.27059,0.54510,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#458B00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::chocolate           (0.82353,0.41176,0.11765);  /// \htmlonly <table><tr><td width="300" bgcolor="#D2691E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::chocolate1          (1.00000,0.49804,0.14118);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF7F24">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::chocolate2          (0.93333,0.46275,0.12941);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE7621">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::chocolate3          (0.80392,0.40000,0.11373);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD661D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::chocolate4          (0.54510,0.27059,0.07451);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B4513">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::coral               (1.00000,0.49804,0.31373);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF7F50">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::coral1              (1.00000,0.44706,0.33725);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF7256">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::coral2              (0.93333,0.41569,0.31373);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE6A50">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::coral3              (0.80392,0.35686,0.27059);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD5B45">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::coral4              (0.54510,0.24314,0.18431);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B3E2F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cornflowerBlue      (0.39216,0.58431,0.92941);  /// \htmlonly <table><tr><td width="300" bgcolor="#6495ED">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cornsilk            (1.00000,0.97255,0.86275);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFF8DC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cornsilk1           (1.00000,0.97255,0.86275);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFF8DC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cornsilk2           (0.93333,0.90980,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEE8CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cornsilk3           (0.80392,0.78431,0.69412);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDC8B1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cornsilk4           (0.54510,0.53333,0.47059);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B8878">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cyan                (0.00000,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#00FFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cyan1               (0.00000,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#00FFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cyan2               (0.00000,0.93333,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#00EEEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cyan3               (0.00000,0.80392,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#00CDCD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::cyan4               (0.00000,0.54510,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#008B8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkBlue            (0.00000,0.00000,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#00008B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkCyan            (0.00000,0.54510,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#008B8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkGoldenrod       (0.72157,0.52549,0.04314);  /// \htmlonly <table><tr><td width="300" bgcolor="#B8860B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkGoldenrod1      (1.00000,0.72549,0.05882);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFB90F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkGoldenrod2      (0.93333,0.67843,0.05490);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEAD0E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkGoldenrod3      (0.80392,0.58431,0.04706);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD950C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkGoldenrod4      (0.54510,0.39608,0.03137);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B6508">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkGray            (0.66275,0.66275,0.66275);  /// \htmlonly <table><tr><td width="300" bgcolor="#A9A9A9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkGreen           (0.00000,0.39216,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#006400">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkGrey            (0.66275,0.66275,0.66275);  /// \htmlonly <table><tr><td width="300" bgcolor="#A9A9A9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkKhaki           (0.74118,0.71765,0.41961);  /// \htmlonly <table><tr><td width="300" bgcolor="#BDB76B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkMagenta         (0.54510,0.00000,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B008B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOliveGreen      (0.33333,0.41961,0.18431);  /// \htmlonly <table><tr><td width="300" bgcolor="#556B2F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOliveGreen1     (0.79216,1.00000,0.43922);  /// \htmlonly <table><tr><td width="300" bgcolor="#CAFF70">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOliveGreen2     (0.73725,0.93333,0.40784);  /// \htmlonly <table><tr><td width="300" bgcolor="#BCEE68">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOliveGreen3     (0.63529,0.80392,0.35294);  /// \htmlonly <table><tr><td width="300" bgcolor="#A2CD5A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOliveGreen4     (0.43137,0.54510,0.23922);  /// \htmlonly <table><tr><td width="300" bgcolor="#6E8B3D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOrange          (1.00000,0.54902,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF8C00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOrange1         (1.00000,0.49804,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF7F00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOrange2         (0.93333,0.46275,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE7600">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOrange3         (0.80392,0.40000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD6600">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOrange4         (0.54510,0.27059,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B4500">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOrchid          (0.60000,0.19608,0.80000);  /// \htmlonly <table><tr><td width="300" bgcolor="#9932CC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOrchid1         (0.74902,0.24314,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#BF3EFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOrchid2         (0.69804,0.22745,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#B23AEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOrchid3         (0.60392,0.19608,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#9A32CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkOrchid4         (0.40784,0.13333,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#68228B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkRed             (0.54510,0.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B0000">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSalmon          (0.91373,0.58824,0.47843);  /// \htmlonly <table><tr><td width="300" bgcolor="#E9967A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSeaGreen        (0.56078,0.73725,0.56078);  /// \htmlonly <table><tr><td width="300" bgcolor="#8FBC8F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSeaGreen1       (0.75686,1.00000,0.75686);  /// \htmlonly <table><tr><td width="300" bgcolor="#C1FFC1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSeaGreen2       (0.70588,0.93333,0.70588);  /// \htmlonly <table><tr><td width="300" bgcolor="#B4EEB4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSeaGreen3       (0.60784,0.80392,0.60784);  /// \htmlonly <table><tr><td width="300" bgcolor="#9BCD9B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSeaGreen4       (0.41176,0.54510,0.41176);  /// \htmlonly <table><tr><td width="300" bgcolor="#698B69">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSlateBlue       (0.28235,0.23922,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#483D8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSlateGray       (0.18431,0.30980,0.30980);  /// \htmlonly <table><tr><td width="300" bgcolor="#2F4F4F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSlateGray1      (0.59216,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#97FFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSlateGray2      (0.55294,0.93333,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#8DEEEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSlateGray3      (0.47451,0.80392,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#79CDCD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSlateGray4      (0.32157,0.54510,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#528B8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkSlateGrey       (0.18431,0.30980,0.30980);  /// \htmlonly <table><tr><td width="300" bgcolor="#2F4F4F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkTurquoise       (0.00000,0.80784,0.81961);  /// \htmlonly <table><tr><td width="300" bgcolor="#00CED1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::darkViolet          (0.58039,0.00000,0.82745);  /// \htmlonly <table><tr><td width="300" bgcolor="#9400D3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::deepPink            (1.00000,0.07843,0.57647);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF1493">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::deepPink1           (1.00000,0.07843,0.57647);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF1493">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::deepPink2           (0.93333,0.07059,0.53725);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE1289">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::deepPink3           (0.80392,0.06275,0.46275);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD1076">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::deepPink4           (0.54510,0.03922,0.31373);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B0A50">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::deepSkyBlue         (0.00000,0.74902,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#00BFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::deepSkyBlue1        (0.00000,0.74902,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#00BFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::deepSkyBlue2        (0.00000,0.69804,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#00B2EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::deepSkyBlue3        (0.00000,0.60392,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#009ACD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::deepSkyBlue4        (0.00000,0.40784,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#00688B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::dimGray             (0.41176,0.41176,0.41176);  /// \htmlonly <table><tr><td width="300" bgcolor="#696969">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::dimGrey             (0.41176,0.41176,0.41176);  /// \htmlonly <table><tr><td width="300" bgcolor="#696969">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::dodgerBlue          (0.11765,0.56471,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#1E90FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::dodgerBlue1         (0.11765,0.56471,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#1E90FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::dodgerBlue2         (0.10980,0.52549,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#1C86EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::dodgerBlue3         (0.09412,0.45490,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#1874CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::dodgerBlue4         (0.06275,0.30588,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#104E8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::firebrick           (0.69804,0.13333,0.13333);  /// \htmlonly <table><tr><td width="300" bgcolor="#B22222">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::firebrick1          (1.00000,0.18824,0.18824);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF3030">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::firebrick2          (0.93333,0.17255,0.17255);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE2C2C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::firebrick3          (0.80392,0.14902,0.14902);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD2626">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::firebrick4          (0.54510,0.10196,0.10196);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B1A1A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::floralWhite         (1.00000,0.98039,0.94118);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFAF0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::forestGreen         (0.13333,0.54510,0.13333);  /// \htmlonly <table><tr><td width="300" bgcolor="#228B22">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gainsboro           (0.86275,0.86275,0.86275);  /// \htmlonly <table><tr><td width="300" bgcolor="#DCDCDC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::ghostWhite          (0.97255,0.97255,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#F8F8FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gold                (1.00000,0.84314,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFD700">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gold1               (1.00000,0.84314,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFD700">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gold2               (0.93333,0.78824,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEC900">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gold3               (0.80392,0.67843,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDAD00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gold4               (0.54510,0.45882,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B7500">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::goldenrod           (0.85490,0.64706,0.12549);  /// \htmlonly <table><tr><td width="300" bgcolor="#DAA520">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::goldenrod1          (1.00000,0.75686,0.14510);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFC125">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::goldenrod2          (0.93333,0.70588,0.13333);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEB422">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::goldenrod3          (0.80392,0.60784,0.11373);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD9B1D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::goldenrod4          (0.54510,0.41176,0.07843);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B6914">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray                (0.74510,0.74510,0.74510);  /// \htmlonly <table><tr><td width="300" bgcolor="#BEBEBE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray0               (0.00000,0.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#000000">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray1               (0.01176,0.01176,0.01176);  /// \htmlonly <table><tr><td width="300" bgcolor="#030303">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray10              (0.10196,0.10196,0.10196);  /// \htmlonly <table><tr><td width="300" bgcolor="#1A1A1A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray100             (1.00000,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray11              (0.10980,0.10980,0.10980);  /// \htmlonly <table><tr><td width="300" bgcolor="#1C1C1C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray12              (0.12157,0.12157,0.12157);  /// \htmlonly <table><tr><td width="300" bgcolor="#1F1F1F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray13              (0.12941,0.12941,0.12941);  /// \htmlonly <table><tr><td width="300" bgcolor="#212121">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray14              (0.14118,0.14118,0.14118);  /// \htmlonly <table><tr><td width="300" bgcolor="#242424">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray15              (0.14902,0.14902,0.14902);  /// \htmlonly <table><tr><td width="300" bgcolor="#262626">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray16              (0.16078,0.16078,0.16078);  /// \htmlonly <table><tr><td width="300" bgcolor="#292929">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray17              (0.16863,0.16863,0.16863);  /// \htmlonly <table><tr><td width="300" bgcolor="#2B2B2B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray18              (0.18039,0.18039,0.18039);  /// \htmlonly <table><tr><td width="300" bgcolor="#2E2E2E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray19              (0.18824,0.18824,0.18824);  /// \htmlonly <table><tr><td width="300" bgcolor="#303030">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray2               (0.01961,0.01961,0.01961);  /// \htmlonly <table><tr><td width="300" bgcolor="#050505">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray20              (0.20000,0.20000,0.20000);  /// \htmlonly <table><tr><td width="300" bgcolor="#333333">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray21              (0.21176,0.21176,0.21176);  /// \htmlonly <table><tr><td width="300" bgcolor="#363636">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray22              (0.21961,0.21961,0.21961);  /// \htmlonly <table><tr><td width="300" bgcolor="#383838">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray23              (0.23137,0.23137,0.23137);  /// \htmlonly <table><tr><td width="300" bgcolor="#3B3B3B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray24              (0.23922,0.23922,0.23922);  /// \htmlonly <table><tr><td width="300" bgcolor="#3D3D3D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray25              (0.25098,0.25098,0.25098);  /// \htmlonly <table><tr><td width="300" bgcolor="#404040">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray26              (0.25882,0.25882,0.25882);  /// \htmlonly <table><tr><td width="300" bgcolor="#424242">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray27              (0.27059,0.27059,0.27059);  /// \htmlonly <table><tr><td width="300" bgcolor="#454545">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray28              (0.27843,0.27843,0.27843);  /// \htmlonly <table><tr><td width="300" bgcolor="#474747">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray29              (0.29020,0.29020,0.29020);  /// \htmlonly <table><tr><td width="300" bgcolor="#4A4A4A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray3               (0.03137,0.03137,0.03137);  /// \htmlonly <table><tr><td width="300" bgcolor="#080808">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray30              (0.30196,0.30196,0.30196);  /// \htmlonly <table><tr><td width="300" bgcolor="#4D4D4D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray31              (0.30980,0.30980,0.30980);  /// \htmlonly <table><tr><td width="300" bgcolor="#4F4F4F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray32              (0.32157,0.32157,0.32157);  /// \htmlonly <table><tr><td width="300" bgcolor="#525252">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray33              (0.32941,0.32941,0.32941);  /// \htmlonly <table><tr><td width="300" bgcolor="#545454">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray34              (0.34118,0.34118,0.34118);  /// \htmlonly <table><tr><td width="300" bgcolor="#575757">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray35              (0.34902,0.34902,0.34902);  /// \htmlonly <table><tr><td width="300" bgcolor="#595959">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray36              (0.36078,0.36078,0.36078);  /// \htmlonly <table><tr><td width="300" bgcolor="#5C5C5C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray37              (0.36863,0.36863,0.36863);  /// \htmlonly <table><tr><td width="300" bgcolor="#5E5E5E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray38              (0.38039,0.38039,0.38039);  /// \htmlonly <table><tr><td width="300" bgcolor="#616161">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray39              (0.38824,0.38824,0.38824);  /// \htmlonly <table><tr><td width="300" bgcolor="#636363">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray4               (0.03922,0.03922,0.03922);  /// \htmlonly <table><tr><td width="300" bgcolor="#0A0A0A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray40              (0.40000,0.40000,0.40000);  /// \htmlonly <table><tr><td width="300" bgcolor="#666666">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray41              (0.41176,0.41176,0.41176);  /// \htmlonly <table><tr><td width="300" bgcolor="#696969">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray42              (0.41961,0.41961,0.41961);  /// \htmlonly <table><tr><td width="300" bgcolor="#6B6B6B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray43              (0.43137,0.43137,0.43137);  /// \htmlonly <table><tr><td width="300" bgcolor="#6E6E6E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray44              (0.43922,0.43922,0.43922);  /// \htmlonly <table><tr><td width="300" bgcolor="#707070">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray45              (0.45098,0.45098,0.45098);  /// \htmlonly <table><tr><td width="300" bgcolor="#737373">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray46              (0.45882,0.45882,0.45882);  /// \htmlonly <table><tr><td width="300" bgcolor="#757575">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray47              (0.47059,0.47059,0.47059);  /// \htmlonly <table><tr><td width="300" bgcolor="#787878">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray48              (0.47843,0.47843,0.47843);  /// \htmlonly <table><tr><td width="300" bgcolor="#7A7A7A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray49              (0.49020,0.49020,0.49020);  /// \htmlonly <table><tr><td width="300" bgcolor="#7D7D7D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray5               (0.05098,0.05098,0.05098);  /// \htmlonly <table><tr><td width="300" bgcolor="#0D0D0D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray50              (0.49804,0.49804,0.49804);  /// \htmlonly <table><tr><td width="300" bgcolor="#7F7F7F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray51              (0.50980,0.50980,0.50980);  /// \htmlonly <table><tr><td width="300" bgcolor="#828282">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray52              (0.52157,0.52157,0.52157);  /// \htmlonly <table><tr><td width="300" bgcolor="#858585">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray53              (0.52941,0.52941,0.52941);  /// \htmlonly <table><tr><td width="300" bgcolor="#878787">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray54              (0.54118,0.54118,0.54118);  /// \htmlonly <table><tr><td width="300" bgcolor="#8A8A8A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray55              (0.54902,0.54902,0.54902);  /// \htmlonly <table><tr><td width="300" bgcolor="#8C8C8C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray56              (0.56078,0.56078,0.56078);  /// \htmlonly <table><tr><td width="300" bgcolor="#8F8F8F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray57              (0.56863,0.56863,0.56863);  /// \htmlonly <table><tr><td width="300" bgcolor="#919191">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray58              (0.58039,0.58039,0.58039);  /// \htmlonly <table><tr><td width="300" bgcolor="#949494">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray59              (0.58824,0.58824,0.58824);  /// \htmlonly <table><tr><td width="300" bgcolor="#969696">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray6               (0.05882,0.05882,0.05882);  /// \htmlonly <table><tr><td width="300" bgcolor="#0F0F0F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray60              (0.60000,0.60000,0.60000);  /// \htmlonly <table><tr><td width="300" bgcolor="#999999">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray61              (0.61176,0.61176,0.61176);  /// \htmlonly <table><tr><td width="300" bgcolor="#9C9C9C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray62              (0.61961,0.61961,0.61961);  /// \htmlonly <table><tr><td width="300" bgcolor="#9E9E9E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray63              (0.63137,0.63137,0.63137);  /// \htmlonly <table><tr><td width="300" bgcolor="#A1A1A1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray64              (0.63922,0.63922,0.63922);  /// \htmlonly <table><tr><td width="300" bgcolor="#A3A3A3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray65              (0.65098,0.65098,0.65098);  /// \htmlonly <table><tr><td width="300" bgcolor="#A6A6A6">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray66              (0.65882,0.65882,0.65882);  /// \htmlonly <table><tr><td width="300" bgcolor="#A8A8A8">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray67              (0.67059,0.67059,0.67059);  /// \htmlonly <table><tr><td width="300" bgcolor="#ABABAB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray68              (0.67843,0.67843,0.67843);  /// \htmlonly <table><tr><td width="300" bgcolor="#ADADAD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray69              (0.69020,0.69020,0.69020);  /// \htmlonly <table><tr><td width="300" bgcolor="#B0B0B0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray7               (0.07059,0.07059,0.07059);  /// \htmlonly <table><tr><td width="300" bgcolor="#121212">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray70              (0.70196,0.70196,0.70196);  /// \htmlonly <table><tr><td width="300" bgcolor="#B3B3B3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray71              (0.70980,0.70980,0.70980);  /// \htmlonly <table><tr><td width="300" bgcolor="#B5B5B5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray72              (0.72157,0.72157,0.72157);  /// \htmlonly <table><tr><td width="300" bgcolor="#B8B8B8">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray73              (0.72941,0.72941,0.72941);  /// \htmlonly <table><tr><td width="300" bgcolor="#BABABA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray74              (0.74118,0.74118,0.74118);  /// \htmlonly <table><tr><td width="300" bgcolor="#BDBDBD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray75              (0.74902,0.74902,0.74902);  /// \htmlonly <table><tr><td width="300" bgcolor="#BFBFBF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray76              (0.76078,0.76078,0.76078);  /// \htmlonly <table><tr><td width="300" bgcolor="#C2C2C2">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray77              (0.76863,0.76863,0.76863);  /// \htmlonly <table><tr><td width="300" bgcolor="#C4C4C4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray78              (0.78039,0.78039,0.78039);  /// \htmlonly <table><tr><td width="300" bgcolor="#C7C7C7">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray79              (0.78824,0.78824,0.78824);  /// \htmlonly <table><tr><td width="300" bgcolor="#C9C9C9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray8               (0.07843,0.07843,0.07843);  /// \htmlonly <table><tr><td width="300" bgcolor="#141414">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray80              (0.80000,0.80000,0.80000);  /// \htmlonly <table><tr><td width="300" bgcolor="#CCCCCC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray81              (0.81176,0.81176,0.81176);  /// \htmlonly <table><tr><td width="300" bgcolor="#CFCFCF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray82              (0.81961,0.81961,0.81961);  /// \htmlonly <table><tr><td width="300" bgcolor="#D1D1D1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray83              (0.83137,0.83137,0.83137);  /// \htmlonly <table><tr><td width="300" bgcolor="#D4D4D4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray84              (0.83922,0.83922,0.83922);  /// \htmlonly <table><tr><td width="300" bgcolor="#D6D6D6">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray85              (0.85098,0.85098,0.85098);  /// \htmlonly <table><tr><td width="300" bgcolor="#D9D9D9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray86              (0.85882,0.85882,0.85882);  /// \htmlonly <table><tr><td width="300" bgcolor="#DBDBDB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray87              (0.87059,0.87059,0.87059);  /// \htmlonly <table><tr><td width="300" bgcolor="#DEDEDE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray88              (0.87843,0.87843,0.87843);  /// \htmlonly <table><tr><td width="300" bgcolor="#E0E0E0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray89              (0.89020,0.89020,0.89020);  /// \htmlonly <table><tr><td width="300" bgcolor="#E3E3E3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray9               (0.09020,0.09020,0.09020);  /// \htmlonly <table><tr><td width="300" bgcolor="#171717">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray90              (0.89804,0.89804,0.89804);  /// \htmlonly <table><tr><td width="300" bgcolor="#E5E5E5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray91              (0.90980,0.90980,0.90980);  /// \htmlonly <table><tr><td width="300" bgcolor="#E8E8E8">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray92              (0.92157,0.92157,0.92157);  /// \htmlonly <table><tr><td width="300" bgcolor="#EBEBEB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray93              (0.92941,0.92941,0.92941);  /// \htmlonly <table><tr><td width="300" bgcolor="#EDEDED">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray94              (0.94118,0.94118,0.94118);  /// \htmlonly <table><tr><td width="300" bgcolor="#F0F0F0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray95              (0.94902,0.94902,0.94902);  /// \htmlonly <table><tr><td width="300" bgcolor="#F2F2F2">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray96              (0.96078,0.96078,0.96078);  /// \htmlonly <table><tr><td width="300" bgcolor="#F5F5F5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray97              (0.96863,0.96863,0.96863);  /// \htmlonly <table><tr><td width="300" bgcolor="#F7F7F7">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray98              (0.98039,0.98039,0.98039);  /// \htmlonly <table><tr><td width="300" bgcolor="#FAFAFA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::gray99              (0.98824,0.98824,0.98824);  /// \htmlonly <table><tr><td width="300" bgcolor="#FCFCFC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::green               (0.00000,1.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#00FF00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::green1              (0.00000,1.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#00FF00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::green2              (0.00000,0.93333,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#00EE00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::green3              (0.00000,0.80392,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#00CD00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::green4              (0.00000,0.54510,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#008B00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::greenYellow         (0.67843,1.00000,0.18431);  /// \htmlonly <table><tr><td width="300" bgcolor="#ADFF2F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey                (0.74510,0.74510,0.74510);  /// \htmlonly <table><tr><td width="300" bgcolor="#BEBEBE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey0               (0.00000,0.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#000000">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey1               (0.01176,0.01176,0.01176);  /// \htmlonly <table><tr><td width="300" bgcolor="#030303">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey10              (0.10196,0.10196,0.10196);  /// \htmlonly <table><tr><td width="300" bgcolor="#1A1A1A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey100             (1.00000,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey11              (0.10980,0.10980,0.10980);  /// \htmlonly <table><tr><td width="300" bgcolor="#1C1C1C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey12              (0.12157,0.12157,0.12157);  /// \htmlonly <table><tr><td width="300" bgcolor="#1F1F1F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey13              (0.12941,0.12941,0.12941);  /// \htmlonly <table><tr><td width="300" bgcolor="#212121">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey14              (0.14118,0.14118,0.14118);  /// \htmlonly <table><tr><td width="300" bgcolor="#242424">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey15              (0.14902,0.14902,0.14902);  /// \htmlonly <table><tr><td width="300" bgcolor="#262626">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey16              (0.16078,0.16078,0.16078);  /// \htmlonly <table><tr><td width="300" bgcolor="#292929">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey17              (0.16863,0.16863,0.16863);  /// \htmlonly <table><tr><td width="300" bgcolor="#2B2B2B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey18              (0.18039,0.18039,0.18039);  /// \htmlonly <table><tr><td width="300" bgcolor="#2E2E2E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey19              (0.18824,0.18824,0.18824);  /// \htmlonly <table><tr><td width="300" bgcolor="#303030">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey2               (0.01961,0.01961,0.01961);  /// \htmlonly <table><tr><td width="300" bgcolor="#050505">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey20              (0.20000,0.20000,0.20000);  /// \htmlonly <table><tr><td width="300" bgcolor="#333333">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey21              (0.21176,0.21176,0.21176);  /// \htmlonly <table><tr><td width="300" bgcolor="#363636">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey22              (0.21961,0.21961,0.21961);  /// \htmlonly <table><tr><td width="300" bgcolor="#383838">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey23              (0.23137,0.23137,0.23137);  /// \htmlonly <table><tr><td width="300" bgcolor="#3B3B3B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey24              (0.23922,0.23922,0.23922);  /// \htmlonly <table><tr><td width="300" bgcolor="#3D3D3D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey25              (0.25098,0.25098,0.25098);  /// \htmlonly <table><tr><td width="300" bgcolor="#404040">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey26              (0.25882,0.25882,0.25882);  /// \htmlonly <table><tr><td width="300" bgcolor="#424242">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey27              (0.27059,0.27059,0.27059);  /// \htmlonly <table><tr><td width="300" bgcolor="#454545">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey28              (0.27843,0.27843,0.27843);  /// \htmlonly <table><tr><td width="300" bgcolor="#474747">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey29              (0.29020,0.29020,0.29020);  /// \htmlonly <table><tr><td width="300" bgcolor="#4A4A4A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey3               (0.03137,0.03137,0.03137);  /// \htmlonly <table><tr><td width="300" bgcolor="#080808">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey30              (0.30196,0.30196,0.30196);  /// \htmlonly <table><tr><td width="300" bgcolor="#4D4D4D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey31              (0.30980,0.30980,0.30980);  /// \htmlonly <table><tr><td width="300" bgcolor="#4F4F4F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey32              (0.32157,0.32157,0.32157);  /// \htmlonly <table><tr><td width="300" bgcolor="#525252">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey33              (0.32941,0.32941,0.32941);  /// \htmlonly <table><tr><td width="300" bgcolor="#545454">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey34              (0.34118,0.34118,0.34118);  /// \htmlonly <table><tr><td width="300" bgcolor="#575757">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey35              (0.34902,0.34902,0.34902);  /// \htmlonly <table><tr><td width="300" bgcolor="#595959">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey36              (0.36078,0.36078,0.36078);  /// \htmlonly <table><tr><td width="300" bgcolor="#5C5C5C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey37              (0.36863,0.36863,0.36863);  /// \htmlonly <table><tr><td width="300" bgcolor="#5E5E5E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey38              (0.38039,0.38039,0.38039);  /// \htmlonly <table><tr><td width="300" bgcolor="#616161">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey39              (0.38824,0.38824,0.38824);  /// \htmlonly <table><tr><td width="300" bgcolor="#636363">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey4               (0.03922,0.03922,0.03922);  /// \htmlonly <table><tr><td width="300" bgcolor="#0A0A0A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey40              (0.40000,0.40000,0.40000);  /// \htmlonly <table><tr><td width="300" bgcolor="#666666">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey41              (0.41176,0.41176,0.41176);  /// \htmlonly <table><tr><td width="300" bgcolor="#696969">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey42              (0.41961,0.41961,0.41961);  /// \htmlonly <table><tr><td width="300" bgcolor="#6B6B6B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey43              (0.43137,0.43137,0.43137);  /// \htmlonly <table><tr><td width="300" bgcolor="#6E6E6E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey44              (0.43922,0.43922,0.43922);  /// \htmlonly <table><tr><td width="300" bgcolor="#707070">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey45              (0.45098,0.45098,0.45098);  /// \htmlonly <table><tr><td width="300" bgcolor="#737373">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey46              (0.45882,0.45882,0.45882);  /// \htmlonly <table><tr><td width="300" bgcolor="#757575">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey47              (0.47059,0.47059,0.47059);  /// \htmlonly <table><tr><td width="300" bgcolor="#787878">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey48              (0.47843,0.47843,0.47843);  /// \htmlonly <table><tr><td width="300" bgcolor="#7A7A7A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey49              (0.49020,0.49020,0.49020);  /// \htmlonly <table><tr><td width="300" bgcolor="#7D7D7D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey5               (0.05098,0.05098,0.05098);  /// \htmlonly <table><tr><td width="300" bgcolor="#0D0D0D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey50              (0.49804,0.49804,0.49804);  /// \htmlonly <table><tr><td width="300" bgcolor="#7F7F7F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey51              (0.50980,0.50980,0.50980);  /// \htmlonly <table><tr><td width="300" bgcolor="#828282">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey52              (0.52157,0.52157,0.52157);  /// \htmlonly <table><tr><td width="300" bgcolor="#858585">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey53              (0.52941,0.52941,0.52941);  /// \htmlonly <table><tr><td width="300" bgcolor="#878787">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey54              (0.54118,0.54118,0.54118);  /// \htmlonly <table><tr><td width="300" bgcolor="#8A8A8A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey55              (0.54902,0.54902,0.54902);  /// \htmlonly <table><tr><td width="300" bgcolor="#8C8C8C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey56              (0.56078,0.56078,0.56078);  /// \htmlonly <table><tr><td width="300" bgcolor="#8F8F8F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey57              (0.56863,0.56863,0.56863);  /// \htmlonly <table><tr><td width="300" bgcolor="#919191">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey58              (0.58039,0.58039,0.58039);  /// \htmlonly <table><tr><td width="300" bgcolor="#949494">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey59              (0.58824,0.58824,0.58824);  /// \htmlonly <table><tr><td width="300" bgcolor="#969696">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey6               (0.05882,0.05882,0.05882);  /// \htmlonly <table><tr><td width="300" bgcolor="#0F0F0F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey60              (0.60000,0.60000,0.60000);  /// \htmlonly <table><tr><td width="300" bgcolor="#999999">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey61              (0.61176,0.61176,0.61176);  /// \htmlonly <table><tr><td width="300" bgcolor="#9C9C9C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey62              (0.61961,0.61961,0.61961);  /// \htmlonly <table><tr><td width="300" bgcolor="#9E9E9E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey63              (0.63137,0.63137,0.63137);  /// \htmlonly <table><tr><td width="300" bgcolor="#A1A1A1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey64              (0.63922,0.63922,0.63922);  /// \htmlonly <table><tr><td width="300" bgcolor="#A3A3A3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey65              (0.65098,0.65098,0.65098);  /// \htmlonly <table><tr><td width="300" bgcolor="#A6A6A6">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey66              (0.65882,0.65882,0.65882);  /// \htmlonly <table><tr><td width="300" bgcolor="#A8A8A8">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey67              (0.67059,0.67059,0.67059);  /// \htmlonly <table><tr><td width="300" bgcolor="#ABABAB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey68              (0.67843,0.67843,0.67843);  /// \htmlonly <table><tr><td width="300" bgcolor="#ADADAD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey69              (0.69020,0.69020,0.69020);  /// \htmlonly <table><tr><td width="300" bgcolor="#B0B0B0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey7               (0.07059,0.07059,0.07059);  /// \htmlonly <table><tr><td width="300" bgcolor="#121212">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey70              (0.70196,0.70196,0.70196);  /// \htmlonly <table><tr><td width="300" bgcolor="#B3B3B3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey71              (0.70980,0.70980,0.70980);  /// \htmlonly <table><tr><td width="300" bgcolor="#B5B5B5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey72              (0.72157,0.72157,0.72157);  /// \htmlonly <table><tr><td width="300" bgcolor="#B8B8B8">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey73              (0.72941,0.72941,0.72941);  /// \htmlonly <table><tr><td width="300" bgcolor="#BABABA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey74              (0.74118,0.74118,0.74118);  /// \htmlonly <table><tr><td width="300" bgcolor="#BDBDBD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey75              (0.74902,0.74902,0.74902);  /// \htmlonly <table><tr><td width="300" bgcolor="#BFBFBF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey76              (0.76078,0.76078,0.76078);  /// \htmlonly <table><tr><td width="300" bgcolor="#C2C2C2">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey77              (0.76863,0.76863,0.76863);  /// \htmlonly <table><tr><td width="300" bgcolor="#C4C4C4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey78              (0.78039,0.78039,0.78039);  /// \htmlonly <table><tr><td width="300" bgcolor="#C7C7C7">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey79              (0.78824,0.78824,0.78824);  /// \htmlonly <table><tr><td width="300" bgcolor="#C9C9C9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey8               (0.07843,0.07843,0.07843);  /// \htmlonly <table><tr><td width="300" bgcolor="#141414">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey80              (0.80000,0.80000,0.80000);  /// \htmlonly <table><tr><td width="300" bgcolor="#CCCCCC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey81              (0.81176,0.81176,0.81176);  /// \htmlonly <table><tr><td width="300" bgcolor="#CFCFCF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey82              (0.81961,0.81961,0.81961);  /// \htmlonly <table><tr><td width="300" bgcolor="#D1D1D1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey83              (0.83137,0.83137,0.83137);  /// \htmlonly <table><tr><td width="300" bgcolor="#D4D4D4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey84              (0.83922,0.83922,0.83922);  /// \htmlonly <table><tr><td width="300" bgcolor="#D6D6D6">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey85              (0.85098,0.85098,0.85098);  /// \htmlonly <table><tr><td width="300" bgcolor="#D9D9D9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey86              (0.85882,0.85882,0.85882);  /// \htmlonly <table><tr><td width="300" bgcolor="#DBDBDB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey87              (0.87059,0.87059,0.87059);  /// \htmlonly <table><tr><td width="300" bgcolor="#DEDEDE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey88              (0.87843,0.87843,0.87843);  /// \htmlonly <table><tr><td width="300" bgcolor="#E0E0E0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey89              (0.89020,0.89020,0.89020);  /// \htmlonly <table><tr><td width="300" bgcolor="#E3E3E3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey9               (0.09020,0.09020,0.09020);  /// \htmlonly <table><tr><td width="300" bgcolor="#171717">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey90              (0.89804,0.89804,0.89804);  /// \htmlonly <table><tr><td width="300" bgcolor="#E5E5E5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey91              (0.90980,0.90980,0.90980);  /// \htmlonly <table><tr><td width="300" bgcolor="#E8E8E8">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey92              (0.92157,0.92157,0.92157);  /// \htmlonly <table><tr><td width="300" bgcolor="#EBEBEB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey93              (0.92941,0.92941,0.92941);  /// \htmlonly <table><tr><td width="300" bgcolor="#EDEDED">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey94              (0.94118,0.94118,0.94118);  /// \htmlonly <table><tr><td width="300" bgcolor="#F0F0F0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey95              (0.94902,0.94902,0.94902);  /// \htmlonly <table><tr><td width="300" bgcolor="#F2F2F2">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey96              (0.96078,0.96078,0.96078);  /// \htmlonly <table><tr><td width="300" bgcolor="#F5F5F5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey97              (0.96863,0.96863,0.96863);  /// \htmlonly <table><tr><td width="300" bgcolor="#F7F7F7">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey98              (0.98039,0.98039,0.98039);  /// \htmlonly <table><tr><td width="300" bgcolor="#FAFAFA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::grey99              (0.98824,0.98824,0.98824);  /// \htmlonly <table><tr><td width="300" bgcolor="#FCFCFC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::honeydew            (0.94118,1.00000,0.94118);  /// \htmlonly <table><tr><td width="300" bgcolor="#F0FFF0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::honeydew1           (0.94118,1.00000,0.94118);  /// \htmlonly <table><tr><td width="300" bgcolor="#F0FFF0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::honeydew2           (0.87843,0.93333,0.87843);  /// \htmlonly <table><tr><td width="300" bgcolor="#E0EEE0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::honeydew3           (0.75686,0.80392,0.75686);  /// \htmlonly <table><tr><td width="300" bgcolor="#C1CDC1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::honeydew4           (0.51373,0.54510,0.51373);  /// \htmlonly <table><tr><td width="300" bgcolor="#838B83">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::hotPink             (1.00000,0.41176,0.70588);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF69B4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::hotPink1            (1.00000,0.43137,0.70588);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF6EB4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::hotPink2            (0.93333,0.41569,0.65490);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE6AA7">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::hotPink3            (0.80392,0.37647,0.56471);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD6090">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::hotPink4            (0.54510,0.22745,0.38431);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B3A62">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::indianRed           (0.80392,0.36078,0.36078);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD5C5C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::indianRed1          (1.00000,0.41569,0.41569);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF6A6A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::indianRed2          (0.93333,0.38824,0.38824);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE6363">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::indianRed3          (0.80392,0.33333,0.33333);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD5555">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::indianRed4          (0.54510,0.22745,0.22745);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B3A3A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::ivory               (1.00000,1.00000,0.94118);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFFF0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::ivory1              (1.00000,1.00000,0.94118);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFFF0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::ivory2              (0.93333,0.93333,0.87843);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEEEE0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::ivory3              (0.80392,0.80392,0.75686);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDCDC1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::ivory4              (0.54510,0.54510,0.51373);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B8B83">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::khaki               (0.94118,0.90196,0.54902);  /// \htmlonly <table><tr><td width="300" bgcolor="#F0E68C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::khaki1              (1.00000,0.96471,0.56078);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFF68F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::khaki2              (0.93333,0.90196,0.52157);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEE685">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::khaki3              (0.80392,0.77647,0.45098);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDC673">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::khaki4              (0.54510,0.52549,0.30588);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B864E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lavender            (0.90196,0.90196,0.98039);  /// \htmlonly <table><tr><td width="300" bgcolor="#E6E6FA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lavenderBlush       (1.00000,0.94118,0.96078);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFF0F5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lavenderBlush1      (1.00000,0.94118,0.96078);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFF0F5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lavenderBlush2      (0.93333,0.87843,0.89804);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEE0E5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lavenderBlush3      (0.80392,0.75686,0.77255);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDC1C5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lavenderBlush4      (0.54510,0.51373,0.52549);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B8386">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lawnGreen           (0.48627,0.98824,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#7CFC00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lemonChiffon        (1.00000,0.98039,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFACD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lemonChiffon1       (1.00000,0.98039,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFACD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lemonChiffon2       (0.93333,0.91373,0.74902);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEE9BF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lemonChiffon3       (0.80392,0.78824,0.64706);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDC9A5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lemonChiffon4       (0.54510,0.53725,0.43922);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B8970">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightBlue           (0.67843,0.84706,0.90196);  /// \htmlonly <table><tr><td width="300" bgcolor="#ADD8E6">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightBlue1          (0.74902,0.93725,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#BFEFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightBlue2          (0.69804,0.87451,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#B2DFEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightBlue3          (0.60392,0.75294,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#9AC0CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightBlue4          (0.40784,0.51373,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#68838B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightCoral          (0.94118,0.50196,0.50196);  /// \htmlonly <table><tr><td width="300" bgcolor="#F08080">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightCyan           (0.87843,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#E0FFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightCyan1          (0.87843,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#E0FFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightCyan2          (0.81961,0.93333,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#D1EEEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightCyan3          (0.70588,0.80392,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#B4CDCD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightCyan4          (0.47843,0.54510,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#7A8B8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightGoldenrod      (0.93333,0.86667,0.50980);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEDD82">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightGoldenrod1     (1.00000,0.92549,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFEC8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightGoldenrod2     (0.93333,0.86275,0.50980);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEDC82">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightGoldenrod3     (0.80392,0.74510,0.43922);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDBE70">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightGoldenrod4     (0.54510,0.50588,0.29804);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B814C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightGoldenrodYellow(0.98039,0.98039,0.82353);  /// \htmlonly <table><tr><td width="300" bgcolor="#FAFAD2">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightGray           (0.82745,0.82745,0.82745);  /// \htmlonly <table><tr><td width="300" bgcolor="#D3D3D3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightGreen          (0.56471,0.93333,0.56471);  /// \htmlonly <table><tr><td width="300" bgcolor="#90EE90">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightGrey           (0.82745,0.82745,0.82745);  /// \htmlonly <table><tr><td width="300" bgcolor="#D3D3D3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightPink           (1.00000,0.71373,0.75686);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFB6C1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightPink1          (1.00000,0.68235,0.72549);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFAEB9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightPink2          (0.93333,0.63529,0.67843);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEA2AD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightPink3          (0.80392,0.54902,0.58431);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD8C95">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightPink4          (0.54510,0.37255,0.39608);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B5F65">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSalmon         (1.00000,0.62745,0.47843);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFA07A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSalmon1        (1.00000,0.62745,0.47843);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFA07A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSalmon2        (0.93333,0.58431,0.44706);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE9572">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSalmon3        (0.80392,0.50588,0.38431);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD8162">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSalmon4        (0.54510,0.34118,0.25882);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B5742">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSeaGreen       (0.12549,0.69804,0.66667);  /// \htmlonly <table><tr><td width="300" bgcolor="#20B2AA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSkyBlue        (0.52941,0.80784,0.98039);  /// \htmlonly <table><tr><td width="300" bgcolor="#87CEFA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSkyBlue1       (0.69020,0.88627,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#B0E2FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSkyBlue2       (0.64314,0.82745,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#A4D3EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSkyBlue3       (0.55294,0.71373,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#8DB6CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSkyBlue4       (0.37647,0.48235,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#607B8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSlateBlue      (0.51765,0.43922,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#8470FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSlateGray      (0.46667,0.53333,0.60000);  /// \htmlonly <table><tr><td width="300" bgcolor="#778899">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSlateGrey      (0.46667,0.53333,0.60000);  /// \htmlonly <table><tr><td width="300" bgcolor="#778899">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSteelBlue      (0.69020,0.76863,0.87059);  /// \htmlonly <table><tr><td width="300" bgcolor="#B0C4DE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSteelBlue1     (0.79216,0.88235,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#CAE1FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSteelBlue2     (0.73725,0.82353,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#BCD2EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSteelBlue3     (0.63529,0.70980,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#A2B5CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightSteelBlue4     (0.43137,0.48235,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#6E7B8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightYellow         (1.00000,1.00000,0.87843);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFFE0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightYellow1        (1.00000,1.00000,0.87843);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFFE0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightYellow2        (0.93333,0.93333,0.81961);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEEED1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightYellow3        (0.80392,0.80392,0.70588);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDCDB4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::lightYellow4        (0.54510,0.54510,0.47843);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B8B7A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::limeGreen           (0.19608,0.80392,0.19608);  /// \htmlonly <table><tr><td width="300" bgcolor="#32CD32">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::linen               (0.98039,0.94118,0.90196);  /// \htmlonly <table><tr><td width="300" bgcolor="#FAF0E6">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::magenta             (1.00000,0.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF00FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::magenta1            (1.00000,0.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF00FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::magenta2            (0.93333,0.00000,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE00EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::magenta3            (0.80392,0.00000,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD00CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::magenta4            (0.54510,0.00000,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B008B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::maroon              (0.69020,0.18824,0.37647);  /// \htmlonly <table><tr><td width="300" bgcolor="#B03060">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::maroon1             (1.00000,0.20392,0.70196);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF34B3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::maroon2             (0.93333,0.18824,0.65490);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE30A7">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::maroon3             (0.80392,0.16078,0.56471);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD2990">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::maroon4             (0.54510,0.10980,0.38431);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B1C62">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumAquamarine    (0.40000,0.80392,0.66667);  /// \htmlonly <table><tr><td width="300" bgcolor="#66CDAA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumBlue          (0.00000,0.00000,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#0000CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumOrchid        (0.72941,0.33333,0.82745);  /// \htmlonly <table><tr><td width="300" bgcolor="#BA55D3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumOrchid1       (0.87843,0.40000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#E066FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumOrchid2       (0.81961,0.37255,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#D15FEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumOrchid3       (0.70588,0.32157,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#B452CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumOrchid4       (0.47843,0.21569,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#7A378B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumPurple        (0.57647,0.43922,0.85882);  /// \htmlonly <table><tr><td width="300" bgcolor="#9370DB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumPurple1       (0.67059,0.50980,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#AB82FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumPurple2       (0.62353,0.47451,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#9F79EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumPurple3       (0.53725,0.40784,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#8968CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumPurple4       (0.36471,0.27843,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#5D478B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumSeaGreen      (0.23529,0.70196,0.44314);  /// \htmlonly <table><tr><td width="300" bgcolor="#3CB371">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumSlateBlue     (0.48235,0.40784,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#7B68EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumSpringGreen   (0.00000,0.98039,0.60392);  /// \htmlonly <table><tr><td width="300" bgcolor="#00FA9A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumTurquoise     (0.28235,0.81961,0.80000);  /// \htmlonly <table><tr><td width="300" bgcolor="#48D1CC">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mediumVioletRed     (0.78039,0.08235,0.52157);  /// \htmlonly <table><tr><td width="300" bgcolor="#C71585">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::midnightBlue        (0.09804,0.09804,0.43922);  /// \htmlonly <table><tr><td width="300" bgcolor="#191970">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mintCream           (0.96078,1.00000,0.98039);  /// \htmlonly <table><tr><td width="300" bgcolor="#F5FFFA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mistyRose           (1.00000,0.89412,0.88235);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFE4E1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mistyRose1          (1.00000,0.89412,0.88235);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFE4E1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mistyRose2          (0.93333,0.83529,0.82353);  /// \htmlonly <table><tr><td width="300" bgcolor="#EED5D2">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mistyRose3          (0.80392,0.71765,0.70980);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDB7B5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::mistyRose4          (0.54510,0.49020,0.48235);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B7D7B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::moccasin            (1.00000,0.89412,0.70980);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFE4B5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::navajoWhite         (1.00000,0.87059,0.67843);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFDEAD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::navajoWhite1        (1.00000,0.87059,0.67843);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFDEAD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::navajoWhite2        (0.93333,0.81176,0.63137);  /// \htmlonly <table><tr><td width="300" bgcolor="#EECFA1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::navajoWhite3        (0.80392,0.70196,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDB38B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::navajoWhite4        (0.54510,0.47451,0.36863);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B795E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::navy                (0.00000,0.00000,0.50196);  /// \htmlonly <table><tr><td width="300" bgcolor="#000080">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::navyBlue            (0.00000,0.00000,0.50196);  /// \htmlonly <table><tr><td width="300" bgcolor="#000080">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::oldLace             (0.99216,0.96078,0.90196);  /// \htmlonly <table><tr><td width="300" bgcolor="#FDF5E6">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::oliveDrab           (0.41961,0.55686,0.13725);  /// \htmlonly <table><tr><td width="300" bgcolor="#6B8E23">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::oliveDrab1          (0.75294,1.00000,0.24314);  /// \htmlonly <table><tr><td width="300" bgcolor="#C0FF3E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::oliveDrab2          (0.70196,0.93333,0.22745);  /// \htmlonly <table><tr><td width="300" bgcolor="#B3EE3A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::oliveDrab3          (0.60392,0.80392,0.19608);  /// \htmlonly <table><tr><td width="300" bgcolor="#9ACD32">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::oliveDrab4          (0.41176,0.54510,0.13333);  /// \htmlonly <table><tr><td width="300" bgcolor="#698B22">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orange              (1.00000,0.64706,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFA500">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orange1             (1.00000,0.64706,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFA500">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orange2             (0.93333,0.60392,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE9A00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orange3             (0.80392,0.52157,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD8500">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orange4             (0.54510,0.35294,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B5A00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orangeRed           (1.00000,0.27059,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF4500">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orangeRed1          (1.00000,0.27059,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF4500">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orangeRed2          (0.93333,0.25098,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE4000">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orangeRed3          (0.80392,0.21569,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD3700">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orangeRed4          (0.54510,0.14510,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B2500">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orchid              (0.85490,0.43922,0.83922);  /// \htmlonly <table><tr><td width="300" bgcolor="#DA70D6">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orchid1             (1.00000,0.51373,0.98039);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF83FA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orchid2             (0.93333,0.47843,0.91373);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE7AE9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orchid3             (0.80392,0.41176,0.78824);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD69C9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::orchid4             (0.54510,0.27843,0.53725);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B4789">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleGoldenrod       (0.93333,0.90980,0.66667);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEE8AA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleGreen           (0.59608,0.98431,0.59608);  /// \htmlonly <table><tr><td width="300" bgcolor="#98FB98">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleGreen1          (0.60392,1.00000,0.60392);  /// \htmlonly <table><tr><td width="300" bgcolor="#9AFF9A">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleGreen2          (0.56471,0.93333,0.56471);  /// \htmlonly <table><tr><td width="300" bgcolor="#90EE90">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleGreen3          (0.48627,0.80392,0.48627);  /// \htmlonly <table><tr><td width="300" bgcolor="#7CCD7C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleGreen4          (0.32941,0.54510,0.32941);  /// \htmlonly <table><tr><td width="300" bgcolor="#548B54">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleTurquoise       (0.68627,0.93333,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#AFEEEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleTurquoise1      (0.73333,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#BBFFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleTurquoise2      (0.68235,0.93333,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#AEEEEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleTurquoise3      (0.58824,0.80392,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#96CDCD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleTurquoise4      (0.40000,0.54510,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#668B8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleVioletRed       (0.85882,0.43922,0.57647);  /// \htmlonly <table><tr><td width="300" bgcolor="#DB7093">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleVioletRed1      (1.00000,0.50980,0.67059);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF82AB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleVioletRed2      (0.93333,0.47451,0.62353);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE799F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleVioletRed3      (0.80392,0.40784,0.53725);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD6889">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::paleVioletRed4      (0.54510,0.27843,0.36471);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B475D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::papayaWhip          (1.00000,0.93725,0.83529);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFEFD5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::peachPuff           (1.00000,0.85490,0.72549);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFDAB9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::peachPuff1          (1.00000,0.85490,0.72549);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFDAB9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::peachPuff2          (0.93333,0.79608,0.67843);  /// \htmlonly <table><tr><td width="300" bgcolor="#EECBAD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::peachPuff3          (0.80392,0.68627,0.58431);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDAF95">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::peachPuff4          (0.54510,0.46667,0.39608);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B7765">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::peru                (0.80392,0.52157,0.24706);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD853F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::pink                (1.00000,0.75294,0.79608);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFC0CB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::pink1               (1.00000,0.70980,0.77255);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFB5C5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::pink2               (0.93333,0.66275,0.72157);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEA9B8">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::pink3               (0.80392,0.56863,0.61961);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD919E">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::pink4               (0.54510,0.38824,0.42353);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B636C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::plum                (0.86667,0.62745,0.86667);  /// \htmlonly <table><tr><td width="300" bgcolor="#DDA0DD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::plum1               (1.00000,0.73333,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFBBFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::plum2               (0.93333,0.68235,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEAEEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::plum3               (0.80392,0.58824,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD96CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::plum4               (0.54510,0.40000,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B668B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::powderBlue          (0.69020,0.87843,0.90196);  /// \htmlonly <table><tr><td width="300" bgcolor="#B0E0E6">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::purple              (0.62745,0.12549,0.94118);  /// \htmlonly <table><tr><td width="300" bgcolor="#A020F0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::purple1             (0.60784,0.18824,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#9B30FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::purple2             (0.56863,0.17255,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#912CEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::purple3             (0.49020,0.14902,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#7D26CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::purple4             (0.33333,0.10196,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#551A8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::red                 (1.00000,0.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF0000">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::red1                (1.00000,0.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF0000">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::red2                (0.93333,0.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE0000">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::red3                (0.80392,0.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD0000">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::red4                (0.54510,0.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B0000">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::rosyBrown           (0.73725,0.56078,0.56078);  /// \htmlonly <table><tr><td width="300" bgcolor="#BC8F8F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::rosyBrown1          (1.00000,0.75686,0.75686);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFC1C1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::rosyBrown2          (0.93333,0.70588,0.70588);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEB4B4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::rosyBrown3          (0.80392,0.60784,0.60784);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD9B9B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::rosyBrown4          (0.54510,0.41176,0.41176);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B6969">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::royalBlue           (0.25490,0.41176,0.88235);  /// \htmlonly <table><tr><td width="300" bgcolor="#4169E1">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::royalBlue1          (0.28235,0.46275,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#4876FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::royalBlue2          (0.26275,0.43137,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#436EEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::royalBlue3          (0.22745,0.37255,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#3A5FCD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::royalBlue4          (0.15294,0.25098,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#27408B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::saddleBrown         (0.54510,0.27059,0.07451);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B4513">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::salmon              (0.98039,0.50196,0.44706);  /// \htmlonly <table><tr><td width="300" bgcolor="#FA8072">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::salmon1             (1.00000,0.54902,0.41176);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF8C69">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::salmon2             (0.93333,0.50980,0.38431);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE8262">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::salmon3             (0.80392,0.43922,0.32941);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD7054">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::salmon4             (0.54510,0.29804,0.22353);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B4C39">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::sandyBrown          (0.95686,0.64314,0.37647);  /// \htmlonly <table><tr><td width="300" bgcolor="#F4A460">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::seaGreen            (0.18039,0.54510,0.34118);  /// \htmlonly <table><tr><td width="300" bgcolor="#2E8B57">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::seaGreen1           (0.32941,1.00000,0.62353);  /// \htmlonly <table><tr><td width="300" bgcolor="#54FF9F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::seaGreen2           (0.30588,0.93333,0.58039);  /// \htmlonly <table><tr><td width="300" bgcolor="#4EEE94">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::seaGreen3           (0.26275,0.80392,0.50196);  /// \htmlonly <table><tr><td width="300" bgcolor="#43CD80">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::seaGreen4           (0.18039,0.54510,0.34118);  /// \htmlonly <table><tr><td width="300" bgcolor="#2E8B57">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::seashell            (1.00000,0.96078,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFF5EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::seashell1           (1.00000,0.96078,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFF5EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::seashell2           (0.93333,0.89804,0.87059);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEE5DE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::seashell3           (0.80392,0.77255,0.74902);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDC5BF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::seashell4           (0.54510,0.52549,0.50980);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B8682">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::sienna              (0.62745,0.32157,0.17647);  /// \htmlonly <table><tr><td width="300" bgcolor="#A0522D">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::sienna1             (1.00000,0.50980,0.27843);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF8247">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::sienna2             (0.93333,0.47451,0.25882);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE7942">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::sienna3             (0.80392,0.40784,0.22353);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD6839">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::sienna4             (0.54510,0.27843,0.14902);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B4726">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::skyBlue             (0.52941,0.80784,0.92157);  /// \htmlonly <table><tr><td width="300" bgcolor="#87CEEB">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::skyBlue1            (0.52941,0.80784,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#87CEFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::skyBlue2            (0.49412,0.75294,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#7EC0EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::skyBlue3            (0.42353,0.65098,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#6CA6CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::skyBlue4            (0.29020,0.43922,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#4A708B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateBlue           (0.41569,0.35294,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#6A5ACD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateBlue1          (0.51373,0.43529,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#836FFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateBlue2          (0.47843,0.40392,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#7A67EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateBlue3          (0.41176,0.34902,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#6959CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateBlue4          (0.27843,0.23529,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#473C8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateGray           (0.43922,0.50196,0.56471);  /// \htmlonly <table><tr><td width="300" bgcolor="#708090">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateGray1          (0.77647,0.88627,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#C6E2FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateGray2          (0.72549,0.82745,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#B9D3EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateGray3          (0.62353,0.71373,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#9FB6CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateGray4          (0.42353,0.48235,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#6C7B8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::slateGrey           (0.43922,0.50196,0.56471);  /// \htmlonly <table><tr><td width="300" bgcolor="#708090">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::snow                (1.00000,0.98039,0.98039);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFAFA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::snow1               (1.00000,0.98039,0.98039);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFAFA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::snow2               (0.93333,0.91373,0.91373);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEE9E9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::snow3               (0.80392,0.78824,0.78824);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDC9C9">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::snow4               (0.54510,0.53725,0.53725);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B8989">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::springGreen         (0.00000,1.00000,0.49804);  /// \htmlonly <table><tr><td width="300" bgcolor="#00FF7F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::springGreen1        (0.00000,1.00000,0.49804);  /// \htmlonly <table><tr><td width="300" bgcolor="#00FF7F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::springGreen2        (0.00000,0.93333,0.46275);  /// \htmlonly <table><tr><td width="300" bgcolor="#00EE76">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::springGreen3        (0.00000,0.80392,0.40000);  /// \htmlonly <table><tr><td width="300" bgcolor="#00CD66">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::springGreen4        (0.00000,0.54510,0.27059);  /// \htmlonly <table><tr><td width="300" bgcolor="#008B45">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::steelBlue           (0.27451,0.50980,0.70588);  /// \htmlonly <table><tr><td width="300" bgcolor="#4682B4">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::steelBlue1          (0.38824,0.72157,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#63B8FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::steelBlue2          (0.36078,0.67451,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#5CACEE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::steelBlue3          (0.30980,0.58039,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#4F94CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::steelBlue4          (0.21176,0.39216,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#36648B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::tan1                (1.00000,0.64706,0.30980);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFA54F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::tan2                (0.93333,0.60392,0.28627);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE9A49">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::tan3                (0.80392,0.52157,0.24706);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD853F">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::tan4                (0.54510,0.35294,0.16863);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B5A2B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::thistle             (0.84706,0.74902,0.84706);  /// \htmlonly <table><tr><td width="300" bgcolor="#D8BFD8">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::thistle1            (1.00000,0.88235,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFE1FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::thistle2            (0.93333,0.82353,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#EED2EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::thistle3            (0.80392,0.70980,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDB5CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::thistle4            (0.54510,0.48235,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B7B8B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::tomato              (1.00000,0.38824,0.27843);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF6347">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::tomato1             (1.00000,0.38824,0.27843);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF6347">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::tomato2             (0.93333,0.36078,0.25882);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE5C42">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::tomato3             (0.80392,0.30980,0.22353);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD4F39">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::tomato4             (0.54510,0.21176,0.14902);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B3626">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::turquoise           (0.25098,0.87843,0.81569);  /// \htmlonly <table><tr><td width="300" bgcolor="#40E0D0">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::turquoise1          (0.00000,0.96078,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#00F5FF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::turquoise2          (0.00000,0.89804,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#00E5EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::turquoise3          (0.00000,0.77255,0.80392);  /// \htmlonly <table><tr><td width="300" bgcolor="#00C5CD">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::turquoise4          (0.00000,0.52549,0.54510);  /// \htmlonly <table><tr><td width="300" bgcolor="#00868B">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::violet              (0.93333,0.50980,0.93333);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE82EE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::violetRed           (0.81569,0.12549,0.56471);  /// \htmlonly <table><tr><td width="300" bgcolor="#D02090">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::violetRed1          (1.00000,0.24314,0.58824);  /// \htmlonly <table><tr><td width="300" bgcolor="#FF3E96">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::violetRed2          (0.93333,0.22745,0.54902);  /// \htmlonly <table><tr><td width="300" bgcolor="#EE3A8C">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::violetRed3          (0.80392,0.19608,0.47059);  /// \htmlonly <table><tr><td width="300" bgcolor="#CD3278">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::violetRed4          (0.54510,0.13333,0.32157);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B2252">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::wheat               (0.96078,0.87059,0.70196);  /// \htmlonly <table><tr><td width="300" bgcolor="#F5DEB3">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::wheat1              (1.00000,0.90588,0.72941);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFE7BA">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::wheat2              (0.93333,0.84706,0.68235);  /// \htmlonly <table><tr><td width="300" bgcolor="#EED8AE">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::wheat3              (0.80392,0.72941,0.58824);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDBA96">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::wheat4              (0.54510,0.49412,0.40000);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B7E66">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::white               (1.00000,1.00000,1.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFFFF">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::whiteSmoke          (0.96078,0.96078,0.96078);  /// \htmlonly <table><tr><td width="300" bgcolor="#F5F5F5">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::yellow              (1.00000,1.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFF00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::yellow1             (1.00000,1.00000,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#FFFF00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::yellow2             (0.93333,0.93333,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#EEEE00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::yellow3             (0.80392,0.80392,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#CDCD00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::yellow4             (0.54510,0.54510,0.00000);  /// \htmlonly <table><tr><td width="300" bgcolor="#8B8B00">&nbsp; </td></tr></table> \endhtmlonly
const Colorf ColorDefine::yellowGreen         (0.60392,0.80392,0.19608);  /// \htmlonly <table><tr><td width="300" bgcolor="#9ACD32">&nbsp; </td></tr></table> \endhtmlonly

const char *ColorDefine::Name[] =
{
    "aliceBlue",
    "antiqueWhite",
    "antiqueWhite1",
    "antiqueWhite2",
    "antiqueWhite3",
    "antiqueWhite4",
    "aquamarine",
    "aquamarine1",
    "aquamarine2",
    "aquamarine3",
    "aquamarine4",
    "azure",
    "azure1",
    "azure2",
    "azure3",
    "azure4",
    "beige",
    "bisque",
    "bisque1",
    "bisque2",
    "bisque3",
    "bisque4",
    "black",
    "blanchedAlmond",
    "blue",
    "blue1",
    "blue2",
    "blue3",
    "blue4",
    "blueViolet",
    "brown",
    "brown1",
    "brown2",
    "brown3",
    "brown4",
    "burlywood",
    "burlywood1",
    "burlywood2",
    "burlywood3",
    "burlywood4",
    "cadetBlue",
    "cadetBlue1",
    "cadetBlue2",
    "cadetBlue3",
    "cadetBlue4",
    "chartreuse",
    "chartreuse1",
    "chartreuse2",
    "chartreuse3",
    "chartreuse4",
    "chocolate",
    "chocolate1",
    "chocolate2",
    "chocolate3",
    "chocolate4",
    "coral",
    "coral1",
    "coral2",
    "coral3",
    "coral4",
    "cornflowerBlue",
    "cornsilk",
    "cornsilk1",
    "cornsilk2",
    "cornsilk3",
    "cornsilk4",
    "cyan",
    "cyan1",
    "cyan2",
    "cyan3",
    "cyan4",
    "darkBlue",
    "darkCyan",
    "darkGoldenrod",
    "darkGoldenrod1",
    "darkGoldenrod2",
    "darkGoldenrod3",
    "darkGoldenrod4",
    "darkGray",
    "darkGreen",
    "darkGrey",
    "darkKhaki",
    "darkMagenta",
    "darkOliveGreen",
    "darkOliveGreen1",
    "darkOliveGreen2",
    "darkOliveGreen3",
    "darkOliveGreen4",
    "darkOrange",
    "darkOrange1",
    "darkOrange2",
    "darkOrange3",
    "darkOrange4",
    "darkOrchid",
    "darkOrchid1",
    "darkOrchid2",
    "darkOrchid3",
    "darkOrchid4",
    "darkRed",
    "darkSalmon",
    "darkSeaGreen",
    "darkSeaGreen1",
    "darkSeaGreen2",
    "darkSeaGreen3",
    "darkSeaGreen4",
    "darkSlateBlue",
    "darkSlateGray",
    "darkSlateGray1",
    "darkSlateGray2",
    "darkSlateGray3",
    "darkSlateGray4",
    "darkSlateGrey",
    "darkTurquoise",
    "darkViolet",
    "deepPink",
    "deepPink1",
    "deepPink2",
    "deepPink3",
    "deepPink4",
    "deepSkyBlue",
    "deepSkyBlue1",
    "deepSkyBlue2",
    "deepSkyBlue3",
    "deepSkyBlue4",
    "dimGray",
    "dimGrey",
    "dodgerBlue",
    "dodgerBlue1",
    "dodgerBlue2",
    "dodgerBlue3",
    "dodgerBlue4",
    "firebrick",
    "firebrick1",
    "firebrick2",
    "firebrick3",
    "firebrick4",
    "floralWhite",
    "forestGreen",
    "gainsboro",
    "ghostWhite",
    "gold",
    "gold1",
    "gold2",
    "gold3",
    "gold4",
    "goldenrod",
    "goldenrod1",
    "goldenrod2",
    "goldenrod3",
    "goldenrod4",
    "gray",
    "gray0",
    "gray1",
    "gray10",
    "gray100",
    "gray11",
    "gray12",
    "gray13",
    "gray14",
    "gray15",
    "gray16",
    "gray17",
    "gray18",
    "gray19",
    "gray2",
    "gray20",
    "gray21",
    "gray22",
    "gray23",
    "gray24",
    "gray25",
    "gray26",
    "gray27",
    "gray28",
    "gray29",
    "gray3",
    "gray30",
    "gray31",
    "gray32",
    "gray33",
    "gray34",
    "gray35",
    "gray36",
    "gray37",
    "gray38",
    "gray39",
    "gray4",
    "gray40",
    "gray41",
    "gray42",
    "gray43",
    "gray44",
    "gray45",
    "gray46",
    "gray47",
    "gray48",
    "gray49",
    "gray5",
    "gray50",
    "gray51",
    "gray52",
    "gray53",
    "gray54",
    "gray55",
    "gray56",
    "gray57",
    "gray58",
    "gray59",
    "gray6",
    "gray60",
    "gray61",
    "gray62",
    "gray63",
    "gray64",
    "gray65",
    "gray66",
    "gray67",
    "gray68",
    "gray69",
    "gray7",
    "gray70",
    "gray71",
    "gray72",
    "gray73",
    "gray74",
    "gray75",
    "gray76",
    "gray77",
    "gray78",
    "gray79",
    "gray8",
    "gray80",
    "gray81",
    "gray82",
    "gray83",
    "gray84",
    "gray85",
    "gray86",
    "gray87",
    "gray88",
    "gray89",
    "gray9",
    "gray90",
    "gray91",
    "gray92",
    "gray93",
    "gray94",
    "gray95",
    "gray96",
    "gray97",
    "gray98",
    "gray99",
    "green",
    "green1",
    "green2",
    "green3",
    "green4",
    "greenYellow",
    "grey",
    "grey0",
    "grey1",
    "grey10",
    "grey100",
    "grey11",
    "grey12",
    "grey13",
    "grey14",
    "grey15",
    "grey16",
    "grey17",
    "grey18",
    "grey19",
    "grey2",
    "grey20",
    "grey21",
    "grey22",
    "grey23",
    "grey24",
    "grey25",
    "grey26",
    "grey27",
    "grey28",
    "grey29",
    "grey3",
    "grey30",
    "grey31",
    "grey32",
    "grey33",
    "grey34",
    "grey35",
    "grey36",
    "grey37",
    "grey38",
    "grey39",
    "grey4",
    "grey40",
    "grey41",
    "grey42",
    "grey43",
    "grey44",
    "grey45",
    "grey46",
    "grey47",
    "grey48",
    "grey49",
    "grey5",
    "grey50",
    "grey51",
    "grey52",
    "grey53",
    "grey54",
    "grey55",
    "grey56",
    "grey57",
    "grey58",
    "grey59",
    "grey6",
    "grey60",
    "grey61",
    "grey62",
    "grey63",
    "grey64",
    "grey65",
    "grey66",
    "grey67",
    "grey68",
    "grey69",
    "grey7",
    "grey70",
    "grey71",
    "grey72",
    "grey73",
    "grey74",
    "grey75",
    "grey76",
    "grey77",
    "grey78",
    "grey79",
    "grey8",
    "grey80",
    "grey81",
    "grey82",
    "grey83",
    "grey84",
    "grey85",
    "grey86",
    "grey87",
    "grey88",
    "grey89",
    "grey9",
    "grey90",
    "grey91",
    "grey92",
    "grey93",
    "grey94",
    "grey95",
    "grey96",
    "grey97",
    "grey98",
    "grey99",
    "honeydew",
    "honeydew1",
    "honeydew2",
    "honeydew3",
    "honeydew4",
    "hotPink",
    "hotPink1",
    "hotPink2",
    "hotPink3",
    "hotPink4",
    "indianRed",
    "indianRed1",
    "indianRed2",
    "indianRed3",
    "indianRed4",
    "ivory",
    "ivory1",
    "ivory2",
    "ivory3",
    "ivory4",
    "khaki",
    "khaki1",
    "khaki2",
    "khaki3",
    "khaki4",
    "lavender",
    "lavenderBlush",
    "lavenderBlush1",
    "lavenderBlush2",
    "lavenderBlush3",
    "lavenderBlush4",
    "lawnGreen",
    "lemonChiffon",
    "lemonChiffon1",
    "lemonChiffon2",
    "lemonChiffon3",
    "lemonChiffon4",
    "lightBlue",
    "lightBlue1",
    "lightBlue2",
    "lightBlue3",
    "lightBlue4",
    "lightCoral",
    "lightCyan",
    "lightCyan1",
    "lightCyan2",
    "lightCyan3",
    "lightCyan4",
    "lightGoldenrod",
    "lightGoldenrod1",
    "lightGoldenrod2",
    "lightGoldenrod3",
    "lightGoldenrod4",
    "lightGoldenrodYellow",
    "lightGray",
    "lightGreen",
    "lightGrey",
    "lightPink",
    "lightPink1",
    "lightPink2",
    "lightPink3",
    "lightPink4",
    "lightSalmon",
    "lightSalmon1",
    "lightSalmon2",
    "lightSalmon3",
    "lightSalmon4",
    "lightSeaGreen",
    "lightSkyBlue",
    "lightSkyBlue1",
    "lightSkyBlue2",
    "lightSkyBlue3",
    "lightSkyBlue4",
    "lightSlateBlue",
    "lightSlateGray",
    "lightSlateGrey",
    "lightSteelBlue",
    "lightSteelBlue1",
    "lightSteelBlue2",
    "lightSteelBlue3",
    "lightSteelBlue4",
    "lightYellow",
    "lightYellow1",
    "lightYellow2",
    "lightYellow3",
    "lightYellow4",
    "limeGreen",
    "linen",
    "magenta",
    "magenta1",
    "magenta2",
    "magenta3",
    "magenta4",
    "maroon",
    "maroon1",
    "maroon2",
    "maroon3",
    "maroon4",
    "mediumAquamarine",
    "mediumBlue",
    "mediumOrchid",
    "mediumOrchid1",
    "mediumOrchid2",
    "mediumOrchid3",
    "mediumOrchid4",
    "mediumPurple",
    "mediumPurple1",
    "mediumPurple2",
    "mediumPurple3",
    "mediumPurple4",
    "mediumSeaGreen",
    "mediumSlateBlue",
    "mediumSpringGreen",
    "mediumTurquoise",
    "mediumVioletRed",
    "midnightBlue",
    "mintCream",
    "mistyRose",
    "mistyRose1",
    "mistyRose2",
    "mistyRose3",
    "mistyRose4",
    "moccasin",
    "navajoWhite",
    "navajoWhite1",
    "navajoWhite2",
    "navajoWhite3",
    "navajoWhite4",
    "navy",
    "navyBlue",
    "oldLace",
    "oliveDrab",
    "oliveDrab1",
    "oliveDrab2",
    "oliveDrab3",
    "oliveDrab4",
    "orange",
    "orange1",
    "orange2",
    "orange3",
    "orange4",
    "orangeRed",
    "orangeRed1",
    "orangeRed2",
    "orangeRed3",
    "orangeRed4",
    "orchid",
    "orchid1",
    "orchid2",
    "orchid3",
    "orchid4",
    "paleGoldenrod",
    "paleGreen",
    "paleGreen1",
    "paleGreen2",
    "paleGreen3",
    "paleGreen4",
    "paleTurquoise",
    "paleTurquoise1",
    "paleTurquoise2",
    "paleTurquoise3",
    "paleTurquoise4",
    "paleVioletRed",
    "paleVioletRed1",
    "paleVioletRed2",
    "paleVioletRed3",
    "paleVioletRed4",
    "papayaWhip",
    "peachPuff",
    "peachPuff1",
    "peachPuff2",
    "peachPuff3",
    "peachPuff4",
    "peru",
    "pink",
    "pink1",
    "pink2",
    "pink3",
    "pink4",
    "plum",
    "plum1",
    "plum2",
    "plum3",
    "plum4",
    "powderBlue",
    "purple",
    "purple1",
    "purple2",
    "purple3",
    "purple4",
    "red",
    "red1",
    "red2",
    "red3",
    "red4",
    "rosyBrown",
    "rosyBrown1",
    "rosyBrown2",
    "rosyBrown3",
    "rosyBrown4",
    "royalBlue",
    "royalBlue1",
    "royalBlue2",
    "royalBlue3",
    "royalBlue4",
    "saddleBrown",
    "salmon",
    "salmon1",
    "salmon2",
    "salmon3",
    "salmon4",
    "sandyBrown",
    "seaGreen",
    "seaGreen1",
    "seaGreen2",
    "seaGreen3",
    "seaGreen4",
    "seashell",
    "seashell1",
    "seashell2",
    "seashell3",
    "seashell4",
    "sienna",
    "sienna1",
    "sienna2",
    "sienna3",
    "sienna4",
    "skyBlue",
    "skyBlue1",
    "skyBlue2",
    "skyBlue3",
    "skyBlue4",
    "slateBlue",
    "slateBlue1",
    "slateBlue2",
    "slateBlue3",
    "slateBlue4",
    "slateGray",
    "slateGray1",
    "slateGray2",
    "slateGray3",
    "slateGray4",
    "slateGrey",
    "snow",
    "snow1",
    "snow2",
    "snow3",
    "snow4",
    "springGreen",
    "springGreen1",
    "springGreen2",
    "springGreen3",
    "springGreen4",
    "steelBlue",
    "steelBlue1",
    "steelBlue2",
    "steelBlue3",
    "steelBlue4",
    "tan1",
    "tan2",
    "tan3",
    "tan4",
    "thistle",
    "thistle1",
    "thistle2",
    "thistle3",
    "thistle4",
    "tomato",
    "tomato1",
    "tomato2",
    "tomato3",
    "tomato4",
    "turquoise",
    "turquoise1",
    "turquoise2",
    "turquoise3",
    "turquoise4",
    "violet",
    "violetRed",
    "violetRed1",
    "violetRed2",
    "violetRed3",
    "violetRed4",
    "wheat",
    "wheat1",
    "wheat2",
    "wheat3",
    "wheat4",
    "white",
    "whiteSmoke",
    "yellow",
    "yellow1",
    "yellow2",
    "yellow3",
    "yellow4",
    "yellowGreen"
};

const Colorf* ColorDefine::Value[] =
{
    &ColorDefine::aliceBlue,
    &ColorDefine::antiqueWhite,
    &ColorDefine::antiqueWhite1,
    &ColorDefine::antiqueWhite2,
    &ColorDefine::antiqueWhite3,
    &ColorDefine::antiqueWhite4,
    &ColorDefine::aquamarine,
    &ColorDefine::aquamarine1,
    &ColorDefine::aquamarine2,
    &ColorDefine::aquamarine3,
    &ColorDefine::aquamarine4,
    &ColorDefine::azure,
    &ColorDefine::azure1,
    &ColorDefine::azure2,
    &ColorDefine::azure3,
    &ColorDefine::azure4,
    &ColorDefine::beige,
    &ColorDefine::bisque,
    &ColorDefine::bisque1,
    &ColorDefine::bisque2,
    &ColorDefine::bisque3,
    &ColorDefine::bisque4,
    &ColorDefine::black,
    &ColorDefine::blanchedAlmond,
    &ColorDefine::blue,
    &ColorDefine::blue1,
    &ColorDefine::blue2,
    &ColorDefine::blue3,
    &ColorDefine::blue4,
    &ColorDefine::blueViolet,
    &ColorDefine::brown,
    &ColorDefine::brown1,
    &ColorDefine::brown2,
    &ColorDefine::brown3,
    &ColorDefine::brown4,
    &ColorDefine::burlywood,
    &ColorDefine::burlywood1,
    &ColorDefine::burlywood2,
    &ColorDefine::burlywood3,
    &ColorDefine::burlywood4,
    &ColorDefine::cadetBlue,
    &ColorDefine::cadetBlue1,
    &ColorDefine::cadetBlue2,
    &ColorDefine::cadetBlue3,
    &ColorDefine::cadetBlue4,
    &ColorDefine::chartreuse,
    &ColorDefine::chartreuse1,
    &ColorDefine::chartreuse2,
    &ColorDefine::chartreuse3,
    &ColorDefine::chartreuse4,
    &ColorDefine::chocolate,
    &ColorDefine::chocolate1,
    &ColorDefine::chocolate2,
    &ColorDefine::chocolate3,
    &ColorDefine::chocolate4,
    &ColorDefine::coral,
    &ColorDefine::coral1,
    &ColorDefine::coral2,
    &ColorDefine::coral3,
    &ColorDefine::coral4,
    &ColorDefine::cornflowerBlue,
    &ColorDefine::cornsilk,
    &ColorDefine::cornsilk1,
    &ColorDefine::cornsilk2,
    &ColorDefine::cornsilk3,
    &ColorDefine::cornsilk4,
    &ColorDefine::cyan,
    &ColorDefine::cyan1,
    &ColorDefine::cyan2,
    &ColorDefine::cyan3,
    &ColorDefine::cyan4,
    &ColorDefine::darkBlue,
    &ColorDefine::darkCyan,
    &ColorDefine::darkGoldenrod,
    &ColorDefine::darkGoldenrod1,
    &ColorDefine::darkGoldenrod2,
    &ColorDefine::darkGoldenrod3,
    &ColorDefine::darkGoldenrod4,
    &ColorDefine::darkGray,
    &ColorDefine::darkGreen,
    &ColorDefine::darkGrey,
    &ColorDefine::darkKhaki,
    &ColorDefine::darkMagenta,
    &ColorDefine::darkOliveGreen,
    &ColorDefine::darkOliveGreen1,
    &ColorDefine::darkOliveGreen2,
    &ColorDefine::darkOliveGreen3,
    &ColorDefine::darkOliveGreen4,
    &ColorDefine::darkOrange,
    &ColorDefine::darkOrange1,
    &ColorDefine::darkOrange2,
    &ColorDefine::darkOrange3,
    &ColorDefine::darkOrange4,
    &ColorDefine::darkOrchid,
    &ColorDefine::darkOrchid1,
    &ColorDefine::darkOrchid2,
    &ColorDefine::darkOrchid3,
    &ColorDefine::darkOrchid4,
    &ColorDefine::darkRed,
    &ColorDefine::darkSalmon,
    &ColorDefine::darkSeaGreen,
    &ColorDefine::darkSeaGreen1,
    &ColorDefine::darkSeaGreen2,
    &ColorDefine::darkSeaGreen3,
    &ColorDefine::darkSeaGreen4,
    &ColorDefine::darkSlateBlue,
    &ColorDefine::darkSlateGray,
    &ColorDefine::darkSlateGray1,
    &ColorDefine::darkSlateGray2,
    &ColorDefine::darkSlateGray3,
    &ColorDefine::darkSlateGray4,
    &ColorDefine::darkSlateGrey,
    &ColorDefine::darkTurquoise,
    &ColorDefine::darkViolet,
    &ColorDefine::deepPink,
    &ColorDefine::deepPink1,
    &ColorDefine::deepPink2,
    &ColorDefine::deepPink3,
    &ColorDefine::deepPink4,
    &ColorDefine::deepSkyBlue,
    &ColorDefine::deepSkyBlue1,
    &ColorDefine::deepSkyBlue2,
    &ColorDefine::deepSkyBlue3,
    &ColorDefine::deepSkyBlue4,
    &ColorDefine::dimGray,
    &ColorDefine::dimGrey,
    &ColorDefine::dodgerBlue,
    &ColorDefine::dodgerBlue1,
    &ColorDefine::dodgerBlue2,
    &ColorDefine::dodgerBlue3,
    &ColorDefine::dodgerBlue4,
    &ColorDefine::firebrick,
    &ColorDefine::firebrick1,
    &ColorDefine::firebrick2,
    &ColorDefine::firebrick3,
    &ColorDefine::firebrick4,
    &ColorDefine::floralWhite,
    &ColorDefine::forestGreen,
    &ColorDefine::gainsboro,
    &ColorDefine::ghostWhite,
    &ColorDefine::gold,
    &ColorDefine::gold1,
    &ColorDefine::gold2,
    &ColorDefine::gold3,
    &ColorDefine::gold4,
    &ColorDefine::goldenrod,
    &ColorDefine::goldenrod1,
    &ColorDefine::goldenrod2,
    &ColorDefine::goldenrod3,
    &ColorDefine::goldenrod4,
    &ColorDefine::gray,
    &ColorDefine::gray0,
    &ColorDefine::gray1,
    &ColorDefine::gray10,
    &ColorDefine::gray100,
    &ColorDefine::gray11,
    &ColorDefine::gray12,
    &ColorDefine::gray13,
    &ColorDefine::gray14,
    &ColorDefine::gray15,
    &ColorDefine::gray16,
    &ColorDefine::gray17,
    &ColorDefine::gray18,
    &ColorDefine::gray19,
    &ColorDefine::gray2,
    &ColorDefine::gray20,
    &ColorDefine::gray21,
    &ColorDefine::gray22,
    &ColorDefine::gray23,
    &ColorDefine::gray24,
    &ColorDefine::gray25,
    &ColorDefine::gray26,
    &ColorDefine::gray27,
    &ColorDefine::gray28,
    &ColorDefine::gray29,
    &ColorDefine::gray3,
    &ColorDefine::gray30,
    &ColorDefine::gray31,
    &ColorDefine::gray32,
    &ColorDefine::gray33,
    &ColorDefine::gray34,
    &ColorDefine::gray35,
    &ColorDefine::gray36,
    &ColorDefine::gray37,
    &ColorDefine::gray38,
    &ColorDefine::gray39,
    &ColorDefine::gray4,
    &ColorDefine::gray40,
    &ColorDefine::gray41,
    &ColorDefine::gray42,
    &ColorDefine::gray43,
    &ColorDefine::gray44,
    &ColorDefine::gray45,
    &ColorDefine::gray46,
    &ColorDefine::gray47,
    &ColorDefine::gray48,
    &ColorDefine::gray49,
    &ColorDefine::gray5,
    &ColorDefine::gray50,
    &ColorDefine::gray51,
    &ColorDefine::gray52,
    &ColorDefine::gray53,
    &ColorDefine::gray54,
    &ColorDefine::gray55,
    &ColorDefine::gray56,
    &ColorDefine::gray57,
    &ColorDefine::gray58,
    &ColorDefine::gray59,
    &ColorDefine::gray6,
    &ColorDefine::gray60,
    &ColorDefine::gray61,
    &ColorDefine::gray62,
    &ColorDefine::gray63,
    &ColorDefine::gray64,
    &ColorDefine::gray65,
    &ColorDefine::gray66,
    &ColorDefine::gray67,
    &ColorDefine::gray68,
    &ColorDefine::gray69,
    &ColorDefine::gray7,
    &ColorDefine::gray70,
    &ColorDefine::gray71,
    &ColorDefine::gray72,
    &ColorDefine::gray73,
    &ColorDefine::gray74,
    &ColorDefine::gray75,
    &ColorDefine::gray76,
    &ColorDefine::gray77,
    &ColorDefine::gray78,
    &ColorDefine::gray79,
    &ColorDefine::gray8,
    &ColorDefine::gray80,
    &ColorDefine::gray81,
    &ColorDefine::gray82,
    &ColorDefine::gray83,
    &ColorDefine::gray84,
    &ColorDefine::gray85,
    &ColorDefine::gray86,
    &ColorDefine::gray87,
    &ColorDefine::gray88,
    &ColorDefine::gray89,
    &ColorDefine::gray9,
    &ColorDefine::gray90,
    &ColorDefine::gray91,
    &ColorDefine::gray92,
    &ColorDefine::gray93,
    &ColorDefine::gray94,
    &ColorDefine::gray95,
    &ColorDefine::gray96,
    &ColorDefine::gray97,
    &ColorDefine::gray98,
    &ColorDefine::gray99,
    &ColorDefine::green,
    &ColorDefine::green1,
    &ColorDefine::green2,
    &ColorDefine::green3,
    &ColorDefine::green4,
    &ColorDefine::greenYellow,
    &ColorDefine::grey,
    &ColorDefine::grey0,
    &ColorDefine::grey1,
    &ColorDefine::grey10,
    &ColorDefine::grey100,
    &ColorDefine::grey11,
    &ColorDefine::grey12,
    &ColorDefine::grey13,
    &ColorDefine::grey14,
    &ColorDefine::grey15,
    &ColorDefine::grey16,
    &ColorDefine::grey17,
    &ColorDefine::grey18,
    &ColorDefine::grey19,
    &ColorDefine::grey2,
    &ColorDefine::grey20,
    &ColorDefine::grey21,
    &ColorDefine::grey22,
    &ColorDefine::grey23,
    &ColorDefine::grey24,
    &ColorDefine::grey25,
    &ColorDefine::grey26,
    &ColorDefine::grey27,
    &ColorDefine::grey28,
    &ColorDefine::grey29,
    &ColorDefine::grey3,
    &ColorDefine::grey30,
    &ColorDefine::grey31,
    &ColorDefine::grey32,
    &ColorDefine::grey33,
    &ColorDefine::grey34,
    &ColorDefine::grey35,
    &ColorDefine::grey36,
    &ColorDefine::grey37,
    &ColorDefine::grey38,
    &ColorDefine::grey39,
    &ColorDefine::grey4,
    &ColorDefine::grey40,
    &ColorDefine::grey41,
    &ColorDefine::grey42,
    &ColorDefine::grey43,
    &ColorDefine::grey44,
    &ColorDefine::grey45,
    &ColorDefine::grey46,
    &ColorDefine::grey47,
    &ColorDefine::grey48,
    &ColorDefine::grey49,
    &ColorDefine::grey5,
    &ColorDefine::grey50,
    &ColorDefine::grey51,
    &ColorDefine::grey52,
    &ColorDefine::grey53,
    &ColorDefine::grey54,
    &ColorDefine::grey55,
    &ColorDefine::grey56,
    &ColorDefine::grey57,
    &ColorDefine::grey58,
    &ColorDefine::grey59,
    &ColorDefine::grey6,
    &ColorDefine::grey60,
    &ColorDefine::grey61,
    &ColorDefine::grey62,
    &ColorDefine::grey63,
    &ColorDefine::grey64,
    &ColorDefine::grey65,
    &ColorDefine::grey66,
    &ColorDefine::grey67,
    &ColorDefine::grey68,
    &ColorDefine::grey69,
    &ColorDefine::grey7,
    &ColorDefine::grey70,
    &ColorDefine::grey71,
    &ColorDefine::grey72,
    &ColorDefine::grey73,
    &ColorDefine::grey74,
    &ColorDefine::grey75,
    &ColorDefine::grey76,
    &ColorDefine::grey77,
    &ColorDefine::grey78,
    &ColorDefine::grey79,
    &ColorDefine::grey8,
    &ColorDefine::grey80,
    &ColorDefine::grey81,
    &ColorDefine::grey82,
    &ColorDefine::grey83,
    &ColorDefine::grey84,
    &ColorDefine::grey85,
    &ColorDefine::grey86,
    &ColorDefine::grey87,
    &ColorDefine::grey88,
    &ColorDefine::grey89,
    &ColorDefine::grey9,
    &ColorDefine::grey90,
    &ColorDefine::grey91,
    &ColorDefine::grey92,
    &ColorDefine::grey93,
    &ColorDefine::grey94,
    &ColorDefine::grey95,
    &ColorDefine::grey96,
    &ColorDefine::grey97,
    &ColorDefine::grey98,
    &ColorDefine::grey99,
    &ColorDefine::honeydew,
    &ColorDefine::honeydew1,
    &ColorDefine::honeydew2,
    &ColorDefine::honeydew3,
    &ColorDefine::honeydew4,
    &ColorDefine::hotPink,
    &ColorDefine::hotPink1,
    &ColorDefine::hotPink2,
    &ColorDefine::hotPink3,
    &ColorDefine::hotPink4,
    &ColorDefine::indianRed,
    &ColorDefine::indianRed1,
    &ColorDefine::indianRed2,
    &ColorDefine::indianRed3,
    &ColorDefine::indianRed4,
    &ColorDefine::ivory,
    &ColorDefine::ivory1,
    &ColorDefine::ivory2,
    &ColorDefine::ivory3,
    &ColorDefine::ivory4,
    &ColorDefine::khaki,
    &ColorDefine::khaki1,
    &ColorDefine::khaki2,
    &ColorDefine::khaki3,
    &ColorDefine::khaki4,
    &ColorDefine::lavender,
    &ColorDefine::lavenderBlush,
    &ColorDefine::lavenderBlush1,
    &ColorDefine::lavenderBlush2,
    &ColorDefine::lavenderBlush3,
    &ColorDefine::lavenderBlush4,
    &ColorDefine::lawnGreen,
    &ColorDefine::lemonChiffon,
    &ColorDefine::lemonChiffon1,
    &ColorDefine::lemonChiffon2,
    &ColorDefine::lemonChiffon3,
    &ColorDefine::lemonChiffon4,
    &ColorDefine::lightBlue,
    &ColorDefine::lightBlue1,
    &ColorDefine::lightBlue2,
    &ColorDefine::lightBlue3,
    &ColorDefine::lightBlue4,
    &ColorDefine::lightCoral,
    &ColorDefine::lightCyan,
    &ColorDefine::lightCyan1,
    &ColorDefine::lightCyan2,
    &ColorDefine::lightCyan3,
    &ColorDefine::lightCyan4,
    &ColorDefine::lightGoldenrod,
    &ColorDefine::lightGoldenrod1,
    &ColorDefine::lightGoldenrod2,
    &ColorDefine::lightGoldenrod3,
    &ColorDefine::lightGoldenrod4,
    &ColorDefine::lightGoldenrodYellow,
    &ColorDefine::lightGray,
    &ColorDefine::lightGreen,
    &ColorDefine::lightGrey,
    &ColorDefine::lightPink,
    &ColorDefine::lightPink1,
    &ColorDefine::lightPink2,
    &ColorDefine::lightPink3,
    &ColorDefine::lightPink4,
    &ColorDefine::lightSalmon,
    &ColorDefine::lightSalmon1,
    &ColorDefine::lightSalmon2,
    &ColorDefine::lightSalmon3,
    &ColorDefine::lightSalmon4,
    &ColorDefine::lightSeaGreen,
    &ColorDefine::lightSkyBlue,
    &ColorDefine::lightSkyBlue1,
    &ColorDefine::lightSkyBlue2,
    &ColorDefine::lightSkyBlue3,
    &ColorDefine::lightSkyBlue4,
    &ColorDefine::lightSlateBlue,
    &ColorDefine::lightSlateGray,
    &ColorDefine::lightSlateGrey,
    &ColorDefine::lightSteelBlue,
    &ColorDefine::lightSteelBlue1,
    &ColorDefine::lightSteelBlue2,
    &ColorDefine::lightSteelBlue3,
    &ColorDefine::lightSteelBlue4,
    &ColorDefine::lightYellow,
    &ColorDefine::lightYellow1,
    &ColorDefine::lightYellow2,
    &ColorDefine::lightYellow3,
    &ColorDefine::lightYellow4,
    &ColorDefine::limeGreen,
    &ColorDefine::linen,
    &ColorDefine::magenta,
    &ColorDefine::magenta1,
    &ColorDefine::magenta2,
    &ColorDefine::magenta3,
    &ColorDefine::magenta4,
    &ColorDefine::maroon,
    &ColorDefine::maroon1,
    &ColorDefine::maroon2,
    &ColorDefine::maroon3,
    &ColorDefine::maroon4,
    &ColorDefine::mediumAquamarine,
    &ColorDefine::mediumBlue,
    &ColorDefine::mediumOrchid,
    &ColorDefine::mediumOrchid1,
    &ColorDefine::mediumOrchid2,
    &ColorDefine::mediumOrchid3,
    &ColorDefine::mediumOrchid4,
    &ColorDefine::mediumPurple,
    &ColorDefine::mediumPurple1,
    &ColorDefine::mediumPurple2,
    &ColorDefine::mediumPurple3,
    &ColorDefine::mediumPurple4,
    &ColorDefine::mediumSeaGreen,
    &ColorDefine::mediumSlateBlue,
    &ColorDefine::mediumSpringGreen,
    &ColorDefine::mediumTurquoise,
    &ColorDefine::mediumVioletRed,
    &ColorDefine::midnightBlue,
    &ColorDefine::mintCream,
    &ColorDefine::mistyRose,
    &ColorDefine::mistyRose1,
    &ColorDefine::mistyRose2,
    &ColorDefine::mistyRose3,
    &ColorDefine::mistyRose4,
    &ColorDefine::moccasin,
    &ColorDefine::navajoWhite,
    &ColorDefine::navajoWhite1,
    &ColorDefine::navajoWhite2,
    &ColorDefine::navajoWhite3,
    &ColorDefine::navajoWhite4,
    &ColorDefine::navy,
    &ColorDefine::navyBlue,
    &ColorDefine::oldLace,
    &ColorDefine::oliveDrab,
    &ColorDefine::oliveDrab1,
    &ColorDefine::oliveDrab2,
    &ColorDefine::oliveDrab3,
    &ColorDefine::oliveDrab4,
    &ColorDefine::orange,
    &ColorDefine::orange1,
    &ColorDefine::orange2,
    &ColorDefine::orange3,
    &ColorDefine::orange4,
    &ColorDefine::orangeRed,
    &ColorDefine::orangeRed1,
    &ColorDefine::orangeRed2,
    &ColorDefine::orangeRed3,
    &ColorDefine::orangeRed4,
    &ColorDefine::orchid,
    &ColorDefine::orchid1,
    &ColorDefine::orchid2,
    &ColorDefine::orchid3,
    &ColorDefine::orchid4,
    &ColorDefine::paleGoldenrod,
    &ColorDefine::paleGreen,
    &ColorDefine::paleGreen1,
    &ColorDefine::paleGreen2,
    &ColorDefine::paleGreen3,
    &ColorDefine::paleGreen4,
    &ColorDefine::paleTurquoise,
    &ColorDefine::paleTurquoise1,
    &ColorDefine::paleTurquoise2,
    &ColorDefine::paleTurquoise3,
    &ColorDefine::paleTurquoise4,
    &ColorDefine::paleVioletRed,
    &ColorDefine::paleVioletRed1,
    &ColorDefine::paleVioletRed2,
    &ColorDefine::paleVioletRed3,
    &ColorDefine::paleVioletRed4,
    &ColorDefine::papayaWhip,
    &ColorDefine::peachPuff,
    &ColorDefine::peachPuff1,
    &ColorDefine::peachPuff2,
    &ColorDefine::peachPuff3,
    &ColorDefine::peachPuff4,
    &ColorDefine::peru,
    &ColorDefine::pink,
    &ColorDefine::pink1,
    &ColorDefine::pink2,
    &ColorDefine::pink3,
    &ColorDefine::pink4,
    &ColorDefine::plum,
    &ColorDefine::plum1,
    &ColorDefine::plum2,
    &ColorDefine::plum3,
    &ColorDefine::plum4,
    &ColorDefine::powderBlue,
    &ColorDefine::purple,
    &ColorDefine::purple1,
    &ColorDefine::purple2,
    &ColorDefine::purple3,
    &ColorDefine::purple4,
    &ColorDefine::red,
    &ColorDefine::red1,
    &ColorDefine::red2,
    &ColorDefine::red3,
    &ColorDefine::red4,
    &ColorDefine::rosyBrown,
    &ColorDefine::rosyBrown1,
    &ColorDefine::rosyBrown2,
    &ColorDefine::rosyBrown3,
    &ColorDefine::rosyBrown4,
    &ColorDefine::royalBlue,
    &ColorDefine::royalBlue1,
    &ColorDefine::royalBlue2,
    &ColorDefine::royalBlue3,
    &ColorDefine::royalBlue4,
    &ColorDefine::saddleBrown,
    &ColorDefine::salmon,
    &ColorDefine::salmon1,
    &ColorDefine::salmon2,
    &ColorDefine::salmon3,
    &ColorDefine::salmon4,
    &ColorDefine::sandyBrown,
    &ColorDefine::seaGreen,
    &ColorDefine::seaGreen1,
    &ColorDefine::seaGreen2,
    &ColorDefine::seaGreen3,
    &ColorDefine::seaGreen4,
    &ColorDefine::seashell,
    &ColorDefine::seashell1,
    &ColorDefine::seashell2,
    &ColorDefine::seashell3,
    &ColorDefine::seashell4,
    &ColorDefine::sienna,
    &ColorDefine::sienna1,
    &ColorDefine::sienna2,
    &ColorDefine::sienna3,
    &ColorDefine::sienna4,
    &ColorDefine::skyBlue,
    &ColorDefine::skyBlue1,
    &ColorDefine::skyBlue2,
    &ColorDefine::skyBlue3,
    &ColorDefine::skyBlue4,
    &ColorDefine::slateBlue,
    &ColorDefine::slateBlue1,
    &ColorDefine::slateBlue2,
    &ColorDefine::slateBlue3,
    &ColorDefine::slateBlue4,
    &ColorDefine::slateGray,
    &ColorDefine::slateGray1,
    &ColorDefine::slateGray2,
    &ColorDefine::slateGray3,
    &ColorDefine::slateGray4,
    &ColorDefine::slateGrey,
    &ColorDefine::snow,
    &ColorDefine::snow1,
    &ColorDefine::snow2,
    &ColorDefine::snow3,
    &ColorDefine::snow4,
    &ColorDefine::springGreen,
    &ColorDefine::springGreen1,
    &ColorDefine::springGreen2,
    &ColorDefine::springGreen3,
    &ColorDefine::springGreen4,
    &ColorDefine::steelBlue,
    &ColorDefine::steelBlue1,
    &ColorDefine::steelBlue2,
    &ColorDefine::steelBlue3,
    &ColorDefine::steelBlue4,
    &ColorDefine::tan1,
    &ColorDefine::tan2,
    &ColorDefine::tan3,
    &ColorDefine::tan4,
    &ColorDefine::thistle,
    &ColorDefine::thistle1,
    &ColorDefine::thistle2,
    &ColorDefine::thistle3,
    &ColorDefine::thistle4,
    &ColorDefine::tomato,
    &ColorDefine::tomato1,
    &ColorDefine::tomato2,
    &ColorDefine::tomato3,
    &ColorDefine::tomato4,
    &ColorDefine::turquoise,
    &ColorDefine::turquoise1,
    &ColorDefine::turquoise2,
    &ColorDefine::turquoise3,
    &ColorDefine::turquoise4,
    &ColorDefine::violet,
    &ColorDefine::violetRed,
    &ColorDefine::violetRed1,
    &ColorDefine::violetRed2,
    &ColorDefine::violetRed3,
    &ColorDefine::violetRed4,
    &ColorDefine::wheat,
    &ColorDefine::wheat1,
    &ColorDefine::wheat2,
    &ColorDefine::wheat3,
    &ColorDefine::wheat4,
    &ColorDefine::white,
    &ColorDefine::whiteSmoke,
    &ColorDefine::yellow,
    &ColorDefine::yellow1,
    &ColorDefine::yellow2,
    &ColorDefine::yellow3,
    &ColorDefine::yellow4,
    &ColorDefine::yellowGreen
};

const int ColorDefine::Size = sizeof(Value)/sizeof(Colorf*);

const char *ColorDefine::DistinctName[]={
  "aliceBlue",
  "antiqueWhite",
  "aquamarine",
  "azure",
  "beige",
  "bisque",
  "black",
  "blanchedAlmond",
  "blue",
  "blueViolet",
  "brown",
  "burlywood",
  "cadetBlue",
  "chartreuse",
  "chocolate",
  "coral",
  "cornflowerBlue",
  "cornsilk",
  "cyan",
  "deepPink",
  "deepSkyBlue",
  "dimGray",
  "dodgerBlue",
  "firebrick",
  "floralWhite",
  "forestGreen",
  "gainsboro",
  "ghostWhite",
  "gold",
  "goldenrod",
  "gray",
  "green",
  "greenYellow",
  "honeydew",
  "hotPink",
  "indianRed",
  "ivory",
  "khaki",
  "lavender",
  "lavenderBlush",
  "lawnGreen",
  "lemonChiffon",
  "limeGreen",
  "linen",
  "magenta",
  "maroon",
  "midnightBlue",
  "mintCream",
  "mistyRose",
  "moccasin",
  "navajoWhite",
  "navy",
  "oldLace",
  "orange",
  "orangeRed",
  "orchid",
  "papayaWhip",
  "peachPuff",
  "peru",
  "pink",
  "plum",
  "powderBlue",
  "purple",
  "red",
  "rosyBrown",
  "royalBlue",
  "saddleBrown",
  "salmon",
  "sandyBrown",
  "seaGreen",
  "seashell",
  "sienna",
  "skyBlue",
  "slateBlue",
  "slateGray",
  "snow",
  "springGreen",
  "steelBlue",
  "thistle",
  "tomato",
  "turquoise",
  "violet",
  "violetRed",
  "wheat",
  "whiteSmoke",
  "yellow",
  "yellowGreen"
};

const Colorf* ColorDefine::DistinctValue[]={
  &ColorDefine::red,
  &ColorDefine::indianRed,
  &ColorDefine::blue,
  &ColorDefine::blueViolet,
  &ColorDefine::brown,
  &ColorDefine::burlywood,
  &ColorDefine::cadetBlue,
  &ColorDefine::chartreuse,
  &ColorDefine::chocolate,
  &ColorDefine::coral,
  &ColorDefine::cornflowerBlue,
  &ColorDefine::cyan,
  &ColorDefine::cornsilk,
  &ColorDefine::aliceBlue,
  &ColorDefine::antiqueWhite,
  &ColorDefine::aquamarine,
  &ColorDefine::azure,
  &ColorDefine::beige,
  &ColorDefine::bisque,
  &ColorDefine::black,
  &ColorDefine::blanchedAlmond,
  &ColorDefine::deepPink,
  &ColorDefine::deepSkyBlue,
  &ColorDefine::dimGray,
  &ColorDefine::dodgerBlue,
  &ColorDefine::firebrick,
  &ColorDefine::floralWhite,
  &ColorDefine::forestGreen,
  &ColorDefine::gainsboro,
  &ColorDefine::ghostWhite,
  &ColorDefine::gold,
  &ColorDefine::goldenrod,
  &ColorDefine::gray,
  &ColorDefine::green,
  &ColorDefine::greenYellow,
  &ColorDefine::honeydew,
  &ColorDefine::hotPink,
  &ColorDefine::ivory,
  &ColorDefine::khaki,
  &ColorDefine::lavender,
  &ColorDefine::lavenderBlush,
  &ColorDefine::lawnGreen,
  &ColorDefine::lemonChiffon,
  &ColorDefine::limeGreen,
  &ColorDefine::linen,
  &ColorDefine::magenta,
  &ColorDefine::maroon,
  &ColorDefine::midnightBlue,
  &ColorDefine::mintCream,
  &ColorDefine::mistyRose,
  &ColorDefine::moccasin,
  &ColorDefine::navajoWhite,
  &ColorDefine::navy,
  &ColorDefine::oldLace,
  &ColorDefine::orange,
  &ColorDefine::orchid,
  &ColorDefine::papayaWhip,
  &ColorDefine::peachPuff,
  &ColorDefine::peru,
  &ColorDefine::pink,
  &ColorDefine::plum,
  &ColorDefine::powderBlue,
  &ColorDefine::purple,
  &ColorDefine::rosyBrown,
  &ColorDefine::royalBlue,
  &ColorDefine::saddleBrown,
  &ColorDefine::salmon,
  &ColorDefine::sandyBrown,
  &ColorDefine::seaGreen,
  &ColorDefine::seashell,
  &ColorDefine::sienna,
  &ColorDefine::skyBlue,
  &ColorDefine::slateBlue,
  &ColorDefine::slateGray,
  &ColorDefine::snow,
  &ColorDefine::springGreen,
  &ColorDefine::steelBlue,
  &ColorDefine::thistle,
  &ColorDefine::tomato,
  &ColorDefine::turquoise,
  &ColorDefine::violet,
  &ColorDefine::violetRed,
  &ColorDefine::wheat,
  &ColorDefine::whiteSmoke,
  &ColorDefine::yellow,
  &ColorDefine::yellowGreen
};

const int ColorDefine::DistinctSize=sizeof(DistinctValue)/sizeof(Colorf*);
}