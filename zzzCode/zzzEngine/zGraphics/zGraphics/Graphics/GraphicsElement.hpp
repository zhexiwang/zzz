#pragma once

namespace zzz{
template <typename T>
class GraphicsElement3
{
public:
  virtual T DistanceTo(const GraphicsElement3<T> &other) const=0;
  typedef enum {ELEMENT_POINT,ELEMENT_LINE,ELEMENT_PLANE,ELEMENT_BOX} ELEMENT_TYPE;
  GraphicsElement3(const ELEMENT_TYPE t):type(t){}
  const ELEMENT_TYPE type;
};

template <typename T>
class GraphicsElement2
{
public:
  virtual T DistanceTo(const GraphicsElement2<T> &other) const=0;
  typedef enum {ELEMENT_POINT,ELEMENT_LINE,ELEMENT_BOX} ELEMENT_TYPE;
  GraphicsElement2(const ELEMENT_TYPE t):type(t){}
  const ELEMENT_TYPE type;
};
}