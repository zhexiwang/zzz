#pragma once
#include <zCore/Math/Range.hpp>

namespace zzz {
template <unsigned int N, typename T >
class AABB : public Range<N, T> {
public:
#undef max
#undef min
  AABB()
    : Range<N, T>(){}
  AABB(const Vector<N,T> &min,const Vector<N,T> &max)
    : Range<N, T>(min, max){}
  explicit AABB(const VectorBase<N,T> &p)
    : Range<N, T>(p){}
  explicit AABB(const vector<Vector<N,T> > &data)
    : Range<N, T>(data){}

  // create from list of vertex
  using Range<N, T>::Reset;
  using Range<N, T>::Min;
  using Range<N, T>::Max;
  using Range<N, T>::Bound;
  using Range<N, T>::Offset;
  using Range<N, T>::Center;
  using Range<N, T>::Diff;
  using Range<N, T>::Volume;
  using Range<N, T>::IsLegal;
  using Range<N, T>::IsEmpty;
  using Range<N, T>::AddData;
  using Range<N, T>::IsInside;
  using Range<N, T>::IsIntersect;
  using Range<N, T>::operator+=;
  using Range<N, T>::operator+;
  using Range<N, T>::operator*=;
  using Range<N, T>::operator*;
  using Range<N, T>::operator==;

  /// Whether it intersect a sphere
  bool sphereIntersect(const VectorBase<N,T>& center, T r) const {
    Vector<N, T> minpos(center - r);
    Vector<N, T> maxpos(center + r);
    //the sphere's AABB do not intersect with it, it absolutely is not
    if(!IsIntersect(AABB<N, T>(minpos, maxpos)))
      return false;
    //TODO: more accurate testing
    return true;
  }

  static AABB<N,T> ZERO() {
    return AABB(Vector<N,T>(T(0)), Vector<N,T>(T(0)));
  }
  static AABB<N,T> COVERALL() {
    return AABB(Vector<N,T>(-numeric_limits<T>::max()), Vector<N,T>(numeric_limits<T>::max()));
  }
};

typedef AABB<2,zint32>     AABB2i32;
typedef AABB<2,zfloat32>   AABB2f32;
typedef AABB<2,zfloat64>   AABB2f64;
typedef AABB<3,zfloat32>   AABB3f32;
typedef AABB<3,zfloat64>   AABB3f64;
typedef AABB<4,zfloat32>   AABB4f32;
typedef AABB<4,zfloat64>   AABB4f64;

typedef AABB<2,int>     AABB2i;
typedef AABB<2,float>   AABB2f;
typedef AABB<2,double>  AABB2d;
typedef AABB<3,float>   AABB3f;
typedef AABB<3,double>  AABB3d;
typedef AABB<4,float>   AABB4f;
typedef AABB<4,double>  AABB4d;
}
