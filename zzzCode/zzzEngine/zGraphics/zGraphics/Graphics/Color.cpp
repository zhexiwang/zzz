#include "Color.hpp"
#include "ColorDefine.hpp"

namespace zzz {
template<>
Colorf FromString<Colorf>(const string& str) {
  string lstr = ToLow_copy(str);
  for (int i = 0; i < ColorDefine::Size; ++i)
    if (lstr == ToLow_copy(ColorDefine::Name[i])) return *ColorDefine::Value[i];
  return Colorf(FromString<Vector3f>(str));
}


template<>
void Color<float>::SetRGB(float r, float g, float b) {
  v[0] = r; v[1] = g; v[2] = b; v[3] = 1.0f;
}

template<>
void Color<double>::SetRGB(double r, double g, double b) {
  v[0] = r; v[1] = g; v[2] = b; v[3] = 1.0;
}
template<>
void Color<zubyte>::SetRGB(zubyte r, zubyte g, zubyte b) {
  v[0] = r; v[1] = g; v[2] = b; v[3] = 255;
}

template<>
void Color<float>::ApplyGL() const {
  glColor4fv(v);
}
template<>
void Color<double>::ApplyGL() const {
  glColor4dv(v);
}
template<>
void Color<zuchar>::ApplyGL() const {
  glColor4ubv(v);
}
template<>
float Color<float>::Brightness() const {
  return 0.212671f*r() + 0.715160f*g() + 0.072169f*b();
}
template<>
double Color<double>::Brightness() const {
  return 0.212671*r() + 0.715160*g() + 0.072169*b();
}
template<>
zubyte Color<zubyte>::Brightness() const {
  return (zubyte)(0.212671*r() + 0.715160*g() + 0.072169*b());
}

};  // namespace zzz
