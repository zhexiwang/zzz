#include "Camera.hpp"
#include <zCore/Utility/CmdParser.hpp>

ZFLAGS_DOUBLE(camera_near, 0.01, "Camera Near z-Plane");
ZFLAGS_DOUBLE(camera_far, 1000, "Camera Far z-Plane");
ZFLAGS_DOUBLE(camera_fovy, 60, "Camera FovY");
ZFLAGS_DOUBLE(camera_ortho_scale, 1.0, "Camera Ortho Scale");
ZFLAGS_BOOL(camera_ortho_mode, false, "Camera Ortho Mode");
namespace zzz{
Camera::Camera()
  : z_near_(ZFLAG_camera_near),
    z_far_(ZFLAG_camera_far),
    fovy_(ZFLAG_camera_fovy),
    ortho_scale_(ZFLAG_camera_ortho_scale),
    ortho_mode_(ZFLAG_camera_ortho_mode) {
  Reset();
}

void Camera::SetPerspective(const int cw, const int ch, const double near, const double far, const double angle) {
  if (near != -1) z_near_ = near;
  if (far != -1) z_far_ = far;
  if (angle != -1) fovy_ = angle;
  if (ch == 0) return;
  width_ = cw;
  height_ = ch;
  aspect_ = (double)cw / (double)ch;
  glMatrixMode(GL_PROJECTION); 
  glLoadIdentity();
  if (!ortho_mode_) {
    gluPerspective(fovy_,aspect_,z_near_,z_far_);
  } else {
    double r = ortho_scale_;
    double t = r / aspect_;
    GLTransformationd trans;
    trans.Identical();
    trans(0, 0) = 1.0 / r;
    trans(1, 1) = 1.0 / t;
    trans(2, 2) = -2.0 / 100000; //(zFar_-zNear_);
    trans(2, 3) = 0; //-(zFar_+zNear_)/(zFar_-zNear_);
    trans.ApplyGL();
  }
  glMatrixMode(GL_MODELVIEW); 
}

void Camera::OffsetPosition(const double &x, const double &y, const double &z) {
  Position_[0] += x;
  Position_[1] += y;
  Position_[2] += z;
}

void Camera::SetPosition(const double &x, const double &y, const double &z) {
  Position_[0] = x;
  Position_[1] = y;
  Position_[2] = z;
}

void Camera::OffsetPosition(const Vector<3,double> &offset) {
  Position_ += offset;
}

void Camera::Update() {
  Quaterniond q;

  // Make the Quaternions that will represent our rotations
  pitch_.SetAxisAngle(Vector<3,double>(1, 0, 0), pitch_degrees_);
  heading_.SetAxisAngle(Vector<3,double>(0, 1, 0), yaw_degrees_);

  // Combine the pitch and heading rotations and store the results in q
  q = pitch_ * heading_;
  transform_ = GLTransformationd(Rotationd(q));

  //original forward is 0,0,-1
  direction_vector_ = q.RotateBackVector(Vector3d(0, 0, -1));
}


void Camera::SetPosition(const Vector<3,double> &pos) {
  Position_ = pos;
}

// positive: forwards, negative: backwards

void Camera::MoveForwards(double dist) {
  // Increment our position by the vector
  Position_ += direction_vector_*dist;
}

// positive: leftwards, negative: rightwards
void Camera::MoveLeftwards(double dist) {
  Quaternion<double> toHead(direction_vector_.Cross(Vector3d(0, 1, 0)), PI / 2);
  Vector3d newhead = toHead.RotateVector(direction_vector_);
  Quaternion<double> toSide(newhead, PI / 2);
  Vector3d LeftDir = toSide.RotateVector(direction_vector_);
  // Increment our position by the vector
  Position_ += LeftDir*dist;
}

void Camera::MoveUpwards(double dist) {
  Quaternion<double> toHead(direction_vector_.Cross(Vector3d(0, 1, 0)), PI / 2);
  Vector3d newhead = toHead.RotateVector(direction_vector_);
  // Increment our position by the vector
  Position_ += newhead * dist;
}

void Camera::ChangeYaw(double degrees) {
  if(Abs(degrees) < Abs(max_yaw_rate_)) {
    // Our Heading is less than the max heading rate that we 
    // defined so lets increment it but first we must check
    // to see if we are inverted so that our heading will not
    // become inverted.
    if(pitch_degrees_ > 90 * C_D2R && pitch_degrees_ < 270 * C_D2R ||
       (pitch_degrees_ < -90 * C_D2R && pitch_degrees_ > -270 * C_D2R)) {
      yaw_degrees_ -= degrees;
    } else {
      yaw_degrees_ += degrees;
    }
  } else {
    // Our heading is greater than the max heading rate that
    // we defined so we can only increment our heading by the 
    // maximum allowed value.
    if(degrees < 0) {
      // Check to see if we are upside down.
      if((pitch_degrees_ > 90 * C_D2R && pitch_degrees_ < 270 * C_D2R) ||
         (pitch_degrees_ < -90 * C_D2R && pitch_degrees_ > -270 * C_D2R)) {
        // Ok we would normally decrement here but since we are upside
        // down then we need to increment our heading
        yaw_degrees_ += max_yaw_rate_;
      } else {
        // We are not upside down so decrement as usual
        yaw_degrees_ -= max_yaw_rate_;
      }
    } else {
      // Check to see if we are upside down.
      if(pitch_degrees_ > 90 * C_D2R && pitch_degrees_ < 270 * C_D2R ||
         (pitch_degrees_ < -90 * C_D2R && pitch_degrees_ > -270 * C_D2R)) {
        // Ok we would normally increment here but since we are upside
        // down then we need to decrement our heading.
        yaw_degrees_ -= max_yaw_rate_;
      } else {
        // We are not upside down so increment as usual.
        yaw_degrees_ += max_yaw_rate_;
      }
    }
  }

  // We don't want our heading to run away from us either. Although it
  // really doesn't matter I prefer to have my heading degrees
  // within the range of -360.0f to 360.0f
  if(yaw_degrees_ > 360 * C_D2R) {
    yaw_degrees_ -= 360 * C_D2R;
  } else if(yaw_degrees_ < -360 * C_D2R) {
    yaw_degrees_ += 360 * C_D2R;
  }

  Update();
}


void Camera::ChangePitch(double degrees) {
  if(Abs(degrees) < Abs(max_pitch_rate_)) {
    // Our pitch is less than the max pitch rate that we 
    // defined so lets increment it.
    pitch_degrees_ += degrees;
  } else {
    // Our pitch is greater than the max pitch rate that
    // we defined so we can only increment our pitch by the 
    // maximum allowed value.
    if(degrees < 0) {
      // We are pitching down so decrement
      pitch_degrees_ -= max_pitch_rate_;
    } else {
      // We are pitching up so increment
      pitch_degrees_ += max_pitch_rate_;
    }
  }

  // We don't want our pitch to run away from us. Although it
  // really doesn't matter I prefer to have my pitch degrees
  // within the range of -360.0f to 360.0f
  if(pitch_degrees_ > 360 * C_D2R) {
    pitch_degrees_ -= 360 * C_D2R;
  } else if(pitch_degrees_ < -360 * C_D2R) {
    pitch_degrees_ += 360 * C_D2R;
  }
  Update();
}


void Camera::ApplyGL(void) const {
  // Let OpenGL set our new perspective on the world!
  transform_.ApplyGL();

  // Translate to our new position.
  glTranslated(-Position_[0],-Position_[1],-Position_[2]);
}

void Camera::Reset() {
  // Initialize all our member variables.
  max_pitch_rate_ = 5;
  max_yaw_rate_ = 5;
  yaw_degrees_ = 0;
  pitch_degrees_ = 0;
  Position_.Set(0, 0, 0);
  Update();
}

zzz::GLTransformationd Camera::GetGLTransformation() {
  GLTransformationd trans(Translationd(-Position_[0], -Position_[1], -Position_[2]));
  trans = trans * transform_;
  return trans;
}

void Camera::LookAt(const Vector3d &from, const Vector3d &to, const Vector3d &up) {
  Vector3d f = to - from;
  f.Normalize();
  Vector3d s = Cross(f, up.Normalized());
  Vector3d u = Cross(s, f);
  Rotationd rot(Matrix3x3d(s, u, -f));
  transform_.Set(rot, Translationd(0, 0, 0));

  // hopefully correct, roll must be zero
  double roll;
  rot.ToEulerAngles(pitch_degrees_, yaw_degrees_, roll);
  pitch_degrees_ *= C_R2D;
  yaw_degrees_ *= C_R2D;
}

void Camera::SetOrthoMode(bool mode) {
  ortho_mode_ = mode;
  SetPerspective(width_, height_);
}

} // namespace zzz