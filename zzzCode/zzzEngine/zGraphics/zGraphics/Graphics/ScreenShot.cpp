#include "ScreenShot.hpp"

namespace zzz {
void zzz::ScreenShot::Shot(boost::function<void(void)>setupcamera, 
                           boost::function<void(void)>drawobj, Image4uc &img) {
  Vector4i viewport=GLViewport::Get();
  tex_.Create(viewport[2], viewport[3]);
  CHECK_GL_ERROR();
  helper_.RenderToTexture(tex_, setupcamera, drawobj);
  CHECK_GL_ERROR();

  tex_.TextureToImage(img);
}

}