#include <zGraphics/Renderer/RendererHelper.hpp>
#include <zGraphics/Resource/Texture/Texture2D.hpp>
#include <zImage/Image/Image.hpp>

// Screen shot helper function, use render-to-texture to capture the screen.
namespace zzz {
class ScreenShot {
public:
  void Shot(boost::function<void(void)>setupcamera, 
            boost::function<void(void)>drawobj, Image4uc &img);
private:
  Texture2D tex_;
  RendererHelper helper_;
};
}