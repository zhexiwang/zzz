#pragma once
#include <zCore/Math/Matrix3x3.hpp>
#include "Quaternion.hpp"
//#include "Transformation.hpp"

namespace zzz {
template<typename T> class GLTransformation;
template <typename T>
class Rotation : public Matrix<3,3,T> {
public:
  Rotation():Matrix<3,3,T>(1,0,0,0,1,0,0,0,1){}
  Rotation(const Rotation<T> &other):Matrix<3,3,T>(other){}
  explicit Rotation(const MatrixBase<3,3,T> &other):Matrix<3,3,T>(other){}
  explicit Rotation(const Quaternion<T> &q) {
    T* v=Data();
    v[0] = 1.0 - 2.0 * (q[1] * q[1] + q[2] * q[2]);
    v[1] = 2.0 * (q[0] * q[1] - q[2] * q[3]);
    v[2] = 2.0 * (q[0] * q[2] + q[1] * q[3]);          

    v[3] = 2.0 * (q[0] * q[1] + q[2] * q[3]);
    v[4]= 1.0 - 2.0 * (q[0] * q[0] + q[2] * q[2]);
    v[5] = 2.0 * (q[1] * q[2] - q[0] * q[3]);

    v[6] = 2.0 * (q[0] * q[2] - q[1] * q[3]);
    v[7] = 2.0 * (q[1] * q[2] + q[0] * q[3]);
    v[8] = 1.0 - 2.0 * (q[0] * q[0] + q[1] * q[1]);
  }
  Rotation(const Vector<3,T> &normal, const T angle) {
    if (normal.Len()==0) Diagonal(1);
    else *this=Rotation<T>(Quaternion<T>(normal,angle));
  }

  // Rx =   1   0         0
  //        0   cos(ax)   -sin(ax)
  //        0   sin(ax)   cos(ax)

  // Ry =   cos(ay)   0   sin(ay)
  //        0         1   0
  //        -sin(ay)  0   cos(ay)

  // Rz =	  cos(az)   -sin(az)  0
  //        sin(az)   cos(az)   0
  //        0         0         1

//  mat = Rz*Ry*Rx =	cos(az)*cos(ay)   (-sin(az))*cos(ax)+cos(az)*sin(ay)*sin(ax)  sin(az)*sin(ax)+cos(az)*sin(ay)*cos(ax)
//                    sin(az)*cos(ay)   cos(az)*cos(ax)+sin(az)*sin(ay)*sin(ax)     cos(az)*(-sin(ax))+sin(az)*sin(ay)*sin(ax)
//                    -sin(ay)          cos(ay)*sin(ax)                             cos(ay)*cos(ax)
  Rotation(T ax, T ay, T az) {
    T cosx = cos(ax), sinx = sin(ax);
    T cosy = cos(ay), siny = sin(ay);
    T cosz = cos(az), sinz = sin(az);
    Matrix<3, 3, T> rx(1, 0, 0, 0, cosx, -sinx, 0, sinx, cosx);
    Matrix<3, 3, T> ry(cosy, 0, siny, 0, 1, 0, -siny, 0, cosy);
    Matrix<3, 3, T> rz(cosz, -sinz, 0, sinz, cosz, 0, 0, 0, 1);
    *this = rx * ry * rz;
  }
  
  void ToEulerAngles(T &pitch, T &yaw, T &roll) {
    T *v=Data();
    if (v[6] == -1) {
      roll = 0;
      yaw = C_PI_2;
      pitch = atan2(v[1], v[2]);
    } else if (v[6] == 1) {
      roll = 0;
      yaw = -C_PI_2;
      pitch = atan2(-v[1], -v[2]);
    } else {
      yaw = -asin(v[6]);
      pitch = atan2(v[7]/cos(yaw), v[8]/cos(yaw));
      roll = atan2(v[9]/cos(yaw), v[0]/cos(yaw));
    }
  }

  inline static Rotation<T> GetIdentical() {
    static Rotation<T> rot(MatrixBase<3, 3, T>::GetIdentical());
    return rot;
  }

  using Matrix<3,3,T>::operator=;
  using Matrix<3,3,T>::Data;
  using Matrix<3,3,T>::Diagonal;
  
  VectorBase<3,T> Apply(const VectorBase<3,T> &a) const {
    return (*this)*a;
  }

  //OpenGL
  void ApplyGL() const {
    GLTransformation<T>(*this).ApplyGL();
  }
};

typedef Rotation<zfloat32> Rotationf32;
typedef Rotation<zfloat64> Rotationf64;

typedef Rotation<float> Rotationf;
typedef Rotation<double> Rotationd;

}
