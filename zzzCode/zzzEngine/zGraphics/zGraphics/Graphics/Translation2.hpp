#pragma once
#include <zCore/Math/Vector2.hpp>

namespace zzz{
template <typename T>
class Translation2 : public Vector<2,T> {
  using Vector<2,T>::v;
public:
  Translation2():Vector<2,T>(0){}
  Translation2(const Translation2<T> &other):Vector<2,T>(other.v){}
  Translation2(const T x, const T y):Vector<2,T>(x,y){}
  explicit Translation2(const VectorBase<2,T> &other):Vector<2,T>(other){}

  using Vector<2,T>::operator=;

  VectorBase<2,T> Apply(const VectorBase<2,T>& other) const {
    return other+(*this);
  }
};

typedef Translation2<zfloat32> Translation2f32;
typedef Translation2<zfloat64> Translation2f64;

typedef Translation2<float> Translation2f;
typedef Translation2<double> Translation2d;
}
