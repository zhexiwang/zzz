#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zCore/Utility/AnyHolder.hpp>

// This is basically AnyHolder.
// Only that it will delete the object when needed.
// So remove is overloaded.

namespace zzz{
class Texture2D;
class Shader;
class Material;

class ZGRAPHICS_CLASS ResourceManager : public AnyHolder
{
public:
  bool Add(Texture2D *tex, const string &name, bool cover=true);
  bool Add(Shader *shader, const string &name, bool cover=true);
  bool Add(Material *mat, const string &name, bool cover=true);
private:
  void Destroy(Any &v);
};

extern ResourceManager __default_RM; //used to the program that does not have OPENGL

//for convinience
#define ZRM ((Context::current_context_==NULL)?&__default_RM:Context::current_context_->GetRM())

}