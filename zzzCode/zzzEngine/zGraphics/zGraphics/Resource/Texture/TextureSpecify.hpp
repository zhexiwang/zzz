#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "Texture2D.hpp"
#include "TextureCube.hpp"

namespace zzz{

#define TEXTURE2D_SPECIFY(classname,iformat) \
class classname : public Texture2D\
{\
public:\
  classname():Texture2D(iformat){;}\
  classname(const string & filename):Texture2D(filename,iformat){;}\
  classname(int width, int height):Texture2D(width,height,iformat){;}\
};

TEXTURE2D_SPECIFY(Texture2D4f,GL_RGBA16F_ARB)
TEXTURE2D_SPECIFY(Texture2D3f,GL_RGB16F_ARB)
TEXTURE2D_SPECIFY(Texture2D4ub,GL_RGBA)
TEXTURE2D_SPECIFY(Texture2D3ub,GL_RGB)

#define TEXTURECUBE_SPECIFY(classname,iformat) \
class classname : public TextureCube\
{\
public:\
  classname():TextureCube(iformat){;}\
  classname(const string & filename):TextureCube(filename,iformat){;}\
  classname(int width, int height):TextureCube(width,height,iformat){;}\
  classname(const string & f1,const string & f2,const string & f3,const string & f4,const string & f5,const string & f6):TextureCube(f1,f2,f3,f4,f5,f6,iformat){;}\
};

TEXTURECUBE_SPECIFY(TextureCube4f,GL_RGBA16F_ARB)
TEXTURECUBE_SPECIFY(TextureCube3f,GL_RGB16F_ARB)
TEXTURECUBE_SPECIFY(TextureCube4ub,GL_RGBA)
TEXTURECUBE_SPECIFY(TextureCube3ub,GL_RGB)
}