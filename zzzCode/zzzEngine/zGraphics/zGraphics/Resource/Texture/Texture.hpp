#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zCore/common.hpp>
#include "../../Graphics/Graphics.hpp"
#include "../../Graphics/OpenGLTools.hpp"
#include <zCore/Utility/Uncopyable.hpp>

namespace zzz{
class ZGRAPHICS_CLASS Texture : public Uncopyable {
public:
  Texture(GLenum tex_target, GLenum internalFormat);
  virtual ~Texture();
  static bool Disable(GLenum level);
  static void DisableAll();
  virtual bool Bind(GLenum bindto=GL_TEXTURE0)=0;
  virtual void ChangeInternalFormat(zint32 newiformat)=0;
  virtual void ChangeSize(zuint32 width,zuint32 height)=0;
  virtual void ChangeParameter(GLenum filter,GLenum wrap)=0;
  bool IsValid() const;
  GLuint GetID();
  static int GetMaxSize();
  zint32 GetInternalFormat() const  {return internal_format_;}
  zint32 GetFilterParam() const {return filter_para_;}
  zint32 GetWrapParam() const {return wrap_para_;}

  GLenum tex_target_;
protected:
  zint32 internal_format_;
  zint32 filter_para_,wrap_para_;

protected:
  GLuint tex_;
};
}
