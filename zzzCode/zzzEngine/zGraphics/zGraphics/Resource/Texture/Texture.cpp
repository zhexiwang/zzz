#include "Texture.hpp"

namespace zzz {

bool zzz::Texture::Disable(GLenum level) {
  int lastLevel;
  CHECK_GL_ERROR()
  glGetIntegerv(GL_ACTIVE_TEXTURE,&lastLevel);
  CHECK_GL_ERROR()
  glActiveTexture(level);
  CHECK_GL_ERROR()
  glDisable(GL_TEXTURE_1D);
  CHECK_GL_ERROR()
  glDisable(GL_TEXTURE_2D);
  CHECK_GL_ERROR()
  glDisable(GL_TEXTURE_3D);
  CHECK_GL_ERROR()
  glDisable(GL_TEXTURE_CUBE_MAP);
  CHECK_GL_ERROR()

  glActiveTexture(lastLevel);

  CHECK_GL_ERROR()
  return true;
}

void Texture::DisableAll() {
  Disable(GL_TEXTURE0);
  Disable(GL_TEXTURE1);
  Disable(GL_TEXTURE2);
  Disable(GL_TEXTURE3);
  Disable(GL_TEXTURE4);
  Disable(GL_TEXTURE5);
  Disable(GL_TEXTURE6);
  Disable(GL_TEXTURE7);
//  Disable(GL_TEXTURE8);
//  Disable(GL_TEXTURE9);
//  Disable(GL_TEXTURE10);
//  Disable(GL_TEXTURE11);
//  Disable(GL_TEXTURE12);
//  Disable(GL_TEXTURE13);
//  Disable(GL_TEXTURE14);
//  Disable(GL_TEXTURE15);
//   Disable(GL_TEXTURE16);
//   Disable(GL_TEXTURE17);
//   Disable(GL_TEXTURE18);
//   Disable(GL_TEXTURE19);
//   Disable(GL_TEXTURE20);
//   Disable(GL_TEXTURE21);
//   Disable(GL_TEXTURE22);
//   Disable(GL_TEXTURE23);
//   Disable(GL_TEXTURE24);
//   Disable(GL_TEXTURE25);
//   Disable(GL_TEXTURE26);
//   Disable(GL_TEXTURE27);
//   Disable(GL_TEXTURE28);
//   Disable(GL_TEXTURE29);
//   Disable(GL_TEXTURE30);
//   Disable(GL_TEXTURE31);
  CHECK_GL_ERROR()
}

Texture::Texture(GLenum tex_target, GLenum internalFormat)
  : tex_(0),
    tex_target_(tex_target),
    internal_format_(internalFormat),
    filter_para_(GL_LINEAR),
    wrap_para_(GL_CLAMP_TO_EDGE) {
}

Texture::~Texture() {
  if (IsValid())
    glDeleteTextures(1,&tex_);
  CHECK_GL_ERROR()
}


bool Texture::IsValid() const {
  bool valid=(glIsTexture(tex_)==GL_TRUE);
  CHECK_GL_ERROR()
  return valid;
}

GLuint Texture::GetID() {
  if (!IsValid()) {
    glGenTextures(1,&tex_);
    CHECK_GL_ERROR()
    ZCHECK_NOT_ZERO(tex_);
  }
  return tex_;
}

int Texture::GetMaxSize() {
  GLint max_size = 0;
  glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_size);
  return max_size;
}

} // namespace zzz
