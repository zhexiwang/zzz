#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "Texture.hpp"

namespace zzz {
class ZGRAPHICS_CLASS Texture1D : public Texture {
public:
  Texture1D(int newiformat=GL_RGB);
  Texture1D(zuint width, int newiformat);
  void Create(zuint width);
  bool Bind(GLenum bindto=GL_TEXTURE0);
  template<typename T>
  void DataToTexture(const T *v, zsize size);
  template<typename T>
  void TextureToData(T *v);

  void ChangeInternalFormat(int newiformat);
  void ChangeSize(zuint width,zuint useless);
  void ChangeSize(zuint width);
  void ChangeParameter(GLenum filter,GLenum wrap);

  zuint width_;
};


}
