#include "ResourceManager.hpp"
#include "Texture/Texture2D.hpp"
#include "Shader/Shader.hpp"
#include "Mesh/Material.hpp"
#include <zCore/Utility/StringTools.hpp>
#include <zCore/Utility/FileTools.hpp>

namespace zzz{
ResourceManager __default_RM;
bool ResourceManager::Add(Texture2D *tex, const string &name, bool cover)
{
  return AnyHolder::Add(tex,name,cover);
}

bool ResourceManager::Add(Shader *shader, const string &name, bool cover)
{
  return AnyHolder::Add(shader,name,cover);
}

bool ResourceManager::Add(Material *mat, const string &name, bool cover)
{
  return AnyHolder::Add(mat,name,cover);
}

void ResourceManager::Destroy(Any &v)
{
  if (v.IsType<Texture2D*>())
    delete any_cast<Texture2D*>(v);
  else if (v.IsType<Shader*>())
    delete any_cast<Shader*>(v);
  else if (v.IsType<Material*>())
    delete any_cast<Material*>(v);
  else
    ZLOGF << "Unknown type stored! This should not happen!\n";
}

}