#include <zCore/Math/Array2.hpp>
#include "Material.hpp"
#include "../../Graphics/Graphics.hpp"
//#include <zGraphics/Resource/Texture/Texture2DIOObject.hpp>
#include <zCore/Utility/Tools.hpp>
#include <zCore/Utility/CmdParser.hpp>

ZFLAGS_STRING(default_mat_dif, "1 1 1 1", "Default material diffuse.");
ZFLAGS_STRING(default_mat_amb, "0 0 0 1", "Default material ambient.");
ZFLAGS_STRING(default_mat_spe, "0 0 0 1", "Default material specular.");
ZFLAGS_STRING(default_mat_emi, "0 0 0 1", "Default material emission.");
ZFLAGS_DOUBLE(default_mat_shi, 8, "Default material shininess.");
namespace zzz{

Material::Material()
  : diffuse_(FromString<Vector4f>(ZFLAG_default_mat_dif)),
    ambient_(FromString<Vector4f>(ZFLAG_default_mat_amb)),
    specular_(FromString<Vector4f>(ZFLAG_default_mat_spe)),
    emission_(FromString<Vector4f>(ZFLAG_default_mat_emi)),
    shininess_(ZFLAG_default_mat_shi),
    HasFlags(MAT_AMB | MAT_DIF | MAT_SPE) {
}

Material::~Material() {
}

void Material::ApplyMaterial() {
  if (HasFlag(MAT_AMB))
    glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,ambient_.Data());
  if (HasFlag(MAT_DIF))
    glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,diffuse_.Data());
  if (HasFlag(MAT_SPE)) {
    glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,specular_.Data());
    glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,shininess_);
  }
  if (HasFlag(MAT_EMI))
    glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,emission_.Data());
}

void Material::BindDiffuseTexture(GLenum bindto /*= GL_TEXTURE0*/) {
  if (HasFlag(MAT_DIFTEX))
    diffuseTex_.Bind(bindto);
}

void Material::UnbindDiffuseTexture() {
  if (HasFlag(MAT_DIFTEX))
    diffuseTex_.Unbind();
}

void Material::BindAmbientTexture(GLenum bindto /*= GL_TEXTURE0*/) {
  if (HasFlag(MAT_AMBTEX))
    ambientTex_.Bind(bindto);
}

void Material::UnbindAmbientTexture() {
  if (HasFlag(MAT_AMBTEX))
    ambientTex_.Unbind();
}

}

