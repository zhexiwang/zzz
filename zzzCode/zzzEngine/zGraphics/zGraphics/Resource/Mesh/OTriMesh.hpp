#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#pragma warning(disable:4267)
#pragma warning(disable:4244)
#include "../../Graphics/Graphics.hpp"
#include "Mesh.hpp"
#include "../../Math/Coordinate.hpp"
#define _USE_MATH_DEFINES
#include <OpenMesh/Core/io/MeshIO.hh>
#include <OpenMesh/Core/Mesh/Types/TriMesh_ArrayKernelT.hh>
#ifdef _DEBUG
#pragma comment(lib,"OpenMeshCored.lib")
#pragma comment(lib,"OpenMeshToolsd.lib")
#else
#pragma comment(lib,"OpenMeshCore.lib")
#pragma comment(lib,"OpenMeshTools.lib")
#endif

namespace zzz{
class ZGRAPHICS_CLASS OTriMesh : public Mesh
{
public:
  OTriMesh();
  bool Clear();
  bool LoadFromFile(const string &filename);
  bool Draw(int bt=Mesh::MESH_VERTEX|Mesh::MESH_NORMAL);
  bool DrawVBO(int bt);
  bool CreateVBO(int bt);
  bool Apply(Rotation<float> &r);
  bool Apply(Translation<float> &t);
  bool Apply(Transformation<float> &t);
  bool BindVBO(int bt=MESH_ALL);

private:
  typedef OpenMesh::TriMesh_ArrayKernelT<> MyMesh;
  MyMesh mesh_;

  //meterial
  GLfloat mat_emi_[4],mat_amb_[4],mat_dif_[4],mat_spe_[4],mat_shi_;

};
}
