#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "Mesh.hpp"

namespace zzz{
class ZGRAPHICS_CLASS PolygonMesh
{
public:
  struct PolygonMeshGroup
  {
    string name_;
    string matname_;

    //original data
    vector<vector<int> > facep_,facen_,facet_;
  };
  PolygonMesh();
  ~PolygonMesh();
  bool Clear();

  bool Apply(const Translation<float>&);
  bool Apply(const Rotation<float>&);
  bool Apply(const Transformation<float>&);
  bool Scale(float coef);
  bool MakeCenter();
  bool Normalize();

  //simple draw and vbo draw
  bool Draw(int bt=MESH_POS|MESH_NOR);
  bool Draw(int idx, int bt);
  bool Draw(const string &name, int bt);

  //original data
  vector<Vector3f> pos_,nor_,tex_,color_;
  vector<PolygonMeshGroup*> groups_;
  //per vertex generalized data
  vector<Vector3f> posnor_,postan_,posbino_;
  vector<float> posratio_;
  float allarea_;

  float MeshScale_;
  Vector3f MeshOffset_;
  float Diameter_;
  AABB<3,float> AABB_;
  Vector3f OriCenter_;

  bool loaded_;
};
}
