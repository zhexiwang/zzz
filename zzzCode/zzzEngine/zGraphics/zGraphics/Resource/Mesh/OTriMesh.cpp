#ifdef ZZZ_OPENMESH
#include "OTriMesh.hpp"
#include "../../Utility/Tools.hpp"

namespace zzz{
OTriMesh::OTriMesh()
{
  mat_emi_[0]=0; mat_emi_[1]=0; mat_emi_[2]=0; mat_emi_[3]=1;
  mat_amb_[0]=1; mat_amb_[1]=1; mat_amb_[2]=1; mat_amb_[3]=1;
  mat_dif_[0]=1; mat_dif_[1]=1; mat_dif_[2]=1; mat_dif_[3]=1;
  mat_spe_[0]=1; mat_spe_[1]=1; mat_spe_[2]=1; mat_spe_[3]=1;
  mat_shi_=1;

}
bool OTriMesh::LoadFromFile(const string &filename)
{
  mesh_.request_face_normals();
  mesh_.request_vertex_normals();
  mesh_.request_vertex_texcoords3D();
  OpenMesh::IO::Options opt;
  if (! OpenMesh::IO::read_mesh(mesh_,filename,opt))
    return false;

  if (! opt.check(OpenMesh::IO::Options::FaceNormal))
    mesh_.update_face_normals();
  if (! opt.check(OpenMesh::IO::Options::VertexNormal))
    mesh_.update_vertex_normals();

  return true;  
}

bool OTriMesh::Clear()
{
  mesh_.clear();
  return true;
}

bool OTriMesh::Draw(int bt)
{
  glMaterialfv(GL_FRONT,GL_EMISSION,mat_emi_);
  glMaterialfv(GL_FRONT,GL_AMBIENT,mat_amb_);
  glMaterialfv(GL_FRONT,GL_DIFFUSE,mat_dif_);
  glMaterialfv(GL_FRONT,GL_SPECULAR,mat_spe_);
  glMaterialfv(GL_FRONT,GL_SHININESS,&mat_shi_);

  MyMesh::FaceIter v_it,v_end(mesh_.faces_end());
  glBegin(GL_TRIANGLES);
  for (v_it=mesh_.faces_begin();v_it!=v_end; ++v_it)
  {
    for (MyMesh::FaceVertexIter fv_it=mesh_.fv_iter(v_it);fv_it; ++fv_it)
    {
      if (CheckFlag(bt,Mesh::MESH_VERTEX))
        glVertex3fv(mesh_.point(fv_it).data());

      if (CheckFlag(bt,Mesh::MESH_NORMAL))
        glNormal3fv(mesh_.normal(fv_it).data());
    }
  }
  glEnd();
  return true;
}

bool OTriMesh::DrawVBO(int bt)
{
  return true;
}

bool OTriMesh::CreateVBO(int bt)
{
  return true;
}

bool OTriMesh::Rotate(float angle, float x, float y, float z)
{
  return true;
}

bool OTriMesh::Translate(float x, float y, float z)
{
  return true;
}

bool OTriMesh::MultiMatrix(float *rotatematrix)
{
  return true;
}

bool OTriMesh::BindVBO(int bt/*=MESH_ALL*/)
{
  return true;
}
}
#endif