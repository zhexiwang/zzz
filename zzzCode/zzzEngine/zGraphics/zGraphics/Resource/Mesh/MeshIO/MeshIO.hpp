#pragma once
#include "../Mesh.hpp"
#include <zCore/Utility/FileTools.hpp>
#include "ObjIO.hpp"
#include "DaeIO.hpp"
#include "WrlIO.hpp"
#include "Ply2IO.hpp"
#include "XIO.hpp"
#include <zCore/Utility/Timer.hpp>
#include "IOObjectMesh.hpp"
#include <zCore/Utility/IOFile.hpp>
#include <zCore/Utility/ZCheck.hpp>

namespace zzz{
class MeshIO {
public:
  static bool Load(const string &filename, TriMesh &mesh, bool load_mat=true) {
    ZLOG(ZVERBOSE)<<"Loading "<<filename<<" ...\n";
    Timer timer;
    bool result=false;
    string ext=ToLow_copy(GetExt(filename));
    
    if (ext==".obj") result=ObjIO::Load(filename,mesh,load_mat);
    else if (ext==".dae") result=DaeIO::Load(filename,mesh,load_mat);
    else if (ext==".wrl") result=WrlIO::Load(filename,mesh,load_mat);
    else if (ext==".vrml") result=WrlIO::Load(filename,mesh,load_mat);
    else if (ext==".ply2") result=Ply2IO::Load(filename,mesh,load_mat);
    else if (ext==".x") result=XIO::Load(filename,mesh,load_mat);
    else if (ext==".rfmesh") result=IOFile::LoadFileR(filename, mesh);

    mesh.AABB_.Reset();
    mesh.AABB_ += mesh.pos_;

    mesh.filename_ = filename;

    if (result) 
      ZLOG(ZVERBOSE)<<"Loaded "<<filename<<" in "<<timer.Elapsed()<<" seconds.\n";
    else 
      ZLOG(ZVERBOSE)<<"Loaded "<<filename<<" FAILED!\n";
    return result;
  }

  static bool Save(const string &filename, TriMesh &mesh, bool save_mat=true) {
    ZLOG(ZVERBOSE)<<"Saving "<<filename<<" ...\n";
    Timer timer;
    bool result=false;
    string ext=ToLow_copy(GetExt(filename));

    if (ext==".obj") result=ObjIO::Save(filename,mesh,save_mat);
    else if (ext==".dae") result=DaeIO::Save(filename,mesh,save_mat);
    else if (ext==".wrl") result=WrlIO::Save(filename,mesh,save_mat);
    else if (ext==".vrml") result=WrlIO::Save(filename,mesh,save_mat);
    else if (ext==".ply2") result=Ply2IO::Save(filename,mesh,save_mat);
    else if (ext==".x") result=XIO::Save(filename,mesh,save_mat);
    else if (ext==".rfmesh") result=IOFile::SaveFileR(filename, mesh);
    
    if (result) 
      ZLOG(ZVERBOSE)<<"Saved "<<filename<<" in "<<timer.Elapsed()<<" seconds.\n";
    else 
      ZLOG(ZVERBOSE)<<"Saved "<<filename<<" FAILED!\n";
    return result;
  }

  static bool Save(TriMesh &mesh, bool save_mat=true) {
    ZCHECK_FALSE(mesh.filename_.empty());
    return Save(mesh.filename_, mesh, save_mat);
  }
};
}
