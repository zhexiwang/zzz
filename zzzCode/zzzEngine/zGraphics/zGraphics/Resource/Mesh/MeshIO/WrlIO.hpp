#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "../SimpleMesh.hpp"
#include "../Mesh.hpp"


namespace zzz{
class ZGRAPHICS_CLASS WrlIO {
public:
  static bool Load(const string &filename, TriMesh &mesh,bool load_mat=true);
  static bool Save(const string &filename, TriMesh &mesh,bool save_mat=true);
  static bool Load(const string &filename, SimpleMesh &mesh,bool load_mat=true);
  static bool Save(const string &filename, SimpleMesh &mesh,bool save_mat=true);
};

}