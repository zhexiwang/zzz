#include "Ply2IO.hpp"
#include <zCore/common.hpp>

namespace zzz{
bool Ply2IO::Load(const string &filename, TriMesh &mesh, bool load_mat) {
  ifstream fi(filename.c_str());
  if (!fi.good()) return false;
  int posn,facen;
  fi >> posn >> facen;
  if (fi.fail()) return false;
  mesh.Clear();
  Vector3f p;
  for (int i = 0; i < posn; ++i) {
    fi >> p;
    if (fi.fail()) return false;
    mesh.pos_.push_back(p);
  }
  mesh.SetFlag(MESH_POS);

  mesh.groups_.push_back(new TriMesh::MeshGroup);
  TriMesh::MeshGroup *g=mesh.groups_.back();
  int x;
  Vector3i f;
  for (int i = 0; i < facen; ++i) {
    fi >> x >> f;
    if (x!=3 || fi.fail()) return false;
    g->facep_.push_back(f);
  }
  g->SetFlag(MESH_POS);
  fi.close();
  return true;
}

bool Ply2IO::Save(const string &filename, TriMesh &mesh, bool save_mat) {
  ofstream fo(filename.c_str());
  if (!fo.good()) return false;
  fo << mesh.pos_.size() << endl;
  int facen = 0;
  for (zuint i = 0; i < mesh.groups_.size(); ++i) facen += mesh.groups_[i]->facep_.size();
  fo << facen << endl;

  for (zuint i = 0; i < mesh.pos_.size(); ++i) {
    fo << mesh.pos_[i] << endl;
  }

  for (zuint g = 0; g < mesh.groups_.size(); ++g) {
    for (zuint i = 0; i < mesh.groups_[g]->facep_.size(); ++i)
      fo << "3 " << mesh.groups_[g]->facep_[i] << endl;
  }
  fo.close();
  return true;
}

}
