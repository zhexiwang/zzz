#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "../Mesh.hpp"
#include <zCore/Xml//RapidXMLNode.hpp>

namespace zzz{
class ZGRAPHICS_CLASS DaeIO {
public:
  static bool Load(const string &filename, TriMesh &mesh, bool load_mat=true);
  static bool Save(const string &filename, TriMesh &mesh, bool save_mat=true);

private:
  static bool load_mat_;
  static bool save_mat_;
  static string dae_path_;

  struct DaeData
  {
    DaeData(RapidXMLNode _libImagesNode, RapidXMLNode _libMaterialsNode, RapidXMLNode _libEffectsNode, RapidXMLNode _libGeometriesNode, RapidXMLNode _libVisualsNode)
      :libImagesNode(_libImagesNode),
      libMaterialsNode(_libMaterialsNode),
      libEffectsNode(_libEffectsNode),
      libGeometriesNode(_libGeometriesNode),
      libVisualsNode(_libVisualsNode){}
    RapidXMLNode libImagesNode;
    RapidXMLNode libMaterialsNode;
    RapidXMLNode libEffectsNode;
    RapidXMLNode libGeometriesNode;
    RapidXMLNode libVisualsNode;
  };

  static string ToId(const string &str);
  static string ToSource(const string &str);
  static RapidXMLNode FindNodeById(RapidXMLNode &father, const string &name, string &id);
  static const int SEM_VERTEX=0;
  static const int SEM_TEXCOORD=1;
  static const int SEM_NORMAL=2;
  static const int SEM_COLOR=3;
  static void ReadMaterial(TriMesh &mesh, DaeData &dae, string &url);
  static void ReadGeometry(TriMesh &mesh, DaeData &dae, string &url);
  static void ReadVisual(TriMesh &mesh, DaeData &dae, string &url);

  static void WriteMaterial(TriMesh &mesh, DaeData &dae, string &url);
  static void WriteGeometry(TriMesh &mesh, DaeData &dae, string &url);
  static void WriteVisual(TriMesh &mesh, DaeData &dae, string &url);
};
}
