#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "../SimpleMesh.hpp"
#include "../Mesh.hpp"

namespace zzz{
class ZGRAPHICS_CLASS ObjIO {
public:
  static bool Load(const string &filename, TriMesh &mesh, bool load_mat=true);
  static bool Save(const string &filename, TriMesh &mesh, bool save_mat=true);
  static bool Load(const string &filename, SimpleMesh &mesh, bool load_mat=true);
  static bool Save(const string &filename, SimpleMesh &mesh, bool save_mat=true);

  static bool ReadMtl(const string &filename, const string &prefix);
  static bool WriteMtl(const string &filename, const vector<string> &texnames);
};

}
