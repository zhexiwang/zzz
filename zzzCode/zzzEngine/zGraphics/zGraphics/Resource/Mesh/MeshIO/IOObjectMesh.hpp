#pragma once
#include <zCore/Utility/IOObject.hpp>
#include "../Mesh.hpp"
#include "../Material.hpp"
namespace zzz {
template<zuint PN>
class IOObject<Mesh<PN> >
{
public:
  static const zint32 RF_POS = 1;
  static const zint32 RF_NOR = 2;
  static const zint32 RF_TEX = 3;
  static const zint32 RF_COLOR = 4;
  static const zint32 RF_GROUP = 5;
  static const zint32 RF_GROUP_FACEP = 1;
  static const zint32 RF_GROUP_FACEN = 2;
  static const zint32 RF_GROUP_FACET = 3;
  static const zint32 RF_GROUP_FACEC = 4;
  static const zint32 RF_GROUP_NAME = 5;
  static const zint32 RF_GROUP_MATNAME = 6;
  static const zint32 RF_GROUP_MATERIAL = 7;

  static void WriteFileR(RecordFile &rf, const zint32 label, const Mesh<PN> &src) {
    rf.WriteChildBegin(label);
      IOObj::WriteFileR(rf, RF_POS, src.pos_);
      IOObj::WriteFileR(rf, RF_NOR, src.nor_);
      IOObj::WriteFileR(rf, RF_TEX, src.tex_);
      IOObj::WriteFileR(rf, RF_COLOR, src.color_);
      rf.WriteRepeatBegin(RF_GROUP);
        for (zuint i=0; i<src.groups_.size(); i++) {
          rf.WriteRepeatChild();
          IOObj::WriteFileR(rf, RF_GROUP_FACEP, src.groups_[i]->facep_);
          IOObj::WriteFileR(rf, RF_GROUP_FACEN, src.groups_[i]->facen_);
          IOObj::WriteFileR(rf, RF_GROUP_FACET, src.groups_[i]->facet_);
          IOObj::WriteFileR(rf, RF_GROUP_FACEC, src.groups_[i]->facec_);
          IOObj::WriteFileR(rf, RF_GROUP_NAME, src.groups_[i]->name_);
          IOObj::WriteFileR(rf, RF_GROUP_MATNAME, src.groups_[i]->matname_);
          if (!src.groups_[i]->matname_.empty()) {
            IOObj::WriteFileR(rf, RF_GROUP_MATERIAL, *(ZRM->Get<Material*>(src.groups_[i]->matname_)));
          }
        }
      rf.WriteRepeatEnd();
    rf.WriteChildEnd();
  }
  static void ReadFileR(RecordFile &rf, const zint32 label, Mesh<PN> &dst) {
    dst.Clear();
    rf.ReadChildBegin(label);
      IOObj::ReadFileR(rf, RF_POS, dst.pos_);
      IOObj::ReadFileR(rf, RF_NOR, dst.nor_);
      IOObj::ReadFileR(rf, RF_TEX, dst.tex_);
      IOObj::ReadFileR(rf, RF_COLOR, dst.color_);
      rf.ReadRepeatBegin(RF_GROUP);
        while(rf.ReadRepeatChild()) {
          typename Mesh<PN>::MeshGroup *group = new typename Mesh<PN>::MeshGroup();
          dst.groups_.push_back(group);
          IOObj::ReadFileR(rf, RF_GROUP_FACEP, group->facep_);
          IOObj::ReadFileR(rf, RF_GROUP_FACEN, group->facen_);
          IOObj::ReadFileR(rf, RF_GROUP_FACET, group->facet_);
          IOObj::ReadFileR(rf, RF_GROUP_FACEC, group->facec_);
          IOObj::ReadFileR(rf, RF_GROUP_NAME, group->name_);
          IOObj::ReadFileR(rf, RF_GROUP_MATNAME, group->matname_);
          if (!group->matname_.empty()) {
            Material *mat = new Material();
            IOObj::ReadFileR(rf, RF_GROUP_MATERIAL, *mat);
            ZRM->Add(mat, group->matname_);
          }
        }
      rf.ReadRepeatEnd();
    rf.ReadChildEnd();
    dst.SetDataFlags();
  }
};
};  // namespace zzz
