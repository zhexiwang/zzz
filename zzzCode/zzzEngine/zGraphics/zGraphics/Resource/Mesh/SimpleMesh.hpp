#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Utility/HasFlag.hpp>
#include <zCore/Utility/STLVector.hpp>
//hold only vertex
//2 ways, face indexes vertex
//or redundant triangle
//can convert from one to another
namespace zzz{
const int SMESH_POS      =0x00000001;
const int SMESH_NOR      =0x00000002;
const int SMESH_TEX      =0x00000004;
const int SMESH_TRI      =0x00000008;

class ZGRAPHICS_CLASS SimpleMesh : public HasFlags
{
public:
  SimpleMesh();
  void SetDataFlags();
  void Clear();

  typedef Vector<3,Vector3f> Triangle;
  STLVector<Triangle> triangles_;

  STLVector<Vector3f> vertices_;
  STLVector<Vector3i> faces_;

  void DrawTriangles();
  void DrawTriangle(zuint i);

  void DrawFaces();
  void DrawFace(zuint i);
  void DrawPoint(zuint i);

  void FromTriangles();
  void ToTriangles();

  STLVector<Vector3f> normals_;
  STLVector<Vector3f> texcoords_;

};

}