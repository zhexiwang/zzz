#include "ShaderHelper.hpp"
#include "Shader.hpp"
#include <zCore/Utility/StringTools.hpp>
#include <zCore/Utility/FileTools.hpp>
#include <zCore/Utility/ZCheck.hpp>

namespace zzz{
bool ShaderHelper::LoadShaderFile(const string &vertexFile, const string &fragmentFile, const string &geometryFile, Shader *o) {
  ZLOGI << "Load shader from file: " << ZVAR(vertexFile) << ZVAR(fragmentFile) << ZVAR(geometryFile) << "...\n";
  bool ret=o->LoadVertexFile(vertexFile)\
    && o->LoadFragmentFile(fragmentFile)\
    && o->LoadGeometryFile(geometryFile)\
    && o->Link();
  return ret;
}

bool ShaderHelper::LoadShaderMemory(const char * vertexMem, const char * fragmentMem, const char * geometryMem, Shader *o) {
  ZLOGI << "Load shader from memory...\n";
  bool ret=o->LoadVertexMemory(vertexMem)\
    && o->LoadFragmentMemory(fragmentMem)\
    && o->LoadGeometryMemory(geometryMem)\
    && o->Link();
  return ret;
}

Shader *ShaderHelper::LoadShaderFile(const char * vertexFile, const char * fragmentFile,const char * geometryFile) {
  ZLOGI << "Load shader from file: " << ZVAR(vertexFile) << ZVAR(fragmentFile) << ZVAR(geometryFile) << "...\n";
  Shader *o = new Shader;
  bool ret=o->LoadVertexFile(vertexFile)\
    && o->LoadFragmentFile(fragmentFile)\
    && o->LoadGeometryFile(geometryFile)\
    && o->Link();
  if (ret) return o;
  delete o;
  return NULL;
}

Shader *ShaderHelper::LoadShaderMemory(const char * vertexMem, const char * fragmentMem, const char * geometryMem) {
  ZLOGI << "Load shader from memory...\n";
  Shader *o = new Shader;
  bool ret=o->LoadVertexMemory(vertexMem)\
    && o->LoadFragmentMemory(fragmentMem)\
    && o->LoadGeometryMemory(geometryMem)\
    && o->Link();
  if (ret) return o;
  delete o;
  return NULL;
}

}
