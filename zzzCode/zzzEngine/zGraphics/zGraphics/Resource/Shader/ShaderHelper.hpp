#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include <zCore/common.hpp>

namespace zzz {
class Shader;
class ZGRAPHICS_CLASS ShaderHelper {
public:
  static bool LoadShaderFile(const string &vertexFile, const string &fragmentFile, const string &geometryFile, Shader *o);
  static bool LoadShaderMemory(const char * vertexMem,const char * fragmentMem, const char *geometryMen, Shader *shader);
  static Shader *LoadShaderFile(const char * vertexFile, const char * fragmentFile,const char * geometryFile);
  static Shader *LoadShaderMemory(const char * vertexMem,const char * fragmentMem, const char *geometryMen);
};
}
