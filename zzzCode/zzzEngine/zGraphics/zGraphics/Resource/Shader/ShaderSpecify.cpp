#include "ShaderSpecify.hpp"

namespace zzz{
//////////////////////////////////////////////////////////////////////////
//COLORSHADER
const char ColorShaderVert[]="\
varying vec4 color; \
void main(void)\
{\
  gl_Position=ftransform(); \
  color=gl_Color; \
}\
";
const char ColorShaderFrag[]="\
varying vec4 color; \
void main(void)\
{\
  gl_FragColor=color; \
}\
";
//////////////////////////////////////////////////////////////////////////
//DIFFUSESHADER
// NORMAL SHOULD NOT BE -
// BUT THE RESULT IS ALWAYS WRONG
// DON'T KNOW WHY
const char DiffuseShaderVert[]="\
varying vec3 color, normal, lightDir;\
void main() {	\
  normal = gl_NormalMatrix * gl_Normal;\
  vec3 vVertex = vec3(gl_ModelViewMatrix * gl_Vertex);\
  lightDir = -vVertex;\
  color = vec3(gl_Color);\
  gl_Position = ftransform();\
}\
";

const char DiffuseShaderFrag[]="\
varying vec3 color, normal, lightDir;\
void main() {\
  vec3 final_color = vec3(0, 0, 0);\
  vec3 N = normalize(normal);\
  vec3 L = normalize(lightDir);\
  float lambertTerm = dot(N,L);\
  if(lambertTerm > 0.0) {\
    final_color += color * lambertTerm;\
  }\
  gl_FragColor = vec4(final_color, 1);\
}\
";
//////////////////////////////////////////////////////////////////////////
//LIGHTSHADER
// NORMAL SHOULD NOT BE -
// BUT THE RESULT IS ALWAYS WRONG
// DON'T KNOW WHY
const char LightShaderVert[]="\
varying vec3 color, normal, lightDir, eyeVec;\
void main() {	\
  normal = gl_NormalMatrix * gl_Normal;\
  vec3 vVertex = vec3(gl_ModelViewMatrix * gl_Vertex);\
  eyeVec = -vVertex;\
  lightDir = -vVertex;\
  color = vec3(gl_Color);\
  gl_Position = ftransform();\
}\
";

const char LightShaderFrag[]="\
varying vec3 color, normal, lightDir, eyeVec;\
void main() {\
  vec3 final_color = vec3(0, 0, 0);\
  vec3 N = normalize(normal);\
  vec3 L = normalize(lightDir);\
  float lambertTerm = dot(N,L);\
  if(lambertTerm > 0.0) {\
    final_color += color * lambertTerm;\
    vec3 E = normalize(eyeVec);\
    vec3 R = reflect(-L, N);\
    float specular = pow(max(dot(R, E), 0.0), 50);\
    final_color += color * specular;\
  }\
  gl_FragColor = vec4(final_color, 1);\
}\
";
//////////////////////////////////////////////////////////////////////////
//TEXTURESHADER
const char TextureShaderVert[]="\
varying vec2 texc; \
void main(void) {\
  gl_Position=ftransform(); \
  texc=vec2(gl_MultiTexCoord0); \
}\
";
const char TextureShaderFrag[]="\
varying vec2 texc; \
uniform sampler2D tex; \
void main(void) {\
  gl_FragColor=texture2D(tex,texc); \
  gl_FragColor[3]=1; \
}\
";
//////////////////////////////////////////////////////////////////////////
//TEXTUREDIFFUSESHADER
const char TextureDiffuseShaderVert[]="\
varying vec2 texc; \
varying vec3 color, normal, lightDir;\
void main(void) {\
  normal = gl_NormalMatrix * gl_Normal;\
  vec3 vVertex = vec3(gl_ModelViewMatrix * gl_Vertex);\
  lightDir = -vVertex;\
  color = vec3(gl_Color);\
  gl_Position = ftransform();\
  texc=vec2(gl_MultiTexCoord0); \
}\
";

const char TextureDiffuseShaderFrag[]="\
varying vec2 texc; \
uniform sampler2D tex; \
varying vec3 color, normal, lightDir;\
void main(void) {\
  vec3 final_color = vec3(0, 0, 0);\
  vec3 N = normalize(normal);\
  vec3 L = normalize(lightDir);\
  float lambertTerm = dot(N,L);\
  if(lambertTerm > 0.0) {\
    final_color += color * lambertTerm;\
  }\
  final_color = final_color * vec3(texture2D(tex, texc)); \
  gl_FragColor = vec4(final_color, 1);\
}\
";
//////////////////////////////////////////////////////////////////////////
//TEXTURELIGHTSHADER
const char TextureLightShaderVert[]="\
varying vec2 texc; \
varying vec3 color, normal, lightDir, eyeVec;\
void main(void) {\
  normal = gl_NormalMatrix * gl_Normal;\
  vec3 vVertex = vec3(gl_ModelViewMatrix * gl_Vertex);\
  eyeVec = -vVertex;\
  lightDir = -vVertex;\
  color = vec3(gl_Color);\
  gl_Position = ftransform();\
  texc=vec2(gl_MultiTexCoord0); \
}\
";

const char TextureLightShaderFrag[]="\
varying vec2 texc; \
uniform sampler2D tex; \
varying vec3 color, normal, lightDir, eyeVec;\
void main(void) {\
  vec3 final_color = vec3(0, 0, 0);\
  vec3 N = normalize(normal);\
  vec3 L = normalize(lightDir);\
  float lambertTerm = dot(N,L);\
  if(lambertTerm > 0.0) {\
    final_color += color * lambertTerm;\
    vec3 E = normalize(eyeVec);\
    vec3 R = reflect(-L, N);\
    float specular = pow(max(dot(R, E), 0.0), 50);\
    final_color += color * specular;\
  }\
  final_color = final_color * vec3(texture2D(tex,texc)); \
  gl_FragColor = vec4(final_color, 1);\
}\
";
//////////////////////////////////////////////////////////////////////////
//TEXCOORDSHADER
const char TexCoordShaderVert[]="\
varying vec2 texc; \
void main(void) {\
  gl_Position=ftransform();\
  texc=vec2(gl_MultiTexCoord0); \
}\
";
const char TexCoordShaderFrag[]="\
varying vec2 texc;\
void main(void) {\
gl_FragColor=vec4(texc[0], texc[1], 0, 1); \
}\
";
//////////////////////////////////////////////////////////////////////////
//CUBEMAPSHADER
const char CubemapShaderVert[]="\
varying vec3 texc; \
void main(void)\
{\
  gl_Position=ftransform(); \
  texc=vec3(gl_MultiTexCoord0); \
}\
";
const char CubemapShaderFrag[]="\
varying vec3 texc; \
uniform samplerCube tex; \
void main(void) {\
  gl_FragColor=textureCube(tex,texc); \
}\
";

void MakeShaders() {
  if (ZRM->IsExist("ColorShader")) return;

  printf("Prepare general shaders...\n");
  ZRM->Add(ShaderHelper::LoadShaderMemory(ColorShaderVert,ColorShaderFrag,NULL),"ColorShader");
  ZRM->Add(ShaderHelper::LoadShaderMemory(DiffuseShaderVert,DiffuseShaderFrag,NULL),"DiffuseShader");
  ZRM->Add(ShaderHelper::LoadShaderMemory(LightShaderVert,LightShaderFrag,NULL),"LightShader");
  ZRM->Add(ShaderHelper::LoadShaderMemory(TextureShaderVert,TextureShaderFrag,NULL),"TextureShader");
  ZRM->Add(ShaderHelper::LoadShaderMemory(TextureDiffuseShaderVert,TextureDiffuseShaderFrag,NULL),"TextureDiffuseShader");
  ZRM->Add(ShaderHelper::LoadShaderMemory(TextureLightShaderVert,TextureLightShaderFrag,NULL),"TextureLightShader");
  ZRM->Add(ShaderHelper::LoadShaderMemory(TexCoordShaderVert,TexCoordShaderFrag,NULL),"TexCoordShader");
  ZRM->Add(ShaderHelper::LoadShaderMemory(CubemapShaderVert,CubemapShaderFrag,NULL),"CubemapShader");
}
}