#pragma once
#include <zGraphics/zGraphicsConfig.hpp>
#include "Shader.hpp"
#include "ShaderHelper.hpp"

namespace zzz{

ZGRAPHICS_FUNC void MakeShaders();
}
#define ZCOLORSHADER ZRM->Get<Shader*>("ColorShader")
#define ZDIFFUSESHADER ZRM->Get<Shader*>("DiffuseShader")
#define ZLIGHTSHADER ZRM->Get<Shader*>("LightShader")
#define ZTEXTURESHADER ZRM->Get<Shader*>("TextureShader")
#define ZTEXTURELIGHTSHADER ZRM->Get<Shader*>("TextureLightShader")
#define ZTEXCOORDSHADER ZRM->Get<Shader*>("TexCoordShader")
#define ZCUBEMAPSHADER ZRM->Get<Shader*>("CubemapShader")

#define ZSHADEREND() Shader::End();
