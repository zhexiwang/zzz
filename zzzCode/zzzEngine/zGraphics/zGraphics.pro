include($$PWD/../../zzzlib.pri)

QT       -= core gui

TARGET = zGraphics$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    $$files($$PWD/zGraphics/Context/*.cpp) \
    $$files($$PWD/zGraphics/FBO/*.cpp) \
    $$files($$PWD/zGraphics/Graphics/*.cpp) \
    $$files($$PWD/zGraphics/GraphicsAlgo/*.cpp) \
    $$files($$PWD/zGraphics/GraphicsGUI/*.cpp) \
    $$files($$PWD/zGraphics/RayTracer/*.cpp) \
    $$files($$PWD/zGraphics/Renderer/*.cpp) \
    $$files($$PWD/zGraphics/Resource/Mesh/*.cpp) \
    $$files($$PWD/zGraphics/Resource/Mesh/MeshIO/*.cpp) \
    $$files($$PWD/zGraphics/Resource/Shader/*.cpp) \
    $$files($$PWD/zGraphics/Resource/Texture/*.cpp) \
    $$files($$PWD/zGraphics/VBO/*.cpp) \
    $$files($$PWD/zGraphics/*.cpp)

SOURCES -= $$files($$PWD/zGraphics/GraphicsAlgo/HDS.cpp)
SOURCES -= $$files($$PWD/zGraphics/Graphics/CubeMap.cpp)

HEADERS += \
    $$files($$PWD/zGraphics/Context/*.hpp) \
    $$files($$PWD/zGraphics/FBO/*.hpp) \
    $$files($$PWD/zGraphics/Graphics/*.hpp) \
    $$files($$PWD/zGraphics/GraphicsAlgo/*.hpp) \
    $$files($$PWD/zGraphics/GraphicsGUI/*.hpp) \
    $$files($$PWD/zGraphics/RayTracer/*.hpp) \
    $$files($$PWD/zGraphics/Renderer/*.hpp) \
    $$files($$PWD/zGraphics/Resource/Mesh/*.hpp) \
    $$files($$PWD/zGraphics/Resource/Mesh/MeshIO/*.hpp) \
    $$files($$PWD/zGraphics/Resource/Shader/*.hpp) \
    $$files($$PWD/zGraphics/Resource/Texture/*.hpp) \
    $$files($$PWD/zGraphics/VBO/*.hpp) \
    $$files($$PWD/zGraphics/*.hpp)

OTHER_FILES += \

Z3RDENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZ3rd.pri) {
    error("Cannot include QTFlagsZ3rd.pri")
}

ZCOREENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZCore.pri) {
    error("Cannot include QTFlagsZCore.pri")
}

ZIMAGEENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZImage.pri) {
    error("Cannot include QTFlagsZImage.pri")
}

GLEWENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsGlew.pri) {
    error("Cannot include QTFlagsGlew.pri")
}

BOOSTENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsBoost.pri) {
    error("Cannot include QTFlagsBoost.pri")
}

DEFINES = $$replace(DEFINES, ZETA, ZZZ_LIB)
DEFINES = $$replace(DEFINES, _ENABLE, )
message($$DEFINES)

