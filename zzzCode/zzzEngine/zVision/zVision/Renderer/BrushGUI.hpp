#pragma once
#include <zGraphics/GraphicsGUI/GraphicsGUI.hpp>
#include <zGraphics/Renderer/Vis2DRenderer.hpp>

namespace zzz {
class BrushGUI : public GraphicsGUI<Vis2DRenderer>
{
public:
  BrushGUI();
  bool OnLButtonDown(unsigned int nFlags,int x,int y);
  bool OnLButtonUp(unsigned int nFlags,int x,int y);
  bool OnRButtonDown(unsigned int nFlags,int x,int y);
  bool OnRButtonUp(unsigned int nFlags,int x,int y);
  bool OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
  bool OnMouseMove(unsigned int nFlags,int x,int y);
  void Prepare(const Image4f &img);
  Image4f show_;
  Imageuc mask_;
  zuchar sel_id_;
private:
  bool left_, right_;
  int x_, y_;
  Vector2ui pos_, last_;
  Image4f ori_;
  int brushsize_;
  void PrepareShow();
};
}  // namespace zzz