#pragma once
#include <zGraphics/Renderer/Vis2DRenderer.hpp>

namespace zzz{

template<typename T>
class MatchRenderer : public Vis2DRenderer, public GraphicsHelper
{
public:
  typedef pair<Vector2i,Vector2i> MatchPair;
  MatchRenderer():shows_(10){}
  void DrawObj()
  {
    if (shows_>match_.size()) shows_=match_.size();
    DrawImage(*img1_);
    SetRasterPosRelative(img1_->Cols(),0);
    DrawImage(*img2_);
    for (zuint i=0; i<shows_; i++)
    {
      Draw2DLine(   match_[i].first[0],          match_[i].first[1],\
              img1_->Cols()+match_[i].second[0],  match_[i].second[1],\
              *(ColorDefine::Value[(i*10)%ColorDefine::Size]),  1);
    }

    Draw2DLine(   match_[shows_-1].first[0],        match_[shows_-1].first[1],\
            img1_->Cols()+match_[shows_-1].second[0],  match_[shows_-1].second[1],\
            *(ColorDefine::Value[((shows_-1)*10)%ColorDefine::Size]),  4);
    
  }
  void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags)
  {
    switch(nChar)
    {
    case ZZZKEY_RIGHT:
      if (shows_<match_.size()) 
      {
        shows_++;
        zout<<"("<<match_[shows_-1].first[0]<<","<<match_[shows_-1].first[1]<<")==>";
        zout<<"("<<match_[shows_-1].second[0]<<","<<match_[shows_-1].second[1]<<")\n";
      }
      break;
    case ZZZKEY_LEFT:
      if (shows_>1)
      {
        shows_--;
        zout<<"("<<match_[shows_-1].first[0]<<","<<match_[shows_-1].first[1]<<")==>";
        zout<<"("<<match_[shows_-1].second[0]<<","<<match_[shows_-1].second[1]<<")\n";
      }
      break;
    case ZZZKEY_UP:
      if (shows_<match_.size()) 
      {
        if (shows_<match_.size()-9) shows_+=10;
        else shows_=match_.size();
        zout<<"("<<match_[shows_-1].first[0]<<","<<match_[shows_-1].first[1]<<")==>";
        zout<<"("<<match_[shows_-1].second[0]<<","<<match_[shows_-1].second[1]<<")\n";
      }
      break;
    case ZZZKEY_DOWN:
      if (shows_>1)
      {
        if (shows_>9) shows_-=10;
        else shows_=1;
        zout<<"("<<match_[shows_-1].first[0]<<","<<match_[shows_-1].first[1]<<")==>";
        zout<<"("<<match_[shows_-1].second[0]<<","<<match_[shows_-1].second[1]<<")\n";
      }
      break;
    case ZZZKEY_END:
      shows_=match_.size();
      break;
    case ZZZKEY_HOME:
      shows_=1;
      break;
    }
    Redraw();
  }

  Image<T> *img1_, *img2_;
  vector<MatchPair> match_;
  zuint shows_;
};

}

