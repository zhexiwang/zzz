#pragma once
#include <zGraphics/Renderer/Vis2DRenderer.hpp>
#include <zGraphics/GraphicsGUI/GraphicsGUI.hpp>
#include <zCore/Math/Vector2.hpp>
#include <zVision/VisionAlgo/EdgeTracer.hpp>

namespace zzz {
class CutGUI : public GraphicsGUI<Vis2DRenderer>
{
public:
  CutGUI();
  bool OnLButtonDown(unsigned int nFlags,int x,int y);
  bool OnLButtonUp(unsigned int nFlags,int x,int y);
  bool OnRButtonDown(unsigned int nFlags,int x,int y);
  bool OnRButtonUp(unsigned int nFlags,int x,int y);
  bool OnMouseMove(unsigned int nFlags,int x,int y);
  bool OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
  bool OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);

  void Prepare(const Image4f &img);
  void GetCurSeg(Imageuc &saveimg);
  Image4f show_;
  zzz::Imageuc segments_;
  zuchar curseg_;
private:
  bool left_, right_;
  int x_, y_;
  Vector2i pos_;

  // data
  Image4f ori_;
  Imageuc mask_;

  //segment
  static const zuchar CUT_EDGE=1;
  static const zuchar CUT_INSIDE=0;
  static const zuchar CUT_OUTSIDE=2;
  void GetCurCut(Imageuc &img);
  void CombineSeg(Imageuc &cut, zuchar seg_id);

  // click optimization
  bool optmode_;
  zzz::Vector2i opt_;
  int boxsize_;

  // mask
  bool usemask_;
  zzz::Vector2i last_;
  int masksize_;

  // core path finder
  EdgeTracer pathfinder_;

  // path
  vector<vector<Vector2i> > fixedpath_;
  vector<Vector2i> curpath_;
  vector<Vector2i> seeds_;

  //input status
  enum {FIRST_CLICK,REST_CLICK,FINISH_CLICK} inputstatus_;

  //prepare what to show
  enum ShowMode{CUT_MODE, MASK_MODE};
  void PrepareShow(ShowMode showmode);

};
}  // namespace zzz