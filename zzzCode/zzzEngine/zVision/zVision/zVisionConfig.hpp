#pragma once

#include <zCore/EnvDetect.hpp>
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_OS_WIN32
#include "zVisionConfig.hpp.win32"
#endif