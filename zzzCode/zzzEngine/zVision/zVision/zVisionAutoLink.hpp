#pragma once

// Separate link so when change a lib, only link needs redo.
#include <zImage/zImageConfig.hpp>

#ifndef ZZZ_NO_PRAGMA_LIB

#ifdef ZZZ_COMPILER_MSVC

#ifdef ZZZ_DEBUG
#ifndef ZZZ_OS_WIN64
#pragma comment(lib,"zVisionD.lib")
#else
#pragma comment(lib,"zVisionD_x64.lib")
#endif // ZZZ_OS_WIN64
#else
#ifndef ZZZ_OS_WIN64
#pragma comment(lib,"zVision.lib")
#else
#pragma comment(lib,"zVision_x64.lib")
#endif // ZZZ_OS_WIN64
#endif


#ifdef ZZZ_LIB_FAST
#ifdef _DEBUG
#pragma comment(lib,"fastD.lib")
#else
#pragma comment(lib,"fast.lib")
#endif // _DEBUG
#endif // ZZZ_LIB_DEVIL

#endif // ZZZ_COMPILER_MSVC

#endif  // ZZZ_NO_PRAGMA_LIB
