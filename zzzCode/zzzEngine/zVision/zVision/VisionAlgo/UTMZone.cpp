#include "UTMZone.hpp"
#include <zCore/Math/Math.hpp>
#include <zCore/Utility/StringPrintf.hpp>

namespace zzz {
  // Constants that define the WGS84 Datum to describe the ellipsoid of the earth
const double kWGS84_Ellipsoid_Radius = 6378137;
const double kWGS84_Ellipsoid_SquaredEccentricity = 0.00669438;
const double FOURTHPI = C_PI / 4;

UTMZone::UTMZone() : longitude_zone_(0), latitude_zone_('I'),
                     utm_origin_(0, 0) {}

UTMZone::UTMZone(double latitude, double longitude) {
  Set(latitude, longitude);
}

UTMZone::UTMZone(double latitude, double longitude, bool set_origin) {
  Set(latitude, longitude, set_origin);
}

UTMZone::UTMZone(const zuint8 longitude_zone, const char latitude_zone)
   : longitude_zone_(longitude_zone), latitude_zone_(latitude_zone),
     utm_origin_(0, 0) {
  // no content
}

UTMZone::UTMZone(const string& zone) {
  int lng;
  char dummy;
  // A char will be converted into dummy and sscanf will return 3 if there is
  // garbage after the zone letter.
  ZCHECK(sscanf(zone.c_str(), "%d%c%c", &lng, &latitude_zone_, &dummy)==2)<< "UTM zone name parse error";
  longitude_zone_ = lng;
}

void UTMZone::Set(double latitude, double longitude) {
  Set(latitude, longitude, false);
}

void UTMZone::Set(double latitude, double longitude, bool set_origin) {
  // Make sure the longitude is between -180.00 .. 179.9
  double adj_longitude =
    (longitude+180)-static_cast<int>((longitude+180)/360)*360-180;

  longitude_zone_ = static_cast<int>((adj_longitude + 180)/6) + 1;

  if (latitude >= 56.0 && latitude < 64.0 &&
      adj_longitude >= 3.0 && adj_longitude < 12.0)
    longitude_zone_ = 32;

  // Special zones for Svalbard
  if (latitude >= 72.0 && latitude < 84.0) {
    if (adj_longitude >= 0.0  && adj_longitude <  9.0)
      longitude_zone_ = 31;
    else if (adj_longitude >= 9.0  && adj_longitude < 21.0)
      longitude_zone_ = 33;
    else if (adj_longitude >= 21.0 && adj_longitude < 33.0)
      longitude_zone_ = 35;
    else if (adj_longitude >= 33.0 && adj_longitude < 42.0)
      longitude_zone_ = 37;
  }

  latitude_zone_ = ComputeUTMZoneLetter(latitude);

  utm_origin_.Set(0, 0);
  if (set_origin) {
    double easting_origin, northing_origin;
    LatLngToLocal(latitude, longitude, &easting_origin, &northing_origin);
    utm_origin_.Set(easting_origin, northing_origin);
  }
}

string UTMZone::StringRepresentation() const {
  return StringPrintf("%u%c", LongitudeZone(), LatitudeZone());
}

void UTMZone::LatLngToLocal(double latitude, double longitude,
                            double* utm_easting, double* utnor_thing) const {
  ZCHECK(longitude_zone_ > 0);

  double a = kWGS84_Ellipsoid_Radius;
  double eccSquared = kWGS84_Ellipsoid_SquaredEccentricity;
  double k0 = 0.9996;

  double longitude_origin;
  double eccPrimeSquared;
  double N, T, C, A, M;

  // Make sure the longitude is between -180.00 .. 179.9
  double adj_longitude =
    (longitude+180)-static_cast<int>((longitude+180)/360)*360-180;

  double latitude_radians = latitude*C_D2R;
  double longitude_radians = adj_longitude*C_D2R;
  double longitude_origin_radians;

  bool is_southern_hemisphere = latitude < 0;

  // The "+3" puts the origin in the middle of the zone
  longitude_origin = (longitude_zone_ - 1)*6 - 180 + 3;
  longitude_origin_radians = longitude_origin * C_D2R;

  eccPrimeSquared = (eccSquared)/(1-eccSquared);

  N = a/sqrt(1-eccSquared*sin(latitude_radians)*sin(latitude_radians));
  T = tan(latitude_radians)*tan(latitude_radians);
  C = eccPrimeSquared*cos(latitude_radians)*cos(latitude_radians);
  A = cos(latitude_radians)*(longitude_radians-longitude_origin_radians);

  M = a*(latitude_radians       *(1-eccSquared/4-3*eccSquared*eccSquared/64
                                  -5*eccSquared*eccSquared*eccSquared/256)
         -sin(2*latitude_radians)*(3*eccSquared/8 + 3*eccSquared*eccSquared/32
                                   + 45*eccSquared*eccSquared*eccSquared/1024)
         +sin(4*latitude_radians)*(15*eccSquared*eccSquared/256
                                   + 45*eccSquared*eccSquared*eccSquared/1024)
         -sin(6*latitude_radians)*(35*eccSquared*eccSquared*eccSquared/3072));


  *utm_easting = static_cast<double>(k0*N*(A+(1-T+C)*A*A*A/6
                                + (5-18*T+T*T+72*C-
                                   58*eccPrimeSquared)*A*A*A*A*A/120)
                                + 500000.0);

  *utnor_thing = static_cast<double>(k0*(M+N*tan(latitude_radians)*
                               (A*A/2+(5-T+9*C+4*C*C)*A*A*A*A/24
                                + (61-58*T+T*T+600*C-330*eccPrimeSquared)
                                *A*A*A*A*A*A/720)));
  // 10000000 meter offset for southern hemisphere
  if (is_southern_hemisphere)
    *utnor_thing += 10000000.0;

  // Adjust for origin
  *utm_easting -= utm_origin_[0];
  *utnor_thing -= utm_origin_[1];
}

void UTMZone::LocalToLatLng(double utm_easting, double utnor_thing,
                            double* latitude, double* longitude) const {
  ZCHECK(longitude_zone_ > 0);

  // Equations from USGS Bulletin 1532
  double k0 = 0.9996;
  double a = kWGS84_Ellipsoid_Radius;
  double eccSquared = kWGS84_Ellipsoid_SquaredEccentricity;
  double eccPrimeSquared;
  double e1 = (1-sqrt(1-eccSquared))/(1+sqrt(1-eccSquared));
  double N1, T1, C1, R1, D, M;
  double longitude_origin;
  double mu, phi1, phi1Rad;
  double x, y;
  bool in_northern_hemisphere;

  // remove 500,000 meter offset for longitude
  x = utm_easting - 500000.0;
  y = utnor_thing;

  // Adjust by origin
  x += utm_origin_[0];
  y += utm_origin_[1];

  if ((latitude_zone_ - 'N') >= 0) {
    // point is in northern hemisphere
    in_northern_hemisphere = true;
  } else {
    // point is in southern hemisphere
    in_northern_hemisphere = false;
    // remove 10,000,000 meter offset used for southern hemisphere
    y -= 10000000.0;
  }

  // (+3 puts origin in middle of zone)
  longitude_origin = (longitude_zone_ - 1)*6 - 180 + 3;

  eccPrimeSquared = (eccSquared)/(1-eccSquared);

  M = y / k0;
  mu = M/(a*(1-eccSquared/4-3*eccSquared*eccSquared/64-
             5*eccSquared*eccSquared*eccSquared/256));

  phi1Rad = mu  + (3*e1/2-27*e1*e1*e1/32)*sin(2*mu)
            + (21*e1*e1/16-55*e1*e1*e1*e1/32)*sin(4*mu)
            +(151*e1*e1*e1/96)*sin(6*mu);
  phi1 = phi1Rad*C_R2D;

  N1 = a/sqrt(1-eccSquared*sin(phi1Rad)*sin(phi1Rad));
  T1 = tan(phi1Rad)*tan(phi1Rad);
  C1 = eccPrimeSquared*cos(phi1Rad)*cos(phi1Rad);
  R1 = a*(1-eccSquared)/pow(1-eccSquared*sin(phi1Rad)*sin(phi1Rad), 1.5);
  D = x/(N1*k0);

  *latitude = phi1Rad - (N1*tan(phi1Rad)/R1)*
              (D*D/2-(5+3*T1+10*C1-4*C1*C1-9*eccPrimeSquared)*D*D*D*D/24
               +(61+90*T1+298*C1+45*T1*T1-
                 252*eccPrimeSquared-3*C1*C1)*D*D*D*D*D*D/720);
  // Convert to degrees
  *latitude = *latitude * C_R2D;

  *longitude = (D-(1+2*T1+C1)*D*D*D/6+
                (5-2*C1+28*T1-3*C1*C1+8*eccPrimeSquared+24*T1*T1)
                *D*D*D*D*D/120)/cos(phi1Rad);
  // Convert to degrees and add zone offset
  *longitude = longitude_origin + *longitude * C_R2D;
}

char UTMZone::ComputeUTMZoneLetter(double latitude) {
  // This routine determines the correct UTM letter designator for the given
  // latitude returns 'Z' if latitude is outside the UTM limits of 84N to 80S
  if ((84 >= latitude) && (latitude >= 72)) return 'X';
  else if ((72 > latitude) && (latitude >= 64)) return 'W';
  else if ((64 > latitude) && (latitude >= 56)) return 'V';
  else if ((56 > latitude) && (latitude >= 48)) return 'U';
  else if ((48 > latitude) && (latitude >= 40)) return 'T';
  else if ((40 > latitude) && (latitude >= 32)) return 'S';
  else if ((32 > latitude) && (latitude >= 24)) return 'R';
  else if ((24 > latitude) && (latitude >= 16)) return 'Q';
  else if ((16 > latitude) && (latitude >= 8)) return 'P';
  else if ((8 > latitude) && (latitude >= 0)) return 'N';
  else if ((0 > latitude) && (latitude >= -8)) return 'M';
  else if ((-8> latitude) && (latitude >= -16)) return 'L';
  else if ((-16 > latitude) && (latitude >= -24)) return 'K';
  else if ((-24 > latitude) && (latitude >= -32)) return 'J';
  else if ((-32 > latitude) && (latitude >= -40)) return 'H';
  else if ((-40 > latitude) && (latitude >= -48)) return 'G';
  else if ((-48 > latitude) && (latitude >= -56)) return 'F';
  else if ((-56 > latitude) && (latitude >= -64)) return 'E';
  else if ((-64 > latitude) && (latitude >= -72)) return 'D';
  else if ((-72 > latitude) && (latitude >= -80)) return 'C';

  ZLOGS << "Latitude outside UTM limits: " << latitude;

  // If we get here, we're either in the north pole or south pole.
  // Pick a letter as appropriate:
  if (latitude > 84)
    return 'Z';
  else if (latitude < -80)
    return 'A';

  ZLOGF << "Impossible: " << latitude;
  // Never reached, but satisfies compiler that all code paths return.
  return 'Z';
}

}