#pragma once
#include <zCore/Define.hpp>
#include <zCore/common.hpp>
#include <zCore/Math/Vector2.hpp>

namespace zzz {
class UTMZone {
 public:
  UTMZone();
  UTMZone(double latitude, double longitude);
  UTMZone(double latitude, double longitude, bool set_origin);
  UTMZone(zuint8 logitude_zone, char latitude_zone);
  explicit UTMZone(const string& zone);

  bool IsValid() const {return longitude_zone_ > 0; }
  bool operator==(const UTMZone& other) const {
    return longitude_zone_ == other.longitude_zone_ &&
           latitude_zone_  == other.latitude_zone_ &&
           utm_origin_     == other.utm_origin_; }
  void SetInvalid() { longitude_zone_ = 0; latitude_zone_ = 'I'; }

  void Set(double latitude, double longitude);
  void Set(double latitude, double longitude, bool set_origin);

  inline zuint8 LongitudeZone() const { return longitude_zone_; }
  inline char LatitudeZone()  const { return latitude_zone_;  }
  string StringRepresentation() const;

  //------ Satisfy the MapProjection base class interface -----------------
  virtual void LatLngToLocal(double lat, double lng,
                             double* x, double* y) const;
  virtual void LocalToLatLng(double x, double y,
                             double* lat, double* lng) const;
  void SetReference(double lat, double lng) { Set(lat, lng, true); }

  // Returns the [utm_easting, utnor_thing] location.
  const Vector2d& utm_origin() const {return utm_origin_; }

  void set_utm_origin(const Vector2d& new_origin) { utm_origin_ = new_origin; }

 private:
  zint longitude_zone_;
  char latitude_zone_;

  // Allow an origin to be specified by which all the points will be translated.
  Vector2d utm_origin_;

  static char ComputeUTMZoneLetter(double latitude);
};

}  // namespace zzz

