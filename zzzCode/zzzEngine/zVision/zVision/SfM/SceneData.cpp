#include "SceneData.hpp"

namespace zzz {
template<>
const zuint32 SceneData<zfloat32>::Point2D::BAD = MAX_ZUINT32;
template<>
const zuint32 SceneData<zfloat32>::SIFTMatch::BAD = MAX_ZUINT32;
};  // namespace zzz