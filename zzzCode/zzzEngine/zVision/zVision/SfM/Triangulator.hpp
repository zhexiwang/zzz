#pragma once
#include "ProjectionMat.hpp"
#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Math/Vector.hpp>
#include "SceneData.hpp"

namespace zzz{
class Triangulator
{
public:
  static Vector3d LinearTriangulate(const ProjectionMat<double> *P, const Vector2d *pos2ds, int number);
  static Vector3d LinearTriangulate(const vector<ProjectionMat<double> > &P, const vector<Vector2d> &pos2ds);
  static void LinearTriangulate(const ProjectionMat<double> P[2], const vector<Vector2d> pos2ds[2], vector<Vector3d> &pos3ds);
  static void LinearTriangulate(const ProjectionMat<double> P[2], const Vector2d pos2d[2], Vector3d &pos3d);
  static void LinearTriangulate(SceneData<double> &sd);

  static void NonLinearTriangulate(const ProjectionMat<double> P[2], const vector<Vector2d> pos2ds[2], vector<Vector3d> &pos3ds);
};

}
