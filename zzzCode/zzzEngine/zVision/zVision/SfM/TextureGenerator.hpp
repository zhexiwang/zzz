#pragma once
#include <zImage/Image/Image.hpp>

//to generate textures according to a homography transformation
//input original point on the original image and corresponding projection point on a new image
//it will fill the new image by a homography transformation

namespace zzz{
template<typename T>
class TextureGenerator
{
public:
  TextureGenerator(void){}
  ~TextureGenerator(void){}
  void SetImage(const Image<T> &r){img_=r;}

  //ATTENTION: coordinate is homogenous and should be (r,c,w)
  void Cut(const vector<Vector3d> &orip, const vector<Vector3d> &prjp, Image<T> &tex)
  {
    Homography h;
    h.Create(orip,prjp);
    for (zuint r=0; r<tex.Rows(); r++) for (zuint c=0; c<tex.Cols(); c++)
    {
      Vector3d prj(r,c,1);
      Vector3d ori=h.ToA(prj);
      tex.At(r,c)=img_.Interpolate(ori[0],ori[1]);
    }
  }

private:
  Image<T> img_;
};

}