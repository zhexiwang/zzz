#include "XMS.hpp"
#include <zCore/Utility/TextProgressBar.hpp>
#include <zCore/Utility/CmdParser.hpp>

ZFLAGS_BOOL(XMS_check_errors, true, "Check error when loading XMS file.");

namespace zzz {
void XMS::Clear() {
  projections_.clear();
  tracks_.clear();
  cameras_.clear();
  camera_proj_.clear();
  track_proj_.clear();
  camera_ids_.clear();
  projection_ids_.clear();
  track_ids_.clear();
  relative_.Identical();
}

const XMS& XMS::operator=(const XMS& other) {
  Clear();
  projections_ = other.projections_;
  tracks_ = other.tracks_;
  cameras_ = other.cameras_;
  camera_proj_ = other.camera_proj_;
  track_proj_ = other.track_proj_;
  camera_ids_ = other.camera_ids_;
  projection_ids_ = other.projection_ids_;
  track_ids_ = other.track_ids_;
  relative_ = other.relative_;
  return *this;
}

void XMS::Load(const string &filename) {
  Clear();
  zzz::RapidXML xml;
  xml.LoadFile(filename);
  RapidXMLNode pro_node = xml.GetNode("projections");
  int n;
  FromString(pro_node.GetNode("num").GetText(), n);
  map<Vector2i, int> proj_idx;
  {ScopeTextProgressBar bar("Read Projections", 0, n - 1, 1);
  for (RapidXMLNode node = pro_node.GetFirstNode("val"); node.IsValid(); node.GotoNextSibling("val")) {
    bar.DeltaUpdate();
    XMSProjection p;
    Vector<8, float> tmp(0);
    FromString(node.GetText(), tmp);
    p.camera_id = tmp[0];
    p.track_id = tmp[1];
    p.proj[0] = tmp[2];
    p.proj[1] = tmp[3];
    p.flag = tmp[4];
    p.color[0] = tmp[5] / 256.0f;
    p.color[1] = tmp[6] / 256.0f;
    p.color[2] = tmp[7] / 256.0f;
    node.GotoNextSibling("key");
    int key = 0;
    FromString(node.GetText(), key);
    if (p.flag == 16) {
      if (ZFLAG_XMS_check_errors) {
        ZCHECK(projections_.find(key) == projections_.end()) <<
          "2 projections of the same key are found: " << ZVAR(key);
        ZCHECK(proj_idx.find(Vector2i(p.camera_id, p.track_id)) == proj_idx.end()) <<
          "2 projections of the same camera id and same track id are found: " <<
          proj_idx[Vector2i(p.camera_id, p.track_id)] << " and " << key;
        proj_idx[Vector2i(p.camera_id, p.track_id)] = key;
      }
      projections_[key] = p;
    }
  }}
  //ZCHECK_EQ(n, projections_.size());

  RapidXMLNode tra_node = xml.GetNode("tracks");
  FromString(tra_node.GetNode("num").GetText(), n);
  {ScopeTextProgressBar bar("Read Tracks", 0, n - 1, 1);
  for (RapidXMLNode node = tra_node.GetFirstNode("val"); node.IsValid(); node.GotoNextSibling("val")) {
    bar.DeltaUpdate();
    XMSTrack p;
    FromString(node.GetNode("flag").GetText(), p.flag);
    FromString(node.GetNode("cr").GetText(), p.color);
    FromString(node.GetNode("pos").GetText(), p.pos);
    node.GotoNextSibling("key");
    int key;
    FromString(node.GetText(), key);
    if (p.flag == 1) {
      ZCHECK(tracks_.find(key) == tracks_.end()) <<
        "2 tracks of the same key are found: " << ZVAR(key);
      tracks_[key] = p;
    }
  }}
  //ZCHECK_EQ(n, tracks_.size());

  RapidXMLNode cam_node = xml.GetNode("cameras");
  FromString(cam_node.GetNode("num").GetText(), n);
  {ScopeTextProgressBar bar("Read Cameras", 0, n - 1, 1);
  for (RapidXMLNode node = cam_node.GetFirstNode("val"); node.IsValid(); node.GotoNextSibling("val")) {
    bar.DeltaUpdate();
    XMSCamera p;
    FromString(node.GetNode("flag").GetText(), p.flag);
    FromString(node.GetNode("keyframe").GetText(), p.keyframe);
    Matrix<3, 4, double> P;
    FromString(node.GetNode("cam").GetText(), P);
    p.cam.SetP(P);
    node.GotoNextSibling("key");
    int key;
    FromString(node.GetText(), key);
    ZLOGD << ZVAR(key) << endl;
    p.cam.Decompose();
    if (p.flag == 1) {
      ZCHECK(cameras_.find(key) == cameras_.end()) <<
        "2 cameras of the same key are found: " << ZVAR(key);
      cameras_[key] = p;
    }
  }}

  if (xml.HasNode("config")) {
    RapidXMLNode cfg_node = xml.GetNode("config");
    if (FromString<int>(cfg_node.GetNode("camera_config").GetText()) == 2) {
      RapidXMLNode rel_node = cfg_node.GetNode("camera_relative_pose");
      Matrix3x3d R = FromString<Matrix3x3d>(rel_node.GetNode("relative_rotation").GetText());
      Vector3d t = FromString<Vector3d>(rel_node.GetNode("relative_translation").GetText());
      relative_[0]  = R[0];  relative_[1] = R[1];  relative_[2]  = R[2];  relative_[3] = t[0];
      relative_[4]  = R[3];  relative_[5] = R[4];  relative_[6]  = R[5];  relative_[7] = t[1];
      relative_[8]  = R[6];  relative_[9] = R[7];  relative_[10] = R[8];  relative_[11] = t[2];
      relative_[12] = 0;     relative_[13] = 0;    relative_[14]  = 0;    relative_[15] = 1;
    }
  } else if (xml.HasNode("right_rel")) {
    FromString(xml.GetNode("right_rel").GetText(), relative_);
  }
  ZLOGI << ZVAR(relative_) << endl;

  //ZCHECK_EQ(n, cameras_.size());
}

void XMS::Save(const string &filename) const {
  zzz::RapidXML xml(false);
  // output config
  xml.AppendCommentNode() << "Configuration shared in the whole file";
  RapidXMLNode cfg_node = xml.AppendNode("config");
  cfg_node.AppendCommentNode() << "The number of cameras: 0: unconstraint; 1: monocular; 2: binocular; etc.";
  cfg_node.AppendNode("camera_config") << "2";
  cfg_node.AppendCommentNode() << "The offset of IDs of cameras";
  cfg_node.AppendNode("camera_id_offset") << RIGHT_START;
  RapidXMLNode rel_node = cfg_node.AppendNode("camera_relative_pose");
  rel_node << make_pair("size", 1);
  Matrix3x3d R(relative_[0], relative_[1], relative_[2], relative_[4], relative_[5], relative_[6], relative_[8], relative_[9], relative_[10]);
  Vector3d t(relative_[3], relative_[7], relative_[11]);
  rel_node.AppendNode("relative_translation") << t;
  rel_node.AppendNode("relative_rotation") << R;

  // output projections
  xml.AppendCommentNode() << "Projection format: cameraID, trackID, projection_x, projection_y, flag, color_r, color_g, color_b\n"
                             "Projection flags: 0: UNKNOWN; 1: OUTLIER; 16: INLIER";
  RapidXMLNode pro_node = xml.AppendNode("projections");
  pro_node.AppendNode("num") << projections_.size();
  for (unordered_map<int, XMSProjection>::const_iterator ui = projections_.begin(); ui != projections_.end(); ++ui) {
    const XMSProjection &p = ui->second;
    Vector<8, float> tmp(0);
    pro_node.AppendNode("val") << p.camera_id << ' ' << p.track_id << ' ' 
                               << p.proj[0] << ' ' << p.proj[1] << ' ' << p.flag <<  ' '
                               << Clamp<int>(0, p.color[0] * 256.0f, 255) << ' '
                               << Clamp<int>(0, p.color[1] * 256.0f, 255) << ' '
                               << Clamp<int>(0, p.color[2] * 256.0f, 255);
    pro_node.AppendNode("key") << ui->first;
  }

  // output tracks
  xml.AppendCommentNode() << "Track flags: 0 -- UNKNOWN; 1 -- GOODTRACK; 16 -- BADTRACK";
  RapidXMLNode tra_node = xml.AppendNode("tracks");
  tra_node.AppendNode("num") << tracks_.size();
  for (unordered_map<int, XMSTrack>::const_iterator ui = tracks_.begin(); ui != tracks_.end(); ++ui) {
    const XMSTrack &p = ui->second;
    RapidXMLNode val_node = tra_node.AppendNode("val");
    val_node.AppendNode("flag") << p.flag;
    val_node.AppendNode("cr") << p.color;
    val_node.AppendNode("pos") << p.pos;
    tra_node.AppendNode("key") << ui->first;
  }

  // output cameras
  xml.AppendCommentNode() << "Camera flags: 0 -- UNKNOWN; 1 -- ESTIMATED; 16 -- FAILED";
  RapidXMLNode cam_node = xml.AppendNode("cameras");
  cam_node.AppendNode("num") << cameras_.size();
  for (unordered_map<int, XMSCamera>::const_iterator ui = cameras_.begin(); ui != cameras_.end(); ++ui) {
    const XMSCamera &p = ui->second;
    RapidXMLNode val_node = cam_node.AppendNode("val");
    val_node.AppendNode("flag") << p.flag;
    val_node.AppendNode("keyframe") << p.keyframe;
    val_node.AppendNode("cam") << p.cam.P();
    cam_node.AppendNode("key") << ui->first;
  }

  xml.SaveFile(filename);
}
const vector<int>& XMS::ProjOfCamera(int c) const {
  if (camera_proj_.empty()) {
    for (unordered_map<int, XMSCamera>::const_iterator ui = cameras_.begin(); ui != cameras_.end(); ++ui)
      camera_proj_[ui->first] = vector<int>();
    for (unordered_map<int, XMSProjection>::const_iterator ui = projections_.begin(); ui != projections_.end(); ++ui) {
      int idx = ui->first;
      int camera_id = ui->second.camera_id;
      camera_proj_[camera_id].push_back(idx);
    }
    for (unordered_map<int, vector<int> >::iterator ui = camera_proj_.begin(); ui != camera_proj_.end(); ++ui) {
      sort(ui->second.begin(), ui->second.end());
    }
  }
  return camera_proj_[c];
}

const vector<int>& XMS::ProjOfTrack(int c) const {
  if (track_proj_.empty()) {
    for (unordered_map<int, XMSCamera>::const_iterator ui = cameras_.begin(); ui != cameras_.end(); ++ui)
      track_proj_[ui->first] = vector<int>();
    for (unordered_map<int, XMSProjection>::const_iterator ui = projections_.begin(); ui != projections_.end(); ++ui) {
      int idx = ui->first;
      int track_id = ui->second.track_id;
      track_proj_[track_id].push_back(idx);
    }
    for (unordered_map<int, vector<int> >::iterator ui = track_proj_.begin(); ui != track_proj_.end(); ++ui) {
      sort(ui->second.begin(), ui->second.end());
    }
  }
  return track_proj_[c];
}

const vector<int>& XMS::CameraIDs() const {
  if (camera_ids_.empty()) {
    vector<int>& ids(camera_ids_);
    ids.reserve(cameras_.size());
    for (unordered_map<int, XMSCamera>::const_iterator ui = cameras_.begin(); ui != cameras_.end(); ++ui)
      ids.push_back(ui->first);
    sort(ids.begin(), ids.end());
  }
  return camera_ids_;
}

const vector<int>& XMS::ProjectionIDs() const {
  if (projection_ids_.empty()) {
    vector<int>& ids(projection_ids_);
    ids.reserve(projections_.size());
    for (unordered_map<int, XMSProjection>::const_iterator ui = projections_.begin(); ui != projections_.end(); ++ui)
      ids.push_back(ui->first);
    sort(ids.begin(), ids.end());
  }
  return projection_ids_;
}

const vector<int>& XMS::TrackIDs() const {
  if (track_ids_.empty()) {
    vector<int>& ids(track_ids_);
    ids.reserve(tracks_.size());
    for (unordered_map<int, XMSTrack>::const_iterator ui = tracks_.begin(); ui != tracks_.end(); ++ui)
      ids.push_back(ui->first);
    sort(ids.begin(), ids.end());
  }
  return track_ids_;
}

} // namespace zzz
