#pragma once
#include <zCore/Xml/RapidXML.hpp>
#include <zCore/common.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Math/Vector2.hpp>
#include "ProjectionMat.hpp"

namespace zzz {
struct XMSProjection {
  int track_id, camera_id;
  Vector2d proj;
  int flag;
  Vector3d color;
};

struct XMSTrack {
  int flag;
  Vector3d color;
  Vector3d pos;
};

struct XMSCamera {
  int flag;
  int keyframe;
  ProjectionMatd cam;
};
SIMPLE_IOOBJECT(XMSProjection);
SIMPLE_IOOBJECT(XMSTrack);
SIMPLE_IOOBJECT(XMSCamera);
class XMS {
public:
  XMS(void) {}
  XMS(const XMS& other) {*this = other;}
  const XMS& operator=(const XMS& other);
  void Clear();
  void Load(const string &filename);
  void Save(const string &filename) const;
  const vector<int>& ProjOfCamera(int c) const;
  const vector<int>& ProjOfTrack(int c) const;
  const vector<int>& CameraIDs() const;
  const vector<int>& ProjectionIDs() const;
  const vector<int>& TrackIDs() const;

  XMSProjection &Projection(const int tid) {return projections_[tid];}
  const XMSProjection &Projection(const int tid) const {unordered_map<int, XMSProjection>::const_iterator ui = projections_.find(tid); ZCHECK(ui != projections_.end()); return ui->second;}
  XMSTrack &Track(const int tid) {return tracks_[tid];}
  const XMSTrack &Track(const int tid) const {unordered_map<int, XMSTrack>::const_iterator ui = tracks_.find(tid); ZCHECK(ui != tracks_.end()); return ui->second;}
  XMSCamera &Camera(const int tid) {return cameras_[tid];}
  const XMSCamera &Camera(const int tid) const {unordered_map<int, XMSCamera>::const_iterator ui = cameras_.find(tid); ZCHECK(ui != cameras_.end()); return ui->second;}

//  void LoadRF(const string &filename) {
//    RecordFile rf;
//    rf.LoadFileBegin(filename);
//    IOObj::ReadFileR(rf, RF_PROJECTIONS, projections_);
//    IOObj::ReadFileR(rf, RF_TRACKS, tracks_);
//    IOObj::ReadFileR(rf, RF_CAMERAS, cameras_);
//    IOObj::ReadFileR(rf, RF_RELATIVE, relative_);
//    rf.LoadFileEnd();
//  }
//  void SaveRF(const string &filename) const {
//    RecordFile rf;
//    rf.SaveFileBegin(filename);
//    IOObj::WriteFileR(rf, RF_PROJECTIONS, projections_);
//    IOObj::WriteFileR(rf, RF_TRACKS, tracks_);
//    IOObj::WriteFileR(rf, RF_CAMERAS, cameras_);
//    IOObj::WriteFileR(rf, RF_RELATIVE, relative_);
//    rf.SaveFileEnd();
//  }
  unordered_map<int, XMSProjection> projections_; static const int RF_PROJECTIONS = 1;
  unordered_map<int, XMSTrack> tracks_;           static const int RF_TRACKS = 2;
  unordered_map<int, XMSCamera> cameras_;         static const int RF_CAMERAS = 3;

  zzz::Matrix4x4d relative_;                      static const int RF_RELATIVE = 4;
  static const int RIGHT_START = 10000000;
private:
  mutable unordered_map<int, vector<int> > camera_proj_, track_proj_;
  mutable vector<int> camera_ids_, projection_ids_, track_ids_;
};

} // namespace zzz
