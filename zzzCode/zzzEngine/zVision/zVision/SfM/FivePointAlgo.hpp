#pragma once
#include "FundamentalMat.hpp"
#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/IterExitCond.hpp>

//Calculate relative pose given intrinsic calibration, fundamental matrix and corresponding point pairs
namespace zzz{
int FivePointAlgo(EssentialMat &E, const Matrix3x3d &K, const vector<Vector2d> &ls, const vector<Vector2d> &rs, const double outlier_shreshold, IterExitCond<double> &cond);
}

