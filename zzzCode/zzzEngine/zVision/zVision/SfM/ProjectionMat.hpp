#pragma once
#include <zCore/Math/Matrix3x3.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Math/Vector4.hpp>
#include "../VisionTools.hpp"
#include <zGraphics/Graphics/Rotation.hpp>
#include <zGraphics/Graphics/Translation.hpp>
#include <zMatrix/zMat.hpp>

namespace zzz{
template<typename DataType=double>
class ProjectionMat {
public:
  ProjectionMat()
    : K_(1, 0, 0,
         0, 1, 0,
         0, 0, 1),
      R_(1, 0, 0,
         0, 1, 0,
         0, 0, 1),
      T_(0, 0, 0) {
  }

  void Identical() {
    Dress(P_) = (Diag(Ones<DataType>(3, 1)),Zeros<DataType>(3, 1));
    R_.Identical();
    T_=0;
  }

  void SetP(const Matrix<3,3,DataType> &M, const Vector<3,DataType> &m) {
    //F=[M|m]
    Dress(P_)=(Dress(M), Dress(m));
  }

  // Set P
  void SetP(const Matrix<3, 4, DataType> &P) {
    P_ = P;
  }

  // Calculate P from R and T
  void MakeP() {
    //P=K[R|T]
    Dress(P_) = (Dress(K_ * R_),Dress(K_ * T_));
    if (P_(2, 3) != 1) P_ /= Abs(P_(2, 3));
  }

  // Set R and T
  void SetRT(const Matrix<3,3,DataType> &R, const Vector<3,DataType> &T) {
    R_ = R;
    T_ = T;
  }

  void SetRotationTranslation(const Rotation<DataType> &R, const Translation<DataType> &t) {
    SetRT(R.Inverted(), R.Inverted() * (-t));
  }

  // Calculate R and T from P and K
  void MakeRT() {
    Matrix<3, 3, DataType> inv_K = K_.Inverted();
    Dress(R_) = Dress(P_)(Colon(), Colon(0, 2));
    Dress(T_) = Dress(P_)(Colon(), 3);
    R_ = inv_K * R_;
    T_ = inv_K * T_;
    // scale to rotation matrix
    DataType scale = 1.0 / R_.Row(0).Len();
    R_ *= scale;
    T_ *= scale;
  }

  void SetK(const Matrix<3,3,DataType> &K) {
    K_ = K;
  }

  void SetK(DataType fx, DataType fy, DataType cx, DataType cy) {
    K_.Zero();
    K_(0, 0) = fx;
    K_(1, 1) = fy;
    K_(0, 2) = cx;
    K_(1, 2) = cy;
    K_(2, 2) = 1;
  }

  // Vanishing point, 0:X 1:Y 2:Z
  Vector<3,DataType> VPoint(int x) {
    Vector<3,DataType> res;
    Dress(res) = Dress(P_)(Colon(),x);
    return res;
  }

  // Decompose from P
  void Decompose() {
    // computer internal and external parameters
    Vector<3, DataType> q1(P_.Row(0)), q2(P_.Row(1)), q3(P_.Row(2));

    Rotation<DataType> R;
    DataType p = DataType(1) / q3.Len();
    R.Row(2) = p * q3;	//r3 = p * q3;
    DataType u0 = p * p * FastDot(q1, q3);
    DataType v0 = p * p * FastDot(q2, q3);
    DataType alpha_u = Sqrt(p * p * FastDot(q1, q1) - u0 * u0);
    DataType alpha_v = Sqrt(p * p * FastDot(q2, q2) - v0 * v0);
    R.Row(0) = p * (q1 - (u0 * q3)) / alpha_u;	//r1 = p * (q1 - (u0*q3))/alpha_u;
    R.Row(1) = p * (q2 - (v0 * q3)) / alpha_v;	//r2 = p * (q2 - (v0*q3))/alpha_v;
    Vector<3, DataType> t;
    t[2] = p * P_(2, 3);
    t[0] = p * (P_(0, 3) - u0 * P_(2, 3)) / alpha_u;
    t[1] = p * (P_(1, 3) - v0 * P_(2, 3)) / alpha_v;

    Vector<3, DataType> center(-FastDot(t, R.Row(0)), -FastDot(t, R.Row(1)), -FastDot(t, R.Row(2)));

    Matrix<3, 3, DataType> M(q1, q2, q3);
    Matrix<3, 3, DataType> invM = M;
    invM.Invert();
    Vector<3, DataType> p4(P_(0, 3), P_(1, 3), P_(2, 3));
    Vector<3, DataType> principle_axis = M.Row(2);
    if (M.Determinant() < 0) principle_axis = -principle_axis;
    principle_axis.Normalize();

    // Check whether the determinant of the rotation of the extrinsic is positive one.
    if (R.Determinant() < 0) {
      Matrix<3, 3, DataType> K;
      K(0, 0) = K(1, 1) = alpha_u;
      K(0, 2) = u0;
      K(1, 2) = v0;
      K(2, 2) = 1;
      Matrix<3, 4, DataType> E;
      for (int r = 0, j = 0; r < 3; ++r) {
        for (int c = 0; c < 3; ++c, ++j) {
          E(r, c) = -R[j];
        }
      }
      E(0, 3) = -t[0];
      E(1, 3) = -t[1];
      E(2, 3) = -t[2];

      P_ = K * E;

      // Redo the decomposition.
      // computer internal and external parameters
      Vector<3, DataType> q1(P_.Row(0)), q2(P_.Row(1)), q3(P_.Row(2));

      Rotation<DataType> R;
      Vector<3, DataType> t;
      DataType p = DataType(1) / q3.Len();
      R.Row(2) = p * q3;	//r3 = p * q3;
      DataType u0 = p * p * FastDot(q1, q3);
      DataType v0 = p * p * FastDot(q2, q3);
      DataType alpha_u = Sqrt(p * p * FastDot(q1, q1) - u0 * u0);
      DataType alpha_v = Sqrt(p * p * FastDot(q2, q2) - v0 * v0);
      R.Row(0) = p * (q1 - (u0 * q3)) / alpha_u;	//r1 = p * (q1 - (u0*q3))/alpha_u;
      R.Row(1) = p * (q2 - (v0 * q3)) / alpha_v;	//r2 = p * (q2 - (v0*q3))/alpha_v;
      t[2] = p * P_(2, 3);
      t[0] = p * (P_(0, 3) - u0 * P_(2, 3)) / alpha_u;
      t[1] = p * (P_(1, 3) - v0 * P_(2, 3)) / alpha_v;

      Vector<3, DataType> center(-FastDot(t, R.Row(0)), -FastDot(t, R.Row(1)), -FastDot(t, R.Row(2)));

      Matrix<3, 3, DataType> M(q1, q2, q3);
      Matrix<3, 3, DataType> invM = M;
      invM.Invert();
      Vector<3, DataType> p4(P_(0, 3), P_(1, 3), P_(2, 3));
      Vector<3, DataType> principle_axis = M.Row(2);
      if (M.Determinant() < 0) principle_axis = -principle_axis;
      principle_axis.Normalize();
    }
    K_.Zero();
    K_(0, 0) = K_(1, 1) = alpha_u;
    K_(0, 2) = u0;
    K_(1, 2) = v0;
    K_(2, 2) = 1;
    MakeRT();
  }

  // Camera center
  // Implementation of table 6.1
  Vector<3, DataType> CameraCenter() const {
    Matrix<3, 3, DataType> M;
    Dress(M) = Dress(P_)(Colon(), Colon(0,2));
    Vector<3, DataType> p4;
    Dress(p4) = Dress(P_)(Colon(), 3);
    return -(M.Inverted()*p4);
  }

  Vector<3,DataType> PrincipalPoint() const {
    Matrix<3, 3, DataType> M;
    Dress(M) = Dress(P_)(Colon(),Colon(0,2));
    return M * M.Row(2);
  }

  Vector<3,DataType> PrincipalRay() const {
    Matrix<3, 3, DataType> M;
    Dress(M) = Dress(P_)(Colon(), Colon(0,2));
    return (M.Determinant() * M.Row(2)).Normalized();
  }

  Vector<4,DataType> PrincipalPlane() const {
    return P_.Row(2);
  }

  // rotation and translation
  Rotation<DataType> GetRotation() const {
    return Rotation<DataType>(R_.Inverted());
  }

  Translation<DataType> GetTranslation() const {
    return Translation<DataType>(-(R_.Inverted() * T_));
  }

  const Matrix<3,4,DataType>& P() const {return P_;}
  const Matrix<3,3,DataType>& R() const {return R_;}
  const Vector<3,DataType>& T() const {return T_;}
  const Matrix<3,3,DataType>& K() const {return K_;}

  const Vector<3,DataType> operator*(const VectorBase<4,DataType> &x) const {
    return P_*x;
  }

  const Vector<2,DataType> operator*(const VectorBase<3,DataType> &x) const {
    return FromHomogeneous(P_*ToHomogeneous(x));
  }
private:
  Matrix<3,4,DataType> P_;
  Matrix<3,3,DataType> R_;
  Vector<3,DataType> T_;
  Matrix<3,3,DataType> K_;
};

typedef ProjectionMat<double> ProjectionMatd;
typedef ProjectionMat<float> ProjectionMatf;
} // namespace zzz

