#pragma once
#include <zCore/common.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Math/Matrix3x3.hpp>
#include "../VisionTools.hpp"

namespace zzz {
class Affine {
public:
  static const int MIN_POINT_NUMBER = 3;

  Affine(){Hab_.Identical(); Hba_.Identical();}
  bool Create(const vector<Vector2d> &pa, const vector<Vector2d> &pb);
  Vector2d ToA(const Vector2d &pb) {
    return Vector2d(Hba_*ToHomogeneous(pb));
  }
  Vector2d ToB(const Vector2d &pb) {
    return Vector2d(Hab_*ToHomogeneous(pb));
  }
  Matrix3x3d GetHab(){return Hab_;}
  Matrix3x3d GetHba(){return Hba_;}
private:
  Matrix3x3d Hab_;
  Matrix3x3d Hba_;
};
}  // namespace zzz