#pragma once
#include <z3rd/Wrapper/ANNChar4Vector.hpp>
#include <zCore/common.hpp>
#include "SIFTKeys.hpp"

namespace zzz{
#ifdef ZZZ_LIB_ANN_CHAR
class SIFTMatcher
{
public:
  SIFTMatcher();
  SIFTMatcher(const SIFTKeys &keys);
  void Prepair(const SIFTKeys &keys);
  void Match(const SIFTKeys &keys);
  void Sort();

  vector<pair<zuint, zuint> > matches_;
  vector<double> dists_;
private:
  ANNChar4Vector<128> ann_;
  zuint keys1n_;
};
#endif
}

