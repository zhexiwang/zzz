#pragma once
#include <zCore/common.hpp>
#include <zImage/Image/Image.hpp>
#include <zImage/Image/ImageCoord.hpp>
#include <zGraphics/Graphics/ColorDefine.hpp>
#include <zCore/Utility/STLVector.hpp>

namespace zzz{
struct SIFTKey
{
  CoordX_Yf32 pos;
  zfloat32 scale;
  zfloat32 dir;
};

class SIFTKeys
{
public:
  static const int KEY_LEN = 128;

  SIFTKeys();
  explicit SIFTKeys(const string &filename);
  SIFTKeys(const SIFTKeys& other);
  const SIFTKeys& operator=(const SIFTKeys &other);
  void LoadKeyFile(const string &filename);
  void DrawOnImage(Image3uc &img, Colorf color=ColorDefine::white);

  STLVector<SIFTKey> keys_;
  STLVector<Vector<KEY_LEN, zuint8> > desc_;
};

SIMPLE_IOOBJECT(SIFTKey);

template<>
class IOObject<SIFTKeys> {
public:
  typedef SIFTKeys DataType;
  static const int RF_KEYS = 1;
  static const int RF_DESC = 2;
  static void WriteFileR(RecordFile &rf, const DataType &src) {
    IOObj::WriteFileR(rf, RF_KEYS, src.keys_);
    IOObj::WriteFileR(rf, RF_DESC, src.desc_);
  }
  static void ReadFileR(RecordFile &rf, DataType &dst) {
    IOObj::ReadFileR(rf, RF_KEYS, dst.keys_);
    IOObj::ReadFileR(rf, RF_DESC, dst.desc_);
  }
};
}
