#include "SIFTKeys.hpp"
#include <zImage/Image/ImageDrawer.hpp>

namespace zzz{
SIFTKeys::SIFTKeys(){}

SIFTKeys::SIFTKeys(const string &filename) {
  LoadKeyFile(filename);
}

SIFTKeys::SIFTKeys(const SIFTKeys& other) {
  *this=other;
}

void SIFTKeys::LoadKeyFile(const string &filename) {
  ifstream fi(filename.c_str());
  if (!fi.good()) {
    ZLOGE << "Cannot open file: " << filename << endl;
    return;
  }
  int keynum,keylen;
  fi >> keynum >> keylen;
  ZCHECK_EQ(keylen, KEY_LEN);
  keys_.assign(keynum,SIFTKey());
  desc_.assign(keynum,Vector<KEY_LEN,zuchar>());
  for (int i = 0; i < keynum; ++i)   {
    fi >> keys_[i].pos[1]
       >> keys_[i].pos[0]
       >> keys_[i].scale
       >> keys_[i].dir;
//    fi>>desc_[i];  //since >>zuchar will consider it as a char,,,
    for (zuint j = 0; j < 128; ++j) {
      int x;
      fi >> x;
      desc_[i][j] = x;
    }
  }
  fi.close();

}

void SIFTKeys::DrawOnImage(Image3uc &img, Colorf color) {
  Vector3uc c = color.ToColor<zuchar>().RGB();
  ImageDrawer<Vector3uc> drawer(img);
  for (zuint i = 0; i < keys_.size(); ++i) {
    drawer.DrawCircle(c, img.Rows()-1-keys_[i].pos[1], keys_[i].pos[0], keys_[i].scale);
    drawer.DrawLine(c,
                    img.Rows() - 1 - keys_[i].pos[1], keys_[i].pos[0],
                    img.Rows() - 1 - keys_[i].pos[1] + cos(keys_[i].dir) * keys_[i].scale,
                    keys_[i].pos[0] - sin(keys_[i].dir) * keys_[i].scale);
  }
}

const SIFTKeys& SIFTKeys::operator=(const SIFTKeys &other) {
  keys_ = other.keys_;
  desc_ = other.desc_;
  return *this;
}

} // namespace zzz
