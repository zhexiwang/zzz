include($$PWD/../../zzzlib.pri)

QT       -= core gui

TARGET = zVision$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    $$files($$PWD/zVision/Feature/*.cpp) \
    $$files($$PWD/zVision/Renderer/*.cpp) \
    $$files($$PWD/zVision/SfM/*.cpp) \
    $$files($$PWD/zVision/VisionAlgo/*.cpp) \
    $$files($$PWD/zVision/*.cpp)

HEADERS += \
    $$files($$PWD/zVision/Feature/*.hpp) \
    $$files($$PWD/zVision/Renderer/*.hpp) \
    $$files($$PWD/zVision/SfM/*.hpp) \
    $$files($$PWD/zVision/VisionAlgo/*.hpp) \
    $$files($$PWD/zVision/*.hpp)

OTHER_FILES += \

Z3RDENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZ3rd.pri) {
    error("Cannot include QTFlagsZ3rd.pri")
}

ZCOREENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZCore.pri) {
    error("Cannot include QTFlagsZCore.pri")
}

ZMATRIXENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZMatrix.pri) {
    error("Cannot include QTFlagsZMatrix.pri")
}

ZIMAGEENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZImage.pri) {
    error("Cannot include QTFlagsZImage.pri")
}

ZGRAPHICSENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZGraphics.pri) {
    error("Cannot include QTFlagsZGraphics.pri")
}

GLEWENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsGlew.pri) {
    error("Cannot include QTFlagsGlew.pri")
}


DEFINES = $$replace(DEFINES, ZETA, ZZZ_LIB)
DEFINES = $$replace(DEFINES, _ENABLE, )
message($$DEFINES)

message($$INCLUDEPATH)
