#pragma once

#ifndef ZZZ_NO_PRAGMA_LIB

#ifdef _DEBUG
#ifndef ZZZ_OS_WIN64
#pragma comment(lib,"z3rdD.lib")
#else
#pragma comment(lib,"z3rdD_x64.lib")
#endif // ZZZ_OS_WIN64
#else
#ifndef ZZZ_OS_WIN64
#pragma comment(lib,"z3rd.lib")
#else
#pragma comment(lib,"z3rd_x64.lib")
#endif // ZZZ_OS_WIN64
#endif

#endif  // ZZZ_NO_PRAGMA_LIB
