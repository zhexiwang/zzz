#include "CGALArrangement.hpp"
#ifdef ZZZ_LIB_CGAL

namespace zzz {
void CGALArrangement::Build(const vector<Vector2f>& points, const vector<Vector2i>& segments) {
  vector<Segment_2> segs;
  segs.reserve(segments.size());
  for (zuint i = 0; i < segments.size(); ++i) {
    const Vector2f &p0(points[segments[i][0]]), &p1(points[segments[i][1]]);
    segs.push_back(Segment_2(Point_2(p0[0], p0[1]), Point_2(p1[0], p1[1])));
  }
  CGAL::insert(arr, segs.begin(), segs.end());
}

void CGALArrangement::LocatePoints(const vector<Vector2f>& points, vector<Result>& ret) {
  // Perform a batched point-location query.
  std::vector<Point_2>       query_points;
  std::vector<Query_result>  results;

  query_points.reserve(points.size());
  map<Point_2, int> back_idx;
  for (zuint i = 0; i < points.size(); ++i) {
    Point_2 p(points[i][0], points[i][1]);
    back_idx[p] = query_points.size();
    query_points.push_back(p);
  }

  results.reserve(points.size());
  locate(arr, query_points.begin(), query_points.end(),
    std::back_inserter(results));

  // Print the results.
  Arrangement_2::Vertex_const_handle       v;
  Arrangement_2::Halfedge_const_handle     e;
  Arrangement_2::Face_const_handle         f;

  ret.resize(results.size());
  for (vector<Query_result>::const_iterator vi = results.begin();
    vi != results.end(); ++vi) {
      int idx = back_idx[vi->first];
      if (CGAL::assign (f, vi->second)) {
        // The current qeury point is located inside a face:
        if (f->is_unbounded())
          ret[idx] = make_pair(TYPE_FACE_UNBOUNDED, (int)f.ptr());
        else
          ret[idx] = make_pair(TYPE_FACE_BOUNDED, (int)f.ptr());
      } else if (CGAL::assign (e, vi->second)) {
        ret[idx] = make_pair(TYPE_LINE, (int)e.ptr());
      } else if (CGAL::assign (v, vi->second)) {
        if (v->is_isolated())
          ret[idx] = make_pair(TYPE_POINT_ISOLATED, (int)v.ptr());
        else
          ret[idx] = make_pair(TYPE_POINT, (int)v.ptr());
      }
  }
}
} // namespace zzz

#endif // ZZZ_LIB_CGAL
