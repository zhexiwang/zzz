#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_OPENCV
#ifdef ZZZ_COMPILER_MSVC
#pragma comment(lib, "comctl32.lib")
#endif
#ifdef ZZZ_DEBUG
#pragma comment(lib, "libjpegd.lib")
#pragma comment(lib, "libpngd.lib")
#pragma comment(lib, "libtiffd.lib")
#pragma comment(lib, "libjasperd.lib")
#pragma comment(lib, "IlmImfd.lib")
#pragma comment(lib, "zlibd.lib")
#pragma comment(lib, "opencv_calib3d246d.lib")
#pragma comment(lib, "opencv_contrib246d.lib")
#pragma comment(lib, "opencv_core246d.lib")
#pragma comment(lib, "opencv_features2d246d.lib")
#pragma comment(lib, "opencv_nonfree246d.lib")
//#pragma comment(lib, "opencv_ffmpeg246d.lib")
#pragma comment(lib, "opencv_flann246d.lib")
#pragma comment(lib, "opencv_gpu246d.lib")
#pragma comment(lib, "opencv_highgui246d.lib")
#pragma comment(lib, "opencv_imgproc246d.lib")
#pragma comment(lib, "opencv_legacy246d.lib")
#pragma comment(lib, "opencv_ml246d.lib")
#pragma comment(lib, "opencv_objdetect246d.lib")
#pragma comment(lib, "opencv_video246d.lib")
#else
#pragma comment(lib, "libjpeg.lib")
#pragma comment(lib, "libpng.lib")
#pragma comment(lib, "libtiff.lib")
#pragma comment(lib, "libjasper.lib")
#pragma comment(lib, "IlmImf.lib")
#pragma comment(lib, "zlib.lib")
#pragma comment(lib, "opencv_calib3d246.lib")
#pragma comment(lib, "opencv_contrib246.lib")
#pragma comment(lib, "opencv_core246.lib")
#pragma comment(lib, "opencv_features2d246.lib")
#pragma comment(lib, "opencv_nonfree246.lib")
//#pragma comment(lib, "opencv_ffmpeg246.lib")
#pragma comment(lib, "opencv_flann246.lib")
#pragma comment(lib, "opencv_gpu246.lib")
#pragma comment(lib, "opencv_highgui246.lib")
#pragma comment(lib, "opencv_imgproc246.lib")
#pragma comment(lib, "opencv_legacy246.lib")
#pragma comment(lib, "opencv_ml246.lib")
#pragma comment(lib, "opencv_objdetect246.lib")
#pragma comment(lib, "opencv_video246.lib")
#endif // ZZZ_DEBUG
#endif // ZZZ_LIB_OPENCV