#pragma once

#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_ZLIB
#ifdef ZZZ_OS_WIN64
#pragma comment(lib, "zlib_x64.lib")
#else
#pragma comment(lib, "zlib.lib")
#endif
#endif

#ifdef ZZZ_LIB_MRF
#ifdef ZZZ_DEBUG
#pragma comment(lib, "MultiLabelMRFD.lib")
#else
#pragma comment(lib, "MultiLabelMRF.lib")
#endif // ZZZ_DEBUG
#endif // ZZZ_LIB_GCO

#ifdef ZZZ_LIB_FAST
#ifdef ZZZ_DEBUG
#pragma comment(lib, "fastD.lib")
#else
#pragma comment(lib, "fast.lib")
#endif // ZZZ_DEBUG
#endif // ZZZ_LIB_FAST