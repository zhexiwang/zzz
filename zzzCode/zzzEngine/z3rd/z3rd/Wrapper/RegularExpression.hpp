#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_BOOST
#include <boost/regex.hpp>
#include <zCore/common.hpp>

namespace zzz{
class RegularExpression {
public:
  RegularExpression();
  RegularExpression(const string &pattern);
  void Compile(const string &pattern);
  bool Match(const string &str) const;
  bool Match(const string &str, vector<string> &res) const;
  int Search(const string &str, int start_pos=0) const;
  int Search(const string &str, vector<string> &res, int start_pos = 0) const;
  string Replace(const string &str, const string &rep) const;
  friend ostream& operator<<(ostream& os, const RegularExpression& me) {
    os << me.e.str();
    return os;
  }
private:
  boost::regex e;
};
}
#endif // ZZZ_LIB_BOOST

