#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_CGAL
#ifdef ZZZ_OS_WIN64
  #ifdef ZZZ_DEBUG
    #pragma comment(lib, "libCGAL-vc100-mt-gd-4.2_x64.lib")
    #pragma comment(lib, "libCGAL_CORE-vc100-mt-gd-4.2_x64.lib")
    #pragma comment(lib, "mpir_x64.lib")
  #else
    #pragma comment(lib, "libCGAL-vc100-mt-4.2_x64.lib")
    #pragma comment(lib, "libCGAL_CORE-vc100-mt-4.2_x64.lib")
    #pragma comment(lib, "mpir_x64.lib")
  #endif // ZZZ_DEBUG
#else
  #ifdef ZZZ_DEBUG
    #pragma comment(lib, "libCGAL-vc100-mt-gd-4.2.lib")
    #pragma comment(lib, "libCGAL_CORE-vc100-mt-gd-4.2.lib")
    #pragma comment(lib, "mpir.lib")
  #else
    #pragma comment(lib, "libCGAL-vc100-mt-4.2.lib")
    #pragma comment(lib, "libCGAL_CORE-vc100-mt-4.2.lib")
    #pragma comment(lib, "mpir.lib")
  #endif // ZZZ_DEBUG
#endif
#endif  // ZZZ_LIB_CGAL