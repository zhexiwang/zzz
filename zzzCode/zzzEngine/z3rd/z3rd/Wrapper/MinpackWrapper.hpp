#pragma once

#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_MINPACK
#pragma comment(lib, "cminpack.lib")
#endif // ZZZ_LIB_MINPACK