#include <z3rd/LibraryConfig.hpp>
#include <zCore/common.hpp>
#include <zCore/Utility/Log.hpp>
#ifdef ZZZ_LIB_YAML_CPP
#include <yaml-cpp/yaml.h>

#ifdef ZZZ_LIB_YAML_CPP
#ifdef ZZZ_OS_WIN64
  #ifdef ZZZ_DEBUG
  #pragma comment(lib, "yaml-cppD_X64.lib")
  #else
  #pragma comment(lib, "yaml-cpp_X64.lib")
  #endif // ZZZ_DEBUG
#else
  #ifdef ZZZ_DEBUG
  #pragma comment(lib, "yaml-cppD.lib")
  #else
  #pragma comment(lib, "yaml-cpp.lib")
  #endif // ZZZ_DEBUG
#endif // ZZZ_OS_WIN64
#endif  // ZZZ_LIB_YAML_CPP

namespace zzz {
bool ParseYAMLTo(const string &str, YAML::Node &doc);
}  // namespace zzz
#endif