#define ZCORE_SOURCE
#include "CGALPolygon.hpp"
#ifdef ZZZ_LIB_CGAL

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2_algorithms.h>
#include <iostream>
#include <zCore/Utility/TextProgressBar.hpp>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_2 Point;
using std::cout; using std::endl;

namespace zzz {
bool CGALPointInsidePolygon(const vector<Vector2f> &polygon, const Vector2f &p) {
  vector<Point> points;
  for (zuint i=0; i<polygon.size(); i++) {
    points.push_back(Point(polygon[i][0], polygon[i][1]));
  }
  switch(CGAL::bounded_side_2(points.begin(), points.end(), Point(p[0], p[1]), K())) {
  case CGAL::ON_BOUNDED_SIDE :
  case CGAL::ON_BOUNDARY:
    return true;
    break;
  case CGAL::ON_UNBOUNDED_SIDE:
    return false;
    break;
  }
  return false;
}

void CGALPointsInsidePolygon(const vector<Vector2f> &polygon, const vector<Vector2f> &p, vector<bool> &res) {
  vector<Point> points;
  for (zuint i=0; i<polygon.size(); i++) {
    points.push_back(Point(polygon[i][0], polygon[i][1]));
  }
  res.clear();
  res.reserve(p.size());
  {ScopeTextProgressBar bar("point in polygon", 0, p.size() - 1, 1);
  K k;
  for (zuint i=0; i<p.size(); i++) {
    bar.Update(i);
    switch(CGAL::bounded_side_2(points.begin(), points.end(), Point(p[i][0], p[i][1]), k)) {
    case CGAL::ON_BOUNDED_SIDE :
    case CGAL::ON_BOUNDARY:
      res.push_back(true);
      break;
    case CGAL::ON_UNBOUNDED_SIDE:
      res.push_back(false);
      break;
    }
  }}
}

bool CGALPolygonIsSimple(const vector<Vector2f> &polygon) {
  vector<Point> points;
  for (zuint i=0; i<polygon.size(); i++) {
    points.push_back(Point(polygon[i][0], polygon[i][1]));
  }
  return CGAL::is_simple_2(points.begin(), points.end(), K());
}

bool CGALAllPointsInsidePolygon(const vector<Vector2f> &polygon, const vector<Vector2f> &p) { vector<bool> res;
  CGALPointsInsidePolygon(polygon, p, res);
  for (zuint i = 0; i < res.size(); ++i) {
    if (res[i] = false) return false;
  }
  return true;
}

};  // namespace zzz

#endif // ZZZ_LIB_CGAL
