#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_FFTW
#include <zCore/Math/Array2.hpp>
#include <zCore/Math/Complex.hpp>
#include <fftw3.h>

#pragma comment(lib, "libfftw3-3.lib")

namespace zzz{
//can only shift back even*even matrix
//if rows or cols is odd, can only shift once
inline void FFTShift(const Array<2,Complex<double> > &oricoeff, Array<2,Complex<double> > &shiftcoeff)
{
  int rows=oricoeff.Size(0),cols=oricoeff.Size(1);
  //rearrange
  shiftcoeff.SetSize(oricoeff.Size());
  for (int r=0; r<rows; r++) for (int c=0; c<cols; c++)
  {
    int rr=(r+rows-rows/2)%rows,cc=(c+cols-cols/2)%cols;
    shiftcoeff[Vector2i(r, c)]=oricoeff[Vector2i(rr,cc)];
  }
}

inline void FFT2(const Array<2,double> &_data, Array<2,Complex<double> > &coeff) {
  Array<2,double> data(_data);
  int rows=data.Size(0),cols=data.Size(1);
  fftw_complex *out=(fftw_complex*)fftw_malloc(sizeof(fftw_complex)*rows*(cols/2+1));
  fftw_plan fft=fftw_plan_dft_r2c_2d(rows,cols,data.Data(),out,FFTW_PRESERVE_INPUT|FFTW_ESTIMATE);
  fftw_execute(fft);

  //expand
  coeff.SetSize(data.Size());
  for (int r=0; r<rows; r++) for (int c=0; c<cols/2+1; c++) {
    int pos=r*(cols/2+1)+c;
    coeff[Vector2i(r, c)][0]=out[pos][0];
    coeff[Vector2i(r, c)][1]=out[pos][1];
  }
  for (int r=0; r<rows; r++) for (int c=cols/2+1; c<cols; c++) {
    int rr=(rows-r)%rows,cc=(cols-c)%cols;
    int pos=rr*(cols/2+1)+cc;
    coeff[Vector2i(r, c)][0]=out[pos][0];
    coeff[Vector2i(r, c)][1]=-out[pos][1];
  }

  fftw_destroy_plan(fft);
  fftw_free(out);
}


inline void IFFT2(const Array<2,Complex<double> > &coeff, Array<2,double> &inv_data) {
  int rows=coeff.Size(0),cols=coeff.Size(1);
  inv_data.SetSize(coeff.Size());
  fftw_complex *in=(fftw_complex*)fftw_malloc(sizeof(fftw_complex)*rows*(cols/2+1));
  fftw_plan ifft=fftw_plan_dft_c2r_2d(rows,cols,in,inv_data.Data(),FFTW_ESTIMATE);

  for (int r=0; r<rows; r++) for (int c=0; c<cols/2+1; c++) {
    int pos=r*(cols/2+1)+c;
    in[pos][0]=coeff[Vector2i(r, c)][0];
    in[pos][1]=coeff[Vector2i(r, c)][1];
  }

  fftw_execute(ifft);
  fftw_destroy_plan(ifft);
  fftw_free(in);
  inv_data/=rows*cols;
}

//can only shift back even vector
inline void FFTShift(const vector<Complex<double> > &oricoeff, vector<Complex<double> > &shiftcoeff) {
  int size=oricoeff.size();
  //rearrange
  shiftcoeff.assign(size,Complexd(0, 0));
  for (int i=0; i<size; i++) {
    int ii=(i+size-size/2)%size;
    shiftcoeff[i]=oricoeff[ii];
  }
}

inline void FFT(vector<double> &data, vector<Complex<double> > &coeff) {
  int size=data.size();
  fftw_complex *out=(fftw_complex*)fftw_malloc(sizeof(fftw_complex)*(size/2+1));
  fftw_plan fft=fftw_plan_dft_r2c_1d(size, &(data[0]),out,FFTW_PRESERVE_INPUT|FFTW_ESTIMATE);
  fftw_execute(fft);

  //expand
  coeff.assign(size,Complexd(0, 0));
  for (int i=0; i<size/2+1; i++) {
    coeff[i][0]=out[i][0];
    coeff[i][1]=out[i][1];
  }
  for (int i=size/2+1; i<size; i++) {
    int ii=(size-i)%size;
    coeff[i][0]=out[ii][0];
    coeff[i][1]=-out[ii][1];
  }

  fftw_destroy_plan(fft);
  fftw_free(out);
}


inline void IFFT(const vector<Complex<double> > &coeff, vector<double> &inv_data) {
  int size=coeff.size();
  inv_data.assign(size, 0);
  fftw_complex *in=(fftw_complex*)fftw_malloc(sizeof(fftw_complex)*(size/2+1));
  fftw_plan ifft=fftw_plan_dft_c2r_1d(size,in, &(inv_data[0]),FFTW_ESTIMATE);

  for (int i=0; i<size/2+1; i++) {
    in[i][0]=coeff[i][0];
    in[i][1]=coeff[i][1];
  }

  fftw_execute(ifft);
  fftw_destroy_plan(ifft);
  fftw_free(in);
  for (zuint i=0; i<inv_data.size(); i++)
    inv_data[i]/=size;
}

}
#endif // ZZZ_LIB_FFTW

