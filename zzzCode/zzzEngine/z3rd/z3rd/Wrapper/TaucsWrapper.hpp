#pragma once

#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_TAUCS
#pragma comment(lib, "libtaucs.lib")
#pragma comment(lib, "libmetis.lib")
#pragma comment(lib, "libf77blas.lib")
#pragma comment(lib, "clapack.lib")
#pragma comment(lib, "libf2c.lib")      // fortune to c
#pragma comment(lib, "cblaswrap.lib")   // f2c_blas to c_blas
#pragma comment(lib, "f77blaswrap.lib")   // f2c_blas to f77_blas
//#pragma comment(lib, "BLAS.lib")      // original blas, use this or following atlas
#pragma comment(lib, "libcblas.lib")    // cblas to atlas implementation
#pragma comment(lib, "libatlas.lib")    // atlas implementation

#endif  // ZZZ_LIB_TAUCS