#pragma once
#include <z3rd/Wrapper/OpenCVWrapper.hpp>

#ifdef ZZZ_LIB_OPENCV
#include <opencv2/opencv.hpp>
#include <zCore/Math/Array2.hpp>
#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Math/Vector4.hpp>

namespace zzz {
template<typename T> struct CVType {};
template<> struct CVType<zint8>             {enum {TYPE = CV_8SC1};};
template<> struct CVType<Vector<2, zint8> > {enum {TYPE = CV_8SC2};};
template<> struct CVType<Vector<3, zint8> > {enum {TYPE = CV_8SC3};};
template<> struct CVType<Vector<4, zint8> > {enum {TYPE = CV_8SC4};};

template<> struct CVType<zuint8>             {enum {TYPE = CV_8UC1};};
template<> struct CVType<Vector<2, zuint8> > {enum {TYPE = CV_8UC2};};
template<> struct CVType<Vector<3, zuint8> > {enum {TYPE = CV_8UC3};};
template<> struct CVType<Vector<4, zuint8> > {enum {TYPE = CV_8UC4};};

template<> struct CVType<zint16>             {enum {TYPE = CV_16SC1};};
template<> struct CVType<Vector<2, zint16> > {enum {TYPE = CV_16SC2};};
template<> struct CVType<Vector<3, zint16> > {enum {TYPE = CV_16SC3};};
template<> struct CVType<Vector<4, zint16> > {enum {TYPE = CV_16SC4};};

template<> struct CVType<zuint16>             {enum {TYPE = CV_16UC1};};
template<> struct CVType<Vector<2, zuint16> > {enum {TYPE = CV_16UC2};};
template<> struct CVType<Vector<3, zuint16> > {enum {TYPE = CV_16UC3};};
template<> struct CVType<Vector<4, zuint16> > {enum {TYPE = CV_16UC4};};

template<> struct CVType<zint32>             {enum {TYPE = CV_32SC1};};
template<> struct CVType<Vector<2, zint32> > {enum {TYPE = CV_32SC2};};
template<> struct CVType<Vector<3, zint32> > {enum {TYPE = CV_32SC3};};
template<> struct CVType<Vector<4, zint32> > {enum {TYPE = CV_32SC4};};

template<> struct CVType<zfloat32>             {enum {TYPE = CV_32FC1};};
template<> struct CVType<Vector<2, zfloat32> > {enum {TYPE = CV_32FC2};};
template<> struct CVType<Vector<3, zfloat32> > {enum {TYPE = CV_32FC3};};
template<> struct CVType<Vector<4, zfloat32> > {enum {TYPE = CV_32FC4};};

template<> struct CVType<zfloat64>             {enum {TYPE = CV_64FC1};};
template<> struct CVType<Vector<2, zfloat64> > {enum {TYPE = CV_64FC2};};
template<> struct CVType<Vector<3, zfloat64> > {enum {TYPE = CV_64FC3};};
template<> struct CVType<Vector<4, zfloat64> > {enum {TYPE = CV_64FC4};};

template<typename T>
void CVMatToArray(const cv::Mat &src, Array<2, T> &dst) {
  ZCHECK_EQ(CVType<T>::TYPE, src.type()) << "cv::mat and Array types do not match.\n";
  dst.SetSize(src.rows, src.cols);
  dst.SetData((T*)src.ptr());
}

template<typename T>
void ArrayToCVMat(const Array<2, T> &src, cv::Mat &dst) {
  dst.create(src.Size(0), src.Size(1), CVType<T>::TYPE);
  memcpy(dst.ptr(), src.Data(), src.size() * sizeof(T));
}

} // namespace zzz
#endif // ZZZ_LIB_OPENCV