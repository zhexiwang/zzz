#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_GCO
#include <gco/graph.h>

namespace zzz{
template<typename T>
class MaxFlow
{
public:
  MaxFlow():graph_(NULL){}
  ~MaxFlow()
  {
    if (graph_)
      delete graph_;
  }
  typedef Graph<T, T, T> GraphType;

  //[i, j].first is weight from i to j
  //[j, i].first is weight from j to i
  typedef vector< pair< pair<zuint,zuint>,pair<T, T> > > NLinks;

  //i-item.first: from source to i
  //i-item.second: from i to sink
  typedef vector<pair<T, T> > TLinks;

  //return max flow
  T CalMaxFlow(const NLinks &nlinks, const TLinks &tlinks)
  {
    if (graph_) delete graph_;
    graph_ = new GraphType(tlinks.size(), nlinks.size());

    zuint noden=tlinks.size();

    for (zuint i=0; i<noden; i++)
      graph_->add_node();
    for (zuint i=0; i<noden; i++)
      graph_->add_tweights(i,tlinks[i].first,tlinks[i].second);

    for (zuint i=0; i<nlinks.size(); i++)
      graph_->add_edge(nlinks[i].first.first,nlinks[i].first.second,nlinks[i].second.first,nlinks[i].second.second);

    T flow=graph_->maxflow();
    return flow;
  }

  //segementation
  bool InSourceSet(int n)
  {
    return graph_->what_segment(n)==GraphType::SOURCE;
  }
  bool InSinkSet(int n)
  {
    return graph_->what_segment(n)==GraphType::SINK;
  }

  GraphType *graph_;
};

}
#endif // ZZZ_LIB_MAXFLOW

