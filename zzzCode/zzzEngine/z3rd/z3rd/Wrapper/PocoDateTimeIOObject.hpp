#pragma once
#include <z3rd/Wrapper/PocoFoundationWrapper.hpp>
#include <zCore/Utility/IOObject.hpp>

#ifdef ZZZ_LIB_POCO

namespace zzz {
SIMPLE_IOOBJECT(Timestamp);
SIMPLE_IOOBJECT(DateTime);
SIMPLE_IOOBJECT(LocalDateTime);
SIMPLE_IOOBJECT(Timespan);
};  // namespace zzz

#endif  // ZZZ_LIB_POCO