#pragma once
#include <z3rd/LibraryConfig.hpp>
#ifdef ZZZ_LIB_POCO

#include <Poco/Net/ServerSocket.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTMLForm.h>
#include <zCore/Utility/IOObject.hpp>

#ifdef ZZZ_OS_WIN64
  #ifdef ZZZ_DEBUG
  #pragma comment(lib, "PocoNetD_X64.lib")
  #else
  #pragma comment(lib, "PocoNetX64.lib")
  #endif // ZZZ_DEBUG
#else
  #ifdef ZZZ_DEBUG
  #pragma comment(lib, "PocoNetD.lib")
  #else
  #pragma comment(lib, "PocoNet.lib")
  #endif // ZZZ_DEBUG
#endif // ZZZ_OS_WIN64

namespace zzz {
using Poco::Net::ServerSocket;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerParams;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTMLForm;

// Shortcut for HTTPResponse
inline void ReplyText(HTTPServerResponse &rsp, const string &str) {
  rsp.setStatus(HTTPResponse::HTTP_OK);
  rsp.setContentType("text/plain");
  std::ostream &os = rsp.send();
  os << str;
  os.flush();
}
inline void ReplyHTML(HTTPServerResponse &rsp, const string &str) {
  rsp.setStatus(HTTPResponse::HTTP_OK);
  rsp.setContentType("text/html");
  std::ostream &os = rsp.send();
  os << str;
  os.flush();
}
inline void ReplySpecial(HTTPServerResponse &rsp, const HTTPResponse::HTTPStatus &status) {
  rsp.setStatus(status);
  rsp.setContentType("text/html");
  std::ostream &os = rsp.send();
  switch (status) {
  case HTTPResponse::HTTP_NOT_FOUND:
    os << "<html><body><h1>Error 404 NOT_FOUND</h1></body></html>"; break;
  case HTTPResponse::HTTP_INTERNAL_SERVER_ERROR:
    os << "<html><body><h1>Error 500 INTERNAL_SERVER_ERROR</h1></body></html>"; break;
  default:
    os << "<html><body><h1>Error " << status << "</h1></body></html>";
  }
  os.flush();
}
};  // namespace zzz

#endif  // ZZZ_LIB_POCO