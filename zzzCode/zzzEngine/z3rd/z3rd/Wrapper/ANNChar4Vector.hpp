#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_ANN_CHAR
#include <zCore/Math/Vector.hpp>
#include <ANN/ANN_char.h>
#include <zCore/Utility/Timer.hpp>

#pragma comment(lib, "ann_1.1_char.lib")

namespace zzz{
template<int N>
class ANNChar4Vector
{
public:
  ANNChar4Vector()
    :tree_(NULL),dataPts(NULL),queryPt(NULL){}
  ANNChar4Vector(const vector<Vector<N,zuchar> > &data)
    :tree_(NULL),dataPts(NULL),queryPt(NULL)
  {
    SetData(data);
  }
  ~ANNChar4Vector()
  {
    Clear();
  }

  void Clear()
  {
    if (dataPts) ann_1_1_char::annDeallocPts(dataPts);
    dataPts=NULL;
    if (tree_) delete tree_;
    tree_=NULL;
  }

  void SetData(const vector<Vector<N,zuchar> > &data)
  {
    Clear();
    if (data.empty()) return;
    dataPts=ann_1_1_char::annAllocPts(data.size(), N);
    IOObject<zuchar>::CopyData(dataPts[0],data[0].Data(),sizeof(zuchar)*N*data.size());
    tree_=new ann_1_1_char::ANNkd_tree(dataPts,data.size(), N,16);
  }

  void Query(const Vector<N,zuchar> &pt, int k, vector<int> &idx, vector<int> &dist)
  {
    idx.assign(k, 0);
    dist.assign(k, 0.0);
    tree_->annkPriSearch((ann_1_1_char::ANNpoint)pt.Data(), k, &(idx[0]), &(dist[0]));
  }

  //return number of points lies inside the region
  //set k=0 to just query the number
  int RangeQuery(const Vector<N,zuchar> &pt, double radius, int k, vector<int> &idx, vector<double> &dist)
  {
    if (k==0) return tree_->annkFRSearch((ann_1_1_char::ANNpoint)pt.Data(),radius*radius, 0,NULL,NULL);
    idx.assign(k, 0);
    dist.assign(k, 0.0);
    return tree_->annkFRSearch(
                static_cast<ann_1_1_char::ANNcoord*>(pt.Data()),
                static_cast<ann_1_1_char::ANNdist>(radius*radius),
                k,
                static_cast<ann_1_1_char::ANNidx*>(&(idx[0])),
                static_cast<ann_1_1_char::ANNdist*>(&(dist[0])),
                0.0);
  }

  static void SetMaxPointVisit(int maxPts)
  {
    ann_1_1_char::annMaxPtsVisit(maxPts);
  }
private:
  ann_1_1_char::ANNkd_tree *tree_;
  ann_1_1_char::ANNpointArray dataPts;
  ann_1_1_char::ANNpoint queryPt;
};
}
#endif // ZZZ_LIB_ANN_CHAR
