#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_ANN
#include <zCore/Math/Vector.hpp>
#include <ANN/ANN.h>

#ifdef ZZZ_OS_WIN64
  #pragma comment(lib, "ann_x64.lib")
#else
  #pragma comment(lib, "ann.lib")
#endif

namespace zzz{
template<int N, typename T>
class ANN4Vector {
public:
  ANN4Vector()
    :tree_(NULL),dataPts(NULL){}
  ANN4Vector(const vector<Vector<N, T> > &data)
    :tree_(NULL),dataPts(NULL) {
    SetData(data);
  }
  ~ANN4Vector() {
    Clear();
  }

  bool IsValid() {return tree_ != NULL;}

  void Clear() {
    if (dataPts) annDeallocPts(dataPts);
    dataPts=NULL;
    if (tree_) delete tree_;
    tree_=NULL;
  }

  void Build(const vector<Vector<N, T> > &data) {
    ZCHECK_FALSE(data.empty()) << "You use non point set to build ANN!";
    Clear();
    dataPts=annAllocPts(data.size(), N);
    for (zuint i=0; i<data.size(); i++) for (zuint j=0; j<N; j++)
      dataPts[i][j]=data[i][j];
    ZLOGV<<"Create ANN with "<<data.size()<<" points.\n";
    Timer timer;
    tree_=new ANNkd_tree(dataPts,data.size(), N);
    ZLOGV<<"Done in "<<timer.Elapsed()<<" seconds.\n";
  }

  void Query(const Vector<N, T> &pt, int k, vector<int> &idx, vector<double> &dist) {
    idx.assign(k, -1);
    dist.assign(k, -1);
    Vector<N,double> ptd(pt);
    tree_->annkSearch(static_cast<ANNpoint>(ptd.Data()), k, &(idx[0]), &(dist[0]));
  }
  
  //return number of points lies inside the region
  //set k=0 to just query the number
  int RangeQuery(const Vector<N, T> &pt, double radius, int k, vector<int> &idx, vector<double> &dist) {
    Vector<N,double> ptd(pt);
    if (k==0) return tree_->annkFRSearch(static_cast<ANNpoint>(ptd.Data()), radius*radius, 0, NULL, NULL);
    idx.assign(k, 0);
    dist.assign(k, 0.0);
    return tree_->annkFRSearch(static_cast<ANNpoint>(ptd.Data()),radius*radius, k, &(idx[0]), &(dist[0]));
  }

  static void SetMaxPointVisit(int maxPts) {
    annMaxPtsVisit(maxPts);
  }

private:
  ANNkd_tree *tree_;
  ANNpointArray dataPts;
};
}
#endif // ZZZ_LIB_ANN


