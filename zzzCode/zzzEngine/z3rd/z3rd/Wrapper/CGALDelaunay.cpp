#define ZCORE_SOURCE
#include "CGALDelaunay.hpp"
#ifdef ZZZ_LIB_CGAL
#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/common.hpp>
#include "CGALPolygon.hpp"

//#define _USE_MATH_DEFINES
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_euclidean_traits_xy_3.h>
#include <CGAL/Triangulation_hierarchy_2.h>

#include <CGAL/Triangulation_2.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/point_generators_2.h>
#include <CGAL/algorithm.h>
#include <zCore/Utility/TextProgressBar.hpp>

struct K : CGAL::Exact_predicates_inexact_constructions_kernel {
};
typedef CGAL::Triangulation_vertex_base_2<K>              Vbb;
typedef CGAL::Triangulation_hierarchy_vertex_base_2<Vbb>  Vb;
typedef CGAL::Triangulation_face_base_2<K>                Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb>       Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds>             Dt;
typedef CGAL::Triangulation_hierarchy_2<Dt>               Triangulation;
typedef Triangulation::Point                              Point;
typedef Triangulation::Vertex_circulator                  Vertex_circulator;
typedef Triangulation::Vertex_handle                      Vertex_handle;
typedef Triangulation::Triangle                           Triangle;
typedef Triangulation::Finite_faces_iterator              Face_Iterator;

typedef CGAL::Constrained_triangulation_face_base_2<K>    CFb;
typedef CGAL::Triangulation_data_structure_2<Vb,CFb>      CTds;
typedef CGAL::Constrained_Delaunay_triangulation_2<K, CTds>  CDt;
typedef CGAL::Triangulation_hierarchy_2<CDt>                 CTriangulation;
typedef CTriangulation::Point                     CPoint;
typedef CTriangulation::Vertex_circulator         CVertex_circulator;
typedef CTriangulation::Vertex_handle             CVertex_handle;
typedef CTriangulation::Triangle                  CTriangle;
typedef CTriangulation::Finite_faces_iterator     CFace_Iterator;


namespace zzz {
void CGALDelaunayTriangulate(const vector<Vector2f> &points, vector<Vector3i> &triangles) {
  Triangulation dt;
  map<Triangulation::Vertex_handle, int> vLookup;
  {ScopeTextProgressBar bar("CGAL Delaunay", 0, points.size() - 1, 3);
  for (zuint i=0; i<points.size(); i++) {
    bar.Update(i);
    vLookup[dt.insert(Point(points[i][0], points[i][1]))]=i;
  }}
  triangles.clear();
  for (Triangulation::Face_iterator fi = dt.faces_begin(); fi != dt.faces_end(); ++fi) {
    triangles.push_back(Vector3i(vLookup[fi->vertex(0)], vLookup[fi->vertex(1)], vLookup[fi->vertex(2)]));
  }
}

void CGALConstrainedDelaunayTriangulate(const vector<Vector2f> &points, const vector<Vector2i> &constraint, vector<Vector3i> &triangles) {
  CTriangulation cdt;
  map<CTriangulation::Vertex_handle, int> vLookup;
  vector<CTriangulation::Vertex_handle> v_handles;
  {ScopeTextProgressBar bar("CGAL ConstrainedDelaunay", 0, points.size() - 1, 2);
  for (zuint i=0; i<points.size(); i++) {
    bar.Update(i);
    v_handles.push_back(cdt.insert(CPoint(points[i][0], points[i][1])));
    vLookup[v_handles.back()]=i;
  }}
  for (zuint i = 0; i < constraint.size(); ++i) {
    cdt.insert_constraint(v_handles[constraint[i][0]], v_handles[constraint[i][1]]);
  }
  triangles.clear();
  for (CTriangulation::Face_iterator fi = cdt.faces_begin(); fi != cdt.faces_end(); ++fi) {
    triangles.push_back(Vector3i(vLookup[fi->vertex(0)], vLookup[fi->vertex(1)], vLookup[fi->vertex(2)]));
  }
}

void CGALPointLocationInTriangulation(const vector<Vector2f> &points, const vector<Vector2f> &check_points, vector<Vector3i> &locations) {
  Triangulation dt;
  map<Triangulation::Vertex_handle, int> vLookup;
  {ScopeTextProgressBar bar("CGAL Delaunay", 0, points.size() - 1, 2);
  for (zuint i=0; i<points.size(); i++) {
    bar.Update(i);
    vLookup[dt.insert(Point(points[i][0], points[i][1]))]=i;
  }}

  locations.clear();
  {ScopeTextProgressBar bar("Point Locate", 0, check_points.size() - 1, 2);
  for (zuint i = 0; i < check_points.size(); ++i) {
    bar.Update(i);
    Triangulation::Face_handle fh = dt.locate(Point(check_points[i][0], check_points[i][1]));
    if (fh == Triangulation::Face_handle())
      locations.push_back(Vector3i(-1));
    else
      locations.push_back(Vector3i(vLookup[fh->vertex(0)], vLookup[fh->vertex(1)], vLookup[fh->vertex(2)]));
  }}
}

ZCORE_FUNC void CGALPolygonTriangulation(const vector<Vector2f> &polygon, vector<Vector3i> &tri) {
  vector<Vector2i> constraints;
  for (zuint i = 0; i < polygon.size(); ++i)
    constraints.push_back(Vector2i(i, (i + 1) % polygon.size()));
  vector<Vector3i> all_tri;
  //CGALConstrainedDelaunayTriangulate(polygon, constraints, all_tri);
  CGALDelaunayTriangulate(polygon, all_tri);
  vector<Vector2f> centers;
  centers.reserve(all_tri.size());
  for (zuint i = 0; i < all_tri.size(); ++i) {
    const Vector3i &t = all_tri[i];
    centers.push_back((polygon[t[0]] + polygon[t[1]] + polygon[t[2]]) / 3.0f);
  }
  vector<bool> inside;
  CGALPointsInsidePolygon(polygon, centers, inside);
  tri.clear();
  tri.reserve(all_tri.size());
  for (zuint i = 0; i < inside.size(); ++i) {
    if (inside[i]) tri.push_back(all_tri[i]);
  }
}

ZCORE_FUNC void CGALPolygonTriangulationStrict(const vector<Vector2f> &polygon, vector<Vector3i> &tri) {
  vector<Vector2i> constraints;
  for (zuint i = 0; i < polygon.size(); ++i)
    constraints.push_back(Vector2i(i, (i + 1) % polygon.size()));
  vector<Vector3i> all_tri;
  CGALConstrainedDelaunayTriangulate(polygon, constraints, all_tri);
  vector<Vector2f> centers;
  centers.reserve(all_tri.size());
  for (zuint i = 0; i < all_tri.size(); ++i) {
    const Vector3i &t = all_tri[i];
    centers.push_back((polygon[t[0]] + polygon[t[1]] + polygon[t[2]]) / 3.0f);
  }
  vector<bool> inside;
  CGALPointsInsidePolygon(polygon, centers, inside);
  tri.clear();
  tri.reserve(all_tri.size());
  for (zuint i = 0; i < inside.size(); ++i) {
    if (inside[i]) tri.push_back(all_tri[i]);
  }
}
};  // namespace zzz

#endif
