#pragma once


#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_BOOST
  #ifdef ZZZ_OS_WIN64
  #else
    #ifdef ZZZ_DEBUG
      #pragma comment(lib, "libboost_program_options-vc100-mt-gd-1_51.lib")
      #pragma comment(lib, "libboost_chrono-vc100-mt-gd-1_51.lib")
      #pragma comment(lib, "libboost_thread-vc100-mt-gd-1_51.lib")
      #pragma comment(lib, "libboost_filesystem-vc100-mt-gd-1_51.lib")
      #pragma comment(lib, "libboost_system-vc100-mt-gd-1_51.lib")
      #pragma comment(lib, "libboost_regex-vc100-mt-gd-1_51.lib")
      #pragma comment(lib, "libboost_locale-vc100-mt-gd-1_51.lib")
    #else
      #pragma comment(lib, "libboost_program_options-vc100-mt-1_51.lib")
      #pragma comment(lib, "libboost_chrono-vc100-mt-1_51.lib")
      #pragma comment(lib, "libboost_thread-vc100-mt-1_51.lib")
      #pragma comment(lib, "libboost_filesystem-vc100-mt-1_51.lib")
      #pragma comment(lib, "libboost_system-vc100-mt-1_51.lib")
      #pragma comment(lib, "libboost_regex-vc100-mt-1_51.lib")
      #pragma comment(lib, "libboost_locale-vc100-mt-1_51.lib")
    #endif
  #endif
#endif
