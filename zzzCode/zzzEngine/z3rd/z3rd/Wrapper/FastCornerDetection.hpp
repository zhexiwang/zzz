#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_FAST
#include <fast.h>

namespace zzz {
void Fast9Detect(const Imageuc &_img, vector<Vector2i> &_corners, int _threshold = 20)
{
  int ncorners;
  xy* corners = fast9_detect_nonmax(_img.Data(), _img.Cols(), _img.Rows(), _img.Cols(), _threshold, &ncorners);
  _corners.clear();
  _corners.reserve(ncorners);
  for (int i=0; i<ncorners; i++)
    _corners.push_back(Vector2i(corners[i].y, corners[i].x));
  free(corners);
}
void Fast10Detect(const Imageuc &_img, vector<Vector2i> &_corners, int _threshold = 20)
{
  int ncorners;
  xy* corners = fast10_detect_nonmax(_img.Data(), _img.Cols(), _img.Rows(), _img.Cols(), _threshold, &ncorners);
  _corners.clear();
  _corners.reserve(ncorners);
  for (int i=0; i<ncorners; i++)
    _corners.push_back(Vector2i(corners[i].y, corners[i].x));
  free(corners);
}
void Fast11Detect(const Imageuc &_img, vector<Vector2i> &_corners, int _threshold = 20)
{
  int ncorners;
  xy* corners = fast11_detect_nonmax(_img.Data(), _img.Cols(), _img.Rows(), _img.Cols(), _threshold, &ncorners);
  _corners.clear();
  _corners.reserve(ncorners);
  for (int i=0; i<ncorners; i++)
    _corners.push_back(Vector2i(corners[i].y, corners[i].x));
  free(corners);
}
void Fast12Detect(const Imageuc &_img, vector<Vector2i> &_corners, int _threshold = 20)
{
  int ncorners;
  xy* corners = fast12_detect_nonmax(_img.Data(), _img.Cols(), _img.Rows(), _img.Cols(), _threshold, &ncorners);
  _corners.clear();
  _corners.reserve(ncorners);
  for (int i=0; i<ncorners; i++)
    _corners.push_back(Vector2i(corners[i].y, corners[i].x));
  free(corners);
}
};  // namespace zzz

#endif