#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_BOOST
#pragma warning(push)
#pragma warning(disable:4180)//qualifier applied to function type has no meaning; ignored, for boost::signals2
#include <boost/signals2.hpp>
#pragma warning(pop)
#endif // ZZZ_LIB_BOOST