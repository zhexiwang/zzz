#pragma once

#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_PTHREAD
#pragma comment(lib, "pthreadVC2.lib")
#endif  // ZZZ_LIB_PTHREAD
