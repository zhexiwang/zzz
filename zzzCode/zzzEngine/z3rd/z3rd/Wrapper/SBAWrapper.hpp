#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_SBA
#include <sba/sba.h>

#ifdef ZZZ_OS_WIN64
  #ifdef ZZZ_DEBUG
  #pragma comment(lib, "sbaD_X64.lib")
  #else
  #pragma comment(lib, "sba_X64.lib")
  #endif // ZZZ_DEBUG
#else
  #ifdef ZZZ_DEBUG
  #pragma comment(lib, "sbaD.lib")
  #else
  #pragma comment(lib, "sba.lib")
  #endif // ZZZ_DEBUG
#endif // ZZZ_OS_WIN64
#endif // ZZZ_LIB_SBA
