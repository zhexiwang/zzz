#include "RegularExpression.hpp"

namespace zzz {
RegularExpression::RegularExpression(){}
RegularExpression::RegularExpression(const string &pattern) {
  Compile(pattern);
}
void RegularExpression::Compile(const string &pattern) {
  e=pattern;
}
bool RegularExpression::Match(const string &str) const {
  boost::cmatch what;
  bool ret=regex_match(str.c_str(),what, e);
  if (!ret) return false;
  return true;
}
bool RegularExpression::Match(const string &str, vector<string> &res) const {
  boost::cmatch what;
  bool ret=regex_match(str.c_str(),what, e);
  if (!ret) return false;
  res.clear();
  for (zuint i=1; i<what.size(); i++)
    res.push_back(string(what[i].first,what[i].second));
  return true;
}
int RegularExpression::Search(const string &str, int start_pos/*=0*/) const {
  boost::match_results<std::string::const_iterator> what;
  boost::match_flag_type flags = boost::match_default; 
  bool ret=regex_search(str.begin()+start_pos, str.end(), what, e, flags);
  if (!ret) return -1;
  return what[0].first-str.begin();
}
int RegularExpression::Search(const string &str, vector<string> &res, int start_pos/*=0*/) const {
  boost::match_results<std::string::const_iterator> what;
  boost::match_flag_type flags = boost::match_default; 
  bool ret=regex_search(str.begin()+start_pos, str.end(), what, e, flags);
  if (!ret) return -1;

  res.clear();
  for (zuint i=1; i<what.size(); i++)
    res.push_back(string(what[i].first,what[i].second));

  return what[0].first-str.begin();
}
string RegularExpression::Replace(const string &str, const string &rep) const {
  return regex_replace(str, e, rep, boost::match_default | boost::format_all);
}
};  // namespace zzz