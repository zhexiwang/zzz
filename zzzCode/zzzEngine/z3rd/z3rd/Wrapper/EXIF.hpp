#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_EXIF
#include <zCore/common.hpp>
#include <exiv2/image.hpp>
#include <exiv2/exif.hpp>

#ifdef ZZZ_DEBUG
#pragma comment(lib, "exiv2D.lib")
#pragma comment(lib, "libexpatD.lib")
#pragma comment(lib, "xmpsdkD.lib")
#pragma comment(lib, "zlib.lib")
#else
#pragma comment(lib, "exiv2.lib")
#pragma comment(lib, "libexpat.lib")
#pragma comment(lib, "xmpsdk.lib")
#pragma comment(lib, "zlib.lib")
#endif

namespace zzz{
class Z3RD_CLASS EXIF {
public:
  EXIF();
  EXIF(const string &filename);
  bool LoadFile(const string &filename);
  bool HasEXIF(const string &key) const;
  string GetEXIFString(const string &key) const;
  long GetEXIFLong(const string &key) const;
  float GetEXIFFloat(const string &key) const;
  pair<int,int> GetEXIFRational(const string &key) const;


  float FocalLength() const{return GetEXIFFloat("Exif.Photo.FocalLength");}
  string Make() const{return GetEXIFString("Exif.Image.Make");}
  string Model() const{return GetEXIFString("Exif.Image.Model");}
  long Orientation() const{return GetEXIFLong("Exif.Image.Orientation");}
  string DateTime() const{return GetEXIFString("Exif.Image.DateTime");}
  float ExposureTime() const{return GetEXIFFloat("Exif.Photo.ExposureTime");}
  float ShutterSpeedValue() const{return GetEXIFFloat("Exif.Photo.ShutterSpeedValue");}
  float ApertureValue() const{return GetEXIFFloat("Exif.Photo.ApertureValue");}
  float ISOSpeedRatings() const{return GetEXIFFloat("Exif.Photo.ISOSpeedRatings");}

  float CCDSize() const;

  struct EXIFInfo {
    string key;
    string value;
  };
  void GetAllEXIF(vector<EXIFInfo> &info) const;
private:
  Exiv2::Image::AutoPtr image;
};

}
#endif // ZZZ_LIB_EXIF

