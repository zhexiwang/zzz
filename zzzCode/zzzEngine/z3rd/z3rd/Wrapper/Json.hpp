#pragma once
#include <iostream>
#include <zCore/3rdParty/rapidjson/rapidjson.h>
#include <zCore/3rdParty/rapidjson/document.h>
#include "JsonWriter.hpp"
#include <string>
using std::string;

namespace zzz {

typedef rapidjson::Value JsonNode;
typedef rapidjson::Document Json;
//class Json : public rapidjson::Document {
//public:
//  Json() : rapidjson::Document(&allocator_) {}
//protected:
//  static rapidjson::MemoryPoolAllocator<> allocator_;
//};


inline std::ostream& operator<<(std::ostream &os, const Json &json) {
  return os << JsonString(json);
}

inline std::ostream& operator<<(std::ostream &os, const JsonNode &json) {
  return os << JsonString(json);
}

}  // namespace zzz
