#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_LAPACK
extern "C" {
#undef small  // <-windows.h <- NpcNdr.h
#include <lapack/f2c.h>
#undef min
#undef max
#undef abs
#undef dabs
#undef min
#undef max
#undef dmin
#undef dmax
#undef bit_test
#undef bit_clear
#undef bit_set
#undef VOID
#include <lapack/blaswrap.h>
#include <lapack/cblas.h>
#include <lapack/clapack.h>
}

#ifdef ZZZ_OS_WIN64
#pragma comment(lib, "clapack_X64.lib")
#pragma comment(lib, "libf2c_X64.lib")      // fortune to c
#pragma comment(lib, "BLAS_X64.lib")      // original blas, use this or following atlas
#else
//#pragma comment(lib, "atlas_clapack.lib")
#pragma comment(lib, "clapack.lib")
#pragma comment(lib, "libf2c.lib")      // fortune to c
//#pragma comment(lib, "BLAS.lib")      // original blas, use this or following atlas
#pragma comment(lib, "cblaswrap.lib")   // f2c_blas to c_blas
#pragma comment(lib, "libcblas.lib")    // cblas to atlas implementation
#pragma comment(lib, "libatlas.lib")    // atlas implementation
#endif // ZZZ_OS_WIN64
#endif // ZZZ_LIB_LAPACK
