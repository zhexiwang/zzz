#pragma once
#include <zCore/common.hpp>
#include <z3rd/LibraryConfig.hpp>
#ifdef ZZZ_LIB_SUPERLU

#ifdef ZZZ_OS_WIN64
  #ifdef ZZZ_DEBUG
  #pragma comment(lib, "SuperLUD_X64.lib")
  #else
  #pragma comment(lib, "SuperLU_X64.lib")
  #endif // ZZZ_DEBUG
#else
  #ifdef ZZZ_DEBUG
  #pragma comment(lib, "SuperLUD.lib")
  #else
  #pragma comment(lib, "SuperLU.lib")
  #endif // ZZZ_DEBUG
#endif // ZZZ_OS_WIN64

extern "C"{
#include <superlu/slu_ddefs.h>
};
namespace zzz{
//column major in data
typedef std::pair<int,double> ccs_data_node;
typedef std::vector<ccs_data_node>  ccs_data_column;
typedef std::vector<ccs_data_column> ccs_data;
inline void AddValue(ccs_data &data, int row, int col, double value) {
  data[col].push_back(ccs_data_node(row,value));
}
//////////////////////////////////////////////////////////////////////////
//SuperLU Solver
bool LUSolve(const ccs_data &data, int nrow, int ncol, std::vector<double> b[], int nb=1);

//////////////////////////////////////////////////////////////////////////
//iterative solver
void IterativeSolve(const ccs_data &data, int nrow, int ncol, std::vector<double> b[], const std::vector<double> start[], int nb=1);
void MustSolve(const ccs_data &data, int nrow, int ncol, std::vector<double> b[], const std::vector<double> start[], int nb=1);
}
#endif // ZZZ_LIB_SUPERLU
