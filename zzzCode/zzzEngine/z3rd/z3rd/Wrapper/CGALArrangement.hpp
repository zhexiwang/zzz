#pragma once
#include <z3rd/Wrapper/CGALWrapper.hpp>

#ifdef ZZZ_LIB_CGAL

#include <zCore/common.hpp>
#include <zCore/Utility/ZCheck.hpp>
#include <zCore/Math/Vector2.hpp>
#undef POINT
#undef LINE
#undef RAY
#undef SEGMENT
#undef EMPTY
#undef min
#undef max
#include <CGAL/Quotient.h>
#include <CGAL/Cartesian.h>
#include <CGAL/MP_Float.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arrangement_2.h>
#include <CGAL/Arr_batched_point_location.h>

namespace zzz {
class CGALArrangement {
public:
  typedef CGAL::Quotient<CGAL::MP_Float>     Number_type;
  typedef CGAL::Cartesian<Number_type>       Kernel;
  typedef CGAL::Arr_segment_traits_2<Kernel> Traits_2;
  typedef Traits_2::Point_2                  Point_2;
  typedef Traits_2::X_monotone_curve_2       Segment_2;
  typedef CGAL::Arrangement_2<Traits_2>      Arrangement_2;
  typedef std::pair<Point_2, CGAL::Object>   Query_result;

  void Build(const std::vector<Vector2f>& points, const std::vector<Vector2i>& segments);
  typedef enum {TYPE_FACE_BOUNDED, TYPE_FACE_UNBOUNDED,
    TYPE_LINE,
    TYPE_POINT_ISOLATED, TYPE_POINT} ResultType;
  typedef pair<ResultType, int> Result;
  void LocatePoints(const std::vector<Vector2f>& points, std::vector<Result>& ret);
private:
  Arrangement_2    arr;
};
} // namespace zzz

#endif // ZZZ_LIB_CGAL
