#pragma once
#include <z3rd/Wrapper/CGALWrapper.hpp>
#ifdef ZZZ_LIB_CGAL

#include <zCore/common.hpp>
#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/Vector3.hpp>
namespace zzz {
ZCORE_FUNC void CGALDelaunayTriangulate(const vector<Vector2f> &points, vector<Vector3i> &triangles);
ZCORE_FUNC void CGALConstrainedDelaunayTriangulate(const vector<Vector2f> &points, const vector<Vector2i> &constraint, vector<Vector3i> &triangles);
ZCORE_FUNC void CGALPointLocationInTriangulation(const vector<Vector2f> &points, const vector<Vector2f> &check_points, vector<Vector3i> &locations); 
ZCORE_FUNC void CGALPolygonTriangulation(const vector<Vector2f> &polygon, vector<Vector3i> &tri); 
ZCORE_FUNC void CGALPolygonTriangulationStrict(const vector<Vector2f> &polygon, vector<Vector3i> &tri); 
};  // namespace zzz

#endif // ZZZ_LIB_CGAL
