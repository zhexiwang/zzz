#pragma once
#include <zCore/common.hpp>
#include <zCore/3rdParty/rapidjson/writer.h>
#include <zCore/3rdParty/rapidjson/stringbuffer.h>
#include <zCore/3rdParty/rapidjson/document.h>

namespace zzz {

namespace {
typedef rapidjson::Writer<rapidjson::GenericStringBuffer<rapidjson::UTF8<> > > RapidJsonWriter;
}  // namespace
class JsonWriter : public RapidJsonWriter {
public:
  JsonWriter() : RapidJsonWriter(buffer_) {}
  ~JsonWriter() {}
  void GetString(std::string &str) {str = buffer_.GetString();}
  string GetString() {return buffer_.GetString();}
  void String(const std::string &str) {RapidJsonWriter::String(str.c_str(), str.size());}
  using RapidJsonWriter::Null;
  using RapidJsonWriter::Bool;
  using RapidJsonWriter::Int;
  using RapidJsonWriter::Uint;
  using RapidJsonWriter::Int64;
  using RapidJsonWriter::Uint64;
  using RapidJsonWriter::Double;
  using RapidJsonWriter::StartObject;
  using RapidJsonWriter::EndObject;
  using RapidJsonWriter::StartArray;
  using RapidJsonWriter::EndArray;
  using RapidJsonWriter::String;

  void Json(const rapidjson::Value& json) {json.Accept(*this);}
  private:
  rapidjson::GenericStringBuffer<rapidjson::UTF8<> > buffer_;
};

inline string JsonString(const rapidjson::Value& json) {
  JsonWriter writer;
  writer.Json(json);
  string str;
  writer.GetString(str);
  return str;
}
}  // namespace zzz
