#include <map>
#include <iostream>
using namespace std;
#include "SuperLUWrapper.hpp"
#include <zCore/Utility/ZCheck.hpp>
#include <zCore/Utility/STLVector.hpp>
#include <zCore/Math/Math.hpp>
#ifdef ZZZ_LIB_SUPERLU
namespace zzz{
void buildCCS(SuperMatrix *mat, int nrow, int ncol, const ccs_data &data) {
  size_t size=0;
  for (size_t i=0;i<data.size();i++) size+=data[i].size();
  int *colptr = intMalloc(nrow + 1);
  int *rowind = intMalloc(size);
  double *d = doubleMalloc(size);

  for (size_t pos = 0, i = 0; i < data.size(); ++i) {
    if (i == 0) colptr[0] = 0;
    else colptr[i] = colptr[i - 1] + (int)data[i - 1].size();
    for (size_t j = 0; j < data[i].size(); ++j, ++pos) {
      rowind[pos] = data[i][j].first;
      d[pos] = data[i][j].second;
    }
  }
  colptr[nrow] = colptr[nrow - 1] + (int)data.back().size();
  dCreate_CompCol_Matrix(mat, nrow, ncol, size, d, rowind, colptr, SLU_NC, SLU_D, SLU_GE);
}

bool LUSolve(const ccs_data &data, int nrow, int ncol, vector<double> b[], int nb/*=1*/) {
  SuperMatrix A, B, L, U;
  buildCCS(&A, nrow, ncol, data);

  double *bdata = doubleMalloc(nrow * nb);
  for (int i = 0; i < nb; ++i)
    memcpy(bdata + i * nrow, &(b[i][0]), sizeof(double) * nrow);
  dCreate_Dense_Matrix(&B, nrow, nb, bdata, nrow, SLU_DN, SLU_D, SLU_GE);

  superlu_options_t options;
  set_default_options(&options);
  options.ColPerm = NATURAL;

  SuperLUStat_t stat;
  StatInit(&stat);

  int *perm_r = intMalloc(nrow), *perm_c = intMalloc(ncol), info;
  ZLOGV << "enter solver\n";
  dgssv(&options, &A, perm_c, perm_r, &L, &U, &B, &stat, &info);

  if (info!=0) {//failed
    if (info<=A.ncol)
      ZLOGE << "Solve failed: U(" << info << ", " << info << ") is zero, so singular\n";
    else
      ZLOGE << "Solve failed: Memory allocate failed after allocated " << info + A.ncol << " bytes\n";
    SUPERLU_FREE(perm_r);
    SUPERLU_FREE(perm_c);
    Destroy_CompCol_Matrix(&A);
    Destroy_SuperMatrix_Store(&B);
    Destroy_SuperNode_Matrix(&L);
//    Destroy_CompCol_Matrix(&U);
    StatFree(&stat);
    return false;
  } else {
//    double *sol = (double*) ((DNformat*) B.Store)->nzval;
    for (int i = 0; i < nb; ++i)
      memcpy(&(b[i][0]), bdata + i * nrow, sizeof(double) * nrow);
    SUPERLU_FREE(perm_r);
    SUPERLU_FREE(perm_c);
    Destroy_CompCol_Matrix(&A);
    Destroy_SuperMatrix_Store(&B);
    Destroy_SuperNode_Matrix(&L);
    Destroy_CompCol_Matrix(&U);
    StatFree(&stat);
    return true;
  }
}

//////////////////////////////////////////////////////////////////////////
//iterative solver
//NOT A UNIVERSAL SOLVER
using namespace std;
void iterativeSolve( const ccs_data &data, int nrow, int ncol, vector<double> b[], const vector<double> start[], int nb/*=1*/ ) {
  //column major to row major
  STLVector<STLVector<pair<int,double> > > A(nrow);
  for (size_t i=0;i<data.size();i++) {
    const ccs_data_column &column = data[i];
    for (size_t j=0;j<column.size();j++)
      A[column[j].first].push_back(make_pair((int)i, column[j].second));
  }

  //solve
  double *res1 = new double[nrow * nb];
  for (int n = 0; n < nb; ++n)
    memcpy(res1 + n * nrow, &(start[n][0]), sizeof(double) * nrow);
  double *res2 = new double[nrow * nb];
  double *nowres = res2, *lastres = res1;
  double *sum = new double[nb];

  int count=0;
  double lastresidue=0;
  while(true) {
    for (int i = 0; i < nrow; ++i) {
      double coeff = 0.0;
      for (int n = 0; n < nb; ++n)
        sum[n] = b[n][i];
      STLVector<pair<int, double> > &row = A[i];
      for (size_t j = 0; j < row.size(); ++j) {
        pair<int,double> &node=row[j];
        if (node.first == i) {
          coeff=node.second;
        } else {
          for (int n = 0; n < nb; ++n)
            sum[n]-=node.second*lastres[n*nrow+node.first];
        }
      }
      if (coeff==0.0) {
        ZLOGE << "Error while solve iteratively. The matrix must be a specific form!\n";
        return;
      } else {
        for (int n = 0; n < nb; ++n) {
          double res = sum[n] / coeff;
          nowres[n * nrow + i] = res;
        }
      }
    }
    double residue = 0;
    for (int i = 0;i < nrow * nb; ++i)
      residue += Abs(nowres[i] - lastres[i]);
    residue /= nrow * nb;
    if (residue - lastresidue < EPSILON5) ++count;
    else count = 0;
    if (count > 10) break;
    Swap(nowres, lastres);
  }

  //write back
  for (int n = 0; n < nb; ++n)
    memcpy(&(b[n][0]), nowres + n * nrow, sizeof(double) * nrow);

  delete res1;
  delete res2;
  delete sum;
}

void MustSolve(const ccs_data &data, int nrow, int ncol, vector<double> b[], const vector<double> start[], int nb/*=1*/ ) {
  if (!LUSolve(data,nrow,ncol,b,nb))
    iterativeSolve(data,nrow,ncol,b,start,nb);
}
}

#endif // ZZZ_LIB_SUPERLU
