#pragma once
#include <z3rd/LibraryConfig.hpp>
#ifdef ZZZ_LIB_POCO

#include <Poco/Timestamp.h>
#include <Poco/DateTime.h>
#include <Poco/LocalDateTime.h>
#include <Poco/Timespan.h>
#include <Poco/Timezone.h>
#include <Poco/DateTimeFormatter.h>
#include <Poco/DateTimeFormat.h>
#include <Poco/DateTimeParser.h>
#include <Poco/Thread.h>

#ifdef ZZZ_OS_WIN64
  #ifdef ZZZ_DEBUG
  #pragma comment(lib, "PocoFoundationD_X64.lib")
  #else
  #pragma comment(lib, "PocoFoundationX64.lib")
  #endif // ZZZ_DEBUG
#else
  #ifdef ZZZ_DEBUG
  #pragma comment(lib, "PocoFoundationD.lib")
  #else
  #pragma comment(lib, "PocoFoundation.lib")
  #endif // ZZZ_DEBUG
#endif // ZZZ_OS_WIN64

namespace zzz {
using Poco::Timestamp;
using Poco::DateTime;
using Poco::LocalDateTime;
using Poco::Timespan;
using Poco::Timezone;
using Poco::DateTimeFormatter;
using Poco::DateTimeFormat;
using Poco::DateTimeParser;

inline string FormatDateTime(const DateTime &d, const string &pat) {return DateTimeFormatter::format(d, pat);}
inline string FormatLocalDateTime(const LocalDateTime &d, const string &pat) {return DateTimeFormatter::format(d, pat);}
inline string FormatTimespan(const Timespan &d, const string &pat) {return DateTimeFormatter::format(d, pat);}
inline string FormatTimestamp(const Timestamp &d, const string &pat) {return DateTimeFormatter::format(d, pat);}

inline void ParseDateTime(DateTime &d, const string &str, const string &pat) {int tzd; DateTimeParser::parse(pat, str, d, tzd);}
inline void ParseTimestamp(Timestamp &d, const string &str, const string &pat) {int tzd; DateTime dt; DateTimeParser::parse(pat, str, dt, tzd); d = dt.timestamp();}
inline void ParseLocalDateTime(LocalDateTime &d, const string &str, const string &pat) {DateTime dt; int tzd; DateTimeParser::parse(pat, str, dt, tzd); d = LocalDateTime(tzd, dt);}
inline bool TryParseDateTime(DateTime &d, const string &str, const string &pat) {int tzd; return DateTimeParser::tryParse(pat, str, d, tzd);}
inline bool TryParseLocalDateTime(LocalDateTime &d, const string &str, const string &pat) {DateTime dt; int tzd; return DateTimeParser::tryParse(pat, str, dt, tzd);}

inline ostream &operator<<(ostream &os, const Timestamp &ts) { return os << FormatTimestamp(ts, "%Y-%m-%d %H:%M:%S.%i"); }
inline ostream &operator<<(ostream &os, const DateTime &ts) { return os << FormatDateTime(ts, "%Y-%m-%d %H:%M:%S.%i"); }
inline ostream &operator<<(ostream &os, const LocalDateTime &ts) { return os << FormatLocalDateTime(ts, "%Y-%m-%d %H:%M:%S.%i %z"); }
inline ostream &operator<<(ostream &os, const Timespan &ts) { return os << FormatTimespan(ts, "%dd %H:%M:%S.%i"); }

};  // namespace zzz

#endif  // ZZZ_LIB_POCO