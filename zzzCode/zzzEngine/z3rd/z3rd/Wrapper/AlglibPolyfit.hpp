#include <z3rd/LibraryConfig.hpp>
#ifdef ZZZ_LIB_ALGLIB
#include <alglib/interpolation.h>
#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/Vector.hpp>
using namespace alglib;

#ifdef ZZZ_DEBUG
#pragma comment(lib, "alglibD.lib")
#else
#pragma comment(lib, "alglib.lib")
#endif // ZZZ_DEBUG

namespace zzz{
template<unsigned int N>
double AlglibPolyVal(const Vector<N, double>& p, double x) {
  double ret = 0, xx=1;
  for (zuint i=0; i<N; i++) {
    ret += xx * p[i];
    xx *= x;
  }
  return ret;
}

template<unsigned int N, typename T>
Vector<N, double> AlglibPolyFit(const vector<T>& _x, const vector<T>& _y)
{
  ZCHECK_EQ(_x.size(), _y.size())<<"Input array size must be equal.";
  real_1d_array x, y;
  x.setlength(_x.size());
  y.setlength(_y.size());
  for (zuint i=0; i<_x.size(); i++) {
    x[i]=_x[i];
    y[i]=_y[i];
  }

  barycentricinterpolant p;
  polynomialfitreport rep;
  ae_int_t info;
  polynomialfit(x, y, N, info, p, rep);
  real_1d_array a;
  a.setlength(4);
  alglib_impl::ae_state state;
  ae_state_init(&state);
  alglib_impl::polynomialbar2pow(p.c_ptr(), 0, 1, a.c_ptr(), &state);
  ae_state_clear(&state);
  Vector<N,double> ret;
  for (zuint i=0; i<N; i++)
    ret[i]=a[i];
  return ret;
}

template<unsigned int N, typename T>
Vector<N, double> AlglibPolyFit(const vector<Vector<2, T> >& _v)
{
  real_1d_array x, y;
  x.setlength(_v.size());
  y.setlength(_v.size());
  for (zuint i=0; i<_v.size(); i++) {
    x[i]=_v[i][0];
    y[i]=_v[i][1];
  }

  barycentricinterpolant p;
  polynomialfitreport rep;
  ae_int_t info;
  polynomialfit(x, y, N, info, p, rep);
  real_1d_array a;
  a.setlength(4);
  alglib_impl::ae_state state;
  ae_state_init(&state);
  alglib_impl::polynomialbar2pow(p.c_ptr(), 0, 1, a.c_ptr(), &state);
  ae_state_clear(&state);
  Vector<N,double> ret;
  for (zuint i=0; i<N; i++)
    ret[i]=a[i];
  return ret;
}

inline double PolyCurvature(const Vector<3,double> &p, const double x) {
  // y(x) = p[0] + p[1]x + p[2]x^2 ==>
  // x(t) = t
  // y(t) = p[0] + p[1]t + p[2]t^2 
  // x' = 1, x" = 0
  // y' = p[1] + 2p[2]t, y" = 2p[2]
  // curvature = (x'y" - y'x") / Sqrt(Cube(Sqr(x')+Sqr(y')))
  // curvature = 2p[2] / Sqrt(Cube(1+ Sqr(p[1]+2p[2]t)))
  return (2*p[2]) / Sqrt(Cube(1+ Sqr(p[1]+2*p[2]*x)));
}

inline double PolyCurvature(const Vector<4,double> &p, const double x) {
  // y(x) = p[0] + p[1]x + p[2]x^2 + p[3]x^3 ==>
  // x(t) = t
  // y(t) = p[0] + p[1]t + p[2]t^2 + p[3]t^3
  // x' = 1, x" = 0
  // y' = p[1] + 2p[2]t + 3p[3]t^2, y" = 2p[2] + 6p[3]t
  // curvature = (x'y" - y'x") / Sqrt(Cube(Sqr(x')+Sqr(y')))
  // curvature = (2p[2] + 6p[3]t) / Sqrt(Cube(1+ Sqr(p[1]+2p[2]t+3p[3]t^2)))
  return (2*p[2] + 6*p[3] * x) / Sqrt(Cube(1+ Sqr(p[1]+2*p[2]*x+3*p[3]*Sqr(x))));
}

inline double Derivative(const Vector<3,double> &p, const double x) {
  // y(x) = p[0] + p[1]x + p[2]x^2
  // y' = p[1] + 2p[2]x
  return p[1] + 2 * p[2] * x;
}

inline double Derivative(const Vector<4,double> &p, const double x) {
  // y(x) = p[0] + p[1]x + p[2]x^2 + p[3]x^3
  // y' = p[1] + 2p[2]x + 3p[3]x^2
  return p[1] + 2 * p[2] * x + 3 * p[3] * Sqr(x);
}

template<unsigned int N>
inline Vector2f ClosestPointOnPoly(const Vector<N,double> &p, const Vector2f &x) {
  VerboseLevel::Set(ZINFO);
  // local iterative search
  Vector2f x0(x[0], AlglibPolyVal(p, x[0]));
  Vector2f v0(x-x0), v1(1, Derivative(p, x0[0]));
  float v0dist = v0.SafeNormalize();
  if (v0dist < EPSILON)
    return x0;
  v1.Normalize();
  float costheta = FastDot(v0, v1);
  IterExitCond<float> cond;
  cond.SetCondIterCount(10);
  cond.SetCondResidue(cos(89*C_D2R));
  do {
    x0 = x0 + v1 * costheta * v0dist;
    x0[1] = AlglibPolyVal(p, x0[0]);
    v0 = x-x0;
    v1 = Vector2f(1, Derivative(p, x0[0]));
    v0dist = v0.Normalize();
    v1.Normalize();
    costheta = FastDot(v0, v1);
    if (costheta < 0) {
      costheta = -costheta;
      v1 = -v1;
    }
  } while(cond.NeedLoop(costheta));
  VerboseLevel::Restore();
  return x0;
}
} // namespace zzz

#endif //ZZZ_LIB_ALGLIB