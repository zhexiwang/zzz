#include "YAML.hpp"

namespace zzz {

bool ParseYAMLTo(const string &str, YAML::Node &doc) {
  istringstream fin(str);
  YAML::Parser parser(fin);
  try {
    parser.GetNextDocument(doc);
  } catch (YAML::ParserException e) {
    ZLOGE << "Error in parsing YAML: " << e.what();
    istringstream fi(str);
    string line;
    int count = 0;
    while (getline(fi, line)) {
      if (count == e.mark.line) {
        ZLOGE << line;
        ostringstream oss;
        for (int i = 0; i < e.mark.column; ++i)
          oss << ' ';
        oss << '^';
        ZLOGE << oss.str();
        break;
      }
    }
    return false;
  }
  return true;
}

}  // namespace zzz