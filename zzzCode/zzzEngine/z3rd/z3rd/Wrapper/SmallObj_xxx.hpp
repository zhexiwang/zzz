#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_LOKI
#include <loki/SmallObj.h>

#ifdef ZZZ_LIB_LOKI
#ifdef ZZZ_DEBUG
#pragma comment(lib, "lokiD.lib")
#else
#pragma comment(lib, "loki.lib")
#endif
#endif // ZZZ_LIB_LOKI

namespace zzz{
template<typename T>
class SmallObj : public T, public Loki::SmallObject<> {
};
}
#endif // ZZZ_LIB_LOKI

