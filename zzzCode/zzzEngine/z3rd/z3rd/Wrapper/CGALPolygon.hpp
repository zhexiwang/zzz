#pragma once
#include <z3rd/Wrapper/CGALWrapper.hpp>
#ifdef ZZZ_LIB_CGAL

#include <zCore/common.hpp>
#include <zCore/Math/Vector2.hpp>
namespace zzz {
ZCORE_FUNC bool CGALPointInsidePolygon(const vector<Vector2f> &polygon, const Vector2f &p);
ZCORE_FUNC bool CGALAllPointsInsidePolygon(const vector<Vector2f> &polygon, const vector<Vector2f> &p);
ZCORE_FUNC void CGALPointsInsidePolygon(const vector<Vector2f> &polygon, const vector<Vector2f> &p, vector<bool> &res);
ZCORE_FUNC bool CGALPolygonIsSimple(const vector<Vector2f> &polygon);
};  // namespace zzz

#endif // ZZZ_LIB_CGAL
