#pragma once
#undef min
#undef max
#include <RStarTree/RStarTree.h>
#include <zGraphics/Graphics/AABB.hpp>

namespace zzz{
  template<zuint N, typename T, int min_child_items = 32, int max_child_items = 64>
  class RTree {
  public:
    void SetData(const vector<AABB<N, T> > &data);
    void QueryOverlap(const AABB<N, T> &range, vector<int> &result);
    void QueryEnclose(const AABB<N, T> &range, vector<int> &result);
  private:
    typedef RStarTree<T, N, min_child_items, max_child_items> RStarTreeType;
    RStarTreeType rtree_;
    typedef typename RStarTreeType::Leaf RStarTreeLeaf;
    typedef typename RStarTreeType::BoundingBox RStarTreeBoundingBox;
    struct Visitor {
      bool ContinueVisiting;
      Visitor(vector<int> &res) : res_(res), ContinueVisiting(true) {res_.clear();}
      void operator()(const RStarTreeLeaf *const leaf) {
        res_.push_back(leaf->leaf);
      }
      vector<int> &res_;
    };

  };

  template<zuint N, typename T, int min_child_items, int max_child_items>
  void zzz::RTree<N, T, min_child_items, max_child_items>::QueryEnclose(const AABB<N, T> &range, vector<int> &result) {
    Visitor visitor(result);
    RStarTreeBoundingBox box;
    for (zuint j = 0; j < N; ++j) {
      box.edges[j].first = range.Min(j);
      box.edges[j].second = range.Max(j);
    }
    rtree_.Query(RStarTreeType::AcceptEnclosing(box), visitor);
  }

  template<zuint N, typename T, int min_child_items, int max_child_items>
  void zzz::RTree<N, T, min_child_items, max_child_items>::QueryOverlap(const AABB<N, T> &range, vector<int> &result) {
    Visitor visitor(result);
    RStarTreeBoundingBox box;
    for (zuint j = 0; j < N; ++j) {
      box.edges[j].first = range.Min(j);
      box.edges[j].second = range.Max(j);
    }
    rtree_.Query(RStarTreeType::AcceptOverlapping(box), visitor);
  }

  template<zuint N, typename T, int min_child_items, int max_child_items>
  void zzz::RTree<N, T, min_child_items, max_child_items>::SetData(const vector<AABB<N, T> > &data) {
    for (zuint i = 0; i < data.size(); ++i) {
      RStarTreeBoundingBox box;
      const AABB<N, T> &aabb = data[i];
      for (zuint j = 0; j < N; ++j) {
        box.edges[j].first = aabb.Min(j);
        box.edges[j].second = aabb.Max(j);
      }
      rtree_.Insert(i, box);
    }
  }

}