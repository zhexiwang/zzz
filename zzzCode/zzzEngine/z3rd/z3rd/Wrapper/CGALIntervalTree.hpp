#pragma once
#include <z3rd/Wrapper/CGALWrapper.hpp>
#ifdef ZZZ_LIB_CGAL
#include <zCore/Math/Vector2.hpp>
#include <CGAL/Interval_skip_list.h>
#include <CGAL/Interval_skip_list_interval.h>
#include <vector>
#include <list>
#include <map>

namespace zzz {
template<typename T>
class CGALIntervalTree {
public:
  typedef CGAL::Interval_skip_list_interval<T> Interval;
  typedef CGAL::Interval_skip_list<Interval> IntervalSkipList;

  void Build(const vector<Vector<2, T> > &data, bool closed = true) {
    STLVector<Interval> intervals;
    intervals.reserve(data.size());
    ori_idx_.clear();
    for (zuint i = 0; i < data.size(); ++i) {
      intervals.push_back(Interval(data[i][0], data[i][1], closed, closed));
      ori_idx_.insert(make_pair(data[i], i));
    }
    isl_.clear();
    isl_.insert(intervals.begin(), intervals.end());
  }

  void Build(const vector<MinMax<T> > &data, bool closed = true) {
    STLVector<Interval> intervals;
    intervals.reserve(data.size());
    ori_idx_.clear();
    for (zuint i = 0; i < data.size(); ++i) {
      intervals.push_back(Interval(data[i].Min(), data[i].Max(), closed, closed));
      ori_idx_.insert(make_pair(Vector<2, T>(data[i].Min(), data[i].Max()), i));
    }
    isl_.clear();
    isl_.insert(intervals.begin(), intervals.end());
  }

  void FindIntervals(const T x, vector<zuint> &result) {
    result.clear();
    std::list<Interval> L;
    isl_.find_intervals(x, std::back_inserter(L));
    result.reserve(L.size());
    for (std::list<Interval>::iterator it = L.begin(); it != L.end(); it++){
      Vector<2, T> v(it->inf(), it->sup());
      multimap<Vector<2, T>, zuint>::iterator itlow = ori_idx_.lower_bound(v);
      multimap<Vector<2, T>, zuint>::iterator itup = ori_idx_.upper_bound(v);
      for (multimap<Vector<2, T>, zuint>::iterator it2 = itlow; it2 != itup; ++it2)
        result.push_back(it2->second);
    }
  }

private:
  IntervalSkipList isl_;
  std::multimap<Vector<2, T>, zuint> ori_idx_;
};
}  // namespace zzz

#endif //  ZZZ_LIB_CGAL
