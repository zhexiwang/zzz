#pragma once

#include <zCore/EnvDetect.hpp>

//#define ZZZ_LIB_MINIMAL

#ifdef ZZZ_LIB_MINIMAL
#include "LibraryConfig.minimal.hpp"
#else

#if defined(ZZZ_COMPILER_MSVC) && defined(ZZZ_OS_WIN32)
#include "LibraryConfig.win32.hpp"
#endif

#if defined(ZZZ_COMPILER_MSVC) && defined(ZZZ_OS_WIN64)
#include "LibraryConfig.win64.hpp"
#endif

#ifdef ZZZ_OS_MACOS
#include "LibraryConfig.macos.hpp"
#endif

#ifdef ZZZ_OS_LINUX
#include "LibraryConfig.linux.hpp"
#endif

#ifdef ZZZ_COMPILER_MINGW
#include "LibraryConfig.mingw.hpp"
#endif

#endif // ZZZ_LIB_MINIMAL

#ifdef ZZZ_DYNAMIC
  #pragma warning(disable:4251)  // needs to have dll-interface
  #pragma warning(disable:4275)  // non dll-interface class 'zzz::Uncopyable' used as base for dll-interface class 'zzz::RapidXML'

  #ifdef ZCORE_SOURCE
    #define Z3RD_FUNC __declspec(dllexport)
    #define Z3RD_CLASS __declspec(dllexport)
  #else
    #define Z3RD_FUNC __declspec(dllimport)
    #define Z3RD_CLASS __declspec(dllimport)
  #endif
#else
  #define Z3RD_FUNC
  #define Z3RD_CLASS
#endif