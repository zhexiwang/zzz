#pragma once

// Only include libraries that are needed for zCore here.

#include "LibraryConfig.hpp"

#ifndef ZZZ_NO_AUTO_LINK
#ifdef ZZZ_DYNAMIC
  #ifdef ZZZ_DEBUG
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib, "z3rddllD.lib")
    #else
      #pragma comment(lib, "z3rddllD_x64.lib")
    #endif // ZZZ_OS_WIN64
  #else
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib, "z3rddll.lib")
    #else
      #pragma comment(lib, "z3rddll_x64.lib")
    #endif // ZZZ_OS_WIN64
  #endif
#else
  #ifdef ZZZ_DEBUG
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib, "z3rdD.lib")
    #else
      #pragma comment(lib, "z3rdD_x64.lib")
    #endif // ZZZ_OS_WIN64
  #else
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib, "z3rd.lib")
    #else
      #pragma comment(lib, "z3rd_x64.lib")
    #endif // ZZZ_OS_WIN64
  #endif
#endif

#endif // ZZZ_NO_AUTO_LINK