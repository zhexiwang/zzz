#pragma once

// Mulitple use library.
#define ZZZ_LIB_BOOST
//#define ZZZ_LIB_LOKI

// Math
//#define ZZZ_LIB_LAPACK
//#define ZZZ_LIB_MINPACK
//#define ZZZ_LIB_FFTW
//#define ZZZ_LIB_SBA
//#define ZZZ_LIB_TAUCS
#define ZZZ_LIB_CGAL
#define CGAL_NO_AUTOLINK
#define ZZZ_LIB_GMP
//#define ZZZ_LIB_ALGLIB
//#define ZZZ_LIB_SUPERLU

// Algorithm
//#define ZZZ_LIB_ANN
//#define ZZZ_LIB_ANN_CHAR
//#define ZZZ_LIB_GCO
//#define ZZZ_LIB_OPENCV

// Image loader.
#define ZZZ_LIB_LIBJPEG
#define ZZZ_LIB_LIBPNG
#define ZZZ_LIB_DEVIL
//#define ZZZ_LIB_FREEIMAGE

// File format
//#define ZZZ_LIB_EXIF
//#define ZZZ_LIB_TINYXML

// OpenGL
#define ZZZ_LIB_OPENGL

// GUI
#define ZZZ_LIB_QT4
//#define ZZZ_LIB_MFC
//#define ZZZ_LIB_SFML
//#define ZZZ_LIB_GLUI
//#define ZZZ_LIB_GLFW
#define ZZZ_LIB_ANTTWEAKBAR
#define TW_NO_LIB_PRAGMA

// Misc
// #define ZZZ_LIB_OPENMESH
#define ZZZ_LIB_PTHREAD
//#define ZZZ_LIB_FAST

//#define ZZZ_LIB_YAML_CPP
