include($$PWD/../../zzzlib.pri)

QT       -= core gui

TARGET = z3rd$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    $$files($$PWD/z3rd/Wrapper/*.cpp) \
    $$files($$PWD/z3rd/*.cpp)

HEADERS += \
    $$files($$PWD/z3rd/Wrapper/*.hpp) \
    $$files($$PWD/z3rd/*.hpp)

OTHER_FILES += \

ZCOREENABLE = 1
!include($${top_srcdir}/QtFlag/QTFlagsZCore.pri) {
    error("Cannot include QTFlagsZCore.pri")
}

BOOSTENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsBoost.pri) {
    error("Cannot include QTFlagsBoost.pri")
}

DEFINES = $$replace(DEFINES, ZETA, ZZZ_LIB)
DEFINES = $$replace(DEFINES, _ENABLE, )
message($$DEFINES)
