#pragma once

namespace zzz{
template<typename T>
struct RTreeFunction
{
  typedef RTreeFunction<T> *(*RTreeFuncGenerator)(void*);
  virtual int operator()(const T *v)=0;
  virtual void print()=0;
};


#define MAX_LEVEL 12
#define MIN_NELEMENT 1

}