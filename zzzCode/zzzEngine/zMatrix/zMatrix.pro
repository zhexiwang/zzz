include($$PWD/../../zzzlib.pri)

QT       -= core gui

TARGET = zMatrix$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    $$files($$PWD/zMatrix/*.cpp)

HEADERS += \
    $$files($$PWD/zMatrix/*.hpp)

OTHER_FILES += \

ZCOREENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZCore.pri) {
    error("Cannot include QTFlagsZCore.pri")
}

Z3RDENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZ3rd.pri) {
    error("Cannot include QTFlagsZ3rd.pri")
}
DEFINES = $$replace(DEFINES, ZETA, ZZZ_LIB)
DEFINES = $$replace(DEFINES, _ENABLE, )
message($$DEFINES)
