#pragma once

#include "zMatrixBase.hpp"

namespace zzz{
template<typename T,class Major, typename E1, typename E2>
class zHCombineMatrixR : public zMatrixBaseR<T,Major, zHCombineMatrixR<T, Major, E1, E2> > {
  typedef zHCombineMatrixR<T, Major, E1, E2> MeType;
public:
  explicit zHCombineMatrixR(const zMatrixBaseR<T,Major, E1> &mat1, const zMatrixBaseR<T,Major, E2> &mat2)
    :mat1_(mat1),mat2_(mat2),zMatrixBaseR<T,Major, zHCombineMatrixR<T, Major, E1, E2> >(mat1.rows_,mat1.cols_+mat2.cols_)
  {ZCHECK_EQ(mat1.rows_, mat2.rows_);}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    if (c<mat1_.cols_) return mat1_(r,c);
    else return mat2_(r,c-mat1_.cols_);
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major, E1> &mat1_;
  const zMatrixBaseR<T,Major, E2> &mat2_;
};

template<typename T,class Major, typename E1, typename E2>
const zHCombineMatrixR<T,Major,E1, E2> operator,(const zMatrixBaseR<T,Major, E1> &mat1, const zMatrixBaseR<T,Major, E2> &mat2)
{return zHCombineMatrixR<T,Major,E1, E2>(mat1,mat2);}

template<typename T,class Major, typename E1, typename E2>
class zVCombineMatrixR : public zMatrixBaseR<T,Major, zVCombineMatrixR<T, Major, E1, E2> > {
  typedef zVCombineMatrixR<T, Major, E1, E2> MeType;
public:
  explicit zVCombineMatrixR(const zMatrixBaseR<T,Major, E1> &mat1, const zMatrixBaseR<T,Major, E2> &mat2)
    :mat1_(mat1),mat2_(mat2),zMatrixBaseR<T,Major, zVCombineMatrixR<T, Major, E1, E2> >(mat1.rows_+mat2.rows_,mat1.cols_)
  {ZCHECK_EQ(mat1.cols_, mat2.cols_);}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    if (r<mat1_.rows_) return mat1_(r,c);
    else return mat2_(r-mat1_.rows_,c);
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major,E1> &mat1_;
  const zMatrixBaseR<T,Major,E2> &mat2_;
};

template<typename T,class Major, typename E1, typename E2>
const zVCombineMatrixR<T,Major, E1, E2> operator%(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zVCombineMatrixR<T,Major, E1, E2>(mat1,mat2);}
}
