#pragma once
#include "zMatrixBase.hpp"

namespace zzz{
//func(mat,mat)
#define CONSTANT_EXPRESSIONM(name,func) \
template<typename T,class Major, typename E1, typename E2>\
class zFunction##name##ExpressionM : public zMatrixBaseR<T,Major, zFunction##name##ExpressionM<T, Major, E1, E2> > {\
  typedef zFunction##name##ExpressionM<T, Major, E1, E2> MeType; \
public:\
  explicit zFunction##name##ExpressionM(const zMatrixBaseR<T,Major,E1>& mat1, const zMatrixBaseR<T,Major,E2>& mat2)\
  :zMatrixBaseR<T,Major, zFunction##name##ExpressionM<T, Major, E1, E2> >(mat1.rows_,mat1.cols_),Mat1_(mat1),Mat2_(mat2)\
  {ZCHECK_EQ(Mat1_.rows_,Mat2_.rows_); ZCHECK_EQ(Mat1_.cols_,Mat2_.cols_);}\
  const T operator()(zuint r, zuint c) const {\
    CheckRange(r, c); \
    return func(Mat1_(r,c), Mat2_(r,c)); \
  }\
  ZMATRIXR_METHOD(MeType); \
private:\
  const zMatrixBaseR<T,Major,E1> &Mat1_; \
  const zMatrixBaseR<T,Major,E2> &Mat2_; \
};

CONSTANT_EXPRESSIONM(Pow,Pow<T>)
template<typename T,class Major, typename E1, typename E2>
const zFunctionPowExpressionM<T,Major,E1,E2> operator^(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zFunctionPowExpressionM<T,Major,E1,E2>(mat1,mat2);};

}
