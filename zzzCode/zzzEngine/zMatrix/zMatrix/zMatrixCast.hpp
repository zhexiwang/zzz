#pragma once
#include "zMatrixBase.hpp"
#include "zMatrix.hpp"
#include <zCore/Math/Vector.hpp>
#include <zCore/Math/Matrix.hpp>

namespace zzz{
//////////////////////////////////////////////
//Dress to cast value
template<typename T, typename FROM, class Major, typename E>
class zMatrixCastR : public zMatrixBaseR<T, Major, zMatrixCastR<T, FROM, Major, E> > {
  typedef zMatrixCastR<T, FROM, Major, E> MeType;
public:
  explicit zMatrixCastR(const zMatrixBaseR<FROM,Major, E> &mat):Mat_(mat),zMatrixBaseR<T, Major, zMatrixCastR<T, FROM, Major, E> >(mat.rows_,mat.cols_){}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return static_cast<T>(Mat_(r,c));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<FROM,Major, E> &Mat_;
};
template<typename T, typename FROM, class Major, typename E>
const zMatrixCastR<T,FROM,Major, E> Cast(const zMatrixBaseR<FROM,Major, E> &mat)
{return zMatrixCastR<T,FROM,Major, E>(mat);}

}

