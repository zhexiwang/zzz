#pragma once
#include <z3rd/LibraryConfig.hpp>
#ifdef ZZZ_LIB_TAUCS
extern "C" {
#include <lapack/blaswrap.h>
#include <taucs.h>
}
#include <zMatrix/zMat.hpp>
#include <zCore/Math/Array1.hpp>
namespace zzz {
class ZCORE_CLASS TaucsSolver {
public:
  void CreateTaucsCCS(zSparseMatrix<double> &mat);
  void Explain(int error);

  virtual bool Creater(zSparseMatrix<double> &mat, char *log_filename = "none", int* ResCode=NULL)=0;
  virtual bool Destroy(int* ResCode=NULL)=0;
  virtual bool Solve(double *x, double *b, int* ResCode=NULL)=0;

  taucs_ccs_matrix matrix_;
  Array1i colptr_, rowind_;
  Array1d values_;
};

class ZCORE_CLASS TaucsCholeskySolver : public TaucsSolver
{
public:
  TaucsCholeskySolver();
  ~TaucsCholeskySolver();
  virtual bool Creater(zSparseMatrix<double> &mat, char *log_filename = "none", int* ResCode=NULL);
  virtual bool Destroy(int* ResCode=NULL);
  virtual bool Solve(double *x, double *b, int* ResCode=NULL);
private:
  void *factor_;
};
};  // namespace zzz

#endif  // ZZZ_LIB_TAUCS
