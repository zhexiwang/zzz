#pragma once
#include <zMatrix/zMat.hpp>

namespace zzz{
//AX=B
class LinearSystem
{
public:
  LinearSystem(int n_var, int n_cons):nVar_(n_var),nConstraints_(n_cons){}
  const int nVar_;
  const int nConstraints_;

  virtual void Initialize(zMatrix<double> &A, zVector<double> &B)=0;
  virtual void WriteBack(zVector<double> &x)=0;
};

class LinearSystemSolver
{
public:
  virtual int Solve(LinearSystem &sys)=0;
};
}