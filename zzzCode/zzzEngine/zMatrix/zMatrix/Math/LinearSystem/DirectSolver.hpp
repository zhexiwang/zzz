#include "LinearSystem.hpp"

namespace zzz{
class DirectSolver : LinearSystemSolver
{
public:
  int Solve(LinearSystem &sys);
};
}