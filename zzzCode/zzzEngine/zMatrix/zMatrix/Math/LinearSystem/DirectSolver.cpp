#define ZCORE_SOURCE
#include "DirectSolver.hpp"

namespace zzz{
int DirectSolver::Solve(LinearSystem &sys)
{
  zMatrix<double> A;
  zVector<double> B;
  sys.Initialize(A, B);
  zMatrix<double> U, S,VT;
  bool res=SVD(A, U, S,VT);
  ZCHECK(res)<<"SVD Failed in DirectSolver";
  for (zuint i=0; i<S.Size(0); i++)
    if (S(i, i)<EPSILON) S(i, i)=0;
    else S(i, i)=1.0/S(i, i);
  zMatrix<double> PSU(Trans(VT)*S*Trans(U));
  zVector<double> X(PSU*B);
  sys.WriteBack(X);
  return 0;
}

}