#pragma once
#include <z3rd/Wrapper/OpenCVWrapper.hpp>

#ifdef ZZZ_LIB_OPENCV
#include <opencv2/highgui/highgui.hpp>
#include <zMatrix/zMat.hpp>

namespace zzz {
////////////////////////////////////////////////////
// dress for array
template<typename T,class Major>
class zMatrixCvMatDressR : public zMatrixBaseR<T,Major> {
  using zMatrixBaseR<T,Major>::rows_;
  using zMatrixBaseR<T,Major>::cols_;
public:
  explicit zMatrixCvMatDressR(const cv::Mat_<T> &mat)
    :zMatrixBaseR<T,Major>(mat.rows,mat.cols),mat_(mat){
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return mat_(r, c);
  }
  using zMatrixBaseR<T,Major>::operator();
private:
  const cv::Mat_<T> &mat_;
};
template<typename T,class Major>
class zMatrixCvMatDressW : public zMatrixBaseW<T,Major> {
  using zMatrixBaseW<T,Major>::rows_;
  using zMatrixBaseW<T,Major>::cols_;
public:
  explicit zMatrixCvMatDressW(cv::Mat_<T> &mat)
    :zMatrixBaseW<T,Major>(mat.rows,mat.cols),mat_(mat){}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return mat_(r, c);
  }
  T &operator()(zuint r, zuint c) {
    CheckRange(r, c);
    return mat_(r, c);
  }
  const zMatrixBaseW<T,Major> &operator=(const zMatrixBaseR<T,Major> &other) {
    mat_.create(other.rows_,other.cols_);
    rows_=other.rows_;
    cols_=other.cols_;
    return zMatrixBaseW<T,Major>::operator=(other);
  }
  using zMatrixBaseW<T,Major>::operator();
private:
  cv::Mat_<T> &mat_;
};
template<typename T>
const zMatrixCvMatDressR<T,zColMajor> Dress(const cv::Mat_<T> &v)
{return zMatrixCvMatDressR<T,zColMajor>(v);}
template<typename T>
zMatrixCvMatDressW<T,zColMajor> Dress(cv::Mat_<T> &v)
{return zMatrixCvMatDressW<T,zColMajor>(v);}

};  // namespace zzz
#endif