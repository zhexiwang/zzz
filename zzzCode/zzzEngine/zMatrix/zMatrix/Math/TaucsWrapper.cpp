#define ZCORE_SOURCE
#include "TaucsWrapper.hpp"

#ifdef ZZZ_LIB_TAUCS
#include <zMatrix/zMat.hpp>
namespace zzz {
void TaucsSolver::CreateTaucsCCS(zSparseMatrix<double> &mat) {
  matrix_.m=mat.Size(0);
  matrix_.n=mat.Size(1);
  matrix_.flags=TAUCS_DOUBLE|TAUCS_SYMMETRIC|TAUCS_LOWER;
  colptr_.SetSize(mat.Size(1)+1);
  rowind_.SetSize(mat.DataSize());
  values_.SetSize(mat.DataSize());
  matrix_.colptr=colptr_.Data();
  matrix_.rowind=rowind_.Data();
  matrix_.values.d=values_.Data();

  mat.ColMajorSort();
  int curcol = -1;
  for (zuint i=0; i<mat.DataSize(); i++) {
    const zSparseMatrix<double>::zSparseMatrixNode &node = mat.GetNode(i);
    values_[i]=node.v_;
    rowind_[i] = node.row_;
    while(curcol < int(node.col_)) {
      colptr_[++curcol] = i;
    }
  }
  while(curcol < int(mat.Size(1))) {
    colptr_[++curcol] = mat.DataSize();
  }
}

void TaucsSolver::Explain(int error) {
  switch(error) {
  case TAUCS_SUCCESS:
    ZLOGE<<"The routine completed its task successfully\n";
    break;
  case TAUCS_ERROR:
    ZLOGE<<"An error occured, but either non of the specific error codes is appropriate, or the routine could not determine which error code to return (this might happen if the error occured deep in the code)\n";
    break;
  case TAUCS_ERROR_NOMEM:
    ZLOGE<<"Failure to dynamically allocate memory\n";
    break;
  case TAUCS_ERROR_BADARGS:
    ZLOGE<<"The arguments to the routine are invalid\n";
    break;
  case TAUCS_ERROR_MAXDEPTH:
    ZLOGE<<"Recursion is too deep and might cause a stack overflow\n";
    break;
  case TAUCS_ERROR_INDEFINITE:
    ZLOGE<<"Input matrix was supposed to be positive definite but appears to be indefinite\n";
    break;
  }
}

///////////////////////////////////////////////////////
// functions for Cholesky Solver
TaucsCholeskySolver::TaucsCholeskySolver()
  : factor_(NULL) {}

bool TaucsCholeskySolver::Creater(zSparseMatrix<double> &mat, char *log_filename /*= NULL*/, int* ResCode/*=NULL*/) {
  Destroy();
  char* options[] = {"taucs.factor.LLT=true", NULL};
  void* opt_arg[] = { NULL };
  //char* options[] = {"taucs.factor.LLT=true", "taucs.factor.droptol=1e-2", "taucs.solve.cg=true", NULL};
  // Open log file
  taucs_logfile(log_filename);
  // Create matrix
  CreateTaucsCCS(mat);
  // Factor matrix
  int rc = taucs_linsolve(&matrix_, &factor_, 0, NULL, NULL, options, opt_arg);
  if (ResCode) *ResCode=rc;
  if (rc!=TAUCS_SUCCESS)
    Explain(rc);
  return rc == TAUCS_SUCCESS;
}

bool TaucsCholeskySolver::Solve(double *x, double *b, int *ResCode) {
  char* options [] = {"taucs.factor=false", NULL};
  //char* options [] = {"taucs.factor=false", "taucs.solve.cg=true", NULL};
  if (factor_ == NULL) return false;
  int rc = taucs_linsolve(&matrix_, &factor_, 1, x, b, options, NULL);
  if (ResCode) *ResCode=rc;
  if (rc!=TAUCS_SUCCESS)
    Explain(rc);
  return rc == TAUCS_SUCCESS;
}

bool TaucsCholeskySolver::Destroy(int *ResCode) {
  int rc = 0;
  if (factor_) {
    rc = taucs_linsolve(NULL, &factor_, 0, NULL, NULL, NULL, NULL);
    factor_ = NULL;
  }
  if (ResCode) *ResCode=rc;
  if (rc!=TAUCS_SUCCESS)
    Explain(rc);
  return rc == TAUCS_SUCCESS;
}

TaucsCholeskySolver::~TaucsCholeskySolver() {
  Destroy();
}

};  // namespace zzz
#endif  // ZZZ_LIB_TAUCS
