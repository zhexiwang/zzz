#pragma once
#include <z3rd/Wrapper/MinpackWrapper.hpp>
#include "NonlinearSystem.hpp"

#ifdef ZZZ_LIB_MINPACK
namespace zzz{
class MinpackSolver : public NonlinearSystemSolver {
public:
  int Solve(NonlinearSystem &sys);
};
}
#endif // ZZZ_LIB_MINPACK