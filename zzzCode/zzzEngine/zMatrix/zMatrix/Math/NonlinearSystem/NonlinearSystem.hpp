#pragma once
#include <zMatrix/zMat.hpp>

namespace zzz {
class NonlinearSystem {
public:
  NonlinearSystem(int n_var, int n_cons):nVar_(n_var),nConstraints_(n_cons){}
  int nVar_;
  int nConstraints_;

  void Initialize(zVector<double> &init_x);
  void WriteBack(const zVector<double> &result_x);
  void CalculateResidue(double *residue, const double *x);
  //jac is nConstraints*nVar
  template<typename E>
  void CalculateJacobian(zMatrixBaseW<double,zColMajor,E> &jac, const double *x);
};

class NonlinearSystemSolver {
public:
  virtual int Solve(NonlinearSystem &sys)=0;
};
} // namespace zzz