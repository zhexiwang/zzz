#pragma once
#include <zCore/Math/Complex.hpp>
#include "zMatrixBase.hpp"

namespace zzz{
//specific for complex
#define COMPLEX_EXPRESSION(name,ope) \
template<typename T,class Major, typename E>\
class zComplex##name##Expression : public zMatrixBaseR<Complex<T>,Major, zComplex##name##Expression<T, Major, E> > {\
  typedef zComplex##name##Expression<T, Major, E> MeType; \
public:\
  explicit zComplex##name##Expression(const zMatrixBaseR<Complex<T>,Major, E>& mat)\
  :zMatrixBaseR<Complex<T>,Major, zComplex##name##Expression<T, Major, E> >(mat.rows_,mat.cols_),Mat_(mat){}\
  const Complex<T> operator()(zuint r, zuint c) const {\
    CheckRange(r, c); \
    return ope(Mat_(r,c)); \
  }\
  ZMATRIXR_METHOD(MeType); \
private:\
  const zMatrixBaseR<Complex<T>,Major, E> &Mat_; \
  using zMatrixBaseR<Complex<T>,Major, zComplex##name##Expression<T, Major, E> >::CheckRange; \
}; \
template<typename T,class Major, typename E>\
const zComplex##name##Expression<T,Major,E> name(const zMatrixBaseR<Complex<T>,Major, E> &mat) {\
  return zComplex##name##Expression<T,Major,E>(mat); \
}

template<typename T>
inline Complex<T> conj(const Complex<T> &v) {
  return Complex<T>(v.Real(),-v.Imag());
}

COMPLEX_EXPRESSION(Conj,conj)

//complex to real
#define COMPLEX2_EXPRESSION(name,ope) \
template<typename T,class Major, typename E>\
class zComplex##name##Expression : public zMatrixBaseR<T,Major, zComplex##name##Expression<T, Major, E> > {\
  typedef zComplex##name##Expression<T, Major, E> MeType; \
public:\
  explicit zComplex##name##Expression(const zMatrixBaseR<Complex<T>,Major, E>& mat)\
  :zMatrixBaseR<T,Major, zComplex##name##Expression<T, Major, E> >(mat.rows_,mat.cols_),Mat_(mat){}\
  const T operator()(zuint r, zuint c) const {\
    CheckRange(r, c); \
    return ope(Mat_(r,c)); \
  }\
  ZMATRIXR_METHOD(MeType); \
private:\
  const zMatrixBaseR<Complex<T>,Major, E> &Mat_; \
  using zMatrixBaseR<Complex<T>,Major, zComplex##name##Expresson<T, Major, E> >::CheckRange; \
}; \
template<typename T, class Major, typename E>\
const zComplex##name##Expression<T,Major> name(const zMatrixBaseR<Complex<T>,Major, E> &mat) {\
  return zComplex##name##Expression<T,Major>(mat); \
}

template<typename T>
inline T real(const Complex<T> &v) {
  return v.Real();
}

template<typename T>
inline T imag(const Complex<T> &v) {
  return v.Imag();
}

COMPLEX_EXPRESSION(Real,real)
COMPLEX_EXPRESSION(Imag,imag)

}
