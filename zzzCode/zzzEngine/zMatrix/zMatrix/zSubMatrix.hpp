#pragma once
#include <zCore/Math/Vector2.hpp>

//sub matrix support, works like matlab

namespace zzz{
template<typename T,class Major,typename E>
class zMatrixBaseR;

class EndlessArray {
public:
  explicit EndlessArray(int begin):begin_(begin){}
  int begin_;
};
inline const EndlessArray Colon(int begin=0) {
  return EndlessArray(begin);
}

//continuous column vector
template<typename T,class Major>
class zVConstantContinuousVector : public zMatrixBaseR<T,Major,zVConstantContinuousVector<T,Major> > {
public:
  // constructor
  explicit zVConstantContinuousVector(T v0, T v1, T step=1)
    : zMatrixBaseR<T,Major, zVConstantContinuousVector<T, Major> >(zuint((v1-v0)/step+1),1),V0_(v0),V1_(v1),Step_(step){}
  // operator()
  const T operator()(zuint r, zuint c) const { CheckRange(r, c); return V0_+r*Step_; }
public:
  T V0_,V1_,Step_;
};

template<typename T,class Major>
inline zVConstantContinuousVector<T,Major> Colon(T begin, T end, T step=1) {
  return zVConstantContinuousVector<T,Major>(begin,end,step);
}

inline zVConstantContinuousVector<double,zColMajor> Colond(double begin, double end, double step=1) {
  return zVConstantContinuousVector<double,zColMajor>(begin,end,step);
}

inline zVConstantContinuousVector<float,zColMajor> Colonf(float begin, float end, float step=1) {
  return zVConstantContinuousVector<float,zColMajor>(begin,end,step);
}

inline zVConstantContinuousVector<int,zColMajor> Colon(int begin, int end, int step=1) {
  return zVConstantContinuousVector<int,zColMajor>(begin,end,step);
}

inline zVConstantContinuousVector<int,zColMajor> Colon(const Vector2i &range, int step=1) {
  return zVConstantContinuousVector<int,zColMajor>(range[0],range[1],step);
}

template<typename T,class Major>
class zHConstantContinuousVector : public zMatrixBaseR<T,Major,zHConstantContinuousVector<T,Major> > { //should not matter if rowmajor or colmajor
public:
  //constructor
  explicit zHConstantContinuousVector(T v0, T v1, T step=1)
  :zMatrixBaseR<T,Major>(1,zuint((v1-v0)/step+1)),V0_(v0),V1_(v1),Step_(step){}
  //operator()
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return V0_ + c * Step_;
  }
public:
  T V0_, V1_, Step_;
};

template<typename T,class Major>
inline zHConstantContinuousVector<T,Major> HColon(T begin, T end, T step=1) {
  return zHConstantContinuousVector<T,Major>(begin,end,step);
}

///////////////////////////////////////////////////
// SubMatrixR
template<typename T,class Major, typename E>
class zSubMatrixIIR : public zMatrixBaseR<T,Major,zSubMatrixIIR<T, Major, E> > {
  typedef zSubMatrixIIR<T, Major, E> MeType;
public:
  zSubMatrixIIR(const zMatrixBaseR<T,Major,E> &mat, const int r, const int c):Mat_(mat),
    zMatrixBaseR<T,Major,zSubMatrixIIR<T, Major, E> >(1,1),ri_(r),ci_(c) {
      mat.CheckRange(ri_, ci_);
  }

  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2 = ri_, c2 = ci_;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major,E> &Mat_;
  int ri_, ci_;
};

template<typename T,class Major,typename E>
class zSubMatrixEER : public zMatrixBaseR<T,Major,zSubMatrixEER<T,Major,E> > {
  typedef zSubMatrixEER<T,Major,E> MeType;
public:
  zSubMatrixEER(const zMatrixBaseR<T,Major,E> &mat, const EndlessArray &r, const EndlessArray &c):Mat_(mat),
    zMatrixBaseR<T,Major,zSubMatrixEER<T,Major,E> >(mat.rows_-r.begin_,mat.cols_-c.begin_),ri_(r.begin_),ci_(c.begin_) {
      mat.CheckRange(ri_, ci_);
  }

  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2 = ri_ + r, c2 = ci_ + c;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major,E> &Mat_;
  int ri_, ci_;
};

template<typename T,class Major,typename E>
class zSubMatrixEIR : public zMatrixBaseR<T,Major,zSubMatrixEIR<T,Major,E> > {
  typedef zSubMatrixEIR<T, Major, E> MeType;
public:
  zSubMatrixEIR(const zMatrixBaseR<T,Major,E> &mat, const EndlessArray &r, const int c):Mat_(mat),
    zMatrixBaseR<T,Major, zSubMatrixEIR<T, Major, E> >(mat.rows_-r.begin_,1),ri_(r.begin_),ci_(c) {
      mat.CheckRange(ri_, ci_);
  }

  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2 = ri_ + r, c2 = ci_;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major,E> &Mat_;
  int ri_, ci_;
};

template<typename T,class Major,typename E>
class zSubMatrixIER : public zMatrixBaseR<T,Major,zSubMatrixIER<T,Major,E> > {
  typedef zSubMatrixIER<T, Major, E> MeType;
public:
  zSubMatrixIER(const zMatrixBaseR<T,Major, E> &mat, const int r, const EndlessArray &c):Mat_(mat),
    zMatrixBaseR<T,Major, zSubMatrixIER<T, Major, E> >(1,mat.cols_-c.begin_),ri_(r),ci_(c.begin_) {
      mat.CheckRange(ri_, ci_);
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2 = ri_, c2 = ci_ + c;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major,E> &Mat_;
  int ri_, ci_;
};

template<typename T,class Major,typename E, typename E1>
class zSubMatrixEMR : public zMatrixBaseR<T,Major,zSubMatrixEMR<T, Major, E, E1> > {
  typedef zSubMatrixEMR<T, Major, E, E1> MeType;
public:
  zSubMatrixEMR(const zMatrixBaseR<T,Major,E> &mat, const EndlessArray &r, const zMatrixBaseR<int,Major, E1> &c):Mat_(mat),
    zMatrixBaseR<T,Major, zSubMatrixEMR<T, Major, E, E1> >(mat.rows_-r.begin_,c.rows_),ri_(r.begin_),c_(c) {
      ZCHECK_GE(ri_, 0);
      ZCHECK_LT(ri_, (int)mat.rows_);
      ZCHECK_EQ(c.cols_, 1);
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2=ri_+r, c2=c_(c,0);
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major,E> &Mat_;
  int ri_;
  const zMatrixBaseR<int,Major, E1> &c_;
};

template<typename T,class Major, typename E, typename E1>
class zSubMatrixMER : public zMatrixBaseR<T,Major,zSubMatrixMER<T, Major, E, E1> > {
  typedef zSubMatrixMER<T, Major, E, E1> MeType;
public:
  zSubMatrixMER(const zMatrixBaseR<T,Major, E> &mat, const zMatrixBaseR<int,Major, E1> &r, const EndlessArray &c):Mat_(mat),
    zMatrixBaseR<T,Major, zSubMatrixMER<T, Major, E, E1> >(r.rows_,mat.cols_-c.begin_),r_(r),ci_(c.begin_) {
      ZCHECK_EQ(r.cols_, 1);
      ZCHECK_GE(ci_, 0);
      ZCHECK_LT(ci_, (int)mat.cols_);
  }

  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2=r_(r,0), c2=ci_+c;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major, E> &Mat_;
  int ci_;
  const zMatrixBaseR<int,Major, E1> &r_;
};

template<typename T,class Major, typename E, typename E1>
class zSubMatrixMIR : public zMatrixBaseR<T,Major, zSubMatrixMIR<T, Major, E, E1> > {
  typedef zSubMatrixMIR<T, Major, E, E1> MeType;
public:
  zSubMatrixMIR(const zMatrixBaseR<T,Major,E> &mat, const zMatrixBaseR<int,Major, E1> &r, const int c):Mat_(mat),
    zMatrixBaseR<T,Major, zSubMatrixMIR<T,Major, E, E1> >(r.rows_,1),r_(r),ci_(c) {
      ZCHECK_EQ(r.cols_, 1);
      ZCHECK_GE(ci_, 0);
      ZCHECK_LT(ci_, (int)mat.cols_);
  }

  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2=r_(r,0), c2=ci_;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major, E> &Mat_;
  int ci_;
  const zMatrixBaseR<int,Major, E1> &r_;
};

template<typename T,class Major, typename E, typename E1>
class zSubMatrixIMR : public zMatrixBaseR<T,Major, zSubMatrixIMR<T, Major, E, E1> > {
  typedef zSubMatrixIMR<T, Major, E, E1> MeType;
public:
  zSubMatrixIMR(const zMatrixBaseR<T,Major,E> &mat, const int r, const zMatrixBaseR<int,Major, E1> &c):Mat_(mat),
    zMatrixBaseR<T,Major, zSubMatrixIMR<T, Major, E, E1> >(1,c.rows_),ri_(r),c_(c) {
      ZCHECK_GE(ri_, 0);
      ZCHECK_LT(ri_, (int)mat.rows_);
      ZCHECK_EQ(c.cols_, 1);
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2=ri_, c2=c_(c,0);
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major, E> &Mat_;
  int ri_;
  const zMatrixBaseR<int,Major, E1> &c_;
};

template<typename T,class Major, typename E, typename E1, typename E2>
class zSubMatrixMMR : public zMatrixBaseR<T,Major, zSubMatrixMMR<T, Major, E, E1, E2> > {
  typedef zSubMatrixMMR<T, Major, E, E1, E2> MeType;
public:
  zSubMatrixMMR(const zMatrixBaseR<T,Major,E> &mat, const zMatrixBaseR<int,Major,E1> &r, const zMatrixBaseR<int,Major,E2> &c):Mat_(mat),
    zMatrixBaseR<T,Major, zSubMatrixMMR<T, Major, E, E1, E2> >(r.rows_,c.rows_),r_(r),c_(c) {
      ZCHECK_EQ(r.cols_, 1);
      ZCHECK_EQ(c.cols_, 1);
  }

  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2=r_(r,0), c2=c_(c,0);
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major, E> &Mat_;
  const zMatrixBaseR<int,Major, E1> &r_;
  const zMatrixBaseR<int, Major, E2> &c_;
};
///////////////////////////////////////////////////////
// SubMatrixW
template<typename T,class Major, typename E>
class zSubMatrixIIW : public zMatrixBaseW<T,Major,zSubMatrixIIW<T, Major, E> > {
  typedef zSubMatrixIIW<T, Major, E> MeType;
public:
  zSubMatrixIIW(zMatrixBaseW<T,Major,E> &mat, const int r, const int c):Mat_(mat),
    zMatrixBaseW<T,Major,zSubMatrixIIW<T, Major, E> >(1,1),ri_(r),ci_(c) {
      mat.CheckRange(ri_, ci_);
  }

  T& operator()(zuint r, zuint c) {
    CheckRange(r, c);
    int r2 = ri_, c2 = ci_;
    return Mat_(zuint(r2),zuint(c2));
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2 = ri_, c2 = ci_;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXW_METHOD(MeType);
private:
  zMatrixBaseW<T,Major,E> &Mat_;
  int ri_, ci_;
};

template<typename T,class Major,typename E>
class zSubMatrixEEW : public zMatrixBaseW<T,Major,zSubMatrixEEW<T,Major,E> > {
  typedef zSubMatrixEEW<T,Major,E> MeType;
public:
  zSubMatrixEEW(zMatrixBaseW<T,Major,E> &mat, const EndlessArray &r, const EndlessArray &c):Mat_(mat),
    zMatrixBaseW<T,Major,zSubMatrixEEW<T,Major,E> >(mat.rows_-r.begin_,mat.cols_-c.begin_),ri_(r.begin_),ci_(c.begin_) {
      mat.CheckRange(ri_, ci_);
  }

  T& operator()(zuint r, zuint c) {
    CheckRange(r, c);
    int r2 = ri_ + r, c2 = ci + c;
    return Mat_(zuint(r2),zuint(c2));
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2 = ri_ + r, c2 = ci_ + c;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXW_METHOD(MeType);
private:
  zMatrixBaseW<T,Major,E> &Mat_;
  int ri_, ci_;
};

template<typename T,class Major,typename E>
class zSubMatrixEIW : public zMatrixBaseW<T,Major,zSubMatrixEIW<T,Major,E> > {
  typedef zSubMatrixEIW<T, Major, E> MeType;
public:
  zSubMatrixEIW(zMatrixBaseW<T,Major,E> &mat, const EndlessArray &r, const int c):Mat_(mat),
    zMatrixBaseW<T,Major, zSubMatrixEIW<T, Major, E> >(mat.rows_-r.begin_,1),ri_(r.begin_),ci_(c) {
      mat.CheckRange(ri_, ci_);
  }

  T& operator()(zuint r, zuint c) {
    CheckRange(r, c);
    int r2 = ri + r_, c2 = ci_;
    return Mat_(zuint(r2),zuint(c2));
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2 = ri_ + r, c2 = ci_;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXW_METHOD(MeType);
private:
  zMatrixBaseW<T,Major,E> &Mat_;
  int ri_, ci_;
};

template<typename T,class Major,typename E>
class zSubMatrixIEW : public zMatrixBaseW<T,Major,zSubMatrixIEW<T,Major,E> > {
  typedef zSubMatrixIEW<T, Major, E> MeType;
public:
  zSubMatrixIEW(zMatrixBaseW<T,Major, E> &mat, const int r, const EndlessArray &c):Mat_(mat),
    zMatrixBaseW<T,Major, zSubMatrixIEW<T, Major, E> >(1,mat.cols_-c.begin_),ri_(r),ci_(c.begin_) {
      mat.CheckRange(ri_, ci_);
  }

  T& operator()(zuint r, zuint c) {
    CheckRange(r, c);
    int r2 = ri_, c2 = ci_ + c;
    return Mat_(zuint(r2),zuint(c2));
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2 = ri_, c2 = ci_ + c;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXW_METHOD(MeType);
private:
  zMatrixBaseW<T,Major,E> &Mat_;
  int ri_, ci_;
};

template<typename T,class Major,typename E, typename E1>
class zSubMatrixEMW : public zMatrixBaseW<T,Major,zSubMatrixEMW<T, Major, E, E1> > {
  typedef zSubMatrixEMW<T, Major, E, E1> MeType;
public:
  zSubMatrixEMW(zMatrixBaseW<T,Major,E> &mat, const EndlessArray &r, const zMatrixBaseR<int,Major,E1> &c):Mat_(mat),
    zMatrixBaseW<T,Major, zSubMatrixEMW<T, Major, E, E1> >(mat.rows_-r.begin_,c.rows_),ri_(r.begin_),c_(c) {
      ZCHECK_GE(ri_, 0);
      ZCHECK_LT(ri_, (int)mat.rows_);
      ZCHECK_EQ(c.cols_, 1);
  }

  T& operator()(zuint r, zuint c) {
    CheckRange(r, c);
    int r2=ri_+r, c2=c_(c,0);
    return Mat_(zuint(r2),zuint(c2));
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2=ri_+r, c2=c_(c,0);
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXW_METHOD(MeType);
private:
  zMatrixBaseW<T,Major,E> &Mat_;
  int ri_;
  const zMatrixBaseR<int,Major,E1> &c_;
};

template<typename T,class Major, typename E, typename E1>
class zSubMatrixMEW : public zMatrixBaseW<T,Major,zSubMatrixMEW<T, Major, E, E1> > {
  typedef zSubMatrixMEW<T, Major, E, E1> MeType;
public:
  zSubMatrixMEW(zMatrixBaseW<T,Major, E> &mat, const zMatrixBaseR<int,Major, E1> &r, const EndlessArray &c):Mat_(mat),
    zMatrixBaseW<T,Major, zSubMatrixMEW<T, Major, E, E1> >(r.rows_,mat.cols_-c.begin_),r_(r),ci_(c.begin_) {
      ZCHECK_EQ(r.cols_, 1);
      ZCHECK_GE(ci_, 0);
      ZCHECK_LT(ci_, (int)mat.cols_);
  }

  T& operator()(zuint r, zuint c) {
    CheckRange(r, c);
    int r2=r_(r,0), c2=ci_+c;
    return Mat_(zuint(r2),zuint(c2));
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2=r_(r,0), c2=ci_+c;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXW_METHOD(MeType);
private:
  zMatrixBaseW<T,Major, E> &Mat_;
  int ci_;
  const zMatrixBaseR<int,Major, E1> &r_;
};

template<typename T,class Major, typename E, typename E1>
class zSubMatrixMIW : public zMatrixBaseW<T,Major, zSubMatrixMIW<T, Major, E, E1> > {
  typedef zSubMatrixMIW<T, Major, E, E1> MeType;
public:
  zSubMatrixMIW(zMatrixBaseW<T,Major,E> &mat, const zMatrixBaseR<int,Major, E1> &r, const int c):Mat_(mat),
    zMatrixBaseW<T,Major, zSubMatrixMIW<zSubMatrixMIW<T,Major, E, E1> >(r.rows_,1),r_(r),ci_(c) {
      ZCHECK_EQ(r.cols_, 1);
      ZCHECK_GE(ci_, 0);
      ZCHECK_LT(ci_, (int)mat.cols_);
  }

  T& operator()(zuint r, zuint c) {
    CheckRange(r, c);
    int r2=r_(r,0), c2=ci_; break;
    return Mat_(zuint(r2),zuint(c2));
  }

  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2=r_(r,0), c2=ci_; break;
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXW_METHOD(MeType);
private:
  zMatrixBaseW<T,Major, E> &Mat_;
  int ci_;
  const zMatrixBaseR<int,Major, E1> &r_;
};

template<typename T,class Major, typename E, typename E1>
class zSubMatrixIMW : public zMatrixBaseW<T,Major, zSubMatrixIMW<T, Major, E, E1> > {
  typedef zSubMatrixIMW<T, Major, E, E1> MeType;
public:
  zSubMatrixIMW(zMatrixBaseW<T,Major,E> &mat, const int r, const zMatrixBaseR<int,Major, E1> &c):Mat_(mat),
    zMatrixBaseW<T,Major, zSubMatrixIMW<T, Major, E, E1> >(1,c.rows_),ri_(r),c_(c) {
      ZCHECK_GE(ri_, 0);
      ZCHECK_LT(ri_, (int)mat.rows_);
      ZCHECK_EQ(c.cols_, 1);
  }
  T& operator()(zuint r, zuint c) {
    CheckRange(r, c);
    int r2=ri_, c2=c_(c,0);
    return Mat_(zuint(r2),zuint(c2));
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2=ri_, c2=c_(c,0);
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXW_METHOD(MeType);
private:
  zMatrixBaseW<T,Major, E> &Mat_;
  int ri_;
  const zMatrixBaseR<int,Major, E1> &c_;
};

template<typename T,class Major, typename E, typename E1, typename E2>
class zSubMatrixMMW : public zMatrixBaseW<T,Major, zSubMatrixMMW<T, Major, E, E1, E2> > {
  typedef zSubMatrixMMW<T, Major, E, E1, E2> MeType;
public:
  zSubMatrixMMW(zMatrixBaseW<T,Major,E> &mat, const zMatrixBaseR<int,Major,E1> &r, const zMatrixBaseR<int,Major,E2> &c):Mat_(mat),
    zMatrixBaseW<T,Major, zSubMatrixMMW<T, Major, E, E1, E2> >(r.rows_,c.rows_),r_(r),c_(c) {
      ZCHECK_EQ(r.cols_, 1);
      ZCHECK_EQ(c.cols_, 1);
  }

  T& operator()(zuint r, zuint c) {
    CheckRange(r, c);
    int r2=r_(r,0), c2=c_(c,0);
    return Mat_(zuint(r2),zuint(c2));
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    int r2=r_(r,0), c2=c_(c,0);
    return Mat_(zuint(r2),zuint(c2));
  }
  ZMATRIXW_METHOD(MeType);
private:
  zMatrixBaseW<T,Major, E> &Mat_;
  const zMatrixBaseR<int,Major, E1> &r_;
  const zMatrixBaseR<int, Major, E2> &c_;
};

}
