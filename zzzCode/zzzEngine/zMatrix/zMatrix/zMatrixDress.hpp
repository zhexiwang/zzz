#pragma once
#include "zMatrixBase.hpp"
#include "zMatrix.hpp"
#include <zCore/Math/Vector.hpp>
#include <zCore/Math/Matrix.hpp>

namespace zzz{
//////////////////////////////////////////////////////////////////////////
//Vector Dress
template<typename T, unsigned int N,typename Major>
class zMatrixVectorDressR : public zMatrixBaseR<T,Major, zMatrixVectorDressR<T, N, Major> > {
  typedef zMatrixVectorDressR<T, N, Major> MeType;
public:
  explicit zMatrixVectorDressR(const VectorBase<N,T> &vec)
  :zMatrixBaseR<T,Major, zMatrixVectorDressR<T, N, Major> >(Major::Dim_==0?1:N,Major::Dim_==0?N:1),vec_(vec){}
  const T operator()(zuint r, zuint c) const {
    if (Major::Dim_==0) {
      ZRCHECK_EQ(r, 0);
      ZRCHECK_LT(c, N);
      return vec_[c];
    } else {
      ZRCHECK_EQ(c, 0);
      ZRCHECK_LT(r, N);
      return vec_[r];
    }
  }
  ZMATRIXR_METHOD(MeType);
private:
  const VectorBase<N,T> &vec_;
};
template<typename T, unsigned int N,typename Major>
class zMatrixVectorDressW : public zMatrixBaseW<T,Major, zMatrixVectorDressW<T, N, Major> > {
  typedef zMatrixVectorDressW<T, N, Major> MeType;
public:
  explicit zMatrixVectorDressW(VectorBase<N,T> &vec)
  :zMatrixBaseW<T,Major, zMatrixVectorDressW<T, N, Major> >(Major::Dim_==0?1:N,Major::Dim_==0?N:1),vec_(vec){}
  const T operator()(zuint r, zuint c) const {
    if (Major::Dim_==0) {
      ZRCHECK_EQ(r, 0);
      ZRCHECK_LT(c, N);
      return vec_[c];
    } else {
      ZRCHECK_EQ(c, 0);
      ZRCHECK_LT(r, N);
      return vec_[r];
    }
  }

  T &operator()(zuint r, zuint c) {
    if (Major::Dim_==0) {
      ZRCHECK_EQ(r, 0);
      ZRCHECK_LT(c, N);
      return vec_[c];
    } else {
      ZRCHECK_EQ(c, 0);
      ZRCHECK_LT(r, N);
      return vec_[r];
    }
  }
  ZMATRIXW_METHOD(MeType);
private:
  VectorBase<N,T> &vec_;
};
template<typename T, unsigned int N>
const zMatrixVectorDressR<T,N,zColMajor> Dress(const VectorBase<N,T> &v)
{return zMatrixVectorDressR<T,N,zColMajor>(v);}
template<typename T, unsigned int N>
zMatrixVectorDressW<T,N,zColMajor> Dress(VectorBase<N,T> &v)
{return zMatrixVectorDressW<T,N,zColMajor>(v);}

//////////////////////////////////////////////////////////////////////////
//Matrix Dress
template<typename T, unsigned int R, unsigned int C, class Major>
class zMatrixMatrixDressR : public zMatrixBaseR<T,Major, zMatrixMatrixDressR<T, R, C, Major> > {
  typedef zMatrixMatrixDressR<T, R, C, Major> MeType;
public:
  explicit zMatrixMatrixDressR(const MatrixBase<R,C,T> &mat)
    : zMatrixBaseR<T, Major, zMatrixMatrixDressR<T, R, C, Major> >(R,C),mat_(mat){}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return mat_.At(r,c);
  }
  ZMATRIXR_METHOD(MeType);
private:
  const MatrixBase<R,C,T> &mat_;
};

template<typename T, unsigned int R, unsigned int C, class Major>
class zMatrixMatrixDressW : public zMatrixBaseW<T,Major, zMatrixMatrixDressW<T, R, C, Major> > {
  typedef zMatrixMatrixDressW<T, R, C, Major> MeType;
public:
  explicit zMatrixMatrixDressW(MatrixBase<R,C,T> &mat)
    : zMatrixBaseW<T,Major, zMatrixMatrixDressW<T, R, C, Major> >(R,C),mat_(mat){}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return mat_.At(r,c);
  }

  T &operator()(zuint r, zuint c) {
    CheckRange(r, c);
    return mat_.At(r,c);
  }
  ZMATRIXW_METHOD(MeType);
private:
  MatrixBase<R,C,T> &mat_;
};
template<typename T, unsigned int R, unsigned int C>
const zMatrixMatrixDressR<T,R,C,zColMajor> Dress(const MatrixBase<R,C,T> &v)
{return zMatrixMatrixDressR<T,R,C,zColMajor>(v);}
template<typename T, unsigned int R, unsigned int C>
zMatrixMatrixDressW<T,R,C,zColMajor> Dress(MatrixBase<R,C,T> &v)
{return zMatrixMatrixDressW<T,R,C,zColMajor>(v);}

////////////////////////////////////////////////////
// ArrayBase Dress
template<typename T,class Major>
class zMatrixArrayDressR : public zMatrixBaseR<T,Major, zMatrixArrayDressR<T, Major> > {
  typedef zMatrixArrayDressR<T, Major> MeType;
public:
  explicit zMatrixArrayDressR(const ArrayBase<2,T> &arr):zMatrixBaseR<T,Major, zMatrixArrayDressR<T, Major> >(arr.Size(0),arr.Size(1)),arr_(arr){}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return arr_.At(Vector2i(r,c));
  }
  ZMATRIXR_METHOD(MeType);
private:
  const ArrayBase<2,T> &arr_;
};
template<typename T,class Major>
class zMatrixArrayDressW : public zMatrixBaseW<T,Major, zMatrixArrayDressW<T, Major> > {
  typedef zMatrixArrayDressW<T, Major> MeType;
public:
  explicit zMatrixArrayDressW(ArrayBase<2,T> &arr):zMatrixBaseW<T,Major, zMatrixArrayDressW<T, Major> >(arr.Size(0),arr.Size(1)),arr_(arr){}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return arr_.At(Vector2i(r,c));
  }
  T &operator()(zuint r, zuint c) {
    CheckRange(r, c);
    return arr_.At(Vector2i(r,c));
  }
  // Special operator =, since it can resize
  template<typename E>
  const zMatrixBaseW<T,Major,zMatrixArrayDressW<T,Major> > &operator=(const zMatrixBaseR<T,Major,E> &other) {
    arr_.SetSize(Vector2i(other.rows_,other.cols_));
    rows_=other.rows_;
    cols_=other.cols_;
    return zMatrixBaseW<T,Major, zMatrixArrayDressW<T, Major> >::operator=(other);
  }
  ZMATRIXW_METHOD(MeType);
private:
  ArrayBase<2,T> &arr_;
};
template<typename T>
const zMatrixArrayDressR<T,zColMajor> Dress(const ArrayBase<2,T> &v)
{return zMatrixArrayDressR<T,zColMajor>(v);}
template<typename T>
zMatrixArrayDressW<T,zColMajor> Dress(ArrayBase<2,T> &v)
{return zMatrixArrayDressW<T,zColMajor>(v);}
////////////////////////////////////////////////////
// Raw C++ Array Dress
template<typename T,class Major>
class zMatrixRawArrayDressR : public zMatrixBaseR<T,Major, zMatrixRawArrayDressR<T, Major> > {
  typedef zMatrixRawArrayDressR<T, Major> MeType;
public:
  zMatrixRawArrayDressR(const T *arr, zuint nrow, zuint ncol):zMatrixBaseR<T,Major, zMatrixRawArrayDressR<T, Major> >(nrow,ncol),arr_(arr){
    ZCHECK_NOT_NULL(arr);
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return arr_[Major::ToIndex(r,c,rows_,cols_)];
  }
  ZMATRIXR_METHOD(MeType);
private:
  const T *arr_;
};
template<typename T,class Major>
class zMatrixRawArrayDressW : public zMatrixBaseW<T,Major, zMatrixRawArrayDressW<T, Major> > {
  typedef zMatrixRawArrayDressW<T, Major> MeType;
public:
  zMatrixRawArrayDressW(T *arr, zuint nrow, zuint ncol):zMatrixBaseW<T,Major, zMatrixRawArrayDressW<T, Major> >(nrow,ncol),arr_(arr){
    ZCHECK_NOT_NULL(arr);
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return arr_[Major::ToIndex(r,c,rows_,cols_)];
  }
  T &operator()(zuint r, zuint c) {
    CheckRange(r, c);
    return arr_[Major::ToIndex(r,c,rows_,cols_)];
  }
  ZMATRIXW_METHOD(MeType);
private:
  T *arr_;
};
template<typename T>
const zMatrixRawArrayDressR<T,zColMajor> Dress(const T *v, zuint nrow, zuint ncol)
{return zMatrixRawArrayDressR<T,zColMajor>(v,nrow,ncol);}
template<typename T>
zMatrixRawArrayDressW<T,zColMajor> Dress(T *v, zuint nrow, zuint ncol)
{return zMatrixRawArrayDressW<T,zColMajor>(v,nrow,ncol);}
////////////////////////////////////////////
//Dress for single value
template<typename T,class Major>
class zMatrixSingleDressR : public zMatrixBaseR<T,Major, zMatrixSingleDressR<T, Major> > {
  typedef zMatrixSingleDressR<T, Major> MeType;
public:
  explicit zMatrixSingleDressR(const T &v):zMatrixBaseR<T,Major, zMatrixSingleDressR<T, Major> >(1,1),v_(v){}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return v_;
  }
  ZMATRIXR_METHOD(MeType);
private:
  const T &v_;
};
template<typename T,class Major>
class zMatrixSingleDressW : public zMatrixBaseW<T,Major, zMatrixSingleDressW<T, Major> > {
  typedef zMatrixSingleDressW<T, Major> MeType;
public:
  explicit zMatrixSingleDressW(T &v):zMatrixBaseW<T,Major, zMatrixSingleDressW<T, Major> >(1,1),v_(v){}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return v_;
  }
  T &operator()(zuint r, zuint c) {
    CheckRange(r, c);
    return v_;
  }
  ZMATRIXW_METHOD(MeType);
private:
  T &v_;
};
//////////////////////////////////////////////////////////////////////////
// std::vector Dress
template<typename T, typename Major>
class zMatrixStdVectorDressR : public zMatrixBaseR<T,Major, zMatrixStdVectorDressR<T, Major> > {
  typedef zMatrixStdVectorDressR<T, Major> MeType;
public:
  explicit zMatrixStdVectorDressR(const std::vector<T> &vec)
    :zMatrixBaseR<T,Major, zMatrixStdVectorDressR<T, Major> >(Major::Dim_==0?1:vec.size(),Major::Dim_==0?vec.size():1),vec_(vec){}
  const T operator()(zuint r, zuint c) const {
    if (Major::Dim_==0) {
      ZRCHECK_EQ(r, 0);
      ZRCHECK_LT(c, vec_.size());
      return vec_[c];
    } else {
      ZRCHECK_EQ(c, 0);
      ZRCHECK_LT(r, vec_.size());
      return vec_[r];
    }
  }
  ZMATRIXR_METHOD(MeType);
private:
  const std::vector<T> &vec_;
};
template<typename T, typename Major>
class zMatrixStdVectorDressW : public zMatrixBaseW<T,Major, zMatrixStdVectorDressW<T, Major> > {
  typedef zMatrixStdVectorDressW<T, Major> MeType;
public:
  explicit zMatrixStdVectorDressW(std::vector<T> &vec)
    :zMatrixBaseW<T,Major, zMatrixStdVectorDressW<T, Major> >(Major::Dim_==0?1:vec.size(),Major::Dim_==0?vec.size():1),vec_(vec){}
  const T operator()(zuint r, zuint c) const {
    if (Major::Dim_==0) {
      ZRCHECK_EQ(r, 0);
      ZRCHECK_LT(c, vec_.size());
      return vec_[c];
    } else {
      ZRCHECK_EQ(c, 0);
      ZRCHECK_LT(r, vec_.size());
      return vec_[r];
    }
  }

  T &operator()(zuint r, zuint c) {
    if (Major::Dim_==0) {
      ZRCHECK_EQ(r, 0);
      ZRCHECK_LT(c, vec_.size());
      return vec_[c];
    } else {
      ZRCHECK_EQ(c, 0);
      ZRCHECK_LT(r, vec_.size());
      return vec_[r];
    }
  }
  template<typename E>
  const zMatrixStdVectorDressW<T,Major>& operator=(const zMatrixBaseR<T,Major, E> &other) {
    if (Major::Dim_==0) {
      ZRCHECK_EQ(other.rows_, 1);
      vec_.resize(other.cols_);
      cols_=other.cols_;
    } else {
      ZRCHECK_EQ(other.cols_, 1);
      vec_.resize(other.rows_);
      rows_=other.rows_;
    }
    return zMatrixBaseW<T,Major, zMatrixStdVectorDressW<T, Major> >::operator=(other);
  }
  ZMATRIXW_METHOD(MeType);
private:
  std::vector<T> &vec_;
};
template<typename T>
const zMatrixStdVectorDressR<T,zColMajor> Dress(const std::vector<T> &v)
{return zMatrixStdVectorDressR<T,zColMajor>(v);}
template<typename T>
zMatrixStdVectorDressW<T,zColMajor> Dress(std::vector<T> &v)
{return zMatrixStdVectorDressW<T,zColMajor>(v);}


//ATTENTION: cannot make a template Dress for single value
//since every object can be interpreted this way
#define SINGLEDRESS(T) \
inline const zMatrixSingleDressR<T,zColMajor> Dress(const T &v)\
{return zMatrixSingleDressR<T,zColMajor>(v);}\
inline zMatrixSingleDressW<T,zColMajor> Dress(T &v)\
{return zMatrixSingleDressW<T,zColMajor>(v);}

SINGLEDRESS(zint8);
SINGLEDRESS(zuint8);
SINGLEDRESS(zint16);
SINGLEDRESS(zuint16);
SINGLEDRESS(zint32);
SINGLEDRESS(zuint32);
SINGLEDRESS(float);
SINGLEDRESS(double);
}
