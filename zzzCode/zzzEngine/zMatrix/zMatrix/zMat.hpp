#pragma once

// Since there are many inter-reference inside these files,
// it is better to include this file when use zMatrix.


#include "zMatrixBase.hpp"
#include "zSubMatrix.hpp"
#include "zMatrix.hpp"
#include "zExpressionFunctionC.hpp"
#include "zExpressionFunctionM.hpp"
#include "zExpressionC.hpp"
#include "zExpressionFunction.hpp"
#include "zExpressionM.hpp"
#include "zExpressionComplex.hpp"
#include "zExpressionSpecial.hpp"
#include "zMatrixConstant.hpp"
#include "zMatrixOperation.hpp"
#include "zVector.hpp"
#include "zMatrixDress.hpp"
#include "zMatrixCombine.hpp"
#include "zMatrixCast.hpp"
#include "zMatrixReshape.hpp"
#include "zMatrixLapack.hpp"
#include "zMatrixFunction.hpp"
#include "zSparseMatrix.hpp"

#ifdef ZZZ_DYNAMIC
  #ifdef _DEBUG
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zMatrixdllD.lib")
    #else
      #pragma comment(lib,"zMatrixdllD_x64.lib")
    #endif // ZZZ_OS_WIN64
  #else
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zMatrixdll.lib")
    #else
      #pragma comment(lib,"zMatrixdll_x64.lib")
    #endif // ZZZ_OS_WIN64
  #endif
#else
  #ifdef _DEBUG
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zMatrixD.lib")
    #else
      #pragma comment(lib,"zMatrixD_x64.lib")
    #endif // ZZZ_OS_WIN64
  #else
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib,"zMatrix.lib")
    #else
      #pragma comment(lib,"zMatrix_x64.lib")
    #endif // ZZZ_OS_WIN64
  #endif
#endif