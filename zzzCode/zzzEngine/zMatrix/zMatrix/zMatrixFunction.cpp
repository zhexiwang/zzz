#include "zMatrixFunction.hpp"

namespace zzz {
zMatrix<double, zColMajor> Rand(zuint row, zuint col) {
  RandomReal<double> r(0.0, 1.0);
  zMatrix<double, zColMajor> mat(row, col);
  for (zuint i=0; i<mat.size(); i++)
    mat[i]=r.Rand();
  return mat;
}
};  // namespace zzz