#pragma once
#include "zMatrixBase.hpp"

//for constant matrix like ones and zeros

namespace zzz{
template<typename T,class Major>
class zConstantMatrix : public zMatrixBaseR<T,Major, zConstantMatrix<T, Major> > { //should not matter if rowmajor or colmajor
  typedef zConstantMatrix<T, Major> MeType;
public:
  //constructor
  zConstantMatrix(zuint r, zuint c, T v):zMatrixBaseR<T,Major, zConstantMatrix<T, Major> >(r,c),V_(v){}  
  //operator()
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return V_;
  }
  ZMATRIXR_METHOD(MeType);
private:
  const T V_;
};

template<typename T>
inline const zConstantMatrix<T,zColMajor> Ones(zuint r, zuint c, const T& v=1) {
  return zConstantMatrix<T,zColMajor>(r,c,v);
}

inline const zConstantMatrix<double,zColMajor> Onesd(zuint r, zuint c, const double& v=1) {
  return zConstantMatrix<double,zColMajor>(r,c,v);
}

inline const zConstantMatrix<float,zColMajor> Onesf(zuint r, zuint c, const float& v=1) {
  return zConstantMatrix<float,zColMajor>(r,c,v);
}

template<typename T>
inline const zConstantMatrix<T,zColMajor> Zeros(zuint r, zuint c) {
  return zConstantMatrix<T,zColMajor>(r,c,0);
}

inline const zConstantMatrix<double,zColMajor> Zerosd(zuint r, zuint c) {
  return zConstantMatrix<double,zColMajor>(r,c,0);
}

inline const zConstantMatrix<float,zColMajor> Zerosf(zuint r, zuint c) {
  return zConstantMatrix<float,zColMajor>(r,c,0);
}

template<typename T,class Major, typename E>
class zConstantDiagMatrix : public zMatrixBaseR<T,Major, zConstantDiagMatrix<T, Major, E> > { //should not matter if rowmajor or colmajor
  typedef zConstantDiagMatrix<T, Major, E> MeType;
public:
  //constructor
  explicit zConstantDiagMatrix(const zMatrixBaseR<T,Major, E> &mat)
  :zMatrixBaseR<T,Major, zConstantDiagMatrix<T, Major, E> >(mat.rows_,mat.rows_),Mat_(mat) {
    ZCHECK_EQ(mat.cols_, 1);
  }
  //operator()
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    if (r==c) return Mat_(r,0);
    else return 0;
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major, E> &Mat_;
};

template<typename T,class Major, typename E>
const zConstantDiagMatrix<T,Major,E> Diag(const zMatrixBaseR<T,Major,E> &mat) {
  return zConstantDiagMatrix<T,Major,E>(mat);
}

template<typename T>
const zConstantDiagMatrix<T,zColMajor, zConstantMatrix<T,zColMajor> > Eye(int n, const T &v=1) {
  return zConstantDiagMatrix<T,zColMajor, zConstantMatrix<T,zColMajor> >(Ones<T>(n,1,v));
}

}
