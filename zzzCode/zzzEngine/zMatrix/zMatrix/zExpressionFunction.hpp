#pragma once
#include "zMatrixBase.hpp"

//for f(mat)

namespace zzz{
#define CONSTANT_EXPRESSION(name,ope) \
template<typename T,class Major, typename E>\
class zFunction##name##Expression : public zMatrixBaseR<T,Major, zFunction##name##Expression<T, Major, E> > {\
  typedef zFunction##name##Expression<T, Major, E> MeType; \
public:\
  explicit zFunction##name##Expression(const zMatrixBaseR<T,Major,E>& mat)\
  :zMatrixBaseR<T,Major, zFunction##name##Expression<T, Major, E> >(mat.rows_,mat.cols_),Mat_(mat){}\
  const T operator()(zuint r, zuint c) const {\
    CheckRange(r, c); \
    return ope(Mat_(r,c)); \
  }\
  ZMATRIXR_METHOD(MeType); \
private:\
  const zMatrixBaseR<T,Major,E> &Mat_; \
}; \
template<typename T, class Major, typename E>\
const zFunction##name##Expression<T,Major,E> name(const zMatrixBaseR<T,Major,E> &mat) {\
  return zFunction##name##Expression<T,Major,E>(mat); \
}

CONSTANT_EXPRESSION(Sin,sin)
CONSTANT_EXPRESSION(Cos,cos)
CONSTANT_EXPRESSION(Tan,tan)
CONSTANT_EXPRESSION(Pos,+)
CONSTANT_EXPRESSION(Neg,-)

template<typename T,class Major, typename E>
const zFunctionPosExpression<T,Major,E> operator+(const zMatrixBaseR<T,Major,E> &mat) {
  return zFunctionPosExpression<T,Major,E>(mat);
}

template<typename T,class Major, typename E>
const zFunctionNegExpression<T,Major,E> operator-(const zMatrixBaseR<T,Major,E> &mat) {
  return zFunctionNegExpression<T,Major,E>(mat);
}

template<typename T, class Major, typename E>
T Trace(const zMatrixBaseR<T,Major, E> &mat) {
  ZCHECK_EQ(mat.rows_, mat.cols_);
  T t=0;
  for (zuint i=0; i<mat.rows_; i++) t+=mat(i,i);
  return t;
}
}
