#pragma once
#include "zMatrixBase.hpp"

//for mat1+mat2, two matricies

namespace zzz{
#define MATRIX_EXPRESSION(name,ope) \
template<typename T,class Major, typename E1, typename E2>\
class zMatrix##name##Expression : public zMatrixBaseR<T,Major, zMatrix##name##Expression<T, Major, E1, E2> > {\
  typedef zMatrix##name##Expression<T, Major, E1, E2> MeType; \
public:\
  explicit zMatrix##name##Expression(const zMatrixBaseR<T,Major, E1> &mat1, const zMatrixBaseR<T,Major, E2> &mat2)\
  :zMatrixBaseR<T,Major, zMatrix##name##Expression<T,Major,E1,E2> >(mat1.rows_,mat1.cols_),Mat1_(mat1),Mat2_(mat2) {\
    ZCHECK_EQ(mat1.rows_, mat2.rows_); \
    ZCHECK_EQ(mat1.cols_, mat2.cols_); \
  }\
  const T operator()(zuint r, zuint c) const {\
    CheckRange(r, c); \
    return (Mat1_(r,c) ope Mat2_(r,c)); \
  }\
  ZMATRIXR_METHOD(MeType); \
private:\
  const zMatrixBaseR<T,Major, E1> &Mat1_; \
  const zMatrixBaseR<T,Major, E2> &Mat2_; \
};

MATRIX_EXPRESSION(Plus,+)
MATRIX_EXPRESSION(Minus,-)
MATRIX_EXPRESSION(Times,*)
MATRIX_EXPRESSION(DividedBy,/)

template<typename T,class Major, typename E1, typename E2>
const zMatrixPlusExpression<T,Major,E1,E2> operator+(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixPlusExpression<T,Major,E1,E2>(mat1,mat2);}

template<typename T,class Major, typename E1, typename E2>
const zMatrixMinusExpression<T,Major,E1,E2> operator-(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixMinusExpression<T,Major,E1,E2>(mat1,mat2);}

template<typename T,class Major, typename E1, typename E2>
const zMatrixTimesExpression<T,Major,E1,E2> DotTimes(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixTimesExpression<T,Major,E1,E2>(mat1,mat2);}

template<typename T,class Major, typename E1, typename E2>
const zMatrixDividedByExpression<T,Major,E1,E2> DotDividedBy(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixDividedByExpression<T,Major,E1,E2>(mat1,mat2);}

MATRIX_EXPRESSION(EQ,==)
MATRIX_EXPRESSION(NE,!=)
MATRIX_EXPRESSION(LT,<)
MATRIX_EXPRESSION(LE,<=)
MATRIX_EXPRESSION(GT,>)
MATRIX_EXPRESSION(GE,>=)

template<typename T,class Major,typename E1, typename E2>
const zMatrixEQExpression<T,Major,E1,E2> operator==(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixEQExpression<T,Major,E1,E2>(mat1,mat2);}

template<typename T,class Major,typename E1, typename E2>
const zMatrixNEExpression<T,Major,E1,E2> operator!=(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixNEExpression<T,Major,E1,E2>(mat1,mat2);}

template<typename T,class Major,typename E1, typename E2>
const zMatrixLTExpression<T,Major,E1,E2> operator<(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixLTExpression<T,Major,E1,E2>(mat1,mat2);}

template<typename T,class Major,typename E1, typename E2>
const zMatrixLEExpression<T,Major,E1,E2> operator<=(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixLEExpression<T,Major,E1,E2>(mat1,mat2);}

template<typename T,class Major,typename E1, typename E2>
const zMatrixGTExpression<T,Major,E1,E2> operator>(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixGTExpression<T,Major,E1,E2>(mat1,mat2);}

template<typename T,class Major,typename E1, typename E2>
const zMatrixGEExpression<T,Major,E1,E2> operator>=(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixGEExpression<T,Major,E1,E2>(mat1,mat2);}

template<typename T,class Major,typename E1, typename E2>
class zMatrixProductExpression : public zMatrixBaseR<T,Major, zMatrixProductExpression<T, Major, E1, E2> > {
  typedef zMatrixProductExpression<T, Major, E1, E2> MeType;
public:
  explicit zMatrixProductExpression(const zMatrixBaseR<T,Major, E1> &mat1, const zMatrixBaseR<T,Major, E2> &mat2)
  :zMatrixBaseR<T,Major, zMatrixProductExpression<T, Major, E1, E2> >(mat1.rows_,mat2.cols_),Mat1_(mat1),Mat2_(mat2),commonLength_(mat1.cols_) {
    ZCHECK_EQ(mat1.cols_, mat2.rows_);
  }
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    T v=0;
    for (zuint i=0; i<commonLength_; i++)
      v+=Mat1_(r,i)*Mat2_(i,c);
    return v;
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major, E1> &Mat1_;
  const zMatrixBaseR<T,Major, E2> &Mat2_;
  const zuint commonLength_;
};

template<typename T,class Major, typename E1, typename E2>
const zMatrixProductExpression<T,Major,E1,E2> operator*(const zMatrixBaseR<T,Major,E1> &mat1, const zMatrixBaseR<T,Major,E2> &mat2)
{return zMatrixProductExpression<T,Major,E1,E2>(mat1,mat2);}

}
