#pragma once

namespace zzz{
/////////////////////////////////////////////////
//GradientX
template<typename T,class Major, typename E>
class zSpecialGradxExpression : public zMatrixBaseR<T,Major,zSpecialGradxExpression<T, Major, E> > {
  typedef zSpecialGradxExpression<T, Major, E> MeType;
public:
  explicit zSpecialGradxExpression(const zMatrixBaseR<T,Major, E>& mat)
    :zMatrixBaseR<T,Major,zSpecialGradxExpression<T, Major, E> >(mat.rows_,mat.cols_),Mat_(mat){ZCHECK_GE(cols_, 2);}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    if (c==0) return Mat_(r,c+1)-Mat_(r,c);
    if (c==cols_-1) return Mat_(r,c)-Mat_(r,c-1);
    return (Mat_(r,c+1)-Mat_(r,c-1))/2;
  }
  ZMATRIXR_METHOD(MeType); 
private:
  const zMatrixBaseR<T,Major,E> &Mat_;
};
template<typename T,class Major, typename E>
const zSpecialGradxExpression<T,Major,E> GradientX(const zMatrixBaseR<T,Major,E> &mat) {
  return zSpecialGradxExpression<T,Major,E>(mat);
}

/////////////////////////////////////////////////
//GradientY
template<typename T,class Major, typename E>
class zSpecialGradyExpression : public zMatrixBaseR<T,Major, zSpecialGradyExpression<T, Major, E> > {
  typedef zSpecialGradyExpression<T, Major, E> MeType;
public:
  explicit zSpecialGradyExpression(const zMatrixBaseR<T,Major, E>& mat)
    :zMatrixBaseR<T,Major, zSpecialGradyExpression<T, Major, E> >(mat.rows_,mat.cols_),Mat_(mat){ZCHECK_GE(rows_, 2);}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    if (r==0) return Mat_(r+1,c)-Mat_(r,c);
    if (r==rows_-1) return Mat_(r,c)-Mat_(r-1,c);
    return (Mat_(r+1,c)-Mat_(r-1,c))/2;
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major,E> &Mat_;
};
template<typename T,class Major, typename E>
const zSpecialGradyExpression<T,Major, E> GradientY(const zMatrixBaseR<T,Major, E> &mat) {
  return zSpecialGradyExpression<T,Major, E>(mat);
}

//////////////////////////////////////////////////
//Lower triangle
template<typename T,class Major, typename E>
class zSpecialTrilExpression : public zMatrixBaseR<T,Major, zSpecialTrilExpression<T, Major, E> > {
  typedef zSpecialTrilExpression<T, Major, E> MeType;
public:
  explicit zSpecialTrilExpression(const zMatrixBaseR<T,Major, E>& mat)
    :zMatrixBaseR<T,Major, zSpecialTrilExpression<T, Major, E> >(mat.rows_,mat.cols_),Mat_(mat){ZCHECK_EQ(rows_, cols_);}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    if (c > r) return 0;
    return Mat_(r, c);
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major, E> &Mat_;
};
template<typename T,class Major, typename E>
const zSpecialTrilExpression<T,Major,E> Tril(const zMatrixBaseR<T,Major,E> &mat) {
  return zSpecialTrilExpression<T,Major,E>(mat);
}
}
