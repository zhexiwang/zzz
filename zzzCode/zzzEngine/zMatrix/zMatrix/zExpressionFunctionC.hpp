#pragma once
#include "zMatrixBase.hpp"

namespace zzz{
//func(v,mat)
#define CONSTANT_LEFT_EXPRESSIONC(name,func) \
template<typename T,class Major, typename E>\
class zFunctionLeft##name##ExpressionC : public zMatrixBaseR<T,Major, zFunctionLeft##name##ExpressionC<T, Major, E> > {\
  typedef zFunctionLeft##name##ExpressionC<T, Major, E> MeType; \
public:\
  explicit zFunctionLeft##name##ExpressionC(const T &v, const zMatrixBaseR<T,Major, E>& mat)\
  :zMatrixBaseR<T,Major,zFunctionLeft##name##ExpressionC<T,Major, E> >(mat.rows_,mat.cols_),Mat_(mat),v_(v){}\
  const T operator()(zuint r, zuint c) const {\
    CheckRange(r, c); \
    return func(v_, Mat_(r,c)); \
  }\
  ZMATRIXR_METHOD(MeType); \
private:\
  const T &v_; \
  const zMatrixBaseR<T,Major, E> &Mat_; \
};

CONSTANT_LEFT_EXPRESSIONC(Pow,Pow<T>)
template<typename T,class Major, typename E>
const zFunctionLeftPowExpressionC<T,Major,E> operator^(const T &value, const zMatrixBaseR<T,Major,E> &mat)
{return zFunctionLeftPowExpressionC<T,Major,E>(value,mat);};

//func(mat,c)
#define CONSTANT_RIGHT_EXPRESSIONC(name,func) \
template<typename T,class Major, typename E>\
class zFunctionRight##name##ExpressionC : public zMatrixBaseR<T,Major, zFunctionRight##name##ExpressionC<T,Major,E> > {\
  typedef zFunctionRight##name##ExpressionC<T,Major,E> MeType; \
public:\
  explicit zFunctionRight##name##ExpressionC(const zMatrixBaseR<T,Major, E>& mat,const T &v)\
  :zMatrixBaseR<T,Major, zFunctionRight##name##ExpressionC<T, Major, E> >(mat.rows_,mat.cols_),Mat_(mat),v_(v){}\
  const T operator()(zuint r, zuint c) const {\
    CheckRange(r, c); \
    return func(Mat_(r,c),v_); \
  }\
  ZMATRIXR_METHOD(MeType); \
private:\
  const T &v_; \
  const zMatrixBaseR<T,Major, E> &Mat_; \
};

CONSTANT_RIGHT_EXPRESSIONC(Pow,Pow<T>);
template<typename T,class Major, typename E>
const zFunctionRightPowExpressionC<T,Major, E> operator^(const zMatrixBaseR<T,Major, E> &mat,const T &value)
{return zFunctionRightPowExpressionC<T,Major, E>(mat,value);};


}
