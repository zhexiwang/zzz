#pragma once

//some tools for matrix

#include "zMatrix.hpp"
namespace zzz{
template<typename T,class Major, typename E>
const zMatrix<T,zColMajor> Temp(const zMatrixBaseR<T,Major,E> &mat) {
  zMatrix<T,zColMajor> v(mat);
  return v;
}

template<typename T,class Major, typename E>
class zTransMatrix : public zMatrixBaseR<T,Major, zTransMatrix<T, Major, E> > {
  typedef zTransMatrix<T, Major, E> MeType;
public:
  explicit zTransMatrix(const zMatrixBaseR<T,Major,E>& mat)
  :zMatrixBaseR<T,Major, zTransMatrix<T, Major, E> >(mat.cols_,mat.rows_),Mat_(mat){}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return Mat_(c,r);
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major,E> &Mat_;
};
template<typename T,class Major, typename E>
const zTransMatrix<T,Major,E> Trans(const zMatrixBaseR<T,Major,E>& mat)
{return zTransMatrix<T,Major,E>(mat);}

template<typename T,class Major, typename E>
T Sum(const zMatrixBaseR<T,Major,E> &mat) {
  T sum=0;
  for (zuint r=0; r<mat.rows_; r++) for (zuint c=0; c<mat.cols_; c++)
    sum+=mat(r,c);
  return sum;
}

template<typename T,class Major, typename E1, typename E2>
T Dot(const zMatrixBaseR<T,Major, E1> &mat1, const zMatrixBaseR<T,Major, typename E2> &mat2) {
  T sum=Sum(DotTimes(mat1,mat2));
  return sum;
}

template<typename T,class Major, typename E>
T Norm(const zMatrixBaseR<T,Major,E> &mat) {
  T sqrsum=Dot(mat,mat);
  return Sqrt<T>(sqrsum);
}
}
