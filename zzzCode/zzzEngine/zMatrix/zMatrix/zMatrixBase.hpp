#pragma once
#include <zCore/Utility/ZCheck.hpp>

//for base class of all zMatrix classes
//can choose row major or column major, column major is default

//#include "zSubMatrix.hpp"

namespace zzz{
class EndlessArray;
template<typename T, typename Major, typename E> class zSubMatrixEER;
template<typename T, typename Major, typename E> class zSubMatrixIER;
template<typename T, typename Major, typename E> class zSubMatrixEIR;
template<typename T, typename Major, typename E, typename E1> class zSubMatrixEMR;
template<typename T, typename Major, typename E, typename E1> class zSubMatrixMER;
template<typename T, typename Major, typename E, typename E1, typename E2> class zSubMatrixMMR;
template<typename T, typename Major, typename E, typename E1> class zSubMatrixIMR;
template<typename T, typename Major, typename E, typename E1> class zSubMatrixMIR;

template<typename T, typename Major, typename E> class zSubMatrixEEW;
template<typename T, typename Major, typename E> class zSubMatrixIEW;
template<typename T, typename Major, typename E> class zSubMatrixEIW;
template<typename T, typename Major, typename E, typename E1> class zSubMatrixEMW;
template<typename T, typename Major, typename E, typename E1> class zSubMatrixMEW;
template<typename T, typename Major, typename E, typename E1, typename E2> class zSubMatrixMMW;
template<typename T, typename Major, typename E, typename E1> class zSubMatrixIMW;
template<typename T, typename Major, typename E, typename E1> class zSubMatrixMIW;

template<typename T, typename Major> class zVConstantContinuousVector;

class zColMajor {
public:
  static zuint ToIndex(const zuint r, const zuint c, const zuint rows, const zuint) {return c * rows + r;}
  static void ToIndex(const zuint idx, const zuint rows, const zuint, zuint &r, zuint &c) {r = idx % rows; c = zuint(idx / rows);}
  static const int Dim_ = 1;
};

class zRowMajor {
public:
  static zuint ToIndex(zuint r, zuint c, zuint, zuint cols) {return r * cols + c;}
  static void ToIndex(const zuint idx, const zuint, const zuint cols, zuint &r, zuint &c) {c = idx % cols; r = zuint(idx / cols);}
  static const int Dim_=0;
};

//Readable Matrix
template<typename T, class Major, typename E>
class zMatrixBaseR {
public:
  // constructor
  zMatrixBaseR(zuint r, zuint c):rows_(r),cols_(c){}
  // Check range
  void CheckRange(zuint r, zuint c) const { ZRCHECK_LT(r, rows_); ZRCHECK_LT(c, cols_); }
  void CheckRange(zuint idx) const { ZRCHECK_LT(idx, size()); }
  operator E const&() const { return static_cast<const E&>(*this); }
  const T& operator[](zuint idx) { zuint r, c; ToIndex(idx, r, c); return (*this)(r,c); }
  const T operator()(zuint r, zuint c) const { return static_cast<const E&>(*this)(r, c);}

  // Submatrix
  zSubMatrixEER<T,Major,E> operator()(const EndlessArray &r, const EndlessArray &c) const { return zSubMatrixEER<T, Major, E>(*this, r, c); }
  zSubMatrixIER<T, Major,E> operator()(int r, const EndlessArray &c) const { return zSubMatrixIER<T, Major, E>(*this, r, c); }
  zSubMatrixEIR<T,Major,E> operator()(const EndlessArray &r, int c) const { return zSubMatrixEIR<T,Major,E>(*this,r,c); }

  template<typename E1>
  zSubMatrixEMR<T,Major,E,E1> operator()(const EndlessArray &r, const zMatrixBaseR<int,Major,E1> &c) const { return zSubMatrixEMR<T,Major,E,E1>(*this,r,c); }
  template<typename E1>
  zSubMatrixMER<T,Major,E,E1> operator()(const zMatrixBaseR<int,Major,E1> &r, const EndlessArray &c) const { return zSubMatrixMER<T,Major,E,E1>(*this,r,c); }

  template<typename E1, typename E2>
  zSubMatrixMMR<T,Major,E,E1,E2> operator()(const zMatrixBaseR<int,Major,E1> &r, const zMatrixBaseR<int,Major,E2> &c) const { return zSubMatrixMMR<T,Major,E,E1,E2>(*this,r,c); }
  template<typename E1>
  zSubMatrixIMR<T,Major,E,E1> operator()(int r, const zMatrixBaseR<int,Major,E1> &c) const { return zSubMatrixIMR<T,Major,E,E1>(*this,r,c); }
  template<typename E1>
  zSubMatrixMIR<T,Major,E,E1> operator()(const zMatrixBaseR<int,Major,E1> &r, int c) const { return zSubMatrixMIR<T,Major,E,E1>(*this,r,c); }

  zSubMatrixEMR<T,Major,E,zVConstantContinuousVector<int,zColMajor> > operator()(const EndlessArray &r, const zVConstantContinuousVector<int,zColMajor> &c) const
  { return zSubMatrixEMR<T,Major,E,zVConstantContinuousVector<int, zColMajor> >(*this,r,c); }
  zSubMatrixMER<T,Major,E,zVConstantContinuousVector<int,zColMajor> > operator()(const zVConstantContinuousVector<int,zColMajor> &r, const EndlessArray &c) const
  { return zSubMatrixMER<T,Major,E,zVConstantContinuousVector<int, zColMajor> >(*this,r,c); }
  zSubMatrixMMR<T,Major,E,zVConstantContinuousVector<int, zColMajor>,zVConstantContinuousVector<int, zColMajor>> operator()(const zVConstantContinuousVector<int, zColMajor> &r, const zVConstantContinuousVector<int, zColMajor> &c) const
  { return zSubMatrixMMR<T,Major,E,zVConstantContinuousVector<int, zColMajor>, zVConstantContinuousVector<int, zColMajor> >(*this,r,c); }
  zSubMatrixIMR<T,Major,E,zVConstantContinuousVector<int, zColMajor> > operator()(int r, const zVConstantContinuousVector<int, zColMajor> &c) const
  { return zSubMatrixIMR<T,Major,E,zVConstantContinuousVector<int, zColMajor> >(*this,r,c); }
  zSubMatrixMIR<T,Major,E,zVConstantContinuousVector<int, zColMajor> > operator()(const zVConstantContinuousVector<int, zColMajor> &r, int c) const
  { return zSubMatrixMIR<T,Major,E,zVConstantContinuousVector<int, zColMajor> >(*this,r,c); }

  //operator bool() const {
  //  for (zuint r=0; r<rows_; r++) for (zuint c=0; c<cols_; c++)
  //    if (!((*this)(r,c))) return false;
  //  return true;
  //}

  // According to Matlab, == is itemwise ==
  //operator==
  template<typename E>
   bool operator==(const zMatrixBaseR<T,Major,E> &other) const {
     if (rows_!=other.rows_ || cols_!=other.cols_) return false;
     for (zuint r=0; r<rows_; r++) for (zuint c=0; c<cols_; c++)
       if ((*this)(r,c) != other(r,c)) return false;
     return true;
   }
  //get size
  zuint Size(zuint i) const {
    ZCHECK(i==0 || i==1)<<"Size() accept only 0 or 1";
    if (i==0) return rows_;
    else if (i==1) return cols_;
    return -1;
  }
  zuint Rows() const {return rows_;}
  zuint Cols() const {return cols_;}
  zsize size() const { return rows_ * cols_; }
  //IO
  friend inline ostream& operator<<(ostream& os,const zMatrixBaseR<T,Major,E> &me) {
    for (zuint r=0; r<me.rows_; r++)  { for (zuint c=0; c<me.cols_; c++) os << me(r,c) << ' '; if (r!=me.rows_-1) os << '\n'; }
    return os;
  }
  //the only member variable
  zuint rows_,cols_;
protected:
  inline zuint ToIndex(const zuint r, const zuint c) const {
    return Major::ToIndex(r,c,zMatrixBaseR<T,Major,E>::rows_,zMatrixBaseR<T,Major,E>::cols_);
  }
  inline void ToIndex(const zuint idx, zuint &r, zuint &c) const {
    Major::ToIndex(idx,zMatrixBaseR<T,Major,E>::rows_,zMatrixBaseR<T,Major,E>::cols_,r,c);
  }
};

//Writable Matrix is also readable
template<typename T, typename Major, typename E>
class zMatrixBaseW : public zMatrixBaseR<T, Major, E> {
public:
  zMatrixBaseW(zuint r,zuint c):zMatrixBaseR<T,Major,E>(r,c){}
  operator E&() { return static_cast<E&>(*this); }
  // operator()
  T& operator()(zuint r, zuint c){return static_cast<E&>(*this)(r,c);}
  // This function can be over ride to get faster performance
  T& operator[](zuint idx) { zuint r, c; ToIndex(idx, r, c); return (*this)(r,c); }
  template<typename E1>
  const zMatrixBaseW<T,Major,E>& operator=(const zMatrixBaseR<T,Major,E1> &other) {
    ZCHECK_EQ(rows_, other.rows_);
    ZCHECK_EQ(cols_, other.cols_);
    for (zuint i=0; i<rows_; i++) for (zuint j=0; j<cols_; j++)
      (*this)(i,j)=other(i,j);
    return *this;
  }

  // submatrix
  zSubMatrixEEW<T,Major,E> operator()(const EndlessArray &r, const EndlessArray &c) { return zSubMatrixEEW<T, Major, E>(*this, r, c); }
  zSubMatrixIEW<T, Major,E> operator()(int r, const EndlessArray &c) { return zSubMatrixIEW<T, Major, E>(*this, r, c); }
  zSubMatrixEIW<T,Major,E> operator()(const EndlessArray &r, int c) { return zSubMatrixEIW<T,Major,E>(*this,r,c); }

  template<typename E1>
  zSubMatrixEMW<T,Major,E,E1> operator()(const EndlessArray &r, const zMatrixBaseR<int,Major,E1> &c) { return zSubMatrixEMW<T,Major,E,E1>(*this,r,c); }
  template<typename E1>
  zSubMatrixMEW<T,Major,E,E1> operator()(const zMatrixBaseR<int,Major,E1> &r, const EndlessArray &c) { return zSubMatrixMEW<T,Major,E,E1>(*this,r,c); }

  template<typename E1, typename E2>
  zSubMatrixMMW<T,Major,E,E1,E2> operator()(const zMatrixBaseR<int,Major,E1> &r, const zMatrixBaseR<int,Major,E2> &c) { return zSubMatrixMMW<T,Major,E,E1,E2>(*this,r,c); }
  template<typename E1>
  zSubMatrixIMW<T,Major,E,E1> operator()(int r, const zMatrixBaseR<int,Major,E1> &c) { return zSubMatrixIMW<T,Major,E,E1>(*this,r,c); }
  template<typename E1>
  zSubMatrixMIW<T,Major,E,E1> operator()(const zMatrixBaseR<int,Major,E1> &r, int c) { return zSubMatrixMIW<T,Major,E,E1>(*this,r,c); }

  zSubMatrixEMW<T,Major,E,zVConstantContinuousVector<int,zColMajor> > operator()(const EndlessArray &r, const zVConstantContinuousVector<int,zColMajor> &c)
  { return zSubMatrixEMW<T,Major,E,zVConstantContinuousVector<int, zColMajor> >(*this,r,c); }
  zSubMatrixMEW<T,Major,E,zVConstantContinuousVector<int,zColMajor> > operator()(const zVConstantContinuousVector<int,zColMajor> &r, const EndlessArray &c)
  { return zSubMatrixMEW<T,Major,E,zVConstantContinuousVector<int, zColMajor> >(*this,r,c); }
  zSubMatrixMMW<T,Major,E,zVConstantContinuousVector<int, zColMajor>,zVConstantContinuousVector<int, zColMajor>> operator()(const zVConstantContinuousVector<int, zColMajor> &r, const zVConstantContinuousVector<int, zColMajor> &c)
  { return zSubMatrixMMW<T,Major,E,zVConstantContinuousVector<int, zColMajor>, zVConstantContinuousVector<int, zColMajor> >(*this,r,c); }
  zSubMatrixIMW<T,Major,E,zVConstantContinuousVector<int, zColMajor> > operator()(int r, const zVConstantContinuousVector<int, zColMajor> &c)
  { return zSubMatrixIMW<T,Major,E,zVConstantContinuousVector<int, zColMajor> >(*this,r,c); }
  zSubMatrixMIW<T,Major,E,zVConstantContinuousVector<int, zColMajor> > operator()(const zVConstantContinuousVector<int, zColMajor> &r, int c)
  { return zSubMatrixMIW<T,Major,E,zVConstantContinuousVector<int, zColMajor> >(*this,r,c); }
  //IO
  friend inline istream& operator>>(istream& is,zMatrixBaseW<T,Major,E> &me) {
    for (zuint r=0; r<me.rows_; r++) for (zuint c=0; c<me.cols_; c++)
      is >> me(r,c);
    return is;
  }
  using zMatrixBaseR<T,Major,E>::operator();
  using zMatrixBaseR<T,Major,E>::ToIndex;
  using zMatrixBaseR<T,Major,E>::CheckRange;
  using zMatrixBaseR<T,Major,E>::rows_;
  using zMatrixBaseR<T,Major,E>::cols_;
};

}

#define ZMATRIXR_METHOD(E) \
  using zMatrixBaseR<T, Major, E >::rows_; \
  using zMatrixBaseR<T, Major, E >::cols_; \
  using zMatrixBaseR<T, Major, E >::CheckRange; \
  using zMatrixBaseR<T, Major, E >::operator();

#define ZMATRIXW_METHOD(E) \
  using zMatrixBaseW<T, Major, E >::rows_; \
  using zMatrixBaseW<T, Major, E >::cols_; \
  using zMatrixBaseW<T, Major, E >::CheckRange; \
  using zMatrixBaseW<T, Major, E >::operator(); \
  using zMatrixBaseW<T, Major, E >::operator=; \
  const E& operator=(const E &other) { \
    return zMatrixBaseW<T, Major, E >::operator=(static_cast<const zMatrixBaseR<T, Major, E > &>(other));\
  }