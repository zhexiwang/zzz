#pragma once
#include "zMatrix.hpp"
//just a special derivation of zmatrix(1,n) or zmatrix(n,1), column major is default

namespace zzz{
template<typename T, typename Major=zColMajor>
class zVector : public zMatrix<T,Major> {
public:
  //constructor
  explicit zVector(){}
  explicit zVector(zuint l):zMatrix<T,Major>(Major::Dim_==0?1:l, Major::Dim_==0?l:1){}
  zVector(const zVector<T,Major> &other) {
    *this=other;
  }
  template<typename E>
  explicit zVector(const zMatrixBaseR<T,Major,E> &other) {
    zMatrix<T,Major>::operator=(other);
  }
  //operator=
  const zMatrix<T,Major>& operator=(const zVector<T,Major>& other) {
    return zMatrix<T,Major>::operator =((const zMatrix<T,Major>&)other);
  }
  using zMatrix<T,Major>::operator ();
};

}
