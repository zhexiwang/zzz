#pragma once
#include "zMatrixBase.hpp"

// for Reshape(mat,row,col)

namespace zzz{
template<typename T,class Major, typename E>
class zFunctionReshapeR : public zMatrixBaseR<T,Major, zFunctionReshapeR<T, Major, E> > {
  typedef zFunctionReshapeR<T, Major, E> MeType;
public:
  explicit zFunctionReshapeR(const zMatrixBaseR<T,Major, E>& mat, zuint row, zuint col)
  :zMatrixBaseR<T,Major, zFunctionReshapeR<T, Major, E> >(row,col),Mat_(mat){ZCHECK_EQ(row * col, mat.rows_ * mat.cols_);}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return Mat_[ToIndex(r,c)];
  }
  ZMATRIXR_METHOD(MeType);
private:
  const zMatrixBaseR<T,Major,E> &Mat_;
};

template<typename T,class Major, typename E>
class zFunctionReshapeW : public zMatrixBaseW<T,Major, zFunctionReshapeW<T, Major, E> > {
  typedef zFunctionReshapeW<T, Major, E> MeType;
public:
  explicit zFunctionReshapeW(zMatrixBaseW<T,Major,E>& mat, zuint row, zuint col)
  :zMatrixBaseW<T,Major, zFunctionReshapeW<T, Major, E> >(row,col),Mat_(mat){assert(row*col==mat.rows_*mat.cols_);}
  const T operator()(zuint r, zuint c) const {
    CheckRange(r, c);
    return Mat_[ToIndex(r,c)];
  }
  T& operator()(zuint r, zuint c) {
    CheckRange(r, c);
    return Mat_[ToIndex(r,c)];
  }
  ZMATRIXR_METHOD(MeType);
private:
  zMatrixBaseW<T,Major,E> &Mat_;
};


template<typename T,class Major, typename E>
const zFunctionReshapeR<T,Major,E> Reshape(const zMatrixBaseR<T,Major,E> &mat, zuint row, zuint col) {
  return zFunctionReshapeR<T,Major,E>(mat,row,col);
}

template<typename T,class Major, typename E>
zFunctionReshapeW<T,Major,E> Reshape(zMatrixBaseW<T,Major,E> &mat, zuint row, zuint col) {
  return zFunctionReshapeW<T,Major,E>(mat,row,col);
}

}
