include($$PWD/../../zzzlib.pri)

QT       -= core gui

TARGET = zCore$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    $$files($$PWD/zCore/3rdParty/*.cpp) \
    $$files($$PWD/zCore/3rdParty/*.c) \
    $$files($$PWD/zCore/Math/*.cpp) \
    $$files($$PWD/zCore/Utility/*.cpp) \
    $$files($$PWD/zCore/Xml/*.cpp) \
    $$files($$PWD/zCore/*.cpp)

SOURCES -= $$PWD/zCore/Utility/Time.cpp
SOURCES -= $$PWD/zCore/Utility/Date.cpp

HEADERS += \
    $$files($$PWD/zCore/3rdParty/*.hpp) \
    $$files($$PWD/zCore/3rdParty/*.h) \
    $$files($$PWD/zCore/Math/*.hpp) \
    $$files($$PWD/zCore/Utility/*.hpp) \
    $$files($$PWD/zCore/Xml/*.hpp) \
    $$files($$PWD/zCore/*.hpp)

HEADERS -= $$PWD/zCore/Utility/Time.hpp
HEADERS -= $$PWD/zCore/Utility/Date.hpp

OTHER_FILES += \

BOOSTENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsBoost.pri) {
    error("Cannot include QTFlagsBoost.pri")
}

Z3RDENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZ3rd.pri) {
    error("Cannot include QTFlagsZ3rd.pri")
}

YAMLCPPENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsYamlCpp.pri) {
    error("Cannot include QTFlagsYamlCpp.pri")
}

DEFINES = $$replace(DEFINES, ZETA, ZZZ_LIB)
DEFINES = $$replace(DEFINES, _ENABLE, )
message($$DEFINES)
