#pragma once
#include <zCore/common.hpp>
#include "Vector2.hpp"
#include <zCore/Utility/zVar.hpp>

namespace zzz{
template <typename T>
class MinMax : public Vector<2, T> {
public:
#undef max
#undef min
  using Vector<2,T>::at;

  MinMax()
    : Vector<2, T>(numeric_limits<T>::max(), numeric_limits<T>::is_signed ? -numeric_limits<T>::max() : 0){}
  MinMax(const T &min,const T &max)
    : Vector<2, T>(min, max){}
  explicit MinMax(const T &p)
    : Vector<2, T>(p){}
  explicit MinMax(const vector<T> &data)
    : Vector<2, T>(numeric_limits<T>::max(), numeric_limits<T>::is_signed ? -numeric_limits<T>::max() : 0){
    *this += data;
  }

  // create from list of vertex
  void Reset() {
    at(0) = T(numeric_limits<T>::max());
    at(1) = T(-numeric_limits<T>::max());
  }

  // min and max
  T &Min(){return at(0);}
  const T &Min() const{return at(0);}
  T &Max(){return at(1);}
  const T &Max() const{return at(1);}

  // Offset
  void SetOffset(const T &offset) {
    at(0) += offset;
    at(1) += offset;
  }
  // Center
  T Center() const{return (at(0) + at(1))*0.5;}

  // Difference
  T Diff() const{return at(1) - at(0);}

  Vector<2, T> Range() const {return *this;}

  bool IsLegal() const {
    if (at(0) > at(1))
      return false;
    return true;
  }

  void AddData(const T &p) {
    if (at(0) > p) at(0) = p;
    if (at(1) < p) at(1) = p;
  }

  template<typename T1>
  void AddData(const T1& _begin, const T1& _end) {
    for (T1 i = _begin; i != _end; i++)
      AddData(*i);
  }

  void operator+=(const T &p) {
    AddData(p);
  }
  
  void operator+=(const std::vector<T> &p) {
    AddData(p.begin(), p.end());
  }
  
  /// Boolean union
  void operator+=(const MinMax<T> &box) {
    AddData(box.Min());
    AddData(box.Max());
  }

  const MinMax<T>& operator+(const MinMax<T> &box) const {
    MinMax<T> ret(*this);
    ret.AddData(box);
    return ret;
  }

  /// Boolean intersection
  void operator*=(const MinMax<T> &box) {
    if (at(0) < box.Min()) at(0) = box.Min();
    if (at(1) > box.Max()) at(1) = box.Max();
  }

  const MinMax<T>& operator*(const MinMax<T> &box) const {
    MinMax<T> ret(*this);
    ret *= box;
    return ret;
  }

  /// Box equality operator
  bool operator==(const MinMax<T> &box) const {
    return at(0) == box.Min() && at(1 )== box.Max();
  }

  /// Volumetric classification
  bool Contain(const T &pos) const {
    if (!Within<T>(at(0), pos, at(1))) 
      return false;
    return true;
  }

  bool Contain(const MinMax<T> &box) const {
    if (box.Min() >= Min() && box.Max() <= Max()) 
      return true;
    return false;
  }

  /// Intersection between boxes
  bool Overlap(const MinMax<T> &box) const {
    if (Min() > box.Max()) return false;
    if (Max() < box.Min()) return false;
    return true;
  }

  static MinMax<T> ZERO() {
    return MinMax<T>(T(0), T(0));
  }

  static MinMax<T> COVERALL() {
    return MinMax<T>();
  }
};

typedef MinMax<int> MinMaxi;
typedef MinMax<float> MinMaxf;
typedef MinMax<double> MinMaxd;

SIMPLE_IOOBJECT(MinMaxi);
SIMPLE_IOOBJECT(MinMaxf);
SIMPLE_IOOBJECT(MinMaxd);

template <typename T>
class zVar<zzz::MinMax<T> > {
public:
  zVar(const zzz::MinMax<T> &v, const int limit):v_(v),limit_(limit){}
  friend inline ostream& operator<<(ostream& os, const zVar<zzz::MinMax<T> > &me) {
    os<<ZVARVALUE_LIMIT(me.v_.Min(), me.limit_) << "~" << ZVARVALUE_LIMIT(me.v_.Max(), me.limit_);
    return os;
  }
private:
  const zzz::MinMax<T> &v_;
  const int limit_;
};
}
