#pragma once
#include "Array.hpp"
#include "Vector2.hpp"

namespace zzz {
template <typename T>
struct Array<2, T> : public ArrayBase<2, T> {
  using ArrayBase<2, T>::v;
  using ArrayBase<2, T>::subsizes;
  using ArrayBase<2, T>::Size;
public:
  typedef typename ArrayBase<2, T>::size_type size_type;
  //constructor
  Array(void){}
  Array(const size_type r, const size_type c):ArrayBase<2, T>(Vector2i(r, c)){}
  explicit Array(const VectorBase<2,size_type> &size):ArrayBase<2, T>(size){}
  Array(const T *data, const size_type r, const size_type c):ArrayBase<2, T>(data, Vector2i(r, c)){}
  Array(const T *data, const VectorBase<2,size_type> &size):ArrayBase<2, T>(data, size){}
  Array(const Array<2, T>& a):ArrayBase<2, T>(a) {}
  Array(const ArrayBase<2, T>& a):ArrayBase<2, T>(a) {}

  //assign
  inline const Array<2, T> &operator=(const Array<2, T>& a){ArrayBase<2, T>::operator =(a); return *this;}
  using ArrayBase<2, T>::operator=;

  //reference
  using ArrayBase<2, T>::operator();
  using ArrayBase<2, T>::At;
  const T& At(const size_type r, const size_type c) const{
    ZRCHECK_LT(r, Size(0));
    ZRCHECK_LT(c, Size(1));
    return At(ToIndex(r, c));
  }
  T& At(const size_type r, const size_type c) {
    ZRCHECK_LT(r, Size(0));
    ZRCHECK_LT(c, Size(1));
    return At(ToIndex(r, c));
  }
  const T& operator()(const size_type r, const size_type c) const{return At(r, c);}
  T& operator()(const size_type r, const size_type c) {return At(r, c);}

  T Interpolate(const double r, const double c) const {return ArrayBase<2, T>::Interpolate(Vector2d(r, c));}
  using ArrayBase<2, T>::Interpolate;

  size_type ToIndex(const size_type r, const size_type c) const {return r*subsizes[0] + c*subsizes[1];}
  using ArrayBase<2, T>::ToIndex;

  bool CheckIndex(const size_type r, const size_type c) const {return ArrayBase<2, T>::CheckIndex(Vector2i(r, c));}
  using ArrayBase<2, T>::CheckIndex;

  void SetSize(const size_type r, const size_type c) {ArrayBase<2, T>::SetSize(Vector2i(r, c));}
  using ArrayBase<2, T>::SetSize;

  void Fill(const size_type r, const size_type c, const T &a) {
    SetSize(r, c);
    Fill(a);
  }
  using ArrayBase<2, T>::Fill;

  //IO
  friend inline ostream& operator<<(ostream& os,const Array<2, T> &me) {
    for (int i = 0; i < me.Size(0); ++i) {
      for (int j = 0; j < me.Size(1); ++j) {
        os << me.At(i, j) << ' ';
      }
      os << endl;
    }
    return os;
  }
};

typedef Array<2,zint8> Array2i8;
typedef Array<2,zuint8> Array2ui8;
typedef Array<2,zint16> Array2i16;
typedef Array<2,zuint8> Array2ui16;
typedef Array<2,zint32> Array2i32;
typedef Array<2,zuint32> Array2ui32;
typedef Array<2,zint64> Array2i64;
typedef Array<2,zuint64> Array2ui64;
typedef Array<2,zfloat32> Array2f32;
typedef Array<2,zfloat64> Array2f64;

typedef Array<2,char> Array2c;
typedef Array<2,zuchar> Array2uc;
typedef Array<2,short> Array2s;
typedef Array<2,zushort> Array2us;
typedef Array<2,int> Array2i;
typedef Array<2,zuint> Array2ui;
typedef Array<2,float> Array2f;
typedef Array<2,double> Array2d;
};  // namespace zzz
