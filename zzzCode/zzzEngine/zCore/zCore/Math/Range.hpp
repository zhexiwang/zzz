#pragma once
#include <zCore/common.hpp>
#include <zCore/Math/Vector2.hpp>

namespace zzz {
template <unsigned int N, typename T >
class Range : public Vector<2, Vector<N,T> > {
public:
#undef max
#undef min
  using Vector<2, Vector<N, T> >::at;
  Range()
    : Vector<2, Vector<N, T> >(Vector<N, T>(numeric_limits<T>::max()),Vector<N, T>(-numeric_limits<T>::max())){}
  Range(const Vector<N,T> &min,const Vector<N,T> &max)
    : Vector<2, Vector<N, T> >(min, max){}
  explicit Range(const VectorBase<N,T> &p)
    : Vector<2, Vector<N, T> >(p){}
  explicit Range(const vector<Vector<N,T> > &data)
    : Vector<2, Vector<N, T> >(Vector<N, T>(numeric_limits<T>::max()),Vector<N, T>(-numeric_limits<T>::max())){
    *this += data;
  }

  // create from list of vertex
  void Reset() {
    at(0) = Vector<N,T>(numeric_limits<T>::max());
    at(1) = Vector<N,T>(-numeric_limits<T>::max());
  }

  // min and max
  Vector<N,T> &Min(){return at(0);}
  const Vector<N,T> &Min() const{return at(0);}
  T Min(zuint i) const {return at(0)[i];}
  Vector<N,T> &Max(){return at(1);}
  const Vector<N,T> &Max() const{return at(1);}
  T Max(zuint i) const {return at(1)[i];}

  // range
  Vector<2, T> Bound(zuint i) const {return Vector<2, T>(Min(i), Max(i));}
  Vector<2, Vector<N,T> > Bound() const {return *this;}

  // Offset
  void Offset(const Vector<N,T> &offset) {
    at(0) += offset;
    at(1) += offset;
  }
  // Box center
  Vector<N,T> Center() const{return (at(0) + at(1))*0.5;}
  T Center(zuint i) const{return (at(0)[i] + at(1)[i])*0.5;}

  // Box difference
  Vector<N,T> Diff() const{return at(1) - at(0);}
  T Diff(zuint i) const{return at(1)[i] - at(0)[i];}

  // Volume
  T Volume() const{T v = 1; for (zuint i = 0; i < N; ++i) v *= Diff(i); return v;}

  bool IsLegal() const {
    for (zuint i = 0; i < N; ++i)
      if (at(0)[i] > at(1)[i]) {
        return false;
      }
    return true;
  }

  bool IsEmpty() const {
    return at(0) == at(1);
  }

  void AddData(const Vector<N,T> &p) {
    for (zuint i = 0; i < N; ++i) {
      if (at(0)[i] > p[i]) at(0)[i] = p[i];
      if (at(1)[i] < p[i]) at(1)[i] = p[i];
    }
  }

  template<typename T1>
  void AddData(const T1 &_begin, const T1 &_end) {
    for (T1 x = _begin; x != _end; ++x)
      AddData(*x);
  }

  void operator+=(const VectorBase<N, T> &p) {
    AddData(p);
  }
  
  void operator+=(const std::vector<Vector<N, T> > &p) {
    AddData(p.begin(), p.end());
  }
  
  /// Boolean union
  void operator+=(const Range<N,T> &box) {
    *this += box.Min();
    *this += box.Max();
  }

  const Range<N,T>& operator+(const Range<N,T> &box) const {
    Range<N,T> ret(*this);
    ret+=box;
    return ret;
  }

  /// Boolean intersection
  void operator*=(const Range<N,T> &box) {
    for (zuint i = 0; i < N; ++i) {
      if (at(0)[i] < box.Min(i)) at(0)[i] = box.Min(i);
      if (at(1)[i] > box.Max(i)) at(1)[i] = box.Max(i);
    }
  }

  const Range<N,T>& operator*(const Range<N,T> &box) const {
    Range<N, T> ret(*this);
    ret *= box;
    return ret;
  }

  /// Box equality operator
  bool operator==(const Range<N,T> &box) const {
    return Min() == box.Min() && Max() == box.Max();
  }

  /// Volumetric classification
  bool IsInside(const Vector<N,T> &pos) const {
    for (zuint i=0; i<N; i++)
      if (!Within<T>(Min(i), pos[i], Max(i))) 
        return false;
    return true;
  }

  /// Intersection between boxes
  bool IsIntersect(const Range<N,T> &box) const {
    for (zuint i = 0; i < N; ++i) {
      if (Min(i) > box.Max(i)) return false;
      if (Max(i) < box.Min(i)) return false;
    }
    return true;
  }

  static Range<N,T> ZERO() {
    return Range(Vector<N,T>(T(0)), Vector<N,T>(T(0)));
  }
  static Range<N,T> COVERALL() {
    return Range(Vector<N,T>(-numeric_limits<T>::max()), Vector<N,T>(numeric_limits<T>::max()));
  }
};

typedef Range<2,zint32>     Range2i32;
typedef Range<2,zfloat32>   Range2f32;
typedef Range<2,zfloat64>   Range2f64;
typedef Range<3,zfloat32>   Range3f32;
typedef Range<3,zfloat64>   Range3f64;
typedef Range<4,zfloat32>   Range4f32;
typedef Range<4,zfloat64>   Range4f64;

typedef Range<2,int>     Range2i;
typedef Range<2,float>   Range2f;
typedef Range<2,double>  Range2d;
typedef Range<3,float>   Range3f;
typedef Range<3,double>  Range3d;
typedef Range<4,float>   Range4f;
typedef Range<4,double>  Range4d;
}