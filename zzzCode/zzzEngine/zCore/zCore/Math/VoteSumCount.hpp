#pragma once
#include "SumCount.hpp"
#include "../Utility/STLVector.hpp"

namespace zzz {
template<typename T>
class VoteSumCount {
public:
  explicit VoteSumCount(const T& range) : range_(range){}
  
  // Return if the new data falls to some of the exist category.
  bool AddData(const T &x) {
    for (zuint i = 0; i < sc.size(); ++i) {
      if (Abs(sc[i].Mean() - x) < range_) {
        sc[i] += x;
        return true;
      }
    }
    sc.push_back(SumCount<T>(x));
    return false;
  }

  bool operator+=(const T &x) {
    return AddData(x);
  }

  SumCount<T> &GetMaxVote() {
    int pos = 0;
    for (zuint i = 1; i < sc.size(); ++i) {
      if (sc[pos].Count() < sc[i].Count())
        pos = i;
    }
    return sc[pos];
  }

  bool BelongToMaxVote(const T &x) {
    return Abs(GetMaxVote().Mean() - x) < range_;
  }
private:
  STLVector<SumCount<T> > sc;
  T range_;
};

}
