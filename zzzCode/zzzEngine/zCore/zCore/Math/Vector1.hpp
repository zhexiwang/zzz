#pragma once
#include "../common.hpp"
#include "Vector.hpp"
#undef min
#undef max

namespace zzz{
template <typename T>
class Vector<1, T> : public VectorBase<1, T>
{
protected:
  using VectorBase<1, T>::v;
public:
  //constructor
  Vector(void){}
  Vector(const Vector<1, T>& a):VectorBase<1, T>(a){}
  explicit Vector(const T &a){v[0]=a;}
  Vector(const VectorBase<1, T>& a):VectorBase<1, T>(a){}
  template<typename T1> explicit Vector(const T1 *p):VectorBase<1, T>(p){}
  template<typename T1> explicit Vector(const Vector<1,T1>& a):VectorBase<1, T>(a){}
  template<typename T1> explicit Vector(const VectorBase<1,T1>& a):VectorBase<1, T>(a){}

  //assign
  inline const Vector<1, T> &operator=(const Vector<1, T>& a){VectorBase<1, T>::operator =(a); return *this;}
  inline void Set(const T &a){v[0]=a;}
  using VectorBase<1, T>::operator=;

  // alias
  inline T& x(){return v[0];}
  inline const T& x() const {return v[0];}
};

typedef Vector<1,zint8> Vector1i8;
typedef Vector<1,zuint8> Vector1ui8;
typedef Vector<1,zint8> Vector1i16;
typedef Vector<1,zuint8> Vector1ui16;
typedef Vector<1,zint8> Vector1i32;
typedef Vector<1,zuint8> Vector1ui32;
typedef Vector<1,zint8> Vector1i64;
typedef Vector<1,zuint8> Vector1ui64;
typedef Vector<1,zfloat32> Vector1f32;
typedef Vector<1,zfloat64> Vector1f64;


typedef Vector<1,zuint> Vector1ui;
typedef Vector<1,int> Vector1i;
typedef Vector<1,float> Vector1f;
typedef Vector<1,double> Vector1d;

}
