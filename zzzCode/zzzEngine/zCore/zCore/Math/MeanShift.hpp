#pragma once

namespace zzz {
template<typename T>
class MeanShift {
public:
  MeanShift(const T& range)
    : range_(range) {
  }
  void Run(const vector<T>& x) {
    // mean shift
    bool changed = true;
    while (changed) {
      STLVector<int> last_labels = labels_;
      // update centers
      map<int, SumCount<T> > cs;
      for (zuint i = 0; i < labels_.size(); ++i) {
        int label = labels_[i];
        if (cs.find(label) == cs.end()) cs[label] = SumCount<T>();
        cs[label] += x[i];
      }
      centers_.clear();
      for (map<int, SumCount<T> >::iterator mi = cs.begin(); mi != cs.end(); ++mi) {
        centers_[mi->first] = mi->second.Mean();
      }
      // update labels
      changed = false;
      STLVector<int> last_label = labels_;
      labels_.assign(x.size(), -1);
      counts_.clear();
      for (map<int, T>::iterator mi = centers_.begin(); mi != centers_.end(); ++mi) {
        const int label = mi->first;
        counts_[label] = 0;
        for (zuint i = 0; i < x.size(); ++i) {
          if (labels_[i] > 0) continue;
          if (Abs(mi->second - x[i]) <= range_) {
            labels_[i] = label;
            ++counts_[label];
            if (labels_[i] != last_label[i]) changed = true;
          }
        }
      }
      for (zuint i = 0; i < x.size(); ++i) {
        if (labels_[i] > 0) continue;
        labels_[i] = centers_.size();
        centers_[labels_[i]] = x[i];
        counts_[labels_[i]] = 1;
        changed = true;
      }
    }
  }
  STLVector<int> labels_;
  map<int, T> centers_;
  map<int, int> counts_;
  T range_;
};
}