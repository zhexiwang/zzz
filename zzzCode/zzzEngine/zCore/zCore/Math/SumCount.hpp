#pragma once
#include <zCore/common.hpp>

namespace zzz{
template<typename T>
class SumCount {
public:
  SumCount():all_(0), count_(0), mean_(0), cached_(false) {}
  explicit SumCount(const T &v):all_(v), count_(1), mean_(v), cached_(true) {}

  const T& Sum() const {return all_;}
  zuint Count() const {return count_;}
  const T& Mean() const {
    ZCHECK_NOT_ZERO(count_);
    if (!cached_) {
      mean_ = all_ / count_;
      cached_ = true;
    }
    return mean_;
  }

  void AddData(const T &p) {
    all_ += p;
    ++count_;
    cached_ = false;
  }

  void RemData(const T &p) {
    ZCHECK_NOT_ZERO(count_);
    all_ -= p;
    --count_;
    cached_ = false;
  }

  template<typename T1>
  void AddData(const T1& _begin, const T1& _end) {
    for (T1 i = _begin; i != _end; ++i)
      AddData(*i);
  }

  template<typename T1>
  void RemData(const T1& _begin, const T1& _end) {
    for (T1 i = _begin; i != _end; ++i)
      RemData(*i);
  }

  void operator+=(const T &x) {
    AddData(x);
  }

  void operator-=(const T &x) {
    RemData(x);
  }

  void operator+=(const vector<T> &x) {
    AddData(x.begin(), x.end());
  }

  void operator-=(const vector<T> &x) {
    RemData(x.begin(), x.end());
  }

  void operator+=(const SumCount<T> &x) {
    count_ += x.Count();
    all_ += x.Sum();
    cached_ = false;
  }

  void operator-=(const SumCount<T> &x) {
    ZCHECK_GE(count_, x.count_);
    count_ -= x.Count();
    all_ -= x.All();
    cached_ = false;
  }

  void Reset() {
    all_ = 0;
    count_ = 0;
    cached_ = false;
  }

  void Reset(const T& x) {
    all_ = x;
    count_ = 1;
    mean_ = x;
    cached_ = true;
  }

private:
  T all_;
  zuint count_;
  mutable bool cached_;
  mutable T mean_;
};
}  // namespace zzz
