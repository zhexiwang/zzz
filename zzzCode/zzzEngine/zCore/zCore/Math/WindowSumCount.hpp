#pragma once
#include <zCore/common.hpp>

namespace zzz{
template<typename T>
class WindowSumCount {
public:
  WindowSumCount(){SetWindow(1);}
  explicit WindowSumCount(zuint window){SetWindow(window);}

  void SetWindow(zuint window) {
    window_ = window;
    data_.clear();
    summed_ = false;
    all_ = 0;
  }

  const T Sum() const {
    if (!summed_) {
      all_= accumulate(data_.begin(), data_.end(), T(0));
      summed_ = true;
    }
    return all_;
  }
  zuint Count() const {return data_.size();}
  const T Mean() const {return Sum()/Count();}

  void AddData(const T &p) {
    if (data_.size() == window_)
      data_.pop_front();
    data_.push_back(p);
    summed_ = false;
  }

  template<typename T1>
  void AddData(const T1& _begin, const T1& _end) {
    for (T1 i=_begin; i!=_end; i++)
      AddData(*i);
  }

  void operator+=(const T &x) {
    AddData(x);
  }

  void operator+=(const vector<T> &x) {
    AddData(x.begin(), x.end());
  }

  void Reset() {
    data_.clear();
    summed_ = false;
    all_ = 0;
  }

private:
  deque<T> data_;
  zuint window_;
  mutable bool summed_;
  mutable T all_;
};
}  // namespace zzz
