#pragma once
#include "Vector1.hpp"
#include "Array.hpp"

namespace zzz {
template <typename T>
struct Array<1, T> : public ArrayBase<1, T> {
protected:
  using ArrayBase<1, T>::v;
public:
  //constructor
  typedef typename ArrayBase<1, T>::size_type size_type;
  Array(void){}
  explicit Array(const size_type size):ArrayBase<1, T>(Vector1i(size)){}
  explicit Array(const VectorBase<1, size_type> &size):ArrayBase<1, T>(size){}
  Array(const T *data, const size_type size):ArrayBase<1, T>(data, Vector1i(size)){}
  Array(const T *data, const VectorBase<1,size_type> &size):ArrayBase<1, T>(data, size){}
  Array(const Array<1, T>& a):ArrayBase<1, T>(a) {}
  Array(const ArrayBase<1, T>& a):ArrayBase<1, T>(a) {}

  //assign
  inline const Array<1, T> &operator=(const Array<1, T>& a){ArrayBase<1, T>::operator =(a); return *this;}
  using ArrayBase<1, T>::operator=;

  using ArrayBase<1, T>::Size;
  const T& At(const size_type i) const{
    ZRCHECK_LT(i, Size(0));
    return At(ToIndex(i));
  }
  T& At(const size_type i) {
    ZRCHECK_LT(i, Size(0));
    return At(ToIndex(i));
  }
  const T& operator()(const size_type i) const{return At(i);}
  T& operator()(const size_type i) {return At(i);}
  using ArrayBase<1, T>::operator();
  using ArrayBase<1, T>::At;

  T Interpolate(const double coord) const {return ArrayBase<1, T>::Interpolate(Vector1d(coord));}
  using ArrayBase<1, T>::Interpolate;

  VectorBase<1, size_type> ToIndex(const size_type pos) const {return VectorBase<1, size_type>(pos);}
  size_type ToIndex(const VectorBase<1, size_type> pos) const {return pos[0];}
  using ArrayBase<1, T>::ToIndex;

  bool CheckIndex(const size_type pos) const {return ArrayBase<1, T>::CheckIndex(Vector1i(pos));}
  using ArrayBase<1, T>::CheckIndex;

  void SetSize(const size_type size) {ArrayBase<1, T>::SetSize(Vector1i(size));}
  using ArrayBase<1, T>::SetSize;

  void Fill(const size_type size, const T &a) {
    SetSize(size);
    Fill(a);
  }
  using ArrayBase<1, T>::Fill;

  void Set(const T* data, const size_type size) {ArrayBase<1, T>::Set(data, Vector1i(size));}
  void Set(const vector<T>& data) {Set(data.data(), data.size());}
  using ArrayBase<1, T>::Set;
};

typedef Array<1,zint8> Array1i8;
typedef Array<1,zuint8> Array1ui8;
typedef Array<1,zint16> Array1i16;
typedef Array<1,zuint8> Array1ui16;
typedef Array<1,zint32> Array1i32;
typedef Array<1,zuint32> Array1ui32;
typedef Array<1,zint64> Array1i64;
typedef Array<1,zuint64> Array1ui64;
typedef Array<1,zfloat32> Array1f32;
typedef Array<1,zfloat64> Array1f64;


typedef Array<1,char> Array1c;
typedef Array<1,zuchar> Array1uc;
typedef Array<1,short> Array1s;
typedef Array<1,zushort> Array1us;
typedef Array<1,int> Array1i;
typedef Array<1,zuint> Array1ui;
typedef Array<1,float> Array1f;
typedef Array<1,double> Array1d;

};  // namespace zzz
