#pragma once
#include "../common.hpp"
namespace zzz{
// Donald Knuth's 32 bit Mix Function
// Discussed by Thomas Wang, HP
// http://www.concentric.net/~Ttwang/tech/inthash.htm
inline zuint HashKnuthUInt(zuint key)
{
    // 2654435761 is the golden ratio of 2^32
    return key*2654435761u;
}

// Robert Jenkin's 32 bit Mix Function
// Better variation in low order bits than Knuth's method
// Discussed by Thomas Wang, HP
// http://www.concentric.net/~Ttwang/tech/inthash.htm
inline zuint HashJenkinsUInt(zuint key)
{
  key += (key << 12);
  key ^= (key >> 22);
  key += (key << 4);
  key ^= (key >> 9);
  key += (key << 10);
  key ^= (key >> 2);
  key += (key << 7);
  key ^= (key >> 12);
  return key;
}

// Thomas Wang's 32 bit Mix Function
// Better variation in low order bits than Knuth's method
// Discussed by Thomas Wang, HP
// http://www.concentric.net/~Ttwang/tech/inthash.htm
inline zuint HashWangUInt(zuint key)
{
  key += ~(key << 15);
  key ^=  (key >> 10);
  key +=  (key << 3);
  key ^=  (key >> 6);
  key += ~(key << 11);
  key ^=  (key >> 16);
  return key;
}
}