#pragma once

// Compiler
#ifdef _MSC_VER
  #define ZZZ_COMPILER_MSVC
  #if _MSC_VER == 1800
    #define ZZZ_COMPILER_MSVC_2013
    #define ZZZ_COMPILER_MSVC_12
  #elif _MSC_VER == 1700
    #define ZZZ_COMPILER_MSVC_2012
    #define ZZZ_COMPILER_MSVC_11
  #elif _MSC_VER == 1600
    #define ZZZ_COMPILER_MSVC_2010
    #define ZZZ_COMPILER_MSVC_10
  #elif _MSC_VER == 1500
    #define ZZZ_COMPILER_MSVC_2008
    #define ZZZ_COMPILER_MSVC_9
  #elif _MSC_VER == 1400
    #define ZZZ_COMPILER_MSVC_2005
    #define ZZZ_COMPILER_MSVC_8
  #elif _MSC_VER == 1310
    #define ZZZ_COMPILER_MSVC_2003
    #define ZZZ_COMPILER_MSVC_7_1
  #elif _MSC_VER == 1300
    #define ZZZ_COMPILER_MSVC_7
  #elif _MSC_VER == 1200
    #define ZZZ_COMPILER_MSVC_6
  #elif _MSC_VER == 1100
    #define ZZZ_COMPILER_MSVC_5
  #endif
#elif defined(__MINGW32__)
  #define ZZZ_COMPILER_MINGW
#elif defined(__CYGWIN__)
  #define ZZZ_COMPILER_CYGWIN
#elif defined(__GNUC__)
  #define ZZZ_COMPILER_GCC
#elif defined(__INTEL_COMPILER)
  #define ZZZ_COMPILER_INTEL
#endif

// OS
#if defined(_UNIX)
  #define ZZZ_OS_UNIX
#elif defined(__linux)
  #define ZZZ_OS_LINUX
#elif defined(_MACOSX)
  #define ZZZ_OS_OSX
#elif defined(_WINDOWS) || defined(WIN32) || defined(WIN64) || defined(_WIN32) || defined(_WIN64)
  #define ZZZ_OS_WIN
  #if defined(_WIN64)
    #define ZZZ_OS_WIN64
  #else
    #define ZZZ_OS_WIN32
  #endif
#endif

#if defined(ZZZ_COMPILER_GCC) || defined(ZZZ_COMPILER_INTEL)
#ifdef __APPLE__
#define ZZZ_OS_MACOS
#endif
#endif

// Architecture
// By GNU C
#ifdef __i386__
#define ZZZ_ARCH_I386
#elif defined(__ia64__)
#define ZZZ_ARCH_I64
#endif

// By MSVC, Intel
#ifdef _M_IX86
#define ZZZ_ARCH_I386
#elif defined(_M_IA64)
#define ZZZ_ARCH_I64
#endif

// By MinGW32
#ifdef _X86_
#define ZZZ_ARCH_I386
#endif

// Debug or Release
#ifdef ZZZ_COMPILER_MSVC
  #ifdef _DEBUG
    #define ZZZ_DEBUG
  #else
    #define ZZZ_RELEASE
  #endif
#endif

#ifdef ZZZ_COMPILER_GCC
  #ifdef NDEBUG
    #define ZZZ_RELEASE
  #else
    #define ZZZ_DEBUG
  #endif
#endif
