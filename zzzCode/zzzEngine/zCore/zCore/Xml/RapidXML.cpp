#define ZCORE_SOURCE
#include "RapidXML.hpp"
#include <zCore/Utility/ZCheck.hpp>
#include <zCore/Utility/FileTools.hpp>

#include <zCore/3rdParty/rapidxml/rapidxml_print.hpp>
using namespace rapidxml;
namespace zzz{
RapidXML::RapidXML(bool add_declaration)
  : RapidXMLNode(&doc_, this),
    add_declaration_(add_declaration) {
}

bool RapidXML::LoadFile(const string &filename) {
  filename_ = filename;
  string text;
  ZCHECK(ReadFileToString(filename, text)) << "Cannot open file " << filename;
  filetext_.SetSize(Vector<1, int>(text.size()) + 1);
  memcpy(filetext_.Data(), text.data(), text.size());
  filetext_.back() = 0;
  doc_.parse<0>(filetext_.Data());
  return true;
}

bool RapidXML::SaveFile(const string &filename) {
  filename_ = filename;
  ofstream fo(filename.c_str());
  ZCHECK(fo.good()) << "Cannot open file " << filename;
  fo << doc_;
  return true;
}

const string& RapidXML::GetFilename() const {
  return filename_;
}

}
