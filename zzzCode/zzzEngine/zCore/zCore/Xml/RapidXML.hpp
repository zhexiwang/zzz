#pragma once
#include "../Utility/IOInterface.hpp"
#include "../Utility/Uncopyable.hpp"
#include "RapidXMLNode.hpp"
#include <zCore/Math/Array.hpp>
namespace zzz{
class ZCORE_CLASS RapidXML : public RapidXMLNode, public IOData, public Uncopyable {
public:
  RapidXML(bool add_declaration = true);
  bool LoadFile(const string &);
  bool SaveFile(const string &);
  const string& GetFilename() const;
private:
  rapidxml::xml_document<> doc_;
  string filename_;
  Array<1, char> filetext_;
  bool add_declaration_;
};
}
