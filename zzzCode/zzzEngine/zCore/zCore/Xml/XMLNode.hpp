#pragma once
#include <z3rd/LibraryConfig.hpp>

#ifdef ZZZ_LIB_TINYXML
#include "../common.hpp"
#include "../Utility/StringTools.hpp"
class TiXmlElement;
namespace zzz{
class XML;
class XMLNode
{
public:
  XMLNode(const XMLNode &node);
  explicit XMLNode(TiXmlElement *node, const XML *root);

  bool IsValid() const;
  bool SetName(const string &name);

  XMLNode GetParent();

  zuint NodeNumber() const;
  XMLNode GetNode(zuint i);
  XMLNode GetNode(const string &name);
  XMLNode AppendNode(const string &name);
  bool HasNode(const string &name);
  bool RemoveNode(const string &name);
  bool RemoveNode(zuint i);

  //iteration
  XMLNode GetFirstNode(const char *name=NULL);
  XMLNode GetNextSibling(const char *name=NULL);
  void GotoNextSibling(const char *name=NULL);
  void operator++();

  bool HasAttribute(const string &name) const;
  const char *GetAttribute(const string &name, const char *missing=NULL) const;
  template<typename T>bool GetAttribute(const string &name, T& value) const
  {
    const char *str=GetAttribute(name);
    if (str==NULL) return false;
    value=FromString<T>(str);
    return true;
  }
  bool SetAttribute(const string &name, const string &value) const;
  bool SetAttribute(const string &name, const char *value) const;
  template<typename T>
  bool SetAttribute(const string &name, const T &value) const {
    return SetAttribute(name,ToString(value));
  }
  bool RemoveAttribute(const string &name);

  const char *GetText() const;
  bool SetText(const string &text) const;
  bool RemoveText();
  const char *GetName() const;

  const XML *root_;
protected:
  TiXmlElement* node_;
};

//input text
inline const XMLNode& operator<<(const XMLNode &node, const char *text)
{
  node.SetText(text);
  return node;
}

inline const XMLNode& operator<<(const XMLNode &node, const string &text)
{
  node.SetText(text.c_str());
  return node;
}

//input attribute
template<typename T>
inline const XMLNode& operator<<(const XMLNode &node, const T &text)
{
  node.SetText(ToString(text).c_str());
  return node;
}

inline const XMLNode& operator<<(const XMLNode &node, const pair<const string &, const char *> &attrib)
{
  node.SetAttribute(attrib.first, attrib.second);
  return node;
}

inline const XMLNode& operator<<(const XMLNode &node, const pair<const string &, string> &attrib)
{
  node.SetAttribute(attrib.first, attrib.second.c_str());
  return node;
}

template<typename T>
inline const XMLNode& operator<<(const XMLNode &node, const pair<const string &, T> &attrib)
{
  node.SetAttribute(attrib.first, ToString(attrib.second).c_str());
  return node;
}


}
#endif // ZZZ_LIB_TINYXML
