#pragma once
#include "../Utility/IOInterface.hpp"
#include "../Utility/Uncopyable.hpp"
#include "XMLNode.hpp"

#ifdef ZZZ_LIB_TINYXML
class TiXmlDocument;
namespace zzz{
class XML : public IOData, public Uncopyable
{
public:
  XML();
  ~XML();
  bool LoadFile(const string &);
  bool SaveFile(const string &);

  zuint NodeNumber() const;
  XMLNode GetNode(zuint i);
  XMLNode GetNode(const string &name);
  bool HasNode(const string &name);
  XMLNode AppendNode(const string &name);
  bool RemoveNode(const string &name);
  bool RemoveNode(zuint i);

  const string& GetFilename() const;
private:
  TiXmlDocument *doc_;
  string filename_;
};
}
#endif // ZZZ_LIB_TINYXML
