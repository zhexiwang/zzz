#pragma once
#include <zCore/zCoreConfig.hpp>
#include "../common.hpp"
#include "../Utility/StringTools.hpp"
#include <zCore/3rdParty/rapidxml/rapidxml.hpp>

namespace zzz{
class RapidXML;
class ZCORE_CLASS RapidXMLNode {
public:
  RapidXMLNode(const RapidXMLNode &node);
  explicit RapidXMLNode(rapidxml::xml_node<> *node, const RapidXML *root);

  bool IsValid() const;
  bool SetName(const string &name);

  RapidXMLNode GetParent();

  zuint NodeNumber(rapidxml::node_type t = rapidxml::node_element) const;
  zuint NodeNumber(const string &name, rapidxml::node_type t = rapidxml::node_element) const;
  RapidXMLNode GetNode(zuint i, rapidxml::node_type t = rapidxml::node_element);
  RapidXMLNode GetNode(const string &name);
  RapidXMLNode AppendNode(const string &name, rapidxml::node_type t = rapidxml::node_element);
  bool HasNode(const string &name, rapidxml::node_type t = rapidxml::node_element) const;
  bool RemoveNode(const string &name);
  bool RemoveNode(zuint i, rapidxml::node_type t = rapidxml::node_element);

  // Iteration
  RapidXMLNode GetFirstNode(const string &name=string()) const;
  RapidXMLNode GetNextSibling(const string &name=string()) const;
  void GotoNextSibling(const string &name=string());
  void operator++();
  void operator++(int);

  bool HasAttribute(const string &name) const;
  const char *GetAttribute(const string &name, const char *missing=NULL) const;
  template<typename T>bool GetAttribute(const string &name, T& value) const {
    const string &str=GetAttribute(name);
    value = FromString<T>(str);
    return true;
  }
  bool SetAttribute(const string &name, const char *value);
  bool SetAttribute(const string &name, const string &value);
  template<typename T>
  bool SetAttribute(const string &name, const T &value) {
    return SetAttribute(name,ToString(value));
  }
  bool RemoveAttribute(const string &name);

  const char *GetText() const;
  bool SetText(const char *text);
  bool SetText(const string &text);
  bool RemoveText();

  RapidXMLNode AppendCommentNode();
  
  const char *GetName() const;
protected:
  const RapidXML *root_;
  rapidxml::xml_node<>* node_;
};

//input text
inline RapidXMLNode operator<<(RapidXMLNode node, const char *text) {
  string ori = node.GetText();
  ori += text;
  node.SetText(ori);
  return node;
}

inline RapidXMLNode operator<<(RapidXMLNode node, const string &text) {
  string ori = node.GetText();
  ori += text;
  node.SetText(text);
  return node;
}

//input attribute
template<typename T>
inline RapidXMLNode operator<<(RapidXMLNode node, const T &text) {
  string ori = node.GetText();
  ori += ToString(text);
  node.SetText(ori);
  return node;
}

inline RapidXMLNode operator<<(RapidXMLNode node, const pair<const char *, const char *> &attrib) {
  node.SetAttribute(attrib.first, attrib.second);
  return node;
}

inline RapidXMLNode operator<<(RapidXMLNode node, const pair<const char *, const string> &attrib) {
  node.SetAttribute(attrib.first, attrib.second.c_str());
  return node;
}

template<typename T>
inline RapidXMLNode operator<<(RapidXMLNode node, const pair<const char *, T> &attrib) {
  node.SetAttribute(attrib.first, ToString(attrib.second));
  return node;
}

inline RapidXMLNode operator<<(RapidXMLNode node, const pair<const string, const char *> &attrib) {
  node.SetAttribute(attrib.first, attrib.second);
  return node;
}

inline RapidXMLNode operator<<(RapidXMLNode node, const pair<const string, const string> &attrib) {
  node.SetAttribute(attrib.first, attrib.second);
  return node;
}

template<typename T>
inline RapidXMLNode operator<<(RapidXMLNode  node, const pair<const string, T> &attrib) {
  node.SetAttribute(attrib.first, ToString(attrib.second));
  return node;
}

} // namespace zzz
