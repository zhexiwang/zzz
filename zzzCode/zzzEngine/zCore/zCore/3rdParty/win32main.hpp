#pragma once
#include <zCore/zCoreConfig.hpp>

//include this file and change subsystem to window
//application will become a win32 application without a console
#ifdef ZZZ_OS_WIN32
#include <windows.h>
#include <io.h>

//to create console window in win32 application
inline bool CreateConsoleWindow(const string &title=0, int lines=-1)
{
  int  hConHandle;
  long lStdHandle;
  FILE *fp;
  if (AllocConsole()==FALSE) return false;
  // set the screen buffer to be big enough to let us scroll text
  if (lines!=-1)
  {
    CONSOLE_SCREEN_BUFFER_INFO coninfo;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),  &coninfo);
    coninfo.dwSize.Y = lines;
    // How many lines do you want to have in the console buffer
    SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE),  coninfo.dwSize);
  }
  // redirect unbuffered STDOUT to the console
  lStdHandle = (long)(__int64)GetStdHandle(STD_OUTPUT_HANDLE);
  hConHandle = _open_osfhandle(lStdHandle, 0x4000);
  fp = _fdopen(hConHandle, "w");
  *stdout = *fp;
  setvbuf(stdout, NULL, _IONBF, 0); 
  // redirect unbuffered STDIN to the console
  lStdHandle = (long)(__int64)GetStdHandle(STD_INPUT_HANDLE); 
  hConHandle = _open_osfhandle(lStdHandle, 0x4000);
  fp = _fdopen(hConHandle, "r"); 
  *stdin = *fp;
  setvbuf(stdin, NULL, _IONBF, 0);
  // redirect unbuffered STDERR to the console
  lStdHandle = (long)(__int64)GetStdHandle(STD_ERROR_HANDLE); 
  hConHandle = _open_osfhandle(lStdHandle, 0x4000);
  fp = _fdopen(hConHandle, "w");
  *stderr = *fp;
  setvbuf(stderr, NULL, _IONBF, 0);
  if (title==0) SetConsoleTitle("zzz Debug Console");
  else SetConsoleTitle(title);
  return true;
}

extern int main(int argc, char* argv[]);
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, INT)
{
  return main(__argc, __argv);
}
#endif // ZZZ_WIN32

