#pragma once

#include <zCore/EnvDetect.hpp>

//#define ZZZ_CONFIG_SIMPLE

#ifdef ZZZ_CONFIG_SIMPLE
  #include "zCoreConfig.simple.hpp"
#else
  #ifdef ZZZ_OS_WIN32
    #include "zCoreConfig.win32.hpp"
  #endif

  #ifdef ZZZ_OS_WIN64
    #include "zCoreConfig.win64.hpp"
  #endif

  #ifdef ZZZ_OS_MACOS
    #include "zCoreConfig.macos.hpp"
  #endif

  #ifdef ZZZ_OS_LINUX
    #include "zCoreConfig.linux.hpp"
  #endif

#endif

#ifdef ZZZ_DYNAMIC
  #pragma warning(disable:4251)  // needs to have dll-interface
  #pragma warning(disable:4275)  // non dll-interface class 'zzz::Uncopyable' used as base for dll-interface class 'zzz::RapidXML'

  #ifdef ZCORE_SOURCE
    #define ZCORE_FUNC __declspec(dllexport)
    #define ZCORE_CLASS __declspec(dllexport)
  #else
    #define ZCORE_FUNC __declspec(dllimport)
    #define ZCORE_CLASS __declspec(dllimport)
  #endif
#else
  #define ZCORE_FUNC
  #define ZCORE_CLASS
#endif

#ifdef ZZZ_COMPILER_MSVC
//WARNINGs
#pragma warning(disable:4100)//unreferenced formal parameter
#pragma warning(disable:4244)//double to float
#pragma warning(disable:4305)//double to float
#pragma warning(disable:4267)//: 'initializing' : conversion from 'size_t' to 'uint', possible loss of data
#pragma warning(disable:4522)//multiple assignment operators specified
#pragma warning(disable:4503)//decorated name length exceeded, name was truncated
#pragma warning(disable:4996)//'std::_Equal1': Function call with parameters that may be unsafe
#endif
