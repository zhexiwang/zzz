#pragma once
#include <zCore/EnvDetect.hpp>
#include <stdint.h>

namespace zzz {
//common
#undef min
#undef max

typedef uint8_t zuint8;
typedef uint16_t zuint16;
typedef uint32_t zuint32;
typedef uint64_t zuint64;
typedef int8_t zint8;
typedef int16_t zint16;
typedef int32_t zint32;
typedef int64_t zint64;

// zsize should only be used in IO.
// size variable used inside memory should follow the system.
#ifdef ZSIZE_32
typedef zuint32 zsize;
#else
typedef zuint64 zsize;
#endif

typedef unsigned char zuchar;
typedef unsigned short zushort;
typedef unsigned int zuint;
typedef unsigned long zulong;
typedef char zchar;
typedef short zshort;
typedef int zint;
typedef long zlong;

typedef zuint8 zbyte;
typedef zuint8 zubyte;

typedef float zfloat32;
typedef double zfloat64;

} // namespace zzz
