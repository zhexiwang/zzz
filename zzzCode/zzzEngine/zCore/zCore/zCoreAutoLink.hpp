#pragma once

// Separate link so when change a lib, only link needs redo.
#include "zCoreConfig.hpp"

#ifndef ZZZ_NO_PRAGMA_LIB

#ifdef ZZZ_COMPILER_MSVC

#ifdef ZZZ_DYNAMIC
  #ifdef ZZZ_DEBUG
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib, "zCoredllD.lib")
    #else
      #pragma comment(lib, "zCoredllD_x64.lib")
    #endif // ZZZ_OS_WIN64
  #else
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib, "zCoredll.lib")
    #else
      #pragma comment(lib, "zCoredll_x64.lib")
    #endif // ZZZ_OS_WIN64
  #endif
#else
  #ifdef ZZZ_DEBUG
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib, "zCoreD.lib")
    #else
      #pragma comment(lib, "zCoreD_x64.lib")
    #endif // ZZZ_OS_WIN64
  #else
    #ifndef ZZZ_OS_WIN64
      #pragma comment(lib, "zCore.lib")
    #else
      #pragma comment(lib, "zCore_x64.lib")
    #endif // ZZZ_OS_WIN64
  #endif
#endif

#endif // ZZZ_COMPILER_MSVC

#endif // ZZZ_NO_PRAGMA_LIB