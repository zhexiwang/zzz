#pragma once
//zzz Defination
#include "Define.hpp"
//C++ and STL
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cstdarg>
#include <ctime>
#include <cassert>
#include <vector>
#include <deque>
#include <stack>
#include <queue>
#include <map>
#include <set>
#include <list>
//#include <hash_map>
#include <functional>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <limits>
#include <string>
#include <sstream>
#include <exception>
#include <iterator>

#if (__GNUC__*10000+__GNUC_MINOR__*100+__GNUC_PATCHLEVEL__) > 40400
#include <tr1/unordered_map>
#include <tr1/unordered_set>
using std::tr1::unordered_map;
using std::tr1::unordered_set;
#elif defined(ZZZ_COMPILER_MSVC)
#include <unordered_map>
#include <unordered_set>
#else
#include <boost/tr1/unordered_map.hpp>
#include <boost/tr1/functional.hpp>
#endif

using namespace std;

//nvwa
#ifdef ENABLE_NVWA
#include <nvwa/debug_new.h>
#endif


