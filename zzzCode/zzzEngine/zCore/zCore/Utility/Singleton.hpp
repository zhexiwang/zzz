#pragma once

namespace zzz {
// for automatically destroy after program exists.
template<typename T>
class SingletonDestroyer {
public:
  SingletonDestroyer()
    : obj(nullptr){
  }
  ~SingletonDestroyer(){delete obj;}
  T *obj;
};

template<typename T>
class Singleton {
public:
  static T& Instance() {
    return *InstancePointer();
  }
  static T* InstancePointer() {
    if (nullptr == instance_) {
      instance_ = new T;
      destroyer_.obj = instance_;
    }
    return instance_;
  }
protected:
  Singleton() {
    instance_ = nullptr;
  }
private:
  static T* instance_;
  static SingletonDestroyer<T> destroyer_;
};

template<typename T>
T* Singleton<T>::instance_ = nullptr;

template<typename T>
SingletonDestroyer<T> Singleton<T>::destroyer_;
};  // namespace zzz
