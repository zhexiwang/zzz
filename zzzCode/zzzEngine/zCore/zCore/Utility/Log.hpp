#pragma once
#include <zCore/zCoreConfig.hpp>
#include <zCore/common.hpp>
#include "Singleton.hpp"
#include "zVar.hpp"
#include "StringTools.hpp"
#include "Timer.hpp"
#include <zCore/3rdParty/StackWalker.h>
#include <zCore/Utility/Thread.hpp>
#include <z3rd/Wrapper/PocoFoundationWrapper.hpp>

#ifdef ZZZ_COMPILER_MSVC
#pragma warning(disable:4722) // destructor never returns, potential memory leak
#endif
namespace zzz{
/////////////////////////////////////////////////
// VerboseLevel Setter
class VerboseLevel {
public:
  static int Set(int x) {
    old_=Get();
    verbose_level_=x;
    return old_;
  }
  static void Restore() {
    Set(old_);
  }
  static int Get() {
    return verbose_level_;
  }
private:
  ZCORE_FUNC static int old_;
  ZCORE_FUNC static int verbose_level_;
};

class ZCORE_CLASS GlobalLogger : public Singleton<GlobalLogger> {
public:

  GlobalLogger();
  ~GlobalLogger();

  void Clear();
  void Add(ostream *s);
  void Remove(ostream *s);
  typedef enum { WHITE, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, GRAY, DARKRED, DARKGREEN, DARKYELLOW, DARKBLUE, DARKMAGENTA, DARKCYAN} LoggerColor;
  void SetColor(LoggerColor color);
  void TestPrint();

  ZCORE_FUNC friend GlobalLogger& operator<<(GlobalLogger &,basic_ostream<char>& (&func)(basic_ostream<char>& _Ostr));
  ZCORE_FUNC friend GlobalLogger& operator<<(GlobalLogger &,ostream &v);
  ZCORE_FUNC friend GlobalLogger& operator<<(GlobalLogger &,const string &v);
  ZCORE_FUNC friend GlobalLogger& operator<<(GlobalLogger &,const char *v);
  
  inline void Lock() {log_lock_.Lock();}
  inline void Unlock() {log_lock_.Unlock();}
  void LockPrint(LoggerColor color, const string &screen_head, const string &file_head, const string& msg, LoggerColor after_color);
private:
  vector<ostream*> ostreams_;
  vector<ofstream*> ofstreams_;
  LoggerColor cur_color_;
  zzz::Mutex log_lock_;
};

ZCORE_FUNC GlobalLogger& operator<<(GlobalLogger &,basic_ostream<char>& (&func)(basic_ostream<char>& _Ostr));
ZCORE_FUNC GlobalLogger& operator<<(GlobalLogger &,ostream &v);
ZCORE_FUNC GlobalLogger& operator<<(GlobalLogger &,const string &v);
ZCORE_FUNC GlobalLogger& operator<<(GlobalLogger &,const char *v);

template<typename T>
GlobalLogger& operator<<(GlobalLogger &g, const T &v) {
  g << ToString(v);
  return g;
}

////////////////////////////////////////////
#ifdef ZZZ_NO_LOG
#define zout (cout)
#else
#define zout (GlobalLogger::Instance())
#endif

////////////////////////////////////////////
// All debug information, generally the information is useless.
class ZDEBUG {
public:
  static const char HEAD = 'D';
  static const GlobalLogger::LoggerColor COLOR = GlobalLogger::GREEN;
  static const int LEVEL = 0;
};
// Less important information, report more detailed message.
class ZVERBOSE {
public:
  static const char HEAD = 'V';
  static const GlobalLogger::LoggerColor COLOR = GlobalLogger::GRAY;
  static const int LEVEL = 30;
};
// Normal information.
class ZINFO {
public:
  static const char HEAD = 'I';
  static const GlobalLogger::LoggerColor COLOR = GlobalLogger::WHITE;
  static const int LEVEL = 60;
};
// Significant information, highlighting, will show even ZINFO is not shown.
class ZSIGNIFICANT {
public:
  static const char HEAD = 'S';
  static const GlobalLogger::LoggerColor COLOR = GlobalLogger::YELLOW;
  static const int LEVEL = 70;
};
// An error occurred, but it can be handled.
class ZERROR {
public:
  static const char HEAD = 'E';
  static const GlobalLogger::LoggerColor COLOR = GlobalLogger::RED;
  static const int LEVEL = 80;
};
// An unhandlable error occurred, program will exit after this.
class ZFATAL {
public:
  static const char HEAD = 'F';
  static const GlobalLogger::LoggerColor COLOR = GlobalLogger::MAGENTA;
  static const int LEVEL = 100;
};

template<typename SETTING>
class ZCORE_CLASS LevelLogger {
public:
  explicit LevelLogger(const char *filename=NULL, int line=-1) {
    if (SETTING::LEVEL >= VerboseLevel::Get()) {
      available_ = true;
      file_head_ << '[' << SETTING::HEAD << Poco::DateTimeFormatter::format(Poco::Timestamp(), "%m%d %H:%M:%S.%F ");
      if (filename) {
        const char *file = GetFilename(filename);
        screen_head_ << file << '(' << line << "): ";  // Visual Studio style error
        file_head_ << file << ':' << line;  // Google style
      }
      file_head_ << "] ";
    } else {
      available_ = false;
    }
  }
  ~LevelLogger() {
    if (available_) {
      zout.LockPrint(SETTING::COLOR, screen_head_.str(), file_head_.str(), oss_.str(), GlobalLogger::WHITE);
      if (SETTING::LEVEL >= 100) {
        zzz::PressAnyKeyToContinue();
        exit(-1);
      }
    }
  }

  template<typename SETTING>
  ZCORE_FUNC friend const LevelLogger<SETTING>& operator<<(const LevelLogger<SETTING> &,ostream&(&func)(ostream&_Ostr));
  template<typename SETTING>
  ZCORE_FUNC friend const LevelLogger<SETTING>& operator<<(const LevelLogger<SETTING> &,ostream &v);
  template<typename SETTING>
  ZCORE_FUNC friend const LevelLogger<SETTING>& operator<<(const LevelLogger<SETTING> &,const string &v);
  template<typename SETTING>
  ZCORE_FUNC friend const LevelLogger<SETTING>& operator<<(const LevelLogger<SETTING> &,const char *v);
  template<typename SETTING, typename T>
  ZCORE_FUNC friend const LevelLogger<SETTING>& operator<<(const LevelLogger<SETTING> &g, const T &v);

private:
  const char* GetFilename(const char* filename) {
    int len = strlen(filename);
    int start = len - 1;
    for (; start >= 0; --start) {
      switch (filename[start]) {
      case '/':
      case '\\':
        break;
      default:
        continue;
      }
      break;
    }
    ++start;
    return filename + start;
  } 
  int level_;
  bool available_;
  mutable ostringstream oss_, screen_head_, file_head_;
  GlobalLogger::LoggerColor color_;
};

template<typename SETTING>
ZCORE_FUNC inline const LevelLogger<SETTING>& operator<<(const LevelLogger<SETTING> &g, ostream&(&func)(ostream&_Ostr)) {
  if (g.available_) g.oss_ << func;
  return g;
}
template<typename SETTING>
ZCORE_FUNC inline const LevelLogger<SETTING>& operator<<(const LevelLogger<SETTING> &g, ostream &v) {
  if (g.available_) g.oss_ << v;
  return g;
}
template<typename SETTING>
ZCORE_FUNC inline const LevelLogger<SETTING>& operator<<(const LevelLogger<SETTING> &g, const string &v) {
  if (g.available_) g.oss_ << v;
  return g;
}
template<typename SETTING>
ZCORE_FUNC inline const LevelLogger<SETTING>& operator<<(const LevelLogger<SETTING> &g, const char *v) {
  if (g.available_) g.oss_ << v;
  return g;
}
template<typename SETTING, typename T>
ZCORE_FUNC inline const LevelLogger<SETTING>& operator<<(const LevelLogger<SETTING> &g, const T &v) {
  if (g.available_) g.oss_ << v;
  return g;
}

/////////////////////////////////////////////
class DummyLevelLogger : public LevelLogger<ZDEBUG> {
public:
  DummyLevelLogger(){}
  friend const DummyLevelLogger& operator<<(const DummyLevelLogger &,basic_ostream<char>& (&func)(basic_ostream<char>& _Ostr));
  friend const DummyLevelLogger& operator<<(const DummyLevelLogger &,ostream &v);
  friend const DummyLevelLogger& operator<<(const DummyLevelLogger &,const string &v);
  friend const DummyLevelLogger& operator<<(const DummyLevelLogger &,const char *v);
  template<typename T>
  friend const DummyLevelLogger& operator<<(const DummyLevelLogger &, const T &v);
};
inline const DummyLevelLogger& operator<<(const DummyLevelLogger &g,basic_ostream<char>& (&)(basic_ostream<char>& _Ostr)) {return g;}
inline const DummyLevelLogger& operator<<(const DummyLevelLogger &g,ostream &) {return g;}
inline const DummyLevelLogger& operator<<(const DummyLevelLogger &g,const string &) {return g;}
inline const DummyLevelLogger& operator<<(const DummyLevelLogger &g,const char *) {return g;}
template<typename T>
const DummyLevelLogger& operator<<(const DummyLevelLogger &g, const T &v) {return g;}

// Normal log
//#define ZLOG(LOG_SETTING) zzz::LevelLogger<LOG_SETTING>()
#define ZLOG(LOG_SETTING) ZLLOG(LOG_SETTING)
// ZL* will output the file and line of logging place
#define ZLLOG(LOG_SETTING) zzz::LevelLogger<LOG_SETTING>(__FILE__, __LINE__)
#define ZNOLOG() if (false) (zzz::DummyLevelLogger())


// Debug log, only log at debug mode
#ifdef ZZZ_DEBUG
#define ZDLOG(level) ZLOG(level)
#define ZLDLOG(level) ZLLOG(level)
#else
#define ZDLOG(level) ZNOLOG()
#define ZLDLOG(level) ZNOLOG()
#endif

// Release log, only log at non-fastest mode
#ifndef ZZZ_FASTEST
#define ZRLOG(level) ZLOG(level)
#define ZLRLOG(level) ZLLOG(level)
#else
#define ZRLOG(level) ZNOLOG()
#define ZLRLOG(level) ZNOLOG()
#endif

// Stop the code for debug or output stack
////////////////////////////////////////////////
#ifdef ZZZ_OS_WIN
  #if defined(ZZZ_RELEASE) && defined(ZZZ_STACKWALKER)
    class StackWalkerToConsole : public StackWalker {
    protected:
      virtual void OnOutput(LPCSTR szText) {
        LPCSTR filter1 = "C:\\Windows\\";
        if (equal(filter1, filter1 + strlen(filter1), szText) ||
            szText[8] == ' ' || szText[6] == ' ')
          return;
        ZLOG(zzz::ZERROR) << szText;
      }
    };
    #define OUTPUTSTACK() ZLOG(zzz::ZERROR) << endl; zzz::StackWalkerToConsole sw; sw.ShowCallstack(); 
  #else
    #define OUTPUTSTACK() void()
  #endif
#else
#define OUTPUTSTACK() void()
#endif

#ifdef ZZZ_DEBUG
  #ifdef ZZZ_OS_WIN
    #define CRASH_FUNC() _CrtDbgBreak()
  #else
    #define CRASH_FUNC() abort()
  #endif
#else
    #define CRASH_FUNC() void()
#endif

class ZCrasher {
public:
  ZCrasher(){}
  ZCrasher(int){};
  ~ZCrasher(){OUTPUTSTACK();}
  template<typename SETTING>
  const LevelLogger<SETTING>& operator=(const LevelLogger<SETTING>& x) const {return x;}
};
#define ZCRASHER() zzz::ZCrasher((CRASH_FUNC(), 1)) = ZLLOG(zzz::ZFATAL)


#define ZLOGI ZLOG(zzz::ZINFO)
#define ZLOGD ZLOG(zzz::ZDEBUG)
#define ZLOGV ZLOG(zzz::ZVERBOSE)
#define ZLOGS ZLOG(zzz::ZSIGNIFICANT)
#define ZLOGE ZLLOG(zzz::ZERROR)
#define ZLOGF ZCRASHER()

#define ZDLOGI ZDLOG(zzz::ZINFO)
#define ZDLOGD ZDLOG(zzz::ZDEBUG)
#define ZDLOGV ZDLOG(zzz::ZVERBOSE)
#define ZDLOGM ZDLOG(zzz::ZSIGNIFICANT)
#define ZDLOGE ZLDLOG(zzz::ZERROR)
#ifdef ZZZ_DEBUG
  #define ZDLOGF ZCRASHER()
#else
  #define ZDLOGF ZNOLOG()
#endif


#define ZRLOGI ZRLOG(zzz::ZINFO)
#define ZRLOGD ZDLOG(zzz::ZDEBUG)
#define ZRLOGV ZRLOG(zzz::ZVERBOSE)
#define ZRLOGS ZRLOG(zzz::ZSIGNIFICANT)
#define ZRLOGE ZLRLOG(zzz::ZERROR)
#ifndef ZZZ_FASTEST
  #define ZRLOGF ZCRASHER()
#else
  #define ZRLOGF ZNOLOG() 
#endif

#define ZMSG (ZLOGI)

////////////////////////////////////////////////
string SplitLine(const string &msg, int linelen=60);
string SplitLine3(const string &msg, int linelen=60);
////////////////////////////////////////////////
template<typename SETTING>
class ScopeLogger : public LevelLogger<SETTING> {
public:
  ScopeLogger() {
    timer_.Restart();
  }
  ~ScopeLogger() {
    (*this) << " Done. (" << timer_.Elapsed() << ")\n";
  }
private:
  Timer timer_;
};
#define ZSLOG(LOG_SETTING) zzz::ScopeLogger<LOG_SETTING> ScopeLogger##__LINE__();ScopeLogger##__LINE__
#define ZSLOGI ZSLOG(zzz::ZINFO)
#define ZSLOGD ZSLOG(zzz::ZDEBUG)
#define ZSLOGV ZSLOG(zzz::ZVERBOSE)
#define ZSLOGS ZSLOG(zzz::ZSIGNIFICANT)
} //  namespace zzz
