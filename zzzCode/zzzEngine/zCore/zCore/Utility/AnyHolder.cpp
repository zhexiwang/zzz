#include "AnyHolder.hpp"
#include <zCore/3rdParty/rapidjson/document.h>
#include <zCore/3rdParty/rapidjson/writer.h>
#include <zCore/3rdParty/rapidjson/stringbuffer.h>

namespace zzz {

void JsonToString(const rapidjson::Document &doc, string &str) {
  rapidjson::GenericStringBuffer<rapidjson::UTF8<> > buffer;
  rapidjson::Writer<rapidjson::GenericStringBuffer<rapidjson::UTF8<> > > writer(buffer);
  doc.Accept(writer);
  str = buffer.GetString();
}

void GetJsonString(const AnyHolder& holder, string& str) {
  vector<string> names;
  holder.GetNames(names);
  rapidjson::Document doc;
  doc.SetObject();
  for (zuint i = 0; i < names.size(); ++i) {
    const string& name = names[i];
    if (holder.IsType<int*>(name)) {
      doc.AddMember(names[i].c_str(), *holder.Get<int*>(name), doc.GetAllocator());
    } else if (holder.IsType<string*>(name)) {
      doc.AddMember(names[i].c_str(), holder.Get<string*>(name)->c_str(), doc.GetAllocator());
    } else if (holder.IsType<double*>(name)) {
      doc.AddMember(names[i].c_str(), *holder.Get<double*>(name), doc.GetAllocator());
    } else if (holder.IsType<float*>(name)) {
      doc.AddMember(names[i].c_str(), *holder.Get<float*>(name), doc.GetAllocator());
    } 
  }
  JsonToString(doc, str);
}

bool AnyHolder::Rename(const string &oldname, const string &newname) {
  map<string,Any>::iterator mi=holder_.find(oldname);
  if (mi == holder_.end()) {
    ZLOGE << "Cannot find: " << oldname << endl;
    return false;
  }
  if (holder_.find(newname)!=holder_.end()) {
    ZLOGE << "Already existed: " << newname << endl;
    return false;
  }
  holder_[newname]=mi->second;
  holder_.erase(oldname);
  return true;
}

void AnyHolder::GetNames(vector<string> &names) const {
  names.clear();
  names.reserve(holder_.size());
  for (map<string, Any>::const_iterator mi = holder_.begin(); mi != holder_.end(); ++mi)
    names.push_back(mi->first);
}

void AnyHolder::GetJsonString(string& str) const {
  return zzz::GetJsonString(*this, str);
}

}  // namespace zzz