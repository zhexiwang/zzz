#define ZCORE_SOURCE
#include <z3rd/LibraryConfig.hpp>
#include <zCore/zCoreConfig.hpp>
#include "Log.hpp"
#include "CmdParser.hpp"
#include "Tools.hpp"
#ifdef ZZZ_LIB_BOOST
  #include <boost/date_time/posix_time/posix_time.hpp>
#else
  #include <ctime>
#endif
#include "FileTools.hpp"
#include "StringPrintf.hpp"
#include <zCore/EnvDetect.hpp>
#ifdef ZZZ_OS_WIN
#include <windows.h>
#endif

namespace zzz{
int VerboseLevel::old_=60;
int VerboseLevel::verbose_level_=60;

// If VERBOSE_xxx is defined, a certain verbose level is used
// else default for DEBUG is ZDEBUG and for RELEASE is ZVERBOSE
#if defined VERBOSE_DEBUG
  const int DEFAULT_VERBOSE = 0;
#elif defined VERBOSE_VERBOSE
  const int DEFAULT_VERBOSE = 30;
#elif defined VERBOSE_INFO
  const int DEFAULT_VERBOSE = 60;
#elif defined VERBOSE_MORE
  const int DEFAULT_VERBOSE = 70;
#elif defined VERBOSE_ERROR
  const int DEFAULT_VERBOSE = 80;
#elif defined VERBOSE_FATAL
  const int DEFAULT_VERBOSE = 100;
#elif defined ZZZ_DEBUG
  const int DEFAULT_VERBOSE = 0;
#else
  const int DEFAULT_VERBOSE = 30;
#endif

ZFLAGS_INT(verbose_level, DEFAULT_VERBOSE, "Set Verbose Level");
ZFLAGS_SWITCH(verbose_debug, "Set Verbose Level: Debug (Show all msg)");
ZFLAGS_SWITCH(verbose_verbose, "Set Verbose Level: Verbose (Show additional msg)");
ZFLAGS_SWITCH(verbose_info, "Set Verbose Level: Info (Show normal msg)");
ZFLAGS_SWITCH(verbose_more, "Set Verbose Level: More (Show more important and error msg)");
ZFLAGS_SWITCH(verbose_error, "Set Verbose Level: Error (Only show error msg)");
ZFLAGS_SWITCH(verbose_fatal, "Set Verbose Level: Fatal (Only show fatal msg)");
ZFLAGS_STRING(glog_file, "", "Global Logger File");
ZFLAGS_BOOL(auto_log, true, "Automatically log to file");
ZFLAGS_STRING(auto_log_dir, GetEnv("ZLOG_DIR"), "Automatically log file directory");
ZFLAGS_BOOL(log_to_stdout, true, "Log to stdout");

void PostParseVerbose() {
  if (ZFLAG_verbose_debug)
    ZFLAG_verbose_level = 0;
  else if (ZFLAG_verbose_verbose)
    ZFLAG_verbose_level = 30;
  else if (ZFLAG_verbose_info)
    ZFLAG_verbose_level = 60;
  else if (ZFLAG_verbose_more)
    ZFLAG_verbose_level = 70;
  else if (ZFLAG_verbose_error)
    ZFLAG_verbose_level = 80;
  VerboseLevel::Set(ZFLAG_verbose_level);
}
ZFLAGS_POST(PostParseVerbose);

GlobalLogger::GlobalLogger()
    : cur_color_(RED) {
  SetColor(WHITE); // set color to white
  if (!ZFLAG_glog_file.empty()) {
    ofstream *fo = new ofstream(ZFLAG_glog_file.c_str());
    // zlog is not available yet! so we cannot use ZCHECK
    if (!fo->good()) {
      ZLOGE << "Cannot open file " << ZFLAG_glog_file << endl;
    } else {
      ofstreams_.push_back(fo);
      ostreams_.push_back(fo);
    }
  }

  if (ZFLAG_auto_log && !ZFLAG_auto_log_dir.empty()) {
    ZFLAG_auto_log_dir = CompleteDirName(ZFLAG_auto_log_dir);
#ifdef ZZZ_LIB_BOOST
    std::ostringstream msg;
    const boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
    boost::posix_time::time_facet *lf = new boost::posix_time::time_facet("%Y_%m_%d-%H_%M_%S");
    msg.imbue(std::locale(msg.getloc(),lf));
    msg << now;
    const string timestr = msg.str();
#else
    time_t rawtime;
    time(&rawtime);
    const string timestr = ctime(&rawtime);
#endif
    const string progstr = GetFilename(ZGLOBAL_GET(string, __CURRENT_PROGRAM_NAME__));
    const string dirstr = CompleteDirName(PathFile(ZFLAG_auto_log_dir, progstr));
    if (!DirExists(dirstr) && !MakeDir(dirstr)) {
      cerr << "Cannot create directory " << dirstr.c_str() << endl;
      cerr << "This directory is to store default log files for GlobalLogger. If you do not need the log file, set --no_auto_log\n";
      exit(-1);
    }
#if defined(ZZZ_DEBUG)
    const string filename = PathFile(dirstr, StringPrintf("[%s][%s][DEBUG].zlog", timestr.c_str(), progstr.c_str()));
#elif defined(ZZZ_FASTEST)
    const string filename = PathFile(dirstr, StringPrintf("[%s][%s][FASTEST].zlog", timestr.c_str(), progstr.c_str()));
#else
    const string filename = PathFile(dirstr, StringPrintf("[%s][%s][RELEASE].zlog", timestr.c_str(), progstr.c_str()));
#endif
    ofstream *fo = new ofstream(filename.c_str());
    // zlog is not available yet! so we cannot use ZCHECK
    if (!fo->good()) {
      cerr << "Cannot open file " << filename.c_str() << endl;
      cerr << "This file is a default log file for GlobalLogger. If you do not need the log file, set --no_auto_log\n";
      exit(-1);
    }
    ofstreams_.push_back(fo);
    ostreams_.push_back(fo);
  }
}

GlobalLogger::~GlobalLogger() {
  SetColor(GRAY);
  Clear();
}

void GlobalLogger::Clear() {
  ostreams_.clear();
  for (zuint i=0; i<ofstreams_.size(); i++)
    delete ofstreams_[i];
  ofstreams_.clear();
}

void GlobalLogger::Add(ostream *s) {
  ostreams_.push_back(s);
}

#ifdef ZZZ_OS_WIN
void GlobalLogger::SetColor(LoggerColor color) {
  if (cur_color_ == color) return;
  static HANDLE hCon=NULL;
  if(hCon == NULL) {
    hCon = GetStdHandle(STD_OUTPUT_HANDLE);
  }
  switch(color) {
  case WHITE:    // White on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
    break;
  case RED:    // Red on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_INTENSITY | FOREGROUND_RED);
    break;
  case GREEN:    // Green on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_INTENSITY | FOREGROUND_GREEN);
    break;
  case BLUE:    // Blue on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_INTENSITY | FOREGROUND_BLUE);
    break;
  case YELLOW:    // Yellow on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN);
    break;
  case MAGENTA:    // Magenta on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE);
    break;
  case CYAN:    // Cyan on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE);
    break;
  case GRAY:    // White on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
    break;
  case DARKRED:    // Red on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_RED);
    break;
  case DARKGREEN:    // Green on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_GREEN);
    break;
  case DARKBLUE:    // Blue on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_BLUE);
    break;
  case DARKYELLOW:    // Yellow on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_RED | FOREGROUND_GREEN);
    break;
  case DARKMAGENTA:    // Magenta on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_RED | FOREGROUND_BLUE);
    break;
  case DARKCYAN:    // Cyan on Black
    SetConsoleTextAttribute(hCon,
      FOREGROUND_GREEN | FOREGROUND_BLUE);
    break;
  }
  cur_color_ = color;
}
#else
void GlobalLogger::SetColor(LoggerColor color){}
#endif 

void GlobalLogger::TestPrint() {
  SetColor(WHITE); zout<<"WHITE ";
  SetColor(RED); zout<<"RED ";
  SetColor(GREEN); zout<<"GREEN ";
  SetColor(BLUE); zout<<"BLUE ";
  SetColor(YELLOW); zout<<"YELLOW ";
  SetColor(MAGENTA); zout<<"MAGENTA ";
  SetColor(CYAN); zout<<"CYAN ";
  SetColor(GRAY); zout<<"GRAY ";
  SetColor(DARKRED); zout << "DARKRED ";
  SetColor(DARKGREEN); zout << "DARKGREEN ";
  SetColor(DARKBLUE); zout << "DARKBLUE ";
  SetColor(DARKYELLOW); zout << "DARKYELLOW ";
  SetColor(DARKMAGENTA); zout << "DARKMAGENTA ";
  SetColor(DARKCYAN); zout << "DARKCYAN";
  zout << endl;

  VerboseLevel::Set(0);
  ZLOGD << "DEBUG MESSAGE\n";
  ZLOGV << "VERBOSE MESSAGE\n";
  ZLOGI << "INFO MESSAGE\n";
  ZLOGS << "SIGNIFICANT MESSAGE\n";
  ZLOGE << "ERROR MESSAGE\n";
  ZLOGF << "FATAL MESSAGE\n";
  VerboseLevel::Restore();
}

GlobalLogger& operator<<(GlobalLogger &g,basic_ostream<char>& (&func)(basic_ostream<char>& _Ostr)) {
  if (ZFLAG_log_to_stdout) cout << func << flush;
  for (vector<ostream*>::iterator vi = g.ostreams_.begin(); vi != g.ostreams_.end(); ++vi)
    (**vi) << func << flush;
  return g;
}

GlobalLogger& operator<<(GlobalLogger &g,ostream &v) {
  if (ZFLAG_log_to_stdout) cout << v << flush;
  for (vector<ostream*>::iterator vi = g.ostreams_.begin(); vi != g.ostreams_.end(); ++vi)
    (**vi) << v << flush;
  return g;
}

GlobalLogger& operator<<(GlobalLogger &g, const string &v) {
  if (ZFLAG_log_to_stdout) cout << v << flush;
  for (vector<ostream*>::iterator vi = g.ostreams_.begin(); vi != g.ostreams_.end(); ++vi)
    (**vi) << v.c_str() << flush;
  return g;
}

GlobalLogger& operator<<(GlobalLogger &g, const char *v) {
  if (ZFLAG_log_to_stdout) cout << v << flush;
  for (vector<ostream*>::iterator vi = g.ostreams_.begin(); vi != g.ostreams_.end(); ++vi)
    (**vi) << v << flush;
  return g;
}

void GlobalLogger::LockPrint(LoggerColor color, const string &screen_head, const string &file_head, const string& msg, LoggerColor after_color) {
  Lock();
  SetColor(color);
  if (ZFLAG_log_to_stdout) cout << screen_head << msg << endl << flush;
  for (vector<ostream*>::iterator vi = ostreams_.begin(); vi != ostreams_.end(); ++vi)
    (**vi) << file_head << msg << endl << flush;
  SetColor(after_color);
  Unlock();
}

string SplitLine(const string &msg, int linelen) {
  int len = msg.size();
  if (len + 2 >= linelen) return msg;
  int flen = (linelen - len) / 2;
  int blen = linelen - len - flen;
  if (flen == 1 || blen == 1) return "*" + string(msg) + "*";
  return string(flen - 1, '=') + ' ' + msg + ' ' + string(blen - 1, '=');
}

string SplitLine3(const string &msg, int linelen) {
  int len = msg.size();
  if (len + 2 >= linelen) return string(linelen, '=') + '\n' + string(msg) + '\n' + string(linelen, '=');
  int flen = (linelen - len) / 2;
  int blen = linelen - len - flen;
  if (flen == 1 || blen == 1) return string(linelen, '=') + "\n*" + string(msg) + "*\n" + string(linelen, '=');
  return string(linelen, '=') + "\n*" + string(flen - 1, ' ') + msg + string(blen - 1, ' ') + "*\n" + string(linelen, '=');
}

}
