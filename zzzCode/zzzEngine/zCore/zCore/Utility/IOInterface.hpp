#pragma once
#include "../common.hpp"
#include "../Xml/RapidXMLNode.hpp"

namespace zzz{
class IOData {
public:
  virtual bool LoadFile(const string &)=0;
  virtual bool SaveFile(const string &)=0;
};

class IODataA : public IOData {
public:
  bool SaveFile(const string &filename) {
    ofstream fo(filename.c_str());
    if (!fo.good()) return false;
    WriteFileA(fo);
    fo.close();
  }
  bool LoadFile(const string &filename) {
    ifstream fi(filename.c_str());
    if (!fi.good()) return false;
    ReadFileA(fi);
    fi.close();
  }
  virtual void WriteFileA(ostream &fo)=0;
  virtual void ReadFileA(istream &fi)=0;
};

class IODataB : public IOData {
public:
  bool SaveFile(const string &filename) {
    FILE *fp = fopen(filename.c_str(), "wb");
    if (!fp) return false;
    WriteFileB(fp);
    fclose(fp);
  }
  bool LoadFile(const string &filename) {
    FILE *fp = fopen(filename.c_str(), "rb");
    if (!fp) return false;
    ReadFileB(fp);
    fclose(fp);
  }
  virtual void WriteFileB(FILE *fp)=0;
  virtual void ReadFileB(FILE *fp)=0;
};

class IODataXml : public IOData {
public:
  virtual void WriteFileXml(RapidXMLNode &node)=0;
  virtual void ReadFileXml(RapidXMLNode &node)=0;
};

}
