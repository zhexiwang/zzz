#pragma once
#include "Any.hpp"
#include "ZCheck.hpp"

namespace zzz {
class AnyHolder {
public:
  virtual ~AnyHolder() {
    Clear();
  }

  template<typename T>
  bool Add(T v, const string &name, bool cover=false) {
    if (holder_.find(name)!=holder_.end()) {
      if (cover) {
        Remove(name);
        ZLOG(ZVERBOSE)<<"Duplicated name in AnyHolder: "<<name<<", removed the old one!\n";
      } else {
        ZLOG(ZVERBOSE)<<"Duplicated name in AnyHolder: "<<name<<", refuse to add!\n";
        return false;
      }
    }
    holder_[name]=Any(v);
    return true;
  }

  virtual bool Remove(const string &name) {
    map<string,Any>::iterator mi=holder_.find(name);
    if (mi==holder_.end()) {
      ZLOG(ZERROR)<<"object "<<name<<" is not in ResourceManager\n";
      return false;
    }
    Destroy(mi->second);
    holder_.erase(mi);
    return true;
  }

  virtual void Clear() {
    for (map<string,Any>::iterator mi=holder_.begin();mi!=holder_.end();mi++)
      Destroy(mi->second);
    holder_.clear();
  }

  template<typename T>
  T Get(const string &name) {
    map<string,Any>::iterator mi = holder_.find(name);
    ZCHECK(mi != holder_.end()) << "AnyHolder cannot find: " << name << endl;
    return any_cast<T>(mi->second);
  }

  template<typename T>
  const T Get(const string &name) const {
    map<string,Any>::const_iterator mi = holder_.find(name);
    ZCHECK(mi != holder_.end()) << "AnyHolder cannot find: " << name << endl;
    return any_cast<const T>(mi->second);
  }

  inline bool IsExist(const string &name) const {
    return holder_.find(name) != holder_.end();
  }

  template<typename T>
  bool IsType(const string &name) const {
    map<string, Any>::const_iterator mi=holder_.find(name);
    ZCHECK(mi != holder_.end()) << "Any Holder cannot find: " << name << endl;
    return mi->second.IsType<T>();
  }

  bool Rename(const string &oldname, const string &newname);
  void GetNames(vector<string> &names) const;
  void GetJsonString(string& str) const;
protected:
  virtual void Destroy(Any &){}
  map<string, Any> holder_;
};
}  // namespace zzz
