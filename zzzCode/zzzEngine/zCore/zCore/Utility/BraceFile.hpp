#pragma once

#include "FileTools.hpp"
#include "BraceNode.hpp"

namespace zzz {
class BraceItem {
public:
  BraceItem():head_(0),tail_(0){}
  virtual ~BraceItem();
  void Clear();
  BraceItem* AddNode(const string &str, const char h=0, const char t=0);
  string text_;
  char head_,tail_;
  vector<BraceItem*> children_;
};

typedef BraceItem SmallBraceItem;
class BraceFile : public BraceNode {
public:
  BraceFile(const string &braces="{}[]");
  void SetBraces(const string &braces="{}[]");
  bool LoadFile(const string &filename);
  bool SaveFile(const string &filename, const string &indent="\t");
protected:
  string braces_;
  BraceItem head_;
};
} // namespace zzz
