#pragma once

#include "../common.hpp"
namespace zzz{

#ifdef NO_ZCHECKPOINT
#define ZCHECKPOINT(msg) ;
#else
#define ZCHECKPOINT(msg) ZLOGI<<"*CP* "<<msg<<" at file: "<<__FILE__<<" line: "<<__LINE__<<endl;
#endif

}