#pragma once
#include "../common.hpp"
#include "IndexSet.hpp"

namespace zzz{
class StringGenerator : public IndexSet<string>
{
public:
  StringGenerator(){}
  StringGenerator(const vector<string> &strs){strings_=strs;}
  StringGenerator(const StringGenerator &other){strings_=other.strings_;}
  const StringGenerator& operator=(const StringGenerator &other){strings_=other.strings_;}
  const StringGenerator& operator=(const vector<string> &strs){strings_=strs;}

  string Get(const zuint &i){return strings_[i];}
  zuint Size(){return strings_.size();}

  void Clear(){strings_.clear();}
  void PopBack(const string &str){strings_.pop_back();}
  void PushBack(const string &str){strings_.push_back(str);}
private:
  vector<string> strings_;
};

class StringContGenerator : public IndexSet<string>
{
public:
  StringContGenerator()
  :start_(0), end_(0)
  {}
  StringContGenerator(const string &pattern, int start, int end)
  :pattern_(pattern), start_(start), end_(end)
  {}
  StringContGenerator(const StringContGenerator &other)
  :pattern_(other.pattern_), start_(other.start_), end_(other.end_)
  {}
  const StringContGenerator& operator=(const StringContGenerator &other)
  {
    pattern_=other.pattern_;
    start_=other.start_;
    end_=other.end_;
  }

  void Set(const string &pattern, int start, int end)
  {
    pattern_=pattern;
    start_=start;
    end_=end;
  }

  string Get(zuint i)
  {
    char str[1024];
    sprintf(str,pattern_.c_str(), i+start_);
    return string(str);
  }

  zuint Size(){return end_-start_+1;}

private:
  string pattern_;
  int start_,end_;
};


}