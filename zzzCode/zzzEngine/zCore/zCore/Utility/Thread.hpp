#pragma once
#include <z3rd/Wrapper/PThreadWrapper.hpp>
#include <zCore/zCoreConfig.hpp>
#include "Uncopyable.hpp"

#ifdef ZZZ_LIB_PTHREAD
#include <pthread.h>

namespace zzz{
class ZCORE_CLASS Thread {
public:
  Thread();
  void Start();
  void SyncStart();
  void Wait();
  void Stop() {
    running_ = false;
    Wait();
  }

  bool IsRunning();
  virtual void Main()=0;

  pthread_t thread_;
  bool running_;
};

class ZCORE_CLASS Mutex : Uncopyable {
public:
  Mutex();
  ~Mutex();
  void Lock();
  void Unlock();
  bool TryLock();
  pthread_mutex_t mutex_; 
};

class ZCORE_CLASS Condition : Uncopyable {
public:
  Condition(Mutex &mutex);
  ~Condition();
  void Signal();
  void Wait();

  pthread_mutex_t *mutex_;
  pthread_cond_t cond_;
};
}
#endif // ZZZ_LIB_PTHREAD
