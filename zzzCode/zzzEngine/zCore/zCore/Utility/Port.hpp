#ifdef WIN32
#include <stdarg.h>
inline void va_copy(va_list &a, va_list &b) {
  a = b;
}

typedef int uid_t;
#endif