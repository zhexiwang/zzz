#pragma once
#include "AnyHolder.hpp"
// Options is a AnyHolder that holds porperties.
// These properties should be simple variables and string.
namespace zzz {
class Options : public AnyHolder {
public:
	bool Parse(const string &str);
};
}  // namespace zzz
