#include "Tools.hpp"
#include <zCore/Utility/CmdParser.hpp>


ZFLAGS_BOOL(enable_press_any_key_to_continue, true, "Wait for a key if PressAnyKeyToContinue() is called.");

namespace zzz {
void PressAnyKeyToContinue() {
  cout << "~~~ PRESS ANY KEY TO CONTINUE ~~~" << endl;
  if (ZFLAG_enable_press_any_key_to_continue)
    _getch();
  else
    cout << "Disabled waiting for key press" << endl;
}

}  // namespace zzz
