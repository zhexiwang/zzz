#pragma once
#include <zCore/common.hpp>
#include "Singleton.hpp"
#include "AnyHolder.hpp"

// Usage:
// ------------------------------------
// ZGLOBAL_DEFINE(int, y, 10);
// ZGLOBAL_GET(int, y) = 100;
// CHECK_EQUAL(100, y);
// CHECK_EQUAL(100, ZGLOBAL_GET(int, y));
// CHECK(ZGLOBAL_EXIST(y));
// CHECK(ZGLOBAL_ISTYPE(int, y));
// CHECK(!ZGLOBAL_ISTYPE(float, y));
// -------------------------------------

namespace zzz{
typedef Singleton<AnyHolder> Global;

// Add global variable with a name
#define ZGLOBAL_ADD_NAME(var, name) zzz::Global::Instance().Add(&var, name, false)
// Add global variable with the name of its own
#define ZGLOBAL_ADD(var) ZGLOBAL_ADD_NAME(var, #var)
// Access the variable with name as a string
#define ZGLOBAL_GET_NAME(type, name) (*(zzz::Global::Instance().Get<type*>(name)))
#define ZGLOBAL_ISTYPE_NAME(type, name) (zzz::Global::Instance().IsType<type*>(name))
#define ZGLOBAL_EXIST_NAME(name) (zzz::Global::Instance().IsExist(name))
// Access the variable with the name of its own
#define ZGLOBAL_GET(type, var) ZGLOBAL_GET_NAME(type, #var)
#define ZGLOBAL_ISTYPE(type, var) ZGLOBAL_ISTYPE_NAME(type, #var)
#define ZGLOBAL_EXIST(var) ZGLOBAL_EXIST_NAME(#var)

template<typename T>
class GlobalRegisterHelper {
public:
  GlobalRegisterHelper(const string &name, T& var) {
    ZGLOBAL_ADD_NAME(var, name);
  }
};
#define ZGLOBAL_DEFINE(type, name, default_value) \
  type name = default_value;\
  zzz::GlobalRegisterHelper<type> ___GLOBAL_##name##_HELPER___(#name, name);
}