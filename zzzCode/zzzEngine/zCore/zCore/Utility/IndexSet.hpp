#pragma once
#include <zCore/common.hpp>

//it is the base class of a kind of set
//it will take a integer and return a value
//for example, filename set can return filename
//and image set can return an image

namespace zzz{
template<typename T, typename IDX=zuint>
class IndexSet
{
public:
  virtual T Get(const IDX &i)=0;
};
}