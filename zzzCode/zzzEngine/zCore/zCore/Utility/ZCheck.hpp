#pragma once
#include "Log.hpp"

// Check and output message
#define ZCHECK_WITH_MSG(x, msg) if (!(x)) ZCRASHER() << msg

#ifdef ZZZ_DEBUG
#define ZDCHECK_WITH_MSG(x, msg) if (!(x)) ZCRASHER() << msg
#else
#define ZDCHECK_WITH_MSG(x, msg) (x); ZNOOUTPUT()
#endif

#ifndef ZZZ_FASTEST
#define ZRCHECK_WITH_MSG(x, msg) if (!(x)) ZCRASHER() << msg
#else
#define ZRCHECK_WITH_MSG(x, msg) (x); ZNOOUTPUT()
#endif

// General check
#define ZCHECK(x) ZCHECK_WITH_MSG((x), "ZCHECK failed: " << #x << " is TRUE!")
#define ZCHECK_FALSE(x) ZCHECK_WITH_MSG((!(x)), "ZCHECK failed: " << #x << " is FALSE!")

#define ZDCHECK(x) ZDCHECK_WITH_MSG((x), "ZCHECK failed: " << #x <<" is TRUE!")
#define ZDCHECK_FALSE(x) ZDCHECK_WITH_MSG((!(x)), "ZCHECK failed: " << #x << " is FALSE!")

#define ZRCHECK(x) ZRCHECK_WITH_MSG((x), "ZCHECK failed: " << #x << " is TRUE!")
#define ZRCHECK_FALSE(x) ZRCHECK_WITH_MSG((!(x)), "ZCHECK failed: " << #x << " is FALSE!")

// Combine
#define ZCHECK_AND(x, y) ZCHECK_WITH_MSG((x) && (y), "ZCHECK failed: ("<< #x <<" && "<<#y<<")")
#define ZCHECK_OR(x, y) ZCHECK_WITH_MSG((x) || (y), "ZCHECK failed: ("<<#x <<" || "<<#y<<")")
#define ZCHECK_NEITHER(x, y) ZCHECK_WITH_MSG(!(x) && !(y), "ZCHECK failed: (!"<<#x <<" && !"<<#y<<")")

#define ZDCHECK_AND(x, y) ZDCHECK_WITH_MSG((x) && (y), "ZCHECK failed: ("<< #x <<" && "<<#y<<")")
#define ZDCHECK_OR(x, y) ZDCHECK_WITH_MSG((x) || (y), "ZCHECK failed: ("<<#x <<" || "<<#y<<")")
#define ZDCHECK_NEITHER(x, y) ZDCHECK_WITH_MSG(!(x) && !(y), "ZCHECK failed: (!"<<#x <<" && |"<<#y<<")")

// Math
#define ZCHECK_EQ(x, y) ZCHECK_WITH_MSG((x)==(y), "ZCHECK failed: " << #x << " == " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZCHECK_NE(x, y) ZCHECK_WITH_MSG((x)!=(y), "ZCHECK failed: " << #x << " != " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZCHECK_LT(x, y) ZCHECK_WITH_MSG((x)<(y), "ZCHECK failed: " << #x << " < "<< #y << ":" << ZVAR(x) << ZVAR(y))
#define ZCHECK_LE(x, y) ZCHECK_WITH_MSG((x)<=(y), "ZCHECK failed: " << #x << " <= " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZCHECK_GT(x, y) ZCHECK_WITH_MSG((x)>(y), "ZCHECK failed: " << #x << " > " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZCHECK_GE(x, y) ZCHECK_WITH_MSG((x)>=(y), "ZCHECK failed: " << #x << " >= " << #y << ":" << ZVAR(x) << ZVAR(y))

#define ZDCHECK_EQ(x, y) ZDCHECK_WITH_MSG((x)==(y), "ZCHECK failed: " << #x << " == " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZDCHECK_NE(x, y) ZDCHECK_WITH_MSG((x)!=(y), "ZCHECK failed: " << #x << " != " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZDCHECK_LT(x, y) ZDCHECK_WITH_MSG((x)<(y), "ZCHECK failed: " << #x << " < "<< #y << ":" << ZVAR(x) << ZVAR(y))
#define ZDCHECK_LE(x, y) ZDCHECK_WITH_MSG((x)<=(y), "ZCHECK failed: " << #x << " <= " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZDCHECK_GT(x, y) ZDCHECK_WITH_MSG((x)>(y), "ZCHECK failed: " << #x << " > " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZDCHECK_GE(x, y) ZDCHECK_WITH_MSG((x)>=(y), "ZCHECK failed: " << #x << " >= " << #y << ":" << ZVAR(x) << ZVAR(y))

#define ZRCHECK_EQ(x, y) ZRCHECK_WITH_MSG((x)==(y), "ZCHECK failed: " << #x << " == " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZRCHECK_NE(x, y) ZRCHECK_WITH_MSG((x)!=(y), "ZCHECK failed: " << #x << " != " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZRCHECK_LT(x, y) ZRCHECK_WITH_MSG((x)<(y), "ZCHECK failed: " << #x << " < "<< #y << ":" << ZVAR(x) << ZVAR(y))
#define ZRCHECK_LE(x, y) ZRCHECK_WITH_MSG((x)<=(y), "ZCHECK failed: " << #x << " <= " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZRCHECK_GT(x, y) ZRCHECK_WITH_MSG((x)>(y), "ZCHECK failed: " << #x << " > " << #y << ":" << ZVAR(x) << ZVAR(y))
#define ZRCHECK_GE(x, y) ZRCHECK_WITH_MSG((x)>=(y), "ZCHECK failed: " << #x << " >= " << #y << ":" << ZVAR(x) << ZVAR(y))

// Within
#define ZCHECK_WITHIN(_min, _x, _max) ZCHECK_WITH_MSG(Within(_min, _x, _max), "ZCHECK failed: " #_x << " is within [" << _min << ", "<< _max << "]:" << ZVAR(_x))
#define ZDCHECK_WITHIN(_min, _x, _max) ZDCHECK_WITH_MSG(Within(_min, _x, _max), "ZCHECK failed: " #_x << " is within [" << _min << ", "<< _max << "]:" << ZVAR(_x))
#define ZRCHECK_WITHIN(_min, _x, _max) ZRCHECK_WITH_MSG(Within(_min, _x, _max), "ZCHECK failed: " #_x << " is within [" << _min << ", "<< _max << "]:" << ZVAR(_x))

// NULL positive, negative
#define ZCHECK_NULL(x) ZCHECK_WITH_MSG((x)==NULL, "ZCHECK failed: " << #x << " == NULL:" << ZVAR(x))
#define ZCHECK_NOT_NULL(x) ZCHECK_WITH_MSG((x)!=NULL, "ZCHECK failed: " << #x<< " != NULL:" << ZVAR(x))
#define ZCHECK_ZERO(x) ZCHECK_WITH_MSG((x)==0, "ZCHECK failed: " << #x << " == 0:" << ZVAR(x))
#define ZCHECK_NOT_ZERO(x) ZCHECK_WITH_MSG((x)!=0, "ZCHECK failed: " << #x << " != 0:" << ZVAR(x))
#define ZCHECK_POSITIVE(x) ZCHECK_WITH_MSG((x)>0, "ZCHECK failed: " << #x << " > 0:" << ZVAR(x))
#define ZCHECK_NEGATIVE(x) ZCHECK_WITH_MSG((x)<0, "ZCHECK failed: " << #x << " < 0:" << ZVAR(x))
#define ZCHECK_NON_POSITIVE(x) ZCHECK_WITH_MSG((x)<=0, "ZCHECK failed: " << #x << " <= 0:" << ZVAR(x))
#define ZCHECK_NON_NEGATIVE(x) ZCHECK_WITH_MSG((x)>=0, "ZCHECK failed: " << #x << " >= 0:" << ZVAR(x))

#define ZDCHECK_NULL(x) ZDCHECK_WITH_MSG((x)==NULL, "ZCHECK failed: " << #x << " == NULL:" << ZVAR(x))
#define ZDCHECK_NOT_NULL(x) ZDCHECK_WITH_MSG((x)!=NULL, "ZCHECK failed: " << #x<< " != NULL:" << ZVAR(x))
#define ZDCHECK_ZERO(x) ZDCHECK_WITH_MSG((x)==0, "ZCHECK failed: " << #x << " == 0:" << ZVAR(x))
#define ZDCHECK_NOT_ZERO(x) ZDCHECK_WITH_MSG((x)!=0, "ZCHECK failed: " << #x << " != 0:" << ZVAR(x))
#define ZDCHECK_POSITIVE(x) ZDCHECK_WITH_MSG((x)>0, "ZCHECK failed: " << #x << " > 0:" << ZVAR(x))
#define ZDCHECK_NEGATIVE(x) ZDCHECK_WITH_MSG((x)<0, "ZCHECK failed: " << #x << " < 0:" << ZVAR(x))
#define ZDCHECK_NON_POSITIVE(x) ZDCHECK_WITH_MSG((x)<=0, "ZCHECK failed: " << #x << " <= 0:" << ZVAR(x))
#define ZDCHECK_NON_NEGATIVE(x) ZDCHECK_WITH_MSG((x)>=0, "ZCHECK failed: " << #x << " >= 0:" << ZVAR(x))

#define ZRCHECK_NULL(x) ZRCHECK_WITH_MSG((x)==NULL, "ZCHECK failed: " << #x << " == NULL:" << ZVAR(x))
#define ZRCHECK_NOT_NULL(x) ZRCHECK_WITH_MSG((x)!=NULL, "ZCHECK failed: " << #x<< " != NULL:" << ZVAR(x))
#define ZRCHECK_ZERO(x) ZRCHECK_WITH_MSG((x)==0, "ZCHECK failed: " << #x << " == 0:" << ZVAR(x))
#define ZRCHECK_NOT_ZERO(x) ZRCHECK_WITH_MSG((x)!=0, "ZCHECK failed: " << #x << " != 0:" << ZVAR(x))
#define ZRCHECK_POSITIVE(x) ZRCHECK_WITH_MSG((x)>0, "ZCHECK failed: " << #x << " > 0:" << ZVAR(x))
#define ZRCHECK_NEGATIVE(x) ZRCHECK_WITH_MSG((x)<0, "ZCHECK failed: " << #x << " < 0:" << ZVAR(x))
#define ZRCHECK_NON_POSITIVE(x) ZRCHECK_WITH_MSG((x)<=0, "ZCHECK failed: " << #x << " <= 0:" << ZVAR(x))
#define ZRCHECK_NON_NEGATIVE(x) ZRCHECK_WITH_MSG((x)>=0, "ZCHECK failed: " << #x << " >= 0:" << ZVAR(x))
