#pragma once
#include "AutoloadCacheSet.hpp"

namespace zzz{
// It need a IndexSet<string, IDX> to provide filename, and load will call LoadFile(filename) to load object.
// The object need to implement IOData interface.
// Image is a good example.
template<typename T, typename IDX>
class ObjectCacheSet : public AutoloadCacheSet<T*, IDX>
{
public:
  using AutoloadCacheSet<T*, IDX>::SetSize;
    using AutoloadCacheSet<T*, IDX>::Clear;
  void SetData(IndexSet<string, IDX> *fset, zuint cache_size) {
    Clear();
    filenameSet=fset;
    SetSize(cache_size);
  }
  string GetFilename(const IDX &i) {
    return filenameSet->Get(i);
  }
private:
  T* Load(const IDX &i) {
    T *obj = new T;
    if (obj->LoadFile(GetFilename(i).c_str())) {
      return obj;
    } else {
      delete obj;
      return NULL;
    }
  }
  IndexSet<string, IDX> *filenameSet;
};
}
