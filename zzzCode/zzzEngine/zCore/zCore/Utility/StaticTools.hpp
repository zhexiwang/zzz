#pragma once

namespace zzz {
template<bool> struct StaticAssert;
template<> struct StaticAssert<true> {};
}  // namespace zzz