#pragma once
#include "Tools.hpp"
#include "IOObject.hpp"

namespace zzz{
class HasFlags {
public:
  HasFlags():flags_(0){}
  explicit HasFlags(unsigned int init):flags_(init){}
  bool HasFlag(int bit) const {return CheckBit(flags_,bit);}
  bool HasNoFlag(int bit) const {return !CheckBit(flags_,bit);}
  void SetFlag(int bit){return SetBit(flags_,bit);}
  void ClrFlag(int bit){return ClearBit(flags_,bit);}
  void ToggleFlag(int bit){return ToggleBit(flags_,bit);}
  void SetAllFlags(){flags_=0xFFFFFFFF;}
  void ClrAllFlags(){flags_=0;}
protected:
  unsigned int flags_;
};

SIMPLE_IOOBJECT(HasFlags);
}