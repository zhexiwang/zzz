#pragma once
#include <zCore/zCoreConfig.hpp>
#include "../common.hpp"

namespace zzz {
ZCORE_FUNC string StringPrintf(const char *format, ...);
ZCORE_FUNC const string &SStringPrintf(string &dst, const char *format, ...);
ZCORE_FUNC void StringAppendF(string &dst, const char *format, ...);
}  // namespace zzz
