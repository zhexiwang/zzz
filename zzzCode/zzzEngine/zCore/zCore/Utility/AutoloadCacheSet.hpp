#pragma once
#include "CacheSet.hpp"

// It is a kind of cache set.
// The different from cache set is it will automatically load when get fails.
// Derived class should overload Load() to load correct object.

namespace zzz{
template<typename T, typename IDX=zuint>
class AutoloadCacheSet : public CacheSet<T, IDX> {
public:
  using CacheSet<T, IDX>::Add;
  // Get data.
  T Get(const IDX &i) {
    T ret = CacheSet<T,IDX>::Get(i);
    if (ret == NULL) {
      ret = Load(i);
      if (ret != NULL)
        Add(i, ret);
    }
    return ret;
  }

  // Reload data
  T Reload(const IDX &i) {
    T* ret = Load(i);
    if (ret != NULL)
      Add(i, ret);
    return ret;
  }

protected:
  virtual T Load(const IDX& i)=0;
};
}
