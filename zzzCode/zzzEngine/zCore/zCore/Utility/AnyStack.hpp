#include "Any.hpp"
#include <zCore/common.hpp>
#include "Singleton.hpp"

namespace zzz {
class AnyStack {
public:
  template<typename T>
  void Push(const T &v) {
    stack_.push(Any(v));
  }

  template<typename T>
  void Top(T &v) {
    v=any_cast<T>(stack_.top());
  }

  template<typename T>
  T Top() {
    T v = any_cast<T>(stack_.top());
    return v;
  }

  template<typename T>
  void Pop(T &v) {
    Top(v);
    stack_.pop();
  }

  template<typename T>
  T Pop() {
    T v = Top<T>();
    stack_.pop();
    return v;
  }

  void Clear() {
    while(!stack_.empty())
      stack_.pop();
  }

  bool Empty() {
    return stack_.empty();
  }

  zuint Size() {
    return stack_.size();
  }
private:
  stack<Any> stack_;
};

typedef Singleton<AnyStack> GlobalStack;
};  // namespace zzz

#define ZGS (zzz::GlobalStack::Instance())
