#include "Atomic.hpp"
#include <zCore/EnvDetect.hpp>

#if defined(ZZZ_NO_ATOMIC)
  int zzz::AtomicInc(long &value) { return ++value; }
  int zzz::AtomicDec(long &value) { return --value; }
#elif defined(ZZZ_OS_XENON) /// XBox360
  #include <xtl.h>
  int zzz::AtomicInc(long &value) { return InterlockedIncrement((LONG*)&value); }
  int zzz::AtomicDec(long &value) { return InterlockedDecrement((LONG*)&value); }
#elif defined(ZZZ_OS_WIN)
#define WIN32_MEAN_AND_LEAN
  #include <windows.h>
  int zzz::AtomicInc(long &value) { return InterlockedIncrement((LONG*)&value); }
  int zzz::AtomicDec(long &value) { return InterlockedDecrement((LONG*)&value); }
#elif defined(ZZZ_OS_LINUX)
//
// atomic_inc_and_test() and atomic_dec_and_test() from asm/atomic.h is not meant 
// to be used outside the Linux kernel. Instead we should use the GNUC provided 
// __sync_add_and_fetch() and __sync_sub_and_fetch() functions.
//
// Reference: http://golubenco.org/blog/atomic-operations/
//
// These are only available in GCC 4.1 and above, so for older versions we 
// use the critical sections, though it is a lot slower.
// 
  int zzz::AtomicInc(long &value) { return __sync_add_and_fetch(&value, 1); }
  int zzz::AtomicDec(long &value) { return __sync_sub_and_fetch(&value, 1); }
#elif defined(ZZZ_OS_MACOS)
  #include <libkern/OSAtomic.h>
  int zzz::AtomicInc(long &value) { return OSAtomicIncrement32((int32_t*)&value); }
  int zzz::AtomicDec(long &value) { return OSAtomicDecrement32((int32_t*)&value); }
#else
// If we get here, then the configuration in as_config.h
//  is wrong for the compiler/platform combination. 
  int ERROR_PleaseFixTheConfig[-1];
#endif