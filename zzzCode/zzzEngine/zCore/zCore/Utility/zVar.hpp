#pragma once
#include <zCore/common.hpp>

namespace zzz {
#ifdef ZZZ_COMPILER_MSVC
template <typename T>
class zVar {
public:
  zVar(const T &v, const int) : v_(v){}
private:
  inline friend ostream& operator<<(ostream& os, const zVar<T> &me) {
    os << me.v_;
    return os;
  }
  const T &v_;
};

template <>
class zVar<bool> {
public:
  zVar(const bool &v, const int) : v_(v){}
private:
  inline friend ostream& operator<<(ostream& os, const zVar<bool> &me) {
    os << me.v_ ? "true" : "false";
    return os;
  }
  const bool &v_;
};

template <>
class zVar<char> {
public:
  zVar(const char &v, const int) : v_(v){}
private:
  inline friend ostream& operator<<(ostream& os, const zVar<char> &me) {
    if (isprint(me.v_))
      os << me.v_;
    else
      os << '\\' << int(me.v_);
    return os;
  }
  const char &v_;
};
#else
template <typename T>
class zVar {
public:
  zVar(const T &v, const int) : v_(v){}
  const T &v_;
};

inline ostream& operator<<(ostream& os, const zVar<string> &me) {
  os << me.v_.c_str();
  return os;
}

template<typename T>
inline ostream& operator<<(ostream& os, const zVar<T> &me) {
  os << me.v_;
  return os;
}
#endif

template<typename T>
inline const zVar<T> GetzVar(const T &v, const int limit) {return zVar<T>(v, limit);}

#define ZVAR_DEFAULT_LIMIT 10

#define ZVARVALUE_LIMIT(x, limit) ::zzz::GetzVar(x, limit)
#define ZVARVALUE_FULL(x) ZVARVALUE_LIMIT(x, 0)
#define ZVARVALUE(x) ZVARVALUE_LIMIT(x, ZVAR_DEFAULT_LIMIT)

#define ZVAR2_FULL(name, x) '{' << name << '=' << ZVARVALUE_FULL(x) << '}'
#define ZVAR2_LIMIT(name, x, limit) '{' << name << '=' << ZVARVALUE_LIMIT(x, limit) << '}'
#define ZVAR2(name, x) ZVAR2_LIMIT(name, x, ZVAR_DEFAULT_LIMIT)

#define ZVAR_FULL(x) ZVAR2_FULL(#x, x)
#define ZVAR_LIMIT(x, limit) ZVAR2_LIMIT(#x, x, limit)
#define ZVAR(x) ZVAR_LIMIT(x, ZVAR_DEFAULT_LIMIT)

#define ZVARARRAY2_LIMIT(name, x, len, limit) '{' << name << '=' << GetzVarArray(x, len, limit) << '}'
#define ZVARARRAY2_FULL(name, x, len) ZVARARRAY2_LIMIT(name, x, len, 0)
#define ZVARARRAY2(name, x, len) ZVARARRAY2_LIMIT(name, x, len, ZVAR_DEFAULT_LIMIT)
#define ZVARARRAY_FULL(x, len) ZVARARRAY2_FULL(#x, x, len)
#define ZVARARRAY_LIMIT(x, len, limit) ZVARARRAY2_LIMIT(#x, x, len, limit)
#define ZVARARRAY(x, len) ZVARARRAY2(#x, x, len)

template <typename T>
class zVarArray {
public:
  zVarArray(const T *p, const int len, const int limit):p_(p), len_(len), limit_(limit){}
  friend inline ostream& operator<<(ostream& os, const zVarArray<T> &me) {
    for (int i = 0; i < me.len_; ++i)
      os << '<' << ZVARVALUE_LIMIT(me.p_[i], me.limit_) << '>';
    return os;
  }
private:
  const T *p_;
  const int len_, limit_;
};

template <typename T>
inline const zVarArray<T> GetzVarArray(const T *p, const int len, const int limit) {return zVarArray<T>(p, len, limit);}

#ifdef ZZZ_COMPILER_MSVC
template<>
class zVar<std::string> {
public:
  zVar(const std::string &v, const int limit):v_(v), limit_(limit * 10){}
  friend inline ostream& operator<<(ostream& os, const zVar<std::string> &me) {
    os << "[";
    if (int(me.v_.size()) > me.limit_) os << ZVAR2("Size", me.v_.size()) << "<";
    if (me.limit_ <= 0 || int(me.v_.size()) <= me.limit_) {
      os << me.v_;
    } else {
      std::string tmp(me.v_.begin(), me.v_.begin() + me.limit_);
      os << tmp << "><" << me.v_.size() - me.limit_ << " More>";
    }
    if (int(me.v_.size()) > me.limit_) os << ">";
    os << "]";
    return os;
  }
private:
  const std::string &v_;
  const int limit_;
};
#endif


template <typename T1, typename T2>
class zVar<std::pair<T1, T2> > {
public:
  zVar(const std::pair<T1, T2> &v, const int limit):v_(v),limit_(limit){}
  friend inline ostream& operator<<(ostream& os, const zVar<std::pair<T1, T2> > &me) {
    os << ZVARVALUE_LIMIT(me.v_.first, me.limit_) << "|" << ZVARVALUE_LIMIT(me.v_.second, me.limit_);
    return os;
  }
private:
  const std::pair<T1, T2> &v_;
  const int limit_;
};

// container with one typename
#define STL_CONTAINER_ZVAR(C) \
template <typename T>\
class zVar< C<T> > {\
public:\
  zVar(const C<T> &v, const int limit):v_(v), limit_(limit){}\
  friend inline ostream& operator<<(ostream& os, const zVar< C<T> > &me) {\
    os << "[" << ZVAR2("Size", me.v_.size());\
    typename C<T>::const_iterator vi;\
    if (me.limit_ > 0) {\
      int i;\
      for (i = 0, vi = me.v_.begin(); i < me.limit_ && vi != me.v_.end(); ++vi, ++i)\
        os << "<" << ZVARVALUE_LIMIT(*vi, me.limit_) << ">";\
      if (i == me.limit_ && vi != me.v_.end())\
        os << "<" << me.v_.size() - i << " More>";\
    } else {\
      for (vi =me.v_.begin(); vi != me.v_.end(); ++vi)\
        os << "<" << ZVARVALUE_LIMIT(*vi, me.limit_) << ">";\
    }\
    os << "]";\
    return os;\
  }\
private:\
  const C<T> &v_;\
  const int limit_;\
};

// container with two typename
#define STL_CONTAINER_2_ZVAR(C) \
template <typename T1, typename T2>\
class zVar< C<T1, T2> > {\
public:\
  zVar(const C<T1, T2> &v, const int limit):v_(v), limit_(limit){}\
  friend inline ostream& operator<<(ostream& os, const zVar< C<T1, T2> > &me) {\
    os << "[" << ZVAR2("Size", me.v_.size());\
    typename C<T1, T2>::const_iterator vi;\
    if (me.limit_ > 0) {\
      int i;\
      for (i = 0, vi = me.v_.begin(); i < me.limit_ && vi != me.v_.end(); ++vi, ++i)\
        os << "<" << ZVARVALUE_LIMIT(*vi, me.limit_) << ">";\
      if (i == me.limit_ && vi != me.v_.end())\
        os << "<" << me.v_.size() - i << " More>";\
    } else {\
      for (vi =me.v_.begin(); vi != me.v_.end(); ++vi)\
        os << "<" << ZVARVALUE_LIMIT(*vi, me.limit_) << ">";\
    }\
    os << "]";\
    return os;\
  }\
private:\
  const C<T1, T2> &v_;\
  const int limit_;\
};

STL_CONTAINER_ZVAR(std::vector);
STL_CONTAINER_ZVAR(std::list);
STL_CONTAINER_ZVAR(std::deque);
STL_CONTAINER_ZVAR(std::stack);
STL_CONTAINER_ZVAR(std::queue);
STL_CONTAINER_ZVAR(std::set);
STL_CONTAINER_ZVAR(std::multiset);
STL_CONTAINER_ZVAR(std::tr1::unordered_set);
STL_CONTAINER_ZVAR(std::tr1::unordered_multiset);
STL_CONTAINER_2_ZVAR(std::map);
STL_CONTAINER_2_ZVAR(std::multimap);
STL_CONTAINER_2_ZVAR(std::tr1::unordered_map);
STL_CONTAINER_2_ZVAR(std::tr1::unordered_multimap);

}; // namespace zzz
