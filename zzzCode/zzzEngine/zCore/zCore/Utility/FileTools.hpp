#pragma once
#include <zCore/zCoreConfig.hpp>
#include <zCore/common.hpp>
#include <z3rd/LibraryConfig.hpp>
#include <z3rd/Wrapper/BoostWrapper.hpp>
#include "STLVector.hpp"
#ifdef ZZZ_LIB_BOOST
#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_NO_CXX11_SCOPED_ENUMS  // This make it compilable in mingw.
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#endif
namespace zzz{
//////////////////////////////////////////////////////////////////////////
//path
#ifdef ZZZ_LIB_BOOST
typedef boost::filesystem::path Path;
#else
typedef string Path;
#endif

#undef CopyFile // defined in windows

ZCORE_FUNC string GetExt(const string &str);
ZCORE_FUNC string GetBase(const string &str);
ZCORE_FUNC string GetFilename(const string &str);
ZCORE_FUNC string GetPath(const string &str);
ZCORE_FUNC vector<string> SplitPath(const string &path);
ZCORE_FUNC void SplitPath(const string &path, vector<string> &splits);
ZCORE_FUNC string CompleteDirName(const string &str);
ZCORE_FUNC string PathFile(const string &path,const string &file);
ZCORE_FUNC string RelativeTo(const string &a,const string &to_a);
ZCORE_FUNC string NormalizePath(const string &path);
ZCORE_FUNC bool PathEquals(const string &f1, const string &f2);
ZCORE_FUNC string CurrentPath();
ZCORE_FUNC bool ChangePath(const string &cd);
ZCORE_FUNC string InitialPath();

///////////////////////////////////////////////////////////////////////////
//file operation
ZCORE_FUNC bool FileCanOpen(const string &filename);
ZCORE_FUNC bool FileExists(const string &filename);
ZCORE_FUNC bool DirExists(const string &filename);
ZCORE_FUNC bool IsSymlink(const string &filename);
ZCORE_FUNC void ListFileOnly(const string &path, bool recursive, vector<string> &files);
ZCORE_FUNC void ListDirOnly(const string &path, bool recursive, vector<string> &files);
ZCORE_FUNC void ListFileAndDir(const string &path, bool recursive, vector<string> &files);
ZCORE_FUNC bool CopyFile(const string &from, const string &to);
ZCORE_FUNC bool RenameFile(const string &from, const string &to);
ZCORE_FUNC bool RemoveFile(const string &f, bool recursive=false);
ZCORE_FUNC bool MakeDir(const string &dir);
ZCORE_FUNC bool MakeHardLink(const string &from, const string &to);
ZCORE_FUNC bool MakeSymLink(const string &from, const string &to);

//////////////////////////////////////////////////////////////////////////
//file
ZCORE_FUNC zuint64 GetFileSize(const string &filename);
ZCORE_FUNC bool ReadFileToString(const string &filename, char **buf);
ZCORE_FUNC bool ReadFileToString(const string &filename, string &buf);
ZCORE_FUNC bool SaveStringToFile(const string &filename, const string &buf);
ZCORE_FUNC string RemoveComments_copy(const string &buf);
ZCORE_FUNC bool RemoveComments(string &buf);
///////////////////////////////////////////////////////////////////////////
//help function
ZCORE_FUNC zuint FileCountLine(ifstream &fi);
ZCORE_FUNC zuint FileCountLine(const string &filename);

}
