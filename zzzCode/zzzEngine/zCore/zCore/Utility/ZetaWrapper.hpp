#pragma once
#include <zCore/Math/Vector4.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Math/Vector2.hpp>

namespace zzz {
template<typename T>
inline const Vector<2, T> z2z(const zeta::Vec<T, 2>& x) {
  return Vector<2, T>(x[0], x[1]);
}

template<typename T>
inline const Vector<3, T> z2z(const zeta::Vec<T, 3>& x) {
  return Vector<3, T>(x[0], x[1], x[2]);
}

template<typename T>
inline const Vector<4, T> z2z(const zeta::Vec<T, 4>& x) {
  return Vector<4, T>(x[0], x[1], x[2], x[3]);
}
}