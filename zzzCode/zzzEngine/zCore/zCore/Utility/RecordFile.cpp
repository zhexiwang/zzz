#define ZCORE_SOURCE
#include "RecordFile.hpp"
#include "IOObject.hpp"

namespace zzz {
SIMPLE_IOOBJECT(RecordItem::TableItem);

RecordItem::RecordItem(FILE *fp, zint64 itebegin_pos, RecordItem *parent)
  : fp_(fp),
    itebegin_pos_(itebegin_pos),
    child_(NULL),
    repeat_label_(-1),
    parent_(parent) {
  SetPos(itebegin_pos_+sizeof(zint64));
}

//////////////////////////////////////////////////////////////////////////
// Read
void RecordItem::ReadTable() {
  SetPos(itebegin_pos_);
  IOObj::ReadFileB(fp_, table_begin_pos_);
  SetPos(table_begin_pos_);
  IOObject<vector<RecordItem::TableItem> >::ReadFileB(fp_, table_);
}

RecordItem* RecordItem::ReadChild(const zint32 label) {
  ZCHECK(LabelExist(label));
  if (child_) delete child_;
  child_=new RecordItem(fp_, GetChildPos(label), this);
  zint64 pos = child_->GetPos();
  child_->ReadTable();
  child_->SetPos(pos);
  return child_;
}

RecordItem* RecordItem::ReadFinish() {
  return parent_;
}

RecordItem* RecordItem::ReadRepeatBegin(const zint32 label) {
  ZCHECK(LabelExist(label));
  if (child_) delete child_;
  child_ = new RecordItem(fp_, GetChildPos(label), this);
  child_->ReadTable();
  child_->repeat_label_=0;
  return child_;
}

RecordItem* RecordItem::ReadRepeatChild() {
  if (parent_->repeat_label_>=0) {
    // It's parent is repeat head.
    return parent_->ReadRepeatChild();
  } else {
    // Itself is repeat head.
    repeat_label_++;
    if (!LabelExist(repeat_label_))
      return NULL;
    return ReadChild(repeat_label_);
  }
}

size_t RecordItem::GetRepeatNumber() {
  if (parent_->repeat_label_>=0) {
    // It's child is repeat head.
    return parent_->GetItemNumber();
  } else {
    // Itself is repeat head.
    return GetItemNumber();
  }
}

RecordItem* RecordItem::ReadRepeatEnd() {
  if (parent_->repeat_label_>=0) {
    return parent_->ReadRepeatEnd();
  } else {
    if (child_) {
      child_->ReadFinish();
      delete child_;
      child_=NULL;
    }
    return ReadFinish();
  }
}

bool RecordItem::ReadLabel(const zint32 label) {
  if (!LabelExist(label)) return false;
  SetPos(GetChildPos(label));
  return true;
}

size_t RecordItem::Read(const zint32 label, void *data, size_t size, size_t count) {
  if (!ReadLabel(label)) return 0;
  return Read(data, size, count);
}

size_t RecordItem::Read(void *data, size_t size, size_t count) {
  return fread(data, size, count, fp_);
}


//////////////////////////////////////////////////////////////////////////
// Write
RecordItem* RecordItem::WriteChild(const zint32 label) {
  ZCHECK_FALSE(LabelExist(label)) 
    << "Label "<<label<<" already existed!\n";
  zint64 pos=GetPos();
  if (child_) delete child_;
  child_=new RecordItem(fp_, pos, this);
  table_.push_back(make_pair(label, pos));
  return child_;
}

RecordItem* RecordItem::WriteFinish() {
  // Save current position.
  table_begin_pos_ = GetPos();
  // Write table.
  IOObject<vector<TableItem> >::WriteFileB(fp_, table_);
  zint64 end_pos = GetPos();
  // Jump back.
  SetPos(itebegin_pos_);
  // Write table begin position.
  IOObject<zint64>::WriteFileB(fp_, table_begin_pos_);
  // Jump to end
  SetPos(end_pos);
  return parent_;
}

RecordItem* RecordItem::WriteRepeatBegin(const zint32 label) {
  WriteChild(label);
  child_->repeat_label_=0;
  return child_;
}

RecordItem* RecordItem::WriteRepeatChild() {
  if (parent_->repeat_label_>=0) {
    // It's parent is repeat head.
    return parent_->WriteRepeatChild();
  } else {
    // Itself is repeat head
    if (child_) {
      child_->WriteFinish();
    }
    repeat_label_++;
    return WriteChild(repeat_label_);
  }
}

RecordItem* RecordItem::WriteRepeatEnd() {
  if (parent_->repeat_label_>=0) {
    return parent_->WriteRepeatEnd();
  } else {
    if (child_) {
      child_->WriteFinish();
      delete child_;
      child_=NULL;
    }
    return WriteFinish();
  }
}

bool RecordItem::WriteLabel(const zint32 label) {
  ZCHECK_FALSE(LabelExist(label)) 
    << "Label "<<label<<" already existed!\n";
  table_.push_back(make_pair(label, GetPos()));
  return true;
}

size_t RecordItem::Write(const zint32 label, const void *data, size_t size, size_t count) {
  WriteLabel(label);
  return Write(data, size, count);
}

size_t RecordItem::Write(const void *data, size_t size, size_t count) {
  return fwrite(data, size, count, fp_);
}

bool RecordItem::LabelExist(const zint32 label) {
  for (zuint i = 0; i < table_.size(); ++i) 
    if (table_[i].first==label) return true;
  return false;
}

zint64 RecordItem::GetChildPos(const zint32 label) {
  for (zuint i = 0; i < table_.size(); ++i)
    if (table_[i].first == label) return table_[i].second;
  return -1;
}

zint64 RecordItem::GetPos() {
  zint64 pos = ftell(fp_);
//  fpos_t pos;
//  ZCHECK(fgetpos(fp_, &pos) == 0);
  return pos;
}

void RecordItem::SetPos(zint64 pos) {
  fseek(fp_, pos, SEEK_SET);
//  fpos_t fpos = pos;
//  ZCHECK(fsetpos(fp_, &fpos) == 0);
}

size_t RecordItem::GetItemNumber() {
  return table_.size();
}

vector<pair<zint32, RecordItem::RFNodeType> > RecordItem::GetItemLabels() {
  vector<pair<zint32, RecordItem::RFNodeType> > labels;
  labels.reserve(table_.size());
  for (zuint i = 0; i < table_.size(); ++i) {
    labels.push_back(make_pair(table_[i].first, GetChildNodeType(table_[i].first)));
  }
  return labels;
}

zzz::zint64 RecordItem::GetChildItemSize(const zint32 label) {
  for (zuint i = 0; i < table_.size(); ++i) { 
    if (table_[i].first==label) {
      if (i == table_.size() - 1) return table_begin_pos_ - table_[i].second;
      return table_[i + 1].second - table_[i].second;
    }
  }
  return 0;
}

zzz::zint64 RecordItem::GetItemSize() {
  return table_begin_pos_ - itebegin_pos_ - sizeof(zint64);
}

RecordItem::RFNodeType RecordItem::GetChildNodeType(zint32 label) {
  ZCHECK(LabelExist(label));
  zint64 curpos = GetPos();
  // First hint, table_pos should be inside this node
  zint64 table_pos;
  Read(label, &table_pos, sizeof(zint64), 1);
  zint64 node_pos = GetChildPos(label);
  if (table_pos <= node_pos ||
    table_pos >= node_pos + GetChildItemSize(label)) {
      SetPos(curpos);
      return NON_NODE;
  }
  // Second hint, first node should be the begin
  RecordItem::TableItem table_item;
  zsize vector_size;
  SetPos(table_pos);
  IOObj::ReadFileB(fp_, vector_size);
  IOObj::ReadFileB(fp_, table_item);
  if (node_pos + sizeof(zint64) == table_item.second) {
    SetPos(curpos);
    return NODE;
  } else {
    SetPos(curpos);
    return NON_NODE;
  }
}

//////////////////////////////////////////////////////////////////////////
// RecordFile
const int RECORDFILE_MAGIC=1124;

RecordFile::RecordFile()
  : head_(NULL),
    fp_(NULL),
    cur_(NULL) {
}

void RecordFile::LoadFileBegin(const string &filename) {
  ZCHECK_NULL(fp_);
  ZCHECK_NULL(head_);

  ZCHECK_NOT_NULL(fp_ = fopen(filename.c_str(), "rb")) << "Cannot open " << filename << endl;
  zint32 magic;
  fread(&magic, sizeof(magic), 1, fp_);
  ZCHECK_EQ(magic, RECORDFILE_MAGIC);
  head_ = new RecordItem(fp_, sizeof(RECORDFILE_MAGIC), NULL);
  head_->ReadTable();
  cur_ = head_;
  write_ = false;
}

void RecordFile::LoadFileEnd() {
  ZCHECK_NOT_NULL(fp_);
  ZCHECK_NOT_NULL(head_);
  ZCHECK_FALSE(write_);
  head_->ReadFinish();
  delete head_;
  head_ = NULL;
  cur_ = NULL;
  ZCHECK_ZERO(fclose(fp_));
  fp_ = NULL;
}

void RecordFile::SaveFileBegin(const string &filename) {
  ZCHECK_NULL(fp_);
  ZCHECK_NULL(head_);

  ZCHECK_NOT_NULL(fp_ = fopen(filename.c_str(), "wb")) << ZVAR(filename);
  fwrite(&RECORDFILE_MAGIC, sizeof(RECORDFILE_MAGIC), 1, fp_);
  head_ = new RecordItem(fp_, sizeof(RECORDFILE_MAGIC), NULL);
  cur_ = head_;
  write_ = true;
}

void RecordFile::SaveFileEnd() {
  ZCHECK_NOT_NULL(fp_);
  ZCHECK_NOT_NULL(head_);
  ZCHECK(write_);
  head_->WriteFinish();
  delete head_;
  head_ = NULL;
  cur_ = NULL;
  ZCHECK_ZERO(fclose(fp_));
  fp_ = NULL;
}

void RecordFile::ReadChildBegin(const zint32 label) {
  ZCHECK_NOT_NULL(head_);
  ZCHECK_FALSE(write_);
  cur_ = cur_->ReadChild(label);
}

void RecordFile::ReadChildEnd() {
  ZCHECK_NOT_NULL(head_);
  ZCHECK_FALSE(write_);
  cur_ = cur_->ReadFinish();
}

void RecordFile::ReadRepeatBegin(const zint32 label) {
  ZCHECK_NOT_NULL(head_);
  ZCHECK_FALSE(write_);
  cur_ = cur_->ReadRepeatBegin(label);
}

bool RecordFile::ReadRepeatChild() {
  ZCHECK_NOT_NULL(head_);
  ZCHECK_FALSE(write_);
  RecordItem *next = cur_->ReadRepeatChild();
  if (next == NULL) return false;
  cur_ = next;
  return true;
}

void RecordFile::ReadRepeatEnd() {
  ZCHECK_NOT_NULL(head_);
  ZCHECK_FALSE(write_);
  cur_ = cur_->ReadRepeatEnd();
}

bool RecordFile::ReadLabel(const zint32 label) {
  ZCHECK_NOT_NULL(head_);
  ZCHECK_FALSE(write_);
  return cur_->ReadLabel(label);
}

size_t RecordFile::Read(void *data, size_t size, size_t count) {
  ZCHECK_NOT_NULL(head_);
  ZCHECK_FALSE(write_);
  return cur_->Read(data, size, count);
}

size_t RecordFile::Read(const zint32 label, void *data, size_t size, size_t count) {
  ZCHECK_NOT_NULL(head_);
  ZCHECK_FALSE(write_);
  return cur_->Read(label, data, size, count);
}

void RecordFile::WriteChildBegin(const zint32 label) {
  ZCHECK_NOT_NULL(head_);
  ZCHECK(write_);
  cur_ = cur_->WriteChild(label);
}

void RecordFile::WriteChildEnd() {
  ZCHECK_NOT_NULL(head_);
  ZCHECK(write_);
  cur_ = cur_->WriteFinish();
}

void RecordFile::WriteRepeatBegin(const zint32 label) {
  ZCHECK_NOT_NULL(head_);
  ZCHECK(write_);
  cur_ = cur_->WriteRepeatBegin(label);
}

void RecordFile::WriteRepeatChild() {
  ZCHECK_NOT_NULL(head_);
  ZCHECK(write_);
  cur_ = cur_->WriteRepeatChild();
}

void RecordFile::WriteRepeatEnd() {
  ZCHECK_NOT_NULL(head_);
  ZCHECK(write_);
  cur_ = cur_->WriteRepeatEnd();
}

bool RecordFile::WriteLabel(const zint32 label) {
  ZCHECK_NOT_NULL(head_);
  ZCHECK(write_);
  return cur_->WriteLabel(label);
}

size_t RecordFile::Write(const void *data, size_t size, size_t count) {
  ZCHECK_NOT_NULL(head_);
  ZCHECK(write_);
  return cur_->Write(data, size, count);
}
size_t RecordFile::Write(const zint32 label, const void *data, size_t size, size_t count) {
  ZCHECK_NOT_NULL(head_);
  ZCHECK(write_);
  return cur_->Write(label, data, size, count);
}

bool RecordFile::LabelExist(const zint32 label) {
  return cur_->LabelExist(label);
}

size_t RecordFile::GetItemNumber() {
  return cur_->GetItemNumber();
}

vector<pair<zint32, RecordItem::RFNodeType> > RecordFile::GetItemLabels() {
  return cur_->GetItemLabels();
}

size_t RecordFile::GetItemSize() {
  return cur_->GetItemSize();
}

size_t RecordFile::GetChildItemSize(zint32 label) {
  return cur_->GetChildItemSize(label);
}

};  // namespace zzz