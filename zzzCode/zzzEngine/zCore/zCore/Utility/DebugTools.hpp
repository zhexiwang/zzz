#pragma once

namespace zzz {
struct BreakCount {
  BreakCount(int dest) : count_(0), dest_(dest), enabled_(dest >= 0) {}
  bool UpdateAndCheck() {return enabled_ && count_++ >= dest_;}
  bool Check() {return enabled_ && count_ >= dest_;}
  int count_, dest_;
  bool enabled_;
};
#define BREAK(x) if (x.UpdateAndCheck()) break;
#define BREAKOUT(x) if (x.Check()) break;
};  // namespace zzz