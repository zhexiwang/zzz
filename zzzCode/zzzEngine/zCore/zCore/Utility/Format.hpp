#pragma once
#include <boost/format.hpp>
#include <string>
#include <iostream>
#include <sstream>
#include <memory>

namespace zzz {
class Format {
public:
  Format() {}
  Format(const std::string &str) : format_(new boost::format(str)) {}
  Format(const Format &other) : format_(other.format_) {}
  operator std::string() const {
    return format_->str();
  }

  template<typename T>
  const Format& operator%(const T &v) const {
    (*format_) % v;
    return *this;
  }
  friend std::ostream& operator<<(std::ostream& os, const Format &me) {
    os << (*(me.format_));
    return os;
  }
  operator std::string() {
    std::ostringstream oss;
    oss << (*format_);
    return oss.str();
  }
private:
  std::shared_ptr<boost::format> format_;
};

template<typename T>
Format operator%(const std::string &pat, const T &v) {
  Format a(pat);
  a % v;
  return a;
}

} // namespace zzz;