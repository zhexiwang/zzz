#pragma once
#include "../common.hpp"
//wrap BraceItem

namespace zzz{
class BraceItem;
class BraceNode
{
public:
  BraceNode(const BraceNode &node);
  BraceNode(BraceItem *item,BraceItem *parent,int i);

  bool IsValid() const;

  BraceNode GetParent();

  zuint NodeNumber() const;
  BraceNode GetNode(zuint i);
  BraceNode AppendNode(const string &str, const char h=0, const char t=0) const;
  bool HasNode(const string &str);
  bool RemoveNode(zuint i);

  //iteration
  BraceNode GetFirstNode(const string &str=string());
  BraceNode GetFirstNodeInclude(const string &str);
  BraceNode GetNextSibling(const string &str=string());
  BraceNode GetNextSiblingInclude(const string &str);
  void GotoNextSibling(const string &str=string());
  void GotoNextSiblingInclude(const string &str);
  void operator++();

  //head and tail brace
  char GetHeadBrace();
  char GetTailBrace();
  void SetHeadTail(char h, char t);

  const string& GetText() const;
  void SetText(const string &str);
  void GetChildrenText(string &str) const;
private:
  BraceItem *item;
  BraceItem *parent;
  int idx;
};

inline const BraceNode& operator<<(const BraceNode &node, const string &text)
{
  node.AppendNode(text.c_str());
  return node;
}

inline const BraceNode& operator<<(const BraceNode &node, const char *text)
{
  node.AppendNode(text);
  return node;
}

}

