#pragma once

#include <zCore/common.hpp>

namespace zzz {
class Timer {
public:
  Timer():pauseTime_(0),paused_(false) { startTime_ = clock(); } 
  void Restart() { startTime_ = clock(); pauseTime_=0; } 
  void Pause() { if (!paused_) {paused_=true; pauseTime_=clock() - startTime_;} }
  void Resume() { if (paused_) {paused_=false; startTime_ = clock();} }
  double Elapsed() const { return  paused_ ? double(pauseTime_) / CLOCKS_PER_SEC : double(clock() - startTime_ + pauseTime_) / CLOCKS_PER_SEC; }
  double Elapsed_max() const { return (double((std::numeric_limits<clock_t>::max)()) - double(startTime_)) / double(CLOCKS_PER_SEC); }
  double Elapsed_min() const { return double(1)/double(CLOCKS_PER_SEC); }
  //busy waiting
  static void Sleep(zuint msec){clock_t goal=msec*CLOCKS_PER_SEC/1000+clock(); while(goal > clock()) ; }
private:
  clock_t startTime_;
  clock_t pauseTime_;
  bool paused_;
};
inline long GetTimeMs() {return clock() * (1000 / CLOCKS_PER_SEC) ;}
}
