#include "IAmTheOnlyOne.hpp"
#include <zCore/EnvDetect.hpp>
#include <zCore//Utility/Log.hpp>
#ifdef ZZZ_OS_WIN
#include <windows.h>
#endif

namespace zzz {

void I_AM_THE_ONLY_ONE(const char* name) {
  ::HANDLE __hMutexOneInstance(::CreateMutexA( NULL, TRUE, name));
  if (__hMutexOneInstance == NULL || ::GetLastError() == ERROR_ALREADY_EXISTS) {
    if(__hMutexOneInstance) {
      ::ReleaseMutex(__hMutexOneInstance);
      ::CloseHandle(__hMutexOneInstance);
    }
    ZLOGS << "One instance of me is running. My name is " << name << std::endl;
    exit(-1);
  }
}

}  // namespace zzz