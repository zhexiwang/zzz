#pragma once
#include "../common.hpp"
namespace zzz{
template <typename T>
struct ObjectCounter {
    static size_t count_;
    ObjectCounter() {++count_;}
    ObjectCounter (const ObjectCounter<T> &) {++count_;}
    ~ObjectCounter() {--count_;}
  static size_t NumOfLiving() {return count_;}
};

// initialize counter with zero
template <typename T>
size_t ObjectCounter<T>::count_ = 0;

}