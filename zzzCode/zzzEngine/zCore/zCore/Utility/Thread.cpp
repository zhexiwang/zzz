#define ZCORE_SOURCE
#include "ZCheck.hpp"
#include "Thread.hpp"

#ifdef ZZZ_LIB_PTHREAD

namespace zzz{
void *__start_thread(void *p) {
  Thread *pt = (Thread*)p;
  pt->running_ = true;
  pt->Main();
  pt->running_ = false;
//  pthread_exit(NULL);
  return NULL;
}

Thread::Thread() {
  running_ = false;
}

void Thread::Start() {
  pthread_attr_t attr; 
  pthread_attr_init(&attr); 
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  int rc = pthread_create(&thread_, &attr, __start_thread, (void *)this); 
  ZCHECK_EQ(rc, 0); // Not sure
  pthread_attr_destroy(&attr);
}

void Thread::SyncStart() {
  running_ = true;
  Main();
  running_ = false;

}

void Thread::Wait() {
  void *status;
  int rc = pthread_join(thread_, &status);
  ZCHECK_EQ(rc, 0); // Not sure.
}

bool Thread::IsRunning() {
  return running_;
}

Mutex::Mutex() {
  pthread_mutex_init(&mutex_, NULL);
}

Mutex::~Mutex() {
  pthread_mutex_destroy(&mutex_);
}

void Mutex::Lock() {
  pthread_mutex_lock(&mutex_); 
}

void Mutex::Unlock() {
  pthread_mutex_unlock(&mutex_); 
}

bool Mutex::TryLock() {
  if (pthread_mutex_trylock(&mutex_)==0) return true;
  else return false;
}

Condition::Condition(Mutex &mutex)
  : mutex_(&(mutex.mutex_)) {
  pthread_cond_init(&cond_, NULL);
}

Condition::~Condition() {
  pthread_cond_destroy(&cond_);
}

void Condition::Signal() {
  pthread_cond_signal(&cond_);
}

void Condition::Wait() {
  pthread_cond_wait(&cond_, mutex_);
}
} // namespace zzz
#endif // ZZZ_LIB_PTHREAD
