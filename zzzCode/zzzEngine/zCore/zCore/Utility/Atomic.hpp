#pragma once

namespace zzz {
int AtomicInc(long &value);
int AtomicDec(long &value);

// use virtual function so this call will work
template<typename T>
class RefCounter {
public:
  RefCounter()
    : ref_count_(1) {
  }
  virtual void AddRef() const {
    AtomicInc(ref_count_);
  }
  virtual void Release() const {
    if (AtomicDec(ref_count_) == 0)
      delete reinterpret_cast<T*>(const_cast<RefCounter*>(this));
  }
  virtual int GetRefCount() {
    return ref_count_;
  }
protected:
  mutable long ref_count_;
};

};  // namespace zzz