# ----- setup  variables ------

# Check whether the libraries is enabled
contains(GLEWENABLE, 1) {
    message(Glew is enable)

    INCLUDEPATH += $${top_srcdir}/3rdParty/Glew
    LIBS += -lGlew$${SUFFIX}
    !contains(DEFINES, ZETA_GLEW_ENABLE) {
        DEFINES += ZETA_GLEW_ENABLE ZETA_OPENGL_ENABLE
    }
}
