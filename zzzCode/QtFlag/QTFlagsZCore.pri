# ----- setup  variables ------

# Check whether the libraries is enabled
contains(ZCOREENABLE, 1) {
    message(ZCORE is enable)

    INCLUDEPATH += $${ZZZ_ENGINE_DIR}/zCore
    LIBS += -L$${ZZZ_LIB}
    LIBS += -lzCore$${SUFFIX}
    !contains(DEFINES, ZETA_ZCORE_ENABLE) {
        DEFINES += ZETA_ZCORE_ENABLE
    }
}
