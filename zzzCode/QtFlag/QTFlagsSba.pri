# ----- setup  variables ------


# Check whether the libraries is enabled
!isEmpty(SBAENABLE) {
    message(sba is enable)
    include(libraryDetector.pri)

    KEYHEADERS = sba/sba.h
    KEYLIBNAME = sba[DEBUGSUFFIX]

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(sba, SBAINCLUDEPATH, $${SBAINCLUDEPATH}, , $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(sba, SBALIBPATH, $${SBALIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    !contains(DEFINES, ZETA_SBA_ENABLE) {
        DEFINES += ZETA_SBA_ENABLE
    }

    VECLIBENABLE = 1
    !include(QTFlagsVecLib.pri) {
        message(To link sba, veclib must be linked)
    }

}
