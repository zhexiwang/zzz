# ----- setup  variables ------

# Check whether the libraries is enabled
contains(ANNMTENABLE, 1) {
    message(ANNmt is enable)
    include(libraryDetector.pri)

    KEYHEADERS = ANNmt/ANN.h
    KEYLIBNAME = ANNmt[DEBUGSUFFIX]

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(ANNmt, ANNMTINCLUDEPATH, $${ANNMTINCLUDEPATH}, , $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(ANNmt, ANNMTLIBPATH, $${ANNMTLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    !contains(DEFINES, ZETA_ANNMT_ENABLE) {
        DEFINES += ZETA_ANNMT_ENABLE
    }
}
