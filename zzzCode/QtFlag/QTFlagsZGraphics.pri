# ----- setup  variables ------

# Check whether the libraries is enabled
contains(ZGRAPHICSENABLE, 1) {
    message(ZGRAPHICS is enable)

    INCLUDEPATH += $${ZZZ_ENGINE_DIR}/zGraphics
    LIBS += -L$${ZZZ_LIB}
    LIBS += -lzGraphics$${SUFFIX}
    !contains(DEFINES, ZETA_ZGRAPHICS_ENABLE) {
        DEFINES += ZETA_ZGRAPHICS_ENABLE
    }
}
