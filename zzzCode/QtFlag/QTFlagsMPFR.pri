# ----- setup  variables ------

# Check whether the libraries is enabled
contains(MPFRENABLE, 1) {
    message(MPFR is enable)
    include(libraryDetector.pri)

    KEYHEADERS = mpfr.h
    KEYLIBNAME = mpfr[DEBUGSUFFIX]

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(MPFR, MPFRINCLUDEPATH, $${MPFRINCLUDEPATH}, mpfr, $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(MPFR, MPFRLIBPATH, $${MPFRLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    !contains(DEFINES, ZETA_MPFR_ENABLE) {
        DEFINES += ZETA_MPFR_ENABLE
    }
}
