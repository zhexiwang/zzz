# ----- setup  variables ------


# Check whether the libraries is enabled
!isEmpty(LEVMARENABLE) {
    message(levmar is enable)
    include(libraryDetector.pri)

    KEYHEADERS = levmar/levmar.h
    KEYLIBNAME = levmar[DEBUGSUFFIX]

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(levmar, LEVMARINCLUDEPATH, $${LEVMARINCLUDEPATH}, , $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(levmar, LEVMARLIBPATH, $${LEVMARLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    !contains(DEFINES, ZETA_LEVMAR_ENABLE) {
        DEFINES += ZETA_LEVMAR_ENABLE
    }

    VECLIBENABLE = 1
    !include(QTFlagsVecLib.pri) {
        message(To link sba, veclib must be linked)
    }
}
