# ----- setup  variables ------

# Check whether the libraries is enabled
contains(ZVISIONENABLE, 1) {
    message(ZVISION is enable)

    INCLUDEPATH += $${ZZZ_ENGINE_DIR}/zVision
    LIBS += -L$${ZZZ_LIB}
    LIBS += -lzVision$${SUFFIX}
    !contains(DEFINES, ZETA_ZVISION_ENABLE) {
        DEFINES += ZETA_ZVISION_ENABLE
    }
}
