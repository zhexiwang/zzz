# ----- setup  variables ------

# Check whether the libraries is enabled
contains(GLOGENABLE, 1) {
    message(glog is enable)
    include(libraryDetector.pri)

    KEYHEADERS = glog/logging.h
    KEYLIBNAME =
    win32-g++ {
        KEYLIBNAME += miniglog[DEBUGSUFFIX]
    }
    !win32-g++ {
        KEYLIBNAME += glog[DEBUGSUFFIX]
    }

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(glog, GLOGINCLUDEPATH, $${GLOGINCLUDEPATH}, , $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(glog, GLOGLIBPATH, $${GLOGLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    !contains(DEFINES, ZETA_GLOG_ENABLE) {
        DEFINES += ZETA_GLOG_ENABLE
    }
}
