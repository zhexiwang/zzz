# ----- setup  variables ------


# Check whether the libraries is enabled
!isEmpty(CERESENABLE) {
    message(ceres is enable)
    include(libraryDetector.pri)

    KEYHEADERS = ceres/ceres.h
    KEYLIBNAME = ceres[DEBUGSUFFIX]

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(Ceres, CERESINCLUDEPATH, $${CERESINCLUDEPATH}, , $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(Ceres, CERESLIBPATH, $${CERESLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    !contains(DEFINES, ZETA_CERES_ENABLE) {
        DEFINES += ZETA_CERES_ENABLE
    }

    # ----- additional dependence ----
    GLOGENABLE = 1
    !include(QTFlagsGlog.pri) {
        error("Cannot include QTFlagsGlog.pri")
    }

    CXSPARSEENABLE = 1
    !include(QTFlagsCXSparse.pri) {
        error("Cannot include QTFlagsCXSparse.pri")
    }

    VECLIBENABLE = 1
    !include(QTFlagsVecLib.pri) {
        error("Cannot include QTFlagsVecLib.pri")
    }

    EIGENENABLE = 1
    !include(QTFlagsEigen.pri) {
        error("Cannot include QTFlagsEigen.pri")
    }

}
