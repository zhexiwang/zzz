# ----- setup  variables ------

# Check whether the libraries is enabled
contains(UNITTESTENABLE, 1) {
    message(UnitTest++ is enable)

    INCLUDEPATH += $${top_srcdir}/3rdParty/UnitTest++/src
    LIBS += -lUnitTest$${SUFFIX}
    !contains(DEFINES, ZETA_UNITTEST_ENABLE) {
        DEFINES += ZETA_UNITTEST_ENABLE
    }
}
