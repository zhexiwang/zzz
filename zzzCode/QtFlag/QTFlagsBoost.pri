# ----- setup  variables ------

BOOSTVER *=
win32 {
    BOOSTVER = #-1_51
}
macx {
    BOOSTVER =
}
linux-g++* {
    BOOSTVER =
}

BOOSTCOMPILER *=
win32-g++ {
    BOOSTCOMPILER = #-mgw44
}
win32-msvc {
    BOOSTCOMPILER = #to be added.
}

# Check whether the libraries is enabled
!isEmpty(BOOSTENABLE) {
    message(Boost is enable)

    include(libraryDetector.pri)

    KEYHEADERS = boost/array.hpp
    KEYLIBNAME =
    macx {
        KEYLIBNAME = boost_thread-mt boost_system-mt boost_filesystem-mt \
                boost_regex-mt
    }

    linux-g++* {
        KEYLIBNAME = boost_thread-mt boost_system-mt boost_filesystem-mt \
                boost_regex-mt
    }

    win32-g++ {
        KEYLIBNAME = boost_thread$${BOOSTCOMPILER}-mt$${BOOSTVER}.dll \
                     boost_system$${BOOSTCOMPILER}-mt$${BOOSTVER}.dll \
                     boost_chrono$${BOOSTCOMPILER}-mt$${BOOSTVER}.dll \
                     boost_filesystem$$(BOOSTCOMPILER)-mt$${BOOSTVER}.dll \
                     #boost_regex-mgw44-mt$${BOOSTVER}.dll
    }

    message($$getLibraryPaths($${KEYLIBNAME}))

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(Boost, BOOSTINCLUDEPATH, $${BOOSTINCLUDEPATH}, , $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(Boost, BOOSTLIBPATH, $${BOOSTLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------

    !contains(DEFINES, ZETA_BOOST_ENABLE) {
        DEFINES += ZETA_BOOST_ENABLE
    }

    SOCKETENABLE = 1
    !include(QTFlagsSocket.pri) {
        message(boost asio system requires system socket libraries)
    }
}
