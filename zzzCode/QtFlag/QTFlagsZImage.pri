# ----- setup  variables ------

# Check whether the libraries is enabled
contains(ZIMAGEENABLE, 1) {
    message(ZIMAGE is enable)

    INCLUDEPATH += $${ZZZ_ENGINE_DIR}/zImage
    LIBS += -L$${ZZZ_LIB}
    LIBS += -lzImage$${SUFFIX}
    !contains(DEFINES, ZETA_ZIMAGE_ENABLE) {
        DEFINES += ZETA_ZIMAGE_ENABLE
    }
}
