# ----- setup  variables ------


# Check whether the libraries is enabled
!isEmpty(CXSPARSEENABLE) {
    message(cxsparse is enable)
    include(libraryDetector.pri)

    KEYHEADERS = SuiteSparse_config.h
    KEYLIBNAME =
    macx {
        KEYLIBNAME += cholmod colamd amd ccolamd camd cxsparse suitesparseconfig
    }

    linux-g++* {
        KEYLIBNAME += cxsparse cholmod colamd amd ccolamd
    }

    win32-g++ {
        KEYLIBNAME += cholmod colamd amd ccolamd camd cxsparse suitesparseconfig
    }

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(cxsparse, CXSPARSEINCLUDEPATH, $${CXSPARSEINCLUDEPATH}, suitesparse, $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(cxsparse, CXSPARSELIBPATH, $${CXSPARSELIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    !contains(DEFINES, ZETA_CXSPARSE_ENABLE) {
        DEFINES += ZETA_CXSPARSE_ENABLE
    }

}
