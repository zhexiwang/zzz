# ----- setup  variables ------

# Check whether the libraries is enabled
contains(GMPENABLE, 1) {
    message(GMP is enable)
    include(libraryDetector.pri)

    KEYHEADERS = gmp.h
    KEYLIBNAME = gmp[DEBUGSUFFIX]

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(GMP, GMPINCLUDEPATH, $${GMPINCLUDEPATH}, gmp, $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(GMP, GMPLIBPATH, $${GMPLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))


    # ----- post-build section -------
    !contains(DEFINES, ZETA_GMP_ENABLE) {
        DEFINES += ZETA_GMP_ENABLE
    }
}
