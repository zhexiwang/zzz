# ----- setup  variables ------

# Check whether the libraries is enabled
contains(Z3RDENABLE, 1) {
    message(Z3RD is enable)

    INCLUDEPATH += $${ZZZ_ENGINE_DIR}/z3rd
    LIBS += -L$${ZZZ_LIB}
    LIBS += -lz3rd$${SUFFIX}
    !contains(DEFINES, ZETA_Z3RD_ENABLE) {
        DEFINES += ZETA_Z3RD_ENABLE
    }
}
