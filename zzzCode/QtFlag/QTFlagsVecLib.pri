# ----- setup  variables ------


# Check whether the libraries is enabled
!isEmpty(VECLIBENABLE) {
    message(veclib is enable)

    !macx {
        include(libraryDetector.pri)

        KEYHEADERS = f2c.h
        KEYLIBNAME =
        linux-g++* {
            KEYLIBNAME += cminpack lapack blas f2c
        }
        win32 {
            KEYLIBNAME += cminpack[DEBUGSUFFIX] lapack[DEBUGSUFFIX] blas[DEBUGSUFFIX] f2c[DEBUGSUFFIX]
        }

        # ----- include header path ------
        INCLUDEPATH += $$findIncludes(veclib, VECLIBINCLUDEPATH, $${VECLIBINCLUDEPATH}, clapack, $${KEYHEADERS})

        # ----- library path ------
        LIBS += $$findLibraries(veclib, VECLIBLIBPATH, $${VECLIBLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))
    }

    macx {
        message(veclib is installed with XCode)
        QMAKE_LFLAGS += -framework vecLib
    }

    # ----- post-build section -------
    !contains(DEFINES, ZETA_VECLIB_ENABLE) {
        DEFINES += ZETA_VECLIB_ENABLE
    }
}
