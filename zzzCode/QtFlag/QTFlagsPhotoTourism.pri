# ----- setup  variables ------

# Check whether the libraries is enabled
contains(PHOTOTOURISMENABLE, 1) {
    message(PhotoTourism is enable)
    include(libraryDetector.pri)

    KEYHEADERS = PhotoTourismLib/matrix.h
    KEYLIBNAME = PhotoTourism[DEBUGSUFFIX]

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(PhotoTourism, PHOTOTOURISMINCLUDEPATH, $${PHOTOTOURISMINCLUDEPATH}, , $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(PhotoTourism, PHOTOTOURISMLIBPATH, $${PHOTOTOURISMLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    VECLIBENABLE = 1
    !include(QTFlagsVecLib.pri) {
        message(To link sba, veclib must be linked)
    }

    !contains(DEFINES, ZETA_PHOTOTOURISM_ENABLE) {
        DEFINES += ZETA_PHOTOTOURISM_ENABLE
    }
}
