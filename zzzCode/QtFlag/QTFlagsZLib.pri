# ----- setup  variables ------


# Check whether the libraries is enabled
!isEmpty(ZLIBENABLE) {
    message(zlib is enable)
    include(libraryDetector.pri)

    KEYHEADERS = zlib.h
    KEYLIBNAME =
    !win32 {
        KEYLIBNAME += z
    }

    win32 {
        KEYLIBNAME += zlib[DEBUGSUFFIX]
    }

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(zlib, ZLIBINCLUDEPATH, $${ZLIBINCLUDEPATH}, zlib, $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(zlib, ZLIBLIBPATH, $${ZLIBLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    !contains(DEFINES, ZETA_ZLIB_ENABLE) {
        DEFINES += ZETA_ZLIB_ENABLE
    }

    # ----- post-build section -------
}
