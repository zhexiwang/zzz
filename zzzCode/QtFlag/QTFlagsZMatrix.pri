# ----- setup  variables ------

# Check whether the libraries is enabled
contains(ZMATRIXENABLE, 1) {
    message(ZMATRIX is enable)

    INCLUDEPATH += $${top_srcdir}/zzzEngine/zMatrix
    LIBS += -L$${top_srcdir}/lib
    LIBS += -lzMatrix$${SUFFIX}
    !contains(DEFINES, ZETA_ZMATRIX_ENABLE) {
        DEFINES += ZETA_ZMATRIX_ENABLE
    }
}
