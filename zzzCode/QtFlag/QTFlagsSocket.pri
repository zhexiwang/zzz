# ----- setup  variables ------


# Check whether the libraries is enabled
!isEmpty(SOCKETENABLE) {
    message(socket is enable)
    # basically these libraries should at the system path
    win32 {
        LIBS += -lws2_32 -lwsock32 # for boost asio lib
    }
    !contains(DEFINES, ZETA_SOCKET_ENABLE) {
        DEFINES += ZETA_SOCKET_ENABLE
    }
}
