# ----- setup  variables ------



# Check whether the libraries is enabled
!isEmpty(EIGENENABLE) {
    message(Eigen is enable)
    include(libraryDetector.pri)

    KEYHEADERS = Eigen/Core
    KEYLIBNAME = 

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(Eigen, EIGENINCLUDEPATH, $${EIGENINCLUDEPATH}, eigen3, $${KEYHEADERS})

    # no need to check lib because eigen3 is a pure template-based header library.

    # ----- post-build section -------
    !contains(DEFINES, ZETA_EIGEN_ENABLE) {
        DEFINES += ZETA_EIGEN_ENABLE
    }

}
