# ----- setup  variables ------

win32 {
    OPENSCENEGRAPHVER = 3.0.1
}

macx {
    OPENSCENEGRAPHVER =
}

linux-g++ {
    OPENSCENEGRAPHVER = 3.0.1
    OPENSCENEGRAPHVERSUF = .80
    OPENTHREADVER = .12
}


!isEmpty(OSGENABLE) {
    message(openscenegraph is enable)
    include(libraryDetector.pri)

    KEYHEADERS = osg/node

    KEYLIBNAME = osg[DEBUGSUFFIX]$$DLLSUF \
                osgViewer[DEBUGSUFFIX]$$DLLSUF \
                osgDB[DEBUGSUFFIX]$$DLLSUF \
                osgGA[DEBUGSUFFIX]$$DLLSUF \
                osgQt[DEBUGSUFFIX]$$DLLSUF \
                osgManipulator[DEBUGSUFFIX]$$DLLSUF \
                osgUtil[DEBUGSUFFIX]$$DLLSUF \
                osgText[DEBUGSUFFIX]$$DLLSUF \
                OpenThreads[DEBUGSUFFIX]$$DLLSUF
    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(openscenegraph, OSGINCLUDEPATH, $${OSGINCLUDEPATH}, , $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(openscenegraph, OSGLIBPATH, $${OSGLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    !contains(DEFINES, ZETA_OSG_ENABLE) {
        DEFINES += ZETA_OSG_ENABLE
    }

#    # ----- post-build section -------
#    win32 {
#        dllFolder = $$PWD/../SharedLibraries/bin/w$$ARCHFOLDER
#        dllFolder = $$replace(dllFolder, /, \\)
#        libFolder = $$PWD/../SharedLibraries/lib/w$$ARCHFOLDER
#        libFolder = $$replace(libFolder, /, \\)

#        outFolder = $$OUT_PWD
#        dllSuffix = .dll
#        CONFIG(Release){
#        }
#        else {
#            QTdllSuffix = d4.dll
#            dllSuffix = d.dll
#        }
#        USER_QTPLUGIN = $$[QT_INSTALL_PLUGINS]
#        USER_QTPLUGIN = $$replace(USER_QTPLUGIN, /, \\)
#        outFolder = $$replace(outFolder, /, \\)
#        message($$USER_QTPLUGIN)

#        #gzip for png loading of osg
#        GZIPSUFFIX = 1
#        system(xcopy /D /y $$dllFolder\\libzlib$$ARCHSUFFIX$$DEBUGSUFFIX$$GZIPSUFFIX$$DLLSUF $$outFolder)

#        #OpenSceneGraph
#        system(xcopy /D /y $$dllFolder\\libOpenThreads$$ARCHSUFFIX$$DEBUGSUFFIX$$DLLSUF $$outFolder)
#        system(xcopy /D /y $$dllFolder\\libOsg$$ARCHSUFFIX$$DEBUGSUFFIX$$DLLSUF $$outFolder)
#        system(xcopy /D /y $$dllFolder\\libOsgDB$$ARCHSUFFIX$$DEBUGSUFFIX$$DLLSUF $$outFolder)
#        system(xcopy /D /y $$dllFolder\\libOsgGA$$ARCHSUFFIX$$DEBUGSUFFIX$$DLLSUF $$outFolder)
#        system(xcopy /D /y $$dllFolder\\libOsgManipulator$$ARCHSUFFIX$$DEBUGSUFFIX$$DLLSUF $$outFolder)
#        system(xcopy /D /y $$dllFolder\\libOsgText$$ARCHSUFFIX$$DEBUGSUFFIX$$DLLSUF $$outFolder)
#        system(xcopy /D /y $$dllFolder\\libOsgUtil$$ARCHSUFFIX$$DEBUGSUFFIX$$DLLSUF $$outFolder)
#        system(xcopy /D /y $$dllFolder\\libOsgViewer$$ARCHSUFFIX$$DEBUGSUFFIX$$DLLSUF $$outFolder)
#        !exists($$outFolder/osgPlugins-$$OPENSCENEGRAPHVER) : system(md $$outFolder\\osgPlugins-$$OPENSCENEGRAPHVER)
#        system(xcopy /D /y $$dllFolder\\osgPlugins-$$OPENSCENEGRAPHVER\\mingw_osgdb_freetype$$DEBUGSUFFIX$$DLLSUF $$outFolder\\osgPlugins-$$OPENSCENEGRAPHVER)
#        system(xcopy /D /y $$dllFolder\\osgPlugins-$$OPENSCENEGRAPHVER\\mingw_osgdb_obj$$DEBUGSUFFIX$$DLLSUF $$outFolder\\osgPlugins-$$OPENSCENEGRAPHVER)
#        system(xcopy /D /y $$dllFolder\\osgPlugins-$$OPENSCENEGRAPHVER\\mingw_osgdb_bmp$$DEBUGSUFFIX$$DLLSUF $$outFolder\\osgPlugins-$$OPENSCENEGRAPHVER)
#        system(xcopy /D /y $$dllFolder\\osgPlugins-$$OPENSCENEGRAPHVER\\mingw_osgdb_jpeg$$DEBUGSUFFIX$$DLLSUF $$outFolder\\osgPlugins-$$OPENSCENEGRAPHVER)
#        system(xcopy /D /y $$dllFolder\\osgPlugins-$$OPENSCENEGRAPHVER\\mingw_osgdb_png$$DEBUGSUFFIX$$DLLSUF $$outFolder\\osgPlugins-$$OPENSCENEGRAPHVER)
#        system(xcopy /D /y $$dllFolder\\osgPlugins-$$OPENSCENEGRAPHVER\\mingw_osgdb_pnm$$DEBUGSUFFIX$$DLLSUF $$outFolder\\osgPlugins-$$OPENSCENEGRAPHVER)


#        # Font
#        system(xcopy /D /y $$dllFolder\\freetype\\libfreetype-6.dll $$outFolder)
#        system(move /Y .\\$$outFolder\\libfreetype-6.dll $$outFolder\\freetype6.dll )
#    }

#    linux-g++ {
#        dllFolder = $$PWD/../SharedLibraries/lib/linux$$ARCHFOLDER
#        libFolder = $$PWD/../SharedLibraries/lib/w$$ARCHFOLDER

#        outFolder = $$OUT_PWD
#        dllSuffix = .so
#        CONFIG(release, debug|release){
#        }
#        else {
#            QTdllSuffix = d4.so
#            dllSuffix = d.so
#        }
#        USER_QTPLUGIN = $$[QT_INSTALL_PLUGINS]
#        message($$USER_QTPLUGIN)
#        message($$outFolder)

#        #OpenSceneGraph
#        #system(cp -f $$dllFolder/libOpenThreads$$ARCHSUFFIX$$DEBUGSUFFIX$$dllSuffix$$OPENTHREADVER $$outFolder)
#        #system(cp -fL $$dllFolder/libosg$$ARCHSUFFIX$$DEBUGSUFFIX$$dllSuffix$$OPENSCENEGRAPHVERSUF $$outFolder)
#        #system(cp -fL $$dllFolder/libosgDB$$ARCHSUFFIX$$DEBUGSUFFIX$$dllSuffix$$OPENSCENEGRAPHVERSUF $$outFolder)
#        #system(cp -fL $$dllFolder/libosgGA$$ARCHSUFFIX$$DEBUGSUFFIX$$dllSuffix$$OPENSCENEGRAPHVERSUF $$outFolder)
#        #system(cp -fL $$dllFolder/libosgManipulator$$ARCHSUFFIX$$DEBUGSUFFIX$$dllSuffix$$OPENSCENEGRAPHVERSUF $$outFolder)
#        #system(cp -fL $$dllFolder/libosgText$$ARCHSUFFIX$$DEBUGSUFFIX$$dllSuffix$$OPENSCENEGRAPHVERSUF $$outFolder)
#        #system(cp -fL $$dllFolder/libosgUtil$$ARCHSUFFIX$$DEBUGSUFFIX$$dllSuffix$$OPENSCENEGRAPHVERSUF $$outFolder)
#        #system(cp -fL $$dllFolder/libosgViewer$$ARCHSUFFIX$$DEBUGSUFFIX$$dllSuffix$$OPENSCENEGRAPHVERSUF $$outFolder)
#        !exists($$outFolder/osgPlugins-$$OPENSCENEGRAPHVER) : system(mkdir $$outFolder/osgPlugins-$$OPENSCENEGRAPHVER)
#        system(cp -f $$dllFolder/osgPlugins-$$OPENSCENEGRAPHVER/osgdb_freetype$$DEBUGSUFFIX$$dllSuffix $$outFolder/osgPlugins-$$OPENSCENEGRAPHVER)
#        system(cp -f $$dllFolder/osgPlugins-$$OPENSCENEGRAPHVER/osgdb_obj$$DEBUGSUFFIX$$dllSuffix $$outFolder/osgPlugins-$$OPENSCENEGRAPHVER)
#        system(cp -f $$dllFolder/osgPlugins-$$OPENSCENEGRAPHVER/osgdb_bmp$$DEBUGSUFFIX$$dllSuffix $$outFolder/osgPlugins-$$OPENSCENEGRAPHVER)
#        system(cp -f $$dllFolder/osgPlugins-$$OPENSCENEGRAPHVER/osgdb_jpeg$$DEBUGSUFFIX$$dllSuffix $$outFolder/osgPlugins-$$OPENSCENEGRAPHVER)
#        system(cp -f $$dllFolder/osgPlugins-$$OPENSCENEGRAPHVER/osgdb_png$$DEBUGSUFFIX$$dllSuffix $$outFolder/osgPlugins-$$OPENSCENEGRAPHVER)
#        system(cp -f $$dllFolder/osgPlugins-$$OPENSCENEGRAPHVER/osgdb_pnm$$DEBUGSUFFIX$$dllSuffix $$outFolder/osgPlugins-$$OPENSCENEGRAPHVER)
#    }
}
