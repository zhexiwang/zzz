# ----- setup  variables ------

# Check whether the libraries is enabled
contains(YAMLCPPENABLE, 1) {
    message(Yaml-Cpp is enable)

    INCLUDEPATH += $${ZZZ_DIR}/3rdParty/yaml-cpp/include
    LIBS += -lyaml-cpp$${SUFFIX}
    !contains(DEFINES, ZETA_YAMLCPP_ENABLE) {
        DEFINES += ZETA_YAML_CPP_ENABLE
    }
}
