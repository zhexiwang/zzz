# ----- setup  variables ------



# Check whether the libraries is enabled
!isEmpty(CGALENABLE) {
    message(CGAL is enable)
    include(libraryDetector.pri)

    KEYHEADERS = CGAL/Cartesian.h
    KEYLIBNAME = CGAL[DEBUGSUFFIX] CGAL_Core[DEBUGSUFFIX]

    # ----- include header path ------
    INCLUDEPATH += $$findIncludes(CGAL, CGALINCLUDEPATH, $${CGALINCLUDEPATH}, , $${KEYHEADERS})

    # ----- library path ------
    LIBS += $$findLibraries(CGAL, CGALLIBPATH, $${CGALLIBPATH}, $$getLibraryPaths($${KEYLIBNAME}, any))

    # ----- post-build section -------
    !contains(DEFINES, ZETA_CGAL_ENABLE) {
        DEFINES += ZETA_CGAL_ENABLE
    }

    MPFRENABLE = 1
    !include(QTFlagsMPFR.pri) {
        error(To use CGAL, please link with mpfr lib)
    }

    GMPENABLE = 1
    !include(QTFlagsGMP.pri) {
        error(To use CGAL, please link with gmp lib)
    }

    EIGENENABLE = 1
    !include(QTFlagsEigen.pri) {
        error("Cannot include QTFlagsEigen.pri")
    }
    VECLIBENABLE = 1
    !include(QTFlagsVecLib.pri) {
        message(To link sba, veclib must be linked)
    }
}
