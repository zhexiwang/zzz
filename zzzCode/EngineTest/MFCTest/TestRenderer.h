#pragma once
#include <zzz.hpp>
#include <zGraphics/Renderer/CameraRenderer.hpp>
#include <zGraphics/Resource/Mesh/OTriMesh.hpp>
#include <zGraphics/Graphics/HeightMap.hpp>
using namespace zzz;

class TestRenderer :
  public OneObjRenderer
{
public:
  TestRenderer(void);
  ~TestRenderer(void);

  bool InitData();
  
  HeightMap hm;
  OTriMesh mesh;
private:
};
