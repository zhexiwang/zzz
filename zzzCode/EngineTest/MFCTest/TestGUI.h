#pragma once

#include <zGraphics/GraphicsGUI/AntTweakBarGUI.hpp>
using namespace zzz;
class TestGUI : public AntTweakBarGUI
{
public:
  bool InitData();

private:
  TwBar *bar;

  static void TW_CALL ResetArcBall(void *clientData);
  static void TW_CALL SetMode2D(const void *value, void *clientData);
  static void TW_CALL GetMode2D(void *value, void *clientData);
};
