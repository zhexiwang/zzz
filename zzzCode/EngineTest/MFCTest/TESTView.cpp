// TESTView.cpp : implementation of the CTESTView class
//

#include "stdafx.h"
#include "TEST.h"

#include "TESTDoc.h"
#include "TESTView.h"
#include "stdio.h"
#include <zzz.hpp>

using namespace zzz;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTESTView

IMPLEMENT_DYNCREATE(CTESTView, CView)

BEGIN_MESSAGE_MAP(CTESTView, CView)
  // Standard printing commands
  ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
  ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
  ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
  ON_COMMAND(ID_RTFDATA_REPACK, &CTESTView::OnRtfdataRepack)
  ON_COMMAND(ID_RTFDATA_ANALYSIS, &CTESTView::OnRtfdataAnalysis)
  PUT_IN_VIEW_CPP_MESSAGE_MAP
  ON_COMMAND(ID_RENDERSCRIPT, &CTESTView::OnRenderscript)
  ON_COMMAND(ID_BINDARCBALL, &CTESTView::OnBindarcball)
END_MESSAGE_MAP()

// CTESTView construction/destruction

CTESTView::CTESTView()
{
}

CTESTView::~CTESTView()
{
}

BOOL CTESTView::PreCreateWindow(CREATESTRUCT& cs)
{
  // TODO: Modify the Window class or styles here by modifying
  //  the CREATESTRUCT cs
  CreateConsoleWindow("output window",400);
  return CView::PreCreateWindow(cs);
}

// CTESTView drawing

void CTESTView::OnDraw(CDC* /*pDC*/)
{
  CTESTDoc* pDoc = GetDocument();
  ASSERT_VALID(pDoc);
  if (!pDoc)
    return;

  // TODO: add draw code for native data here
}


// CTESTView printing

BOOL CTESTView::OnPreparePrinting(CPrintInfo* pInfo)
{
  // default preparation
  return DoPreparePrinting(pInfo);
}

void CTESTView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
  // TODO: add extra initialization before printing
}

void CTESTView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
  // TODO: add cleanup after printing
}


// CTESTView diagnostics

#ifdef _DEBUG
void CTESTView::AssertValid() const
{
  CView::AssertValid();
}

void CTESTView::Dump(CDumpContext& dc) const
{
  CView::Dump(dc);
}

CTESTDoc* CTESTView::GetDocument() const // non-debug version is inline
{
  ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTESTDoc)));
  return (CTESTDoc*)m_pDocument;
}
#endif //_DEBUG


// CTESTView message handlers

int samenumber(vector<int> &x,vector<int> &y)
{
  int ret=0;
  for (zzz::zuint i=0,j=0; i<x.size(); i++)
  {
    if (x[i]<y[j]) continue;
    while (j<y.size()-1 && x[i]>y[j]) j++;
    if (x[i]==y[j]) ret++;
  }
  return ret;
}


void CTESTView::OnRtfdataRepack()
{
  CFileDialog dlg(TRUE,NULL,"",0,"*.crtf|*.crtf");
  if (dlg.DoModal()==IDCANCEL)
    return;
  CString filename=dlg.GetPathName();
   float near1,far1,tmp;
   int spheren,facesize,sensorn;
   FILE *fp1=fopen(filename,"rb");
   FILE *fp2=fopen(filename+".repack","wb");
   float coeff[16];
   fread(&near1,sizeof(near1),1,fp1);
   fread(&far1,sizeof(far1),1,fp1);
   fread(&spheren,sizeof(spheren),1,fp1);
   fread(&facesize,sizeof(facesize),1,fp1);
   fread(&sensorn,sizeof(sensorn),1,fp1);
   for(int i=0; i<sensorn; i++)
   {
     fread(&tmp,sizeof(tmp),1,fp1);
     fread(&tmp,sizeof(tmp),1,fp1);
     fread(&tmp,sizeof(tmp),1,fp1);
   }
//   BYTE *data=new BYTE[2046*spheren*facesize*facesize*6*sensorn*3];
//   memset(data,0,sizeof(BYTE)*2046*spheren*facesize*facesize*6*sensorn*3);
  vector<int> x,y;
  int same[33],number[2046];
  double energe[2046],allenerge=0;
  memset(same,0,sizeof(int)*33);
  memset(number,0,sizeof(int)*2046);
  memset(energe,0,sizeof(double)*2046);
  for (int i=0; i<spheren*facesize*facesize*6*sensorn; i++)
  {
    for (int j=0; j<6; j++)
    {
      fread(coeff,sizeof(float),16,fp1);
      //fwrite(coeff,sizeof(float),16,fp2);
    }
    y.clear();
    for (int j=0; j<32; j++)
    {
      unsigned short tmpi;
      fread(&tmpi,sizeof(unsigned short),1,fp1);
//       data[(i*2046+tmpi)*3]=255;
//       data[(i*2046+tmpi)*3+1]=255;
//       data[(i*2046+tmpi)*3+2]=255;
      number[tmpi]++;
      if (i==0) x.push_back(tmpi);
      else y.push_back(tmpi);
      fread(coeff,sizeof(float),16,fp1);
      energe[tmpi]+=abs(coeff[0]);
      allenerge+=abs(coeff[0]);
      //fwrite(coeff,sizeof(float),16,fp2);
      fread(coeff,sizeof(float),16,fp1);
      energe[tmpi]+=abs(coeff[0]);
      allenerge+=abs(coeff[0]);
      //fwrite(coeff,sizeof(float),16,fp2);
      fread(coeff,sizeof(float),16,fp1);
      energe[tmpi]+=abs(coeff[0]);
      allenerge+=abs(coeff[0]);
      //fwrite(coeff,sizeof(float),16,fp2);
    }
    if (i!=0) 
    {
      int samen=samenumber(x,y);
      zout<<samen<<endl;
      same[samen]++;
    }
  }
  fclose(fp1);
  fclose(fp2);
  for (int i=32; i>=0; i--)
    zout<<i<<":"<<same[i]<<endl;
  for (int i=0; i<2046; i++)
    zout<<i<<":"<<number[i]<<'\t';
  sort(energe,energe+2046);
  double all=0;
  for (int i=2045; i>=0; i--)
  {
    all+=energe[i]/allenerge;
    zout<<i<<":"<<all<<'\t';
  }

//   ILuint image;
//   ilGenImages(1,&image);
//   ilBindImage(image);
//   ilTexImage(2046,spheren*facesize*facesize*6*sensorn,1,3,IL_RGB,IL_UNSIGNED_BYTE,data);
//   ilSaveImage(filename+".bmp");
//   ilDeleteImage(image);
}

void CTESTView::OnRtfdataAnalysis()
{
  CFileDialog dlg(TRUE,NULL,"",0,"*.txt|*.txt");
  if (dlg.DoModal()==IDCANCEL)
    return;
  CString filename=dlg.GetPathName();
  HaarCoeffCubemap4TripleFull<32> shhaar[16];
  ifstream fi(filename);
  double ori_energe=0;
  float tmp;
  for (int i=0; i<6; i++)
  {
    for (int sh=0;sh<16;sh++)
    {
      fi>>tmp;
      ori_energe+=tmp*tmp;
      shhaar[sh].Scale[i]=tmp;
    }
  }
  for (int i=0; i<shhaar[0].DATASIZE; i++)
  {
    for (int sh=0;sh<16;sh++)
    {
      fi>>tmp;
      tmp/= float(1 << Log2((i % shhaar[0].FACEDATASIZE) / 32));
      ori_energe+=tmp*tmp;
      shhaar[sh].Square[i].v[0]=tmp;
    }
    for (int sh=0;sh<16;sh++)
    {
      fi>>tmp;
      tmp/= float(1 << Log2((i % shhaar[0].FACEDATASIZE) / 32));
      ori_energe+=tmp*tmp;
      shhaar[sh].Square[i].v[1]=tmp;
    }
    for (int sh=0;sh<16;sh++)
    {
      fi>>tmp;
      tmp/= float(1 << Log2((i % shhaar[0].FACEDATASIZE) / 32));
      ori_energe+=tmp*tmp;
      shhaar[sh].Square[i].v[2]=tmp;
    }
  }
  fi.close();
  ori_energe=sqrt(ori_energe);
  printf("ori_energe:%f\n",ori_energe);
  int keep_number[]={1024,512,256,128,64,32,16,8};
  HaarCoeffCubemap4TripleSparse<32> haars;
  for (int i=0; i<8; i++)
  {
    double dif_energe=0;
    shhaar[0].KeepBigElement(keep_number[i],haars,true);
    for (int j=0,k=0; j<shhaar[0].DATASIZE; j++)
    {
      float xx,yy,zz;
      if (k<haars.Size-1)
        if (j==haars.Coeff[k].index) 
        {
          k++;
          continue;
        }
      if (j==haars.Coeff[k].index) continue;
      xx=shhaar[0].Square[j].v[0];
      xx/= float(1 << Log2((j % shhaar[0].FACEDATASIZE) / 32));
      yy=shhaar[0].Square[j].v[1];
      yy/= float(1 << Log2((j % shhaar[0].FACEDATASIZE) / 32));
      zz=shhaar[0].Square[j].v[2];
      zz/= float(1 << Log2((j % shhaar[0].FACEDATASIZE) / 32));
      dif_energe+=xx*xx+yy*yy+zz*zz;
    }
    dif_energe=sqrt(dif_energe);
    printf("dif_energe for %d biggest element is %f = %f\n",keep_number[i],dif_energe,dif_energe/ori_energe);
  }

}

PUT_IN_VIEW_CPP_GUI(CTESTView,m_renderer,m_gui);
void CTESTView::OnRenderscript()
{
//  m_renderer.LoadRenderScript("e:\\test.rzs");
}

void CTESTView::OnBindarcball()
{
}
