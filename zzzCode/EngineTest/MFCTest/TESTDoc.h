// TESTDoc.h : interface of the CTESTDoc class
//


#pragma once


class CTESTDoc : public CDocument
{
protected: // create from serialization only
  CTESTDoc();
  DECLARE_DYNCREATE(CTESTDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
  virtual BOOL OnNewDocument();
  virtual void Serialize(CArchive& ar);

// Implementation
public:
  virtual ~CTESTDoc();
#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
  DECLARE_MESSAGE_MAP()
};


