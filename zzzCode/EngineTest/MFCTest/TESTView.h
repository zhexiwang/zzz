// TESTView.h : interface of the CTESTView class
//


#pragma once
#include "TestRenderer.h"
#include "TestGUI.h"

class CTESTView : public CView
{
protected: // create from serialization only
  CTESTView();
  DECLARE_DYNCREATE(CTESTView)

// Attributes
public:
  CTESTDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
  virtual void OnDraw(CDC* pDC);  // overridden to draw this view
  virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
  virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
  virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
  virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
  virtual ~CTESTView();
#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
  DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnRtfdataRepack();
  afx_msg void OnRtfdataAnalysis();

public:
  TestRenderer m_renderer;
  TestGUI m_gui;
  PUT_IN_VIEW_H
  afx_msg void OnRenderscript();
  afx_msg void OnBindarcball();
};

#ifndef _DEBUG  // debug version in TESTView.cpp
inline CTESTDoc* CTESTView::GetDocument() const
   { return reinterpret_cast<CTESTDoc*>(m_pDocument); }
#endif

