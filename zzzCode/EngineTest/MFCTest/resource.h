//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TEST.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_TESTTYPE                    129
#define ID_RTFDATA_REPACK               32771
#define ID_RTFDATA_ANALYSIS             32772
#define ID_RENDERSCRIPT                 32773
#define ID_BINDARCBALL                  32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
