#include "StdAfx.h"
#include "TestGUI.h"
#include <zGraphics/Renderer/OneObjRenderer.hpp>


bool TestGUI::InitData() {
  bar = TwNewBar("bar");
  TwDefine(" GLOBAL help='Test GUI.' "); // Message added to the help bar.
  TwDefine(" bar size='300 400' position='0 100' valueswidth='200' color='96 216 224' "); // change default tweak bar size and color
  TwDefine(" bar label='Test Tweak Bar' ");

  TwAddVarRW(bar, "Z Distance", TW_TYPE_DOUBLE, &(renderer_->camera_.m_Position[2]), 
    " group='Camera' min=-10 max=-0.01 step=0.1 keyIncr=z keyDecr=Z help='The distance of camera' ");
  TwAddVarCB(bar, "ArcBall Mode", TW_TYPE_BOOLCPP, TestGUI::SetMode2D, GetMode2D, this,\
    "group='ArcBall' label='ArcBall Mode' key=space help='Toggle rotate mode.' true='2D' false='3D' ");
  TwAddButton(bar,"Reset ArcBall", TestGUI::ResetArcBall,this,"group='ArcBall'");
  OneObjRenderer *renderer=dynamic_cast<OneObjRenderer*>(renderer_);
  TwAddVarRW(bar,"Arc Ball",TW_TYPE_QUAT4F,renderer->m_objArcBall.rot.v.v,"group='ArcBall' readonly open");
  //label CANNOT be the same with bar's name

  return true;
}

void TW_CALL TestGUI::SetMode2D(const void *value, void *clientData) {
  TestGUI *gui=(TestGUI*)clientData;
  OneObjRenderer *renderer=(OneObjRenderer*)gui->renderer_;
  renderer->m_objArcBall.SetMode2D(*(bool*)value);

}

void TW_CALL TestGUI::GetMode2D(void *value, void *clientData) {
  TestGUI *gui=(TestGUI*)clientData;
  OneObjRenderer *renderer=(OneObjRenderer*)gui->renderer_;
  *(bool*)value=renderer->m_objArcBall.GetMode2D();
}

void TW_CALL TestGUI::ResetArcBall(void *clientData) {
  TestGUI *gui=(TestGUI*)clientData;
  OneObjRenderer *renderer=(OneObjRenderer*)gui->renderer_;
  renderer->m_objArcBall.Reset();
}