#include <zCore/zCore.hpp>
#include <z3rd/z3rdAutoLink.hpp>
#include <zGraphics/zGraphics.hpp>
#include <zCore/zCoreAutoLink.hpp>
#include <zImage/zImageAutoLink.hpp>
#include <zGraphics/zGraphicsAutoLink.hpp>


using namespace zzz;

class TestRenderer : public OneObjRenderer, public GraphicsHelper {
public:
  TestRenderer() {
  }

  bool InitData() { 
    OneObjRenderer::InitData(); 
    glClearColor(0.5, 0.5, 0.5, 0); 
    GLfloat light_param[] = {0.5, 0, -1, 0}; 
    GLfloat amb_color[]={0.2,0.2,0.2,1}; 
    GLfloat dif_color[]={0.8,0.8,0.8,1}; 
    glLightfv(GL_LIGHT0, GL_AMBIENT, amb_color); 
    glLightfv(GL_LIGHT0, GL_DIFFUSE, dif_color); 
    glLightfv(GL_LIGHT0, GL_POSITION,light_param); 
    glEnable(GL_LIGHTING); 
    glEnable(GL_LIGHT0); 
    glShadeModel(GL_SMOOTH); 
    camera_.fovy_=C_R2D*1.57;
    show_msg_=true;
    return true; 
  } 

  void SetupCamera() { 
    //gluLookAt(0.7,0,0.7,0,0,0,0,1,0); 
    camera_.ApplyGL();
    obj_arcball_.ApplyGL();
  } 

  void CreateMsg() {
    SStringPrintf(msg_,"To see a world in a grain of sand,\nand a heaven in a wild flower.\nHold infinity in the palm of your hand,\nand eternity in an hour.\nRes: %d x %d, SPF: %lf = FPS: %lf",width_, height_, SPF_, FPS_);
  }

  void DrawObj() { 
    AABB3f aabb(Vector3f(-0.5), Vector3f(0.5));
    DrawAABB(aabb, ColorDefine::white);
    Vector3d points[8] = {
      Vector3d(aabb.Min(0), aabb.Min(1), aabb.Min(2)),
      Vector3d(aabb.Min(0), aabb.Min(1), aabb.Max(2)),
      Vector3d(aabb.Min(0), aabb.Max(1), aabb.Min(2)),
      Vector3d(aabb.Min(0), aabb.Max(1), aabb.Max(2)),
      Vector3d(aabb.Max(0), aabb.Min(1), aabb.Min(2)),
      Vector3d(aabb.Max(0), aabb.Min(1), aabb.Max(2)),
      Vector3d(aabb.Max(0), aabb.Max(1), aabb.Min(2)),
      Vector3d(aabb.Max(0), aabb.Max(1), aabb.Max(2)),
    };
    OpenGLProjector proj;
    for (zuint i = 0; i < 8; ++i) {
      BMPFont::Instance().DrawAt(ToString(points[i]) + '\n' + ToString(proj.Project(points[i][0], points[i][1], points[i][2])), points[i]);
    }
  } 
};

#include "../QT4Test/ObjTestRenderer.h"
int main(int argc, char* argv[]) {
  ZSIMPLEINIT();
  ObjTestRenderer myrenderer;
  if (!ZCMDSEQ.empty())
    myrenderer.LoadFile(ZCMDSEQ.front());

//   myrenderer.gui_=new AntTweakBarGUI;
//   TwBar *bar = TwNewBar("bar");
//   TwDefine(" GLOBAL help='GLUTTest' "); // Message added to the help bar.
//   TwDefine(" bar size='300 600' position='0 100' valueswidth='100' color='96 216 224' "); // change default tweak bar size and color
//   TwDefine(" bar label='GLUT Tweak Bar' ");
//   int nonsence=0;
//   TwAddVarRW(bar, "cur_seg", TW_TYPE_INT32, &nonsence, "label='Current Segment'");

  zzz::GLUTContext context;
  context.Initialize(&myrenderer, zzz::GLUTContext::NOGLUI);
  context.StartMainLoop();
  return 0;
}

