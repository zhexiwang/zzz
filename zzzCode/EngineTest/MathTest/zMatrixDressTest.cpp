#include <zMatrix/zMat.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Math/Matrix3x3.hpp>
#include <UnitTest++.h>

using namespace zzz;

SUITE(zMatrixDressTest) {
  TEST(VectorDress) {
    Vector3f a(1,2,3),b(4,5,6);
    CHECK_EQUAL(1,Dress(a)(0,0));
    CHECK_EQUAL(2,Dress(a)(1,0));
    CHECK_EQUAL(3,Dress(a)(2,0));
    Dress(a)=Dress(b);
    CHECK_ARRAY_EQUAL(b.Data(),a.Data(),3);
    const Vector3f &c=b;
    Dress(a) = 
      Dress(c)(Colon(2,0,-1), 0);
    CHECK_EQUAL(6,a[0]);
    CHECK_EQUAL(5,a[1]);
    CHECK_EQUAL(4,a[2]);
  }
  TEST(MatrixDress) {
    Matrix3x3f a(1,2,3,4,5,6,7,8,9),b(10,11,12,13,14,15,16,17,18);
    CHECK_EQUAL(1,Dress(a)(0,0));
    CHECK_EQUAL(5,Dress(a)(1,1));
    CHECK_EQUAL(9,Dress(a)(2,2));
    Dress(a)=Dress(b);
    Dress(a)=Dress(b)(Colon(),Colon());
    CHECK_ARRAY_EQUAL(b.Data(),a.Data(),9);
    const Matrix3x3f &c=b;
    Dress(a)=Dress(c)(Colon(2,0,-1),Colon());
    CHECK_ARRAY_EQUAL(b.Data()+6,a.Data(),3);
    CHECK_ARRAY_EQUAL(b.Data()+3,a.Data()+3,3);
    CHECK_ARRAY_EQUAL(b.Data(),a.Data()+6,3);
  }

#ifdef ZZZ_LIB_LAPACK
  TEST(UsezMatrixToCalculate) {
    Matrix3x3d a(1,2,4,2,3,4,5,3,1);
    Matrix3x3d U,VT,b;
    Vector3d S;
    zMatrix<double> za,zu,zvt,zs;
    za=Dress(a);
    CHECK(SVD(za,zu,zs,zvt));
    Dress(U)=zu;
    Dress(VT)=zvt;
    Dress(S)=zs;
    Dress(b)=Dress(U)*Diag(Dress(S))*Dress(VT);
    CHECK_ARRAY_CLOSE(a.Data(),b.Data(),9,EPSILON);
  }
#endif

  TEST(ArrayDress) {
    Array<2,double> v;
    Dress(v)=Ones<double>(10,10);
    CHECK_EQUAL(10,v.Size(0));
    CHECK_EQUAL(10,v.Size(1));
    for (zuint i=0; i<v.size(); i++)
      CHECK_EQUAL(1,v[i]);
  }
  TEST(CastDress) {
    zMatrix<double> a(Ones<double>(10,10)),b(Colond(0.0,0.9+EPSILON,0.1));
    zMatrix<int> c(Cast<int>(b * 10.0));
    for (zuint i=0; i<10; i++) CHECK_EQUAL(i,c(i,0));
#if !defined(ZZZ_COMPILER_MSVC_2010) // not supported for vc2010, hope some compiler can support it
    a(Cast<int>(b*10.0),0) =Zeros<double>(10,1); // it is not supported in msnv 2010 yet
    for (zuint i=0; i<10; i++) CHECK_EQUAL(0,a(i,0));
#endif
  }
  TEST(StdVectorDress) {
    vector<int> x;
    Dress(x)=Colon(5,10);
    for (zuint i=0; i<x.size(); i++)
      CHECK_EQUAL(x[i], i+5);
  }
}