#include <UnitTest++.h>
#include <zGraphics/GraphicsAlgo/Delaunay.hpp>
#ifdef ZZZ_LIB_CGAL
#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/Vector3.hpp>
using namespace zzz;

SUITE(DelaunayTest) {
  bool CheckInVector(const Vector3i &t, const int i) {
    if (t[0]==i) return true;
    if (t[1]==i) return true;
    if (t[2]==i) return true;
    return false;
  }
  bool CheckVectorEqual(const Vector3i &t1, const Vector3i &t2) {
    if (CheckInVector(t1, t2[0]) &&
        CheckInVector(t1, t2[1]) &&
        CheckInVector(t1, t2[2])) return true;
    return false;
  }
  bool CheckTriangle(const vector<Vector3i> &tri, const Vector3i &t) {
    for (zuint i=0; i<tri.size(); i++) {
      if (CheckVectorEqual(tri[i],t)) return true;
    }
    return false;
  }

  TEST(DelaunayTest) {
    //6*  *      *
    //5  *  *   *
    //4*  *
    //3      *  *
    //2  *   *  *
    //1*   *   *
    //0 1 2 3 4 5 6
    vector<Vector2f> pos;
    pos.push_back(Vector2f(2, 4));
    pos.push_back(Vector2f(3.5, 3));
    pos.push_back(Vector2f(5, 3));
    pos.push_back(Vector2f(1.5, 2));
    pos.push_back(Vector2f(3.5, 2));
    pos.push_back(Vector2f(5, 2));
    pos.push_back(Vector2f(0.5, 1));
    pos.push_back(Vector2f(2.5, 1));
    pos.push_back(Vector2f(4.5, 1));
    pos.push_back(Vector2f(0.5, 6));
    pos.push_back(Vector2f(2, 6));
    pos.push_back(Vector2f(5.5, 6));
    pos.push_back(Vector2f(1.5, 5));
    pos.push_back(Vector2f(3, 5));
    pos.push_back(Vector2f(5, 5));
    pos.push_back(Vector2f(0.5, 4));
    vector<Vector3i> tset;
    Delaunay del;
    del.Triangulate(pos, tset);
    VerboseLevel::Restore();

    CHECK_EQUAL(20, tset.size());
    CHECK(CheckTriangle(tset, Vector3i(9,10,12)));
    CHECK(CheckTriangle(tset, Vector3i(10,11,13)));
    CHECK(CheckTriangle(tset, Vector3i(11,13,14)));
    CHECK(CheckTriangle(tset, Vector3i(10,12,13)));
    CHECK(CheckTriangle(tset, Vector3i(9,12,15)));
    CHECK(CheckTriangle(tset, Vector3i(12,15,0)));
    CHECK(CheckTriangle(tset, Vector3i(12,13,0)));
    CHECK(CheckTriangle(tset, Vector3i(0,1,13)));
    CHECK(CheckTriangle(tset, Vector3i(1,13,14)));
    CHECK(CheckTriangle(tset, Vector3i(1,2,14)));
    CHECK(CheckTriangle(tset, Vector3i(0,3,15)));
    CHECK(CheckTriangle(tset, Vector3i(0,1,3)));
    CHECK(CheckTriangle(tset, Vector3i(1,2,4)));
    CHECK(CheckTriangle(tset, Vector3i(1,3,4)));
    CHECK(CheckTriangle(tset, Vector3i(3,6,15)));
    CHECK(CheckTriangle(tset, Vector3i(3,6,7)));
    CHECK(CheckTriangle(tset, Vector3i(2,4,5)));
    CHECK(CheckTriangle(tset, Vector3i(3,4,7)));
    CHECK(CheckTriangle(tset, Vector3i(4,7,8)));
    CHECK(CheckTriangle(tset, Vector3i(4,5,8)));
  }
}
#endif // ZZZ_LIB_CGAL
