#include <UnitTest++.h>
#include <zVision/SfM/Triangulator.hpp>
#include <zVision/SfM/ProjectionMat.hpp>

using namespace zzz;
SUITE(VisionTest) {
  TEST(ProjectionTest) {
    ProjectionMatd P[2];
    Matrix<3, 4, double> tmp;
    tmp.Row(0) = Vector4d(4.4157958968792678e+002, 0., 2.1400658416748047e+002, 0.);
    tmp.Row(1) = Vector4d(0., 4.4157958968792678e+002, 1.2687560844421387e+002, 0.);
    tmp.Row(2) = Vector4d(0., 0., 1., 0.);
    P[0].SetP(tmp);
    tmp.Row(0) = Vector4d(4.4157958968792678e+002, 0., 2.1400658416748047e+002, -1.1410240972892557e+005);
    tmp.Row(1) = Vector4d(0., 4.4157958968792678e+002, 1.2687560844421387e+002, 0.);
    tmp.Row(2) = Vector4d(0., 0., 1., 0.);
    P[1].SetP(tmp);
//    P0.Decompose();
//    P1.Decompose();
//    CHECK_CLOSE(4.1105426910931794e+002, P0.K()(0, 0), EPSILON);
//    CHECK_CLOSE(4.1113600141262748e+002, P0.K()(1, 1), EPSILON);
//    CHECK_CLOSE(1.8984500390740473e+002, P0.K()(0, 2), EPSILON);
//    CHECK_CLOSE(1.2775393316877984e+002, P0.K()(1, 2), EPSILON);
//    CHECK_CLOSE(4.1474950001113979e+002, P1.K()(0, 0), EPSILON);
//    CHECK_CLOSE(4.1537595700776797e+002, P1.K()(1, 1), EPSILON);
//    CHECK_CLOSE(1.5788545970480692e+002, P1.K()(0, 2), EPSILON);
//    CHECK_CLOSE(1.2845269963668127e+002, P1.K()(1, 2), EPSILON);
    Vector3d p3d0(10, 20, 30), p3d1(40, 10, 20);
    // Project
    Vector2d p2d0[2] = {P[0] * p3d0, P[1] * p3d0};
    Vector2d p2d1[2] = {P[0] * p3d1, P[1] * p3d1};
    // Triangulate, should be the original point
    Vector3d tri0, tri1;
    Triangulator::LinearTriangulate(P, p2d0, tri0);
    CHECK_CLOSE(0.0, tri0.DistTo(p3d0), EPSILON);
    // Project triangulated point, should project to the same pixels
    Vector2d proj2d[2] = {P[0] * tri0, P[1] * tri0};
    CHECK_CLOSE(0.0, proj2d[0].DistTo(p2d0[0]), EPSILON);
    CHECK_CLOSE(0.0, proj2d[1].DistTo(p2d0[1]), EPSILON);

    // scale translation should scale distance between 3d points;
    ProjectionMatd scale_P[2];
    tmp = P[0].P();
    tmp(0, 3) *= 100;
    tmp(1, 3) *= 100;
    tmp(2, 3) *= 100;
    scale_P[0].SetP(tmp);
    tmp = P[1].P();
    tmp(0, 3) *= 100;
    tmp(1, 3) *= 100;
    tmp(2, 3) *= 100;
    scale_P[1].SetP(tmp);
    Triangulator::LinearTriangulate(scale_P, p2d0, tri0);
    Triangulator::LinearTriangulate(scale_P, p2d1, tri1);
    CHECK_CLOSE(p3d0.DistTo(p3d1) * 100, tri0.DistTo(tri1), EPSILON);

    // add error
    p2d0[0] += Vector2d(5, 5);
    p2d0[1] += Vector2d(-2, -2);
    Triangulator::LinearTriangulate(P, p2d0, tri0);
    CHECK_CLOSE(0.0, tri0.DistTo(p3d0), 0.35);
    proj2d[0] = P[0] * tri0;
    proj2d[1] = P[1] * tri0;
    CHECK_CLOSE(0.0, proj2d[0].DistTo(p2d0[0]), 4);
    CHECK_CLOSE(0.0, proj2d[1].DistTo(p2d0[1]), 4);
  }
}