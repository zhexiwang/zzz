#include <UnitTest++.h>
#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zVision/SfM/CoordNormalizer.hpp>
using namespace zzz;

SUITE(SfMTest)
{
  TEST(CoordNormalizerTest)
  {
    CoordNormalizer normalizer(Vector2ui(432,345));
    Vector2f ori(11,43);
    Vector2f nor(ori);
    normalizer.Normalize(nor);
    Vector2d nor2d(normalizer.GetT()*Vector3d(ori[0],ori[1],1.0));
    Vector2f nor2(nor2d);
    CHECK_EQUAL(nor,nor2);
    normalizer.Restore(nor);
    CHECK_CLOSE(ori[0],nor[0],0.00001);
    CHECK_CLOSE(ori[1],nor[1],0.00001);
  }
}