#include <UnitTest++.h>
#include <zNet/Net/NetworkSession.hpp>
#include <zNet/Net/NetworkClient.hpp>
#include <zNet/Net/NetworkServer.hpp>
#include <zCore/Utility/Thread.hpp>

using namespace zzz;

class UDPEchoSession : public UDPSession {
public:
  UDPEchoSession(boost::asio::io_service& io_service, short port, void* userdata)
    : UDPSession(io_service, port, 1024 * 10, userdata) {}
  void Start() { AsyncReceive(); }
private:
  void AfterAsyncReceive(const boost::system::error_code& error, size_t bytes_transferred) {
    if (!error) {
      buffer_[bytes_transferred] = '\0';
      CHECK_EQUAL("abcdefg", buffer_.c_str());
    }
    AsyncReceive();
  }
};

class TCPEchoSession : public TCPSession {
public:
  TCPEchoSession(boost::asio::io_service& io_service, void *userdata)
    : TCPSession(io_service, 1024 * 10, userdata) {}
  void Start() { AsyncReceive(); }
private:
  void AfterAsyncReceive(const boost::system::error_code& error, size_t bytes_transferred) {
    if (!error) {
      buffer_[bytes_transferred] = '\0';
      CHECK_EQUAL("abcdefg", buffer_.c_str());
    }
    AsyncReceive();
  }
};

SUITE(NetworkTest) {
  TEST(TCPSendAndReceive) {
    TCPServer<TCPEchoSession> server(2222);
    server.Run(1);
    TCPClient<TCPSession> client;
    char msg[] = "abcdefg";
    client.Init("localhost", 2222);
    client.SyncSend(msg, sizeof(msg));
    Timer::Sleep(5);
    server.Stop();
  }
  TEST(UDPSendAndReceive) {
    UDPServer<UDPEchoSession> server(2222);
    server.Run(1);
    UDPClient<UDPSession> client;
    char msg[] = "abcdefg";
    client.Init("localhost", 2222);
    client.SyncSend(msg, sizeof(msg));
    Timer::Sleep(5);
    server.Stop();
  }
};