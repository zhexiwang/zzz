#include <UnitTest++.h>
#include <zCore/zCoreAutoLink.hpp>
#include <z3rd/z3rdAutoLink.hpp>
#include <zCore/Utility/CmdParser.hpp>

int main(int argc, char** argv) {
  ZSIMPLEINIT();
  return UnitTest::RunAllTests();
}

