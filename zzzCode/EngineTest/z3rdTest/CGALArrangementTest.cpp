#include <UnitTest++.h>
#include <z3rd/Wrapper/CGALArrangement.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Math/Vector2.hpp>
#include <zCore/Utility/TextProgressBar.hpp>
#include <vector>

using namespace zzz;
#ifdef ZZZ_LIB_CGAL
SUITE(CGALTest) {
  TEST(PointLocationTest) {
    // Construct the arrangement.
    vector<Vector2f> points;
    points.push_back(Vector2f(0,0));
    points.push_back(Vector2f(3,5));
    points.push_back(Vector2f(9,0));
    points.push_back(Vector2f(0,1));
    points.push_back(Vector2f(3,3));
    points.push_back(Vector2f(0,7));
    points.push_back(Vector2f(0,6));
    points.push_back(Vector2f(9,1));
    points.push_back(Vector2f(-1,0));
    points.push_back(Vector2f(9,1));
    vector<Vector2i> segments;
    segments.push_back(Vector2i(0, 1));
    segments.push_back(Vector2i(1, 2));
    segments.push_back(Vector2i(3, 4));
    segments.push_back(Vector2i(4, 5));
    segments.push_back(Vector2i(6, 7));
    segments.push_back(Vector2i(8, 9));
    CGALArrangement arr;
    arr.Build(points, segments);

    vector<CGALArrangement::Result> results;
    vector<Vector2f> query_points;
    query_points.push_back(Vector2f(1, 4));
    query_points.push_back(Vector2f(4, 3));
    query_points.push_back(Vector2f(6, 3));
    query_points.push_back(Vector2f(3, 2));
    query_points.push_back(Vector2f(5, 2));
    query_points.push_back(Vector2f(1, 0));
    arr.LocatePoints(query_points, results);

    CHECK_EQUAL(CGALArrangement::TYPE_FACE_BOUNDED, results[1].first);
    CHECK_EQUAL(CGALArrangement::TYPE_FACE_BOUNDED, results[3].first);
    CHECK_EQUAL(CGALArrangement::TYPE_FACE_BOUNDED, results[4].first);
    CHECK_EQUAL(results[1].second, results[3].second);
    CHECK_EQUAL(results[1].second, results[4].second);
    CHECK_EQUAL(CGALArrangement::TYPE_FACE_UNBOUNDED, results[0].first);
    CHECK_EQUAL(CGALArrangement::TYPE_FACE_UNBOUNDED, results[2].first);
    CHECK_EQUAL(CGALArrangement::TYPE_FACE_UNBOUNDED, results[5].first);
  }
}

#endif // ZZZ_LIB_CGAL