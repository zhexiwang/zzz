#include <UnitTest++.h>
#include <z3rd/Wrapper/AlglibPolyfit.hpp>
#ifdef ZZZ_LIB_ALGLIB
#include <zCore/Utility/StringTools.hpp>
#include <zCore/Math/Vector4.hpp>
using namespace zzz;
TEST(AlglibPolyFitTest)
{
  vector<float> x,y;
  FromString("0 0.1000 0.2000 0.3000 0.4000 0.5000 0.6000 0.7000 0.8000 0.9000 1.0000 1.1000 1.2000 1.3000\
                    1.4000 1.5000 1.6000 1.7000 1.8000 1.9000 2.0000 2.1000 2.2000 2.3000 2.4000 2.5000", x);
  FromString("0 0.1125 0.2227 0.3286 0.4284 0.5205 0.6039 0.6778 0.7421 0.7969 0.8427 0.8802\
                    0.9103 0.9340 0.9523 0.9661 0.9763 0.9838 0.9891 0.9928 0.9953 0.9970 0.9981 0.9989 0.9993 0.9996", y);
  Vector<4,double> p = AlglibPolyFit<4,float>(x,y);
  CHECK_CLOSE(p[0], -0.019930, EPSILON5);
  CHECK_CLOSE(p[1], 1.384353, EPSILON5);
  CHECK_CLOSE(p[2], -0.623189, EPSILON5);
  CHECK_CLOSE(p[3], 0.092849, EPSILON5);

  Vector<9,double> p2 = AlglibPolyFit<9,float>(x,y);
  for (zuint i=0; i<x.size(); i++) {
    float y2 = AlglibPolyVal(p2,x[i]);
    CHECK_CLOSE(y[i], y2, 0.0000999999999);
  }
}

#endif // ZZZ_LIB_ALGLIB