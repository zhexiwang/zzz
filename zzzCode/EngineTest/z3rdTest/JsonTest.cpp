#include <UnitTest++.h>
#include <z3rd/Wrapper/Json.hpp>
#include <z3rd/Wrapper/JsonWriter.hpp>

using namespace zzz;

SUITE(Json) {
  TEST(Stringify) {
    Json json;
    json.SetObject();
    json.AddMember("abcde", 1, json.GetAllocator());
    CHECK_EQUAL("{\"abcde\":1}", JsonString(json));
  }
  TEST(Parse) {
    const string jsonstr = "{\"abcde\":1}";
    Json json;
    json.Parse<0>(jsonstr.c_str());
    CHECK_EQUAL(1, json["abcde"].GetInt());
  }
}
