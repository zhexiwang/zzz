#include <UnitTest++.h>
#include <z3rd/Wrapper/CGALDelaunay.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Math/Vector2.hpp>
#include <zCore/Utility/TextProgressBar.hpp>
#include <vector>

using namespace zzz;
#ifdef ZZZ_LIB_CGAL
SUITE(CGALTest) {
  bool CheckInVector(const Vector3i &t, const int i) {
    if (t[0]==i) return true;
    if (t[1]==i) return true;
    if (t[2]==i) return true;
    return false;
  }
  bool CheckVectorEqual(const Vector3i &t1, const Vector3i &t2) {
    if (CheckInVector(t1, t2[0]) &&
      CheckInVector(t1, t2[1]) &&
      CheckInVector(t1, t2[2])) return true;
    return false;
  }
  bool CheckTriangle(const vector<Vector3i> &tri, const Vector3i &t) {
    for (zuint i=0; i<tri.size(); i++) {
      if (CheckVectorEqual(tri[i],t)) return true;
    }
    return false;
  }

  TEST(DelaunayTest) {
    //6*  *      *
    //5  *  *   *
    //4*  *
    //3      *  *
    //2  *   *  *
    //1*   *   *
    //0 1 2 3 4 5 6
    vector<Vector2f> pos;
    pos.push_back(Vector2f(2.015, 4.023));
    pos.push_back(Vector2f(3.497, 3.014));
    pos.push_back(Vector2f(5.019, 3.035));
    pos.push_back(Vector2f(1.474, 1.986));
    pos.push_back(Vector2f(3.485, 2.027));
    pos.push_back(Vector2f(5.022, 2.013));
    pos.push_back(Vector2f(0.512, 1.023));
    pos.push_back(Vector2f(2.521, 1.018));
    pos.push_back(Vector2f(4.495, 1.026));
    pos.push_back(Vector2f(0.514, 6.029));
    pos.push_back(Vector2f(2.019, 6.049));
    pos.push_back(Vector2f(5.528, 6.037));
    pos.push_back(Vector2f(1.513, 5.027));
    pos.push_back(Vector2f(3.016, 5.013));
    pos.push_back(Vector2f(5.042, 5.019));
    pos.push_back(Vector2f(0.514, 4.028));
    vector<Vector3i> tset;
    CGALDelaunayTriangulate(pos, tset);
    VerboseLevel::Restore();

    CHECK_EQUAL(23, tset.size());
    CHECK(CheckTriangle(tset, Vector3i(1 , 3 , 4 ))); 
    CHECK(CheckTriangle(tset, Vector3i(14, 13, 1 ))); 
    CHECK(CheckTriangle(tset, Vector3i(1 , 0 , 3 ))); 
    CHECK(CheckTriangle(tset, Vector3i(2 , 14, 1 ))); 
    CHECK(CheckTriangle(tset, Vector3i(15, 3 , 0 ))); 
    CHECK(CheckTriangle(tset, Vector3i(12, 15, 0 ))); 
    CHECK(CheckTriangle(tset, Vector3i(2 , 1 , 5 ))); 
    CHECK(CheckTriangle(tset, Vector3i(5 , 1 , 4 ))); 
    CHECK(CheckTriangle(tset, Vector3i(7 , 3 , 6 ))); 
    CHECK(CheckTriangle(tset, Vector3i(7 , 4 , 3 ))); 
    CHECK(CheckTriangle(tset, Vector3i(8 , 4 , 7 ))); 
    CHECK(CheckTriangle(tset, Vector3i(5 , 4 , 8 ))); 
    CHECK(CheckTriangle(tset, Vector3i(12, 9 , 15))); 
    CHECK(CheckTriangle(tset, Vector3i(13, 10, 12))); 
    CHECK(CheckTriangle(tset, Vector3i(5 , 11, 2 ))); 
    CHECK(CheckTriangle(tset, Vector3i(13, 12, 0 ))); 
    CHECK(CheckTriangle(tset, Vector3i(10, 9 , 12))); 
    CHECK(CheckTriangle(tset, Vector3i(1 , 13, 0 ))); 
    CHECK(CheckTriangle(tset, Vector3i(11, 10, 13))); 
    CHECK(CheckTriangle(tset, Vector3i(11, 14, 2 ))); 
    CHECK(CheckTriangle(tset, Vector3i(11, 13, 14))); 
    CHECK(CheckTriangle(tset, Vector3i(6 , 15, 9 ))); 
    CHECK(CheckTriangle(tset, Vector3i(6 , 3 , 15))); 
  }
}

#endif // ZZZ_LIB_CGAL