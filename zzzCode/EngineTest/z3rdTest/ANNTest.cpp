#include <UnitTest++.h>
#include <z3rd/Wrapper/ANNChar4Vector.hpp>

// using namespace zzz;

// SUITE(ANNTest)
// {
//   TEST(ANNCharTest)
//   {
//     RandomInteger<zuint> r(0,255);
//     ANNChar4Vector<128> ann;
//     vector<Vector<128,zuchar> > pool;
//     pool.reserve(20000);
//     for (zuint i=0; i<20000; i++)
//     {
//       Vector<128,zuchar> a;
//       for (zuint j=0; j<128; j++)
//         a[j]=r.Rand();
//       pool.push_back(a);
//     }
// 
//     Timer timer;
//     ann.SetData(pool);
//     zout<<"ANN Init used: "<<timer.Elapsed()<<endl;
//     timer.Restart();
//     vector<int> idx,dist;
//     idx.assign(2,0);
//     dist.assign(2,0);
//     ann.SetMaxPointVisit(200);
//     for (zuint i=0; i<20000; i++)
//     {
//       Vector<128,zuchar> a;
//       for (zuint j=0; j<128; j++)
//         a[j]=r.Rand();
//       ann.Query(a,2,idx,dist);
//     }
//     zout<<"Query used: "<<timer.Elapsed()<<endl;
//   }
// }