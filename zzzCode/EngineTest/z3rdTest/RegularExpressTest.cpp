#include <UnitTest++.h>
#include <z3rd/Wrapper/RegularExpression.hpp>

#ifdef ZZZ_LIB_BOOST
using namespace zzz;

SUITE(RegularExpressionTest)
{
  TEST(MatchTest)
  {
    RegularExpression re;
    //credit card
    re.Compile("(\\d{4}[- ])(\\d{4}[- ]){2}(\\d{4})");
    CHECK_EQUAL(true,re.Match("1234 5678 9183 1234"));
    CHECK_EQUAL(true,re.Match("1234-5678-9183-1234"));
    CHECK_EQUAL(false,re.Match("123a-5678-9183-1234"));
    vector<string> res;
    re.Match("1234-5678-9183-1234",res);
    CHECK_EQUAL(string("1234-"),res[0]);
    CHECK_EQUAL(string("9183-"),res[1]);
    CHECK_EQUAL(string("1234"),res[2]);
  }
  TEST(SearchTest)
  {
    RegularExpression re;
    //credit card
    re.Compile("(\\d{4}[- ])(\\d{4}[- ]){2}(\\d{4})");
    CHECK_EQUAL(9,re.Search("fdsarewfd1234-5678-9183-1234",0));
    vector<string> res;
    re.Search("fdsarewfd1234-5678-9183-1234",res,0);
    CHECK_EQUAL(string("1234-"),res[0]);
    CHECK_EQUAL(string("9183-"),res[1]);
    CHECK_EQUAL(string("1234"),res[2]);
  }
  TEST(ReplaceTest)
  {
    RegularExpression re;
    //credit card
    re.Compile("(\\d{4}[- ])(\\d{4}[- ]){2}(\\d{4})");
    CHECK_EQUAL(string("1234 9183 xyzw 1234"),re.Replace("1234 5678 9183 1234","\\1\\2xyzw \\3"));
  }
}
#endif // ZZZ_LIB_BOOST