#include <zCore/Math/Array2.hpp>
#include <z3rd/Wrapper/FFTW3Wrapper.hpp>
#include <UnitTest++.h>
#include <zCore/Math/Random.hpp>
#include <vector>
using namespace zzz;

#ifdef ZZZ_LIB_FFTW
SUITE(FFTWTest) {
  TEST(2DFFTWTest) {
    RandomReal<double> r;
    Array<2,double> v(4,6),v2;
    for (zuint i=0; i<v.size(); i++)
      v[i]=r.Rand();
    Array<2,Complex<double> > coeff,shift;
    FFT2(v,coeff);
    FFTShift(coeff,shift);
    FFTShift(shift,coeff);
    IFFT2(coeff,v2);
    for (zuint i=0; i<v.size(); i++)
      CHECK_CLOSE(v[i],v2[i],EPSILON);
  }

  TEST(1DFFTWTest) {
    RandomReal<double> r;
    vector<double> v(10,0),v2;
    for (zuint i=0; i<v.size(); i++)
      v[i]=r.Rand();
    vector<Complex<double> > coeff,shift;
    FFT(v,coeff);
    IFFT(coeff,v2);
    for (zuint i=0; i<v.size(); i++)
      CHECK_CLOSE(v[i],v2[i],EPSILON);
  }
}
#endif