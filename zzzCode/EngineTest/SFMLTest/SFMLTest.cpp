#include <zCore/zCore.hpp>
#include <zGraphics/zGraphics.hpp>
#include <zVision/Vision.hpp>

//beside 3 small memory leaks inside SFML
//other memory should be released, better than FreeGLUT,,,

int main(int argc, char* argv[])
{
  if (argc==1) return 0;

  OneObjRenderer myrenderer;
  zzz::SFMLContext context;
  context.Initialize(&myrenderer);
  return 0;
}



