#include <zCore/zCore.hpp>
using namespace zzz;

Mutex mutex;
int c=0;
class CheckCount:public Condition
{
public:
  bool IsSatisfied()
  {
    if (c==100) return true;
    return false;
  }
}checkcond;

class Counter:public Thread
{
public:
  void Main()
  {
    zout<<"Started!\n";
    bool good=true;
    while(good)
    {
      mutex.Lock();
      c++;
      if (c>=100) good=false;
      zout<<c<<endl;
      checkcond.Check();
      mutex.Unlock();
    }
  }
};

class Watcher:public Thread
{
public:
  void Main()
  {
    checkcond.Wait();
    zout<<"Satisfied\n";
  }
};

int main()
{
  Counter t1,t2;
  Watcher t3;
  t1.Start();
  t2.Start();
  t3.Start();
  t1.Wait();
  t2.Wait();
  t3.Wait();
}
