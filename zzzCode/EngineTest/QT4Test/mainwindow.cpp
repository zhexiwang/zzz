#include "mainwindow.h"
#include <QtGUI/QtGui>

MainWindow::MainWindow()
{
//    textEdit = new QPlainTextEdit;
//    setCentralWidget(textEdit);
  tab=new QTabWidget();
  setCentralWidget(tab);
  glWidget[0] = new zzz::zQt4GLWidget(&m_renderer1);
  glWidget[1] = new zzz::zQt4GLWidget(&m_renderer2);
  glWidget[2] = new zzz::zQt4GLWidget(&m_renderer3);
  tab->addTab(glWidget[0],"Obj Viewer");
  tab->addTab(glWidget[1],"Image Cutter");
  tab->addTab(glWidget[2],"Basic Renderer");
  connect(tab,SIGNAL(currentChanged(int)),this,SLOT(switchdisplay(int)));
  switchdisplay(0);
//  Image3f ori;
//  ori.Load("683.jpg");
//  zout<<ori.Rows()<<'x'<<ori.Cols()<<endl;
//  m_renderer.m_image=new zzz::Image3f;
//  *m_renderer.m_image=ori;
//  zzz::ImageFilter<float>::GaussBlurImage(&ori,m_renderer.m_image,10,100);

    createActions();
    createMenus();
    createToolBars();
    createStatusBar();
    createDock();
    readSettings();
}

MainWindow::~MainWindow()
{
}

void MainWindow::closeEvent(QCloseEvent *event)
{
  writeSettings();
}

void MainWindow::open()
{
    QString fileName = QFileDialog::getOpenFileName(this);
    if (!fileName.isEmpty())
  {
    m_curio->LoadFile(fileName.toLatin1().data());
  }
}

bool MainWindow::save()
{
    QString fileName = QFileDialog::getSaveFileName(this);
    if (!fileName.isEmpty())
  {
    m_curio->SaveFile(fileName.toLatin1().data());
  }
    return true;
}

void MainWindow::about()
{
   QMessageBox::about(this, tr("About Application"),
            tr("The <b>Application</b> example demonstrates how to "
               "write modern GUI applications using Qt, with a menu bar, "
               "toolbars, and a status bar."));
}

void MainWindow::createActions()
{
    openAct = new QAction(QIcon(":/images/open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the document to disk"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    cutAct = new QAction(QIcon(":/images/cut.png"), tr("Cu&t"), this);
    cutAct->setShortcuts(QKeySequence::Cut);
    cutAct->setStatusTip(tr("Cut the current selection's contents to the "
                            "clipboard"));

    copyAct = new QAction(QIcon(":/images/copy.png"), tr("&Copy"), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    copyAct->setStatusTip(tr("Copy the current selection's contents to the "
                             "clipboard"));

    pasteAct = new QAction(QIcon(":/images/paste.png"), tr("&Paste"), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    pasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
                              "selection"));

    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    cutAct->setEnabled(false);
    copyAct->setEnabled(false);
}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);

    menuBar()->addSeparator();

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
}

void MainWindow::createToolBars()
{
    fileToolBar = addToolBar(tr("File"));
    fileToolBar->addAction(openAct);
    fileToolBar->addAction(saveAct);

    editToolBar = addToolBar(tr("Edit"));
    editToolBar->addAction(cutAct);
    editToolBar->addAction(copyAct);
    editToolBar->addAction(pasteAct);
}

void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

void MainWindow::readSettings()
{
    QSettings settings("WZX", "QT4Test");
    QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
    QSize size = settings.value("size", QSize(400, 400)).toSize();
    resize(size);
    move(pos);
}

void MainWindow::writeSettings()
{
    QSettings settings("WZX", "QT4Test");
    settings.setValue("pos", pos());
    settings.setValue("size", size());
}

void MainWindow::createDock()
{
//   displayDock = new QDockWidget(tr("Display"), this);
//   QGroupBox *showbox=new QGroupBox(displayDock);
//   QRadioButton *showimg=new QRadioButton("Image");
//   showimg->setChecked(true);
//   QRadioButton *showmodel=new QRadioButton("Model");
//   QRadioButton *showcut=new QRadioButton("Cut");
// 
//   QVBoxLayout *vbox = new QVBoxLayout;
//   vbox->addWidget(showimg);
//   vbox->addWidget(showmodel);
//   vbox->addWidget(showcut);
//   vbox->addStretch(1);
//   showbox->setLayout(vbox);
//   displayDock->setWidget(showbox);
//   addDockWidget(Qt::RightDockWidgetArea,displayDock);
// 
//   displayGroup=new QButtonGroup();
//   displayGroup->addButton(showimg,1);
//   displayGroup->addButton(showmodel,2);
//   displayGroup->addButton(showcut,3);
//   connect(displayGroup,SIGNAL(buttonClicked(int)),this,SLOT(switchdisplay(int)));
}

void MainWindow::switchdisplay(int x)
{
  m_curio=dynamic_cast<IOData*>(glWidget[x]->GetRenderer());
}