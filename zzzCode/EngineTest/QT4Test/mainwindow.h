#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ImageTestRenderer.h"
#include "ObjTestRenderer.h"
#include <zGraphics/zGraphics.hpp>
#include <QtGUI/QMainWindow>
using namespace zzz;

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
class QPlainTextEdit;
class QDockWidget;
class QButtonGroup;
class QTableWidget;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
  ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void open();
    bool save();
    void about();
  void switchdisplay(int x);

private:
    void createActions();
    void createMenus();
    void createToolBars();
    void createStatusBar();
  void createDock();
    void readSettings();
    void writeSettings();

  ObjTestRenderer m_renderer1;
  ImageTestRenderer m_renderer2;
  OneObjRenderer m_renderer3;
  zzz::zQt4GLWidget *glWidget[3];
  IOData *m_curio;
    QString curFile;

    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *helpMenu;
    QToolBar *fileToolBar;
    QToolBar *editToolBar;
    QAction *openAct;
    QAction *saveAct;
    QAction *exitAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *pasteAct;
    QAction *aboutAct;
  QDockWidget *displayDock;
  QButtonGroup *displayGroup;
  QTabWidget *tab;

  QString fileName;
};
//! [0]

#endif
