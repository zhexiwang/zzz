#pragma once
#include <zGraphics/zGraphics.hpp>
#include <zCore/Utility/IOInterface.hpp>
#include <zImage/Image/Image.hpp>


class ObjTestRenderer :
    public zzz::OneObjRenderer, public zzz::IOData {
public:
  ObjTestRenderer(void);
  ~ObjTestRenderer(void);
  bool InitData();
  void DrawObj();
  bool LoadFile(const string &filename);
  bool SaveFile(const string &filename);
  void OnChar(zzz::zuint nchar, zzz::zuint nrep,zzz::zuint flags);
  void OnMouseMove(unsigned int nFlags,int x,int y);
  void SetupCamera();

  void DrawPicking();
  void RenderPickingTexture();
  void AfterRender();
  void AfterOnSize(unsigned int nType, int cx, int cy);
  void CreateMsg();

private:
  void TrueLoadFile();
  string filename_;

  zzz::TriMesh obj_;
  zzz::Image3uc picking_tex_;
  zzz::VBO vboppos_, vbopcolor_;
  int tri_n_, hovering_;

  bool showwire_;
  bool drawnormal_;
  bool picking_;
};
