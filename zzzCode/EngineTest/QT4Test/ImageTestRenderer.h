#pragma once
#include <zVision/zVision.hpp>
#include <zCore/Utility/IOInterface.hpp>
#include <zCore/Math/Vector3.hpp>

class ImageTestRenderer :
  public zzz::CutRenderer<zzz::Vector4f>, public zzz::IOData
{
public:
  ImageTestRenderer(void);
  ~ImageTestRenderer(void);
  bool InitData();
  bool LoadFile(const string &filename);
  bool SaveFile(const string &filename);
};
