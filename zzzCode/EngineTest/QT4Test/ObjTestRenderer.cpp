#include "ObjTestRenderer.h"
using namespace zzz;

ObjTestRenderer::ObjTestRenderer(void)
  : showwire_(false),
    drawnormal_(false),
    picking_(false) {
  gui_=new AntTweakBarGUI;
  TwBar *bar = TwNewBar("bar");
  TwDefine(" GLOBAL help='QT4Test' "); // Message added to the help bar.
  TwDefine(" bar size='300 600' position='0 100' valueswidth='100' color='96 216 224' "); // change default tweak bar size and color
  TwDefine(" bar label='QT4Test Tweak Bar' ");
  TwAddVarRW(bar, "show_wire", TW_TYPE_BOOLCPP, &showwire_, "label='Show wire'");
  TwAddVarRW(bar, "draw_normal", TW_TYPE_BOOLCPP, &drawnormal_, "label='Draw normal'");
  TwAddVarRW(bar, "pick_triangle", TW_TYPE_BOOLCPP, &picking_, "label='Pick triangle'");
}

ObjTestRenderer::~ObjTestRenderer(void) {
}

bool ObjTestRenderer::InitData() {
  glDisable(GL_BLEND);
  MakeShaders();
  if (filename_.empty() && FileExists("model/hand.obj"))
    filename_ = "model/hand.obj";
  TrueLoadFile();
  return OneObjRenderer::InitData();
}

void ObjTestRenderer::OnChar(zuint nchar, zuint nrep,zuint flags) {
  if (nchar=='f') showwire_=!showwire_;
  else if (nchar=='n') drawnormal_=!drawnormal_;
  else if (nchar=='c') obj_.CalPosNormal();
  Redraw();
}

void ObjTestRenderer::DrawObj() {
  glDisable(GL_CULL_FACE);

  if (showwire_)
    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
  else
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
  glPushMatrix();
  for (zuint i=0; i<obj_.groups_.size(); i++) {
    if (!obj_.groups_[i]->matname_.empty() && obj_.HasFlag(MESH_TEX)) {
      ZRM->Get<Material*>(obj_.groups_[i]->matname_)->BindAmbientTexture();
      ZRM->Get<Material*>(obj_.groups_[i]->matname_)->BindDiffuseTexture();
      ColorDefine::white.ApplyGL();
      ZRM->Get<Shader*>("TextureShader")->Begin();
      obj_.DrawVBO(i,MESH_POS|MESH_TEX);
      Shader::End();
    } else if (obj_.HasFlag(MESH_NOR)) {
      ColorDefine::white.ApplyGL();
      ZRM->Get<Shader*>("LightShader")->Begin();
      obj_.DrawVBO(i,MESH_POS|MESH_COLOR|MESH_NOR);
      Shader::End();
    } else {
      ColorDefine::white.ApplyGL();
      ZRM->Get<Shader*>("ColorShader")->Begin();
      obj_.DrawVBO(i,MESH_POS|MESH_COLOR);
      Shader::End();
    }
  }

  if (drawnormal_) {
    if (obj_.HasNoFlag(MESH_POSNOR)) obj_.CalPosNormal();
    ZRM->Get<Shader*>("ColorShader")->Begin();
    glBegin(GL_LINES);
    for (zuint i=0; i<obj_.pos_.size(); i++) {
      ColorDefine::white.ApplyGL();
      glVertex3fv(obj_.pos_[i].Data());
      ColorDefine::red.ApplyGL();
      glVertex3fv((obj_.pos_[i]+obj_.posnor_[i]).Data());
    }
    glEnd();
    Shader::End();
  }
  glPopMatrix();
}

bool ObjTestRenderer::LoadFile(const string &filename) {
  filename_ = filename;
  if (GLExists())
    TrueLoadFile();
  return true;
}

bool ObjTestRenderer::SaveFile(const string &filename) {
  MakeCurrent();
  return MeshIO::Save(filename,obj_);
}

void ObjTestRenderer::DrawPicking() {
  GLPolygonMode::SetFill();
  vboppos_.Bind();
  vbopcolor_.Bind();
  ZCOLORSHADER->Begin();
  glDrawArrays(GL_TRIANGLES, 0, tri_n_);
  ZSHADEREND();
  GLPolygonMode::Restore();
}

void ObjTestRenderer::RenderPickingTexture() {
  Texture2D tex;
  tex.Create(width_, height_);
  CHECK_GL_ERROR();
  RendererHelper helper;
  helper.RenderToTexture(tex, bind(&ObjTestRenderer::SetupCamera, this), bind(&ObjTestRenderer::DrawPicking, this));
  CHECK_GL_ERROR();
  tex.TextureToImage(picking_tex_);
  //picking_tex_.SaveFile("D:\\debug.png");
}

void ObjTestRenderer::SetupCamera() {
  OneObjRenderer::SetupCamera();
  float diameter=obj_.AABB_.Diff().Max();
  diameter=1.0f/diameter;
  glScalef(diameter,diameter,diameter);
  glTranslatefv((-obj_.AABB_.Center()).Data());
}

void ObjTestRenderer::AfterRender() {
  if (picking_)
    RenderPickingTexture();
}

void ObjTestRenderer::OnMouseMove(unsigned int nFlags,int x,int y) {
  if (picking_) {
    int r = height_ - 1 - y, c = x;
    if (picking_tex_.IsInside(Vector2i(r, c)))
      hovering_ = RendererHelper::ColorToZuint(picking_tex_(r, c));
  }
  Redraw();
  OneObjRenderer::OnMouseMove(nFlags, x, y);
}

void ObjTestRenderer::CreateMsg() {
  OneObjRenderer::CreateMsg();
  if (picking_)
    msg_ << "\nHovering at " << hovering_;
}

void ObjTestRenderer::AfterOnSize( unsigned int nType, int cx, int cy ) {
  if (picking_)
    RenderPickingTexture();
  OneObjRenderer::AfterOnSize(nType, cx, cy);
}

void ObjTestRenderer::TrueLoadFile() {
  if (filename_.empty()) return;
  MakeCurrent();
  ZCHECK(MeshIO::Load(filename_,obj_));
  Redraw();
  // create picking vbo
  STLVector<Vector3f> buffer_pos;
  STLVector<Vector3uc> buffer_color;
  int cur = 0;
  for (zuint i = 0; i < obj_.groups_.size(); ++i) {
    TriMesh::MeshGroup *g = obj_.groups_[i];
    int size=g->facep_.size();
    for (int j = 0; j < size; j++) {
      Vector3uc c = RendererHelper::ZuintToColor(cur).RGB();
      for (int k = 0; k < 3; ++k) {
        buffer_pos.push_back(obj_.pos_[g->facep_[j][k]]);
        buffer_color.push_back(c);
      }
      ++cur;
    }
    
  }
  vboppos_.Create(buffer_pos.Data(), buffer_pos.size(), VBODescript::Vertex3f);
  vbopcolor_.Create(buffer_color.Data(), buffer_color.size(), VBODescript::Color3ub);
  tri_n_ = buffer_color.size();
}
