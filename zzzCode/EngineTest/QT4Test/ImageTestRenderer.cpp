#include "ImageTestRenderer.h"

using namespace zzz;
ImageTestRenderer::ImageTestRenderer(void)
{
}

ImageTestRenderer::~ImageTestRenderer(void)
{
}

bool ImageTestRenderer::InitData()
{
  Image4f img;
  if (FileExists("a.bmp")) {
    img.LoadFile("a.bmp");
  } else {
    img.SetSize(256, 256);
    img.Fill(Vector4f(1, 0, 1, 1));
  }
  Prepare(img);
  return CutRenderer::InitData();
}

bool ImageTestRenderer::LoadFile(const string &filename)
{
  Image4f img(filename);
  Prepare(img);
  Redraw();
  return true;
}

bool ImageTestRenderer::SaveFile(const string &filename)
{
  return true;
}