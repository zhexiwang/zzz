#include "mainwindow.h"
#include <QtGUI/QApplication>
#include <zCore/zCore.hpp>
#include <zCore/zCoreAutoLink.hpp>
#include <zGraphics/zGraphicsAutoLink.hpp>
#include <zImage/zImageAutoLink.hpp>
#include <z3rd/z3rdAutoLink.hpp>

int main(int argc, char *argv[]) {
  ZINIT(argv[0]);
//   TriMesh *mesh=new TriMesh;
//   DaeIO::Load(PathFile(CompleteDirName("d:\\GoogleData\\pitts_model\\a\\"),"mesh.dae").c_str(), *mesh);

  QApplication app(argc, argv);
  MainWindow mainWin;
  mainWin.show();
  return app.exec();
}