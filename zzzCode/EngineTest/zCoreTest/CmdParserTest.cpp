#define SCL_SECURE_NO_WARNINGS
#include <UnitTest++.h>
#include <zCore/Utility/CmdParser.hpp>
using namespace zzz;

ZFLAGS_STRING2(istr, "", "Input string", 's');
ZFLAGS_DOUBLE2(idbl, 50, "Input double", 'd');
ZFLAGS_INT2(iint, 1, "Input int", 'i');
ZFLAGS_SWITCH2(iswt, "Input switch", 'w');
ZFLAGS_BOOL2(ibool, true, "Input bool", 'b');
SUITE(CmdParserTest) {
  TEST(ParseTest) {
    char *argv[10];
    for (int i = 0; i < 9; ++i)
      argv[i] = new char[100];
    argv[9] = NULL;
    strcpy(argv[0], "HAHA");
    strcpy(argv[1], "--no_ibool");
    strcpy(argv[2], "--idbl");
    strcpy(argv[3], "49.9");
    strcpy(argv[4], "-s");
    strcpy(argv[5], "abcde");
    strcpy(argv[6], "--iint");
    strcpy(argv[7], "123");
    strcpy(argv[8], "-w");
    int argc = 9;

    CHECK_EQUAL("", ZFLAG_istr);
    CHECK_EQUAL(50, ZFLAG_idbl);
    CHECK_EQUAL(1, ZFLAG_iint);
    CHECK_EQUAL(false, ZFLAG_iswt);
    CHECK_EQUAL(true, ZFLAG_ibool);

    ZCMDPARSE(argv[0]);

    CHECK_EQUAL("abcde", ZFLAG_istr);
    CHECK_EQUAL(49.9, ZFLAG_idbl);
    CHECK_EQUAL(123, ZFLAG_iint);
    CHECK_EQUAL(true, ZFLAG_iswt);
    CHECK_EQUAL(false, ZFLAG_ibool);
  }
}