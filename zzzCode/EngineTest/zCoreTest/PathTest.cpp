#include <UnitTest++.h>
#include <zCore/Utility/FileTools.hpp>
using namespace std;
using namespace zzz;


SUITE(PathTest)
{
#ifdef ZZZ_OS_WIN
  TEST(PathTest)
  {
    string p="c:\\wzx\\sub\\..\\abcd.txt";
    CHECK_EQUAL("abcd.txt",GetFilename(p));
    CHECK_EQUAL(".txt",GetExt(p));
    CHECK_EQUAL("abcd",GetBase(p));
    CHECK_EQUAL("c:\\wzx\\sub\\..\\",GetPath(p));
#ifdef ZZZ_LIB_BOOST
    p = NormalizePath(p);
    CHECK_EQUAL(Path("c:\\wzx\\abcd.txt"),Path(p));
#endif // ZZZ_LIB_BOOST

    string p2="c:\\wzx\\sub\\";
    CHECK_EQUAL("c:\\wzx\\sub\\",GetPath(p2));

    string f("123.456");
    p="c:\\wzx\\abcd.txt";
    CHECK_EQUAL("c:\\wzx\\123.456",PathFile(p,f));
    CHECK_EQUAL(".\\",GetPath(f));

    f="xyz\\123.456";
    CHECK_EQUAL("c:\\wzx\\xyz\\123.456",PathFile(p,f));

    f="c:\\wzx\\xyz\\123.456";
    CHECK_EQUAL("c:\\wzx\\xyz\\123.456",PathFile(p,f));

#ifdef ZZZ_LIB_BOOST
    f="c:\\wzx\\xyz";
    CHECK_EQUAL("sub\\.", RelativeTo(GetPath(p2), f));
    CHECK_EQUAL(".", RelativeTo(GetPath(p), f));
    CHECK_EQUAL("sub\\.", RelativeTo(p2, f));
    CHECK_EQUAL("abcd.txt", RelativeTo(p, f));

    f="c:\\wzx\\xyz\\";
    CHECK_EQUAL("..\\sub\\.",RelativeTo(GetPath(p2),f));
    CHECK_EQUAL("..\\.",RelativeTo(GetPath(p),f));
    CHECK_EQUAL("..\\sub\\.",RelativeTo(p2,f));
    CHECK_EQUAL("..\\abcd.txt",RelativeTo(p,f));
#endif // ZZZ_LIB_BOOST

    f="c:\\wzx\\xyz\\";
    vector<string> s = SplitPath(f);
    CHECK_EQUAL(5, s.size());
    CHECK_EQUAL("c:", s[0]);
    CHECK_EQUAL("/", s[1]);
    CHECK_EQUAL("wzx", s[2]);
    CHECK_EQUAL("xyz", s[3]);
    CHECK_EQUAL(".", s[4]);

    s = SplitPath(p);
    CHECK_EQUAL(4, s.size());
    CHECK_EQUAL("c:", s[0]);
    CHECK_EQUAL("/", s[1]);
    CHECK_EQUAL("wzx", s[2]);
    CHECK_EQUAL("abcd.txt", s[3]);
  }
#endif
}