#include <UnitTest++.h>
#include <zCore/Math/Vector.hpp>
#include <zCore/Math/Array1.hpp>
#include <zCore/Math/Array2.hpp>
#include <zCore/Math/Random.hpp>
using namespace zzz;

SUITE(ArrayTest) {
  TEST(Constructor) {
    Vector<2,zuint> sizes(2,2);
    Array<2,float> x(2,2);
  }

  TEST(InterpolateTest2) {
    RandomReal<double> r;
    //2D imsymmetric
    Array<2,double> data2(2,2);
    for (zuint i=0; i<data2.size(); i++) data2[i]=r.Rand();
    Vector<2,double> coord;
    coord[0]=0.7; coord[1]=0.1;
    double ans=(data2(Vector2i(0,0))*0.3+data2(Vector2i(1,0))*0.7)*0.9+(data2(Vector2i(0,1))*0.3+data2(Vector2i(1,1))*0.7)*0.1;
    CHECK_CLOSE(ans,data2.Interpolate(coord),EPSILON);
  }

  TEST(InterpolateTest) {
    RandomReal<double> r;
    //1D
    Array<1, double> data1(2);
    for (zuint i = 0; i < data1.size(); ++i) data1[i] = r.Rand();
    CHECK_CLOSE(data1.Sum() / data1.size(), data1.Interpolate(0.5), EPS);

    //2D
    Array<2, double> data2(Vector2i(2));
    for (zuint i = 0; i < data2.size(); ++i) data2[i] = r.Rand();
    CHECK_CLOSE(data2.Sum() / data2.size(), data2.Interpolate(Vector<2, double>(0.5)), EPS);

    //3D
    Array<3,double> data3(Vector3i(2));
    for (zuint i = 0; i < data3.size(); ++i) data3[i] = r.Rand();
    CHECK_CLOSE(data3.Sum() / data3.size(), data3.Interpolate(Vector<3,double>(0.5)), EPS);

    //8D
    Array<8, double> data8(Vector<8, int>(2));
    for (zuint i = 0; i < data8.size(); ++i) data8[i] = r.Rand();
    CHECK_CLOSE(data8.Sum() / data8.size(), data8.Interpolate(Vector<8,double>(0.5)), EPS);
  }
}