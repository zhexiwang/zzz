#include <UnitTest++.h>

#include <zCore/Xml/XML.hpp>
#ifdef ZZZ_LIB_TINYXML
using namespace zzz;

/*
<?xml version="1.0" ?>
<MyApp>
  <Messages>
    <Welcome>Welcome to MyApp</Welcome>
    <Farewell>Thank you for using MyApp</Farewell>
  </Messages>
  <Windows>
    <Window name="MainFrame" x="5" y="15" w="400" h="250" />
  </Windows>
  <Connection ip="192.168.0.1" timeout="123.456000" />
</MyApp>
*/

SUITE(XMLTest)
{
  TEST(XMLWrite)
  {
    XML xml;
    //write
    XMLNode head=xml.AppendNode("MyApp");
    XMLNode messages=head.AppendNode("Messages");
    XMLNode welcome=messages.AppendNode("Welcome");
    welcome<<"Welcome to MyApp";
    XMLNode farewell=messages.AppendNode("Farewell");
    farewell.SetText("Thank you for using MyApp");
    XMLNode windows=head.AppendNode("Windows");
    XMLNode window=windows.AppendNode("Window");
    window<<make_pair("name","MainFrame");
    window<<make_pair("x",5);
    window<<make_pair("y",15);
    window<<make_pair("w","400");
    window<<make_pair("h","250");
    XMLNode connection=head.AppendNode("Connection");
    connection.SetAttribute("ip","192.168.0.1");
    connection.SetAttribute("timeout","123.456000");
    xml.SaveFile("test.xml");
  }

  TEST(XMLRead)
  {
    //read
    XML xml;    
    xml.LoadFile("test.xml");
    CHECK_EQUAL(1,xml.NodeNumber());
    XMLNode rhead=xml.GetNode("MyApp");
    CHECK(rhead.IsValid());
    CHECK_EQUAL(3,rhead.NodeNumber());
    XMLNode rmessages=rhead.GetNode((zuint)0);
    CHECK(rmessages.IsValid());
    CHECK_EQUAL(2,rmessages.NodeNumber());
    XMLNode rwelcome=rmessages.GetNode("Welcome");
    CHECK(rwelcome.IsValid());
    CHECK(strcmp(rwelcome.GetText(),"Welcome to MyApp")==0);
    string str(rmessages.GetNode("Farewell").GetText());
    CHECK(str=="Thank you for using MyApp");
    XMLNode rwindows=rhead.GetNode("Windows");
    CHECK(rwindows.IsValid());
    XMLNode rwindow=rwindows.GetNode((zuint)0);
    CHECK(rwindow.IsValid());
    CHECK(strcmp(rwindow.GetAttribute("name"),"MainFrame")==0);
    CHECK_EQUAL(5,FromString<int>(rwindow.GetAttribute("x")));
    CHECK_EQUAL(15,FromString<int>(rwindow.GetAttribute("y")));
    CHECK_EQUAL(400,FromString<int>(rwindow.GetAttribute("w")));
    CHECK_EQUAL(250,FromString<int>(rwindow.GetAttribute("h")));
    XMLNode rconnection=rhead.GetNode("Connection");
    CHECK(rconnection.IsValid());
    CHECK(strcmp(rconnection.GetAttribute("ip"),"192.168.0.1")==0);
    CHECK_EQUAL(123.456,FromString<double>(rconnection.GetAttribute("timeout")));
    CHECK(!rhead.HasNode("not exist"));
  }
}
#endif // ZZZ_LIB_TINYXML