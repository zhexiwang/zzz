#include <UnitTest++.h>

#include <zCore/XML/RapidXML.hpp>
#ifdef ZZZ_LIB_RAPIDXML
using namespace zzz;

/*
<?xml version="1.0" ?>
<MyApp>
  <Messages>
    <Welcome>Welcome to MyApp</Welcome>
    <Farewell>Thank you for using MyApp</Farewell>
  </Messages>
  <Windows>
    <Window name="MainFrame" x="5" y="15" w="400" h="250" />
  </Windows>
  <Connection ip="192.168.0.1" timeout="123.456000" />
</MyApp>
*/

SUITE(RapidXMLTest)
{
  TEST(XMLWrite)
  {
    RapidXML xml;
    //write
    RapidXMLNode head=xml.AppendNode("MyApp");
    RapidXMLNode messages=head.AppendNode("Messages");
    RapidXMLNode welcome=messages.AppendNode("Welcome");
    welcome<<"Welcome to MyApp";
    RapidXMLNode farewell=messages.AppendNode("Farewell");
    farewell.SetText("Thank you for using MyApp");
    RapidXMLNode windows=head.AppendNode("Windows");
    RapidXMLNode window=windows.AppendNode("Window");
    window<<make_pair("name","MainFrame");
    window<<make_pair("x",5);
    window<<make_pair("y",15);
    window<<make_pair("w","400");
    window<<make_pair("h","250");
    RapidXMLNode connection=head.AppendNode("Connection");
    connection.SetAttribute("ip","192.168.0.1");
    connection.SetAttribute("timeout","123.456000");
    xml.SaveFile("testrapid.xml");
  }

  TEST(XMLRead)
  {
    //read
    RapidXML xml;    
    xml.LoadFile("testrapid.xml");
    CHECK_EQUAL(1,xml.NodeNumber());
    RapidXMLNode rhead=xml.GetNode("MyApp");
    CHECK(rhead.IsValid());
    CHECK_EQUAL(3,rhead.NodeNumber());
    RapidXMLNode rmessages=rhead.GetNode((zuint)0);
    CHECK(rmessages.IsValid());
    CHECK_EQUAL(2,rmessages.NodeNumber());
    RapidXMLNode rwelcome=rmessages.GetNode("Welcome");
    CHECK(rwelcome.IsValid());
    CHECK(strcmp(rwelcome.GetText(),"Welcome to MyApp")==0);
    string str(rmessages.GetNode("Farewell").GetText());
    CHECK(str=="Thank you for using MyApp");
    RapidXMLNode rwindows=rhead.GetNode("Windows");
    CHECK(rwindows.IsValid());
    RapidXMLNode rwindow=rwindows.GetNode((zuint)0);
    CHECK(rwindow.IsValid());
    CHECK(strcmp(rwindow.GetAttribute("name"),"MainFrame")==0);
    CHECK_EQUAL(5,FromString<int>(rwindow.GetAttribute("x")));
    CHECK_EQUAL(15,FromString<int>(rwindow.GetAttribute("y")));
    CHECK_EQUAL(400,FromString<int>(rwindow.GetAttribute("w")));
    CHECK_EQUAL(250,FromString<int>(rwindow.GetAttribute("h")));
    RapidXMLNode rconnection=rhead.GetNode("Connection");
    CHECK(rconnection.IsValid());
    CHECK(strcmp(rconnection.GetAttribute("ip"),"192.168.0.1")==0);
    CHECK_EQUAL(123.456,FromString<double>(rconnection.GetAttribute("timeout")));
    CHECK(!rhead.HasNode("not exist"));
  }
}
#endif // ZZZ_LIB_RAPIDXML