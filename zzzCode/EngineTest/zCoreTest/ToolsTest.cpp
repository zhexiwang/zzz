#include <UnitTest++.h>
#include <zCore/Utility/Timer.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Utility/Tools.hpp>
#include <zCore/Math/Vector4.hpp>

using namespace zzz;
using namespace std;

SUITE(TOOLSTEST) {

  TEST(TimerTest) {
    Timer t;
    int n=0;
    t.Sleep(100);
    double thistime=t.Elapsed();
    t.Pause();
    t.Sleep(100);
    CHECK_EQUAL(thistime,t.Elapsed());
    t.Resume();
    t.Sleep(100);
    CHECK(thistime<t.Elapsed());
  }

  TEST(SafeEqualTest) {
    Vector3f a(1,2,3), b(1,2,3), c(1,3,2);
    CHECK(SafeEqual(a.begin(),a.end(), b.begin(),b.end()));
    CHECK(!SafeEqual(a.begin(),a.end(), c.begin(),c.end()));
    Vector4f d(1,2,3,4);
    CHECK(SafeEqual(a.begin(),a.end(), d.begin(),d.end()));
    CHECK(!SafeEqual(d.begin(),d.end(), a.begin(),a.end()));
  }

  TEST(BitTest) {
    int BIT1=1;
    int BIT2=1<<2;
    int BIT3=1<<10;
    int a = BIT2;
    SetBit(a, BIT1);
    CHECK_EQUAL(BIT1+BIT2,a);
    CHECK(CheckBit(a, BIT1));
    CHECK(CheckBit(a, BIT2));
    CHECK(!CheckBit(a, BIT3));
    ClearBit(a, BIT2);
    CHECK(!CheckBit(a, BIT2));
  }

}