#include <UnitTest++.h>
#include <zCore/Utility/BraceFile.hpp>
#include <zCore/Utility/BraceNode.hpp>
using namespace zzz;
/*
This is the first line
MyApp {
  Messages msg1{
    Welcome to MyApp
    Thank you for using MyApp
  }
  Window {name MainFrame
    pos [5 15] size [400,250]}
  Connection{ ip "192.
  168.
  0.1"
  timeout 123.456000}
}
*/

SUITE(BraceFileTest) {
  TEST(BraceWrite) {
    BraceFile bf("{}[]");
    //write
    BraceNode head=bf.AppendNode("MyApp",'{','}');
    BraceNode messages=head.AppendNode("Messages",'{','}');
    BraceNode welcome=messages.AppendNode("Welcome to MyApp");
    messages<<"Thank you for using MyApp";

    BraceNode window=head.AppendNode("Window",'{','}');
    window<<"name MainFrame";
    window.AppendNode("pos",'[',']')<<"5 15";
    window.AppendNode("size",'[',']')<<"400,250";

    BraceNode connection=head.AppendNode("Connection",'{','}');
    connection.AppendNode("ip \"192.168.0.1\"");
    connection.AppendNode("timeout 123.456000");
  
  }
}