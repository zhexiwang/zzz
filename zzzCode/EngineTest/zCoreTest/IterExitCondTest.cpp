#include <UnitTest++.h>
#include <zCore/Math/IterExitCond.hpp>
using namespace zzz;

SUITE(IterExitCondTest) {
  TEST(ResidueTest) {
    IterExitCond<double> cond;
    cond.SetCondResidue(0.1);
    CHECK(!cond.IsSatisfied(10));
    CHECK(!cond.IsSatisfied(-5));
    CHECK(!cond.IsSatisfied(1));
    CHECK(!cond.IsSatisfied(0.11));
    CHECK(cond.IsSatisfied(0.1));
    cond.Reset();
    CHECK(cond.IsSatisfied(-0.01));
    cond.Reset();
    CHECK(cond.IsSatisfied(0));
  }

  TEST(CountTest) {
    IterExitCond<double> cond;
    cond.SetCondIterCount(3);
    CHECK(!cond.IsSatisfied(10));
    CHECK(!cond.IsSatisfied(10));
    CHECK(cond.IsSatisfied(10));
  }

  TEST(RepeatTest) {
    IterExitCond<double> cond;
    cond.SetCondRepeat(3,3);
    CHECK(!cond.IsSatisfied(10));
    CHECK(!cond.IsSatisfied(11));
    CHECK(!cond.IsSatisfied(9));
    CHECK(!cond.IsSatisfied(10));
    CHECK(!cond.IsSatisfied(8));
    CHECK(!cond.IsSatisfied(9));
    CHECK(!cond.IsSatisfied(10));
    CHECK(!cond.IsSatisfied(8));
    CHECK(!cond.IsSatisfied(9));
    CHECK(!cond.IsSatisfied(10));
    CHECK(cond.IsSatisfied(8));
  }

}