#include <UnitTest++.h>
#include <zCore/Utility/Global.hpp>
using namespace zzz;

SUITE(GlobalTest) {
  TEST(AssignAndRetrieve) {
    int x = 2;
    ZGLOBAL_ADD(x);
    ZGLOBAL_GET(int, x) = 100;
    CHECK_EQUAL(100, x);
  }
  TEST(GlobalHelperFunction) {
    ZGLOBAL_DEFINE(int, y, 10);
    ZGLOBAL_GET(int, y) = 100;
    CHECK_EQUAL(100, y);
    CHECK_EQUAL(100, ZGLOBAL_GET(int, y));
    CHECK(ZGLOBAL_EXIST(y));
    CHECK(ZGLOBAL_ISTYPE(int, y));
    CHECK(!ZGLOBAL_ISTYPE(float, y));
  }
}