#include <zCore/Math/Matrix2x2.hpp>
#include <zCore/Math/Statistics.hpp>
#include <UnitTest++.h>

using namespace zzz;

SUITE(StatisticsTest)
{
  TEST(PCA)
  {
    vector<Vector2d> data;
    data.push_back(Vector2d(2.5, 2.4));
    data.push_back(Vector2d(0.5, 0.7));
    data.push_back(Vector2d(2.2, 2.9));
    data.push_back(Vector2d(1.9, 2.2));
    data.push_back(Vector2d(3.1, 3.0));
    data.push_back(Vector2d(2.3, 2.7));
    data.push_back(Vector2d(2.0, 1.6));
    data.push_back(Vector2d(1.0, 1.1));
    data.push_back(Vector2d(1.5, 1.6));
    data.push_back(Vector2d(1.1, 0.9));

    Matrix2x2d cov;
    Covariance(data, cov);
    CHECK_EQUAL(cov(0,1),cov(1,0));
    CHECK_CLOSE(0.616555556,cov(0,0),EPSILON);
    CHECK_CLOSE(0.615444444,cov(0,1),EPSILON);
    CHECK_CLOSE(0.716555556,cov(1,1),EPSILON);

    Vector2d evalue;
    Matrix<2,2,double> evector;
    PCA(data,evector,evalue);
    CHECK_CLOSE(1.28402771,evalue[0],EPSILON);
    CHECK_CLOSE(0.0490833989,evalue[1],EPSILON);
  }
}