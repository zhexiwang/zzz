#include <UnitTest++.h>
#include <zCore/Utility/AutoloadCacheSet.hpp>
#include <vector>

using namespace zzz;
using namespace std;

SUITE(CacheSetTest) {
  struct Content {
    Content(int i):x(i){construct_seq.push_back(i);}
    ~Content(){destruct_seq.push_back(x);}
    int x;
    static vector<int> construct_seq;
    static vector<int> destruct_seq;
  };
  vector<int> Content::construct_seq, Content::destruct_seq;

  TEST(CacheSetTest) {
    class ContentCacheSet : public AutoloadCacheSet<Content*, pair<int, int> > {
    public:
      ContentCacheSet(){SetSize(10);}
      Content* Load(const pair<int, int>  &i){return new Content(i.first+i.second);}
    };

    ContentCacheSet *s = new ContentCacheSet;
    int sequence[]={100,1,0,8,200,3,10};
    for (int i=0; i<sizeof(sequence)/sizeof(int); i++)
      Content *c=s->Get(make_pair(sequence[i]-i, i));
    delete s;
    CHECK_ARRAY_EQUAL(sequence, &(Content::construct_seq[0]), sizeof(sequence)/sizeof(int));
    CHECK_EQUAL(sizeof(sequence)/sizeof(int), static_cast<int>(Content::destruct_seq.size()));
  }
}