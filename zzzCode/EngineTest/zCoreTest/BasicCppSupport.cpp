#include <UnitTest++.h>
#include <zCore/Math/Vector3.hpp>
using namespace zzz;

SUITE(BasicCppTest) {
  TEST(SizeofTest) {
    int x[10]={};
    CHECK_EQUAL(10, sizeof(x)/sizeof(x[0]));

    Vector3uc y[20]={};
    CHECK_EQUAL(20, sizeof(y)/sizeof(y[0]));

    int z[]={1,2,3};
    CHECK_EQUAL(3, sizeof(z)/sizeof(z[0]));

    CHECK_EQUAL(1, sizeof(char));
    CHECK_EQUAL(2, sizeof(short));
    CHECK_EQUAL(4, sizeof(int));
    CHECK_EQUAL(4, sizeof(long));

    CHECK_EQUAL(1, sizeof(zint8));
    CHECK_EQUAL(1, sizeof(zuint8));
    CHECK_EQUAL(2, sizeof(zint16));
    CHECK_EQUAL(2, sizeof(zuint16));
    CHECK_EQUAL(4, sizeof(zint32));
    CHECK_EQUAL(4, sizeof(zuint32));
    CHECK_EQUAL(8, sizeof(zint64));
    CHECK_EQUAL(8, sizeof(zuint64));

    CHECK_EQUAL(4, sizeof(float));
    CHECK_EQUAL(8, sizeof(double));

    CHECK_EQUAL(4, sizeof(zfloat32));
    CHECK_EQUAL(8, sizeof(zfloat64));
  }
}