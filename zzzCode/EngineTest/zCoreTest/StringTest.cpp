#include <UnitTest++.h>
#include <zCore/Utility/StringTools.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zcore/Utility/Format.hpp>
using namespace zzz;

SUITE(StringTest)
{
  TEST(StringConvertTest)
  {
    CHECK_EQUAL("123",ToString(123));
    CHECK_EQUAL("123.456",ToString(123.456));
    CHECK_EQUAL("123 456 789 ",ToString(Vector3i(123,456,789)));
    
    CHECK_EQUAL(123,FromString<int>(string("123")));
    CHECK_EQUAL(123.456,FromString<double>(string("123.456")));
    CHECK_EQUAL(Vector3i(123,456,789),FromString<Vector3i>(string("123 456 789")));
    
    CHECK_EQUAL(123,FromString<int>("123"));
    CHECK_EQUAL(123.456,FromString<double>("123.456"));
    CHECK_EQUAL(Vector3i(123,456,789),FromString<Vector3i>("123 456 789"));
  }

  TEST(Base64)
  {
    string str="Welcome to zzzEngine";
    string code;
    Base64Encode(str.c_str(),str.size(),code);
    CHECK(code=="V2VsY29tZSB0byB6enpFbmdpbmU=");
    char *data=new char[code.size()];
    data[Base64Decode(code,data,code.size())]='\0';
    CHECK(str==data);
    delete[] data;
  }

  TEST(StringOperatorTest)
  {
    string x;
    x<<"abc";
    CHECK(x=="abc");
    int a=1;
    x<<a;
    CHECK(x=="abc1");
    x<<" "<<':'<<1234<<" "<<1.1234<<'\n';
    CHECK(x=="abc1 :1234 1.1234\n");
  }

  TEST(StringReplacementTest) {
    CHECK_EQUAL(string("This is not a test!"), Replace("This is a test!", " is", " is not"));
    CHECK_EQUAL(string("Any cherry is cherry, and good cherry!"), Replace("Any apple is apple, and good apple!", "apple", "cherry"));
  }

  TEST(TrimTest) {
    string test = "\t \t \nxyz\r\t   ";
    CHECK_EQUAL(string("xyz"), Trim(test));
  }

  TEST(FormatTest) {
    string a = Format("%1%") % 1;
    CHECK_EQUAL("1", a);
    a = Format("%2%%1%") % 2 % a;
    CHECK_EQUAL("12", a);
    string pat("%1%abcd%1%");
    a = pat % a;
    CHECK_EQUAL("12abcd12", a);
  }
}