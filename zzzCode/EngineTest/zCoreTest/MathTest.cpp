#include <zCore/Math/Math.hpp>
#include <zCore/Math/Random.hpp>
#include <UnitTest++.h>
using namespace zzz;
SUITE(MathTest) {
  TEST(IsNaNTest) {
    double c=0;
    double a=1.0/c;
    float b=1.0f/c;
    CHECK(IsInf(a));
    CHECK(IsInf(b));
    CHECK(!IsInf(c));
    CHECK(IsNaN(a-b));

    CHECK(IsBad(a));
    CHECK(IsBad(b-a));
  }

  TEST(StaticIntPow) {
    const int r=StaticPow<2,3>::RESULT;
    CHECK_EQUAL(8,r);
  }

  TEST(MagicSqrt) {
    RandomReal<float> r(0,999999);
    for (int i=0; i<10; i++) {
      float x=r.Rand();
      float root0=Sqrt(x);
      float root1=MagicSqrt(x);
      CHECK_CLOSE(root0,root1,x/1000000.0f);
      CHECK_CLOSE(1.0f/root0,MagicInvSqrt(x),x/1000000.0f);
      CHECK_CLOSE(1.0f/root0,MagicInvSqrt2(x),x/1000000.0f);
    }
  }
  TEST(BasicTest) {
    CHECK_EQUAL(1.1, Min(1.1, 2.1));
    CHECK_EQUAL(2.1, Max(1.1, 2.1));
    CHECK_EQUAL(1, Min(1, 2));
    CHECK_EQUAL(2, Max(1, 2));
    CHECK_EQUAL(2.1, Abs(2.1));
    CHECK_EQUAL(2.1, Abs(-2.1));
    CHECK_EQUAL(2, Abs(2));
    CHECK_EQUAL(2, Abs(-2));
  }
}