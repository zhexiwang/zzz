#include <UnitTest++.h>
#include <zCore/Utility/Any.hpp>
#include <zCore/Utility/AnyStack.hpp>
using namespace zzz;

SUITE(AnyTest)
{
  TEST(Store)
  {
    int a=5;
    double b=10;
    char c='a';
    string x("abcde");
    Any av=a;
    Any ap=&a;
    Any bv=b;
    Any bp=&b;
    Any cv=c;
    Any cp=&c;
    Any xv=x;
    Any xp=&x;
    CHECK_EQUAL(a,any_cast<int>(av));
    CHECK_EQUAL(&a,any_cast<int*>(ap));
    CHECK_EQUAL(b,any_cast<double>(bv));
    CHECK_EQUAL(&b,any_cast<double*>(bp));
    CHECK_EQUAL(c,any_cast<char>(cv));
    CHECK_EQUAL(&c,any_cast<char*>(cp));
    CHECK_EQUAL(x,any_cast<string>(xv));
    CHECK_EQUAL(&x,any_cast<string*>(xp));
  }

  TEST(GlobalStackTest)
  {
    int a=5, a2;
    double b=10, b2;
    char c='a', c2;
    string x("abcde"), x2;
    int ori_size = ZGS.Size();
    ZGS.Push(a);
    ZGS.Push(b);
    ZGS.Push(c);
    ZGS.Push(x);
    CHECK_EQUAL(ori_size+4, ZGS.Size());
    ZGS.Pop(x2);
    ZGS.Pop(c2);
    ZGS.Pop(b2);
    ZGS.Pop(a2);
    CHECK_EQUAL(a, a2);
    CHECK_EQUAL(b, b2);
    CHECK_EQUAL(c, c2);
    CHECK_EQUAL(x, x2);
    CHECK_EQUAL(ori_size, ZGS.Size());
  }
}