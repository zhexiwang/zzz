include($$PWD/../../zzzapp.pri)

QT       -= core gui

TARGET = MathTest$${SUFFIX}
DESTDIR = $${ZZZ_DIR}/bin
message(Building: $$DESTDIR $$TARGET)
TEMPLATE = app

SOURCES += \
    main.cpp \
    AnyTest.cpp

HEADERS +=

OTHER_FILES +=

BOOSTENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsBoost.pri) {
    error("Cannot include QTFlagsBoost.pri")
}

UNITTESTENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsUnitTest++.pri) {
    error("Cannot include QTFlagsUnitTest++.pri")
}

YAMLCPPENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsYamlCpp.pri) {
    error("Cannot include QTFlagsYamlCpp.pri")
}

Z3RDENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZ3rd.pri) {
    error("Cannot include QTFlagsZ3rd.pri")
}

ZCOREENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZCore.pri) {
    error("Cannot include QTFlagsZCore.pri")
}

ZGRAPHICSENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZGraphics.pri) {
    error("Cannot include QTFlagsZGraphics.pri")
}

ZVISIONENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZVision.pri) {
    error("Cannot include QTFlagsZVision.pri")
}

ZIMAGEENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZImage.pri) {
    error("Cannot include QTFlagsZImage.pri")
}

ZMATRIXENABLE = 1
!include($${ZZZ_DIR}/QtFlag/QTFlagsZMatrix.pri) {
    error("Cannot include QTFlagsZMatrix.pri")
}

message(DEFINES: $$DEFINES)
message(INCLUDEPATH: $$INCLUDEPATH)
message(LIBS: $$LIBS)
