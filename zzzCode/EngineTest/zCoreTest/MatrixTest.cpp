#include <zCore/Math/Matrix2x2.hpp>
#include <zCore/Math/Matrix3x3.hpp>
#include <zCore/Math/Matrix4x4.hpp>
#include <zCore/Math/Vector2.hpp>
#include <zCore/Math/Vector3.hpp>
#include <zCore/Math/Math.hpp>
#include <UnitTest++.h>

using namespace zzz;

SUITE(MatrixTest) {
  TEST(Multiple) {
    Vector2i a(1,2);
    Matrix2x2i A(1,2,3,4);
    CHECK_EQUAL(Vector2i(7,10), a * A);
    CHECK_EQUAL(Vector2i(5,11), A * a);
    Matrix2x2i B(5,6,7,8);
    CHECK_EQUAL(Matrix2x2i(19, 22, 43, 50), A * B);
    CHECK_EQUAL(Matrix2x2i(23, 34, 31, 46), B * A);
    CHECK_EQUAL(Matrix2x2i(5, 12, 21, 32), A.ScalarMultiply(B));
    Matrix<2,3,int> C;
    C.Row(0) = Vector3i(5, 6, 7);
    C.Row(1) = Vector3i(8, 9 ,10);
    MatrixBase<2,3,int> res;
    res.Row(0) = Vector3i(21, 24, 27);
    res.Row(1) = Vector3i(47, 54, 61);
    CHECK_EQUAL(res, A * C);
  }
}