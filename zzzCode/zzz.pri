message(Qt version: $$[QT_VERSION])
message(Qt is installed in $$[QT_INSTALL_PREFIX])

ZZZ_DIR = $$PWD
message(ZZZ_DIR: $$ZZZ_DIR)
ZZZ_LIB = $$ZZZ_DIR/lib

###############################################
ZZZ_ENGINE_DIR = $${ZZZ_DIR}/zzzEngine
message(ZZZ_ENGINE_DIR: $$ZZZ_ENGINE_DIR)

SUFFIX =
CONFIG(debug) {
  SUFFIX = D
}
contains(QMAKE_HOST.arch, x86_64) {
  ARCH=64
  SUFFIX+=_X64
}
contains(QMAKE_HOST.arch, x86) {
  ARCH=32
}
QMAKE_CXXFLAGS += -std=c++11

INCLUDEPATH += $$ZZZ_ENGINE_DIR/z3rd
INCLUDEPATH += $$ZZZ_ENGINE_DIR/zCore
INCLUDEPATH += $$ZZZ_ENGINE_DIR/zNet
INCLUDEPATH += $$ZZZ_ENGINE_DIR/zGraphics
INCLUDEPATH += $$ZZZ_ENGINE_DIR/zImage
INCLUDEPATH += $$ZZZ_ENGINE_DIR/zVision
INCLUDEPATH += $$ZZZ_ENGINE_DIR/zMatrix

INCLUDEPATH += $$ZZZ_DIR/3rdParty/Freeglut/include
INCLUDEPATH += $$ZZZ_DIR/3rdParty/Glew
INCLUDEPATH += $$ZZZ_DIR/3rdParty/UnitTest++/src
INCLUDEPATH += $$ZZZ_DIR/3rdParty/yaml-cpp/include

LIBS += -L$$ZZZ_LIB
