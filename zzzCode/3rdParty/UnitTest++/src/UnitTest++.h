#ifndef UNITTESTCPP_H
#define UNITTESTCPP_H

//lint -esym(1509,*Fixture)

#include "Config.h"
#include "Test.h"
#include "TestList.h"
#include "TestSuite.h"
#include "TestResults.h"

#include "TestMacros.h"

#include "CheckMacros.h"
#include "TestRunner.h"
#include "TimeConstraint.h"

#ifndef ZZZ_NO_PRAGMA_LIB

#ifdef _DEBUG
  #ifdef _WIN64
    #pragma comment(lib, "UnitTest++D_X64.lib")
  #else
    #pragma comment(lib, "UnitTest++D.lib")
  #endif  // _WIN64
#else
  #ifdef _WIN64
    #pragma comment(lib, "UnitTest++_X64.lib")
  #else
    #pragma comment(lib, "UnitTest++.lib")
  #endif  // _WIN64
#endif  // _DEBUG

#endif // ZZZ_NO_PRAGMA_LIB


#endif // UNITTESTCPP_H