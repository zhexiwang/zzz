include($$PWD/../../zzzlib.pri)

QT       -= core gui

TARGET = UnitTest$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    $$files($$PWD/src/*.cpp) \
    $$files($$PWD/src/Win32/*.cpp) \
    $$files($$PWD/src/Prosix/*.cpp)

HEADERS += \
    $$files($$PWD/src/*.h) \
    $$files($$PWD/src/Win32/*.h) \
    $$files($$PWD/src/Prosix/*.h)

OTHER_FILES += \

message(DEFINES: $$DEFINES)
