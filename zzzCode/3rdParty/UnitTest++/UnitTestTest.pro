include($$PWD/../../zzzapp.pri)

QT       -= core gui

TARGET = UnitTestTest$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = app

SOURCES += \
    $$files($$PWD/src/tests/*.cpp)

HEADERS += \
    $$files($$PWD/src/tests/*.h)

OTHER_FILES += \

UNITTESTENABLE = 1
!include($$ZZZ_DIR/QtFlag/QTFlagsUnitTest++.pri) {
    error("Cannot include QTFlagsUnitTest++.pri")
}

message(DEFINES: $$DEFINES)
message(DESTDIR: $$DESTDIR)
QMAKE_POST_LINK += "$$DESTDIR/$$TARGET"
