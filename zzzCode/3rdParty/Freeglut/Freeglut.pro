include($$PWD/../../zzzlib.pri)

QT       -= core gui

TARGET = Freeglut$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    $$files($$PWD/src/*.c)

HEADERS += \
    $$files($$PWD/include/GL/*.h)

OTHER_FILES += \

DEFINES += "FREEGLUT_LIB_PRAGMAS=0"
DEFINES += FREEGLUT_STATIC

message(DEFINES: $$DEFINES)
message(INCLUDEPATH: $$INCLUDEPATH)
