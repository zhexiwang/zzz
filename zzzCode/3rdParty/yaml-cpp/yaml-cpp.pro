include($$PWD/../../zzzlib.pri)

QT       -= core gui

TARGET = yaml-cpp$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    $$files($$PWD/src/*.cpp) \
    $$files($$PWD/src/contrib/*.cpp) \
    $$files($$PWD/util/*.cpp) \
    $$files($$PWD/yaml-cpp/*.cpp)

HEADERS += \
    $$files($$PWD/src/*.h) \
    $$files($$PWD/src/contrib/*.h) \
    $$files($$PWD/util/*.h) \
    $$files($$PWD/yaml-cpp/*.h)

OTHER_FILES += \

message($$DEFINES)
