#-------------------------------------------------
#
# Project created by QtCreator 2012-09-29T16:36:28
#
#-------------------------------------------------

QT       -= core gui

TARGET = yaml-cpp
TEMPLATE = lib
CONFIG += staticlib

SOURCES += yamlcpp.cpp

HEADERS += yamlcpp.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
