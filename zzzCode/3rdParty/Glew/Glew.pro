include($$PWD/../../zzzlib.pri)

QT       -= core gui

TARGET = Glew$${SUFFIX}
message(Building: $$TARGET)
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    $$files($$PWD/*.c)

HEADERS += \
    $$files($$PWD/*.h)

OTHER_FILES += \

DEFINES ~= s/ZETA/ZZZ
DEFINES ~= s/_ENABLE/

DEFINES += GLEW_STATIC
message($$[DEFINES])
